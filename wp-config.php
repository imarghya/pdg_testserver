<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pedagoge_test');

/** MySQL database username */
define('DB_USER', 'pedagoge_test');

/** MySQL database password */
define('DB_PASSWORD', 'pedagoge_test_2017');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q,XZmc]i;GWm~*YQLrV gG7>$2~x$Lek AD>2z*~3D1>#g-4CEs7Ea0Q(z%?w#6(');
define('SECURE_AUTH_KEY',  'Q2*z6h!]zVlV(a^Vdm5hWo}.3G{Ib-.7_u~vD`OidpJZ`SDJXbjlX?[FizM[#e3/');
define('LOGGED_IN_KEY',    'I_j4m]x(o[.XH]LW%Cm(]DxSOKwXD]Lf,tnu*Ih3PKl2n$fz=&XtaN* w9;p]ync');
define('NONCE_KEY',        'I=zMrK&:MTYa`3.[S@_R%#@CWzPN_4|L&kJPl2+,[z{]`5|0TF:&mUueAB~4RAWo');
define('AUTH_SALT',        '0^(j,K*o]-+k/`JucR@,2;bv*@|s^q!9{L2^c>S8eOMNXcpm_o^!e47k{$[&vWA~');
define('SECURE_AUTH_SALT', '}CB+@s<F{ bo$T&x0cTekQ~qM/3UYroZU1|O}]-E{?4A`=vV6 Me(S!M@)Sw6+<Z');
define('LOGGED_IN_SALT',   'C4gnL).psAhfKvo@MzB%7Z)]7|+*A.3B4HftM2,V;FdT++teteepP(=}SDd?3 4I');
define('NONCE_SALT',       'y;,#P:[<e{S[;dg^ 7HAesF?ifTEwI3Wd/I@y2W4[hie23w&+I>rpjiHp5Tb6Bs!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
 
define('WP_DEBUG_LOG', false);
 
define('WP_DEBUG_DISPLAY', false);

//define('WP_ALLOW_REPAIR', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
