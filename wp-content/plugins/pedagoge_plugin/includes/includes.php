<?php
 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/***************************Includes Section******************************/
include_once( PEDAGOGE_PLUGIN_DIR.'includes/vendor/autoload.php' ); //Composer Autoloader

include_once( PEDAGOGE_PLUGIN_DIR.'includes/helpers/PedagogeLogWriter.php' );
//include helper utilities file
include_once( PEDAGOGE_PLUGIN_DIR.'includes/helpers/utilities.php' );

//include virtual page constructors
include_once( PEDAGOGE_PLUGIN_DIR.'includes/pedagoge_virtual_pages/pedagoge_virtual_page_constructor.php' );
include_once( PEDAGOGE_PLUGIN_DIR.'includes/pedagoge_virtual_pages/PedagogeRouter.php' );
