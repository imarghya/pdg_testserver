<?php
 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Pedagoge Application's logger. It should always be used for logging any value to the log file.
 * Log file is located at plugin's log folder.
 * 
 * @param $str_val (string) 
 * 		whatever you wish to write in the log file.
 */
function pedagoge_applog( $str_val ) {	
	$logger = new PedagogeLogWriter();
	$logger->applog($str_val);
	unset($logger);
}

/**
 * Create other Application Virtual Pages after a defined route dynamically.
 * 
 * @param PedagogeRouter $pedagoge_router (instance of PedagogeRouter)
 * @param string $endpoint  - defined route
 * 
 * @return void
 */
function fn_pdg_dynamic_route_create(PedagogeRouter $pedagoge_router, array $routes) {	
	
	/**
	 * Current Page URL 
	 */
	$pdg_current_page_url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$wordpress_home_url = trim(home_url());
	
	/**
	 * Removing wordpress home url part from current url so that we can get the url path to work with.
	 * This technique had to be implemented because earlier implementation did not work for localhost.
	 */
	$pdg_replaced_url = str_replace($wordpress_home_url, '', $pdg_current_page_url);
	
	/**
	 * Seperate the url to path and query and just take url path.
	 */
	$pdg_processed_url_path = parse_url($pdg_replaced_url, PHP_URL_PATH);
	
	$route_found = FALSE;
	
	/**
	 * Search if current url starts with any defined route found in the routes array.
	 */
	foreach($routes as $route) {
		$pdg_app_url_position = strpos( $pdg_processed_url_path, $route );
		if($pdg_app_url_position !== false && $pdg_app_url_position == 0) {
			$route_found = TRUE;
			break;
		}
	}
	
	//if route is found then create the dynamic page.
	if($route_found) {
		$pedagoge_router->fn_create_dynamic_pages( $pdg_processed_url_path );
	}	
}

/**
 * Function converts a slug (URL Part) to Pedagoge's Controller Class Name.
 * 
 * @param string $current_slug (slug) required field.
 * 
 * @return string
 * 		Returns Pedagoge's Controller Class Name 
 */
function fn_slug_to_controller_class( $current_slug = null ) {
	if( is_null( $current_slug ) ) {
		return 'Controller404';
	}
	$class_name = strtolower( trim( $current_slug, '/' ) );
	//replace - or _ with /
	$class_name = str_replace( '-', '/', $class_name );
	$class_name = str_replace( '_', '/', $class_name );		
	//explode the string into array
	$class_name_arr = explode('/', $class_name);
	
	$new_class_name = 'Controller'; //All Controller Classes will have this as prefix
	foreach( $class_name_arr as $name ) {
		$new_class_name .= ucfirst( $name ); //Convert First Character to uppercase
	}
	return $new_class_name;
}


/**
 * Search and Load PHP Files automatically based on classes name
 * Pedagoge Application Classes
 * 	Search in
 * 		plugin directory
 * 		plugin template directory
 * 		includes / classes / controllers / helpers / models / views
 * 		
 */
 function pedagoge_autoload_classes( $class ) {
		
	if($class == 'wp_atom_server' ) {
		return;
	}

	$includes_dir = PEDAGOGE_PLUGIN_DIR."swpmvc/";
	$recursive_directory_iterator = new RecursiveDirectoryIterator($includes_dir);
	foreach(new RecursiveIteratorIterator($recursive_directory_iterator) as $file)
	{
		$file_found = FALSE; 
		
		$extension = $file->getExtension();
		
		if ( $extension === 'php') {
				
			$file_name = $file->getFilename();			
			$extension_less_file_name = $file->getBasename('.php');
						
			//unset($exploded_file);
			if( $class === $extension_less_file_name ) {
				$file_found = TRUE;
				include_once($file);
				break;
			}	
		}
	}
}

	
/**
 * Pedagoge Application's temporary comments logger. It should be used when
 * function pedagoge_applog is not available or causing error. 
 * Log File is located at uploads folder in wp-content 
 * 
 * @param $str_val (string)
 * 		Whatever you wish to write in the log file.
 */
function pedagoge_emergency_log_writer( $str_val ) {
	$upload_dir = wp_upload_dir();
	$log_file_path=$upload_dir['basedir'].'/applog.txt';
	
	
	if(empty($str_val))
	{
		$str_val='Empty call (String value was not provided)';
	}
	
	$str_date_time = date('d-m-Y h:i:s A -'); // add timestamp
	$str_val = $str_date_time.$str_val." \n"; // create newline
	
	$my_file = $log_file_path;
	$handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file);	
	fwrite($handle, $str_val);
	fclose($handle);
}


/**
 * Returns the timezone string for a wordpress site, even if it's set to a UTC offset
 *
 * Adapted from http://www.php.net/manual/en/function.timezone-name-from-abbr.php#89155
 * 
 * @return string valid PHP timezone string
 * 
 * @link https://www.skyverge.com/blog/down-the-rabbit-hole-wordpress-and-timezones/
 */ 
function pedagoge_get_wordpress_timezone_string() {	
 
    // if site timezone string exists, return it
    if ( $timezone = get_option( 'timezone_string' ) ) {
    	return $timezone;
    }        
 
    // get UTC offset, if it isn't set then return UTC
    if ( 0 === ( $utc_offset = get_option( 'gmt_offset', 0 ) ) ) {    	
    	return 'UTC';
    }        
 
    // adjust UTC offset from hours to seconds
    $utc_offset *= 3600;
 
    // attempt to guess the timezone string from the UTC offset
    if ( $timezone = timezone_name_from_abbr( '', $utc_offset, 0 ) ) {    	
        return $timezone;
    }
	
    // last try, guess timezone string manually
    $is_dst = date( 'I' );
 
    foreach ( timezone_abbreviations_list() as $abbr ) {
        foreach ( $abbr as $city ) {
            if ( $city['dst'] == $is_dst && $city['offset'] == $utc_offset )
                return $city['timezone_id'];
        }
    }
     
    // fallback to UTC
    return 'UTC';
}

if( !function_exists( 'array_insert_before' ) ) {
	/**
	 * Inserts a new key/value before the key in the array.
	 *
	 * @param $key
	 *   The key to insert before.
	 * @param $array
	 *   An array to insert in to.
	 * @param $new_key
	 *   The key to insert.
	 * @param $new_value
	 *   An value to insert.
	 *
	 * @return
	 *   The new array if the key exists, FALSE otherwise.
	 *
	 * @see array_insert_after()
	 */
	function array_insert_before($key, array $array, $new_key, $new_value) {
	  if (array_key_exists($key, $array)) {
	    $new = array();
	    foreach ($array as $k => $value) {
	      if ($k === $key) {
	        $new[$new_key] = $new_value;
	      }
	      $new[$k] = $value;
	    }
	    return $new;
	  }
	  return FALSE;
	}
}

if( !function_exists( 'array_insert_after' ) ) { 
	/**
	 * Inserts a new key/value after the key in the array.
	 *
	 * @param $key
	 *   The key to insert after.
	 * @param $array
	 *   An array to insert in to.
	 * @param $new_key
	 *   The key to insert.
	 * @param $new_value
	 *   An value to insert.
	 *
	 * @return
	 *   The new array if the key exists, FALSE otherwise.
	 *
	 * @see array_insert_before()
	 */
	function array_insert_after($key, array $array, $new_key, $new_value) {
	  if (array_key_exists($key, $array)) {
	    $new = array();
	    foreach ($array as $k => $value) {
	      $new[$k] = $value;
	      if ($k === $key) {
	        $new[$new_key] = $new_value;
	      }
	    }
	    return $new;
	  }
	  return FALSE;
	}
}

function pedagoge_array_search($array_var, $field, $value) {
   foreach($array_var as $key => $key_value)
   {
      if ( $key_value[$field] === $value )
         return $key;
   }
   return '';
}

/**
 * Function returns visitor's IP Address.
 * 
 * @return string containing IP Address.
 */
function pedagoge_get_visitor_ip() {
	$visitor_ip_address = '';
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		//check ip from share internet
		$visitor_ip_address = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		//to check ip is pass from proxy
		$visitor_ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$visitor_ip_address = $_SERVER['REMOTE_ADDR'];
	}
	return $visitor_ip_address;
}

/**
 * Replace the 'NULL' string with NULL
 * 
 * @param  string $query
 * @return string $query
 */

function pdg_string_null_to_null( $query )
{
    return str_ireplace( "'NULL'", "NULL", $query ); 
}