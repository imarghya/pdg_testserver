<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

global $post, $wp_query, $current_user, $wpdb;

$pdg_current_page_url = trim( $wp_query->virtual_page->getUrl(), '/' );

/**
 * Redirection rules
 */
$profile_page = home_url( '/profile' );
$signup_url   = home_url( '/signup' );
$search_url   = home_url( '/search' );


if ( strpos( $pdg_current_page_url, 'search/' ) !== false ) {
	include_once('pdg_routers_code/pdg_seo_urls_router.php');
} else {
	/**
	 * Redirection rules
	 */
	switch ( $pdg_current_page_url ) {
		case 'signup':
		case 'login':
			if ( is_user_logged_in() ) {
				wp_redirect( $profile_page );
				exit();
			}
			break;
		case 'register':
			if ( is_user_logged_in() ) {
				wp_redirect( $profile_page );
				exit();
			} else {
				wp_redirect( $signup_url );
				exit();
			}
			break;
		case 'trainer':
		case 'teacher':
		case 'institute':
			wp_redirect( $search_url );
			exit();
			break;
		case '':
			break;
		default:
			break;

	}

	$controller_class_name = fn_slug_to_controller_class( $pdg_current_page_url );

	/**
	 * Code section to process extra information for teachers/institutes
	 */
	require_once('pdg_routers_code/pdg_process_teachers_institute_routes.php');

	/**
	 * Code to instantiate class as and when required based on slug
	 * or other scenarios (i.e. login controller /  account activation controller)
	 */
	require_once('pdg_routers_code/pdg_controller_class_loader.php');

	$controller_class->fn_construct_class();

	//set id variables for the public profiles
	if ( in_array( $teacher_institute_url_array[0], $allowed_url_slug_array ) ) {
		$profile_id = isset( $teacher_institute_url_array[1] ) ? $teacher_institute_url_array[1] : null;
		$controller_class->app_data['pdg_profile_slug'] = $profile_id;
	}

	$str_return = $controller_class->fn_display();
	unset( $controller_class );
	echo $str_return;
}

