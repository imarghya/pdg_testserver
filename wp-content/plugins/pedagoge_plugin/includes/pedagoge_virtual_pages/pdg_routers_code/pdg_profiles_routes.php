<?php

if ( ! is_user_logged_in() ) {
	$controller_class = new ControllerLogin();
} else {
	/**
	 * 1. if user account is not activated then load activate controller
	 * 2. load controller for different user roles
	 */
	$current_user_id    = get_current_user_id();
	$current_userinfo   = get_user_by( 'id', $current_user_id );
	$current_user_email = $current_userinfo->user_email;

	if ( ! current_user_can( 'manage_options' ) ) {
		if (!empty($pdg_user_info) && $profile_activated == 'no' ) {
			//load profile activation
			$controller_class = new ControllerActivate();
		} else {
			switch ( $user_role_name ) {
				case 'teacher':
					$controller_class = new ControllerPrivateteacher();
					break;
				case 'institution':
					$controller_class = new ControllerPrivateinstitute();
					break;
				case 'student':
				case 'guardian':
					$controller_class = new ControllerProfileStudent();
					break;
				case 'trainer':
					$controller_class = new ControllerPrivatetrainer();
					break;
				default:
					$controller_class = new $controller_class_name();
					break;
			}			
		}
	} else {
		$controller_class = new $controller_class_name();
	}
}