<?php

$pdg_user_info     = '';
$current_user_id   = '';
$profile_activated = 'no';
$user_role_name    = '';

if(is_user_logged_in()) {
	$current_user_id = get_current_user_id();
	
	switch($pdg_current_page_url) {
		case 'profile':
		case 'manage':
			$view_pdg_user_info = new ModelMaster('view_pdg_user_info');	
			if(!empty($current_user_id) && $current_user_id!=0) {					
				$pdg_user_info = $view_pdg_user_info
									->where(array(), 'user_id', $current_user_id)
									->find();
									
				if(!empty($pdg_user_info)) {
					foreach($pdg_user_info as $pdg_user) {
						$profile_activated = $pdg_user->profile_activated;
						$user_role_name = $pdg_user->user_role_name;
					}	
				}
			}
			break;
	}		
}

$controller_class = null;

if ( class_exists( $controller_class_name ) ) {
	switch ( $pdg_current_page_url ) {
		case 'profile':
			require_once('pdg_profiles_routes.php');
			break;
		case 'settings':
		case 'manage':
		case 'reviews':
		case 'dashboard':
		case 'manageworkshop':
			$controller_class = is_user_logged_in() ? new $controller_class_name() : new ControllerLogin();
			break;
		case 'edit' :
			require_once('pdg_profiles_edit_routes.php');
			break;
		default:
			$controller_class = new $controller_class_name();
			break;
	}

} else {
	//if controller does not exist then load 404		
	$controller_class = new Controller404();
}