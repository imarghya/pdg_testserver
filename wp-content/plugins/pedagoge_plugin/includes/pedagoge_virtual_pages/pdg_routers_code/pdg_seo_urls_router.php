<?php
global $wpdb;
$search_parameter_array = explode('/', $pdg_current_page_url);

if(count($search_parameter_array)<2) {
	wp_redirect( $search_url, 301 );
	exit;
}

$city_name = sanitize_text_field($search_parameter_array[1]);
$subject_name = sanitize_text_field($search_parameter_array[2]);
$user_role = '';

$Illigal_user_role = FALSE;

$str_seo_sql = "
	select * from view_pdg_seo_combo where
	subject_slug = '$subject_name' and
	city_slug = '$city_name' 
";

if(count($search_parameter_array) >= 4) {
	$seo_user_role_name = sanitize_text_field($search_parameter_array[3]);
	switch($seo_user_role_name) {
		case 'tuition-teachers':
			$user_role = 'teacher';
			$str_seo_sql .= " and role = '$user_role'";
			break;
		case 'coaching-classes':
			$user_role = 'institution';
			$str_seo_sql .= " and role = '$user_role'";
			break;
		default:
			$Illigal_user_role = TRUE;
			break;
	}
	
}

$found_seo_result = $wpdb->get_results($str_seo_sql);

$controller_class = new ControllerSearch();
$controller_class->fn_construct_class();

if(empty($found_seo_result) || $Illigal_user_role) {
	//show search not found
	$controller_class->return_empty_search_result = TRUE;	
} else {
	// search and load content
	$seo_title = '';
	$seo_meta = '';	
	$seo_subject_id = '';
	$seo_subject_name = '';
	$seo_city_name = '';
	$seo_city_id = '';
	foreach ( $found_seo_result as $seo_db ) {
		$seo_title       = $seo_db->title;
		$seo_meta        = $seo_db->meta;		
		$seo_subject_id  = $seo_db->subject_id;
		$seo_subject_name = $seo_db->subject_name;
		$seo_city_name = $seo_db->city_name;
		$seo_city_id = $seo_db->city_id;
	}
	if(empty($seo_title)) {
		$seo_title = ControlPanelSeo::fn_return_seo_title($seo_city_name, $seo_subject_name, $user_role);
	}
	
	if(empty($seo_meta)) {
		$seo_meta = ControlPanelSeo::fn_return_seo_meta($seo_city_name, $seo_subject_name, $user_role);
	}
	$controller_class->app_data['seo_title']       = $seo_title;
	$controller_class->app_data['seo_meta']        = $seo_meta;
	$controller_class->app_data['seo_role']        = $user_role;
	//$controller_class->app_data['seo_subject_name']  = $seo_subject_name;
	$controller_class->app_data['seo_city_id'] = $seo_city_id;
	$_GET['pdg_subject'] = $seo_subject_name;
}

$str_return = $controller_class->fn_display();
unset( $controller_class );
echo $str_return;
