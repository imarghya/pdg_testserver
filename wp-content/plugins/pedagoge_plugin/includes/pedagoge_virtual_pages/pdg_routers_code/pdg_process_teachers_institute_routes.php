<?php
//process teachers and institute
$teacher_institute_url_array = explode( '/', $pdg_current_page_url );
$allowed_url_slug_array      = array ( 'teacher', 'institute', 'trainer' );
if ( in_array( $teacher_institute_url_array[0], $allowed_url_slug_array ) ) {

	if ( count( $teacher_institute_url_array ) > 2 ) {
		$teacher_institute_profile_url = '/' . $teacher_institute_url_array[0] . '/' . $teacher_institute_url_array[1];
		wp_redirect( $teacher_institute_profile_url );
		exit();
	} else {

		$controller_class_name = 'Controller' . ucfirst( $teacher_institute_url_array[0] );

	}
}