<?php
$profile_edit_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_teacher_profile_edit');

if($profile_edit_permission) {
	$profile_edit_role = isset($_GET['role']) ? $_GET['role'] : ''; 
	$profile_edit_teacher_institute_id = isset($_GET['id']) ? $_GET['id'] : '';
	switch($profile_edit_role) {
		case 'teacher':			
			$controller_class = new ControllerPrivateteacher();
			break;
		case 'institute':
			$controller_class = new ControllerPrivateinstitute();
			break;
		case 'student' :
		case 'guardian' :
		case 'trainer' :
			$controller_class = new ControllerEdit();
			break;
		default:
			$controller_class = new ControllerEdit();
			break;
	}
	
} else {
	$controller_class = new ControllerEdit();
}
