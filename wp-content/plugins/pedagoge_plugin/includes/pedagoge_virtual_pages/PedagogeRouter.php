<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PedagogeRouter {
	protected $pages_list = null;
	
	public function __construct() {
		
		$this->pages_list = array();
	}	
	
	public function fn_return_menu_list() {		
		return $this->pages_list;
	}
	
	public function fn_create_pages() {
		add_action( 'gm_virtual_pages', array( $this, 'pdg_page_factory' ) );
	}
	
	public function pdg_page_factory( $controller ) {
		$pages_list = $this->pages_list;	
		foreach( $pages_list as $page) {			
			$page_url = $page['url'];			
			$page_title = $page['title'];
			$controller->addPage( new \GM\VirtualPages\Page( $page_url ) )
		        ->setTitle( $page_title )
		        ->setTemplate( 'pedagoge_controller_loader.php' );
		}
	}
	
	public function fn_create_master_page() {		
		//Pedagoge Main Application Root Page
		$pedagoge_app_root_page = array();
		$pedagoge_app_root_page['url'] = PEDAGOGE_ENDPOINT_URL;
		$pedagoge_app_root_page['title'] = 'Welcome to Pedagoge';
		$this->pages_list[] = $pedagoge_app_root_page;
	}
	
	public function fn_create_dynamic_pages( $page_slug ) {
		$dynamic_page = array();
		$url = $page_slug;		
		$dynamic_page['url'] = $url;
		$dynamic_page['title'] = '404 Module Not Found!';
		$this->pages_list[] = $dynamic_page;		
	}
}
