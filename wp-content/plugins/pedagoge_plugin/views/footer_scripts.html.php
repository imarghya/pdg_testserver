<?php

global $wp_query;

$pdg_current_page_url = trim( $wp_query->virtual_page->getUrl(), '/' );

$should_combine = false;

if ( PEDAGOGE_COMBINE_MINIFY ) {
	switch ( $pdg_current_page_url ) {
		case 'search':
			//case 'privateinstitute':
			$should_combine = true;
			break;
	}
}

if ( $should_combine ) {

	$footer_js_array = array ();

	if ( ! empty( $this->footer_common_js ) && is_array( $this->footer_common_js ) ) {
		foreach ( $this->footer_common_js as $js_file ) {
			if ( ! in_array( $js_file, $footer_js_array ) ) {
				$footer_js_array[] = $js_file;
			}
		}
	}
	if ( ! empty( $this->footer_js ) && is_array( $this->footer_js ) ) {
		foreach ( $this->footer_js as $js_file ) {
			if ( ! in_array( $js_file, $footer_js_array ) ) {
				$footer_js_array[] = $js_file;
			}
		}
	}
	if ( ! empty( $footer_js_array ) ) {

		$searialised_js_file_name = substr( md5( serialize( $footer_js_array ) ), 0, 10 );

		$cached_js_file_path = PEDAGOGE_PLUGIN_DIR . 'storage/cache/' . $searialised_js_file_name . '.js';
		$cached_js_file_url = PEDAGOGE_PLUGIN_URL . '/storage/cache/' . $searialised_js_file_name . '.js';
		if ( ! file_exists( $cached_js_file_path ) ) {
			//create cache file.
			$minifier = new MatthiasMullie\Minify\JS();

			foreach ( $footer_js_array as $jsfile ) {
				$processed_file_urlpath = parse_url( $jsfile, PHP_URL_PATH );

				$processed_file_path = str_replace( 'pedagoge_plugin/', '', substr( $processed_file_urlpath, strpos( $processed_file_urlpath, 'pedagoge_plugin' ) ) );
				$processed_file_path = PEDAGOGE_PLUGIN_DIR . $processed_file_path;

				$minifier->add( $processed_file_path );
			}
			$minifier->minify( $cached_js_file_path );
		}

		echo '
				<script type="text/javascript" src="' . $cached_js_file_url . '"></script>
			';
	}

	if ( ! empty( $this->app_js ) && is_array( $this->app_js ) ) {
		foreach ( $this->app_js as $js_file ) {
			echo '
				<script type="text/javascript" src="' . $js_file . '"></script>
			';
		}
	}


} else {

	if ( ! empty( $this->footer_common_js ) && is_array( $this->footer_common_js ) ) {
		foreach ( $this->footer_common_js as $js_file ) {
			echo '
				<script type="text/javascript" src="' . $js_file . '"></script>
			';
		}
	}
	if ( ! empty( $this->footer_js ) && is_array( $this->footer_js ) ) {
		foreach ( $this->footer_js as $js_file ) {
			echo '
				<script type="text/javascript" src="' . $js_file . '"></script>
			';
		}
	}

	if ( isset( $inline_js ) && is_array( $inline_js ) ) {
		foreach ( $inline_js as $js_code ) {
			echo $js_code;
		}
	}

	if ( ! empty( $this->app_js ) && is_array( $this->app_js ) ) {
		foreach ( $this->app_js as $js_file ) {
			echo '
				<script type="text/javascript" src="' . $js_file . '"></script>
			';
		}
	}

	//$this->app_data['inline_js'] = '';
}
?>
<script type='text/javascript'
        src='<?php echo get_template_directory_uri(); ?>/assets/js/custom-script-all.js?ver=1.2'></script>
<link rel='stylesheet'
      href='<?php echo get_template_directory_uri(); ?>/assets/css/custom-styles-all.css?ver=1.2'
      type='text/css' media='all'/>