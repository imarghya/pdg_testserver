
<!--
########  #######   #######  ######## ######## ########  
##       ##     ## ##     ##    ##    ##       ##     ## 
##       ##     ## ##     ##    ##    ##       ##     ## 
######   ##     ## ##     ##    ##    ######   ########  
##       ##     ## ##     ##    ##    ##       ##   ##   
##       ##     ## ##     ##    ##    ##       ##    ##  
##        #######   #######     ##    ######## ##     ##  
-->

	    	<footer class="site-footer site-section footer-bottom-padding">
	            <div class="container">
	                <!-- Footer Links -->
	                <div class="row">
	                    <div class="col-sm-6 col-md-4">
	                        <br><br>
	                        <center>
	                            <a href="<?php echo home_url('/'); ?>"><img src="<?php echo PEDAGOGE_PROUI_URL; ?>/img/Footer/home_35.png"></a>
	                        </center>
	                    </div>
	                    
	                    <div class="col-sm-6 col-md-4">
	                        <center>
	                            <h4 class="footer-heading" style="line-height: 0.2em;">Follow Us</h4>
	                            <ul class="footer-nav footer-nav-social list-inline">
	                                <li><a href="https://www.facebook.com/pedagoge0/" target="_blank"><i class="fa fa-facebook"></i></a></li>
	                                <li><a href="https://twitter.com/pedagogebaba" target="_blank"><i class="fa fa-twitter"></i></a></li>
	                                <li><a href="https://www.linkedin.com/company/pedagoge" target="_blank"><i class="fa fa-linkedin"></i></a></li>
	                                <li><a href="https://www.instagram.com/pedagogebaba/" target="_blank"><i class="fa fa-instagram"></i></a></li>
	                            </ul>

				                <div class="row">
				                	<div class="col-md-12 col-xs-12">
			                            <span class="footer-nav"> Subscribe to our &nbsp;<a href="http://blog.pedagoge.com/" target="_blank"><i class="fa fa-thumb-tack"></i>&nbsp;&nbsp;<b>BLOG</b></a></span>
				                	</div>
				                </div>
	                        </center>
                    	</div>
	                    
	                    <div class="col-sm-6 col-md-4">
	                        <br><br>
	                        <center>
	                            <img src="<?php echo PEDAGOGE_PROUI_URL; ?>/img/Footer/home_52.png">
	                        </center>
	                    </div>
	                </div><br>

	                <div class="row footer-nav">
	                    <div class="col-md-2 col-md-offset-1 text-center">
	                        <a href="<?php echo home_url('/terms'); ?>" target="_blank">Terms &amp; Conditions</a>
	                    </div>
	                    <div class="col-md-2 text-center">
	                        <a href="<?php echo home_url('/privacy-policy'); ?>" target="_blank">Privacy Policy</a>
	                    </div>
	                    <div class="col-md-2 text-center">
	                        <a href="<?php echo home_url('/affiliate'); ?>" target="_blank">Affiliate with us</a>
	                    </div>
	                    <div class="col-md-2 text-center">
	                        <a href="<?php echo home_url('/contact-us'); ?>" target="_blank">Contact Us</a>
	                    </div>
	                    <div class="col-md-2 text-center">
	                        <a href="<?php echo home_url('/site-map'); ?>" target="_blank">Site Map</a>
	                    </div>
	                </div>
	                <br>
	                <center><?php echo date('Y'); ?> &copy; <a href="<?php echo home_url('/'); ?>" target="_blank">Pedagoge.</a></center>
	                
	                <!-- END Footer Links -->
	            </div>
	        </footer>
	        <!-- END Footer -->
		</div>
	    <!-- END Page Container -->
	
	    <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
	    <a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>
		<!-- Custom Javascript Variables -->
		<script type="text/javascript">
			var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        	var $ajaxnonce = '';
        	var $pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>';
        	var $pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler';
        	var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
        	
        	var $ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
			
			
		</script>
		
		<!-- Load Scripts below -->
		<?php 
			
			echo $this->load_view('footer_scripts');
			
			if( isset($this->app_data['dynamic_js']) ) {
				echo $this->app_data['dynamic_js'];
			}
		?>	
	        
	    <script type="text/javascript">
	        $(document).ready(function() {
	        	if($.backstretch) {
	        		$(".change-background").backstretch("<?php echo PEDAGOGE_PROUI_URL; ?>/img/Slider_Images/Art_small.jpg");	
	        	}
	        });
	    </script>
	    <?php
			get_template_part( 'template-parts/google', 'tag' );
	    ?>
		
	</body>
</html>