<?php
/*
@description: Version 2 - Search Page | Desktop
*/

/*$str_class_timing_options = '';
if ( isset( $class_timing_data ) ) {
	foreach ( $class_timing_data as $class_timing ) {
		$str_class_timing_options = '<label><input type="checkbox" class="chk_filter_timing" value="' . $class_timing->class_timing_id . '">' . $class_timing->class_timing . '</label>';
	}
}
*/

$str_class_type_options = '';
if ( isset( $class_type_data ) ) {
	foreach ( $class_type_data as $class_type ) {
		$str_class_type_options .= '<label><input type="checkbox" class="chk_filter_class_type" value="' . $class_type->subject_type_id . '"> ' . $class_type->subject_type . '</label>';
	}
}

$str_academic_board_options = '';
if ( isset( $academic_board_data ) ) {
	foreach ( $academic_board_data as $academic_board ) {
		$str_academic_board_options .= '<label><input type="checkbox" class="chk_filter_academic_board" value="' . $academic_board->academic_board_id . '"> ' . $academic_board->academic_board . '</label>';
	}
}

$str_teaching_xp_options = '';
if ( isset( $teaching_xp_data ) ) {
	foreach ( $teaching_xp_data as $teaching_xp ) {
		$str_teaching_xp_options .= '<label><input type="checkbox" class="chk_filter_teaching_xp" value="' . $teaching_xp->teaching_xp_id . '"> ' . $teaching_xp->teaching_xp . '</label>';
	}
}
$str_teacher_checked = "";
$str_institute_checked = "";
if ( isset( $this->app_data['seo_role'] ) ) {
	switch ( $this->app_data['seo_role'] ) {
		case 'teacher':
			$str_teacher_checked = "checked";
			break;
		case 'institution':
			$str_institute_checked = "checked";
			break;
	}
}
$search_result_items = null;
$searched_subject_name = null;
$searched_locality_name = null;
if ( isset( $this->app_data['search_result_items'] ) ) {
	$search_result_items = $this->app_data['search_result_items'];
}
if ( isset( $this->app_data['searched_subject_name'] ) ) {
	$searched_subject_name = $this->app_data['searched_subject_name'];
}
if ( isset( $this->app_data['searched_locality_name'] ) ) {
	$searched_locality_name = $this->app_data['searched_locality_name'];
}

$search_tpl_file_content = $this->load_view( '/template/tpl-search-cards' );
$search_suggested_locality_ribbon_tpl_file_content = $this->load_view( '/template/tpl-search-suggested-locality-ribbon-card' );

$initialLoad = '';
$initialContent = '';
if ( $search_tpl_file_content && is_array( $search_result_items ) && ! empty( $search_result_items ) ) {
	require_once( PEDAGOGE_PLUGIN_DIR . "includes/third-party/Mustache/Autoloader.php" );
	Mustache_Autoloader::register();

	$searchCardMustache = new Mustache_Engine;
	$initialContent = $searchCardMustache->render( $search_tpl_file_content, $search_result_items );
	$initialLoad = 'initialLoad';
}
?>
	<main class="search_result_main desktop background-adjust non-scrollable-fixed-search-wrapper">
		<!-- scroll to top -->
		<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
		<!--/scroll to top -->

		<div class="searched_subject_name hide" data-value="<?= $searched_subject_name; ?>"></div>
		<div class="searched_locality_name hide" data-value="<?= $searched_locality_name; ?>"></div>
		<div class="hide search-page-type" data-type="search"></div>
		<?php
		require_once( locate_template( 'template-parts/ver2/search_bar.php' ) );
		?>
		<div class="">
			<div class="container-fluid col-xs-12">
				<div class="row main-wrapper">

					<!--Template for rendering the results in the page; Mustache.js library is used for rendering the results-->
					<script id="search_result_item_cards_template" class="hide" type="x-tmpl-mustache">
						<?= $search_tpl_file_content; ?>



					</script>
					<!--/Template-->
					<!--Template for rendering the suggested locality ribbon in the search page-->
					<script id="search_suggested_locality_ribbon" class="hide" type="x-tmpl-mustache">
						<?= $search_suggested_locality_ribbon_tpl_file_content; ?>


					</script>
					<!--/Template-->
					<div class="col-xs-12 col-md-3 other-filters-search-wrapper show_on_search">
						<div class="sidebar other-filters-search-card-wrapper adjustStyle">
							<!--Filter panels-->
							<form id="other-filters" class="filter-form">
								<div id="wrapper_filter_panels" class="wrapper_filter_panels" role="tablist"
								     aria-multiselectable="true">
									<!--sort_panel-->
									<div class="panel panel-inverted">
										<div class="panel-heading" role="tab"
										     id="heading_sort_panel">
											<h4 class="panel-title">
												<a class="accordion-toggle" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_sort_panel"
												   aria-expanded="true" aria-controls="content_sort_panel"> Sort by </a>
											</h4>
										</div>
										<div id="content_sort_panel" class="panel-collapse collapse panel-body in"
										     role="tabpanel"
										     aria-labelledby="heading_sort_panel">
											<div class="sort_panel_margin_fix form-group nomargin-top-10">
												<span class="text-bold">Experience</span>
												<div class="checkbox heading_sort_panel_margin_fix">
													<label class="inline"> <input type="checkbox" class="chk_sort_exp"
													                              value="1"> High </label>
													<label class="inline"> <input type="checkbox" class="chk_sort_exp"
													                              value="0"> Low </label>
												</div>

												<span class="text-bold">Review</span>
												<div class="checkbox heading_sort_panel_margin_fix">
													<label class="inline">
														<input type="checkbox" class="chk_sort_reviews"
														       value="1"> High </label> <label class="inline">
														<input type="checkbox" class="chk_sort_reviews"
														       value="0"> Low </label>
												</div>

												<span class="text-bold">Fee</span>
												<div class="checkbox heading_sort_panel_margin_fix">
													<label class="inline"> <input type="checkbox" class="chk_sort_fee"
													                              value="1"> High </label>
													<label class="inline"> <input type="checkbox" class="chk_sort_fee"
													                              value="0"> Low </label>
												</div>
											</div>
										</div>
									</div>
									<!--/sort_panel-->

									<!--teacher_location_type-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_teacher_location_type">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_teacher_location_type"
												   aria-expanded="true"
												   aria-controls="content_filter_teacher_location_type"> Place </a>
											</h4>
										</div>
										<div id="content_filter_teacher_location_type"
										     class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_teacher_location_type">
											<div class="form-group nomargin-top-10">
												<div class="checkbox">
													<label>
														<input type="checkbox" class="chk_filter_teacher_location_type"
														       value="teacher"> Teacher's </label> <label>
														<input type="checkbox" class="chk_filter_teacher_location_type"
														       value="institute"> Institute </label> <label>
														<input type="checkbox" class="chk_filter_teacher_location_type"
														       value="student"> Student's </label>
												</div>
											</div>
										</div>
									</div>
									<!--/teacher_location_type-->

									<!--teacher_search-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_teacher_search">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_teacher_search"
												   aria-expanded="true"
												   aria-controls="content_filter_teacher_search"> Teacher </a>
											</h4>
										</div>
										<div id="content_filter_teacher_search"
										     class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_teacher_search">
											<div class="form-group col-xs-12 search_teacher_wrapper">
												<select id="search_teacher" class="form-control"></select>
											</div>
										</div>
									</div>
									<!--/teacher_search-->

									<!--fees_range-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_fees_range">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_fees_range" aria-expanded="true"
												   aria-controls="content_filter_fees_range"> Fee Range </a>
											</h4>
										</div>
										<div id="content_filter_fees_range" class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_fees_range">
											<div class="contentpanel-body">
												<div class="form-group nomargin-top-10">
													<div class="checkbox">
														<label> <input type="checkbox" class="chk_filter_fees_range"
														               value="1"> ₹ 0 - ₹ 100 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="2"> ₹ 101 - ₹ 300 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="3"> ₹ 301 - ₹ 500 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="4"> ₹ 501 - ₹ 1000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="5"> ₹ 1001 - ₹ 2000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="6"> ₹ 2001 - ₹ 5000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="7"> ₹ 5001 - ₹ 10000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="8"> ₹ 10001 - ₹ 20000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="9"> ₹ 20001 - ₹ 40000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="10"> ₹ 40001 - ₹ 70000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="11"> ₹ 70001 - ₹ 100000 </label> <label>
															<input type="checkbox" class="chk_filter_fees_range"
															       value="12"> Above ₹ 100001 </label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--/fees_range-->

									<!--teaching_xp-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_teaching_xp">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_teaching_xp" aria-expanded="true"
												   aria-controls="content_filter_teaching_xp"> Experience </a>
											</h4>
										</div>
										<div id="content_filter_teaching_xp" class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_teaching_xp">
											<div class="form-group nomargin-top-10">
												<div class="checkbox">
													<?= $str_teaching_xp_options; ?>
												</div>
											</div>
										</div>
									</div>
									<!--/teaching_xp-->

									<!--class_type-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_class_type">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_class_type" aria-expanded="true"
												   aria-controls="content_filter_class_type"> Class Type </a>
											</h4>
										</div>
										<div id="content_filter_class_type" class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_class_type">

											<div class="contentpanel-body">
												<div class="form-group nomargin-top-10">
													<div class="checkbox">
														<?= $str_class_type_options; ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--/class_type-->

									<!--academic_board-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_academic_board">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_academic_board" aria-expanded="true"
												   aria-controls="content_filter_academic_board"> Academic Board </a>
											</h4>
										</div>
										<div id="content_filter_academic_board"
										     class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_academic_board">
											<div class="form-group nomargin-top-10">
												<div class="checkbox">
													<?= $str_academic_board_options; ?>
												</div>
											</div>
										</div>
									</div>
									<!--/academic_board-->

									<!--days_taught-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_days_taught">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_days_taught" aria-expanded="true"
												   aria-controls="content_filter_days_taught"> Days Taught </a>
											</h4>
										</div>
										<div id="content_filter_days_taught" class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_days_taught">
											<div class="form-group nomargin-top-10">
												<div class="checkbox">

													<label>
														<input type="checkbox" class="chk_filter_days_taught_all_day"
														       value="all_day"> All Day </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="monday"> Monday </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="Tuesday"> Tuesday </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="Wednesday"> Wednesday </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="Thursday"> Thursday </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="Friday"> Friday </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="Saturday"> Saturday </label>

													<label> <input type="checkbox" class="chk_filter_days_taught"
													               value="Sunday"> Sunday </label>
												</div>
											</div>
										</div>
									</div>
									<!--/days_taught-->
									<!--institute_teacher-->
									<div class="panel panel-default">
										<div class="panel-heading" role="tab"
										     id="heading_filter_institute_teacher">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
												   data-parent="#wrapper_filter_panels"
												   href="#content_filter_institute_teacher"
												   aria-expanded="true"
												   aria-controls="content_filter_institute_teacher"> Type of
													teaching </a>
											</h4>
										</div>
										<div id="content_filter_institute_teacher"
										     class="panel-collapse collapse panel-body"
										     role="tabpanel"
										     aria-labelledby="heading_filter_institute_teacher">
											<div class="form-group nomargin-top-10">
												<div class="checkbox">
													<label> <input type="checkbox" <?= $str_teacher_checked; ?>
													               class="chk_filter_institute_teacher"
													               value="teacher"> Teachers </label> <label>
														<input type="checkbox" <?= $str_institute_checked; ?>
														       class="chk_filter_institute_teacher"
														       value="institution"> Institution </label>
												</div>
											</div>
										</div>
									</div>
									<!--/institute_teacher-->
								</div>
							</form>
							<!--/Filter panels-->
							<div class="row button-search-card-wrapper visible-xs visible-sm">
								<div class="col-xs-12">
									<button
											class="btn valign-wrapper btn-raised btn-sm btn-danger left resetBtn noroundcorner"
											type="submit" name="action">Reset&nbsp;
										<i class="valign material-icons right">&#xE5CD;</i>
									</button>
									<button
											class="btn valign-wrapper btn-raised btn-sm btn-success right exit_search noroundcorner"
											type="submit" name="action">Go&nbsp; <i class="valign material-icons right">
											&#xE163;</i>
									</button>
								</div>
							</div>
						</div>
					</div>

					<div class="container-fluid col-xs-12 col-md-9 result-area-wrapper hide_on_search">
						<div class="">
							<div class="row">
								<div class="search_result_noitem_cards hide">
									<div class="center-block text-center ">
										<div class="row">
											<img src="<?= PEDAGOGE_ASSETS_URL; ?>/images/error.png" class="img-fluid"/>
										</div>
										<div class="row">
											<h2>Sorry! No results found</h2>
										</div>
										<div class="row margin-bottom-10">
											<a class="post_req_no_result" href="#" data-toggle="modal" data-target="#request-callback-modal"
											   data-backdrop="static" data-keyboard="false" target="_blank">Post your
												requirements here</a>
										</div>
										<div class="row margin-bottom-10">
											<a href="<?= home_url( 'trp' ) ?>" target="_blank">Are we missing any
												institute/teacher? Refer them here</a>
										</div>
									</div>
								</div>
								<div class="search_result_item_cards <?= $initialLoad; ?>"><?= $initialContent; ?></div>
							</div>
							<!--Loading spinner-->
							<div class="pageLoadingSpinner margin-top-10 margin-bottom-10"></div>
							<!--/Loading spinner-->
							<div class="search_result_item_load_more">
								<!--Load More button-->
								<div class="container-fluid col-xs-12 nomargin-top-10 load-more-wrapper">
									<div class="row">
										<div class="col-xs-12 text-center center-block">
											<a href="javascript:void(0);"
											   class="btn btn-raised btn-danger text-white btn-sm search_result_load_more_button noroundcorner hide">
												<i class="material-icons md-18 material-icons-fix-padding">
													&#xE5D5;</i>&nbsp;Load More Profiles</a>
										</div>
									</div>
								</div>
								<!--/Load More button-->
							</div>
							<div class="visible-xs visible-sm margin-bottom-100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar-fixed-bottom white filter-bottom-wrapper hide_on_search visible-xs visible-sm  padding-top-10">
			<div class="container-fluid col-xs-12">
				<div class="row">
					<div class="text-center center-block filter-wrapper valign">
						<div class="text-center col-xs-12 center-block"><a class="start_filter">Filter</a></div>
						<!--<div class="text-center col-xs-2 center-block">|</div>
						<div class="text-center col-xs-5 center-block"><a class="start_sort">Sort</a></div>-->
					</div>
				</div>
			</div>
		</div>
	</main>


<?php
get_template_part( 'template-parts/ver2/modal-req_callback_search' );
