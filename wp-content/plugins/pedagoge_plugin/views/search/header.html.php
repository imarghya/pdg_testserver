<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo $this->title; ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<?php
			if(isset($this->app_data['seo_meta'])) {
				echo '<meta name="description" content="'.$this->app_data['seo_meta'].'">';
			}
			get_template_part( 'template-parts/google_conversion_10052017');
		?>

		<?php echo $this->load_view('header_scripts'); ?>
		<meta name="author" content="pedagoge">
		<link rel="shortcut icon" href="<?php echo PEDAGOGE_PROUI_URL; ?>/img/fav_icon_logo_orb_small.png" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Hotjar Tracking Code for http://www.pedagoge.com -->
		<?php
			$server_name = $_SERVER['SERVER_NAME'];			
			switch($server_name) {
				case 'www.pedagoge.com':
					pedagoge_applog('jar loaded');
					get_template_part( 'template-parts/hot', 'jar' );
					get_template_part( 'template-parts/fb', 'pixel' );
					break;
			}
		?>
		
    </head>
    <body <?php	body_class( "body-container" );	echo pedagoge_body_tags(); ?>	>
	    <!-- Page Container -->
	    <div id="page-container">	    	
			<?php get_template_part( 'template-parts/main', 'navigation' ); ?>