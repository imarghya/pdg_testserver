<?php

//$class_timing_data
$str_class_timing_options = '';
if (isset($class_timing_data)) {
	foreach ($class_timing_data as $class_timing) {
		$str_class_timing_options .= '
                <div class="col-xs-12">
                    <label class="checkbox-inline search-checkbox-text">
                        <input type="checkbox" class="chk_class_timing_filter" value="' . $class_timing->class_timing_id . '"> ' . $class_timing->class_timing . '
                    </label>
                </div>
            ';
	}
}

$str_class_type_options = '';
if (isset($class_type_data)) {
	foreach ($class_type_data as $class_type) {
		//$str_class_type_options .= '<option value="'.$class_type->subject_type_id.'">'.$class_type->subject_type.'</option>';
		$str_class_type_options .= '
                <div class="col-xs-12">
                    <label class="checkbox-inline search-checkbox-text">
                        <input type="checkbox" class="chk_class_type_filter" value="' . $class_type->subject_type_id . '"> ' . $class_type->subject_type . '
                    </label>
                </div>
            ';
	}
}

$str_academic_board_options = '';
if (isset($academic_board_data)) {
	foreach ($academic_board_data as $academic_board) {
		$str_academic_board_options .= '
                <div class="col-xs-12">
                    <label class="checkbox-inline search-checkbox-text">
                        <input type="checkbox" class="chk_filter_academic_board" value="' . $academic_board->academic_board_id . '"> ' . $academic_board->academic_board . '
                    </label>
                </div>
            ';
	}
}

$str_teaching_xp_options = '';
if (isset($teaching_xp_data)) {
	foreach ($teaching_xp_data as $teaching_xp) {
		$str_teaching_xp_options .= '
                <div class="col-xs-12">
                    <label class="checkbox-inline search-checkbox-text">
                        <input type="checkbox" class="chk_filter_teaching_xp" value="' . $teaching_xp->teaching_xp_id . '"> ' . $teaching_xp->teaching_xp . '
                    </label>
                </div>
            ';
	}
}

$searched_subject_name = '';
if (isset($_GET['pdg_subject'])) {
	$searched_subject_name = sanitize_text_field($_GET['pdg_subject']);
}

$str_teacher_checked = "";
$str_institute_checked = "";
if(isset($this->app_data['seo_role'])) {
	switch($this->app_data['seo_role']) {
		case 'teacher':
			$str_teacher_checked = "checked";
			break;
		case 'institution':
			$str_institute_checked = "checked";
			break;  
	}
}

?>


<!--
#### ##    ## ######## ########   #######
 ##  ###   ##    ##    ##     ## ##     ##
 ##  ####  ##    ##    ##     ## ##     ##
 ##  ## ## ##    ##    ########  ##     ##
 ##  ##  ####    ##    ##   ##   ##     ##
 ##  ##   ###    ##    ##    ##  ##     ##
#### ##    ##    ##    ##     ##  #######
-->
<section
	class="site-section site-section-light site-section-top site-section-bottom themed-background-dark change-background">
	<div class="container text-center">
		<div class="row">
			<div class="col-md-12">

			</div>
		</div>
	</div>
</section>
<!-- END Intro -->

<!-- Search Results -->
<section class="site-content site-section">
	<div class="container">
		<div class="row">
			<!--
				 ######  #### ########  ######## ########     ###    ########
				##    ##  ##  ##     ## ##       ##     ##   ## ##   ##     ##
				##        ##  ##     ## ##       ##     ##  ##   ##  ##     ##
				 ######   ##  ##     ## ######   ########  ##     ## ########
					  ##  ##  ##     ## ##       ##     ## ######### ##   ##
				##    ##  ##  ##     ## ##       ##     ## ##     ## ##    ##
				 ######  #### ########  ######## ########  ##     ## ##     ##
			-->
			<!--Side for md/lg sidebar-->
			<div class="col-md-4 col-lg-3 col-sm-4 hidden-xs hidden-sm">
				<aside class="sidebar site-block">
					<!-- Refine Search -->
					<div class="sidebar-block">
						</br>
						<form action="#" method="post" class="form-horizontal" id="frm_search_sidebar">

							<h5><strong>Filters</strong>
								<button type="reset" id="cmd_reset_search" class="btn btn-default pull-right">RESET
								</button>
							</h5>

							<div class="form-group">
								<div class="col-xs-12">
									<label class="checkbox-inline search-checkbox-text">
										<input type="checkbox" id="chk_filter_teacher" value="teacher" <?php echo $str_teacher_checked;?> > Teachers
									</label>
								</div>
								<div class="col-xs-12">
									<label class="checkbox-inline search-checkbox-text">
										<input type="checkbox" id="chk_filter_institution" value="institution" <?php echo $str_institute_checked; ?> >
										Institution
									</label>
								</div>
							</div>
							<hr>

							<h5><strong>Subject</strong></h5>
							<div class="form-group push-bit">
								<div class="col-xs-12">
									<div class="input-group">
										<input type="text" id="txt_search_subject" class="form-control"
										       placeholder="Interested Subject"
										       value="<?php echo $searched_subject_name; ?>">
										<div class="input-group-btn">
											<button type="button" class="btn btn-primary"
											        id="cmd_search_by_subject_and_locality"><i class="fa fa-search"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<h5><strong>Teacher / Institution Name</strong></h5>
							<div class="form-group push-bit">
								<div class="col-xs-12">
									<div class="input-group">
										<input type="text" id="txt_tutor_insti_search_name" class="form-control"
										       placeholder="Search Tutor or Coaching name">
										<div class="input-group-btn">
											<button type="button" class="btn btn-primary"
											        id="cmd_search_institute_teacher"><i class="fa fa-search"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<h5><strong>Locality</strong>
								<button id="cmd_search_by_locality" class="btn btn-success col-md-4 pull-right">Find
								</button>
							</h5>
							<hr/>
							<div class="form-group">
								<div class="col-sm-12">
									<select id="select_search_locality" class=" multiple_select_control"
									        multiple="multiple">

									</select>
								</div>
							</div>
							<hr>

							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFour">
										<a role="button" data-toggle="collapse" data-parent="#accordion"
										   href="#collapseFour" aria-expanded="true" aria-controls="collapseTwo">
											<h5>
												<strong>Type of Location</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h5>
										</a>
									</div>

									<div id="collapseFour" class="panel-collapse collapse in" role="tabpanel"
									     aria-labelledby="headingFour">
										<div class="panel-body">
											<div class="form-group">
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_teaching_location_type"
														       value="own"> Own
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_teaching_location_type"
														       value="student"> Students
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_teaching_location_type"
														       value="institute"> Institute
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<div class="panel-group" id="accordion_teaching_xp" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_teaching_xp">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_teaching_xp" href="#div_filter_panel_teaching_xp"
										   aria-expanded="true" aria-controls="div_filter_panel_teaching_xp">
											<h5>
												<strong>Experience</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h5>
										</a>
									</div>

									<div id="div_filter_panel_teaching_xp" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_teaching_xp">
										<div class="panel-body">
											<div class="form-group">
												<?php echo $str_teaching_xp_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<div class="panel-group" id="accordion_class_type" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_class_type">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_class_type" href="#div_filter_panel_class_type"
										   aria-expanded="true" aria-controls="div_filter_panel_class_type">
											<h5>
												<strong>Class Type</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h5>
										</a>
									</div>

									<div id="div_filter_panel_class_type" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_class_type">
										<div class="panel-body" style="max-height: 250px;overflow-y: scroll;">
											<div class="form-group">
												<?php echo $str_class_type_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<div class="panel-group" id="accordion_academic_board" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_academic_board">
										<a role="button" data-toggle="collapse" class="collapsed"
										   data-parent="#accordion_academic_board"
										   href="#div_filter_panel_academic_board" aria-expanded="false"
										   aria-controls="div_filter_panel_academic_board">
											<h5>
												<strong>Academic Board</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h5>
										</a>
									</div>

									<div id="div_filter_panel_academic_board" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_academic_board">
										<div class="panel-body">
											<div class="form-group">
												<?php echo $str_academic_board_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<div class="">
								<div class="panel-group" id="accordion_days_taught" role="tablist"
								     aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="heading_collapse_days_taught">
											<a role="button" class="collapsed" data-toggle="collapse"
											   data-parent="#accordion_days_taught" href="#collapse_days_taught"
											   aria-expanded="false" aria-controls="collapse_days_taught">
												<h5>
													<strong>Days Taught</strong>
													<i class="fa fa-caret-down pull-right"></i>
												</h5>
											</a>
										</div>

										<div id="collapse_days_taught" class="panel-collapse collapse" role="tabpanel"
										     aria-labelledby="heading_collapse_days_taught">
											<div class="panel-body">
												<div class="form-group">
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" id="chk_all_day" value="all_day"> All
															Day
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="monday"> Monday
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="Tuesday"> Tuesday
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="Wednesday"> Wednesday
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="Thursday"> Thursday
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="Friday"> Friday
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="Saturday"> Saturday
														</label>
													</div>
													<div class="col-xs-12">
														<label class="checkbox-inline search-checkbox-text">
															<input type="checkbox" class="chk_days_taught"
															       value="Sunday"> Sunday
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<hr>
							</div>

							<div class="panel-group" id="accordion_timing" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_timing">
										<a role="button" data-toggle="collapse" class="collapsed"
										   data-parent="#accordion_timing" href="#collapse_timing" aria-expanded="false"
										   aria-controls="collapse_timing">
											<h5>
												<strong>Timings</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h5>
										</a>
									</div>

									<div id="collapse_timing" class="panel-collapse collapse" role="tabpanel"
									     aria-labelledby="heading_timing">
										<div class="panel-body">
											<div class="form-group">
												<?php echo $str_class_timing_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>

							<div class="panel-group" id="accordion_fees_range" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_fees_range">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_fees_range" href="#div_filter_panel_fees_range"
										   aria-expanded="false" aria-controls="div_filter_panel_fees_range">
											<h5>
												<strong>Fee Range (Except Admission)</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h5>
										</a>
									</div>

									<div id="div_filter_panel_fees_range" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_fees_range">
										<div class="panel-body">
											<div class="form-group">
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="1">
														&#x20B9; 0 - &#x20B9; 100
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="2">
														&#x20B9; 101 - &#x20B9; 300
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="3">
														&#x20B9; 301 - &#x20B9; 500
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="4">
														&#x20B9; 501 - &#x20B9; 1000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="5">
														&#x20B9; 1001 - &#x20B9; 2000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="6">
														&#x20B9; 2001 - &#x20B9; 5000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="7">
														&#x20B9; 5001 - &#x20B9; 10000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="8">
														&#x20B9; 10001 - &#x20B9; 20000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="9">
														&#x20B9; 20001 - &#x20B9; 40000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="10">
														&#x20B9; 40001 - &#x20B9; 70000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="11">
														&#x20B9; 70001 - &#x20B9; 100000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter" value="12"> Above
														&#x20B9; 100001
													</label>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
							<hr>

						</form>
					</div>
					<!-- END Refine Search -->
				</aside>
			</div>
			<!-- END md/lg sidebar -->

			<!--Side for xs/md sidebar-->
			<div class="col-md-8 col-lg-9 col-sm-12 visible-xs visible-sm center-block">
				<aside class="sidebar site-block">
					<!-- Refine Search -->
					<div class="sidebar-block collapse" id="mob-filter">
						</br>

						<form action="#" method="post" class="form-horizontal" id="frm_search_sidebar_mob">
							<div class="form-group">
								<div class="col-xs-3 text-left">
									<h6><strong>Filters</strong>
									</h6>
								</div>
								<div class="col-xs-9 text-right">
									<button type="submit"
									        class="btn btn-default btn-primary-spacing btn-success cmd_sumbit_search_mob">
										Go
									</button>
									<button type="submit"
									        class="btn btn-default btn-primary-spacing btn-info cmd_sumbit_reset_mob">
										Reset
									</button>
									<button type="submit"
									        class="btn btn-default btn-primary-spacing btn-danger mob-filter-close-btn">
										<i class="fa fa-times" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<div class="input-group">
										<label class="checkbox-inline search-checkbox-text">
											<input type="checkbox" id="chk_filter_teacher_mob" value="teacher"> Teachers
										</label>
										<label class="checkbox-inline search-checkbox-text">
											<input type="checkbox" id="chk_filter_institution_mob" value="institution">
											Institution
										</label>
									</div>
								</div>
							</div>

							<h6 class="small-heading"><strong>Subject</strong></h6>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" id="txt_search_subject_mob" class="form-control"
									       placeholder="Interested Subject"
									       value="<?= $searched_subject_name; ?>">
								</div>
							</div>

							<h6 class="small-heading"><strong>Teacher / Institution Name</strong></h6>
							<div class="form-group">
								<div class="col-xs-12">
									<input type="text" id="txt_tutor_insti_search_name_mob" class="form-control"
									       placeholder="Search Tutor or Coaching name">
								</div>
							</div>


							<h6 class="small-heading"><strong>Locality</strong>
							</h6>
							<div class="form-group">
								<div class="col-sm-12">
									<select id="select_search_locality_mob" class="multiple_select_control_mob"
									        multiple="multiple">
									</select>
								</div>
							</div>


							<div class="panel-group" id="accordion_mob" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFour_mob">
										<a role="button" data-toggle="collapse" data-parent="#accordion_mob"
										   href="#collapseFour_mob" aria-expanded="true" aria-controls="collapseTwo">
											<h6 class="small-heading">
												<strong>Type of Location</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="collapseFour_mob" class="panel-collapse collapse" role="tabpanel"
									     aria-labelledby="headingFour_mob">
										<div class="panel-body">
											<div class="form-group">
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_teaching_location_type_mob"
														       value="own"> Own
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_teaching_location_type_mob"
														       value="student"> Students
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_teaching_location_type_mob"
														       value="institute"> Institute
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-group" id="accordion_teaching_xp_mob" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_teaching_xp_mob">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_teaching_xp_mob"
										   href="#div_filter_panel_teaching_xp_mob"
										   aria-expanded="true" aria-controls="div_filter_panel_teaching_xp_mob">
											<h6 class="small-heading">
												<strong>Experience</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="div_filter_panel_teaching_xp_mob" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_teaching_xp_mob">
										<div class="panel-body">
											<div class="form-group">
												<?= $str_teaching_xp_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-group" id="accordion_class_type_mob" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_class_type_mob">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_class_type_mob"
										   href="#div_filter_panel_class_type_mob"
										   aria-expanded="true" aria-controls="div_filter_panel_class_type_mob">
											<h6 class="small-heading">
												<strong>Class Type</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="div_filter_panel_class_type_mob" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_class_type_mob">
										<div class="panel-body" style="max-height: 250px;overflow-y: scroll;">
											<div class="form-group">
												<?php echo $str_class_type_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="panel-group" id="accordion_academic_board_mob" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_academic_board_mob">
										<a role="button" data-toggle="collapse" class="collapsed"
										   data-parent="#accordion_academic_board_mob"
										   href="#div_filter_panel_academic_board_mob" aria-expanded="false"
										   aria-controls="div_filter_panel_academic_board_mob">
											<h6 class="small-heading">
												<strong>Academic Board</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="div_filter_panel_academic_board_mob" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_academic_board_mob">
										<div class="panel-body">
											<div class="form-group">
												<?php echo $str_academic_board_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-group" id="accordion_days_taught_mob" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_collapse_days_taught_mob">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_days_taught_mob" href="#collapse_days_taught_mob"
										   aria-expanded="false" aria-controls="collapse_days_taught_mob">
											<h6 class="small-heading">
												<strong>Days Taught</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="collapse_days_taught_mob" class="panel-collapse collapse"
									     role="tabpanel"
									     aria-labelledby="heading_collapse_days_taught_mob">
										<div class="panel-body">
											<div class="form-group">
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" id="chk_all_day_mob" value="all_day"> All
														Day
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="monday"> Monday
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="Tuesday"> Tuesday
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="Wednesday"> Wednesday
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="Thursday"> Thursday
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="Friday"> Friday
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="Saturday"> Saturday
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_days_taught_mob"
														       value="Sunday"> Sunday
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-group" id="accordion_timing_mob" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_timing_mob">
										<a role="button" data-toggle="collapse" class="collapsed"
										   data-parent="#accordion_timing_mob" href="#collapse_timing_mob"
										   aria-expanded="false"
										   aria-controls="collapse_timing_mob">
											<h6 class="small-heading">
												<strong>Timings</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="collapse_timing_mob" class="panel-collapse collapse" role="tabpanel"
									     aria-labelledby="heading_timing_mob">
										<div class="panel-body">
											<div class="form-group">
												<?php echo $str_class_timing_options; ?>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="panel-group" id="accordion_fees_range_mob" role="tablist"
							     aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="heading_fees_range_mob">
										<a role="button" class="collapsed" data-toggle="collapse"
										   data-parent="#accordion_fees_range_mob"
										   href="#div_filter_panel_fees_range_mob"
										   aria-expanded="false" aria-controls="div_filter_panel_fees_range_mob">
											<h6 class="small-heading">
												<strong>Fee Range (Except Admission)</strong>
												<i class="fa fa-caret-down pull-right"></i>
											</h6>
										</a>
									</div>

									<div id="div_filter_panel_fees_range_mob" class="panel-collapse collapse"
									     role="tabpanel" aria-labelledby="heading_fees_range_mob">
										<div class="panel-body">
											<div class="form-group">
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="1">
														&#x20B9; 0 - &#x20B9; 100
													</label>
												</div>
												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="2">
														&#x20B9; 101 - &#x20B9; 300
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="3">
														&#x20B9; 301 - &#x20B9; 500
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="4">
														&#x20B9; 501 - &#x20B9; 1000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="5">
														&#x20B9; 1001 - &#x20B9; 2000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="6">
														&#x20B9; 2001 - &#x20B9; 5000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="7">
														&#x20B9; 5001 - &#x20B9; 10000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="8">
														&#x20B9; 10001 - &#x20B9; 20000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="9">
														&#x20B9; 20001 - &#x20B9; 40000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="10">
														&#x20B9; 40001 - &#x20B9; 70000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="11">
														&#x20B9; 70001 - &#x20B9; 100000
													</label>
												</div>

												<div class="col-xs-12">
													<label class="checkbox-inline search-checkbox-text">
														<input type="checkbox" class="chk_fees_filter_mob" value="12">
														Above
														&#x20B9; 100001
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12 text-right">
									<button type="submit"
									        class="btn btn-default btn-primary-spacing btn-success cmd_sumbit_search_mob">
										Go
									</button>
									<button type="submit"
									        class="btn btn-default btn-primary-spacing btn-info cmd_sumbit_reset_mob">
										Reset
									</button>
								</div>
							</div>
						</form>
					</div>
					<!-- END Refine Search -->
				</aside>
			</div>
			<!-- END xs/md sidebar -->

			<!--
				########  ########   #######  ########  ##     ##  ######  ########
				##     ## ##     ## ##     ## ##     ## ##     ## ##    ##    ##
				##     ## ##     ## ##     ## ##     ## ##     ## ##          ##
				########  ########  ##     ## ##     ## ##     ## ##          ##
				##        ##   ##   ##     ## ##     ## ##     ## ##          ##
				##        ##    ##  ##     ## ##     ## ##     ## ##    ##    ##
				##        ##     ##  #######  ########   #######   ######     ##
			 -->
			<div class="col-md-8 col-lg-9">
				<div class="form-inline push-bit clearfix">
					<div class="row hidden_item">
						<div class="col-md-4">
							<select id="results-sort" name="results-sort" class="form-control select_control" size="1">
								<option value="0" selected>SORT BY</option>
								<option value="low_to_high" class="option_bs">(Price) Low to High</option>
								<option value="high_to_low" class="option_bs">(Price) High to Low</option>
								<option value="year_of_mfg_high" class="option_bs">Coaching Experience High To Low
								</option>
								<option value="year_of_mfg_low" class="option_bs">Coaching Experience Low To High
								</option>
								<option value="rating">Rating</option>
							</select>
						</div>
						<div class="col-md-6"></div>
						<div class="col-md-2">
							<select id="results-show" name="results-show" class="form-control pull-right select_control"
							        size="1">
								<option value="0" selected>SHOW</option>
								<option value="25">25</option>
								<option value="50">50</option>
								<option value="75">75</option>
								<option value="100">100</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row store-items" id="search_result_area">
					<div class="text-center"><?php echo $str_search_result; ?></div>
				</div> <!-- storeItem rows ends here -->

				<div class="row">
					<div class="col-xs-12 text-center">
						<ul class="pagination" id="search_result_pagination">
							<li class="disabled" id="li_pagination_first"><a href="javascript:void(0)"><i
										class="fa fa-angle-double-left"></i></a></li>
							<li class="disabled" id="li_pagination_left"><a href="javascript:void(0)"><i
										class="fa fa-angle-left"></i></a></li>
							<?php
							$initial_page_count = count($str_pagination, 0);
							if ($initial_page_count > 10) {
								for ($i = 0; $i < 10; $i ++) {
									echo $str_pagination[$i];
								}
							}
							else {
								foreach ($str_pagination as $page) {
									echo $page;
								}
							}
							?>
							<li id="li_pagination_right"><a href="javascript:void(0)"><i class="fa fa-angle-right"></i></a>
							</li>
							<!-- <li id="li_pagination_last"><a href="javascript:void(0)"><i class="fa fa-angle-double-right"></i></a></li> -->
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						
						<!-- teacher's Rating Modal -->
						<div class="modal fade" id="rating_modal_teacher" tabindex="-1" role="dialog" aria-labelledby="rating_modal_label_teacher">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="rating_modal_label_teacher">Rating and Reviews</h4>
									</div>
									<div class="modal-body" id="review_modal_body_teacher">
										<div class="row">
											<div class="col-md-4 text-right"><br />Give your overall ratings - </div>
		                                    <div class="col-md-8">
		                                    	<input class="rating rating-loading pdg_overall_rating_teacher" name="pdg_overall_rating_teacher" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
		                                    </div>
		                                </div>
		                                
		                                <div class="panel-group" role="tablist">
					                        <div class="panel panel-default">
					                            <div class="panel-heading" role="tab" id="collapse_list_group_heading_ratings_teacher">
					                                <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_list_group_ratings_teacher" aria-expanded="false" aria-controls="collapse_list_group_ratings_teacher"> Click here for Detailed Ratings </a> </h4> 
												</div>
					                            <div id="collapse_list_group_ratings_teacher" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse_list_group_heading_ratings_teacher">
					                                <ul class="list-group">
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Cooperative<h6></span>(Is this teacher helpful when needed?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_teacher" name="star1" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Clarity</span><h6>(Is this teacher clear about the class requirements and subject matter?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_teacher" name="star2" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Composure</span><h6>(Is the teacher able to handle the class well?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_teacher" name="star3" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Coolness</span><h6>(Do you look forward to the next class?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_teacher col-md-12" name="star4" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                </ul>			                                
					                            </div>
					                        </div>
					                    </div>
		                                
										<hr />
										<div class="row">
											<div class="col-md-12">Please write your review!</div>
											<div class="col-md-12 form-group">
												<textarea rows="4" class="form-control" id="txt_review_field_teacher"></textarea>
											</div>
											<div class="col-md-12 col-md-offset-8">
												<label><input type="checkbox" name="chk_anonymous_review_teacher" id="chk_anonymous_review_teacher"> Review anonymously</label>
											</div>
										</div>								
										<div class="row">
											<div class="col-md-12" id="div_rating_result_area_teacher"></div>
											<div class="col-md-12 center_content hidden_item" id="div_rating_loader_teacher">
												<img src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" class="center_content" alt="" />
											</div>
											<input type="hidden" id="hidden_teacher_modal_current_user_id" />
											<input type="hidden" id="hidden_teacher_modal_teacher_user_id" />
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary" id="cmd_save_review_teacher" data-loading-text="Saving review...">Save</button>
									</div>
								</div>
							</div>
						</div>
						<!-- End of teacher's Rating Modal -->

						<!-- institute's Rating Modal -->
						<div class="modal fade" id="rating_modal_institute" tabindex="-1" role="dialog" aria-labelledby="rating_modal_label_institute">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="rating_modal_label_institute">Rating and Reviews</h4>
									</div>
									<div class="modal-body" id="review_modal_body_institute">
										<div class="row">
											<div class="col-md-4 text-right"><br />Give your overall ratings - </div>
		                                    <div class="col-md-8">
		                                    	<input class="rating rating-loading pdg_overall_rating_institute" name="pdg_overall_rating_institute" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
		                                    </div>
		                                </div>
		                                
		                                <div class="panel-group" role="tablist">
					                        <div class="panel panel-default">
					                            <div class="panel-heading" role="tab" id="collapse_list_group_heading_ratings_institute">
					                                <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" href="#collapse_list_group_ratings_institute" aria-expanded="false" aria-controls="collapse_list_group_ratings_institute"> Click here for Detailed Ratings </a> </h4> 
												</div>
					                            <div id="collapse_list_group_ratings_institute" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapse_list_group_heading_ratings_institute">
					                                <ul class="list-group">
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Quality<h6></span>(Do you clearly understand the teacher?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_institute" name="star1" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Cooperativeness</span><h6>(Is the institute helpful and flexible when needed?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_institute" name="star2" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Infrastructure</span><h6>(Consider factors such as coaching area, washroom, parking space, equipments used etc)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_institute" name="star3" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                    <li class="list-group-item">
					                                    	<div class="row">
							                                    <div class="col-md-12"><span id="">Delight</span><h6>(Would you like to go back to the class again?)*</h6></div>
							                                    <div class="col-md-12">
							                                        <input class="rating rating-loading pdg_star_rating_institute col-md-12" name="star4" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
							                                    </div>
							                                </div>
					                                    </li>
					                                </ul>			                                
					                            </div>
					                        </div>
					                    </div>
		                                
										<hr />
										<div class="row">
											<div class="col-md-12">Please write your review!</div>
											<div class="col-md-12 form-group">
												<textarea rows="4" class="form-control" id="txt_review_field_institute"></textarea>
											</div>
											<div class="col-md-12 col-md-offset-8">
												<label><input type="checkbox" name="chk_anonymous_review_institute" id="chk_anonymous_review_institute"> Review anonymously</label>
											</div>
										</div>								
										<div class="row">
											<div class="col-md-12" id="div_rating_result_area_institute"></div>
											<div class="col-md-12 center_content hidden_item" id="div_rating_loader_institute">
												<img src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" class="center_content" alt="" />
											</div>
											<input type="hidden" id="hidden_institute_modal_current_user_id" />
											<input type="hidden" id="hidden_institute_modal_institute_user_id" />
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary" id="cmd_save_review_institute" data-loading-text="Saving review...">Save</button>
									</div>
								</div>
							</div>
						</div>
						<!-- End of institute's Rating Modal -->

					</div>
				</div>
			</div>
			<!-- END Products -->
		</div>
	</div>

	<div class="modal fade" id="show-interest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

		<div class="modal-dialog" role="document">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						<center>Drop us a message</center>
					</h4>

					<center>
						<span class="loader_loading hidden_item">Loading Please wait</span>
						<img class="img_locality_loader hidden_item"
						     src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif"
						     alt="Loading...Please Wait"/>
					</center>

				</div>
				<div class="modal-body" id="show_interest_modal_body">
					
					<div class="row">
						<div class="col-md-12">
							<center><label><b>Post Your Requirements Here.</b></label></center>
						</div>
					</div>
					<input type="hidden" id="txt_modal_user_id" value="<?php echo $teacher_user_id; ?>"/>
					<label>Name:</label>*
					<input class="form-control" id="txt_modal_user_name" placeholder="Kindly provide your Name"/>
					<label>Phone No.:</label>*
					<input class="form-control positive" id="txt_modal_phone"
					       placeholder="Kindly provide your Phone no." maxlength="10"/>
					<label>Locality:</label>*
					<input class="form-control" id="txt_modal_locality" placeholder="Kindly provide your Locality"/>
					<label>Special Request:</label>
					<input class="form-control" id="txt_modal_request"
					       placeholder="Please let us know if you have any special request"/>

					<div class="modal-footer">
						<button type="button" class="btn btn-default cmd_close_interest" data-dismiss="modal">Close
						</button>
						<button type="button" class="btn btn-primary cmd_show_interest" id="submit">Send</button>
					</div>
					<div id="show_interest_error"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- review if not login modal -->

                <div class="modal fade " id="no-login-review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content ">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><center>Want to Review/Recommend this teacher? </center></h4>
                            
                            </div>

                            <div class="modal-body">
                                 <div class ="row">
                                        <div class = "col-md-12">
                                            <center><label>You need to login first as a Guardian/Student.</label>
                                            </center>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class = "col-md-12">
                                            <center>
                                            <label>For Login,</label>
                                                <a href=<?php $next_URL = $_SERVER['REQUEST_URI']; echo home_url('/login?nexturl='.$next_URL); ?>>        
                                                    Click Here
                                                 </a>
                                             </center>
                                        </div>
                                    </div><br>
                                    <div class ="row">
                                        <div class="col-md-12">
                                            <center>
                                            <label>For SignUp,</label>
                                                <a href=<?php echo home_url('/signup'); ?>>        
                                                    Click Here
                                                 </a>
                                             </center>
                                        </div>
                                    </div>     
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                                <div id="show_interest_error_review"></div>
                            </div>
                        </div>
                    </div>
                    

                </div>


                <!--end of review if not login modal -->
	
</section>
<!-- END Search Results -->