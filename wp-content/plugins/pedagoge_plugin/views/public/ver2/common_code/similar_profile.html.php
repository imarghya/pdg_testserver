<section class="section-similar-profile slider-strips">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-10 center-block">
				<h5 class="blue-shade-color text-bold">Similar Profiles: </h5>
			</div>
		</div>
		<div class="row">
			<!--Loading spinner-->
			<div class="similar-profile-spinner margin-top-10 margin-bottom-10"></div>
			<!--/Loading spinner-->
			<div class="slideOuter center-block no-select hide">
				<!--Kick start the slides-->
				<div class="kick-start-sliding hide"></div>
				<!--/Kick start the slides-->
				<!--Slides start-->
				<div class="arrowX left"><i class="material-icons">&#xE314;</i></div>
				<div class="col-xs-12 slideWrapper">
					<div class="slideInner no-select"></div>
				</div>
				<!--Slides end-->
				<div class="arrowX right"><i class="material-icons">&#xE315;</i></div>
			</div>
		</div>
	</div>
</section>