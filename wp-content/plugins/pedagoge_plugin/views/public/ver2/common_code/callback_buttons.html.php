<section class="section-buttons">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-12 buttons-wrapper">
				<div class="buttons-sub-wrapper">
					<div class="col-xs-6 center-block text-center">
						<?= $callback_btn; ?>
					</div>
					<div class="col-xs-6 center-block text-center">
						<?= $take_a_demo_btn; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>