<hr />
<section class="section-reviews margin-bottom-20">
	<div class="container-fluid reviews-recommendations-wrapper">
		
		<div class="row">
			
			<div class="col-xs-12 col-sm-12 col-md-7 center-block valign-wrapper min-height-fix">				
				<div class="col-xs-12 col-sm-12 col-md-7 profile_item_rating_wrapper">
					<div class="profile_item_rating desktop valign-wrapper left reviews-wrapper">
						<div class="star_wrapper reviews_item_rating">
							<span class="blue-shade-color text-bold">Overall Reviews (<?= $rating_count; ?>): </span>
							<?= $rating_stars_render; ?>
						</div>
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-5">
					<span class="blue-shade-color text-bold total_recommendation_count">Total Recommendations : <?= $recommendation_count; ?></span>
				</div>			
			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-5 buttons-wrapper">
				<div class="buttons-sub-wrapper">
					<div class="col-xs-6 center-block text-center">
						<?= $review_btn; ?>
					</div>
					<div class="col-xs-6 center-block text-center">
						<?= $str_recommendation_button; ?>
					</div>
				</div>
			</div>
			
		</div>
		
		<hr />
		<div class="row">
			<div class="col-xs-12 col-md-12 reviews-wrapper">
				<div class="reviews-sub-wrapper">
					<div class="container-fluid">
						<!--reviews-items-->
						<div class="row">
							<?= $str_recommendations_html; ?>
						</div>
						<!--/reviews-items-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>