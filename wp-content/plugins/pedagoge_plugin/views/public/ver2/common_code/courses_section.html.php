<section class="section-courses margin-top-10">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-10 center-block">
				<h5 class="blue-shade-color text-bold"> Courses:</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 courses-panel-wrapper">
				<div class="courses-panel-sub-wrapper">
					<!--Courses_Subject-->
					<form id="courses-panel-form" class="courses-panel">
						<div id="courses-panel-encloser" class="courses-panel-encloser" role="tablist"
						     aria-multiselectable="true">
							<div class="panel-group" id="courses-panel-accordion">
								<?= $str_course_details; ?>
							</div>
						</div>
					</form>
					<!--/Courses_Subject-->
				</div>
			</div>
		</div>
	</div>
</section>