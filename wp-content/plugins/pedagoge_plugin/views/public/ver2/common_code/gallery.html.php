<section class="section-gallery <?= $no_gallery ? 'hide' : ''; ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-md-10 center-block">
				<h5 class="blue-shade-color text-bold">Gallery: </h5>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 gallery-wrapper">
				<div class="gallery-sub-wrapper">

					<!--gallery-items-->
					<div class="row">
						<?= $str_slide_counter; ?>
					</div>
					<!--/gallery-items-->

				</div>
			</div>
		</div>
	</div>
</section>