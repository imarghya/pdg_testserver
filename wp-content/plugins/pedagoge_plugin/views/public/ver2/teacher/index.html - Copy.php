<?php include_once('teacher_profile_view_code_processing.html.php'); ?>
<main class="padding-fix center-block">
	<!-- scroll to top -->
	<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
	<!--/scroll to top -->
	<!-- scroll to bottom -->
	<a href="javascript:void(0);" class="scroll-to-bottom"><i class="material-icons">&#xE313;</i></a>
	<!--/scroll to bottom -->
	<!--Template for rendering the results in the page; Mustache.js library is used for rendering the results-->
	<script id="profile_suggestion_item_cards_template" class="hide" type="x-tmpl-mustache">
		<?= $profile_suggestion_tpl_file_content; ?>
	</script>
	<!--/Template-->
	<div class="hide" id="locality_id_data" data-value="<?= $locality_id_data; ?>"></div>
	<div class="hide" id="subjects_names_data" data-value="<?= $subjects_names_data; ?>"></div>
	<div class="search_result_main desktop background-adjust non-scrollable-fixed-search-wrapper">
		<div class="hide search-page-type" data-type="profile"></div>
		<?php require_once( locate_template( 'template-parts/ver2/search_bar.php' ) ); ?>
	</div>
	<!--section-info-->
	<section class="section-info">
		<div class="container-fluid">
			<div class="row scroll_to_here_on_start">
				<div class="col-xs-12 visible-xs center-block text-center heading-padding-fix">
					<h1 class="main-heading text-bold item-profile-name"><?= $teacher_name; ?></h1>
					<h5 class="inline text-bold blue-shade-color">
						<?= $teacher_nick_name; ?>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 col-sm-3 col-md-3 no-padding profile-info-wrapper">
					<div class="col-xs-12">
						<div class="profile_data_img_thumbnail left">
							<img alt="<?= $teacher_name; ?>" class="profile_pic <?= $teacher_profile_image_orientation; ?>" title="<?= $teacher_name; ?>" src="<?= $teacher_profile_image_url; ?>">
						</div>
					</div>
					<div class="col-xs-12">
						<div class="profile_item_rating_wrapper">
							<div class="profile_item_rating desktop valign-wrapper left">
								<div class="star_wrapper">
									<?= $rating_stars_render; ?>
								</div>
								<div class="star_count_wrapper">
									&nbsp;(<span class="text-bold stars_total_rating_count"><?= $rating_count; ?></span>)
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 total_recommendation_count_wrapper center-block text-center text-bold white-text blue-color blue-shade">
						<span class="hidden-xs">Likes </span><i class="material-icons">
							&#xE8DC;</i>&nbsp;<span class="total_recommendation_count"><?= $recommendation_count; ?></span>
					</div>
					<div class="col-xs-12 visible-xs item-padding-fix">
						<div class="inline text-bold item-heading blue-shade-color item-text">
							<?= $teacher_profile_heading; ?>
						</div>
					</div>
					<div class="col-xs-12 item-padding-fix">
						<div class="inline text-bold item-exp black-shade-color item-text">
							Exp:
						</div>
						<div class="inline text-bold item-exp blue-shade-color item-text">
							<?= $teacher_teaching_xp; ?>
						</div>
					</div>
					<div class="col-xs-12 item-padding-fix">
						<div class="inline text-bold item-gender black-shade-color item-text">
							Gender:
						</div>
						<div class="inline text-bold item-gender blue-shade-color item-text"><?= ucfirst( $teacher_gender ); ?>
						</div>
					</div>
					<div class="col-xs-12 item-padding-fix">
						<?= $personal_information_profile_btn; ?>
						<?= $str_show_notes_button; ?>
						<?= $str_show_internal_rating_button; ?>
					</div>
				</div>
				<div class="col-xs-6 col-sm-9 col-md-9 no-padding">
					<div class="container-fluid">
						<div class="col-xs-12 col-sm-7 hidden-xs text-left nomargin-top-20">
							<h1 class="main-heading text-bold"><?= $teacher_name; ?></h1>
							<h5 class="inline text-bold blue-shade-color">
								<?= $teacher_nick_name; ?>
							</h5>
							<div class="hidden-xs text-bold item-heading blue-shade-color item-text">
								<?= $teacher_profile_heading; ?>
							</div>
						</div>
						<!--header button arrangement for tablet, md, lg-->
						<div class="hidden-xs col-sm-5 no-padding">
							<div class="col-xs-12 text-right no-padding">
								<?= $share_btn; ?>
							</div>
							<div class="col-xs-12 text-right no-padding">
								<?= $callback_btn; ?>
							</div>
						</div>
						<!--/header button arrangement for tablet, md, lg-->
						<div class="hidden-xs col-sm-12 no-padding">
							<div class="col-xs-4 text-left pull-left">
								<?= $review_btn; ?>
							</div>
							<div class="col-xs-4 text-center center-block">
								<?= $str_recommendation_button; ?>
							</div>
							<div class="col-xs-4 text-right pull-right no-padding">
								<?= $take_a_demo_btn; ?>
							</div>
						</div>
						<div class="visible-xs">
							<div class="col-xs-12 col-sm-4 col-md-4 center-block text-right">
								<?= $callback_btn; ?>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 center-block text-right">
								<?= $take_a_demo_btn; ?>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 center-block text-right">
								<?= $share_btn; ?>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 center-block text-right">
								<?= $str_recommendation_button; ?>
							</div>
							<div class="hidden-xs col-sm-4 col-md-4 center-block text-right">
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 center-block text-right">
								<?= $review_btn; ?>
							</div>
						</div>

						<!--section-about-->
						<div class="hidden-xs div-about">
							<div class="col-xs-12 <?= $teacher_qualification == "" ? 'hide' : '' ?>">
								<h5 class="item-title text-bold blue-shade-color">Education Qualification:</h5>
							</div>
							<div class="col-xs-12 <?= $teacher_qualification == "" ? 'hide' : '' ?>">
								<div class="expandable-text">
									<p class="item-body black-shade-color"><?= $teacher_qualification; ?></p>
									<p class="item-shade"></p>
								</div>
								<p class="item-readmore text-right">
									<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler more">
										Read More </a>
									<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler less hide">
										Read Less </a>
								</p>
							</div>
							<div class="col-xs-12 <?= $teacher_about_coaching == "" ? 'hide' : '' ?>">
								<h5 class="item-title text-bold blue-shade-color"> About:</h5>
							</div>
							<div class="col-xs-12 <?= $teacher_about_coaching == "" ? 'hide' : '' ?>">
								<div class="expandable-text">
									<p class="item-body black-shade-color"><?= $teacher_about_coaching; ?></p>
									<p class="item-shade"></p>
								</div>
								<p class="item-readmore text-right">
									<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler more">
										Read More </a>
									<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler less hide">
										Read Less </a>
								</p>
							</div>
						</div>
						<!--section-about-->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/section-info-->
	<!--section-about-->
	<section class="section-about visible-xs">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 <?= $teacher_qualification == "" ? 'hide' : '' ?>">
					<h5 class="item-title text-bold blue-shade-color">Education Qualification:</h5>
				</div>
				<div class="col-xs-12 <?= $teacher_qualification == "" ? 'hide' : '' ?>">
					<div class="expandable-text">
						<p class="item-body black-shade-color"><?= $teacher_qualification; ?></p>
						<p class="item-shade"></p>
					</div>
					<p class="item-readmore text-right">
						<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler more">
							Read More </a>
						<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler less hide">
							Read Less </a>
					</p>
				</div>

				<div class="col-xs-12 <?= $teacher_about_coaching == "" ? 'hide' : '' ?>">
					<h5 class="item-title text-bold blue-shade-color"> About:</h5>
				</div>
				<div class="col-xs-12 <?= $teacher_about_coaching == "" ? 'hide' : '' ?>">
					<div class="expandable-text">
						<p class="item-body black-shade-color"><?= $teacher_about_coaching; ?></p>
						<p class="item-shade"></p>
					</div>
					<p class="item-readmore text-right">
						<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler more">
							Read More </a>
						<a href="#" class="btn btn-sm btn-primary nomargin blue-shade-color text-bold toggler less hide">
							Read Less </a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<!--/section-about-->

	<!--section-courses-->
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/public/ver2/common_code/courses_section.html.php'); ?>
	<!--/section-courses-->

	<!--section-additional_details-->
	<section class="section-additional_details courses-panel-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-10 center-block">
					<h5 class="blue-shade-color text-bold">Additional Details: </h5>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-12 additional_details-wrapper">
					<div class="additional_details-sub-wrapper">
						<!--additional_details-items-->
						<div class="additional_details_item_block">
							<div class="inline text-bold item-desc black-shade-color">Achievement</div>
							:
							<div class="inline item blue-shade-color"><?= $teacher_achievement; ?>
							</div>
						</div>
						<div class="additional_details_item_block">
							<div class="inline text-bold item-desc black-shade-color">Present place of work
							</div>
							:
							<div class="inline item blue-shade-color"><?= $teacher_present_place_of_work; ?>
							</div>
						</div>
						<div class="additional_details_item_block">
							<div class="inline text-bold item-desc black-shade-color">Schools Taught
							</div>
							:
							<div class="inline item blue-shade-color">
								<?= $str_schools_teaches; ?>
							</div>
						</div>
						<div class="additional_details_item_block">
							<div class="inline text-bold item-desc black-shade-color">Disclaimer
							</div>
							:
							<div class="inline item blue-shade-color">All details shown in the profile are provided
								by the owner of the profile (teacher/institute) and Pedagoge is not responsible for
								any misleading information. Fees are subjected to a teacher's discretion and can
								vary from that mentioned on the website.
							</div>
						</div>
						
						<!--/additional_details-items-->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/section-additional_details-->

	<!--section-reviews-->
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/public/ver2/common_code/reviews_section.html.php'); ?>
	<!--/section-reviews-->

	<!--section-buttons-->
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/public/ver2/common_code/callback_buttons.html.php'); ?>
	<!--/section-buttons-->

	<!--section-gallery-->
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/public/ver2/common_code/gallery.html.php'); ?>
	<!--/section-gallery-->

	<!--section-similar-profile-->
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/public/ver2/common_code/similar_profile.html.php'); ?>
	<!--/section-similar-profile-->
</main>

<?php

include_once(PEDAGOGE_PLUGIN_DIR.'views/modals/modal_teachers_notes.html.php');
include_once(PEDAGOGE_PLUGIN_DIR.'views/modals/modal_teachers_internal_rating.html.php');
include_once('modal_teacher_personal_info.html.php');
$var_passing_modal['is_private_profile'] = $is_private_profile;
$var_passing_modal['review_allowed'] = $review_allowed;
$var_passing_modal['allow_demo_modal'] = $allow_demo_modal;
echo $this->load_view( 'modals/modal-req_callback_profile', $var_passing_modal );
