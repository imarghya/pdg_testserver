 <footer class="pedagoge_footer text-center">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 pedagoge_footer_link">
                  <ul>
                     <li><a href="<?php echo get_site_url(); ?>/terms">Terms & Conditions</a></li>
                     <li><a href="<?php echo get_site_url(); ?>/privacy-policy">Privacy Policy</a></li>
                     <li><a href="<?php echo get_site_url(); ?>/">Site map</a></li>
                     <li><a href="<?php echo get_site_url(); ?>/about-us">About Us</a></li>
                     <li><a href="<?php echo get_site_url(); ?>/business">Business</a></li>
                     <li><a href="<?php echo get_site_url(); ?>/contact-us">Contact Us</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Powered by</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l1.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Selected for</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l2.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Selected for</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l3.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Backed by</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l4.png" alt="LOGO"></a>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social">
                  <h3>Follow Us</h3>
                  <ul>
                     <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social pull-right">
                  <h3>Subscribe to our Newsletter</h3>
                  <div class="input-group">
                     <input type="email" class="form-control" placeholder="Enter your email address">
                     <span class="input-group-btn">
                     <button class="btn btn-secondary" type="button">Go</button>
                     </span>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social">
                  <h3 class="hidden-xs">&nbsp;</h3>
                  <p>&copy; Pedagoge 2017</p>
               </div>
            </div>
         </div>
      </footer>


<div class="modal fade" id="phone_verifi_modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
       <div class="modal-content">
         
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h5 class="modal-title phone_veri" id="avatar-modal-label">Phone Verification</h5>
             </div>
              <form class="avatar-form" id="otp_form_submit" enctype="multipart/form-data" method="post">
              <div class="modal-body">
				<span class="otp_heading">OTP has been sent to this no. : <span id="mob_no"></span></span>
                <div class="form-group margin_top_10">
                    <label for="recipient-name" class="form-control-label">Enter OTP Code:</label>
                    <input type="text" required class="form-control" value="" id="otp_code">
                </div>
             </div>
	      <div class="modal-footer">
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <button type="button" id="resend_otp" class="btn btn-info">Resend OTP</button>
                <button type="submit" id="st_add_loc_btn" class="btn btn-primary">Submit</button>
              </div>
             </form>
             
          
       </div>
    </div>
</div>
<!--<script src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery-2.1.1.min.js"></script>-->
<!--<script src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/js/ver2_pluginshome.min.js"></script>
<script src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/js/global-all.js"></script>
<script src="<?php echo BOWER_ROOT_URL; ?>/notifyjs/dist/notify.js"></script>
<script src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL."/js/modals_callback.js"; ?>"></script>
<script src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL."/js/profile.js"; ?>"></script>
<script src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL."/js/internal_rating.js"; ?>"></script>
<script src="<?php echo PEDAGOGE_THEME_URL."/assets/js/validationEngine/jquery.validationEngine.js"; ?>"></script>
<script src="<?php echo PEDAGOGE_THEME_URL."/assets/js/validationEngine/jquery.validationEngine-en.js"; ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/js/alertify.min.js"></script>

<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.meanmenu.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/owl.carousel.min.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.slimscroll.min.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/ekko-lightbox.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/base.js"></script>
<!--<script src="<?php //echo PEDAGOGE_ASSETS_URL."/js/social_login.js"; ?>"></script>-->


<script src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585b9d7903b68dc8"></script>
  <script>
    $(document).ready(function(){
        $("#profile_takeademo_callback").validationEngine();
	$("#profile_callback").validationEngine();
	$("#profile_review_callback").validationEngine();
       });
    </script>
<script>
	/* Mean Menu */
	jQuery(document).ready(function () {
		jQuery('.site-navigation').meanmenu();
	});

$(document).ready(function(){
$('#modal_teachers_internal_rating').on('shown.bs.modal', function(){
//$("#cmd_reload_teachers_internal_rating").trigger("click");
load_rating();
});

//Rating validation-------------------------------------------
//------------------------------------------------------------
//For Recomendation--------------------------------------------
$(document).on("click",".write_recomended",function(){
var result = confirm("Are you sure?");
    if (result) {
	var idd=$(".write_recomended").attr("idd");
	var teacher_id=$(this).attr("data-teacher_user_id");
	var student_id=$(this).attr("data-current_user");
	//call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'recomended_teacher',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        teacher_id : teacher_id,
	student_id : student_id,
	idd : idd,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
	    if (idd==0) {
		$(".write_recomended").attr("idd",1);
		$(".write_recomended p").text('Unrecommend');
		$(".write_recomended img").attr('src','<?php echo PEDAGOGE_ASSETS_URL; ?>/teacher/img/un.png');
		$("#chk_recommend_teacher").attr("disabled",true);
                alertify.success("Success: Recomendation Successfully Submitted.");
	    }
	    else{
	    $(".write_recomended").attr("idd",0);
	    $(".write_recomended p").text('Recommend');
	    $("#chk_recommend_teacher").attr("disabled",false);
	    $(".write_recomended img").attr('src','<?php echo PEDAGOGE_ASSETS_URL; ?>/teacher/img/head_icon_2.png');
            alertify.success("Success: Recomendation Successfully Removed.");
	    }
	   
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------	
    }
});
//-------------------------------------------------------------
$('#review-profile-modal').on('shown.bs.modal', function() {
 var idd=$(".write_recomended").attr("idd");
   if (idd==1) {
   $("#chk_recommend_teacher").attr("disabled",true);	
   }
   else{
   $("#chk_recommend_teacher").attr("disabled",false);		
   }
})
//Email verification--------------------------------------------
$("#email_verifi").click(function(){
var email_id=$(this).attr("email-id");
//call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_teacher_email_verification',
        pedagoge_callback_class: 'ControllerTeacher',
        email_id : email_id,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            alertify.success("Success: Verification mail has been sent.");
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------	
});
//--------------------------------------------------------
//Phone verification--------------------------------------
$("#phone_verifi").click(function(){
var phone_no=$(this).attr("phone-no");
var idd=$(this).attr("idd");
$("#mob_no").text(phone_no);
if (idd==0) {	
$("#phone_verifi_modal").modal("show");
//call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_otp_code',
        pedagoge_callback_class: 'ControllerTeacher',
        phone_no : phone_no,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
	//alert(response);
            //alertify.success("Success: Verification mail has been sent.");
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------
}
});
//--------------------------------------------------------
//Resend Otp----------------------------------------------
$("#resend_otp").click(function(){
var phone_no=$("#mob_no").text();
//call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_otp_code',
        pedagoge_callback_class: 'ControllerTeacher',
        phone_no : phone_no,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
	//alert(response);
            alertify.success("Success: Another OTP has been sent.");
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------

});
//--------------------------------------------------------
//Submit otp form-----------------------------------------
$(document).on('submit', '#otp_form_submit', function(event){
event.preventDefault();
var otp_code=$("#otp_code").val();
//call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'check_otp_code',
        pedagoge_callback_class: 'ControllerTeacher',
        otp_code : otp_code,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
	    if (response=='success') {
		$("#phone_verifi_modal").modal("hide");
		$("#phone_verifi").text('Verified');
		$("#phone_verifi").attr("idd",1);
		alertify.success("Success: Phone Verification Successfully Done.");
	   }
	   else{
            alertify.error("Error: OTP Code Mismatch!!");
	   }
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------
});
//--------------------------------------------------------
});	
	
    $(function(){
	
	$('.achivment_part').slimScroll({
						height: '155px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						allowPageScroll: true,
						axis: 'y'
					});
				
		 		$('.public_review_box').slimScroll({
						height: '330px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						allowPageScroll: true,
						wheelStep: 150,
						axis: 'y'
					});
    
			   
		});
				
				$(function(){
					$('.private_review_box').slimScroll({
						height: '110px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						wheelStep: 150,
						allowPageScroll: true,
						axis: 'y'
					});
				});
                                
                                
    var owl = $('.owl1');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: false,
        center: false,
		nav: true,
		navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/myprevimage.png'>","<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/mynextimage.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  600: {
            items: 2
          },
		  730: {
            items: 3
          },
		  960: {
            items: 4
          },
		  1200: {
            items: 5
          }
        }
      })
	  
	  $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
});

    var owl = $('.owl2');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: true,
		nav: true,
		navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/myprevimage1.png'>","<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/mynextimage1.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  600: {
            items: 2
          },
		  730: {
            items: 3
          },
		  1024: {
            items: 4
          }
        }
      })
	  
   $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
});
$(document).ready(function(){
	$('#takeademo-callback-profile-modal').on('shown.bs.modal', function (e) {
	  var teacher_name=$("#te_name").text();
	  $("#callback_teachername-takeademo").val(teacher_name);
	});
	
	$('#request-callback-profile-modal').on('shown.bs.modal', function (e) {
	  var teacher_name=$("#te_name").text();
	  $("#callback_teachername").val(teacher_name);
	});
});

/* Alert Drop Down */

	function myFunction1() {
       var x = document.getElementById('myDIV1');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction3() {
       var x = document.getElementById('myDIV3');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }
</script>
<script>

$(document).ready(function ($) {
                // delegate calls to data-toggle="lightbox"
                $(document).on('click', '[data-toggle="lightbox"]:not([data-gallery="navigateTo"]):not([data-gallery="example-gallery-11"])', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function() {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
						onNavigate: function(direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                            }
						}
                    });
                });

                // disable wrapping
                $(document).on('click', '[data-toggle="lightbox"][data-gallery="example-gallery-11"]', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        wrapping: false
                    });
                });

                //Programmatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });

				// navigateTo
                $(document).on('click', '[data-toggle="lightbox"][data-gallery="navigateTo"]', function(event) {
                    event.preventDefault();

                    return $(this).ekkoLightbox({
                        onShown: function() {

							this.modal().on('click', '.modal-footer a', function(e) {

								e.preventDefault();
								this.navigateTo(2);

                            }.bind(this));

                        }
                    });
                });

                //anchors.options.placement = 'left';
               // anchors.add('h3');
                $('code[data-code]').each(function() {

                    var $code = $(this),
                        $pair = $('div[data-code="'+$code.data('code')+'"]');

                    $code.hide();
                    var text = $code.text($pair.html()).html().trim().split("\n");
                    var indentLength = text[text.length - 1].match(/^\s+/)
                    indentLength = indentLength ? indentLength[0].length : 24;
                    var indent = '';
                    for(var i = 0; i < indentLength; i++)
                        indent += ' ';
                    if($code.data('trim') == 'all') {
                        for (var i = 0; i < text.length; i++)
                            text[i] = text[i].trim();
                    } else  {
                        for (var i = 0; i < text.length; i++)
                            text[i] = text[i].replace(indent, '    ').replace('    ', '');
                    }
                    text = text.join("\n");
                    $code.html(text).show();

                });
            });
			
			
</script>
<!-- Custom Javascript Variables -->
<script type="text/javascript">
var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>',
	$pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>',
	$pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler',
	$pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler',
	$ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
</script>
<?php
if ( isset( $this->app_data['dynamic_js'] ) ) {
echo $this->app_data['dynamic_js'];
}
?>
</body>
</html>
