<?php if ( isset( $private_profile ) && $private_profile ) { ?>
	<!--Modal Personal Information | Review-->
	<div id="personal-information-profile-modal" data-callback-name="profile_personal_information_callback" class="callback_init modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Personal Information</h4>
				</div>
				<form id="profile_personal_information_callback" class="bs-component">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Email:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $user_email; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Contact Number:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $mobile_no; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Alt Contact No:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $alternative_contact_no; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Date of Birth:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $date_of_birth; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Permanent Address:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $permanent_address . ', ' . $permanent_city; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Current Address:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $current_address . ', ' . $current_city; ?></h5>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--/Modal Personal Information | Review-->
<?php } ?>