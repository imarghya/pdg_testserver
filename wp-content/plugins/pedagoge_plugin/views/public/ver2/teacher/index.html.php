<?php
error_reporting(0);
include_once('teacher_profile_view_code_processing.html.php'); ?>
<?php
$teacher_model = new ModelTeacher();
$query_model = new ModelQuery();
$profile_slug = sanitize_text_field( $this->app_data['pdg_profile_slug'] );
$user_id=$teacher_model->get_user_id_byslug($profile_slug);
$user_data_teacher=$teacher_model->get_user_data_teacher($user_id);
//print_r($user_data_teacher);
$rate=$query_model->get_ratings($user_id);
$current_user_id = $this->app_data['pdg_current_user']->ID;
$user_names_det=$teacher_model->get_user_name_nick($user_id);

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url=explode("/",$actual_link);
$next_url=$url[count($url)-3].'/'.$url[count($url)-2];
?>
<?php
$is_current_user_admin = current_user_can('manage_options') || current_user_can('view_teacher_institute_personal_info');
$str_teacher_notes = '';
$str_show_notes_button = '';
if($is_current_user_admin) {
    $str_show_notes_button = '
	    <button class="notes_model" id="cmd_show_notes" data-toggle="modal" data-target="#modal_teachers_notes">
		    <i class="fa fa-info-circle" aria-hidden="true"></i> Notes
	    </button>
    ';
    if(isset($teacher_notes)) {
	    $str_teacher_notes = $teacher_notes;
    }
}

$user = new WP_User( $current_user_id );
$role='';//administrator
if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
    foreach ( $user->roles as $role );	
}
//echo $role;
?>
<section class="body_wrapper">
    
<input type="hidden" id="asset_url" value="<?php echo PEDAGOGE_ASSETS_URL; ?>">
    <div class="container">
	    <div class="row">
	<?php if($current_user_id!=$user_id){ ?>
	    <div class="col-xs-12 col-sm-5 col-md-6 pull-right alert">
		<ul>
		    <li>
			<?php
			if ( ( is_numeric( $logged_in_current_user_id ) && $logged_in_current_user_id > 0 ) && $is_current_user_allowed_to_review == true ) {
				if ( $current_user_has_rated ) {
					$review_btn = '<a href="#" class="write_review_btn" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '"><div class="review_icon_holder"><img src="'.PEDAGOGE_ASSETS_URL.'/teacher/img/head_icon_1.png" alt="Review"></div>
			                             <p>Thanks for rating</p></a>';
				} else {
					//$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#review-profile-modal"><i class="material-icons">&#xE560;</i>&nbsp;Write A Review </button>';
					$review_btn='<a href="#" class="write_review_btn" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#review-profile-modal"><div class="review_icon_holder"><img src="'.PEDAGOGE_ASSETS_URL.'/teacher/img/head_icon_1.png" alt="Review"></div>
			                             <p>Write a <br>Review</p></a>';
					$review_allowed = true;
				}
			} else {
				$review_btn = '<a href="#" data-teacher_user_id="'.$teacher_user_id.'" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal"><div class="review_icon_holder"><img src="'.PEDAGOGE_ASSETS_URL.'/teacher/img/head_icon_1.png" alt="Review"></div>
			<p>Write a <br>Review</p></a>';
			}
			echo $review_btn;
			?>
		    </li>
		    <li>
			<!--<a href="#"><div class="review_icon_holder"><img src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/head_icon_2.png" alt="Review"></div>
			<p>Recommend</p></a>-->
			<?php
			if ( ( is_numeric( $logged_in_current_user_id ) && $logged_in_current_user_id > 0 ) && $is_current_user_allowed_to_review == true ) {

				if ( $current_user_has_recommended ) {
					//$str_recommendation_button = '<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-recommended="yes" data-current_user="' . $logged_in_current_user_id . '" data-teacher_user_id="' . $teacher_user_id . '" data-loading-text="Processing..." data-recommendation_count="' . $recommendation_count . '"><i class="material-icons">&#xE8DB;</i>&nbsp;Unrecommend</button>';
			                $str_recommendation_button='<a href="#" id="write_recomended" class="write_review_btn1 write_recomended" idd="1" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '" ><div class="review_icon_holder"><img src="'.PEDAGOGE_ASSETS_URL.'/teacher/img/un.png" alt="Review"></div>
			                             <p>Unrecommend</p></a>';
				} else {
					//$str_recommendation_button = '<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-recommended="no" data-current_user="' . $logged_in_current_user_id . '" data-teacher_user_id="' . $teacher_user_id . '" data-loading-text="Processing..." data-recommendation_count="' . $recommendation_count . '"><i class="material-icons">&#xE8DC;</i>&nbsp;Recommend</button>';
				       $str_recommendation_button='<a href="#" id="write_recomended" class="write_review_btn1 write_recomended" idd="0" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '" ><div class="review_icon_holder"><img src="'.PEDAGOGE_ASSETS_URL.'/teacher/img/head_icon_2.png" alt="Review"></div>
			                             <p>Recommend</p></a>';
				}
			} else {
				//$str_recommendation_button = '
				//	<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-teacher_user_id="' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal">
				//		<i class="material-icons">&#xE8DC;</i>&nbsp;Recommend
				//	</button>';
					
				$str_recommendation_button='<a href="#" data-teacher_user_id="' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal"><div class="review_icon_holder"><img src="'.PEDAGOGE_ASSETS_URL.'/teacher/img/head_icon_2.png" alt="Review"></div>
			                                   <p>Recommend</p></a>';
			}
			
			echo $str_recommendation_button;
			?>
		    </li>
		    <li>
			<a href="#" data-toggle="modal" data-target="#share-profile-modal"><div class="review_icon_holder"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/head_icon_3.png" alt="Review"></div>
			<p>Share</p></a>
		    </li>
		    <li>
			    <a href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#takeademo-callback-profile-modal"><div class="review_icon_holder"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/head_icon_4.png" alt="Review"></div>
			<p>Take a <br>Demo</p></a>
		    </li>
		    <li>
			    <a href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#request-callback-profile-modal"><div class="review_icon_holder"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/head_icon_5.png" alt="Review"></div>
			<p>Request Callback</p></a>
		    </li>
		    <?php if($user_data_teacher[0]->hometutor=='Online'){ ?>
		    <li>
			<a href="#"><div class="review_icon_holder"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/head_icon_6.png" alt="Review"></div>
			<p>Teaches <br>Online</p></a>
		    </li>
		    <?php } ?>
		</ul>
	    </div>
	    <?php } ?>
	    <?php
	    $nname='';
		 if($user_data_teacher[0]->nick_name!=''){
		  $nname='<span>('.$user_data_teacher[0]->nick_name.')</span>';
		 }
		 if(filter_var($user_data_teacher[0]->nick_name, FILTER_VALIDATE_EMAIL) !== false){
		  $nname='';  
		 }
		 ?>
	    <?php if($current_user_id!=$user_id){ ?>
	    <div class="col-xs-9 col-sm-7 col-md-6 profile_picture">
		    <figure class="h1_heading profile_card profile_card_public">
		    <div class="image_holder pull-left">
		   <img src="<?php echo get_site_url(); ?>/wp-content/plugins/pedagoge_plugin/storage/uploads/images/<?php echo $user_id; ?>/profile.jpg" alt="Profile">
		    </div>
		    <h1><?php echo $teacher_profile_heading; ?></h1>
		    <figcaption>
			<?php if($user_data_teacher[0]->is_phone_verified==1 && $user_data_teacher[0]->is_email_verified==1){ ?>
			    <p class="text-uppercase verified_profile"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_yellow.png" alt="Verified"> Verified</p>
			<?php } ?>
			    <ul>
				<?php foreach($rate['rating_star'] AS $star){ ?>
				 <li>
				<img style="width: 20px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				 </li>
				<?php } ?>
				<li>(<?php echo $rate['rating_count']; ?>)</li>
			</ul>
		    </figcaption>
		</figure>
	    </div>
	    <?php }else{ ?>
	    <div class="col-xs-9 col-sm-7 col-md-6 profile_picture">
                	<figure class="h1_heading profile_card">
                    	<div class="image_holder pull-left">
                        	 <img src="<?php echo get_site_url(); ?>/wp-content/plugins/pedagoge_plugin/storage/uploads/images/<?php echo $user_id; ?>/profile.jpg" alt="Profile">
                        </div>
                        <h1><?php echo $teacher_profile_heading; ?></h1>
                    	<figcaption>
                        	<ul>
                            	<?php foreach($rate['rating_star'] AS $star){ ?>
				 <li>
				<img style="width: 20px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				 </li>
				<?php } ?>
                                <li>(<?php echo $rate['rating_count']; ?>)</li>
                            </ul>
                            <button type="button">Likes <i class="fa fa-thumbs-up" aria-hidden="true"></i> <sub><?php echo $recommendation_count; ?></sub></button>
                            <h2><?php echo $user_data_teacher[0]->name; ?><?php echo $nname; ?></h2>
                        </figcaption>
                    </figure>
                </div>
	        <?php if($user_data_teacher[0]->is_phone_verified==0 || $user_data_teacher[0]->is_email_verified==0){ ?>
	         <div class="col-xs-3 col-sm-5 col-md-5 alert">
                	<button type="button" onclick="myFunction1()"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/alert.png" alt="Alert"></button>
                    <div id="myDIV1" style="display:none;" class="alert_box">
                    	<ul>
                            <?php if($user_data_teacher[0]->is_phone_verified==0){?>
                            <li>Phone Verification Pending <button type="button" phone-no="<?php echo $user_names_det[0]->mobile_no; ?>" idd="0" <?php if($user_data_teacher[0]->is_phone_verified==0){ ?> id="phone_verifi" <?php } ?>><?php if($user_data_teacher[0]->is_phone_verified==0){ echo "Verify Now";}else{echo "Verified";} ?></button></li>
                            <?php } ?>
			    <?php if($user_data_teacher[0]->is_email_verified==0){?>
			    <li>Email Verification Pending <button type="button" email-id="<?php echo $user_names_det[0]->user_email; ?>" <?php if($user_data_teacher[0]->is_email_verified==0){ ?> id="email_verifi" <?php } ?>><?php if($user_data_teacher[0]->is_email_verified==0){ echo "Verify Now";}else{echo "Verified";} ?></button></li>
                            <?php } ?>
			    <!--<li>Address Verification Pending <button type="button" <?php if($user_data_teacher[0]->verified=='no'){ ?> <?php } ?>><?php if($user_data_teacher[0]->verified=='no'){ echo "Verify Now";}else{echo "Verified";} ?></button></li>-->
                        </ul>
                    </div>
                </div>
		<?php } ?>
	    <?php } ?>
	</div>
    </div>
</section>
    
    <section class="body_wrapper_1">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-12 note_button_controller">
		<?php echo $str_show_notes_button; ?>
                </div>
                <div class="col-xs-12 note_button_controller">
                    <?php echo $str_show_internal_rating_button; ?>
                </div>
		<?php if($current_user_id!=$user_id){ ?>
            	<div class="col-xs-12 col-sm-12 col-md-12 margin_top_50 h1_heading edit_contact">
                	<h2 id="te_name"><?php echo $user_data_teacher[0]->name; ?><?php echo $nname; ?></h2>
                </div>
		<?php } ?>
		<?php if($current_user_id==$user_id){ ?>
		<div class="col-xs-12 col-sm-2 col-md-2 pull-right edit_contact">
                	<button type="button" class="edit_button" onclick="window.open('<?php echo get_site_url(); ?>/profile?nexturl=<?php echo $next_url; ?>');">Edit <img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/edit.png" alt="Edit"></button>
                </div>
		<?php } ?>
            	<div class="col-xs-12 col-sm-10 col-md-10 edit_contact">
                	<ul class="h1_heading margin_top_20 edit_contact_details clearfix">
			<?php if($current_user_id!=$user_id){ ?>
                    	<li>
                        	<h1>Basic Info:</h1>
                            <p>
                            	<span><i>Phone <?php if($role=='administrator' || $role=='sales' || $role=='business_development'){echo ': '.$user_names_det[0]->mobile_no;} ?></i> <?php if($user_data_teacher[0]->is_phone_verified==1){?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_right.png" alt="verified"> Verified</strong><?php }else{ ?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/red_alert.png" alt="verified"> Unverified</strong><?php } ?></span> 
                                <span><i>Email <?php if($role=='administrator' || $role=='sales' || $role=='business_development'){echo ': '.$user_names_det[0]->user_email;} ?></i> <?php if($user_data_teacher[0]->is_email_verified==1){?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_right.png" alt="verified"> Verified</strong><?php }else{ ?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/red_alert.png" alt="verified"> Unverified</strong><?php } ?></span> 
                                <!--<span><i>Address</i> <?php if($user_data_teacher[0]->verified=='yes'){?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_right.png" alt="verified"> Verified</strong><?php }else{ ?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/red_alert.png" alt="verified"> Unverified</strong><?php } ?></span>-->
                            </p>
                        </li>
			<?php }else{ ?>
			<li>
                        	<h1>Contact:</h1>
                            <p>
                            	<span><?php echo $user_names_det[0]->mobile_no; ?> <?php if($user_data_teacher[0]->is_phone_verified==1){?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_right.png" alt="verified"> Verified</strong><?php }else{ ?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/red_alert.png" alt="verified"> Unverified</strong><?php } ?></span> 
                                <?php if($user_data_teacher[0]->altcontactnumber!=''){ ?>
				<span><?php if($user_data_teacher[0]->altcontactnumber!='NULL'){ echo $user_data_teacher[0]->altcontactnumber; }?> <strong><!--<img src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/red_alert.png" />--> <!--(Alternative no)--></strong></span>
                                <?php } ?>
			    </p>
                        </li>
			<li>
                        	<h1>Email:</h1>
                            <p>
                            	<span><?php echo $user_names_det[0]->user_email; ?> <?php if($user_data_teacher[0]->is_email_verified==1){?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_right.png" alt="verified"> Verified</strong><?php }else{ ?><strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/red_alert.png" alt="verified"> Unverified</strong><?php } ?></span> 
                            </p>
                        </li>
			<li>
                        	<h1>DOB:</h1>
                            <p>
                            	<span class="text-uppercase"><?php echo $user_data_teacher[0]->date_of_birth; ?></span> 
                            </p>
                        </li>
			<li>
                        	<h1>Address:</h1>
                            <p>
                            	<span><?php if($user_data_teacher[0]->address!=''){ echo $user_data_teacher[0]->address;}else{echo $user_data_teacher[0]->current_address;}; ?></span> 
                            </p>
                        </li>
			<?php } ?>
                        <li>
                        	<h1>Education:</h1>
                            <p>
				<?php
				$quali_arr=array();
				$qualifications=$teacher_model->get_teacher_qualification($user_id);
				foreach($qualifications AS $quali){
				//$quali_arr[]=$quali->qualification;
				//}
				?>
                            	<span><?php echo $quali->qualification;//echo implode(",",$quali_arr); ?><!--<strong><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/star_right.png" alt="verified"> Verified</strong>--></span> 
                                <?php } ?>
                            </p>
                        </li>
			<?php if($current_user_id!=$user_id){ ?>
                        <li>
                        	<h1>Gender:</h1>
                            <p>
                            	<span><?php echo ucwords($user_data_teacher[0]->gender);?></span> 
                            </p>
                        </li>
			<?php } ?>
                        <li>
                        	<h1>Language of Instruction:</h1>
                            <p>
                            	<span><?php echo $user_data_teacher[0]->language; ?></span> 
                            </p>
                        </li>
                        <li> 
                        	<h1>About Me:</h1>
                            <p>
                            	<span><?php echo $user_data_teacher[0]->aboutcoaching; ?></span> 
                            </p>
                        </li>
                        <li>
                        	<h1>Experience:</h1>
                            <p>
                            	<span><?php echo $user_data_teacher[0]->experience; ?></span> 
                            </p>
                        </li>
                        <li>
                        	<h1>Subjects:</h1>
                            <p>
                            	<span><?php if($user_data_teacher[0]->subjects!=''){ echo $user_data_teacher[0]->subjects;}else{echo $user_data_teacher[0]->te_subj;} ?></span> 
                            </p>
                        </li>
                        <li>
                        	<h1>Classes:</h1>
                            <p>
				<?php
				//echo $user_data_teacher[0]->te_subj;
				if($user_data_teacher[0]->is_new==1){?>
				<?php if($user_data_teacher[0]->classofteaching!=''){ ?>
                            	<span><b>School(Standards):</b> <?php echo str_replace("-"," to ",$user_data_teacher[0]->classofteaching); ?></span>
				<?php } ?>
				<?php if($user_data_teacher[0]->classofteaching_college!=''){ ?>
                                <span><b>College(Years):</b> <?php echo str_replace("|"," to ",$user_data_teacher[0]->classofteaching_college); ?></span>
				<?php } ?>
				
				<?php if($user_data_teacher[0]->classofteaching_age!=''){ ?>
                                <span><b>Age:</b> <?php echo str_replace("-"," to ",$user_data_teacher[0]->classofteaching_age); ?> years</span>
				<?php } ?>
				<?php }else{ $class_age=$user_data_teacher[0]->subject_type.','.$user_data_teacher[0]->t_course_age; echo rtrim($class_age,","); }?>
                                
			    </p>
                        </li>
                        <li>
                        	<h1>Localities:</h1>
                            <p>
				<?php
				if($user_data_teacher[0]->locality1!='NULL'){
				$stloc= "Student's Location";
			        $btloc="Both own location and student's location";
				if($user_data_teacher[0]->online==1){
				$ol=' (Online)';	
				}
				$teacher_loc=array();
				if($user_data_teacher[0]->locality1!=''){
				$teacher_loc[]=$user_data_teacher[0]->locality1;
				}
				if($user_data_teacher[0]->locality2!=''){
				$teacher_loc[]=$user_data_teacher[0]->locality2;
				}
				if($user_data_teacher[0]->locality3!=''){
				$teacher_loc[]=$user_data_teacher[0]->locality3;
				}
				if($user_data_teacher[0]->hometutor==$stloc || $user_data_teacher[0]->hometutor==$btloc){
				$wl=' (Willing to Travel)';	
				}
				@$location=implode(",",$teacher_loc).'<br/><span style="color:#3d6a88;display:inline-block;font-weight:bold;">'.$wl.$ol.'</span>';
				}
				else{
				$location=$user_data_teacher[0]->te_loc;	
				}
				?>
                            	<span><?php echo rtrim(str_replace("NULL","",$location),","); ?> <?php //echo $user_data_teacher[0]->locality2; ?> <?php //echo $user_data_teacher[0]->locality3; ?></span> 
                            </p>
                        </li>
                        <li>
                        	<h1>Fees:</h1>
                            <p>
				<?php
				$fees=$user_data_teacher[0]->feeslower;
				$fees_lower=$fees-(($fees*10)/100);
				$fees_up=$fees+(($fees*10)/100);
				?>
                            	<span><!--<i class="fa fa-inr" aria-hidden="true"></i> 1800 to--> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $fees_lower; ?> to <?php echo $fees_up; ?> <?php echo $user_data_teacher[0]->feesupper; ?> </span>  
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <section class="ongoing_wrapper">
	<?php
	$internal_rating=$teacher_model->get_teacher_internal_rat($user_id);
	$total_classes_accepted=0;
	$total_classes_provided=0;
	$total_demo_converted=0;
	$total_demo_provided=0;
	$total_classes_ongoing=0;
	foreach($internal_rating AS $rat){
	$total_classes_accepted=$rat->classes_accepted;
	$total_classes_provided =$rat->classes_provided;
	$total_demo_converted =$rat->demo_converted;
	$total_demo_provided =$rat->demo_provided;
	$total_classes_ongoing =$rat->classes_ongoing;
	}
	$classes_accepted1=@$internal_rating[0]->classes_accepted;
	$intr=count($internal_rating);
	if($total_classes_accepted==0 && $total_demo_converted==0 && $total_classes_ongoing==0 && $current_user_id!=$user_id){
	}
	else{	
	?>
    	<div class="container">
        	<div class="row blue_bg border_radies_6">
            	<div class="col-xs-12 col-sm-4 col-md-4 text-center text-uppercase margin_top_20 ongoing_classes">
                	<figure>
                    	<img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/ongoing_1.png" alt="Classes Accepted">
                    	<figcaption>
                        	<h4><?php if($intr>0){echo $total_classes_accepted.'/'.$total_classes_provided; }else{echo "0/0";}?></h4>
                            <p>Classes Accepted</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 text-center text-uppercase margin_top_20 ongoing_classes">
                	<figure>
                    	<img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/ongoing_2.png" alt="Demo Converted">
                    	<figcaption>
                        	<h4><?php if($intr>0){ echo $total_demo_converted.'/'. $total_demo_provided; }else{echo "0/0";}?></h4>
                            <p>Demo Converted</p>
                        </figcaption>
                    </figure>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 text-center text-uppercase margin_top_20 margin_bottom_20 ongoing_classes">
                	<figure>
                    	<img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/ongoing_3.png" alt="Classes Accepted">
                    	<figcaption>
                        	<h4><?php if($intr>0){ echo $total_classes_ongoing; }else{echo 0;}?></h4>
                            <p>Ongoing Classes</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
	<?php } ?>
    </section>
    
    <section class="body_wrapper_2">
    	<div class="container">
	   <?php
	   $past_schools=array();
	   if($user_data_teacher[0]->past_schools!=''){
	   $past_schools=explode(",",$user_data_teacher[0]->past_schools);
	   }
	   //if(count($past_schools)!=0 ){
	   ?>
           <div class="row margin_top_50">
		<?php if($current_user_id==$user_id){ ?>
		<div class="col-xs-5 col-sm-3 col-md-4 pull-right prominent_school">
                	<button type="button" class="edit_button" onclick="window.open('<?php echo get_site_url(); ?>/profile?nexturl=<?php echo $next_url; ?>&#school');">Edit <img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/edit.png" alt="Edit"></button>
                </div>
		<?php } ?>
		<?php if(count($past_schools)> 0 || $current_user_id==$user_id){ ?>
            	<div class="col-xs-12 col-sm-6 col-md-4 col-xs-offset-0 col-sm-offset-3 col-md-offset-4 text-center prominent_school">
                	<p>Prominent schools where I have taught students</p>
                </div>
		<?php } ?>
            </div>
	   
	    <?php
	    if(count($past_schools)==0 && $current_user_id==$user_id){
		//private
		echo "<div style='text-align: center' class='panel-body'><h4>No schools selected yet</h4></div>";
		}
		elseif(count($past_schools)==0 && $current_user_id!=$user_id){
		//echo "<h4>Not achievements added yet.</h4>";	
		}
	    else{
	    ?>
            <div class="row margin_top_10">
            	<div class="owl-carousel owl-theme owl1">
		<?php
		
		foreach($past_schools AS $past_school){
		if(trim($past_school)!=''){
		?>
                    <div class="item">
                        <div class="text-center image_holder_slider">
                        	<p><?php echo trim($past_school); ?></p>
                        </div>
                    </div>
		<?php }} ?>
                </div>
            </div>
	    <?php } ?>
	    <?php //} ?>
	    
            <div class="row margin_top_20 margin_bottom_20">
            	<div class="col-xs-12 col-sm-6 col-md-6 col-xs-offset-0 col-sm-offset-3 col-md-offset-3 text-center prominent_school">
                	<p>Achievements</p>
                </div>
            </div>
            <div class="row">
            	<div class="col-xs-12 achievements_box">
		    <?php
		    $i=1;
			$teacher_achievements=$teacher_model->get_teacher_achivements($user_id);
			if(count($teacher_achievements)==0 && $current_user_id==$user_id){
			//private
			echo "<div style='text-align: center' class='panel-body'><h4>No acheivement added yet.</h4></div>";
			}
			elseif(count($teacher_achievements)==0 && $current_user_id!=$user_id){
			echo "<div style='text-align: center' class='panel-body'><h4>No achievements added yet.</h4></div>";	
			}
			else{
		    ?>
                    <ul class="achivment_part">
			<?php
			foreach($teacher_achievements AS $teach){
			?>
                        <li>
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading" data-toggle="collapse" data-target='#achiv<?php echo $i; ?>'>
                                         <h4 class="panel-title collapsed" data-toggle="collapse" data-target='#collapse1'><?php echo implode(' ', array_slice(explode(' ', $teach->achievement), 0, 8)); ?></h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="achiv<?php echo $i; ?>">
                                        <div class="panel-body">
                                        	<p><?php echo $teach->achievement; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
			<?php $i++; } ?>
                    </ul>
		    <?php } ?>
                </div>
            </div>
            <div class="row margin_top_20">
            	<div class="col-xs-12 col-sm-6 col-md-6 col-xs-offset-0 col-sm-offset-3 col-md-offset-3 text-center prominent_school">
                	<p>Additional information:</p>
                </div>
            </div>
            <div class="row">
            	<div class="col-xs-6 col-sm-6 col-md-4 col-md-offset-2 sign_board">
                	<div><p><?php if($user_data_teacher[0]->interest_in_hired==0){echo "Not";} ?> Available for <span class="block_element text-uppercase">Hiring</span></p></div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 sign_board">
                	<div><p><?php if($user_data_teacher[0]->interest_in_cw==0){echo "Not";} ?> Available for <span class="block_element text-uppercase">Workshops</span></p></div>
                </div>
            </div>
	    <?php
	    $gallery_imgs=$teacher_model->get_gallery_images($user_id);
	    $count_img=count($gallery_imgs);
	    $gallery_vids=$teacher_model->get_gallery_videos($user_id);
	    $count_vids=count($gallery_vids);
	    if($count_img!=0 || $count_vids!=0){
	    ?>
            <div class="row margin_top_20">
            	<div class="col-xs-12 prominent_school">
                	<p>Gallery :</p>
                </div>
            </div>
            <div class="row margin_top_10">
            	<div class="owl_holder">
                    <div class="owl-carousel owl-theme owl2">
			 <?php
			  if($count_img > 0){
			  foreach($gallery_imgs AS $gallery){
			  ?>
                        <div class="item">
                            <div class="avter_box">
                                <a data-toggle="lightbox" data-gallery="mixedgallery" href="<?php echo get_site_url()."/wp-content/plugins/pedagoge_plugin/storage/uploads/images/".$user_id."/gallery/".$gallery->image; ?>"><img src="<?php echo get_site_url()."/wp-content/plugins/pedagoge_plugin/storage/uploads/images/".$user_id."/gallery/".$gallery->image; ?>" alt="Avter"></a>
                            </div>
                        </div>
			<?php }} ?>
			
			<?php
			  if($count_vids > 0){
			  foreach($gallery_vids AS $gallery_vid){
			  ?>
                        <div class="item">
                            <div class="avter_box">
                                <a data-toggle="lightbox" data-gallery="mixedgallery" href="https://www.youtube.com/embed/<?php echo $gallery_vid->video_url; ?>"><iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $gallery_vid->video_url; ?>" frameborder="0" allowfullscreen></iframe> <div class="transparent_img"></div></a>
                            </div>
                        </div>
			<?php }} ?>
                    </div>
                </div>
            </div>
	    <?php } ?>
	    <?php
	    $recom=$teacher_model->get_teacher_recomen($user_id);
	    $reviews=$teacher_model->get_student_review($user_id);
	    $count_review=count($reviews);
	    if($recom!=0 || $count_review!=0){
	    ?>
            <div class="row margin_top_30">
            	<div class="col-xs-12 col-sm-3 col-md-2 text-center text-uppercase review_panel mobile_view_review">
                  <p>Reviews</p>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-1 text-center review_panel mobile_view_review">
                  <p><?php echo $recom; ?> Recommendation</p>
               </div>
                <div class="col-xs-12 col-sm-3 col-md-3 text-center review_panel mobile_view_review">
                  <p><?php echo $count_review; ?> Review</p>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center review_panel mobile_view_review">
                  <ul>
                     <?php foreach($rate['rating_star'] AS $star){ ?>
				<li>
				<img style="width: 20px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				</li>
		     <?php } ?>
                  </ul>
               </div>
            </div>
            <div class="row margin_top_20">
                <ul class="public_review_box">
		 <?php foreach($reviews AS $rev){ if($rev->review_text!=''){?>	
                    <li>
                        <div class="col-xs-4 col-sm-3 col-md-2 col-xs-offset-4 col-sm-offset-0 col-md-offset-0 text-center student_dummy">
			   <?php
			    //$thumb_name='wp-content/plugins/pedagoge_plugin/storage/uploads/images/'.$rev->reviewer_user_id.'/profile.jpg';
			    //if (file_exists($thumb_name)) {
			   ?>
			  <!-- <img src="<?php echo plugins_url()."/pedagoge_plugin/storage/uploads/images/".$rev->reviewer_user_id ."/profile.jpg"; ?>" alt="Student">-->
			   <?php //}else{ ?>
                           <img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/student.png" alt="Student">
			  
			   <p><?php echo $rev->display_name; ?></p>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-10 text-justify review_details">
                            <p><?php echo $rev->review_text; ?></p>
                        </div>
                    </li>
		    <?php }} ?>
                 </ul>
            </div>
	    <?php } ?>
	     <?php if($current_user_id==$user_id){ ?>
		<div class="row margin_top_40">
		    <div class="col-xs-12 ask_for_review">
			<div id="myDIV3" style="display:none;" class="review_model_box">
			  <h4>Share</h4>
			  <?php
			  $actual_link = 'Please Add a Review on my profile. '.(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			  $sh_link=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			  ?>
			  <textarea placeholder="<?php echo $actual_link; ?>"></textarea>
			  <ul>
			     <li><a href="javascript:void(0)" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $sh_link; ?>&title=Pedagoge&summary=Please Add a Review on my profile&source=Pedagoge', 'sharer', 'toolbar=0, status=0, width=626, height=436');return false;"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/ln.png" alt="Linkdin"></a></li>
			     <li><a target="_blank" href="https://www.whatsapp.com/"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/wt_app.png" alt="Whats App"></a></li>
			     <li><a href="javascript:void(0)" onclick="window.open( 'https://www.facebook.com/sharer/sharer.php?u=<?php echo $sh_link; ?>', 'sharer', 'toolbar=0, status=0, width=626, height=436');return false;"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/fb.png" alt="Face Book"></a></li>
			     <li><a href="javascript:void(0)" onclick="window.open( 'https://plus.google.com/share?url=<?php echo $sh_link; ?>', 'sharer', 'toolbar=0, status=0, width=626, height=436');return false;"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/g_plus.png" alt="Google Plus"></a></li>
			  </ul>
		       </div>
		       <button type="button" class="ask_for_review_button" onclick="myFunction3()">+ Ask for reviews</button>
		       <button type="button" class="ask_for_review_button" onclick="window.open('http://www.pedagoge.com/workshop/');">+ Create a workshop</button>
		       
		   </div>
		</div>
	    <?php } ?>
	    
	    
        </div>
	<?php //include_once(PEDAGOGE_PLUGIN_DIR.'views/public/ver2/common_code/callback_buttons.html.php'); ?>
	
    </section>

<?php
	if($is_current_user_admin) :
?>
<div class="modal fade" id="modal_teachers_notes" tabindex="-1" role="dialog" aria-labelledby="ViewTeacherNotesLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ViewTeacherNotesLabel">Teacher Notes</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-info col-md-12" data-loading="Reloading Notes" id="cmd_reload_teachers_notes">
							<i class="fa fa-refresh" aria-hidden="true"></i> Reload Notes
						</button>
						<label for="txt_teacher_notes">Notes:</label>
						<textarea id="txt_teacher_notes" class="form-control" placeholder="Write your notes"></textarea>
						<label for="txt_teacher_note_maker" class="col-md-6">Your Name</label>
						<input type="text" class="form-control col-md-6" id="txt_teacher_note_maker" placeholder="Input your name"/>
						<input type="hidden" value="<?php echo $teacher_user_id; ?>" id="hidden_teacher_user_id"/>
						<button class="btn btn-success col-md-12" data-loading="Saving Notes" id="cmd_save_teachers_notes">
							<i class="fa fa-floppy-o" aria-hidden="true"></i>
							Save Notes
						</button>
						<br />
						<hr />
						<div id="div_teacher_notes_save_result_area"></div>
					</div>
					<div class="col-md-6">
						<div id="div_teacher_notes" style="overflow:auto; max-height: 400px;">
							<?php echo $str_teacher_notes; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php 

/**

  * Internal Rating System 

  * @author Pritam Ghosh

  */



   if($is_current_user_admin) : 

	?>

	<div class="modal fade model_controller_mid" id="modal_teachers_internal_rating" tabindex="-1" role="dialog" aria-labelledby="ViewTeacherNotesLabel">

		<div class="modal-dialog" role="document">

			<div class="modal-content modal-lg">

				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					<h4 class="modal-title" id="ViewTeacherNotesLabel">Internal Rating , Classes Details and Payment Plans of <?= $teacher_name ?></h4>

				</div>

				<div class="modal-body">

					<div class="row">

						<div class="col-md-6">	

							<form class="internal_rating_input_section" id="rating_form">

							    <div class="well">

							    <center><h5 class="" id="ViewTeacherNotesLabel">Rating</h5></center>

								<label for="punctuality_rating">Punctuality</label>

								<select id="punctuality_rating">

								    <option value=""></option>

								    <?php for ($i=1 ; $i<=5 ; $i++) { 

								    	if ( $i == $punctuality_rating ) { ?>

								    		<option value="<?= $i; ?>" selected><?= $i; ?></option>

								    <?php } else { ?>

								        <option value="<?= $i; ?>"><?= $i; ?></option>

								    <?php } } ?>

							    </select><br>

							    <label for="responsiveness_rating">Responsiveness</label>

							    <select id="responsiveness_rating">

							        <option value=""></option>

							        <?php for ($i=1 ; $i<=5 ; $i++) { 

								    	if ( $i == $responsiveness_rating ) { ?>

								    		<option value="<?= $i; ?>" selected><?= $i; ?></option>

								    <?php } else { ?>

								        <option value="<?= $i; ?>"><?= $i; ?></option>

								    <?php } } ?>

							    </select><br>

							    <label for="behaviour_rating">Behaviour</label>

							    <select id="behaviour_rating">

							        <option value=""></option>

							    	<?php for ($i=1 ; $i<=5 ; $i++) { 

								    	if ( $i == $behaviour_rating ) { ?>

								    		<option value="<?= $i; ?>" selected><?= $i; ?></option>

								    <?php } else { ?>

								        <option value="<?= $i; ?>"><?= $i; ?></option>

								    <?php } } ?>

							    </select><br>

							    <label for="communication_skills_rating">Communication Skills</label>

							    <select id="communication_skills_rating">

							        <option value=""></option>

							    	<?php for ($i=1 ; $i<=5 ; $i++) { 

								    	if ( $i == $communication_skills_rating ) { ?>

								    		<option value="<?= $i; ?>" selected><?= $i; ?></option>

								    <?php } else { ?>

								        <option value="<?= $i; ?>"><?= $i; ?></option>

								    <?php } } ?>

							    </select>

								</div>



								<!-- classes details start -->

								<div class="well">

								<center><h5 class="" id="ViewTeacherNotesLabel">Classes Details</h5></center>

								<!-- classes accepted -->

								<label for="classes_accepted">Classes Accepted</label>

								<?php if ( $classes_accepted > 0 ) { ?>

									<input type="number" id="classes_accepted" value="<?= $classes_accepted; ?>" min="0" style="width: 3em" />&nbsp;<i>/</i>&nbsp;

								<?php } else { ?>

								    <input type="number" id="classes_accepted" min="0" style="width: 3em" />&nbsp;<i>/</i>&nbsp;

								<?php } ?>



								<?php if ( $classes_provided > 0 ) { ?>

									<input type="number" id="classes_provided" value="<?= $classes_provided; ?>" min="0" style="width: 3em" />

								<?php } else { ?>

								    <input type="number" id="classes_provided" min="0" style="width: 3em" />

								<?php } ?>

								

								<p style="color: green">[ Accepted { classes he/she accepted } / Provided { classes we provided } ]</p>

								<br>

								<!-- /classes accepted -->



								<!-- demo converted -->

								<label for="demo_converted">Demo Converted&nbsp;</label>
                                                                <input type="hidden" id="demo_converted_fix" value="<?= $demo_converted; ?>"/>
								<?php if ( $demo_converted > 0 ) { ?>

									<input type="number" id="demo_converted" value="<?= $demo_converted; ?>" min="0" style="width: 3em" />

								<?php } else { ?>

								    <input type="number" id="demo_converted" min="0" style="width: 3em" />&nbsp;<i>/</i>&nbsp;

								<?php } ?>



								<?php if ( $demo_provided > 0 ) { ?>

									<input type="number" id="demo_provided" value="<?= $demo_provided; ?>" min="0" style="width: 3em" />

								<?php } else { ?>

								    <input type="number" id="demo_provided" min="0" style="width: 3em" />

								<?php } ?>

								

								<p style="color: green">[ Converted { demo converted } / Provided { demo he/she gave } ]</p>

								<br>

								<!-- /demo converted -->



								<!-- classes ongoing -->

								<label for="classes_ongoing">Classes Ongoing&nbsp;&nbsp;</label>

								<?php if ( $classes_ongoing > 0 ) { ?>

									<input type="number" id="classes_ongoing" value="<?= $classes_ongoing; ?>" min="0" style="width: 3em" />

								<?php } else { ?>

								    <input type="number" id="classes_ongoing" min="0" style="width: 3em" />

								<?php } ?>

								

								<p style="color: green">[ add only those classes going >= 30 days  ]</p>

								<!-- /classes ongoing -->

								</div>

								<!-- /classes details end -->



								<!-- payment plan -->

								<div class="well">

								<center><h5 class="" id="ViewTeacherNotesLabel">Payment Plan</h5></center>



								<div class="form-group label-floating">

								    <div class="radio" id="payment_plans">

										<?php

										  $payment_plans = array("Plan A","Plan B","Plan C","Plan D","Plan E","Demo");

										  foreach($payment_plans as $payment_plan) {



										  	if ( strcasecmp($payment_plan, $selected_payment_plan) == 0 ) { ?>

										  		<label><input type="radio" value="<?= $selected_payment_plan ?>" name="payment_plan" checked ><?= $payment_plan ?></label>

										<?php } else{ ?>

										        <label><input type="radio" value="<?= $payment_plan ?>" name="payment_plan"><?= $payment_plan ?></label>

										<?php

										     }

										  }

										?>

									</div>

							    </div>

							    </div>

								<!-- /payment plan -->



								<textarea id="txt_teacher_review" class="form-control" placeholder="Write your review" maxlength="500"></textarea>

								

								<input type="text" class="form-control col-md-6" id="txt_teacher_rating_maker" placeholder="Input your name"/>

							</form>



							<input type="hidden" value="<?= $teacher_user_id; ?>" id="hidden_teacher_user_id"/>



							<div class="row">

								<div class="col-md-12">

									<button class="btn btn-sm btn-raised white-text blue-shade style-fix" data-loading="Reloading Notes" id="cmd_reload_teachers_internal_rating">

									<i class="fa fa-refresh" aria-hidden="true"></i>Reload Rating

								   </button>

								   <button class="btn btn-raised white-text green-shade style-fix" data-loading="Saving Ratings" id="cmd_save_teachers_internal_rating">

									<i class="fa fa-floppy-o" aria-hidden="true"></i>

									Save Rating

								   </button>

								</div>

							</div>

							

							<div id="div_teacher_internal_rating_save_result_area"></div>

						</div>

						<div class="col-md-6">

							<div id="div_teacher_internal_rating" style="overflow:auto; max-height: 400px;">

								<?= $str_teacher_internal_rating; ?>
							</div>

						</div>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>

				</div>

			</div>

		</div>

	</div>

<?php endif; ?>
    <?php
   // include_once('modal_teacher_personal_info.html.php');
    $var_passing_modal['is_private_profile'] = $is_private_profile;
    $var_passing_modal['review_allowed'] = $review_allowed;
    $var_passing_modal['allow_demo_modal'] = $allow_demo_modal;
    $var_passing_modal['has_recomended'] = $current_user_has_recommended;
    echo $this->load_view( 'modals/modal-req_callback_profile', $var_passing_modal );
   // echo $this->load_view( 'modals/modal_teachers_internal_rating');
    ?>
   