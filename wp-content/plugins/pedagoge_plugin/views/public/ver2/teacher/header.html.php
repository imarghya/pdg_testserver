<?php
$teacher_model = new ModelTeacher();
$profile_slug = sanitize_text_field( $this->app_data['pdg_profile_slug'] );
$user_id=$teacher_model->get_user_id_byslug($profile_slug);
$user_data_teacher=$teacher_model->get_user_data_teacher($user_id);
$teacher_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/favicon.png">
<link rel="icon" type="image/png" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/favicon.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/favicon.png" sizes="16x16">


<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Please Add a Review on my profile" />
<meta property="og:description" content="Please Add a Review on my profile" />
<meta property="og:url" content="<?php echo $teacher_url; ?>" />
<meta property="og:site_name" content="Pedagoge" />
<meta property="og:image" content="<?php echo get_site_url(); ?>/wp-content/plugins/pedagoge_plugin/storage/uploads/images/<?php echo $user_id; ?>/profile.jpg" />
  
<title>Pedagoge Profile Page</title>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
		var site_var = {};
		site_var.url = "<?=site_url();?>/";
	</script>
<?php //echo $this->load_view( 'header_scripts' ); ?>
	<?php
	$server_name = $_SERVER['SERVER_NAME'];
	switch ( $server_name ) {
		case 'www.pedagoge.com':
			get_template_part( 'template-parts/hot', 'jar' );
			get_template_part( 'template-parts/fb', 'pixel' );
			break;
	}
	?>
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/owl.carousel.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/ekko-lightbox.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/menu.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/css/alertify.core.css" rel="stylesheet" type="text/css">
<link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/css/alertify.default.css" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo PEDAGOGE_THEME_URL; ?>/assets/css/validationEngine/validationEngine.jquery.css" />
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/teacher_public_style.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/media.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>

	<header class="header_wrapper">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 logo">
                	<a href="<?php echo get_site_url(); ?>"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/logo_black.png" alt="Logo"></a>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8 navigation_role">
                	<div class="main-menu-section">
                        <div class="container_12">
                            <nav class="site-navigation main-navigation" role="navigation">
                                <ul class="menu">
                                    <li><a href="career.html">Careers</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                   
				    <li>
					<?php if(is_user_logged_in()) { ?>
					<a href="<?php echo wp_logout_url(); ?>">Logout <i class="fa fa-angle-down" aria-hidden="true"></i></a>
					<?php }else{ ?>
					<a href="<?php echo get_site_url(); ?>/login?nexturl=/">Login</a>
					<?php } ?>
					 <?php if(is_user_logged_in()) { 
                        $user = new WP_User(get_current_user_id());
                        $user_role = $user->roles[0];
                        if($user_role == 'teacher'){
                            $dashboard_link = get_site_url().'/teacherdashboard/';
                        }
                        else
                        {
                            $dashboard_link = get_site_url().'/memberdashboard/?query_type=active';
                        }
                    ?>
                                    	<ul class="sub-menu">
					    <?php if($user_data_teacher[0]->approved=='yes'){ ?>
                                            <li class="menu-item"><a href="<?php echo $dashboard_link; ?>">Dashboard</a></li>
					    <?php } ?>
                                            <li class="menu-item"><a href="<?php echo get_site_url(); ?>/teacher/<?php echo $profile_slug; ?>/">Profile</a></li>
                                            <li class="menu-item"><a href="#">Change Password</a></li>
                                        </ul>
					<?php } ?>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>