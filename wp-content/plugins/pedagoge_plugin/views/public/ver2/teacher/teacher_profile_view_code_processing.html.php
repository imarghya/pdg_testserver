<?php

$str_teacher_data_verified = '
		<div class="text-left txt-verified">
			<i class="fa fa-paper-plane-o"></i>&nbsp;<b>Verified</b>
		</div>';

$teacher_user_id = '';
$teacher_name = '';
$teacher_user_email = '';
$teacher_nick_name = '';
$teacher_profile_heading = '';
$teacher_about_coaching = '';
$teacher_gender = '';
$teacher_present_place_of_work = '';
$teacher_no_of_demo_classes = 'N/A';
$teacher_price_per_demo_class = '';
$teacher_operation_hours_from = '';
$teacher_operation_hours_to = '';
$teacher_teaching_xp = '';
$installment_allowed = '';
$fees_structure = '';
$no_gallery = false;

if ( isset( $teacher_data ) ) {
	foreach ( $teacher_data as $teacher_info ) {
		$teacher_verified = $teacher_info->verified;
		$str_teacher_data_verified = $teacher_verified == 'yes' ? $str_teacher_data_verified : '';
		$teacher_user_id = $teacher_info->user_id;
		$first_name = $teacher_info->first_name;
		$last_name = $teacher_info->last_name;
		$teacher_name = $first_name . ' ' . $last_name;
		$teacher_user_email = $teacher_info->user_email;
		$teacher_nick_name = $teacher_info->students_konws_by;
		if ( $teacher_nick_name == '' ) {
			$teacher_nick_name = '';
		} else {

			$teacher_nick_name = '(' . $teacher_nick_name . ')';
		}

		$teacher_profile_heading = $teacher_info->profile_heading;
		$teacher_about_coaching = $teacher_info->about_coaching;
		$teacher_gender = $teacher_info->gender;
		$teacher_present_place_of_work = $teacher_info->present_place_of_work;
		$teacher_no_of_demo_classes = $teacher_info->no_of_demo_classes;
		$teacher_price_per_demo_class = $teacher_info->price_per_demo_class;
		if ( $teacher_no_of_demo_classes == 0 ) {
			$teacher_price_per_demo_class = '';
		} else if ( $teacher_price_per_demo_class == 0 || $teacher_price_per_demo_class == '' ) {
			$teacher_price_per_demo_class = '(FREE )';
		} else {

			$teacher_price_per_demo_class = '(<small> <i class="fa fa-inr"></i></small>' . $teacher_price_per_demo_class . ' )';
		}

		$teacher_operation_hours_from = $teacher_info->operation_hours_from;
		$teacher_operation_hours_to = $teacher_info->operation_hours_to;
		$teacher_teaching_xp = $teacher_info->teaching_xp;
		$installment_allowed = $teacher_info->installment_allowed;
		$fees_structure = $teacher_info->about_fees_structure;
		if ( $fees_structure == '' || $fees_structure == 0 ) {
			$fees_structure = '';
		} else {
			$fees_structure = 'Fee Structure : ' . $fees_structure . '';
		}
	}
}

////////////////////////////////////////////////Profile Images and Sliders starts//////////////////////////////////////////////
$profile_image_path = 'storage/uploads/images/';
$teacher_profile_image_url = '';
$teacher_no_image_url = '';

$teacher_profile_image_path = PEDAGOGE_PLUGIN_DIR . $profile_image_path . $teacher_user_id . '/profile.jpg';
$teacher_profile_image_orientation = '';
if ( file_exists( $teacher_profile_image_path ) ) {
	$teacher_profile_image_orientation = PDGSearchContent::getImgOrientation( $teacher_profile_image_path );
	$file_timestamp = date( 'U', filemtime( $teacher_profile_image_path ) );
	$teacher_profile_image_url = PEDAGOGE_PLUGIN_URL . '/' . $profile_image_path . $teacher_user_id . '/profile.jpg?' . $file_timestamp;
} else {
	//$teacher_profile_image_url = PEDAGOGE_ASSETS_URL . '/images/sample.jpg';
	$teacher_profile_image_url = ControllerTeacher::get_empty_profile_picture('teacher', $teacher_gender);
}

$gallery_dir = PEDAGOGE_PLUGIN_DIR . $profile_image_path . $teacher_user_id . '/gallery/';
$teacher_no_image_url = PEDAGOGE_ASSETS_URL . '/images/noslide.jpg';
$gallery_url = PEDAGOGE_PLUGIN_URL . '/' . $profile_image_path . $teacher_user_id . '/gallery/';

$str_slide_count = '';
$counter = 0;
$str_slides = '';

if ( is_dir( $gallery_dir ) ) {

	$scanned_directory = array_diff( scandir( $gallery_dir ), array ( '..', '.' ) );
	foreach ( $scanned_directory as $gallery_file ) {
		$str_active = '';
		if ( $counter == 0 ) {
			$str_active = ' active';
			$str_slide_count .= '<li data-target="#gallery-slider" data-slide-to="' . $counter . '" class="active"></li>';
		} else {
			$str_slide_count .= '<li data-target="#gallery-slider" data-slide-to="' . $counter . '"></li>';
		}

		$str_slides .= '
			<div class="item ' . $str_active . '">
				<img src="' . $gallery_url . $gallery_file . '" data-color="lightblue" class="center-block" alt="Gallery Image">
			</div>';

		$counter ++;
	}

	if ( empty( $str_slides ) ) {
		$no_gallery = true;
		$str_slide_counter = '<img src="' . $teacher_no_image_url . '" class="block-rounded slide-institute center-block img-responsive">';
	} else {
		$str_slide_counter = '
	        	<div id="gallery-slider" class="carousel slide" data-ride="carousel">                        
					<ol class="carousel-indicators">
						' . $str_slide_count . '
	                </ol>                       
					<div class="carousel-inner block-rounded" role="listbox">
						' . $str_slides . '
					</div>
					 <!--Controls-->                       
	                <a class="carousel-control" href="#gallery-slider" role="button" data-slide="prev">
	                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
	                    <span class="sr-only">Previous</span>
	                </a>
	                <a class="carousel-control" href="#gallery-slider" role="button" data-slide="next">
	                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
	                    <span class="sr-only">Next</span>
	                </a>
				</div>';
	}

} else {
	$no_gallery = true;
	$str_slide_counter = '<img src="' . $teacher_no_image_url . '" class="block-rounded slide-institute center-block img-responsive">';
}

////////////////////////////////////////////////Teacher Qualification//////////////////////////////////////////////
$teacher_qualification = '';
if ( isset( $teacher_qualification_data ) ) {
	foreach ( $teacher_qualification_data as $qualification_info ) {
		$teacher_qualification .= $qualification_info->qualification . ', ';
	}
	$teacher_qualification = rtrim( trim( $teacher_qualification ), ',' );
}


////////////////////////////////////////////////Teacher Achievements//////////////////////////////////////////////
$teacher_achievement = '';
if ( isset( $achievement_data ) ) {
	foreach ( $achievement_data as $achievement_info ) {
		$teacher_achievement .= $achievement_info->achievement . ', ';
	}
	$teacher_achievement = substr( $teacher_achievement, 0, - 3 );
	if ( $teacher_achievement == '' ) {
		$teacher_achievement .= 'No Achievements yet to show.';
	}
}

////////////////////////////////////////////////Course Details//////////////////////////////////////////////
$str_course_details = 'Course/Subjects details not available.';

if ( isset( $tuition_batch_data ) && ! empty( $tuition_batch_data ) ) {
	$str_course_details = '';
	$str_course_counter = 0;
	foreach ( $tuition_batch_data as $batch_data ) {
		$batch_id = $batch_data->tuition_batch_id;
		$course_days = $batch_data->course_days;
		$class_duration = $batch_data->class_duration;
		$course_length = $batch_data->course_length;
		$course_length_type = $batch_data->course_length_type;
		if ( $course_length == '' || $course_length == 0 ) {
			$course_length = "N/A";
			$course_length_type = " ";
		}
		$pdg_class_capacity = $batch_data->pdg_class_capacity;
		$about_the_course = $batch_data->about_course;
		if ( $about_the_course == '' || $about_the_course == 0 ) {
			$about_the_course = "No Information Available";

		}
		$weekdays_array = array (
			'Sunday',
			'Monday',
			'Tuesday',
			'Wednesday',
			'Thursday',
			'Friday',
			'Saturday',
		);

		$course_days = explode( ',', $course_days );

		$str_new_course_days = '';

		foreach ( $weekdays_array as $weekday ) {
			$rounded_border = '';
			if ( in_array( $weekday, $course_days ) ) {
				$rounded_border .= 'reverse';
			}
			$str_new_course_days .= '<div class="sunday days_items ' . $rounded_border . '" data-toggle="tooltip" title="' . $weekday . '"><span>' . $weekday[0] . '</span></div>';
		}

		$str_class_timing = 'N/A';

		if ( isset( $tuition_batch_class_timing_data ) ) {

			$str_class_timing = '';
			$class_timing_counter = 0;
			foreach ( $tuition_batch_class_timing_data as $class_timing ) {

				$class_timing_batch_id = $class_timing->tuition_batch_id;
				if ( $class_timing_batch_id == $batch_id ) {
					$str_class_timing .= $class_timing->class_timing;
				}
			}
		}

		$str_fees_types = 'N/A';
		if ( isset( $tutor_batch_fees_data ) ) {
			$str_fees_types = '';
			foreach ( $tutor_batch_fees_data as $fees_data ) {
				$fees_data_batch_id = $fees_data->tuition_batch_id;
				$fees_type = $fees_data->fees_type;
				$fees_amount = $fees_data->fees_amount;
				if ( $fees_data_batch_id == $batch_id ) {
					$str_fees_types .= $fees_amount . '/' . trim( preg_replace( '/(Per|Fee)/i', '', $fees_type ) ) . ' | ';
				}
			}
			$str_fees_types = substr( $str_fees_types, 0, - 3 );
		}

		$str_subjects = 'N/A';
		$str_subjects_panel_class = '';
		$str_class_type = 'N/A';
		if ( isset( $tuition_batch_subject_data ) ) {
			$subjects_array = array ();
			$str_subjects_panel_class_array = array ();
			$str_subject_type_array = array();
			foreach ( $tuition_batch_subject_data as $subject_data ) {
				$subject_batch_id = $subject_data->tuition_batch_id;
				$subject_name = $subject_data->subject_name;
				$subject_type = $subject_data->subject_type;
				if ( $subject_batch_id == $batch_id ) {
					if ( ! in_array( $subject_name, $subjects_array ) ) {
						$subjects_array[] = $subject_data->subject_name;
						$str_subjects_panel_class_array[] = trim( preg_replace( "/--+/i", "-", preg_replace( "![^a-z0-9]+!i", "-", strtolower( $subject_data->subject_name ) ) ), "-" );
					}
					if(!in_array($subject_type, $str_subject_type_array)) {
						$str_subject_type_array[] = $subject_type;
					}
				}
			}
			$str_subjects = implode( ' / ', $subjects_array );
			$str_subjects_panel_class = implode( ' ', $str_subjects_panel_class_array );
			$str_class_type = implode(', ', $str_subject_type_array);
		}

		//$str_locality = '';
		$locality_array = array ();
		if ( isset( $tutor_locality_data ) ) {
			$locality_address_student = '';
			foreach ( $tutor_locality_data as $locality_data ) {
				$locality_batch_id = $locality_data->tuition_batch_id;
				$locality_address = $locality_data->tuition_location_type;
				if ( $locality_address != "student" ) {
					$locality_address_student = '
						<div class="col-md-4">
							<div class="inside-block background-primary">
								<b>Address: </b><br />
								<span>' . $locality_data->address . '</span>
		            		</div>
						</div>';
				}
				if ( $locality_batch_id == $batch_id ) {
					$locality_array [] = array (
						'typeoflocation' => ucfirst( $locality_data->tuition_location_type ),
						'locality'       => $locality_data->locality,
						'locality_fees' => $locality_data->locality_fees,
					);
					/* Dead Code
					 * $str_locality .= '
							<div class="row">
								<div class="col-md-6">
									<div class="inside-block background-primary">
										<b>Type of Location: </b><br />
										<span>' . ucfirst( $locality_data->tuition_location_type ) . '</span>
				            		</div>
								</div>
								
								<div class="col-md-6">
									<div class="inside-block background-primary">
										<b>Locality: </b><br />
										<span>' . $locality_data->locality . '</span>
				            		</div>
								</div>								
							</div>
						';*/
				}
			}
		}

		$str_course_counter ++;
		$str_course_details .= '
			<!--subject_item-->
			<div class="panel panel-profile">
				<div class="panel-heading border-bottom-fix" role="button" data-toggle="collapse" 
					data-parent="#courses-panel-accordion" 
					href="#content_subject-'.$str_course_counter.'" 
					aria-expanded="true" 
					aria-controls="content_subject-'.$str_course_counter.'" 
					id="heading_subject-'.$str_course_counter.'">
					<h4 class="panel-title">
						<a class="accordion-toggle collapsed truncate no-select" data-toggle="collapse"
							data-parent="#courses-panel-accordion"
							href="#content_subject-'.$str_course_counter.'"
							aria-expanded="true"
							aria-controls="content_subject-' . $str_course_counter . '" >' . $str_subjects . '</a>
					</h4>
				</div>
				<div id="content_subject-'.$str_course_counter.'"
					class="panel-collapse collapse panel-body ' . $str_subjects_panel_class . '"
					role="tabpanel"
					aria-labelledby="heading_subject-' . $str_course_counter . '">
						<div class="form-group nomargin-top-10">
							'.ControllerTeacher::locality_wise_data( array (
								'locality_array'     => $locality_array,
								'fees_types'         => $str_fees_types,
								'class_timing'       => $str_class_timing,
								'class_capacity'     => $pdg_class_capacity,
								'class_duration'     => $class_duration,
								'course_length'      => $course_length,
								'course_length_type' => $course_length_type,
								'subjects'           => $str_subjects,
								'class_type'		 => $str_class_type
							)).'
							<div class="col-xs-12 course_item_block_wrapper padding-fix">
								<div class="course_item_block">
									<div class="inline text-bold item-desc">Teaching Days</div>
										:
										<div class="inline item">
											<div class="sm-inline text-center teaching_week_days">
												' . $str_new_course_days . '
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			<!--/subject_item-->';
	}
}

////////////////////////////////////////////////Teacher Recommendations//////////////////////////////////////////////
$logged_in_current_user_id = '';
$is_current_user_allowed_to_review = false;
if ( isset( $pdg_current_user ) ) {
	$logged_in_current_user_id = $pdg_current_user->ID;
	if ( isset( $current_user_db_info ) ) {
		$current_user_role = '';
		foreach ( $current_user_db_info as $cur_user ) {
			$current_user_role = $cur_user->user_role_name;
		}
		switch ( $current_user_role ) {
			case 'student':
				$is_current_user_allowed_to_review = true;
				break;
			case 'guardian':
				$is_current_user_allowed_to_review = true;
				break;
		}
	}
}

$recommend_data = '';
if ( isset( $recommendations_data ) ) {
	$recommend_data = $recommendations_data;
}

$recommendation_count = 0;
$current_user_has_recommended = false;
if ( ! empty( $recommend_data ) ) {
	foreach ( $recommend_data as $recommend ) {
		$recommend_to_user_id = $recommend->recommended_to;
		$recommended_by_user_id = $recommend->recommended_by;

		if ( $teacher_user_id == $recommend_to_user_id ) {
			if ( $recommended_by_user_id == $logged_in_current_user_id ) {
				$current_user_has_recommended = true;
			}
			$recommendation_count ++;
		}
	}
}

$str_recommendation_icon = '';

$str_recommendation_button = '';

if ( ( is_numeric( $logged_in_current_user_id ) && $logged_in_current_user_id > 0 ) && $is_current_user_allowed_to_review == true ) {

	if ( $current_user_has_recommended ) {
		$str_recommendation_button = '<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-recommended="yes" data-current_user="' . $logged_in_current_user_id . '" data-teacher_user_id="' . $teacher_user_id . '" data-loading-text="Processing..." data-recommendation_count="' . $recommendation_count . '"><i class="material-icons">&#xE8DB;</i>&nbsp;Unrecommend</button>';

	} else {
		$str_recommendation_button = '<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-recommended="no" data-current_user="' . $logged_in_current_user_id . '" data-teacher_user_id="' . $teacher_user_id . '" data-loading-text="Processing..." data-recommendation_count="' . $recommendation_count . '"><i class="material-icons">&#xE8DC;</i>&nbsp;Recommend</button>';
	}
} else {
	$str_recommendation_button = '
		<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-teacher_user_id="' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal">
			<i class="material-icons">&#xE8DC;</i>&nbsp;Recommend
		</button>';
}

////////////////////////////////////////////////Teacher Reviews and Ratings//////////////////////////////////////////////
$ratings_total = 0;
$rating_count = 0;
$current_user_has_rated = false;
$average_rating = 0;
if ( isset( $rating_data ) && ! empty( $rating_data ) ) {
	foreach ( $rating_data as $ratings ) {
		$rated_to_user_id = $ratings->tutor_institute_user_id;
		$rated_by_user_id = $ratings->reviewer_user_id;
		$star_rating = $ratings->overall_rating;
		$is_approved = $ratings->is_approved;

		if ( $teacher_user_id == $rated_to_user_id ) {
			if ( $rated_by_user_id == $logged_in_current_user_id ) {
				$current_user_has_rated = true;
			}
			if ( $is_approved == 'no' ) {
				continue;
			}
			$rating_count ++;
			$ratings_total += $star_rating;
		}
	}
}
$rating_icon_name = $average_rating;
$rating_stars_render_array = array ();
if ( $rating_count > 0 && $ratings_total > 0 ) {
	$average_rating = $ratings_total / $rating_count;
	$average_rating = PDGSearchContent::pdg_avg_rating( $average_rating );
	$rating_stars_render_array = PDGSearchContent::pdg_avg_rating_to_stars( round( $average_rating * 2, 0 ) / 2 );

	$rating_icon_name = str_replace( '.', '', $average_rating );
}

if ( empty( $rating_stars_render_array ) ) {
	$rating_stars_render_array = PDGSearchContent::pdg_avg_rating_to_stars( 0 );
}
$rating_stars_render = ControllerTeacher::rating_stars_render( $rating_stars_render_array );

$str_rating_icon = '';
$str_rating_button = '';
$review_btn = '';
$review_allowed = false;
if ( ( is_numeric( $logged_in_current_user_id ) && $logged_in_current_user_id > 0 ) && $is_current_user_allowed_to_review == true ) {
	if ( $current_user_has_rated ) {
		$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix disabled" disabled>Thanks for Rating!</button>';
	} else {
		$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#review-profile-modal"><i class="material-icons">&#xE560;</i>&nbsp;Write A Review </button>';
		$review_allowed = true;
	}
} else {
	$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal"><i class="material-icons">&#xE560;</i>&nbsp;Write A Review </button>';
}

////////////////////////////////////////////////Demo Class//////////////////////////////////////////////
$allow_demo_modal = false;
if ( $teacher_no_of_demo_classes == 0 ) {
	$allow_demo_modal = true;
	$take_a_demo_btn = '<button type="button" class="btn btn-raised try_a_demo_btn white-text blue-shade style-fix" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#takeademo-callback-profile-modal"><i class="material-icons">&#xE333;</i>&nbsp;Take a Demo</button>';
} else {
	$take_a_demo_btn = '<button type="button" class="btn btn-raised try_a_demo_btn white-text blue-shade style-fix disabled" disabled><i class="material-icons">&#xE333;</i>&nbsp;No Demo</button>';
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
$str_recommendations_html = ControllerTeacher::fn_return_reviews_content_new( $rating_data );
$callback_btn = '<button type="button" class="btn btn-raised req_a_callback_btn white-text green-shade style-fix" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#request-callback-profile-modal"><i class="material-icons">&#xE0B0;</i>&nbsp;Callback</button>';
$share_btn = '<button type="button" class="btn btn-raised share_profile_btn white-text blue-shade style-fix" data-toggle="modal" data-target="#share-profile-modal">Share&nbsp;<i class="material-icons">&#xE80D;</i></button>';

$locality_id_array = array ();
if ( isset( $tutor_locality_data ) && is_array( $tutor_locality_data ) ) {
	foreach ( $tutor_locality_data as $locality_dataObj ) {
		if ( is_numeric( $locality_dataObj->locality_id ) ) {
			$locality_id_array [ $locality_dataObj->locality_id ] = $locality_dataObj->locality_id;
		}
	}
}
$subjects_names_array = array ();
if ( isset( $tuition_batch_subject_data ) && is_array( $tuition_batch_subject_data ) ) {
	foreach ( $tuition_batch_subject_data as $subject_dataObj ) {
		$subjects_names_array[ md5( $subject_dataObj->subject_name ) ] = $subject_dataObj->subject_name;
	}
}
$locality_id_data = implode( ',', array_map( 'rawurlencode', $locality_id_array ) );
$subjects_names_data = implode( ',', array_map( 'rawurlencode', $subjects_names_array ) );
$profile_suggestion_tpl_file_content = $this->load_view( '/template/tpl-profile_suggestion-cards' );

////////////////////////////////////////////////Teacher Personal Information//////////////////////////////////////////////
$personal_information_profile_btn = '';
$is_private_profile = false;
if ( isset( $private_profile ) && $private_profile ) {
	$is_private_profile = true;
	$personal_information_profile_btn = '<button type="button" class="btn btn-sm btn-raised personal_information_profile_btn white-text blue-shade style-fix" data-toggle="modal" data-target="#personal-information-profile-modal" data-backdrop="static" data-keyboard="false">Personal Information</button>';
}


////////////////////////////////////////////////Teacher Notes //////////////////////////////////////////////
$is_current_user_admin = current_user_can('manage_options') || current_user_can('view_teacher_institute_personal_info');
$str_teacher_notes = '';
$str_show_notes_button = '';
if($is_current_user_admin) {
	$str_show_notes_button = '
		<button type="button" class="btn btn-sm btn-raised white-text blue-shade style-fix" id="cmd_show_notes" data-toggle="modal" data-target="#modal_teachers_notes" data-backdrop="static" data-keyboard="false">
   	 		<i class="fa fa-info-circle" aria-hidden="true"></i> Notes
   	 	</button>
	';
	if(isset($teacher_notes)) {
		$str_teacher_notes = $teacher_notes;
	}
}

////////////////////////////////////////////////Teacher Internal Rating//////////////////////////////////////////////
$is_current_user_admin = current_user_can('manage_options') || current_user_can('view_teacher_institute_personal_info');
$str_teacher_internal_rating = '';
$str_show_internal_rating_button = '';
if($is_current_user_admin) {
	$str_show_internal_rating_button = '
		<button type="button" class="notes_model" id="cmd_show_internal_rating" data-toggle="modal" data-target="#modal_teachers_internal_rating" data-backdrop="static" data-keyboard="false">
   	 		<i class="fa fa-info-circle" aria-hidden="true"></i> Internal Rating
   	 	</button>
	';
	if(isset($teacher_internal_rating)) {
		$str_teacher_internal_rating = $teacher_internal_rating;
	}
}

$str_schools_teaches = 'School Information not available!';
if ( isset( $tuition_batch_data ) && ! empty( $tuition_batch_data ) ) {
	$str_schools_teaches = ControllerTeacher::get_batch_school_information($tuition_batch_data);
}




////////////////////////////////////////////////Rating Details///////////////////////



$rating_details = '';

$rating_details = ControllerTeacher::fn_get_lastInsertedRating( $teacher_user_id );



$punctuality_rating = '';

$responsiveness_rating = '';

$behaviour_rating = '';

$communication_skills_rating = '';



if ( isset( $rating_details ) && !empty( $rating_details ) ) {

	$punctuality_rating = $rating_details[0];

	$responsiveness_rating = $rating_details[1];

	$behaviour_rating = $rating_details[2];

	$communication_skills_rating = $rating_details[3];

}



////////////////////////////////////////////////Rating Details///////////////////////



///////////////////////////////////////////////Classes Details///////////////////////



$str_classes_details = '';

$classes_details_data = ControllerTeacher::fn_get_classesDetails( $teacher_user_id );



$classes_accepted = '';

$classes_provided = '';

$demo_converted = '';

$demo_provided = '';

$classes_ongoing = '';



if ( isset( $classes_details_data ) && !empty( $classes_details_data ) ) {

	$classes_accepted = $classes_details_data[0];

	$classes_provided = $classes_details_data[1];

	$demo_converted = $classes_details_data[2];

	$demo_provided = $classes_details_data[3];

	$classes_ongoing = $classes_details_data[4];

}



///////////////////////////////////////////////Classes Details///////////////////////



//////////////////////////// payment plan //////////////////////////////////////////



$selected_payment_plan = '';



$selected_payment_plan = ControllerTeacher::fn_get_lastInsertedPaymentPlan( $teacher_user_id );

//print($selected_payment_plan);

//exit();



//////////////////////////// payment plan //////////////////////////////////////////



////////////////////////////////////////////////Course Details//////////////////////////////////////////////

$str_course_details = 'Course/Subjects details not available.';



if ( isset( $tuition_batch_data ) && ! empty( $tuition_batch_data ) ) {

	$str_course_details = '';

	$str_course_counter = 0;

	foreach ( $tuition_batch_data as $batch_data ) {

		$batch_id = $batch_data->tuition_batch_id;

		$course_days = $batch_data->course_days;

		$class_duration = $batch_data->class_duration;

		$course_length = $batch_data->course_length;

		$course_length_type = $batch_data->course_length_type;

		if ( $course_length == '' || $course_length == 0 ) {

			$course_length = "N/A";

			$course_length_type = " ";

		}

		$pdg_class_capacity = $batch_data->pdg_class_capacity;

		$about_the_course = $batch_data->about_course;

		if ( $about_the_course == '' || $about_the_course == 0 ) {

			$about_the_course = "No Information Available";



		}

		$weekdays_array = array (

			'Sunday',

			'Monday',

			'Tuesday',

			'Wednesday',

			'Thursday',

			'Friday',

			'Saturday',

		);



		$course_days = explode( ',', $course_days );



		$str_new_course_days = '';



		foreach ( $weekdays_array as $weekday ) {

			$rounded_border = '';

			if ( in_array( $weekday, $course_days ) ) {

				$rounded_border .= 'reverse';

			}

			$str_new_course_days .= '<div class="sunday days_items ' . $rounded_border . '" data-toggle="tooltip" title="' . $weekday . '"><span>' . $weekday[0] . '</span></div>';

		}



		$str_class_timing = 'N/A';



		if ( isset( $tuition_batch_class_timing_data ) ) {



			$str_class_timing = '';

			$class_timing_counter = 0;

			foreach ( $tuition_batch_class_timing_data as $class_timing ) {



				$class_timing_batch_id = $class_timing->tuition_batch_id;

				if ( $class_timing_batch_id == $batch_id ) {

					$str_class_timing .= $class_timing->class_timing;

				}

			}

		}



		$str_fees_types = 'N/A';

		if ( isset( $tutor_batch_fees_data ) ) {

			$str_fees_types = '';

			foreach ( $tutor_batch_fees_data as $fees_data ) {

				$fees_data_batch_id = $fees_data->tuition_batch_id;

				$fees_type = $fees_data->fees_type;

				$fees_amount = $fees_data->fees_amount;

				if ( $fees_data_batch_id == $batch_id ) {

					$str_fees_types .= $fees_amount . '/' . trim( preg_replace( '/(Per|Fee)/i', '', $fees_type ) ) . ' | ';

				}

			}

			$str_fees_types = substr( $str_fees_types, 0, - 3 );

		}



		$str_subjects = 'N/A';

		$str_subjects_panel_class = '';

		$str_class_type = 'N/A';

		if ( isset( $tuition_batch_subject_data ) ) {

			$subjects_array = array ();

			$str_subjects_panel_class_array = array ();

			$str_subject_type_array = array();

			foreach ( $tuition_batch_subject_data as $subject_data ) {

				$subject_batch_id = $subject_data->tuition_batch_id;

				$subject_name = $subject_data->subject_name;

				$subject_type = $subject_data->subject_type;

				if ( $subject_batch_id == $batch_id ) {

					if ( ! in_array( $subject_name, $subjects_array ) ) {

						$subjects_array[] = $subject_data->subject_name;

						$str_subjects_panel_class_array[] = trim( preg_replace( "/--+/i", "-", preg_replace( "![^a-z0-9]+!i", "-", strtolower( $subject_data->subject_name ) ) ), "-" );

					}

					if(!in_array($subject_type, $str_subject_type_array)) {

						$str_subject_type_array[] = $subject_type;

					}

				}

			}

			$str_subjects = implode( ' / ', $subjects_array );

			$str_subjects_panel_class = implode( ' ', $str_subjects_panel_class_array );

			$str_class_type = implode(', ', $str_subject_type_array);

		}



		//$str_locality = '';

		$locality_array = array ();

		if ( isset( $tutor_locality_data ) ) {

			$locality_address_student = '';

			foreach ( $tutor_locality_data as $locality_data ) {

				$locality_batch_id = $locality_data->tuition_batch_id;

				$locality_address = $locality_data->tuition_location_type;

				if ( $locality_address != "student" ) {

					$locality_address_student = '

						<div class="col-md-4">

							<div class="inside-block background-primary">

								<b>Address: </b><br />

								<span>' . $locality_data->address . '</span>

		            		</div>

						</div>';

				}

				if ( $locality_batch_id == $batch_id ) {

					$locality_array [] = array (

						'typeoflocation' => ucfirst( $locality_data->tuition_location_type ),

						'locality'       => $locality_data->locality,

						'locality_fees' => $locality_data->locality_fees,

					);

					/* Dead Code

					 * $str_locality .= '

							<div class="row">

								<div class="col-md-6">

									<div class="inside-block background-primary">

										<b>Type of Location: </b><br />

										<span>' . ucfirst( $locality_data->tuition_location_type ) . '</span>

				            		</div>

								</div>

								

								<div class="col-md-6">

									<div class="inside-block background-primary">

										<b>Locality: </b><br />

										<span>' . $locality_data->locality . '</span>

				            		</div>

								</div>								

							</div>

						';*/

				}

			}

		}



		$str_course_counter ++;

		$str_course_details .= '

			<!--subject_item-->

			<div class="panel panel-profile">

				<div class="panel-heading border-bottom-fix" role="button" data-toggle="collapse" 

					data-parent="#courses-panel-accordion" 

					href="#content_subject-'.$str_course_counter.'" 

					aria-expanded="true" 

					aria-controls="content_subject-'.$str_course_counter.'" 

					id="heading_subject-'.$str_course_counter.'">

					<h4 class="panel-title">

						<a class="accordion-toggle collapsed truncate no-select" data-toggle="collapse"

							data-parent="#courses-panel-accordion"

							href="#content_subject-'.$str_course_counter.'"

							aria-expanded="true"

							aria-controls="content_subject-' . $str_course_counter . '" >' . $str_subjects . '</a>

					</h4>

				</div>

				<div id="content_subject-'.$str_course_counter.'"

					class="panel-collapse collapse panel-body ' . $str_subjects_panel_class . '"

					role="tabpanel"

					aria-labelledby="heading_subject-' . $str_course_counter . '">

						<div class="form-group nomargin-top-10">

							'.ControllerTeacher::locality_wise_data( array (

								'locality_array'     => $locality_array,

								'fees_types'         => $str_fees_types,

								'class_timing'       => $str_class_timing,

								'class_capacity'     => $pdg_class_capacity,

								'class_duration'     => $class_duration,

								'course_length'      => $course_length,

								'course_length_type' => $course_length_type,

								'subjects'           => $str_subjects,

								'class_type'		 => $str_class_type

							)).'

							<div class="col-xs-12 course_item_block_wrapper padding-fix">

								<div class="course_item_block">

									<div class="inline text-bold item-desc">Teaching Days</div>

										:

										<div class="inline item">

											<div class="sm-inline text-center teaching_week_days">

												' . $str_new_course_days . '

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

				</div>

			<!--/subject_item-->';

	}

}



////////////////////////////////////////////////Teacher Recommendations//////////////////////////////////////////////

$logged_in_current_user_id = '';

$is_current_user_allowed_to_review = false;

if ( isset( $pdg_current_user ) ) {

	$logged_in_current_user_id = $pdg_current_user->ID;

	if ( isset( $current_user_db_info ) ) {

		$current_user_role = '';

		foreach ( $current_user_db_info as $cur_user ) {

			$current_user_role = $cur_user->user_role_name;

		}

		switch ( $current_user_role ) {

			case 'student':

				$is_current_user_allowed_to_review = true;

				break;

			case 'guardian':

				$is_current_user_allowed_to_review = true;

				break;

		}

	}

}



$recommend_data = '';

if ( isset( $recommendations_data ) ) {

	$recommend_data = $recommendations_data;

}



$recommendation_count = 0;

$current_user_has_recommended = false;

if ( ! empty( $recommend_data ) ) {

	foreach ( $recommend_data as $recommend ) {

		$recommend_to_user_id = $recommend->recommended_to;

		$recommended_by_user_id = $recommend->recommended_by;



		if ( $teacher_user_id == $recommend_to_user_id ) {

			if ( $recommended_by_user_id == $logged_in_current_user_id ) {

				$current_user_has_recommended = true;

			}

			$recommendation_count ++;

		}

	}

}



$str_recommendation_icon = '';



$str_recommendation_button = '';



if ( ( is_numeric( $logged_in_current_user_id ) && $logged_in_current_user_id > 0 ) && $is_current_user_allowed_to_review == true ) {



	if ( $current_user_has_recommended ) {

		$str_recommendation_button = '<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-recommended="yes" data-current_user="' . $logged_in_current_user_id . '" data-teacher_user_id="' . $teacher_user_id . '" data-loading-text="Processing..." data-recommendation_count="' . $recommendation_count . '"><i class="material-icons">&#xE8DB;</i>&nbsp;Unrecommend</button>';



	} else {

		$str_recommendation_button = '<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-recommended="no" data-current_user="' . $logged_in_current_user_id . '" data-teacher_user_id="' . $teacher_user_id . '" data-loading-text="Processing..." data-recommendation_count="' . $recommendation_count . '"><i class="material-icons">&#xE8DC;</i>&nbsp;Recommend</button>';

	}

} else {

	$str_recommendation_button = '

		<button type="button" class="btn btn-raised btn-sm recommend_btn white-text red-shade style-fix" data-teacher_user_id="' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal">

			<i class="material-icons">&#xE8DC;</i>&nbsp;Recommend

		</button>';

}



////////////////////////////////////////////////Teacher Reviews and Ratings//////////////////////////////////////////////

$ratings_total = 0;

$rating_count = 0;

$current_user_has_rated = false;

$average_rating = 0;

if ( isset( $rating_data ) && ! empty( $rating_data ) ) {

	foreach ( $rating_data as $ratings ) {

		$rated_to_user_id = $ratings->tutor_institute_user_id;

		$rated_by_user_id = $ratings->reviewer_user_id;

		$star_rating = $ratings->overall_rating;

		$is_approved = $ratings->is_approved;



		if ( $teacher_user_id == $rated_to_user_id ) {

			if ( $rated_by_user_id == $logged_in_current_user_id ) {

				$current_user_has_rated = true;

			}

			if ( $is_approved == 'no' ) {

				continue;

			}

			$rating_count ++;

			$ratings_total += $star_rating;

		}

	}

}

$rating_icon_name = $average_rating;

$rating_stars_render_array = array ();

if ( $rating_count > 0 && $ratings_total > 0 ) {

	$average_rating = $ratings_total / $rating_count;

	$average_rating = PDGSearchContent::pdg_avg_rating( $average_rating );

	$rating_stars_render_array = PDGSearchContent::pdg_avg_rating_to_stars( round( $average_rating * 2, 0 ) / 2 );



	$rating_icon_name = str_replace( '.', '', $average_rating );

}



if ( empty( $rating_stars_render_array ) ) {

	$rating_stars_render_array = PDGSearchContent::pdg_avg_rating_to_stars( 0 );

}

$rating_stars_render = ControllerTeacher::rating_stars_render( $rating_stars_render_array );



$str_rating_icon = '';

$str_rating_button = '';

$review_btn = '';

$review_allowed = false;

if ( ( is_numeric( $logged_in_current_user_id ) && $logged_in_current_user_id > 0 ) && $is_current_user_allowed_to_review == true ) {

	if ( $current_user_has_rated ) {

		$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix disabled" disabled>Thanks for Rating!</button>';

	} else {

		$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix" data-current_user = "' . $logged_in_current_user_id . '" data-teacher_user_id = "' . $teacher_user_id . '" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#review-profile-modal"><i class="material-icons">&#xE560;</i>&nbsp;Write A Review </button>';

		$review_allowed = true;

	}

} else {

	$review_btn = '<button type="button" class="btn btn-raised write_review_btn white-text red-shade style-fix" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#no_login_review-profile-modal"><i class="material-icons">&#xE560;</i>&nbsp;Write A Review </button>';

}



////////////////////////////////////////////////Demo Class//////////////////////////////////////////////

$allow_demo_modal = false;

if ( $teacher_no_of_demo_classes == 0 ) {

	$allow_demo_modal = true;

	$take_a_demo_btn = '<button type="button" class="btn btn-raised try_a_demo_btn white-text blue-shade style-fix" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#takeademo-callback-profile-modal"><i class="material-icons">&#xE333;</i>&nbsp;Take a Demo</button>';

} else {

	$take_a_demo_btn = '<button type="button" class="btn btn-raised try_a_demo_btn white-text blue-shade style-fix disabled" disabled><i class="material-icons">&#xE333;</i>&nbsp;No Demo</button>';

}



///////////////////////////////////////////////////////////////////////////////////////////////////////

$str_recommendations_html = ControllerTeacher::fn_return_reviews_content_new( $rating_data );

$callback_btn = '<button type="button" class="btn btn-raised req_a_callback_btn white-text green-shade style-fix" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#request-callback-profile-modal"><i class="material-icons">&#xE0B0;</i>&nbsp;Callback</button>';

$share_btn = '<button type="button" class="btn btn-raised share_profile_btn white-text blue-shade style-fix" data-toggle="modal" data-target="#share-profile-modal">Share&nbsp;<i class="material-icons">&#xE80D;</i></button>';



$locality_id_array = array ();

if ( isset( $tutor_locality_data ) && is_array( $tutor_locality_data ) ) {

	foreach ( $tutor_locality_data as $locality_dataObj ) {

		if ( is_numeric( $locality_dataObj->locality_id ) ) {

			$locality_id_array [ $locality_dataObj->locality_id ] = $locality_dataObj->locality_id;

		}

	}

}

$subjects_names_array = array ();

if ( isset( $tuition_batch_subject_data ) && is_array( $tuition_batch_subject_data ) ) {

	foreach ( $tuition_batch_subject_data as $subject_dataObj ) {

		$subjects_names_array[ md5( $subject_dataObj->subject_name ) ] = $subject_dataObj->subject_name;

	}

}

$locality_id_data = implode( ',', array_map( 'rawurlencode', $locality_id_array ) );

$subjects_names_data = implode( ',', array_map( 'rawurlencode', $subjects_names_array ) );

$profile_suggestion_tpl_file_content = $this->load_view( '/template/tpl-profile_suggestion-cards' );



////////////////////////////////////////////////Teacher Personal Information//////////////////////////////////////////////

$personal_information_profile_btn = '';

$is_private_profile = false;

if ( isset( $private_profile ) && $private_profile ) {

	$is_private_profile = true;

	$personal_information_profile_btn = '<button type="button" class="btn btn-sm btn-raised personal_information_profile_btn white-text blue-shade style-fix" data-toggle="modal" data-target="#personal-information-profile-modal" data-backdrop="static" data-keyboard="false">Personal Information</button>';

}





////////////////////////////////////////////////Teacher Notes //////////////////////////////////////////////

$is_current_user_admin = current_user_can('manage_options') || current_user_can('view_teacher_institute_personal_info');

$str_teacher_notes = '';

$str_show_notes_button = '';

if($is_current_user_admin) {

	$str_show_notes_button = '

		<button type="button" class="btn btn-sm btn-raised white-text blue-shade style-fix" id="cmd_show_notes" data-toggle="modal" data-target="#modal_teachers_notes" data-backdrop="static" data-keyboard="false">

   	 		<i class="fa fa-info-circle" aria-hidden="true"></i> Notes

   	 	</button>

	';

	if(isset($teacher_notes)) {

		$str_teacher_notes = $teacher_notes;

	}

}



////////////////////////////////////////////////Teacher Internal Rating//////////////////////////////////////////////

$is_current_user_admin = current_user_can('manage_options') || current_user_can('view_teacher_institute_personal_info');

$str_teacher_internal_rating = '';

$str_show_internal_rating_button = '';

if($is_current_user_admin) {

	$str_show_internal_rating_button = '

		<button type="button" class="notes_model" id="cmd_show_internal_rating" data-toggle="modal" data-target="#modal_teachers_internal_rating" data-backdrop="static" data-keyboard="false">

   	 		<i class="fa fa-info-circle" aria-hidden="true"></i> Internal Rating

   	 	</button>

	';

	if(isset($teacher_internal_rating)) {

		$str_teacher_internal_rating = $teacher_internal_rating;

	}

}



$str_schools_teaches = 'School Information not available!';

if ( isset( $tuition_batch_data ) && ! empty( $tuition_batch_data ) ) {

	$str_schools_teaches = ControllerTeacher::get_batch_school_information($tuition_batch_data);

}