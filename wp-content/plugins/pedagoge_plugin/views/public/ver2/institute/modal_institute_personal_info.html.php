<?php if ( isset( $private_profile ) && $private_profile ) { ?>
	<!--Modal Institute Information | Review-->
	<div id="institute-information-profile-modal" data-callback-name="profile_institute_information_callback" class="callback_init modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Institute Information</h4>
				</div>
				<form id="profile_institute_information_callback" class="bs-component">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Email:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $user_email; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Contact Person:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $contact_person_name; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Contact Number:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $mobile_no; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Alternate Number:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $institute_alternate_no; ?></h5>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-4">
									<label>Address:</label>
								</div>
								<div class="col-md-8">
									<h5><?= $institute_address_of_correspondence . ', ' . $address_city; ?></h5>
								</div>
							</div>
						</div>						
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--/Modal Institute Information | Review-->
<?php } ?>