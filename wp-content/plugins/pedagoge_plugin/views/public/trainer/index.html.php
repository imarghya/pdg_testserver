<?php
$str_trainer_data_verified              = '';
$trainer_id                             = '';
$trainer_user_id                        = '';
$trainer_first_name                     = '';
$trainer_last_name                      = '';
$profile_heading                        = '';
$other_qualification                    = '';
$past_experiences                       = '';
$other_domains_technical                = '';
$other_domains_nontechnical             = '';
$trainer_domain_list_trainer_xp_id      = '';
$trainer_domain_list_trainer_experience = '';
$trainer_domain_list_domain             = '';
$trainer_domain_list_domain_type        = '';
$user_email                             = '';
$alternate_contact_no                   = '';
$primary_contact_no                     = '';
$trainer_birth_year                     = '';
$trainer_birth_month                    = '';
$trainer_birth_day                      = '';
$trainer_city_id                        = '';
$correspondance_address                 = '';
$total_xp_id                            = '';
$trainer_frequency                      = '';
$training_level                         = '';
$trainer_keywords                       = '';
$company_logo_options                   = '';
$training_reference_items               = '';
$user_video_options                     = array ();
$sample_course_material_path            = '';
$cv_path                                = '';
$companies_logos_array                  = array ();
$trainer_references_contact_no1         = '';
$trainer_references_contact_name1       = '';
$trainer_references_contact_no2         = '';
$trainer_references_contact_name2       = '';
$trainer_references_contact_no3         = '';
$trainer_references_contact_name3       = '';
$trainer_references_contact_no4         = '';
$trainer_references_contact_name4       = '';
$trainer_references_contact_no5         = '';
$trainer_references_contact_name5       = '';
$trainer_references_contact_no6         = '';
$trainer_references_contact_name6       = '';
$trainer_name                           = '';
$gender                                 = '';
$profile_image_path                     = 'storage/uploads/images/';
$trainer_profile_image_url              = '';
$trainer_no_image_url                   = '';
$trainer_dob                            = '';
$companies_served                       = '';

if ( isset( $companies_logos ) && ! empty( $companies_logos ) ) {
	foreach ( $companies_logos as $company_data ) {
		if ( $company_data->company_name === null || trim( $company_data->company_name ) === '' ) {
			continue;
		}
		$companyName                                  = trim( $company_data->company_name );
		$companies_logos_array[ md5( $companyName ) ] = $companyName;
	}
}

$is_trainer_profile_private = false;
if ( isset( $this->app_data['is_trainer_profile_private'] ) && $this->app_data['is_trainer_profile_private'] ) {
	$is_trainer_profile_private = true;
}

if ( isset( $view_trainer_data ) && is_array( $view_trainer_data ) ) {
	foreach ( $view_trainer_data as $view_trainer_info ) {

		$trainer_id                             = $view_trainer_info->trainer_id;
		$user_email                             = $view_trainer_info->user_email;
		$trainer_verified                       = $view_trainer_info->verified;
		$gender                                 = $view_trainer_info->gender;
		$str_trainer_data_verified              = $trainer_verified == 'yes' ? $str_trainer_data_verified : '';
		$first_name                             = $view_trainer_info->first_name;
		$last_name                              = $view_trainer_info->last_name;
		$trainer_name                           = $first_name . ' ' . $last_name;
		$trainer_domain_list_array[]            = array (
			'trainer_domain_list_domain_type' => $view_trainer_info->trainer_domain_list_domain,
			'trainer_domain_list_domain'      => $view_trainer_info->trainer_domain_list_domain_type
		);
		$profile_heading                        = $view_trainer_info->trainer_heading;
		$past_experiences                       = $view_trainer_info->trainer_xp_comments;
		$trainer_user_id                        = $view_trainer_info->user_id;
		$total_xp_id                            = $view_trainer_info->total_xp_id;
		$correspondance_address                 = $view_trainer_info->current_address;
		$primary_contact_no                     = $view_trainer_info->mobile_no;
		$alternate_contact_no                   = $view_trainer_info->alternative_contact_no;
		$trainer_frequency                      = $view_trainer_info->trainer_frequency;
		$trainer_keywords                       = $view_trainer_info->trainer_keywords;
		$training_level                         = $view_trainer_info->training_level;
		$date_of_birth                          = $view_trainer_info->date_of_birth;
		$other_qualification                    = $view_trainer_info->other_qualification;
		$other_domains_technical                = $view_trainer_info->other_domains_technical;
		$other_domains_nontechnical             = $view_trainer_info->other_domains_nontechnical;
		$trainer_domain_list_trainer_xp_id      = $view_trainer_info->trainer_domain_list_trainer_xp_id;
		$trainer_domain_list_trainer_experience = $view_trainer_info->trainer_domain_list_trainer_experience;
		$trainer_domain_list_domain             = $view_trainer_info->trainer_domain_list_domain;
		$trainer_domain_list_domain_type        = $view_trainer_info->trainer_domain_list_domain_type;
		$sample_course_material_path            = $view_trainer_info->sample_course_material_path;
		$cv_path                                = $view_trainer_info->cv_path;
		$trainer_birth_year                     = substr( $date_of_birth, 0, 4 );
		$trainer_birth_month                    = substr( $date_of_birth, 5, 2 );
		$trainer_birth_day                      = substr( $date_of_birth, 8, 2 );
	}
}
$companies_served = beautifyValues( implode( ", ", $companies_logos_array ) );
//frequency
switch ( $trainer_frequency ) {
	case 'fulltime':
		$trainer_frequency = 'Full time';
		break;
	case 'parttime':
		$trainer_frequency = 'Part time';
		break;
}

//Hierarchy
switch ( $training_level ) {
	case 'beginner':
		$training_level = 'Beginner';
		break;
	case 'intermediate':
		$training_level = 'Intermediate';
		break;
	case 'advanced':
		$training_level = 'Advanced';
		break;
}


//Profile pic
$trainer_profile_image_path = PEDAGOGE_PLUGIN_DIR . $profile_image_path . $trainer_user_id . '/profile.jpg';
if ( file_exists( $trainer_profile_image_path ) ) {
	$file_timestamp            = date( 'U', filemtime( $trainer_profile_image_path ) );
	$trainer_profile_image_url = PEDAGOGE_PLUGIN_URL . '/' . $profile_image_path . $trainer_user_id . '/profile.jpg?' . $file_timestamp;
} else {
	$trainer_profile_image_url = PEDAGOGE_ASSETS_URL . '/images/sample.jpg';
}

//Qualification
$str_grad_qualification_options      = '';
$str_experience_options              = '';
$str_post_grad_qualification_options = '';
$str_professional_courses            = '';

$trainer_qualification_modes_array = array ();
if ( isset( $trainer_qualification_modes ) && is_array( $trainer_qualification_modes ) && ! empty( $trainer_qualification_modes ) ) {
	foreach ( $trainer_qualification_modes as $qualification_mode ) {
		$qualification_mode_id = $qualification_mode->qualification_id;
		if ( ! in_array( $qualification_mode_id, $trainer_qualification_modes_array ) ) {
			$trainer_qualification_modes_array[] = $qualification_mode_id;
		}
	}
}

if ( isset( $qualification_data ) && is_array( $qualification_data ) && ! empty( $qualification_data ) ) {
	foreach ( $qualification_data as $qualification ) {
		$qualification_name = $qualification->qualification;
		$qualification_id   = $qualification->qualification_id;
		$qualification_type = $qualification->qualification_type;
		$str_selected       = '';
		switch ( $qualification_type ) {
			case 'bachelor':
				if ( in_array( $qualification_id, $trainer_qualification_modes_array ) ) {
					$str_grad_qualification_options = $qualification_name;
				}
				break;
			case 'postgrad':
				if ( in_array( $qualification_id, $trainer_qualification_modes_array ) ) {
					$str_post_grad_qualification_options = $qualification_name;
				}
				break;
			case 'professional_course':
				if ( in_array( $qualification_id, $trainer_qualification_modes_array ) ) {
					$str_professional_courses .= ', ' . $qualification_name;
				}
				break;
		}

	}
}
$str_professional_courses = beautifyValues( $str_professional_courses );
$other_qualification      = beautifyValues( $other_qualification );

//trainer domain list
$trainer_domain_nontechnical_selected_array = array ();
$trainer_domain_technical_selected_array    = array ();

if ( isset( $trainer_domain_list ) && is_array( $trainer_domain_list ) && ! empty( $trainer_domain_list ) ) {
	foreach ( $trainer_domain_list as $trainer_domain_list_item ) {
		$trainer_domain_type = $trainer_domain_list_item->domain_type;
		$trainer_domain_id   = $trainer_domain_list_item->domain_id;
		$str_selected        = '';
		switch ( $trainer_domain_type ) {
			case  'technical':
				$trainer_domain_technical_selected_array[] = $trainer_domain_id;
				break;
			case 'nontechnical':
				$trainer_domain_nontechnical_selected_array[] = $trainer_domain_id;
				break;
		}
	}
}

//trainer video
if ( isset( $user_video_result ) && is_array( $user_video_result ) && ! empty( $user_video_result ) ) {
	foreach ( $user_video_result as $user_video_value ) {
		$user_video_options[] = $user_video_value->video_url;
	}
}

//domain training
$str_domain_technical_options    = '';
$str_domain_nontechnical_options = '';
if ( isset( $domain_list ) && is_array( $domain_list ) && ! empty( $domain_list ) ) {
	foreach ( $domain_list as $domains ) {
		$domain_type  = $domains->domain_type;
		$domain_id    = $domains->domain_id;
		$domain       = $domains->domain;
		$str_selected = '';
		switch ( $domain_type ) {
			case  'technical':
				if ( in_array( $domain_id, $trainer_domain_technical_selected_array ) ) {
					$str_domain_technical_options .= ', ' . $domain;
				}
				break;
			case 'nontechnical':
				if ( in_array( $domain_id, $trainer_domain_nontechnical_selected_array ) ) {
					$str_domain_nontechnical_options .= ', ' . $domain;
				}
				break;
		}
	}
}
$str_domain_technical_options    = beautifyValues( $str_domain_technical_options . ", " . $other_domains_technical );
$str_domain_nontechnical_options = beautifyValues( $str_domain_nontechnical_options . ", " . $other_domains_nontechnical );
$trainer_keywords                = beautifyValues( $trainer_keywords );

//experience
if ( isset( $training_experience ) && is_array( $training_experience ) && ! empty( $training_experience ) ) {
	foreach ( $training_experience as $experience ) {
		$experience_id   = $experience->trainer_xp_id;
		$experience_name = $experience->trainer_experience;
		$str_selected    = '';

		if ( ! empty( $total_xp_id ) && $experience_id == $total_xp_id ) {
			$str_experience_options = $experience_name;
		}
	}
}

$trainer_references_contact_array = array ();
if ( isset( $view_pdg_trainer_references ) && is_array( $view_pdg_trainer_references ) && ! empty( $view_pdg_trainer_references ) ) {

	foreach ( $view_pdg_trainer_references as $view_pdg_trainer_reference_item ) {
		if ( $view_pdg_trainer_reference_item ) {
			$trainer_references_contact_array[ $view_pdg_trainer_reference_item->reference_type_id ] = array (
				'contact_no'   => $view_pdg_trainer_reference_item->contact_no,
				'contact_name' => $view_pdg_trainer_reference_item->contact_name
			);
		}
	}
}

// remove extra commas and maintain a white space after each comma
function beautifyValues( $values ) {
	return trim( preg_replace( '/(?<!\d),|,(?!\d{3})/', ', ', $values ), ", " );
}

$trainer_dob = $trainer_birth_year . "/" . $trainer_birth_month . "/" . $trainer_birth_day;

/*


$trainer_city_id
$correspondance_address
$total_xp_id
$trainer_frequency
$training_level
$trainer_keywords
$company_logo_options
$training_reference_items
$user_video_options
$sample_course_material_path
$cv_path
$companies_logos_array
$trainer_references_contact_no1
$trainer_references_contact_name1
$trainer_references_contact_no2
$trainer_references_contact_name2
$trainer_references_contact_no3
$trainer_references_contact_name3
$trainer_references_contact_no4
$trainer_references_contact_name4
$trainer_references_contact_no5
$trainer_references_contact_name5
$trainer_references_contact_no6
$trainer_references_contact_name6
 */

?>
<section
	class="site-section site-section-light site-section-top site-section-bottom themed-background-dark change-background">

</section>
<div class="container"><br>
	<div class="row" style="padding-left: 1.35%;">
		<div class="col-md-3 col-xs-12 profile-txt block-institute">
			<div class="row"><br>
				<div class="col-md-6 col-xs-6">
					<?php echo $str_trainer_data_verified; ?>
				</div>
				<div class="col-md-6 col-xs-6 text-right">
				</div>
				<br>
			</div>

			<div class="row">
				<div class="col-md-8 col-md-offset-2 ">
					<img src="<?php echo $trainer_profile_image_url; ?>"
					     class="img-responsive img-display ">
					<h4 class="text-center"><?php echo $is_trainer_profile_private ? $trainer_name : '' ?></h4>
					<p class="text-center"><i class="fa fa-black-tie"></i>&nbsp;&nbsp;Trainer</p>
					<p class="text-center"><?php echo $is_trainer_profile_private ? $user_email : '' ?></p>
					<p class="text-center"><?php echo $is_trainer_profile_private ? $primary_contact_no : '' ?></p>
					<p class="text-center"><?php echo $is_trainer_profile_private ? $alternate_contact_no : '' ?></p>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<!-- course details starts here -->
			<div id="trainer_course_details" class="block background-primary shadow block-fixed-height">
				<!-- Grids Content Title -->
				<h4><b>Domains</b></h4>

				<!-- END Grids Content Title -->
				<?php if ( $str_grad_qualification_options ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-primary">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Graduation
									: </b><?php echo $str_grad_qualification_options; ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<?php if ( $str_post_grad_qualification_options ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-primary">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Post Graduation
									: </b><?php echo $str_post_grad_qualification_options; ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<?php if ( $str_professional_courses ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-primary">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Professional Qualifications
									: </b><?php echo $str_professional_courses; ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<?php if ( $other_qualification ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-primary">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Other Qualifications
									: </b><?php echo $other_qualification; ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<?php if ( $str_domain_technical_options ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-primary">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Technical
									: </b><?php echo $str_domain_technical_options; ?>
							</div>
						</div>
					</div>
				<?php } ?>


				<?php if ( $str_domain_nontechnical_options ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-primary">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Non Technical
									: </b><?php echo $str_domain_nontechnical_options; ?>
							</div>
						</div>
					</div>
				<?php } ?>
				<!-- END Grids Content Content -->
			</div>
			<!-- course details ends here -->
		</div>
	</div>
	<br>

	<div class="block ">
		<!-- Block Title -->
		<div class="block-title block-top-rounded">
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<h2><?php echo $profile_heading; ?></h2>
				</div>
				<div class="col-md-6 col-xs-6">
				</div>
			</div>
		</div>
		<!-- END Block Title -->

		<!-- Block Content -->
		<p>
			<strong>Past Experiences:</strong>
			<?php echo $past_experiences; ?>
		</p>
		<p>
			<strong>Keywords:</strong>
			<?php echo $trainer_keywords; ?>
		</p>
		<!-- END Block Content -->
	</div>
	<br>

	<div class="row">
		<div class="col-md-8">
			<div class="block  background-warning shadow block-fixed-height">
				<!-- Grids Content Title -->
				<h4><b>Companies Served</b></h4>

				<!-- END Grids Content Title -->

				<div class="row">
					<div class="col-md-12">
						<div class="inside-block background-warning">
							<i class="fa fa-genderless fa-fw"></i>&nbsp;
							<?php echo $companies_served ?>
						</div>
					</div>
				</div>


				<?php if ( $is_trainer_profile_private && $correspondance_address ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-warning">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Address of correspondance
									: </b><?php echo $correspondance_address; ?>

							</div>
						</div>
					</div>
				<?php } ?>


				<!-- END Grids Content Content -->
			</div>
		</div>
		<div class="col-md-4">
			<div class="block  background-success shadow block-fixed-height">
				<!-- Grids Content Title -->
				<h4><b>Trainer Details</b></h4>

				<!-- END Grids Content Title -->

				<div class="row">
					<div class="col-md-12">
						<div class="inside-block background-success">
							<i class="fa fa-genderless fa-fw"></i>&nbsp;
							<b>Gender : </b> <?php echo ucfirst( $gender ); ?>
						</div>
					</div>
				</div>

				<?php
				if ( $is_trainer_profile_private && $trainer_dob ) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block background-success">
								<i class="fa fa-genderless fa-fw"></i>&nbsp;
								<b>Date of birth
									: </b><?php echo $trainer_dob; ?>

							</div>
						</div>
					</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-12">
						<div class="inside-block background-success">
							<i class="fa fa-genderless fa-fw"></i>&nbsp;
							<b>Training Experience : </b>
							<?php echo $str_experience_options; ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="inside-block background-success">
							<i class="fa fa-genderless fa-fw"></i>&nbsp;
							<b>Training Frequency
								: </b><?php echo $trainer_frequency; ?>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="inside-block background-success">
							<i class="fa fa-genderless fa-fw"></i>&nbsp;
							<b>Training Hierarchy
								: </b><?php echo $training_level; ?>

						</div>
					</div>
				</div>
				<!-- END Grids Content Content -->
			</div>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-xs-12 hidden-md hidden-lg div_recommendation_area">

		</div>
	</div>
	<?php
	if ( $is_trainer_profile_private ) { ?>
		<div class="row">
			<div class="col-md-8 col-xs-12">
				<div class="block  block-fixed-height block-scroll-y">
					<!-- Grids Content Title -->
					<div class="block-title block-top-rounded">
						<div class="row">
							<div class="col-md-6 col-xs-6">
								<h2><i class="fa fa-comments-o"></i> References&nbsp;&nbsp;&nbsp;</h2>
							</div>

							<div class="pull-right div_recommendation_area">
							</div>
						</div>

						<!-- END Grids Content Title -->
					</div>
					<!-- Grids Content Content -->
					<div class="row">
						<?php
						if ( isset( $trainer_references_contact_array[1] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Client Reference -
										1, Contact No.
										: <?php echo $trainer_references_contact_array[1]['contact_no'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[1] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Client Reference -
										1, Contact Name
										: <?php echo $trainer_references_contact_array[1]['contact_name'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[2] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Client Reference -
										2, Contact No.
										: <?php echo $trainer_references_contact_array[2]['contact_no'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[2] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Client Reference -
										2, Contact Name
										: <?php echo $trainer_references_contact_array[2]['contact_name'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[3] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Peer/Colleague Reference -
										1, Contact No.
										: <?php echo $trainer_references_contact_array[3]['contact_no'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[3] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Peer/Colleague Reference -
										1, Contact Name
										: <?php echo $trainer_references_contact_array[3]['contact_name'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[4] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Peer/Colleague Reference -
										2, Contact No.
										: <?php echo $trainer_references_contact_array[4]['contact_no'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[4] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Peer/Colleague Reference -
										2, Contact Name
										: <?php echo $trainer_references_contact_array[4]['contact_name'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[5] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Personal Reference -
										1, Contact No.
										: <?php echo $trainer_references_contact_array[5]['contact_no'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[5] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Peer/Colleague Reference -
										1, Contact Name
										: <?php echo $trainer_references_contact_array[5]['contact_name'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[6] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Personal Reference -
										2, Contact No.
										: <?php echo $trainer_references_contact_array[6]['contact_no'] ?>
									</div>
								</div>
							</div>
						<? } ?>

						<?php
						if ( isset( $trainer_references_contact_array[6] ) ) { ?>
							<div class="row">
								<div class="col-md-12">
									<div class="inside-block">
										Personal Reference -
										2, Contact Name
										: <?php echo $trainer_references_contact_array[6]['contact_name'] ?>
									</div>
								</div>
							</div>
						<? } ?>
					</div>
					<!-- END Grids Content Content -->
				</div>
			</div>
			<!-- End of COl for review & recmmendation -->

			<div class="col-md-4">
				<div class="block  block-fixed-height">
					<!-- Grids Content Title -->
					<div class="block-title block-top-rounded">
						<h2><i class="fa fa-trophy" style="color: #f7c520;"></i> Whitepaper</h2>
					</div>
					<!-- END Grids Content Title -->

					<!-- Grids Content Content -->
					<div class="row">
						<div class="col-md-12">
							<div class="inside-block">
								<?php
								$resume_exist = ! empty( $cv_path ) && file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/resume/' . $cv_path ) ? true : false;
								echo $resume_exist ? "<a href='" . PEDAGOGE_PLUGIN_URL . "/storage/uploads/resume/" . $cv_path . "'>Resume</a>" : '' ?>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="inside-block">
								<?php
								$sample_course_material_exists = ! empty( $sample_course_material_path ) && file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/sample_course_material/' . $sample_course_material_path ) ? true : false;
								echo $sample_course_material_exists ? "<a href='" . PEDAGOGE_PLUGIN_URL . "/storage/uploads/sample_course_material/" . $sample_course_material_path . "'>Sample Course Material</a>" : '' ?>
							</div>
						</div>
					</div>
					<!-- END Grids Content Content -->
				</div>
			</div>
		</div>
	<? } ?>
	<div class="row">
		<center>
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
			</div>
		</center>
	</div>

</div><br><br>