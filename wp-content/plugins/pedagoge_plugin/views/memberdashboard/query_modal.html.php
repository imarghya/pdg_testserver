<!----------------New Query Start-------------------->                            
<!--------------------------------------------tags start------------------------------------------------------>
<aside class="model_wrapper">
   <div class="modal fade" id="subjectChoose" role="dialog" >
      <div class="modal-dialog">
      <!-- Modal content-->
	 <div class="modal-content blue_border">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title text-center text-uppercase">Choose the subjects/activities you're looking Tutor for</h4>
	    </div>
	    <div class="modal-body no_padding_top_bottom">
	       <div class="row">				     
		  <form action="" method="post">
		     <input name="mytags" id="mySingleField" type="hidden"><!----------use this field ----->
		     <ul id="singleFieldTags"></ul>
		     <div class="modal-footer no_padding_top_bottom">
		     <div id="errortagmsg"></div>
		     <input type="button" value="Submit" class="data_submit blue_background color_white"  data-toggle="modal"  data-dismiss="modal" id="subject_frm_button" >
		     </div>
		  </form>
		</div>
	    </div>
	 </div>
      </div>
   </div>
</aside>
<span id="new_query_modal_item">
<!--New query modal content here-->
</span>				   
<!------------------------------------------------------------New Query End------------------------------------------------------>
<!--------------------Edit Query Modal-------------------------->
<aside class="model_wrapper edit_query">
   <!--<button data-toggle="modal" data-target="#fees">New</button>-->
   <div class="modal fade edit_query_modal" id="edit_query_modal" role="dialog">
      <div class="modal-dialog width_600">
	 <!-- Modal content-->
	 <div class="modal-content blue_border">
	   
	    <form method="post" id="edit_query_form">
	       <div class="modal-body no_padding_top_bottom">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  <div class="row" id="edit_query_data">
		     <!-------Edit Query Content Here--------->
		  </div>
	       </div>
	    </form>
	 </div>
      </div>
   </div>
</aside>
<!------------------------------------------------------>
<!--------------------Edit Past Query Modal-------------------------->
<aside class="model_wrapper edit_query">
   <!--<button data-toggle="modal" data-target="#fees">New</button>-->
   <div class="modal fade edit_query_modal" id="edit_past_query_modal" role="dialog">
      <div class="modal-dialog width_600">
	 <!-- Modal content-->
	 <div class="modal-content blue_border">
	   
	    <form method="post" id="edit_past_query_form">
	       <div class="modal-body no_padding_top_bottom">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  <div class="row past-edit" id="edit_Past_query_data">
		     <!-------Edit Query Content Here--------->
		  </div>
	       </div>
	    </form>
	 </div>
      </div>
   </div>
</aside>
<!------------------------------------------------------>


<!--------------------------------------------delete or activate box starts------------------------------------->

<aside class="model_wrapper">
   <div class="modal fade" id="deleteoractivate" name="deleteoractivate" role="dialog">
      <div class="modal-dialog modal-dialog-next">
      <!-- Modal content-->
         <div class="modal-content blue_border">                                 
            <input type="hidden" id="temp_q_id" value="0">
            <div class="modal-footer no_padding_top_bottom">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <button id="deletearchive" class="data_submit blue_background color_white">Delete</button>
            <button id="editactivate" data-dismiss="modal"  class="data_submit blue_background color_white">Edit and Activate</button>
            </div>
         </div>
      </div>
   </div>
</aside>
<!--------------------------------------popup with edit or delete button------------------------------>
<!-------------------------------------------------alert modal box------------------------------------->
<aside class="model_wrapper">
   <div class="modal fade" id="alertarchive" name="alertarchive" role="dialog" data-backdrop="static" data-keyboard="false" data-dismiss="modal">
      <div class="modal-dialog modal-dialog-next">
         <!-- Modal content-->
         <div class="modal-content blue_border">
            <input type="hidden" id="temp">
            <div class="modal-footer no_padding_top_bottom">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <span class="alert_banner">Are you sure you want to archive?</span>
               <button id="btnok"  class="data_submit blue_background color_white">OK</button>
               <button id="btncancel" data-dismiss="modal"  
                  class="data_submit blue_background color_white">CANCEL</button>
            </div>
         </div>
      </div>
   </div>
</aside>
<!-------------------------------------------------alert modal box------------------------------->
            <!-------------------------------------------------alert archive success------------------------->
            <aside class="model_wrapper">
               <div class="modal fade" id="alertarchivesuccess" name="alertarchivesuccess" role="dialog" data-backdrop="static" data-keyboard="false" data-dismiss="modal">
                  <div class="modal-dialog modal-dialog-next">
                     <!-- Modal content-->
                     <div class="modal-content blue_border">
                        <input type="hidden" id="temp">
                        <div class="modal-footer no_padding_top_bottom">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                           <span class="alert_banner">Archived</span>
                        </div>
                     </div>
                  </div>
               </div>
            </aside>
            <!-------------------------------------------------alert archive success-------------------------->     
            <!-----------------------------------------------alert intro------------------------------------>
            <div id="alertintrodiv"></div>
            <!-----------------------------------alert intro------------------------------------------------>                  
         </div>