<!--<style type="text/css">
    .slider {
        width: 50%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide li {
      width: 20px;
    }
    .slick-prev:before,
    .slick-next:before {
      color: black;
    }
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
  </style>-->
 
<section class="body_wrapper margin_top_20">
   <div class="container">
      <div class="row">
         <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
         </div>
         <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
            <div class="row">
               <div class="col-xs-12 blue_background top_radius h1_heading">
                  <h1 class="color_white text-center">
                    Active Queries <!--<a href="#" class="notification pull-right"><i class="fa fa-bell-o" aria-hidden="true"></i> <sup>2</sup></a>-->
                  </h1>
               </div>
            </div>
            <?php include("query_nav.php"); ?>
            <span id="active_query">
               
               <div class="row margin_top_10">
                  <div class="col-xs-12 pagination_controller">
                     <nav aria-label="..." >
                        <!--<ul class="pagination" id="pagination">
                           <li class="page-item disabled">
                              <a class="page-link" href="#" tabindex="-1"><i class="fa fa-caret-left" aria-hidden="true"></i></a>
                           </li>
                           <li class="page-item"><a class="page-link" href="#">1</a></li>
                           <li class="page-item"><a class="page-link" href="#">2</a></li>
                           <li class="page-item"><a class="page-link" href="#">3</a></li>
                           <li class="page-item"><a class="page-link" href="#">4</a></li>
                           <li class="page-item"><a class="page-link" href="#">5</a></li>
                           <li class="page-item"><a class="page-link" href="#">6</a></li>
                           <li class="page-item">
                              <a class="page-link" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                           </li>
                        </ul>-->
                        
                        <div class="row">
                           <div class="col-xs-4 col-xs-offset-4 col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-5">
                              <section class="center slider text-center" id="pagination">
                              <!--<li class="page-item active"><a class="page-link" href="#">1</a></li>
                              <li class="page-item"><a class="page-link" href="#">2</a></li>
                              <li class="page-item"><a class="page-link" href="#">3</a></li>
                              <li class="page-item"><a class="page-link" href="#">4</a></li>
                              <li class="page-item"><a class="page-link" href="#">5</a></li>
                              <li class="page-item"><a class="page-link" href="#">1</a></li>
                              <li class="page-item"><a class="page-link" href="#">2</a></li>
                              <li class="page-item"><a class="page-link" href="#">3</a></li>
                              <li class="page-item"><a class="page-link" href="#">4</a></li>
                              <li class="page-item"><a class="page-link" href="#">5</a></li>-->
                            </section>
                           </div>
                        </div>
                     </nav>
                  </div>
               </div>
            
               <div class="inner_container">
                 <input type="hidden" id="rec_filter_data" value="0"/>
                 <input type="hidden" id="active_query_id" value=""/>
                 <!-- Start Row-->
                  <div class="row" id="student_query_details"> 
                    <!--           Query Details          -->
                  </div>
                  <!-- End Row-->
               </div>
            </span>
         </div>
         <?php echo $this->load_view('memberdashboard/query_modal'); ?>
         <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
         </div>
      </div>
   </div>
</section>

<?php
if(isset($_GET['q']) && $_GET['q'] == 'postaquery'){
  ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#subjectChoose").modal('show');
    })
  </script>
  <?php
}
?>