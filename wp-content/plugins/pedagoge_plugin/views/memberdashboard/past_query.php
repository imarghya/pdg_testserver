<section class="body_wrapper margin_top_20">
   <div class="container">
      <div class="row">
         <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
         </div>
         <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
            <div class="row">
               <div class="col-xs-12 blue_background top_radius h1_heading">
                  <h1 class="color_white text-center">Past Queries</h1>
               </div>
            </div>
            <?php include("query_nav.php"); ?>
            <?php
            $query_model = new ModelQuery();
	    $results=$query_model->get_past_queries();
            if(count($results)>0){
            foreach($results AS $res){
            $localities = $query_model->get_q_locality($res->localities);
            ?>
            <span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
               <div class="inner_container margin_top_10" id="q_show_<?php echo $res->id; ?>">
                  <div class="row">
                     <div class="col-xs-12 inner_container_text gray_broder">
                        <div class="row">
                           <div class="col-xs-12 gray_background top_radius h1_heading">
                              <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span> <strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong></h2>
                           </div>
                        </div>
                        <div class="row margin_top_10">
                           <div class="col-md-8 col-sm-8 col-xs-7 localities">
                              <p>Year/Standard: <?php echo $res->standard; ?></p>
                              <div class="preferred_location">
                                 <p>Preferred Localities:</p>
                                 <ul class="area">
                                    <?php foreach($localities AS $loc){ ?>
                                    <li><a href="javascript:void(0);" class="gray_background"><?php echo $loc; ?></a></li>
                                    <?php } ?>
                                 </ul>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-4 col-xs-5">
                              <ul class="pull-right side_right_menu for_gray_background text-center">
                                 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="expand">Expand</a></li>
                                 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="archive">Archive</a></li>
                                 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="edit">Edit & Activate</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            
             <!--    ExpandView        -->
               <div class="col-xs-12 inner_container" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
                  <div class="row">
                     <div class="col-xs-12 inner_container_text gray_broder">
                        <div class="row">
                           <div class="col-xs-12 gray_background top_radius h1_heading">
                              <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
                           </div>
                        </div>
                        <div class="row margin_top_10">
                     <div class="col-md-6 col-sm-6 col-xs-12 localities">
                        <p>Year/Standard: <?php echo $res->standard; ?></p>
                        <p>Board/University: <?php echo $res->academic_board; ?></p>
                        <div class="preferred_location">
                           <p>Locality Type:</p>
                           <ul class="area">
                              <?php if($res->myhome){  ?>
                              <li><a href="javascript:void(0);" class="gray_background">My Home</a></li>
                              <?php } ?>
                              <?php if($res->tutorhome){  ?>
                              <li><a href="javascript:void(0);" class="gray_background">Tutor's Home</a></li>
                              <?php } ?>
                              <?php if($res->institute){  ?>
                              <li><a href="javascript:void(0);" class="gray_background">Institue</a></li>
                              <?php } ?>
                           </ul>
                        </div>
                        <div class="preferred_location">
                           <p>Preferred Localities:</p>
                           <ul class="area">
                              <?php foreach($localities AS $loc){ ?>
                                    <li><a href="javascript:void(0);" class="gray_background"><?php echo $loc; ?></a></li>
                              <?php } ?>
                           </ul>
                        </div>
                        <div class="preferred_location">
                           <p class="width_100">Date of commencement: <strong><?php echo $res->start_date; ?></strong></p>
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
                        <h5>Preferences: </h5>
                        <p class="text-justify"><?php echo $res->requirement; ?></p>
                     </div>
                     <div class="col-md-3 col-sm-3 col-xs-12">
                        <ul class="pull-right side_right_menu side_right_menu_media for_gray_background text-center text-center">
                           <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="collapse">Collapse</a></li>
                           <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="archive">Archive</a></li>
                           <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="edit">Edit & Activate</a></li>
                           <!--<li><a href="javascript:void(0);" class="blue_border blue_color">Refer someone</a></li>-->
                        </ul>
                     </div>
                  </div>
               </div>
                     </div>
                  </div>
                  
            </span>
            

            <?php }}else{ echo "<h6>No Past Query Found!</h6>"; }?>
            <?php echo $this->load_view('memberdashboard/query_modal'); ?>
         </div>
         <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
         </div>
      </div>
   </div>
</section>
<script>$(function(){$('.preference p').slimScroll({height: '140px',size: '5px', alwaysVisible: true,distance: '0px',color: '#2e3847', allowPageScroll: true,opacity: 1});});</script>