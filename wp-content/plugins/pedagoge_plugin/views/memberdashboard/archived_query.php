<section class="body_wrapper margin_top_20">
   <div class="container">
      <div class="row">
         <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
         </div>
         <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
            <div class="row">
               <div class="col-xs-12 blue_background top_radius h1_heading">
                  <h1 class="color_white text-center">Queries </h1>
               </div>
            </div>
            <?php include("query_nav.php"); ?>
            <?php
            $query_model = new ModelQuery();
            $results=$query_model->get_archived_queries();
            if(count($results)>0){
               //echo "<pre>"; print_r($results); echo "</pre>";
            foreach($results AS $res){
            $localities = $query_model->get_q_locality($res->localities);
            ?>
            <div class="inner_container margin_top_10" id="qspan_id_<?php echo $res->id; ?>">
               <div class="row">
                  <div class="col-xs-12 inner_container_text gray_broder">
                     <div class="row">
                        <div class="col-xs-12 gray_background top_radius h1_heading">
                           <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span><?php if($res->past!=''){ ?> <strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong><?php } ?></h2>
                        </div>
                     </div>
                     <div class="row margin_top_10">
                        <div class="col-md-8 col-sm-8 col-xs-7 localities">
                           <p>Year/Standard: <?php echo $res->standard; ?></p>
                           <div class="preferred_location">
                              <p>Preferred Localities:</p>
                              <ul class="area">
                                 <?php foreach($localities AS $loc){ ?>
                                    <li><a href="#" class="gray_background"><?php echo $loc; ?></a></li>
                                 <?php } ?>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-5">
                           <ul class="pull-right side_right_menu for_gray_background text-center">
                              <li class="margin_top_20"><a href="#" q_id="<?php echo $res->id; ?>" class="uarchive">Unarchive</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php }}else{ echo "<h6>No Archived Query Found!</h6>"; }?>
            <?php echo $this->load_view('memberdashboard/query_modal'); ?>
         </div>
         <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
         </div>
      </div>
   </div>
</section>