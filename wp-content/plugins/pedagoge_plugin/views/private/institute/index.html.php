<?php

	$admin_notification_div = '';

    $str_institute_data_verified = '
        <div class="text-left txt-verified">
            <i class="fa fa-paper-plane-o"></i>&nbsp;<b>Verified</b>
        </div>';
    $institute_user_id = '';
    $institute_user_email = '';
    $institute_name = '';
    $institute_profile_heading = '';
    $institute_about_coaching = '';
    $institute_size_of_faculty = '';
    $institute_operation_hours_from = '';
    $institute_operation_hours_to = '';
    $institute_coaching_experience = '';
    $institute_demo_class = 'N/A';
    $institute_demo_class_price = '';
    $installment_allowed = '';
    $fees_structure = '';

	$is_active = '';
	$is_approved = '';

    $profile_image_path = 'storage/uploads/images/';
    $institute_profile_image_url = '';
    $institute_no_image_url = '';

    if(isset($institute_data)) {
        foreach($institute_data as $institute_info) {
        	$is_active = $institute_info->active;
			$is_approved = $institute_info->approved;
            $institute_verified = $institute_info->verified;
            $str_institute_data_verified = $institute_verified == 'yes' ? $str_institute_data_verified : '';
            $institute_user_id = $institute_info->user_id;
            $institute_user_email = $institute_info->user_email;
            $institute_name = $institute_info->institute_name;
            $institute_profile_heading = $institute_info->profile_heading;
            $institute_about_coaching = $institute_info->about_coaching;
            $institute_size_of_faculty = $institute_info->size_of_faculty;
            $institute_operation_hours_from = $institute_info->operation_hours_from;
            $institute_operation_hours_to = $institute_info->operation_hours_to;
            $institute_coaching_experience = $institute_info->teaching_xp;
            $institute_demo_class = $institute_info->no_of_demo_class;
            $institute_demo_class_price = $institute_info->price_per_demo_class;
            /**code**/
             if($institute_demo_class == 0 ) {
                    $institute_demo_class = 'N/A';
                    $institute_demo_class_price = '';
                }
           else if($institute_demo_class_price == 0 || $institute_demo_class_price == '')
            {
                $institute_demo_class_price = '(FREE )';
            }
            else {

                    $institute_demo_class_price = '(<small> <i class="fa fa-inr"></i></small>'.$institute_demo_class_price.' )';
                }
            /**code**/

            $installment_allowed = $institute_info->allow_installments;
            $fees_structure = $institute_info->about_fees_structure;
            if($fees_structure == '' || $fees_structure == 0 ) {
                $fees_structure = '';
            }
            else{
                $fees_structure = 'Fee Structure : '.$fees_structure.'';
            }
        }
    }

	/**
	 * Notifications for admin.
	 */
	if($is_active == 'no' || $is_approved == 'no') {
		$str_is_active = '';
		$str_is_approved = '';
		if($is_active == 'no') {
			$str_is_active = '
				<div class="alert alert-danger" role="alert">Profile is not <strong>Active</strong>.</div>
			';
		}
		if($is_approved == 'no') {
			$str_is_approved = '
				<div class="alert alert-danger" role="alert">Profile is not <strong>Approved</strong>.</div>
			';
		}
		$admin_notification_div = '
			<div class="col-md-12 well">
				<div class="col-md-6">'.$str_is_active.'</div>
				<div class="col-md-6">'.$str_is_approved.'</div>
			</div>
		';
	}

    /**image coding start**/
    $institute_profile_image_path = PEDAGOGE_PLUGIN_DIR.$profile_image_path.$institute_user_id.'/profile.jpg';
    if(file_exists($institute_profile_image_path)) {
        $institute_profile_image_url = PEDAGOGE_PLUGIN_URL.'/'.$profile_image_path.$institute_user_id.'/profile.jpg';
    } else {
        $institute_profile_image_url = PEDAGOGE_ASSETS_URL.'/images/sample.jpg';
    }

    $gallary_dir = PEDAGOGE_PLUGIN_DIR.$profile_image_path.$institute_user_id.'/gallery/';
    $teacher_no_image_url = PEDAGOGE_ASSETS_URL.'/images/noslide.jpg';
    $gallery_url = PEDAGOGE_PLUGIN_URL.'/'.$profile_image_path.$institute_user_id.'/gallery/';
    //$scanned_directory = array_diff(scandir($gallary_dir), array('..', '.'));
    $str_slide_count = '';
    $counter = 0;
    $str_slides = '';

    if(is_dir($gallary_dir)) {

        $scanned_directory = array_diff(scandir($gallary_dir), array('..', '.'));
        foreach($scanned_directory as $gallery_file) {
            $str_active = '';
            if($counter==0) {
                $str_active = ' active';
                $str_slide_count.= '<li data-target="#carousel-example-generic" data-slide-to="'.$counter.'" class="active"></li>';
            } else {
                $str_slide_count.= '<li data-target="#carousel-example-generic" data-slide-to="'.$counter.'"></li>';
            }

            $str_slides .= '
                <div class="item '.$str_active.'">
                    <img src="'.$gallery_url.$gallery_file.'" class="block-rounded slide-institute" alt="...">
                </div>
            ';

            $counter++;
        }


        if(empty($str_slides)) {
            $str_slide_counter = '<img src="'.$teacher_no_image_url.'" class="block-rounded slide-institute">';
        } else {
            $str_slide_counter = '
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">                        
                    <ol class="carousel-indicators">
                        '.$str_slide_count.'
                    </ol>                       
                    <div class="carousel-inner block-rounded" role="listbox">
                        '.$str_slides.'
                    </div>
                     <!-- Controls -->                       
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> 
            ';
        }

    } else {
        $str_slide_counter = '<img src="'.$teacher_no_image_url.'" class="block-rounded slide-institute">';
    }
    /**end of image coding**/


    $institute_achievement = '';
    if(isset($achievement_data)) {
        foreach($achievement_data as $achievement_info) {
              $institute_achievement .= '<i class="fa fa-genderless fa-fw"></i>' . $achievement_info->achievement.'<br><br>';
        }
        if($institute_achievement=='')
        {
            $institute_achievement .= '<div class="alert alert-info">No Achievements yet to show.</div>';
        }
    }

    $str_course_details = '<h3>Course/Subjects details not available.</h3>';

    if(isset($tuition_batch_data) && !empty($tuition_batch_data)) {
        $str_course_details = '';
        foreach($tuition_batch_data as $batch_data) {
            $batch_id = $batch_data->tuition_batch_id;
            $course_days = $batch_data->course_days;
            $class_duration = $batch_data->class_duration;
            $course_length = $batch_data->course_length;
            $course_length_type = $batch_data->course_length_type;
            $pdg_class_capacity = $batch_data->pdg_class_capacity;
            $about_the_course = $batch_data->about_course;
            if($about_the_course == '' || $about_the_course == 0 ) {
                $about_the_course = "No Information Available";

            }


            if($course_length == '' || $course_length == 0 ) {
                $course_length = "N/A";
                $course_length_type = " ";
            }

            $weekdays_array = array(
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',

            );

            $course_days = explode(',', $course_days);

            $str_new_course_days = '';

            foreach($weekdays_array as $weekday) {
                $rounded_border = '';
                if(in_array($weekday, $course_days)) {
                    $rounded_border .= 'day-border-round';
                }
                $str_new_course_days .= '<span data-toggle="tooltip" class="'.$rounded_border.'" title="'.$weekday.'">'.$weekday[0].'</span>&nbsp;&nbsp;';
            }


            $str_class_timing = 'N/A';
            $class_timing_counter = 0;

            if(isset($tuition_batch_class_timing_data)) {

                $str_class_timing = '';
                foreach($tuition_batch_class_timing_data as $class_timing) {

                    $class_timing_batch_id = $class_timing->tuition_batch_id;
                    if($class_timing_batch_id == $batch_id) {
                        $str_class_timing .= '
                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            <i class="fa fa-clock-o fa-fw"></i>&nbsp;&nbsp;&nbsp;&nbsp;'.$class_timing->class_timing.'
                        </div>
                    </div>                    
                    ';
                    }
                }
            }

            $str_fees_types = 'N/A';
            if(isset($tutor_batch_fees_data)) {
                $str_fees_types = '';
                foreach($tutor_batch_fees_data as $fees_data) {
                    $fees_data_batch_id = $fees_data->tuition_batch_id;
                    $fees_type = $fees_data->fees_type;
                    $fees_amount = $fees_data->fees_amount;
                    if($fees_data_batch_id == $batch_id) {
                        $str_fees_types .= '<span data-toggle="tooltip" title="'.$fees_type.'">'.$fees_amount.'</span>'.'/';
                    }
                }
                $str_fees_types = substr($str_fees_types,0,-1);
            }

            $str_subjects = 'N/A';
            if(isset($tuition_batch_subject_data)) {
                $subjects_array = array();
                foreach($tuition_batch_subject_data as $subject_data) {
                    $subject_batch_id = $subject_data->tuition_batch_id;
                    $subject_name = $subject_data->subject_name;
                    if($subject_batch_id == $batch_id) {
                        if(!in_array($subject_name, $subjects_array)) {
                            $subjects_array[] = $subject_data->subject_name;
                        }
                    }
                }
                $str_subjects = implode(' / ', $subjects_array);
            }

            $str_locality = '';
            $locality_address_student = '';
            if(isset($tutor_locality_data)) {
                foreach($tutor_locality_data as $locality_data) {
                    $locality_batch_id = $locality_data->tuition_batch_id;
                    $locality_address = $locality_data->tuition_location_type;
                    if($locality_address != "student") {
                        $locality_address_student ='
                        <div class="col-md-4">
                                    <div class="inside-block background-primary">
                                        <strong>Address: </strong><br />
                                        <span>'.$locality_data->address.'</span>
                                    </div>
                                </div>';
                    }
                    if($locality_batch_id == $batch_id) {
                        $str_locality .= '
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="inside-block background-primary">
                                        <b>Type of Location: </b><br />
                                        <span>'.ucfirst($locality_data->tuition_location_type).'</span>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="inside-block background-primary">
                                        <b>Locality: </b><br />
                                        <span>'.$locality_data->locality.'</span>
                                    </div>
                                </div>
                                
                            </div>
                        ';
                    }
                }
            }



            $str_course_details .= '
                <!-- Grids Content Title -->
                <h4>
                    <b><i class="fa fa-tags fa-fw"></i>&nbsp;&nbsp;&nbsp;
                            <span>'.$str_subjects.'</span></b> 
                    <small class="color_white">
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="<div class=\'text-left\'>'.$about_the_course.'</div>">
                        </i>
                    </small>
                </h4>
                <!-- END Grids Content Title -->
            
                <!-- Grids Content Content -->
                
            
                <div class="row ">
                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            <i class="fa fa-calendar fa-fw"></i>&nbsp;&nbsp;
                            '.$str_new_course_days.'
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            <i class="fa fa-inr fa-fw"></i>&nbsp;&nbsp;&nbsp;
                            '.$str_fees_types.'                                                                         
                             <i class="fa fa-question-circle fa-fw" data-html="true" 
                             data-toggle="tooltip" data-placement="right" 
                             title="<div class=\'text-left\'> '.$fees_structure.'
                             <br>Installment Options : '.strtoupper($installment_allowed).'</div>">
                            </i>
                        </div>
                    </div>
                </div>


            
                <div class="row ">
                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            <i class="fa fa-hourglass-half fa-fw"></i>&nbsp;&nbsp;&nbsp;
                            <span>'.$class_duration.' hrs</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            <i class="fa fa-users fa-fw"></i>&nbsp;&nbsp;&nbsp;
                            <span>'.$pdg_class_capacity.' </span>
                        </div>
                    </div>
                </div>
                
                <div class="row ">
                    '.$str_class_timing.'
                </div>      
                <div class="row">
                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            
                            <b>Length of Course: </b>
                            <span>'.$course_length.' '.$course_length_type.'</span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="inside-block background-primary">
                            
                            <b>Demo Class : </b> 
                            '. $institute_demo_class.'
                            '.$institute_demo_class_price.' 
                        </div>
                    </div>
                        
                </div>
                '.$str_locality.'
                <hr>
                <!-- END Grids Content Content -->
            ';

        }
    }

    $logged_in_current_user_id = '';
    $is_current_user_allowed_to_review = FALSE;
    if(isset($pdg_current_user)) {
        $logged_in_current_user_id = $pdg_current_user->ID;
        if(isset($current_user_db_info)) {
            $current_user_role = '';
            foreach($current_user_db_info as $cur_user) {
                $current_user_role = $cur_user->user_role_name;
            }
            switch($current_user_role) {
                case 'student':
                case 'guardian':
                    $is_current_user_allowed_to_review = TRUE;
                    break;
            }

        }
    }

    $recommend_data = '';
    if(isset($recommendations_data)) {
        $recommend_data = $recommendations_data;
    }

    $recommendation_count = 0;
    $current_user_has_recommended = FALSE;
    if(!empty($recommend_data)) {
        foreach($recommend_data as $recommend) {
            $recommend_to_user_id = $recommend->recommended_to;
            $recommended_by_user_id = $recommend->recommended_by;

            if($institute_user_id == $recommend_to_user_id) {
                if($recommended_by_user_id == $logged_in_current_user_id) {
                    $current_user_has_recommended = TRUE;
                }
                $recommendation_count++;
            }
        }
    }

    $str_recommendation_icon = '';
    $str_recommendation_button = '';
    if((is_numeric($logged_in_current_user_id) && $logged_in_current_user_id > 0) && $is_current_user_allowed_to_review==TRUE) {

        if($current_user_has_recommended) {
            $str_recommendation_icon = '
                <a href="#" class="cmd_recommend" data-recommended="yes" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'"><img src="'.PEDAGOGE_PROUI_URL.'/img/icons/recommended.png" class="img-responsive img-recommend"><span class="recommendation_count">'.$recommendation_count.'</span></a>
            ';
            $str_recommendation_button = '<a href="#" class="cmd_recommend cmd_recommend_button btn btn-signup pull-right" data-recommended="yes" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'">Unrecommend</a>';
        } else {
            $str_recommendation_icon = '
                <a href="#" class="cmd_recommend" data-recommended="no" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'"><img src="'.PEDAGOGE_PROUI_URL.'/img/icons/rec.png" class="img-responsive img-recommend"><span class="recommendation_count">'.$recommendation_count.'</span></a>
            ';
            $str_recommendation_button = '<a href="#" class="cmd_recommend cmd_recommend_button btn btn-signup pull-right" data-recommended="no" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'">Recommend</a>';
        }
    } else {

        $str_recommendation_icon = '
            <img src="'.PEDAGOGE_PROUI_URL.'/img/icons/rec.png" class="img-responsive img-recommend"><span class="recommendation_count">'.$recommendation_count.'</span>
        ';
        $str_recommendation_button = '<a href="#" class="btn btn-signup pull-right">Recommend</a>';
    }

    /**
     * Ratings
     */
    $ratings_total = 0;
    $rating_count = 0;
    $current_user_has_rated = FALSE;
    $average_rating = 0;
    if(isset($rating_data) && !empty($rating_data)) {
        foreach($rating_data as $ratings) {
            $rated_to_user_id = $ratings->tutor_institute_user_id;
            $rated_by_user_id = $ratings->reviewer_user_id;
            $star_rating = $ratings->average_rating;
            $is_approved = $ratings->is_approved;

            if($institute_user_id == $rated_to_user_id) {
                if($rated_by_user_id == $logged_in_current_user_id) {
                    $current_user_has_rated = TRUE;
                }
                if($is_approved == 'no') {
                    continue;
                }
                $rating_count++;
                $ratings_total += $star_rating;
            }
        }
    }
    $rating_icon_name = $average_rating;
    if($rating_count>0 && $ratings_total>0) {
        $average_rating = $ratings_total / $rating_count;
        $average_rating = PDGSearchContent::pdg_avg_rating($average_rating);
        /*$floored_val = floor($average_rating);
        $decimal_val =  $average_rating - $floored_val;

        if($decimal_val>0.75) {
            $average_rating = $floored_val + 1;
        } else if($decimal_val<0.25) {
            $average_rating = $floored_val;
        } else {
            $average_rating = $floored_val + 0.5;
        }*/

        $rating_icon_name = str_replace('.', '', $average_rating);
    }

    $str_rating_icon = '';
    $str_rating_button = '';
    if((is_numeric($logged_in_current_user_id) && $logged_in_current_user_id > 0) && $is_current_user_allowed_to_review==TRUE) {

        if($current_user_has_rated) {
            $str_rating_icon = '
                <img src="'.PEDAGOGE_PROUI_URL.'/img/icons/ratyes'.$rating_icon_name.'.png" class="img-responsive img-recommend img_ratings"><span class="rating_count">'.$rating_count.'</span>
            ';
            $str_rating_button = '<a href="#" class="btn btn-signup pull-left" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'">Review</a>';
        } else {
            $str_rating_icon = '
                <a href="#" class="cmd_rating_done" data-toggle="modal" data-target="#rating_modal" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'">
                    <img src="'.PEDAGOGE_PROUI_URL.'/img/icons/ratno'.$rating_icon_name.'.png" class="img-responsive img-recommend img_ratings"><span class="rating_count">'.$rating_count.'</span>
                </a>
            ';
            $str_rating_button = '<a href="#" class="cmd_rating_done btn btn-signup pull-left" data-toggle="modal" data-target="#rating_modal" data-current_user="'.$logged_in_current_user_id.'" data-teacher_user_id="'.$institute_user_id.'">Review</a>';
        }
    } else {

        $str_rating_icon = '
            <img src="'.PEDAGOGE_PROUI_URL.'/img/icons/ratno'.$rating_icon_name.'.png" class="img-responsive img-recommend img_ratings"><span class="rating_count">'.$rating_count.'</span>
        ';
        $str_rating_button = '<a href="#" class="btn btn-signup pull-left">Review</a>';
    }
    $str_recommendations_html = ControllerTeacher::fn_return_reviews_content($rating_data);
?>

<!--
#### ##    ## ######## ########   #######
 ##  ###   ##    ##    ##     ## ##     ##
 ##  ####  ##    ##    ##     ## ##     ##
 ##  ## ## ##    ##    ########  ##     ##
 ##  ##  ####    ##    ##   ##   ##     ##
 ##  ##   ###    ##    ##    ##  ##     ##
#### ##    ##    ##    ##     ##  #######
-->
   <section class="site-section site-section-light site-section-top site-section-bottom themed-background-dark change-background">
        <!-- <div class="container text-center">
            <div class="row">
                <div class="col-md-12 hidden-sm hidden-xs">
                    <center>
                        <div class="input-group">
                            <input type="text" class="form-control btn-round search-box" placeholder="Interested Subject">
                            <input type="text" class="form-control search-box" placeholder="Kolkata">
                            <button type="button" class="form-control btn btn-success btn-round search-btn">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </center>
                </div>
            </div>
        </div> -->
    </section>
    <!-- END Intro -->
<!--
########  ########   #######  ######## #### ##       ########
##     ## ##     ## ##     ## ##        ##  ##       ##
##     ## ##     ## ##     ## ##        ##  ##       ##
########  ########  ##     ## ######    ##  ##       ######
##        ##   ##   ##     ## ##        ##  ##       ##
##        ##    ##  ##     ## ##        ##  ##       ##
##        ##     ##  #######  ##       #### ######## ########
-->
        <div class="container"><br>
            <div class="row" style="padding-left: 1.35%;">
            	<?php echo $admin_notification_div; ?>
                <div class="col-md-3 col-xs-12 profile-txt block-rounded block-institute">
                    <div class="row"><br>
                        <div class="col-md-6 col-xs-6">
                            <?php echo $str_institute_data_verified; ?>
                        </div>
                        <div class="col-md-6 col-xs-6 text-right">
                            <!-- <a href="#"><i class="fa fa-lock"></i></a>&nbsp;
                            <a href="#"><i class="fa fa-pencil-square"></i></a> -->
                        </div><br><br>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <img src="<?php echo $institute_profile_image_url; ?>" class="img-responsive img-display block-rounded">
                            <h4 class="text-center"><?php echo $institute_name;?></h4>
                            <p class="text-center"><i class="fa fa-university"></i>&nbsp;Institution</p>
                        </div>
                    </div>

                    <div class="row div_recommendation_area">
                        <div class="col-md-3 col-md-offset-3 col-xs-3 col-xs-offset-3">
                            <center>
                                <?php echo $str_recommendation_icon; ?>
                            </center>
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <center>
                                 <?php echo $str_rating_icon; ?>
                            </center>
                        </div>
                    </div>
                </div>

                <!-- Rating Modal -->
                <!-- Modal -->
                <div class="modal fade" id="rating_modal" tabindex="-1" role="dialog" aria-labelledby="rating_modal_label">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="rating_modal_label">Rating and Reviews</h4>
                            </div>
                            <div class="modal-body" id="review_modal_body">

                                <div class="row">
                                    <div class="col-md-12"><span id="span_star1">Cooperative<h6></span>(Is this teacher helpful when needed?)*</h6></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="rating rating-loading pdg_star_rating" name="star1" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12"><span id="span_star2">Clarity</span><h6>(Is this teacher clear about the class requirements and subject matter?)*</h6></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="rating rating-loading pdg_star_rating" name="star2" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><span id="span_star3">Composure</span><h6>(Is the teacher able to handle the class well?)*</h6></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="rating rating-loading pdg_star_rating" name="star3" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><span id="span_star4">Coolness</span><h6>(Do you look forward to the next class?)*</h6></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input class="rating rating-loading pdg_star_rating col-md-12" name="star4" data-starvalue="0" data-min="0" data-max="5" data-step="1" data-size="xs" data-show-clear="false" data-show-caption="false">
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">Please write your review!</div>
                                    <div class="col-md-12 form-group">
                                        <textarea rows="8" class="form-control" id="txt_review_field"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" id="div_rating_result_area"></div>
                                    <div class="col-md-12 center_content hidden_item" id="div_rating_loader">
                                        <img src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" class="center_content" alt="" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="cmd_save_review" data-loading-text="Saving review...">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of Rating Modal -->

                <div class="col-md-9">
                    <!-- course details starts here -->
                    <div id="teacher_course_details" class="block block-rounded background-primary shadow block-scroll">
                        <?php echo $str_course_details; ?>
                    </div>
                    <!-- course details ends here -->

                </div>

                <!-- Show Interest Modal -->
                <div class="modal fade " id="show-interest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header text_center">
                                <h3>Institute Information</h3>
                            </div>
                            <div class="modal-body" id="show_interest_modal_body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Email:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo $user_email; ?>
                                    </div>
                                </div></br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Contact Person:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo $contact_person_name; ?>
                                    </div>
                                </div></br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Contact Number:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo $mobile_no; ?>
                                    </div>
                                </div></br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Permanent Address:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo $permanent_address.', '.$permanent_city; ?>
                                    </div>
                                </div></br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Current Address:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <?php echo $current_address.', '.$current_city; ?>
                                    </div>
                                </div></br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>
                <!-- End of Institute Information Modal -->
            </div>
<!--
   ###    ########   #######  ##     ## ########
  ## ##   ##     ## ##     ## ##     ##    ##
 ##   ##  ##     ## ##     ## ##     ##    ##
##     ## ########  ##     ## ##     ##    ##
######### ##     ## ##     ## ##     ##    ##
##     ## ##     ## ##     ## ##     ##    ##
##     ## ########   #######   #######     ##
-->
            <div class="block block-rounded">
                <!-- Block Title -->
                <div class="block-title block-top-rounded">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                    <h2><?php echo $institute_profile_heading;?></h2>
                    </div>

                     <div class="col-md-6 col-xs-6">
                        <button class="btn btn-signup pull-right show_interest" id="edit" data-toggle="modal" data-target="#show-interest">
                            Institute Information
                        </button>
                    </div>
                    </div>
                </div>
                <!-- END Block Title -->

                <!-- Block Content -->
                <p>
                   <?php echo $institute_about_coaching; ?>

                </p>
                <!-- END Block Content -->
            </div><br>

<!--
 ######   #######  ##     ## ########   ######  ########
##    ## ##     ## ##     ## ##     ## ##    ## ##
##       ##     ## ##     ## ##     ## ##       ##
##       ##     ## ##     ## ########   ######  ######
##       ##     ## ##     ## ##   ##         ## ##
##    ## ##     ## ##     ## ##    ##  ##    ## ##
 ######   #######   #######  ##     ##  ######  ########
-->
            <div class="row">
                <div class="col-md-8 hidden-sm hidden-xs">
                     <?php echo $str_slide_counter; ?>
                </div>


                <div class="col-md-4">
                    <div class="block block-rounded block-scroll background-success shadow">
                        <!-- Grids Content Title -->
                        <h4><b>INSTITUTION DETAILS</b></h4>

                        <!-- END Grids Content Title -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="inside-block background-success">
                                    <i class="fa fa-genderless fa-fw"></i>&nbsp;
                                    <b>Size of Faculty : </b>
                                    <?php echo $institute_size_of_faculty; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="inside-block background-success">
                                    <i class="fa fa-genderless fa-fw"></i>&nbsp;
                                    <b>Contact Hours of Coaching :</b> <br>
                                    <div class="timing text-justify">
                                  <?php echo $institute_operation_hours_from."-";
                                   echo $institute_operation_hours_to;?>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-md-12">
                                <div class="inside-block background-success">
                                    <i class="fa fa-genderless fa-fw"></i>&nbsp;
                                    <b>Coaching Experience : </b>
                                    <?php echo $institute_coaching_experience;?>
                                </div>
                            </div>
                        </div>


                        <!-- END Grids Content Content -->
                    </div>
                </div>
            </div><br>

<!--
########  ######## ##     ## #### ######## ##      ##
##     ## ##       ##     ##  ##  ##       ##  ##  ##
##     ## ##       ##     ##  ##  ##       ##  ##  ##
########  ######   ##     ##  ##  ######   ##  ##  ##
##   ##   ##        ##   ##   ##  ##       ##  ##  ##
##    ##  ##         ## ##    ##  ##       ##  ##  ##
##     ## ########    ###    #### ########  ###  ###
-->

            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="block block-rounded block-scroll">
                        <!-- Grids Content Title -->
                        <div class="block-title block-top-rounded" id="block-title-edit">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <h2><i class="fa fa-comments-o"></i> Reviews&nbsp;&nbsp;&nbsp;</h2>
                                </div>
                            </div>
                        <!-- END Grids Content Title -->
                        </div>
                        <!-- Grids Content Content -->
                            <?php echo $str_recommendations_html; ?>
                        <!-- END Grids Content Content -->
                    </div>
                </div>
                <!-- End of COl for review & recmmendation -->

                <div class="col-md-4">
                    <div class="block block-rounded block-scroll">
                        <!-- Grids Content Title -->
                        <div class="block-title block-top-rounded">
                            <h2><i class="fa fa-trophy" style="color: #f7c520;"></i> Achievements
                            </h2>
                        </div>
                        <!-- END Grids Content Title -->

                        <!-- Grids Content Content -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="inside-block">
                                   <?php echo $institute_achievement;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br><br></div>

