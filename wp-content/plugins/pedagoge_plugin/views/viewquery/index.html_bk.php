<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32">-->
      <title>Pedagoge</title>
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css_admin/styleboard.css" rel="stylesheet" type="text/css" media="screen">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css_admin/datepicker.css" rel="stylesheet" type="text/css" media="screen">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css_admin/sb-admin.css" rel="stylesheet" type="text/css" media="screen">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div id="wrapper">
         <!-- Navigation -->
         <nav class="navbar navbar-inverse navbar-fixed-top admin_white_background no_border" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header admin_blue_background admin_header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand color_white" href="index.html"><i class="glyphicon glyphicon-flash"></i> Pedagoge <span>Cp</span></a>
            </div>
            <!-- Menu Second -->
            <ul class="nav navbar-left top-nav admin_top_navigation">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation width_auto no_border" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#">Profile</a>
                     </li>
                     <li>
                        <a href="#">Inbox</a>
                     </li>
                     <li>
                        <a href="#">Settings</a>
                     </li>
                     <li>
                        <a href="#">Log Out</a>
                     </li>
                  </ul>
               </li>
               <li>
                  <p class="inline_block">
                     <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 293.129 293.129" style="enable-background:new 0 0 293.129 293.129;" xml:space="preserve">
                        <g>
                           <path d="M162.179,140.514c3.377-1.727,7.139-2.64,11.042-2.64c6.468,0,12.549,2.511,17.133,7.071l9.868-9.867 c24.42,8.56,52.664,3.08,72.186-16.441c16.426-16.426,22.904-39.026,19.446-60.329c-0.381-2.346-2.042-4.281-4.303-5.011 c-2.261-0.731-4.743-0.137-6.423,1.544l-14.652,14.652c-11.932,11.932-31.279,11.932-43.211,0 c-11.933-11.932-11.933-31.279,0-43.211l14.652-14.652c1.681-1.681,2.28-4.163,1.548-6.425c-0.731-2.263-2.669-3.92-5.016-4.301 c-21.302-3.458-43.903,3.02-60.328,19.446c-19.812,19.812-25.144,48.604-16.032,73.269l-21.402,21.402L162.179,140.514z" fill="#394263"/>
                           <path d="M123.179,179.296l-25.385-25.385L9.029,242.675c-11.542,11.542-11.542,30.255,0,41.797 c11.542,11.542,30.255,11.542,41.797,0l76.521-76.52C119.629,200.193,118.238,188.479,123.179,179.296z" fill="#394263"/>
                           <path d="M179.795,155.597c-1.815-1.815-4.195-2.723-6.574-2.723s-4.759,0.908-6.574,2.723l-5.299,5.299L66.956,66.504l4.412-4.412 c4.02-4.019,3.521-10.686-1.061-14.06L31.795,19.669c-3.701-2.725-8.837-2.338-12.087,0.912L3.356,36.934 c-3.25,3.25-3.637,8.387-0.912,12.087l28.362,38.512c3.374,4.581,10.037,5.085,14.06,1.061l4.412-4.413l94.392,94.392l-5.672,5.672 c-3.631,3.631-3.631,9.517,0,13.148l87.079,87.079c11.542,11.542,30.255,11.542,41.797,0c11.542-11.542,11.542-30.255,0-41.797 L179.795,155.597z" fill="#394263"/>
                        </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                     </svg>
                     Admin Control Panel :<strong>Queries</strong>
                  </p>
                  <!--<form class="navbar-form inline_block query_search" role="search">
                     <div class="input-group blue_border border_radius_30 overflow_hidden">
                        <input type="text" class="form-control border_none" placeholder="Search" name="srch-term" id="srch-term">
                        <div class="input-group-btn">
                           <button class="btn blue_background color_white" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                     </div>
                  </form>-->
               </li>
               <li class="margin_left_300">
                  <ul class="add_edit_icon">
                     <li class="two_box"><a href="#" title="New Query"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                     <li><a href="#" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                     <li class="two_box"><a href="#" title="Clone"><i class="fa fa-clone" aria-hidden="true" style="opacity:0;"></i></a></li>
                     <!--<li><a href="#" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a></li>
                     <li><a href="#" title="Disapprove"><i class="fa fa-times" aria-hidden="true"></i></a></li>-->
                  </ul>
               </li>
               <!--<li>
                  <input type="text" id="assign" name="assign" placeholder="Assign" class="text-uppercase text-center assign">
               </li>-->
            </ul>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav admin_top_navigation">
               <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation" data-toggle="dropdown"> New <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#">New</a>
                     </li>
                     <li>
                        <a href="#">Assigned</a>
                     </li>
                     <li>
                        <a href="#">Pending for Approval</a>
                     </li>
                  </ul>
               </li>-->
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle admin_user_id no_padding_top_bottom" data-toggle="dropdown"><strong><i class="fa fa-bell-o" aria-hidden="true"></i></strong></a>
                  <ul class="dropdown-menu notification_admin">
                     <li><!--For Show and hide notification "hide_notification" and "show_notification"-->
                        <a href="#" class="active">
                        	<div>
                        		<span>12</span>
                        	</div>
                            <div>
								<svg version="1.1" id="Layer_1" x="0px" y="0px"
                                     viewBox="0 0 87.954 63.442" style="enable-background:new 0 0 87.954 63.442;"
                                     xml:space="preserve">
                                <g>
                                    <rect x="78.811" y="33.275"  width="2.783" height="2.626"/>
                                    <rect x="78.811" y="40.63"  width="2.783" height="2.627"/>
                                    <rect x="78.811" y="48.001" width="2.783" height="2.627"/>
                                    <path d="M6.362,47.203l30.061,15.252c2.258,1.183,4.904,1.32,7.285,0.521l25.295-8.016h3.449v2.765h15.502
                                        V26.303H72.452v3.548H62.52c-0.79,0-1.718,0.137-2.521,0.399l-9.004,4.605c-0.402,0.138-0.929,0.261-1.33,0.261H35.881
                                        c-3.449,0-6.358,2.765-6.358,6.174c0,0.523,0,0.921,0.14,1.444l-18.535-6.174c-2.91-1.06-6.096,0.261-7.412,3.024
                                        C2.387,42.336,3.579,45.636,6.362,47.203 M75.098,54.96V29.852v-1.06h10.209v26.306H75.112V54.96H75.098z M6.084,40.63
                                        c0.791-1.581,2.521-2.241,4.239-1.705l21.859,7.234c1.067,0.659,2.258,1.183,3.712,1.183h11.387c1.33,0,2.384,1.045,2.384,2.365
                                        h2.645c0-2.765-2.258-4.992-5.028-4.992H35.756c-1.98,0-3.573-1.596-3.573-3.548c0-1.966,1.606-3.547,3.573-3.547h13.907
                                        c0.791,0,1.594-0.138,2.258-0.399l9.004-4.605c0.527-0.139,1.054-0.262,1.455-0.262h9.933v19.979h-3.712l-25.683,8.154
                                        c-1.717,0.523-3.713,0.399-5.306-0.397L7.678,44.702C6.084,44.054,5.434,42.075,6.084,40.63"/>
                                    <path d="M21.867,35.865c-6.496,0-11.78-5.246-11.78-11.694c0-6.447,5.284-11.693,11.78-11.693
                                        s11.781,5.246,11.781,11.693C33.648,30.619,28.363,35.865,21.867,35.865 M21.867,15.031c-5.076,0-9.207,4.101-9.207,9.14
                                        c0,5.04,4.131,9.139,9.207,9.139c5.077,0,9.207-4.1,9.207-9.139C31.074,19.132,26.944,15.031,21.867,15.031"/>
                                    <path d="M7.199,14.291C3.23,14.291,0,11.085,0,7.145C0,3.205,3.23,0,7.199,0c3.97,0,7.199,3.205,7.199,7.145
                                        C14.398,11.085,11.168,14.291,7.199,14.291 M7.199,2.553c-2.55,0-4.625,2.061-4.625,4.591c0,2.532,2.075,4.591,4.625,4.591
                                        s4.625-2.059,4.625-4.591C11.824,4.614,9.749,2.553,7.199,2.553"/>
                                </g>
                                </svg>
                        	</div>
						</a>
                     </li>
                     <li>
                        <a href="#">
                        	<div>
                                <span>12</span>
                            </div>
                            <div>
								<svg version="1.1" id="Layer_1" x="0px" y="0px"
                                 viewBox="0 0 84.781 84.742" style="enable-background:new 0 0 84.781 84.742;"
                                 xml:space="preserve">
                            <rect x="34.174" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="41.059" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="47.957" y="75.477"  width="2.646" height="2.645"/>
                            <polygon points="84.776,52.951 84.776,84.742 0,84.742 0,52.951 27.69,52.951 27.69,55.598 2.646,55.598 
                                2.646,82.096 82.131,82.096 82.131,55.598 57.086,55.598 57.086,52.951 "/>
                            <rect x="34.174" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="41.059" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="47.957" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="6.358" y="27.781"  width="2.784" height="2.646"/>
                            <rect x="6.358" y="20.371"  width="2.784" height="2.646"/>
                            <rect x="6.358" y="12.945"  width="2.784" height="2.646"/>
                            <path d="M81.591,16.395L51.531,1.032c-2.259-1.191-4.904-1.33-7.286-0.526L18.95,8.582h-3.449V5.797H0v31.654
                                h15.501v-3.574h9.932c0.79,0,1.719-0.139,2.521-0.402l9.004-4.641c0.402-0.139,0.928-0.263,1.33-0.263h13.783
                                c3.449,0,6.358-2.785,6.358-6.22c0-0.527,0-0.928-0.139-1.455l18.535,6.22c2.909,1.067,6.095-0.263,7.411-3.047
                                C85.566,21.298,84.375,17.974,81.591,16.395 M12.854,8.582v25.294v1.066H2.646v-26.5h10.195v0.139H12.854z M81.868,23.016
                                c-0.79,1.593-2.521,2.258-4.239,1.718L55.77,17.447c-1.066-0.665-2.258-1.191-3.712-1.191H40.671c-1.33,0-2.383-1.053-2.383-2.383
                                h-2.646c0,2.785,2.259,5.029,5.028,5.029h11.525c1.98,0,3.573,1.607,3.573,3.574c0,1.981-1.605,3.574-3.573,3.574H38.289
                                c-0.79,0-1.593,0.138-2.258,0.402l-9.004,4.639c-0.527,0.141-1.054,0.264-1.455,0.264H15.64V11.228h3.712l25.683-8.215
                                c1.718-0.526,3.713-0.402,5.306,0.402l29.936,15.501C81.868,19.567,82.519,21.561,81.868,23.016"/>
                            <path d="M58.674,57.207c-0.69-8.279-7.505-15.018-15.957-15.018c-8.453,0-15.578,6.738-16.269,15.018h-2.806
                                v1.762h36.856v-1.762H58.674z M27.622,57.207c1.028-7.262,7.262-13.33,15.011-13.33c7.262,0,13.493,6.068,14.522,13.33H27.622z"/>
                            </svg>
                        </div>
                        </a>
                     </li>
                  </ul>
               </li>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle admin_user_id no_padding_top_bottom" data-toggle="dropdown"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <i class="fa fa-chevron-down down_arrow" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                     </li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                     </li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                     </li>
                  </ul>
               </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse admin_controler">
               <figure>
                  <a href="#" class="user_admin"><i class="fa fa-user-o" aria-hidden="true"></i></a>
                  <figcaption>
                     <p>Pankaj Sharma</p>
                     <ul>
                        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-inbox" aria-hidden="true"></i></a></li>
                     </ul>
                  </figcaption>
               </figure>
               <ul class="nav navbar-nav side-nav admin_menu_background admin_navigation">
                  <li><a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
                  <li class="active"><a href="#"><i class="fa fa-list-ul" aria-hidden="true"></i> Queries</a></li>
                  <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a></li>
                  <li><a href="#"><i class="fa fa-male" aria-hidden="true"></i> Teacher</a></li>
                  <li><a href="#"><i class="fa fa-university" aria-hidden="true"></i> Institute</a></li>                     
                  <li><a href="#"><i class="fa fa-bookmark" aria-hidden="true"></i> Workshop</a></li>
                  <li><a href="#"><i class="fa fa-rebel" aria-hidden="true"></i> Reviews</a></li>   
                  <li><a href="#"><i class="fa fa-book" aria-hidden="true"></i> Course Management</a></li>
                  <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Localities</a></li>
                  <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i> SEO</a></li>
                  <li><a href="#"><i class="fa fa-crosshairs" aria-hidden="true"></i> Import/Export</a></li>
                  <li><a href="#"><i class="fa fa-flag" aria-hidden="true"></i> Reports</a></li>
                  <li><a href="#"><i class="fa fa-wrench" aria-hidden="true"></i> Settings</a></li>
               </ul>
            </div>
            <!-- /.navbar-collapse -->
         </nav>
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row">
                  <aside class="col-xs-12 blue_border border_radius_12 query_model">
                     <div class="inner_container margin_top_10">
                        <div class="row">
                           <div class="col-xs-12 text-uppercase">
                              <h4 class="blue_border">Id 2011</h4>
                           </div>
                           <div class="col-xs-8 col-xs-offset-2 margin_top_20 inner_container_text blue_border white_background">
                              <div class="row">
                                 <div class="col-xs-12 admin_menu_background top_radius h1_heading">
                                    <h2 class="color_white">Looking for a <span class="text-uppercase">Dance Teacher/Institution</span></h2>
                                 </div>
                              </div>
                              <div class="row margin_top_10">
                                 <div class="col-md-9 col-sm-10 col-xs-6 localities">
                                    <p>Name : <span>Subhomoy Pal</span></p>
                                    <p>Phone : <span>123456790</span></p>
                                    <p>Email Id : <span>info@info.com</span></p>
                                    <p>Age : <span>40 Years</span></p>
                                    <p>Board/University : <span>N/A</span></p>
                                    <div class="preferred_location">
                                       <p>Locality Type:</p>
                                       <ul class="area">
                                          <li><a href="#" class="admin_menu_background">My Home</a></li>
                                          <li><a href="#" class="admin_menu_background">Tutor’s Home</a></li>
                                          <li><a href="#" class="admin_menu_background">Institue</a></li>
                                       </ul>
                                    </div>
                                    <div class="preferred_location">
                                       <p>Preferred Localities:</p>
                                       <ul class="area">
                                          <li><a href="#" class="admin_menu_background">salt lake</a></li>
                                          <li><a href="#" class="admin_menu_background">salt lake</a></li>
                                          <li><a href="#" class="admin_menu_background">salt lake</a></li>
                                       </ul>
                                    </div>
                                    <div class="preferred_location">
                                       <p class="width_100">Date of commencement: <strong>11/11/11</strong></p>
                                    </div>
                                 </div>
                                 <div class="col-md-3 col-sm-2 col-xs-12 margin_bottom_20 preference white_space_normal">
                                    <h5>Preferences: </h5>
                                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-2">
                              <div class="admin_logo pull-right">
                                 <a href="http://localhost/pedagoge/activitylog/">
                                    <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 588.601 588.6" style="enable-background:new 0 0 588.601 588.6;" xml:space="preserve">
                                       <g>
                                          <path d="M157.13,263.919c-16.938,0.264-26.702,17.052-26.702,39.293c0,22.417,10.114,38.306,26.871,38.475 c17.112,0.18,27.005-16.675,27.005-39.308C184.304,281.464,174.588,263.643,157.13,263.919z" fill="#FFFFFF"/>
                                          <path d="M577.373,86.999c0-20.838-16.954-37.8-37.8-37.8h-178.2c-0.786,0-1.561,0.076-2.342,0.124V0L11.228,46.417v494.564 L359.031,588.6v-50.814c0.781,0.053,1.551,0.115,2.342,0.115h178.2c20.846,0,37.8-16.964,37.8-37.8V86.999z M359.031,375.369 c0.417,0.259,0.881,0.448,1.403,0.448h138.602c1.492,0,2.7-1.213,2.7-2.7c0-1.497-1.208-2.7-2.7-2.7H360.435 c-0.522,0-0.986,0.19-1.403,0.448v-27c0.417,0.259,0.881,0.448,1.403,0.448h138.602c1.492,0,2.7-1.202,2.7-2.699 c0-1.487-1.208-2.7-2.7-2.7H360.435c-0.522,0-0.986,0.189-1.403,0.448v-32.4c0.417,0.259,0.881,0.448,1.403,0.448h138.602 c1.492,0,2.7-1.202,2.7-2.7c0-1.486-1.208-2.699-2.7-2.699H360.435c-0.522,0-0.986,0.189-1.403,0.447v-26.989 c0.417,0.262,0.881,0.448,1.403,0.448h138.602c1.492,0,2.7-1.215,2.7-2.7c0-1.495-1.208-2.7-2.7-2.7H360.435 c-0.522,0-0.986,0.189-1.403,0.448v-26.099h166.557v162H359.031V375.369z M101.161,359.617l-65.509-1.055V247.751l23.459-0.514 v90.495l42.05,0.369V359.617z M155.965,362.401c-32.648-0.554-51.435-26.282-51.435-58.604c0-33.998,20.735-59.891,53.259-60.626 c34.61-0.783,53.85,25.819,53.85,58.248C211.634,339.968,188.85,362.96,155.965,362.401z M240,347.098 c-10.773-10.305-16.659-25.762-16.482-43.09c0.158-39.21,28.561-62.261,67.927-63.152c15.749-0.345,28,2.434,34.059,5.194 l-5.906,22.162c-6.771-2.803-15.146-5.002-28.54-4.796c-22.723,0.355-39.675,13.323-39.675,39.121 c0,24.553,15.364,39.182,37.692,39.402c6.357,0.063,11.391-0.601,13.584-1.661v-25.328l-18.778-0.011v-21.252l44.608-0.258v64.609 c-8.87,2.727-24.438,6.145-39.928,5.891C266.557,363.551,250.847,357.808,240,347.098z M555.773,500.101 c0,8.928-7.268,16.2-16.2,16.2h-178.2c-0.796,0-1.571-0.116-2.342-0.232v-93.013h163.852c8.949,0,16.2-7.245,16.2-16.2v-210.6 c0-8.941-7.251-16.2-16.2-16.2h-15.299v26.104c0,4.725-3.828,8.554-8.548,8.554c-4.725,0-8.548-3.829-8.548-8.554v-26.104h-27 v26.104c0,4.725-3.829,8.554-8.549,8.554c-4.725,0-8.548-3.829-8.548-8.554v-26.104H413.09v26.104c0,4.725-3.829,8.554-8.549,8.554 c-4.725,0-8.548-3.829-8.548-8.554v-26.104h-27v26.104c0,4.725-3.828,8.554-8.548,8.554c-0.48,0-0.944-0.063-1.403-0.14v-34.518 h9.951v-2.7c0-4.714-3.828-8.543-8.548-8.543c-0.48,0-0.944,0.066-1.403,0.14V71.042c0.771-0.114,1.54-0.243,2.342-0.243h178.2 c8.933,0,16.199,7.27,16.199,16.2v413.103H555.773z" fill="#FFFFFF"/>
                                          <path d="M404.536,168.813c-4.725,0-8.548,3.829-8.548,8.543v2.7h17.102v-2.7C413.084,172.642,409.256,168.813,404.536,168.813z" fill="#FFFFFF"/>
                                          <path d="M454.935,168.813c-4.726,0-8.549,3.829-8.549,8.543v2.7h17.103v-2.7C463.482,172.642,459.654,168.813,454.935,168.813z" fill="#FFFFFF"/>
                                          <path d="M499.036,168.813c-4.725,0-8.548,3.829-8.548,8.543v2.7h17.102v-2.7C507.584,172.642,503.756,168.813,499.036,168.813z" fill="#FFFFFF"/>
                                       </g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                       <g></g>
                                    </svg>
                                 </a>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 text-center text-uppercase down_border_decoration admin_tree">
                                 <h2 class="blue_color">Teacher Assignment</h2>
                                 <span class="tree_divider blue_background"><span class="blue_background"></span></span>

                                 <div class="row">
                                    <div class="col-xs-4 text-center text-uppercase down_border_decoration admin_tree child_tree">
                                       <h2 class="blue_color">Query Responses</h2>
                                       <ul class="query_respons">
                                          <li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="col-xs-4 text-center text-uppercase down_border_decoration admin_tree child_tree">
                                       <h2 class="blue_color">Recommended</h2>
                                       <ul class="query_respons">
                                          <li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="col-xs-4 text-center text-uppercase down_border_decoration admin_tree child_tree">
                                       <h2 class="blue_color">Matched</h2>
                                       <ul class="query_respons">
                                          <li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12">
                                 <aside class="model_wrapper">
                                    <!--<button data-toggle="modal" data-target="#fees">New</button>-->
                                    <div class="modal fade" id="fees" role="dialog">
                                       <div class="modal-dialog width_400">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header fees">
                                                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                                <h4 class="modal-title text-center">Fees</h4>
                                             </div>
                                             <form action="" method="post">
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                         <ul class="student_fees">
                                                            <li><input type="text" name="min_fees" id="min_fees" placeholder="500"></li>
                                                            <li><input type="text" name="max_fees" id="max_fees" placeholder="700"></li>
                                                         </ul>
                                                         <h4 class="modal-title text-center padding10">Available From</h4>
                                                         <ul class="dd_mm_yy">
                                                            <li>
                                                               <select name="fees_dd" id="fees_dd">
                                                                  <option value="DD">DD</option>
                                                                  <option value="1">1</option>
                                                                  <option value="2">2</option>
                                                                  <option value="3">3</option>
                                                                  <option value="4">4</option>
                                                                  <option value="5">5</option>
                                                                  <option value="6">6</option>
                                                                  <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                  <option value="9">9</option>
                                                                  <option value="10">10</option>
                                                                  <option value="11">11</option>
                                                                  <option value="12">12</option>
                                                                  <option value="13">13</option>
                                                                  <option value="14">14</option>
                                                                  <option value="15">15</option>
                                                                  <option value="16">16</option>
                                                                  <option value="17">17</option>
                                                                  <option value="18">18</option>
                                                                  <option value="19">19</option>
                                                                  <option value="20">20</option>
                                                                  <option value="21">21</option>
                                                                  <option value="23">22</option>
                                                                  <option value="24">24</option>
                                                                  <option value="25">25</option>
                                                                  <option value="26">26</option>
                                                                  <option value="27">27</option>
                                                                  <option value="28">28</option>
                                                                  <option value="29">29</option>
                                                                  <option value="30">30</option>
                                                                  <option value="31">31</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_mm" id="fees_mm">
                                                                  <option value="MM">MM</option>
                                                                  <option value="January">January</option>
                                                                  <option value="February">February</option>
                                                                  <option value="March">March</option>
                                                                  <option value="April">April</option>
                                                                  <option value="May">May</option>
                                                                  <option value="June">June</option>
                                                                  <option value="July">July</option>
                                                                  <option value="August">August</option>
                                                                  <option value="September">September</option>
                                                                  <option value="October">October</option>
                                                                  <option value="November">November</option>
                                                                  <option value="December">December</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_yy" id="fees_yy">
                                                                  <option value="YY">YY</option>
                                                                  <option value="1980">1980</option>
                                                                  <option value="1981">1981</option>
                                                                  <option value="1982">1982</option>
                                                                  <option value="1983">1983</option>
                                                                  <option value="1984">1984</option>
                                                                  <option value="1985">1985</option>
                                                                  <option value="1986">1986</option>
                                                                  <option value="1987">1987</option>
                                                                  <option value="1988">1988</option>
                                                                  <option value="1989">1989</option>
                                                                  <option value="1990">1990</option>
                                                                  <option value="1990">1990</option>
                                                                  <option value="1991">1991</option>
                                                                  <option value="1992">1992</option>
                                                                  <option value="1993">1993</option>
                                                                  <option value="1994">1994</option>
                                                                  <option value="1995">1995</option>
                                                                  <option value="1996">1996</option>
                                                                  <option value="1997">1997</option>
                                                                  <option value="1999">1999</option>
                                                                  <option value="1999">1999</option>
                                                                  <option value="2000">2000</option>
                                                                  <option value="2001">2001</option>
                                                                  <option value="2002">2002</option>
                                                                  <option value="2003">2003</option>
                                                                  <option value="2004">2004</option>
                                                                  <option value="2005">2005</option>
                                                                  <option value="2006">2006</option>
                                                                  <option value="2007">2007</option>
                                                                  <option value="2008">2008</option>
                                                                  <option value="2009">2009</option>
                                                                  <option value="2010">2010</option>
                                                                  <option value="2011">2011</option>
                                                                  <option value="2012">2012</option>
                                                                  <option value="2013">2013</option>
                                                                  <option value="2014">2014</option>
                                                                  <option value="2015">2015</option>
                                                                  <option value="2016">2016</option>
                                                                  <option value="2017">2017</option>
                                                               </select>
                                                            </li>
                                                         </ul>
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" id="fees_mobile_number" name="fees_mobile_number" placeholder="+91-98888-88888"> </li>
                                                            <li><input type="email" id="fees_email_id" name="fees_email_id" placeholder="teacher@domain.com"> </li>
                                                            <li><textarea id="fees_message" name="fees_message" placeholder="Message" class="min_height_130"></textarea> </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Approve" class="name_address blue_background pull-left color_white" type="submit">
                                                            <input value="Unapprove" class="name_address blue_background pull-right color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                         <h4 class="modal-title text-center padding10">Internal Rating</h4>
                                                         <ul class="fees_rating">
                                                            <li>
                                                               <p>Punctuality</p>
                                                               <div class="text-right">
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                               </div>
                                                            </li>
                                                            <li>
                                                               <p>Responsiveness</p>
                                                               <div class="text-right">
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                               </div>
                                                            </li>
                                                            <li>
                                                               <p>Behaviour</p>
                                                               <div class="text-right">
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                               </div>
                                                            </li>
                                                            <li>
                                                               <p>Comm Skills</p>
                                                               <div class="text-right">
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star" aria-hidden="true"></i></a>
                                                                  <a href="#"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                               </div>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12 text-center text-uppercase margin_top_50 down_border_decoration child_tree admin_tree">
                                 <h2 class="blue_color">Finalized Teacher</h2>
                                 <div class="white_background blue_border border_radius_12 text-center finalized_teacher">
                                 	<div class="padding20">
                                        <input type="text" id="enter_query_id" name="enter_query_id" placeholder="Enter ID here" class="blue_border admin_white_background border_radius_12  text-center">  
                                        <button type="button" class="blue_background color_white text-uppercase border_none border_radius_12">Update</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12 text-center text-uppercase margin_top_50 down_border_decoration child_tree admin_tree">
                                 <h2 class="blue_color">Finalized Teacher</h2>
                                 <div class="white_background blue_border border_radius_12 text-center finalized_teacher">
                                 	<div class="h1_heading overflow_hidden">
                                    	<h5 class="admin_menu_background top_radius">Teacher ID - 2332</h5>
                                        <ul class="finalized_teacher_profile_details">
                                        	<li><a href="#" class="admin_menu_background color_white text-center border_radius_12">View Profile</a></li>
                                            <li><a href="#" class="admin_menu_background color_white text-center border_radius_12">Change</a></li>
                                        </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="row">
                              <div class="col-xs-8 col-xs-offset-2 text-center text-uppercase margin_top_50 down_border_decoration child_tree admin_tree">
                                 <h2 class="blue_color">Inbound Pament</h2>
                                 <span class="tree_divider blue_background"><span class="blue_background"></span></span>
                                 <div class="row">
                                 	<div class="col-xs-4 margin_top_20 cash_paytm_bank">
                                    	<button class="border_radius_30 blue_border active text-uppercase">Cash</button>
                                    </div>
                                    <div class="col-xs-4 margin_top_20 cash_paytm_bank">
                                    	<button class="border_radius_30 blue_border text-uppercase">Paytm</button>
                                    </div>
                                    <div class="col-xs-4 margin_top_20 cash_paytm_bank">
                                    	<button class="border_radius_30 blue_border text-uppercase">Bank</button>
                                    </div>
                                 </div>
                                 
                                 <form method="post">
                                 <div class="row margin_top_20">
                                 	<div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_demo">Demo</label><input type="radio" id="inbound_payment_demo" name="inbound_payment">
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_fees">Fees</label><input type="radio" id="inbound_payment_fees" name="inbound_payment">
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_instant">Instant</label><input type="radio" id="inbound_payment_instant" name="inbound_payment">
                                    </div>
                                     <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_fastpass">Fast Pass</label><input type="radio" id="inbound_payment_fastpass" name="inbound_payment">
                                    </div>
                                 </div>
                                 
                                 <div class="row margin_top_20">
                                 	<div class="col-xs-12 margin_top_20 cash_paytm_bank">
                                    	<ul class="date_of_payment">
                                        	<li class="text-uppercase">Expected Date of Payment</li>
                                            <li><button type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button></li>
                                        </ul>
                                        <ul class="date_of_payment">
                                        	<li class="text-capitalize">Fees</li>
                                            <li><input type="text" id="date_of_payment_fees" name="date_of_payment_fees" class="assign"></li>
                                        </ul>
                                        <ul class="date_of_payment">
                                        	<li class="text-capitalize">Cycle</li>
                                            <li>
                                            	<select id="date_of_payment_cycle" name="date_of_payment_cycle" class="assign apperence_none">
                                                    <option value=""></option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="50-50">50-50</option>
                                                    <option value="One-Time">One-Time</option>
                                                    <option value="Other">Other</option>
                                                 </select>
                                            </li>
                                        </ul>
                                    </div>
                                 </div>
                                 
                                 <div class="row margin_top_20">
                                 	<div class="col-xs-12 margin_top_20 cash_paytm_bank_update">
                                    	<p>Update on 26th June 2017 11.33 <span class="text-uppercase">AM</span></p>
                                        <div class="border_radius_12"><input type="checkbox" id="cash_receive" name="cash_receive"> <label for="cash_receive">Received</label></div>
                                    </div>
                                    <input type="text" name="invoice_number_update" id="invoice_number_update" class="invoice_number_update blue_border blue_color text-center text-uppercase" placeholder="Invoice Number">
                                 </div>
                                 
                                 <div class="row margin_top_20">
                                 	<div class="col-xs-12 outbound_payment down_border_decoration child_tree">
                                    	<h2 class="blue_color">Outbound Pament</h2>
                                        <ul class="our_teacher_share margin_top_40">
                                        	<li><label>Our Share</label> <input type="text" id="our_share" name="our_share" class="blue_border"></li>
                                            <li><label>Teacher Share</label> <input type="text" id="teacher_share" name="teacher_share" class=" blue_border"></li>
                                        </ul>
                                        <div class="blue_border white_background padding15 margin_top_30 outbond_money_payment">
                                        	<h6 class="border_radius_30 text-uppercase blue_color blue_border">Cash</h6>
                                            <div class="border_radius_12 blue_color blue_border distributor_name">
                                            	<input type="text" placeholder="Distributor's Name">
                                            </div>
                                            <h6 class="border_radius_30 text-uppercase blue_color blue_border">Paytm</h6>
                                             <div class="border_radius_12 blue_color blue_border distributor_name">
                                             	<p class="margin_bottom_10">1234567891 <button type="button" class="border_none blue_color"><i class="fa fa-trash" aria-hidden="true"></i></button></p>
                                            	<input type="text" placeholder="Mobile Number">
                                            </div>
                                            <h6 class="border_radius_30 text-uppercase blue_color blue_border">Bank</h6>
                                             <div class="border_radius_12 blue_color blue_border distributor_name">
                                             	<ul>
                                                	<li><label>Bank Name:</label> <input type="text" name="bank_name" id="bank_name"></li>
                                                    <li><label>Account holder's  Name:</label> <input type="text" name="bank_name" id="bank_name"></li>
                                                    <li><label>Account Number:</label> <input type="text" name="bank_name" id="bank_name"></li>
                                                    <li><label>IFSC Code:</label> <input type="text" name="bank_name" id="bank_name"></li>
                                                    <li><button type="button" class="pull-left admin_blue_background border_none border_radius_30 color_white">Delete</button><button type="button" class="pull-right admin_blue_background border_none border_radius_30 color_white">Add another Bank</button></li>
                                                </ul>
                                            </div>
                                            
                                            <div class="row margin_top_20">
                                                <div class="col-xs-12 cash_paytm_bank_update">
                                                    <div class="border_radius_12"><input type="checkbox" id="cash_receive" name="cash_receive"> <label for="cash_receive">Received</label></div>
                                                    <p class="margin_bottom_10">Update on 26th June 2017 11.33 <span class="text-uppercase">AM</span></p>
                                                </div>
                                                <input type="text" name="invoice_number_update" id="invoice_number_update" class="invoice_number_update blue_border blue_color text-center text-uppercase" placeholder="Invoice Number">
                                           </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <button type="button" class="admin_blue_background color_white border_radius_30 border_none text-uppercase margin_top_20 update_query">Update Query</button>
                                    
                                 </div>
                                 
                                 </form>
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                     </div>
                  </aside>
               </div>
            </div>
         </div>
      </div>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/jquery-2.1.1.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/bootstrap.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/jquery.slimscroll.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/datepicker.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/style.js"></script>
	  <script type="text/javascript">
      	/* Date Picker */

	$(document).ready(function(){
			var date_input=$('input[name="date"]'); //our date input has the name "date"
			var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
			date_input.datepicker({
				format: 'mm/dd/yyyy',
				container: container,
				todayHighlight: true,
				autoclose: true,
			})
		})

/* Date Ppicker */
      </script>
	  
   </body>
</html>

