 <?php
 if(isset($_GET['type']) && $_GET['type'] = 'interest')
 {
    $current_url=home_url()."/teacherdashboard/?page_type=interested&query_id=".$_GET['query_id'];
    $login_url=home_url().'/login/?nexturl='.urlencode("/teacherdashboard/?page_type=interested&query_id=".$_GET['query_id']);
 }
 else if(isset($_GET['type']) && $_GET['type'] = 'disapprove')
 {
 	$current_url=home_url()."/teacherdashboard/?page_type=interested&query_id=".$_GET['query_id'];
	$login_url=home_url().'/login/?nexturl='.urlencode("/teacherdashboard/?page_type=interested&query_id=".$_GET['query_id']);
 }
 else
 {
    $current_url=home_url()."/teacherdashboard/?page_type=referrals&query_id=".$_GET['query_id'];
    $login_url=home_url().'/login/?nexturl='.urlencode("/teacherdashboard/?page_type=referrals&query_id=".$_GET['query_id']);   
 }
 //echo $current_url."<br />".$login_url;
 $query_id=$_GET['query_id'];
 $query_model = new ModelQuery();
 $queries=$query_model->get_query_details_view($query_id);

 ?>
 <section class="body_wrapper margin_top_20 min_height_768">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-1 col-md-1 col-xs-12 ad_section">
                	
                </div>
                <div class="col-sm-10 col-md-10 col-xs-12 queries">
                    
                       <div class="inner_container margin_top_10 margin_bottom_30">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                  <?php
                                 if(count($queries)>0){
                                 foreach($queries AS $res){
                                 $localities = $query_model->get_q_locality($res->localities);
                                 ?>
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-6 col-sm-6 col-xs-12 localities">
                                            	<p>Year/Standard: <?php echo $res->standard; ?></p>
                                                <p>Board/University: <?php echo $res->academic_board; ?></p>
                                                <div class="preferred_location">
                                                    <p>Locality Type:</p> 
                                                    <ul class="area">
                                                        <?php if($res->myhome==1){ ?>
                                                         <li><a href="#" class="blue_background">My Home</a></li>
                                                         <?php } ?>
                                                         <?php if($res->tutorhome==1){ ?>
                                                         <li><a href="#" class="blue_background">Tutors Home</a></li>
                                                         <?php } ?>
                                                         <?php if($res->institute==1){ ?>
                                                         <li><a href="#" class="blue_background">Institue</a></li>
                                                         <?php } ?> 
                                                    </ul>
                                                </div>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <?php foreach($localities AS $loc){ ?>
                                                            <li><a href="#" class="blue_background"><?php echo $loc; ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                                <div class="preferred_location">
                                                    <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
                                                </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
                                         	<h5>Preferences: </h5>
                                            <p class="text-justify"><?php echo trim($res->requirement); ?></p>
                                         </div>
                                          <?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                            <ul class="pull-right side_right_menu side_right_menu_media text-center unic_link">
                                            <?php
                                             if($_SESSION['member_id']!=''){
                                                if(isset($_GET['type']) && $_GET['type'] = 'interest')
                                                {
                                                    ?>
                                                    <li><a href="<?php echo $current_url; ?>">Go to interested tab</a></li>
                                                    <?php
                                                }
                                                else if(isset($_GET['type']) && $_GET['type'] = 'disapprove'){
                                                ?>
                                                <li><a href="<?php echo $current_url; ?>">Go to interested tab</a></li>
                                                <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <li><a href="<?php echo $current_url; ?>">Go to referrals tab</a></li>
                                                    <?php
                                                }
				                ?>

                                             <?php
                                             }else{
                                             	if(isset($_GET['type']) && ($_GET['type'] = 'interest' || $_GET['type'] = 'disapprove'))
                                                {
                                                ?>
                                                <li><a href="<?php echo $login_url; ?>">Login</a></li>
                                                <?php
                                                }
                                                else
                                                {
                                             ?>
                                            	<li><p>To showinterest in this Query please Login/register here</p></li>
                                            	<li><a href="<?php echo $login_url; ?>">Login / Register</a></li>
                                             <?php 
                                             }
                                             }
                                             ?>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       
                </div>
                <div class="col-sm-1 col-md-1 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>