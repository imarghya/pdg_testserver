<?php
	include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
	$str_workshop_link = isset($workshop_data['workshop_view_url']) ? $workshop_data['workshop_view_url'] : '#';
	$str_workshop_name = isset($workshop_data['workshop_name']) ? $workshop_data['workshop_name'] : '';
?>
<!-- Content section starts -->

<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
		<h3>Congratulations!</h3>
		
		<p>Your workshop has been approved! Please visit the workshop page to see your listing!</p>
		
		<p><a href="<?= $str_workshop_link; ?>"><?= $str_workshop_name; ?></a> </p>
		
		<strong>Team Pedagoge</strong></strong><br>
	</td>
</tr>
<!-- Content section ends -->
<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>