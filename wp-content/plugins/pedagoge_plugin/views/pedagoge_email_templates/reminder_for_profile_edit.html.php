<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Incomplete profile Notification!</b><br/>
    </td>
</tr>
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Respected <?php echo $user_name; ?>,</b><br>
		
		<b>Thank You for registering on Pedagoge!</b><br><br>

		We see that you have made a profile on Pedagoge, however, the profile cannot be published as it is incomplete. Users on the platform won't be able to view your profile until it is published. <br />
		
		Update your profile with relevant details to start accepting students today! This will hardly take 10 minutes. Follow the link given below to update your profile: 
		
		<a href="http://www.pedagoge.com/login" target="_blank">Login to Pedagoge!</a><br/><br/>
		
		For any queries or clarifications, please email to ask@pedagoge.com or call us at +91-9073107508. <br /><br /><b>Happy Teaching!</b><br />
		
		<b>Team Pedagoge</b><br>
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>