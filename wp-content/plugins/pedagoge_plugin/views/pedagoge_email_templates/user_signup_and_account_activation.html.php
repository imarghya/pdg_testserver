<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');

//password_reset_url
$pdg_account_email = isset($to_address) ? $to_address : 'Email Not available.';
$pdg_account_activation_url = isset($account_activation_url) ? $account_activation_url : $home_url;
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Just one more step...</b><br/>
        
        Click the big button below to activate your account. <br />        
    </td>
</tr>
<tr>
    <td align="center" bgcolor="#f9f9f9" style="padding: 30px 20px 30px 20px; font-family: Arial, sans-serif;">
    	<b>Account: <?php echo $pdg_account_email; ?></b><br />
        <table bgcolor="#d9416c" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
            <tr>
                <td align="center" height="50" style=" padding: 0 25px 0 25px; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold;" class="button">
                    <a href="<?php echo $pdg_account_activation_url; ?>" target="_blank" style="color: #ffffff; text-align: center; text-decoration: none;">Activate Account</a>
                </td>
            </tr>
        </table>
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>