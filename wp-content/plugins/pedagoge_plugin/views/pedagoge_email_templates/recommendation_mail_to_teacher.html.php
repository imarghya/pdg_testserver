<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Congratulation! Your Pedagoge profile has just been recommended by a user "<?php echo $user_name; ?>"!</b><br/><br />
        
        You may visit your profile by clicking on <a href="<?php echo $teacher_institute_profile_link; ?>" target="_blank">this link!</a> <br/><br/>
        Thank you again! <br />
        <b>Pedagoge Team!</b>
    </td>
</tr>
<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>