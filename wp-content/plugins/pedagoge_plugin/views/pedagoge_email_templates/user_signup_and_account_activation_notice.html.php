<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');

$pdg_user_name = isset($user_name) ? $user_name : 'Info Not available.';
$pdg_user_email = isset($user_email) ? $user_email : 'Info Not available.';
$pdg_user_role = isset($user_role ) ?  $user_role : 'Info Not available.';
?>
            
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>User Signup notification!</b><br /><br />
        This email is to inform you that a new user has just registered with the website. Below are the related information! <br /><br />
        <b>User Name: </b> <?php echo $pdg_user_name; ?><br />
        <b>User Email: </b> <?php echo $pdg_user_email; ?><br />
        <b>User Role: </b> <?php echo $pdg_user_role; ?><br />
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>            