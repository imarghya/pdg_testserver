<?php
	include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
	$workshop_id = isset($workshop_data['workshop_id']) ? $workshop_data['workshop_id'] : '0';
	$str_workshop_link = isset($workshop_data['workshop_view_url']) ? $workshop_data['workshop_view_url'] : '#';
	$str_workshop_name = isset($workshop_data['workshop_name']) ? $workshop_data['workshop_name'] : '';
	
	$participant_name  = isset($additional['participant_name']) ? $additional['participant_name'] : '0';
	$participant_mobile = isset($additional['participant_mobile']) ? $additional['participant_mobile'] : '0';
	$total_seats_booked = isset($additional['total_seats_booked']) ? $additional['total_seats_booked'] : '0';	
?>
<!-- Content section starts -->

<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
		<h3>Hello!</h3>
		
		<p>Some one has just booked seat/s for your workshop!</p>
		
		<p>Some one just booked seat/s for a workshop! Below are the details:</p>
		<table width="100%" border="1">
			<tr>
				<td><strong>Workshop ID</strong></td>
				<td><?= $workshop_id; ?></td>
			</tr>
			<tr>
				<td><strong>Workshop Link</strong></td>
				<td><a href="<?= $str_workshop_link; ?>"><?= $str_workshop_name; ?></a></td>
			</tr>
			
			<tr>
				<td><strong>Participant Name</strong></td>
				<td><?= $participant_name; ?></td>
			</tr>						
			<tr>
				<td><strong>Seats Booked</strong></td>
				<td><?= $total_seats_booked; ?></td>
			</tr>
			<tr>
				<td><strong>Date of booking</strong></td>
				<td><?php echo date('d-m-Y h:i A') ?></td>
			</tr>
		</table>
		
		<p>Pedagoge Team will contact you soon with more details!</p>
		
		<strong>Team Pedagoge</strong></strong><br>
	</td>
</tr>

<!-- Content section ends -->
<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>