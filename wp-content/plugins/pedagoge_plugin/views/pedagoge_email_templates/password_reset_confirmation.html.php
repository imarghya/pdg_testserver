<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
$pdg_account_email = isset($to_address) ? $to_address : 'Email Not available.';
?>
            
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Yay! Password Reset successful!</b> <br /><br />
        This email is to confirm that your Pedagoge Account's password has been changed successfully!<br /><br />					
		<strong>Team Pedagoge</strong></strong><br />
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>            