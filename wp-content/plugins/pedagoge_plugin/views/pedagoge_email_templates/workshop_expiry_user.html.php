<?php
	include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
	$workshop_id = isset($workshop_data['workshop_id']) ? $workshop_data['workshop_id'] : '0';
	$str_workshop_link = isset($workshop_data['workshop_view_url']) ? $workshop_data['workshop_view_url'] : '#';
	$str_workshop_name = isset($workshop_data['workshop_name']) ? $workshop_data['workshop_name'] : '';
?>
<!-- Content section starts -->

<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
		<h3>Hello!</h3>
		
		<p>Your workshop time line has expired! </p>
		
		<table width="100%" border="1">
			<tr>
				<td><strong>Workshop ID</strong></td>
				<td><?= $workshop_id; ?></td>
			</tr>
			<tr>
				<td><strong>Workshop Link</strong></td>
				<td><a href="<?= $str_workshop_link; ?>"><?= $str_workshop_name; ?></a></td>
			</tr>
		</table>
		
		<p><strong>You can create a new workshop again by following the link <a href="<?= home_url('/manageworkshop'); ?>">Create a workshop</a></strong></p>
		
		<strong>Team Pedagoge</strong></strong><br>
	</td>
</tr>

<!-- Content section ends -->
<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>