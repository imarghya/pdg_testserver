<?php
include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
$str_profile_submission_update_type = 'Lead from Institution Page!';
?>
	<tr>
		<td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
			<b><?php echo $str_profile_submission_update_type; ?>!</b><br/><br/>
			<p><strong>Full Name</strong> : <?php echo $template_vars['fullname']; ?></p>
			<p><strong>Institute/School Name</strong> : <?php echo $template_vars['inst_school_name']; ?></p>
			<p><strong>Designation</strong> : <?php echo $template_vars['designation']; ?></p>
			<p><strong>Phone</strong> : <?php echo $template_vars['phoneno']; ?></p>
			<p><strong>Email ID</strong> : <?php echo $template_vars['emailid']; ?> </p>

			<strong>Team Pedagoge</strong><br>
		</td>
	</tr>

<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>