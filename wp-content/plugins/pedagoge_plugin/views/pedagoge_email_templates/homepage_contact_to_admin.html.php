<?php
include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
$str_profile_submission_update_type = 'Someone has contacted us | Home Page!';
?>
	<tr>
		<td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
			<b><?php echo $str_profile_submission_update_type; ?>!</b><br/><br/>
			<p><strong>Name</strong> : <?php echo $template_vars['name']; ?></p>
			<p><strong>Phone</strong> : <?php echo $template_vars['phone']; ?></p>
			<p><strong>Localities</strong> : <?php echo $template_vars['locality']; ?> </p>
			<p><strong>Subjects</strong> : <?php echo $template_vars['subject']; ?> </p><br />
			<strong>Team Pedagoge</strong></strong><br>
		</td>
	</tr>

<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>