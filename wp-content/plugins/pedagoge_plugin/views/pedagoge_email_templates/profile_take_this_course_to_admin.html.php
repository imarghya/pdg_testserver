<?php
include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
$str_profile_submission_update_type = 'Someone has requested for a Specific Course callback | Profile Page!';
?>

	<tr>
		<td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
			<b> <?php echo $str_profile_submission_update_type; ?>!</b><br/><br/>
			<p><strong>Teacher's Name</strong> : <?php echo $template_vars['teachername']; ?></p>
			<p><strong>Page URL</strong> : <?php echo $template_vars['pageurl']; ?></p>
			<p><strong>Name</strong> : <?php echo $template_vars['name']; ?></p>
			<p><strong>Email</strong> : <?php echo $template_vars['email']; ?></p>
			<p><strong>Phone</strong> : <?php echo $template_vars['phone']; ?></p>
			<p><strong>Tuition Locations</strong> : <?php echo $template_vars['tuition_location']; ?>'s Place </p>
			<p><strong>Localities</strong> : <?php echo $template_vars['locality']; ?> </p>
			<p><strong>Subjects</strong> : <?php echo $template_vars['subject']; ?> </p><br/>
			<p><strong>Fees</strong> : <?php echo $template_vars['fees']; ?> </p><br/>
			<p><strong>Batch Size</strong> : <?php echo $template_vars['batchsize']; ?> </p><br/>
			<p><strong>Length</strong> : <?php echo $template_vars['length']; ?> </p><br/>
			<p><strong>Time</strong> : <?php echo $template_vars['time']; ?> </p><br/> <strong>Team
				Pedagoge</strong></strong><br>
		</td>
	</tr>

<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>