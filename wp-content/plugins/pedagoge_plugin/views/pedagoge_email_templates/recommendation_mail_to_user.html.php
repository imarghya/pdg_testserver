<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Thank you for recommending the Teacher!</b><br/><br />        
        Your recommendation has been registered in the database and a notification has been sent to the teacher.<br/>
        We appreciate your recommendation!<br/><br/>
        Thank you again! <br />
        <b>Pedagoge Team!</b>
    </td>
</tr>
<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>