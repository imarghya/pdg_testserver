<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
            
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Congratulations! Your profile has been approved and activated!</b> <br /><br />
        You may visit the profile by clicking on the link below:<br /><br />
        <b><a href="<?php echo $profile_link; ?>">View Your Profile</a></b><br /><br />					
		<strong>Team Pedagoge</strong></strong><br />
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>            