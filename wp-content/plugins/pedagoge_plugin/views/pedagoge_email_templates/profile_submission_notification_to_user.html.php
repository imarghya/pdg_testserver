<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Thank you for updating your profile!</b><br/><br />
        
        Your profile information has been updated in the database and will become active after verification and approval at our end.<br /><br/>
        You will receive a notification email as soon as we approve your profile! You may contact us to expedite the approval process! <br /><br />
        Thank you again! <br />
        <b>Pedagoge Team!</b>
    </td>
</tr>
<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>