<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>A user has just recommened a teacher at pedagoge website!</b><br/><br />
        Please find the relevant information below! <br /><br />
        <b>Teacher Name: </b> <?php echo $teacher_institute_name; ?><br />
        <b>Teacher Email: </b> <?php echo $teacher_institute_email_address; ?><br />
        <b>Teacher Type: </b> <?php echo $teacher_type; ?><br /><br />
        
        <b>User Name: </b> <?php echo $user_name; ?><br />
        <b>User Email: </b> <?php echo $user_email_address; ?><br /><br />
        <b>Teacher Profile Link: </b> <a href="<?php echo $teacher_institute_profile_link; ?>" target="_blank">Click to view</a> <br /><br />
    </td>
</tr>
<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>