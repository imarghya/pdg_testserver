<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Thank you for submitting your review!</b><br/><br />        
        Your review has been received and it will be published as soon as we verify and approve it.<br/>
        You will be notified as soon as we approve the review!<br/><br/>
        Thank you again! <br />
        <b>Pedagoge Team!</b>
    </td>
</tr>
<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>