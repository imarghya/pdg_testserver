<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');

$pdg_account_email = isset($to_address) ? $to_address : 'Email Not available.';
?>
 <tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Welcome to Pedagoge!</b><br/>
    </td>
</tr>
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b>Thank you very much for choosing Pedagoge!</b><br><br>
		
		Pedagoge is an online platform for students, to search and connect with private coaches		
		(academic & non-academic), based on individual needs.<br />
		
		Reviews and recommendations given by organically built trusted community of students and teachers facilitate well-informed decisions.<br><br>
		
		Please use this email address to help us identify you as one of our first members.<br><br>
		
		We hope you enjoy this opportunity to take Pedagoge for a spin. Feel free to test it out and get 
		
		acquainted with no limits and no obligation on our free-to-use platform.<br><br>
		
		Welcome aboard!<br>
		
		<b>Team Pedagoge</b><br>
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>