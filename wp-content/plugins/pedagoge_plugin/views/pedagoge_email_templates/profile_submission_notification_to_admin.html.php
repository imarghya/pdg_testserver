<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
$str_profile_submission_update_type = 'Profile Update Notification!';
if($approved == 'no'){
	$str_profile_submission_update_type = 'Profile Submission Notification!';
}

?>
            
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b> <?php echo $str_profile_submission_update_type; ?>!</b><br /><br />
        This email is to inform you that a user has just updated the profile pending approval. Below are the related information! <br /><br />
        <b>User Name: </b> <?php echo $teacher_institute_name; ?><br />
        <b>User Email: </b> <?php echo $teacher_institute_email_address; ?><br />
        <b>User Mobile: </b> <?php echo $teacher_mobile_number; ?><br />
        <b>User Type: </b> <?php echo $user_role; ?><br /><br />
        <b>Profile Link: </b> <a href="<?php echo $teacher_institute_profile_link; ?>" target="_blank">Click to view</a> <br /><br />        
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>            