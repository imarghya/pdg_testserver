<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');

	$teacher_name='';
	$teacher_email = '';
	$teacher_contact_number = '';
	$teacher_role = '';
	
	$student_name = '';
	$student_email = '';
	$student_contact_number = '';
	$student_role = '';
	
	$review_new_id = isset($review_id) ? $review_id : '';
	
	$review_text = '';
	$rating1 = '';
	$rating2 = '';
	$rating3 = '';
	$rating4 = '';
	$average_rating = '';
	$overall_rating = '';
	$is_anonymous = '';
	$secret_key = '';
		
	if(isset($teacher_data)) {
		foreach($teacher_data as $teacher_info) {
			$teacher_name = $teacher_info->teacher_name;
			$teacher_email = $teacher_info->user_email;
			$teacher_contact_number = $teacher_info->mobile_no;
			$teacher_role = $teacher_info->user_role_display;
		}
	}
	
	if(isset($student_data)) {
		foreach($student_data as $student_info) {
			$student_name = $student_info->first_name.' '.$student_info->last_name;
			$student_email = $student_info->user_email;
			$student_contact_number = $student_info->mobile_no;
			$student_role = $student_info->user_role_display;
		}
	}
	if(isset($review_data)) {		
		$review_text = $review_data['review_text'];
		$rating1 = $review_data['rating1'];
		$rating2 = $review_data['rating2'];
		$rating3 = $review_data['rating3'];
		$rating4 = $review_data['rating4'];
		$average_rating = $review_data['average_rating'];
		$overall_rating = $review_data['overall_rating'];
		$is_anonymous = $review_data['is_anonymous'];
	}
	
	$accept_link = add_query_arg( array(
		'id'=>$review_new_id,
		'action'=>'accept'
	), home_url('/reviews') );
	
	$reject_link = add_query_arg( array(
		'id'=>$review_new_id,
		'action'=>'reject'
	), home_url('/reviews') );
	
	$green_button = "
		background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 15px 32px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;";
	
	$red_button = "
		background-color: #f44336;
	    border: none;
	    color: white;
	    padding: 15px 32px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;
	";
?>

            
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b> A review has been submitted and pending your approval!</b><br /><br />
        
        <h4>Teacher/Institute's Details</h4>
		<table width="100%" border="1">
			<tr>
				<td><strong>Name</strong></td>
				<td><?php echo $teacher_name; ?></td>
				<td><strong>Role</strong></td>
				<td><?php echo $teacher_role; ?></td>
			</tr>
			<tr>
				<td><strong>Email</strong></td>
				<td><?php echo $teacher_email; ?></td>
				<td><strong>Contact</strong></td>
				<td><?php echo $teacher_contact_number; ?></td>
			</tr>
		</table>
		<hr />
		<h4>Student/Guardian's Details</h4>
		<table width="100%" border="1">
			<tr>
				<td><strong>Name</strong></td>
				<td><?php echo $student_name; ?></td>
				<td><strong>Role</strong></td>
				<td><?php echo $student_role; ?></td>
			</tr>
			<tr>
				<td><strong>Email</strong></td>
				<td><?php echo $student_email; ?></td>
				<td><strong>Contact</strong></td>
				<td><?php echo $student_contact_number; ?></td>
			</tr>
		</table>
		<hr />
		<h4>Review Details</h4>
		<table width="100%" border="1">
			<tr>
				<td colspan="8" align="center"><strong>Review Text</strong></td>
			</tr>
			<tr>
				<td colspan="8"><?php echo $review_text; ?></td>
			</tr>
			<tr>
				<td><strong>Rating 1</strong></td>
				<td><?php echo $rating1; ?></td>
				<td><strong>Rating 2</strong></td>
				<td><?php echo $rating2; ?></td>
				<td><strong>Rating 3</strong></td>
				<td><?php echo $rating3; ?></td>
				<td><strong>Rating 4</strong></td>
				<td><?php echo $rating4; ?></td>
			</tr>
			<tr>
				<td><strong>Average Rating</strong></td>
				<td><?php echo $average_rating; ?></td>
				<td><strong>Overall Rating</strong></td>
				<td><?php echo $overall_rating; ?></td>
				<td></td>
				<td colspan="2"><strong>Is Anonymous</strong></td>							
				<td><?php echo $is_anonymous; ?></td>
			</tr>						
		</table>
		<hr />
		<table width="100%" border="0">
			<tr>
				<td width="50%">
					<a href="<?php echo $reject_link; ?>" style="<?php echo $red_button; ?>">Reject Review</a>
				</td>
				<td width="50%">
					<a href="<?php echo $accept_link; ?>" style="<?php echo $green_button; ?>">Accept Review</a>
				</td>
			</tr>
		</table>
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>            