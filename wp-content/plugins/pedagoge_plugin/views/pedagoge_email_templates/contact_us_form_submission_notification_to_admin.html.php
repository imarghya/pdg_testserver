<?php
include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_header.html.php');
?>
            
<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
        <b> A Visitor has just submitted the contact form!</b><br /><br />
        Below are the related information! <br /><br />
        <b>Contact Name: </b> <?php echo $contact_name; ?><br />
        <b>Contact Email: </b> <?php echo $contact_email; ?><br />
        <b>Contact No: </b> <?php echo $contact_number; ?><br />
        <b>Message: </b> <?php echo $contact_message; ?><br /><br />                
    </td>
</tr>

<?php include(PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.'common_footer.html.php'); ?>