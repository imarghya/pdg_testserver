<?php
	include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_header.html.php' );
	$workshop_id = isset($workshop_data['workshop_id']) ? $workshop_data['workshop_id'] : '0';
	$str_workshop_link = isset($workshop_data['workshop_view_url']) ? $workshop_data['workshop_view_url'] : '#';
	$str_workshop_name = isset($workshop_data['workshop_name']) ? $workshop_data['workshop_name'] : '';
	$edit_workshop_link = isset($workshop_data['workshop_edit_url']) ? $workshop_data['workshop_edit_url'] : '#';
	$requested_to_be_featured = isset($workshop_data['requested_to_be_featured']) ? $workshop_data['requested_to_be_featured'] : 'no';
	
	$str_featured = '';
	if($requested_to_be_featured == 'yes') {
		$str_featured = '<h3 style="color:#f88379;">Trainer had requested for his workshop to be featured!</h3>';
	}
?>
<!-- Content section starts -->

<tr>
    <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
		<h3>Hello!</h3>
		<p>A workshop has been updated! Below are the details:</p>
		<table width="100%" border="1">
			<tr>
				<td><strong>Workshop ID</strong></td>
				<td><?= $workshop_id; ?></td>
			</tr>
			<tr>
				<td><strong>Workshop Link</strong></td>
				<td><a href="<?= $str_workshop_link; ?>"><?= $str_workshop_name; ?></a></td>
			</tr>
			<tr>
				<td><strong>Edit Workshop</strong></td>
				<td><a href="<?= $edit_workshop_link; ?>">Edit Workshop</a></td>
			</tr>						
			<tr>
				<td><strong>Date of update</strong></td>
				<td><?php echo date('d-m-Y h:i A') ?></td>
			</tr>
		</table>
		<hr />
		<?= $str_featured; ?>
		<hr />
		<strong>Team Pedagoge</strong></strong><br>
	</td>
</tr>

<!-- Content section ends -->
<?php include( PEDAGOGE_PLUGIN_DIR . 'views/pedagoge_email_templates/' . 'common_footer.html.php' ); ?>