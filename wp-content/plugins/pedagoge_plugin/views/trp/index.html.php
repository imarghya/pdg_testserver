<?php
	$amazon_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/amazon.png'.'" width="120px" height="120px" class="col-xs-11"/>';
	$flipkart_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/flipkart.png'.'" width="120px" height="120px" class="col-xs-11"/>';
	$freecharge_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/freecharge.png'.'" width="120px" height="120px" class="col-xs-11"/>';
	$paytm_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/paytm.png'.'" width="120px" height="120px" class="col-xs-11"/>';
	$snapdeal_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/snapdeal.png'.'" width="120px" height="120px" class="col-xs-11"/>';

	$amazon_img_sm = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/amazon.png'.'" width="60px" height="60px"/>';
	$flipkart_img_sm = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/flipkart.png'.'" width="60px" height="60px"/>';
	$freecharge_img_sm = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/freecharge.png'.'" width="60px" height="60px"/>';
	$paytm_img_sm = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/paytm.png'.'" width="60px" height="60px"/>';
	$snapdeal_img_sm = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/snapdeal.png'.'" width="60px" height="60px"/>';

	$carousel_1_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/trp_left.png" style="width: 292.5px; height: 630px;">';
	$carousel_2_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/trp_right.png" style="width: 292.5px; height: 630px;">';

	$quote_img = '<img src="'.PEDAGOGE_ASSETS_URL.'/images/trp/quote.png'.'" style="width: 60%; height: 60%;" class="col-xs-18"/>';

	$terms_conditions_url = PEDAGOGE_ASSETS_URL.'/files/TRP-Terms&Conditions.pdf';
?>
<!-- Page Head Starts -->
<section class="site-section site-section-trp site-section-top">
	<div class="head">
		<!-- <h1 class="text_center" style="font-size: 50px;">Pedagoge</h1> -->
		<h2 class="trp_header_TRP text_center">Teacher Referral Program</h2></br>
	</div>

<!-- Page Head Ends -->


<!-- Brand Bar Starts -->
	</br>
	<div class="quote hidden-md hidden-xs">
		<center><?php echo $quote_img; ?></center></br></br></br>
	</div>

	<div class="visible-sm visible-xs visible-md visible-lg text_center">
		<h2 style="font-weight: bold;">Earn Rs 5/- for every entry submitted!</h2>
	</div></br></br>

	<div class="container visible-md visible-lg hidden-xs hidden-sm">
		<div class="row">
			<div class="col-md-2 col-md-offset-1 col-xs-3 col-xs-offset-2">
				<?php echo $freecharge_img; ?>
			</div>
			<div class="col-md-2 col-xs-3">
				<?php echo $paytm_img; ?>
			</div>
			<div class="col-md-2 col-xs-3">
				<?php echo $amazon_img; ?>
			</div>
			<div class="col-md-2 col-xs-3">
				<?php echo $snapdeal_img; ?>
			</div>
			<div class="col-md-2 col-xs-3">
				<?php echo $flipkart_img; ?>
			</div>
		</div>
	</div></br></br>

	<div class="container visible-xs visible-sm hidden-md hidden-lg" style="padding-right: 10%;">
		<h3 class="text_center">We provide coupons and recharges from the following sites:</h3>
		<div class="row">
			<div class="col-xs-3 col-sm-2">
				<?php echo $freecharge_img_sm; ?>
			</div>
			<div class="col-xs-3 col-sm-2">
				<?php echo $paytm_img_sm; ?>
			</div>
			<div class="col-xs-3 col-sm-2">
				<?php echo $amazon_img_sm; ?>
			</div>
			<div class="col-xs-3 col-sm-2">
				<?php echo $snapdeal_img_sm; ?>
			</div>
			<!-- <div class="col-xs-3 col-sm-2">
			<?php echo $flipkart_img_sm; ?>
			</div> -->
		</div></br></br>
	</div>
<!-- Brand Bar Ends -->
<div class="container">
	<div class="row">
		<div class="col-md-3 hidden-xs hidden-sm">
			<?php echo $carousel_1_img; ?>
		</div>
		<div class="col-md-6" style="padding-right: 0px;">
			<div class="form_box">		
				<div id="trp_error"></div>
					<a href="<?php echo $terms_conditions_url; ?>" target="_blank" class="pull-right terms">Read T&amp;C</a>
					<h2 class="text_center" style="font-weight: bold;">Teacher Referral Form</h2>
					<div class="form">
						<label>Teacher/Institute Name*</label>
						<input type="text" id="teacher_institute_name" class="form-control" placeholder="Name"/>
						<label>Contact Person Phone*</label>
						<input type="text" id="teacher_institute_phone" class="positive form-control" placeholder="Phone"/>
						<label>Contact Person Email</label>
						<input type="text" id="teacher_institute_email" class="form-control" placeholder="Email"/>
						<label>Subjects Taught*</label>
						<input type="text" id="teacher_institute_subjects" class="form-control" placeholder="Subject Taught">
						<label>Locality*</label>
						<input type="text" id="locality" class="form-control" placeholder="Enter Locality"/>
					</div></br>	</br>
				<button type="button" id="cmd_trp_submit" class="btn btn-success text_center trp_btn_submit col-md-5 col-xs-5 bold">SUBMIT</button></br></br>
				<button type="button" id="cmd_trp_add" class="btn btn-success text_center trp_btn_add col-md-7 col-xs-7 bold">ADD MORE</button></br></br>				
				<button type="button" id="cmd_trp_submit_multiple" class="btn btn-success text_center trp_btn_submit_multiple bold col-md-8 col-xs-8">SUBMIT MULTIPLE</button></br></br>
			</div>
		</div>
		<div class="col-md-3 hidden-xs hidden-sm" style="padding-left: 0px;">
			<?php echo $carousel_2_img; ?>
		</div>
	</div>
</div></br></br>

<!-- User details modal starts -->
<div class="modal fade" id="trp_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

	<div class="modal-dialog modal-lg" role="document">

	    <div class="modal-content">
		    <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title" id="myModalLabel">Provide your details</h4>
                <center>
	                <span class="loader_loading hidden_item">Loading Please wait</span>
	                <img class="img_locality_loader hidden_item" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="Loading...Please Wait" />
				</center>                 
	         </div>
	    	<div class="modal-body" id="trp_modal_body">
		        <label>Name*</label>
		        <input type="text" id="user_name" class="form-control"/>
		        <label>Email</label>
		        <input type="text" id="user_email" class="form-control"/>
		        <label>Contact Number*</label>
		        <input type="text" id="user_contact_number" class="positive form-control"/>                                               
		                        
		    	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	<button type="button" class="btn btn-primary" id="cmd_save_trp_user_modal">Save</button>
		    	</div>
				<div id="trp_modal_error"></div>
	        </div>
	    </div>
	</div>
</div>
<!-- User details modal ends -->
</section>
