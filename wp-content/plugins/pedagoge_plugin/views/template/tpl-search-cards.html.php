{{#items}}
<div class="col-xs-12 card container-fluid margin-bottom-10 no-select search-clickable-card"
     data-url="<?= home_url(); ?>/{{& profile_type}}/{{& url}}">
	<div class="row">
		<div class="col-xs-0 col-md-2 hidden-xs hidden-sm search_result_item">
			<div class="profile_data_img_thumbnail">
				<a href="<?= home_url(); ?>/{{& profile_type}}/{{& url}}" target="_blank" class="search_result_profile_view_link">
					<img data-src="{{& profile_data_img}}" alt="{{& name}}" class="profile_pic {{& profile_data_img_orientation}}" title="{{& name}}"></a>
			</div>
		</div>
		<div class="col-xs-7 search_result_item">
			<div class="col-xs-12 col-md-6 search_result_item">
				<div class="search_result_item_block">
					<div class="inline text-bold item-desc">Name</div>
					:
					<div class="inline name item">{{& name}}</div>
				</div>
				<div class="search_result_item_block visible-xs visible-sm ">
					<div class="inline text-bold item-desc">Experience</div>
					:
					<div class="inline experience item">{{& experience}}</div>
				</div>
				<div class="search_result_item_block">
					<div class="inline text-bold item-desc">Locations</div>
					:
					<div class="inline locality item">{{& locations}}</div>
				</div>
				<div class="search_result_item_block">
					<div class="inline text-bold item-desc">Subject</div>
					:
					<div class="inline subject item">{{& subjects}}</div>
				</div>
				<div class="search_result_item_block  visible-xs visible-sm">
					<div class="inline text-bold item-desc">Starting From (&#x20B9;)</div>
					:
					<div class="inline fees item">{{& fees}}</div>
				</div>
			</div>
			<div class="col-xs-0 col-md-5 hidden-xs hidden-sm search_result_item">
				<div class="search_result_item_block">
					<div class="inline text-bold item-desc">Experience</div>
					:
					<div class="inline experience item">{{& experience}}</div>
				</div>
				<div class="search_result_item_block">
					<div class="inline text-bold item-desc">Type of Location</div>
					:
					<div class="inline item comma-seperated">
						{{#place_of_teaching}}<span>{{& img_teaching_title}}</span>{{/place_of_teaching}}
					</div>
				</div>
				<div class="search_result_item_block">
					<div class="inline text-bold item-desc">Starting From (&#x20B9;)</div>
					:
					<div class="inline fees item">{{& fees}}</div>
				</div>
			</div>
		</div>
		<div class="col-xs-5 col-md-3 container-fluid ">
			<div class="row">
				<div class="search_result_item_rightbar_wrapper text-right">
					<div class="container-fluid col-xs-12 search_result_item_rating_wrapper">
						<div class="search_result_item_rating desktop valign-wrapper">
							<div class="star_wrapper">
								{{#rating_stars}}
								<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/{{& show_stars}}.png">
								{{/rating_stars}}
							</div>
							<div class="star_count_wrapper">&nbsp;(<span
										class="text-bold stars_total_rating_count">{{& stars_total_rating_count}}</span>)
							</div>
						</div>
					</div>
					<div class="container-fluid col-xs-12 search_result_item_recommend_wrapper">
						<div class="search_result_item_recommend"><span class="text-bold recommendations_count desktop">{{& recommendations_count}}</span>&nbsp;like(s)
						</div>
					</div>
					<div class="container-fluid col-xs-12 visible-xs visible-sm search_result_item_place_wrapper">
						{{#place_of_teaching}}
						<i class="material-icons material-icons-fix-padding md-18 no-select" data-toggle="popover"
						   tabindex="0" role="button" data-content="Place of teaching: {{& img_teaching_title}}">{{&
							img_teaching}}</i> {{/place_of_teaching}}
					</div>
					<div class="col-xs-12 hidden-xs hidden-sm search_result_item_req_call_wrapper">
						<a class="noroundcorner btn btn-raised btn-sm search_result_item_req_call desktop text-bold"
						   data-id="{{& profile_data_id}}" data-toggle="modal" data-target="#request-callback-modal"
						   data-backdrop="static" data-keyboard="false">Request Callback</a>
						<a class="btn btn-raised btn-sm noroundcorner search_result_profile_view_button desktop"
						   href="<?= home_url(); ?>/{{& profile_type}}/{{& url}}" target="_blank">View Profile</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row visible-xs visible-sm">
				<div class="col-xs-6 center-block text-center">
					<a class="btn btn-raised btn-sm noroundcorner search_result_profile_view_button mobile"
					   href="<?= home_url(); ?>/{{& profile_type}}/{{& url}}" target="_blank">
						<i class="material-icons material-icons-fix-padding md-18">&#xE853;</i>&nbsp;View Profile </a>

				</div>
				<div class="col-xs-6 search_result_item_req_call_wrapper center-block text-center">
					<a class="noroundcorner btn btn-raised btn-sm search_result_item_req_call mobile text-bold btn-warning"
					   data-id="{{& profile_data_id}}" data-toggle="modal" data-target="#request-callback-modal"
					   data-backdrop="static" data-keyboard="false">Request Callback</a>
				</div>
			</div>
		</div>
	</div>
</div>
{{/items}}