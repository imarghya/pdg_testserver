{{#items}}
<!--similar-profile-items-->
<div class="slideX white text-center no-select center-block z-depth-1" data-url="<?= home_url() ?>/{{& profile_type}}/{{& url}}" data-profile_data_id="{{& profile_data_id}}">
	<a href="<?= home_url(); ?>/{{& profile_type}}/{{& url}}" title="{{& name}}">
		<div class="img_holder {{& profile_data_img_orientation}} no-select" style="background-image: url('{{& profile_data_img}}');"></div>
	</a>
	<p class="blue-shade-color text-bold no-margin truncate no-select"><a href="<?= home_url(); ?>/{{& profile_type}}/{{& url}}" class="blue-shade-color" title="{{& name}}">{{& name}}</a></p>
	<p class="black-shade-color no-margin truncate no-select">{{& locations}}</p>
	<p class="black-shade-color no-margin truncate no-select">{{& subjects}}</p>
</div>
<!--/similar-profile-items-->
{{/items}}