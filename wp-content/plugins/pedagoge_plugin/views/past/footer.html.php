

<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/jquery-2.1.1.min.js"></script>


	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/tag-it.min.js"></script>



<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/bootstrap.min.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/base.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/member_login.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/filter_data.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/submit_academic.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/submit_nonacademic.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/subject_select.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/getsamedetails.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/submit_academicnaboth.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/aftersubmit.js"></script>

<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js/datepicker.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.2/languages/jquery.validationEngine-en.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.2/jquery.validationEngine.min.js"></script>
	
	
	<script>
    
		$(document).ready(function(){
			  $('#frmnameemail').validationEngine('validate');
			  
			  for(var x=1;x<=20;x++) $('#frm_submit_academic'+x).validationEngine('validate');
			  
			 		  
			  for(var x=1;x<=20;x++) $('#frm_submit_academicna'+x).validationEngine('validate');
			  
			  
			  for(var x=1;x<=20;x++) $('#frm_submit_academicnaboth'+x).validationEngine('validate');
			  
		});
		
		
	</script>








<script type="text/javascript" charset="utf-8">
			var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        	var $ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
        	var $pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>';
        	var $pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler';
        	var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
		</script>

 <script>
	
        $(function(){  	
			
		   var country_list=[<?php echo getsub(); ?>];		
            $('#singleFieldTags').tagit({
                availableTags: country_list,              
                singleField: true,
                singleFieldNode: $('#mySingleField'),
				allowSpaces: true
            });			
            $('#readOnlyTags').tagit({
				
                readOnly: true
            });
			
		});
		
		$(function() {     
   var currentlyValidTags = [<?php echo getsub(); ?>];
   $('#singleFieldTags').tagit({
    availableTags: currentlyValidTags,
    autocomplete: { source: function( request, response ) {        
        var filter = removeDiacritics(request.term.toLowerCase());
        response( $.grep(currentlyValidTags, function(element) {
						
                        return (element.toLowerCase().indexOf(filter) === 0);
						
                    }));
    }},  
    beforeTagAdded: function(event, ui) {
      if ($.inArray(ui.tagLabel, currentlyValidTags) == -1) {
		$("#errortagmsg").html("<font color='red'>Subject not exist</font>");
        return false;
      }else{
		  
		 $("#errortagmsg").html("");
	  }
    }          
  });    
});
		
		
		/*
		$(function(){  	
			
		   var country_list=[<?php echo getsub(); ?>];		
            $('#singleFieldTags1').tagit({
                availableTags: country_list,              
                singleField: true,
                singleFieldNode: $('#mySingleField1'),
				allowSpaces: true
            });			
            $('#readOnlyTags').tagit({
                readOnly: true
            });
		});
		*/
		</script>
	
	<script type="text/javascript">
      	/* Date Picker */

	$(document).ready(function(){
			var date_input=$('input[name="start_date"]'); //our date input has the name "date"
			var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
			date_input.datepicker({
				format: 'mm/dd/yyyy',
				container: container,
				todayHighlight: true,
				autoclose: true,
			})
		})

/* Date Ppicker */
      </script>
	  
	  <script type="text/javascript">
	  $(".pastqueryexpand").click(function(){
			
			$(this).hide();
			var id = $(this).attr("rel");
			
			
			
			$('#board'+id).show("100");  
			
			$('#prefloc'+id).show("100"); 
		
			$('#doc'+id).show("100");  
			
			$('#preferences'+id).show("100"); 
			
			$('#pastquerycollapse'+id).show();
			             
		});
		
		$(".pastquerycollapse").click(function(){
			
			$(this).hide();
			var id = $(this).attr("rel");
			
			
			
			$('#board'+id).hide("100");  
			
			$('#prefloc'+id).hide("100");
		
			$('#doc'+id).hide("100");  
			
			$('#preferences'+id).hide("100"); 
			
			$('#pastqueryexpand'+id).show();
			
		});
	
	  
	  
	  </script>
	
	<script type="text/javascript">
	 
	 
	
	function f(e){
		e.preventDefault();
		
		//alert(document.getElementById("frmeditsubjectselectarchive").standard.value);
		
		subid=$("#subid").val();
		
		 
			var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'update_archivedata_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frmeditsubjectselectarchive').serialize()+'&'+$.param({ 'mysubid': subid }),
				
			};
			 
			$.post( $ajax_url, submit_data, function ( response ) {
				
				alert("Updated");
				window.location.href = '<?= home_url() ?>/archived';
			
			} );
	
	}
	
		
		function funcsel(){
			
			
			
			$("#frmeditsubjectselectarchive input[name=localities]").val($('#localitiesz').val());
			
		}
	</script>
		

</body>
</html>
