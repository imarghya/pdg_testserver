    <section class="body_wrapper">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
                <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries margin_top_30">
                    
                    	<div class="row">
                        	<div class="col-xs-12 blue_background top_radius h1_heading">
                                <h1 class="color_white text-center">Queries <a href="#" class="notification pull-right"><i class="fa fa-bell-o" aria-hidden="true"></i> <sup>2</sup></a></h1>
                            </div>
                        </div>
                        
                        <div class="row margin_top_20">
                        	<div class="col-xs-12 navigation_menu no_padding_left_right">
                                <ul class="parent_tab">
                                	<li ><a href="<?= home_url() ?>/memberdashboard" id="activequery" >Active</a></li>
                                    <li class="menu_active"><a href="<?= home_url() ?>/past">Past</a></li>
                                    <li><a href="<?= home_url() ?>/archived/" id="menuarchive" >Archived</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#subjectChoose">New</a></li>
                                </ul>
                            </div>
                      	</div>
						
					
                         <?php
							
							function convertidtosub($id){
								global $wpdb;
								$sql="select subject_name from pdg_subject_name where subject_name_id='$id'";
								$recsub=$wpdb->get_results($sql);		
								return $recsub[0]->subject_name;
								
							}
							
						
							function convertsubtoid($str){
								global $wpdb;
								$sql="select subject_name_id from pdg_subject_name 

where subject_name='$str'";
								$rec=$wpdb->get_results($sql);
								return $rec[0]->subject_name_id;
							}
							
							function convertboardid($str){
								global $wpdb;
								$sql="select academic_board from pdg_academic_board 

where academic_board_id='$str'";
								$rec=$wpdb->get_results($sql);
								return $rec[0]->academic_board;
							}
							
							function getlocality($id){
								global $wpdb;
								$sql="select locality from pdg_locality where locality_id='$id'";
								$recloc=$wpdb->get_results($sql);
								foreach ( $recloc as $loc ) return $loc->locality;
								
								
								
							}
												
												?>	
						
	
	<?php
									
									global $wpdb;
									
									$userid=$_SESSION['member_id'];
									
									//$sql="select * from pdg_shortlist where studentid=$userid and queryid=$subid" ;
									
									$sql="select * from pdg_query where querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) and 

student_id=$userid";
									
									//echo $sql;
									
									
									
									$recs=$wpdb->get_results($sql);									

	
										
									$rowcount = $wpdb->num_rows;
									
									if($rowcount>0){
										
										foreach($recs as $rec){
								
								?>
								


<div class="inner_container margin_top_30">


                            <div class="row">
							
						
							
                                <div class="col-xs-12 inner_container_text gray_broder">
								
								
                                
                                    <div class="row">
                                         <div class="col-xs-12 gray_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?= convertidtosub($rec->subject) ?></span> <strong 

class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong></h2>
                                         </div>
                                    </div>
									
									
<!------------------------------------------------------------------------------------------------------------->									
									
 <div class="row margin_top_10">
                                         <div class="col-md-6 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:<b><?= $rec->standard ?></b></p>
                                                <div class="preferred_location">
											
										
												
												<p style="display:none" id="board<?= $rec->id ?>">Board/University:<b><?= 

convertboardid($rec->board) ?></b></p>
												
												
												<div class="preferred_location" style="display:none" id="prefloc<?= $rec->id ?>">
                                                    <p>Locality Type:</p> 
                                                    <ul class="area">
													<?php
														$teachplace="";
														if($rec->myhome)$teachplace="Student's Location";
														
														else if($rec->tutorhome)$teachplace="Own Location";
														
														else if($rec->institute) $teachplace="Both own location 

and student's location";
														
														$teachplace="'".$teachplace."'";
													
													
													?>
													
													<?php if($rec->myhome){  ?>
                                                        <li>
														
														<a 

href="#" class="blue_background">My Home</a>
														
														
														</li>
													<?php }else { ?>
												<li></li>
													<?php } ?>
													
											<?php if($rec->tutorhome){  ?>
													
                                                        <li>
														
														<a 

href="#" class="blue_background">Tutor's Home</a>
														
														</li>
											<?php } else { ?>
											<li></li>
											<?php } ?>
											
											
											<?php if($rec->institute){  ?>
                                                        <li>
														
														<a 

href="#" class="blue_background">Institute</a>
														
														</li>
											<?php }else{ ?>
											
											<li></li>
											
											<?php } ?>
														
                                                    </ul>
                                                </div>
												
												 <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                    
													<?php
														$localities=explode(",",$rec->localities);
														//$preflocs="";
														foreach($localities as $loc){
															
															//$preflocs.= "'".getlocality($loc)."',";
															

															
													?>
                                                        <li><a href="#" class="blue_background"><?= getlocality($loc) ?></a></li>
                                                       
													<?php
														}
														//$preflocs=rtrim($preflocs,",");
													?>
                                                    
                                                    </ul>
                                                </div>
												<div class="preferred_location" style="display:none" id="doc<?= $rec->id ?>" >
                                                    <p class="width_100">Date of commencement: <strong>
													
													
													<?= $rec->start_date ?>
													
													</strong></p> 
                                                </div>
										
                                         </div>
									
                                    </div>	
									
									 <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
									<div class="col-md-3 col-sm-4 col-xs-12 margin_bottom_20 preference" style="display:none" id="preferences<?= $rec->id ?>">
                                         	<h5>Preferences: </h5>
                                            <p class="text-justify">
											
											<?= $rec->requirement ?>
											
											</p>
                                         </div>
										</div>
                                
 <div class="col-md-3 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<!--<li><a href="#" class="gray_background">Expand</a></li>-->
												
												<li><a href="#" id="pastqueryexpand<?= $rec->id ?>" rel="<?= $rec->id 

?>" class="gray_background pastqueryexpand">Expand</a></li>
                                            	<li><a href="#" id="pastquerycollapse<?= $rec->id ?>" style="display:none" rel="<?= $rec->id ?>" class="gray_background pastquerycollapse">Collapse</a></li>	
												<li><a href="#" class="gray_background">Archive</a></li>
                                                <li><a href="#" class="gray_background">Edit & Activate</a></li>
                                            </ul>
                                         </div>									
							
<!---------------------------------------------------------------------------------------------------------------->									
                                    
                                 </div>
								
                           </div>
						  
                       </div>
					   
					     <?php
										}
									}
									?>
                       
                       <aside class="model_wrapper">
                       
                       		<!--<button data-toggle="modal" data-target="#subjectChoose">New</button>-->
                       
                       		<div class="modal fade" id="subjectChoose" role="dialog">
                                <div class="modal-dialog">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title text-center text-uppercase">Choose the subjects/activities you're looking Tutor for</h4>
                                    </div>
                                    <!--<form action="" method="post">-->
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
                                   
									 
									 <form action="" method="post">
										<input name="mytags" id="mySingleField" type="hidden"><!----------use this field ----->
										<ul id="singleFieldTags"></ul>
										
                                    <div class="modal-footer no_padding_top_bottom">
                                      <!--<input type="button" value="Submit" class="data_submit blue_background color_white"  data-toggle="modal" data-target="#subjectSelect">-->
									  
									  <input type="button" value="Submit" class="data_submit blue_background color_white"  data-toggle="modal" id="subject_frm_button" >
									  
									  <!--<div id="samit"></div>-->
									  
                                    </div>
                                    </form>
									</div></div>
                                  </div>
                               </div>
                          </div>

                       </aside>
					   
<!---------------------------------------------academic starts------------------------------------------------>					   
					   
                       <?php
							
							
						
							for($x=1;$x<=20;$x++){
					   
					   ?>
						
                       <aside class="model_wrapper">
                                          
                       		<div class="modal fade" id="subjectSelect<?= $x ?>" role="dialog">
							
							
                                <div class="modal-dialog modal-dialog-next">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">
                                    <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <ul class="acadamic_subject_form">
                                      	<li>
										<?php
											if($x==1){
										?>
										 <li><a href="#"  data-toggle="modal" data-target="#"><i ></i>
										</a>
										</li>
										<?php										
										
											}else{
										
										?>
										 <li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelect<?= $x-1 ?>" data-dismiss="modal">
										 
										 <div id="selsubjectleft<?= $x ?>" ></div>
										 
										 <i class="fa fa-caret-left" aria-hidden="true"></i>
										</a>
										</li>
										
										<?php
											}
											
										?>
										
                                      	<li class="active_subject"><a href="#">
										
										<div id="mysubject"><div id="selsubject<?= $x ?>"></div></div>
										
												
										</a></li>
                                        <li>
										
										
										<a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelect<?= $x+1 ?>" data-dismiss="modal" id="next<?= $x ?>">
										<div id="selsubjectright<?= $x ?>" ></div>
										
										<i class="fa fa-caret-right" aria-hidden="true"></i></a>
										
										
										</li>
                                      </ul>
                                    </div>
                                    <form id="frm_submit_academic<?= $x ?>" method="post" >
									<div id="mydiv"></div>
									<input type="hidden" id="subject<?= $x ?>">
									<input type="hidden" id="subjectnum">
									
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
									
									<?php
										if($x==1){}else{
									?>
									<div class="col-sm-12 subject_type no_full_width">
                                     	<label for="sameAs">Same as Previous</label> <input class="non_grid_input" type="checkbox" id="sameas" name="sameAs">
                                     </div>
									 
									 <?php
										}
									 ?>
									 
                                     <div class="col-sm-12 subject_type">
                                     	
										<!--<input placeholder="School/college" name="school" id="school" class="validate[required] text-input non_grid_input black_border"    type="text">-->
										
										
										
									
										<select name="school" id="school" class="non_grid_input black_border form-control validate[required]" >
										<option value="">SCHOOL/COLLEGE</option>
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_schools ORDER BY school_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->school_id ?>"><?= $r->school_name ?></option>
										<?php
											}
										?>
										</select>
										
										
										
                                        <!--<input placeholder="Board/university" name="board" id="board" class="validate[required] text-input non_grid_input black_border"   type="text">-->
										
										<select name="board" id="board" class="non_grid_input black_border form-control validate[required]">
										<option value="">BOARD/UNIVERSITY</option>
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_academic_board ORDER BY academic_board_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->academic_board_id ?>"><?= $r->academic_board ?></option>
										<?php
											}
										?>
										</select>
										
										
										
										
                                        <!--<input placeholder="Standard/year" name="standard" id="standard" class="validate[required] text-input non_grid_input black_border"   type="text">
                                     </div>-->
									 
									 <select name="standard" id="standard" class="non_grid_input black_border form-control validate[required]">
										<option value="">STANDARD/YEAR</option>
										<?php
											global $wpdb;
											$SQL = "SELECT subject_type FROM pdg_subject_type ORDER BY subject_type_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->subject_type ?>"><?= $r->subject_type ?></option>
										<?php
											}
										?>
										</select>
									 
									 
									 
                                     
                                     <div class="col-sm-12 subject_type">
                                     	<p class="text-center option_heading">Select your preferred option</p>
                                        <ul class="place_option">
                                        	<li><input type="checkbox" name="myhome" id="myhome"><label for="MyHome">My Home</label></li>
                                            <li><input type="checkbox" name="tutorhome" id="tutorhome"><label for="TutorHome">Tutor's Home</label></li>
                                            <li><input type="checkbox" name="institute" id="institute"><label for="Institute">Institute</label></li>
                                        </ul>
                                        
										<!--<textarea class="validate[required] text-input mentions_localitites black_border" id="localities" name="localities" placeholder="Mentions localitites separate with commas."></textarea>-->
										
										<input type="hidden" name="localities" id="localities">
										<select  id="localitiesx" class="non_grid_input black_border form-control validate[required]" multiple>
										<option value="">MENTION LOCALITIES</option>
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_locality ORDER BY locality_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
										<?php
											}
										?>
										</select>
										
										
                                        
										
										<input  class="mentions_localitites black_border validate[required]" id="start_date" name="start_date" placeholder="When do you want the classes to begin" type="text" readonly="true"/>
										
										
                                        <textarea class="validate[required] text-input mentions_localitites black_border min_height_130" id="requirement" name="requirement" placeholder="Please mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching."></textarea>
                                     </div>
                                     
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                     <button id="submit_academic<?= $x ?>"   class="data_submit blue_background color_white">Submit</button>
									
                                    </div>
                                    </form>
                                  </div>
                               </div>      
                          </div>

                       </aside>
                       
               <?php
			   
							}
			   ?>

<!---------------------------------------------academic ends----------------------------------------------------->
			   
	<!---------------------------------------------non academic starts------------------------------------------>
					   
				    <?php
							
							
						
							for($x=1;$x<=20;$x++){
					   
					   ?>  
					   
					   
					   
					   <aside class="model_wrapper">
                                              
                       		<div class="modal fade" id="subjectSelectNext<?= $x ?>" role="dialog">
                                <div class="modal-dialog modal-dialog-next">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">
                                    <div class="modal-header">
                                      <ul class="acadamic_subject_form">
                                      	<button type="button" class="close" data-dismiss="modal">&times;</button>
										<?php
										
										if($x==1){
											
										?>
										 <li><a href="#"  data-toggle="modal" data-target="#"><i ></i>
										</a>
										</li>
										<?php
										}else{
										
										?>
										
										<li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelectNext<?= $x-1 ?>" data-dismiss="modal" >
										
										<div id="selsubjectleftna<?= $x ?>" ></div>
										
										<i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
										
										<?php
										
											}
										
										?>
										
										
                                      	<li class="active_subject"><a href="#">
										<div id="mysubjectna"><div id="selsubjectna<?= $x ?>"></div></div>
										</a></li>
                                        
										<li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelectNext<?= $x+1 ?>" data-dismiss="modal" id="nextna<?= $x ?>">
										
										<div id="selsubjectrightna<?= $x ?>" ></div>
										
										<i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
										
										
                                      </ul>
                                    </div>
                                    <form id="frm_submit_academicna<?= $x ?>" method="post">
									<input type="hidden" id="subjectna<?= $x ?>">
									<input type="hidden" id="subjectnum">
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
									
									
									<?php
										if($x==1){}else{
									?>
									<div class="col-sm-12 subject_type no_full_width">
                                     	<label for="sameAs">Same as Previous</label> <input class="non_grid_input" type="checkbox" id="sameas" name="sameAs">
                                     </div>
									 
									 <?php
										}
									 ?>
									
									
									
									
                                     <div class="col-sm-12 subject_type">
                                     	<input placeholder="Age" name="age" id="age" class="validate[required,custom[integer],max[100]] text-input non_grid_input black_border" type="text">
                                     </div>
									 
																 
                                     
                                     <div class="col-sm-12 subject_type">
                                     	<p class="text-center option_heading">Select your preferred option</p>
                                        <ul class="place_option">
                                        	<li><input type="checkbox" name="myhome" id="myhome"><label for="MyHome">My Home</label></li>
                                            <li><input type="checkbox" name="tutorhome" id="tutorhome"><label for="TutorHome">Tutor's Home</label></li>
                                            <li><input type="checkbox" name="institute" id="institute"><label for="Institute">Institute</label></li>
                                        </ul>
                                        
										<!--<textarea class="validate[required] text-input mentions_localitites black_border" id="localities" name="localities" placeholder="Mention localities separate with commas."></textarea>-->
										
										
										<input type="hidden" name="localities" id="localities">
										<select  id="localitiesx" class="non_grid_input black_border form-control" multiple>
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_locality ORDER BY locality_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
										<?php
											}
										?>
										</select>
										
										
										
										
										
										
										
                                        <input placeholder="When do you want the classes to begin" class="validate[required] text-input non_grid_input black_border no_upper" type="text" id="start_date" name="start_date" readonly="true">
                                        <textarea class="validate[required] text-input mentions_localitites black_border min_height_130" id="requirement" name="requirement" placeholder="Please mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching."></textarea>
                                     </div>
                                     
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <button id="submit_academicna<?= $x ?>"   class="data_submit blue_background color_white">Submit</button>
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>

                       </aside>
					   
					   <?php
					   
							}
							
						?>
<!-------------------------------------------------------------non academic ends------------------------------------->


<!-----------------------------------------------------academic and non academic starts------------------------------------->
						
						 <?php
							
							
						
							for($x=1;$x<=20;$x++){
					   
					   ?>  
					   
					  <aside class="model_wrapper">
                                              
                       		<div class="modal fade" id="subjectSelectBoth<?= $x ?>" role="dialog">
                                <div class="modal-dialog modal-dialog-next">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">
                                    <div class="modal-header">
                                      <ul class="acadamic_subject_form">
                                      	<button type="button" class="close" data-dismiss="modal">&times;</button>
										<?php
										
										if($x==1){
											
										?>
										 <li><a href="#"  data-toggle="modal" data-target="#"><i ></i>
										</a>
										</li>
										<?php
										}else{
										
										?>
										
										<li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelectBoth<?= $x-1 ?>" data-dismiss="modal">
										<div id="selsubjectleftnaboth<?= $x ?>" ></div>
										<i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
										
										<?php
										
											}
										
										?>
										
										
                                      	<li class="active_subject"><a href="#">
										<div id="mysubjectboth"><div id="selsubjectboth<?= $x ?>"></div></div>
										</a></li>
                                        
										<li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelectBoth<?= $x+1 ?>" data-dismiss="modal" id="nextnaboth<?= $x ?>">
										<div id="selsubjectrightnaboth<?= $x ?>" ></div>
										<i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
										
										
                                      </ul>
                                    </div>
                                    <form id="frm_submit_academicnaboth<?= $x ?>" method="post">
									<input type="hidden" id="subjectnaboth<?= $x ?>">
									<input type="hidden" id="subjectnum">
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
									
									
									<?php
										if($x==1){}else{
									?>
									<div class="col-sm-12 subject_type no_full_width">
                                     	<label for="sameAs">Same as Previous</label> <input class="non_grid_input" type="checkbox" id="sameas" name="sameAs">
                                     </div>
									 
									 <?php
										}
									 ?>
									
									
									
									
                                     <div class="col-sm-12 subject_type">
                                     	<input placeholder="Age" name="age" id="age" class="validate[required,custom[integer],max[100]] text-input non_grid_input black_border non_grid_input black_border" type="text">
                                     </div>
									 
									  <div class="col-sm-12 subject_type" id="sbsnaboth">
                                     	<!--<input placeholder="School/college" name="school" id="school" class="validate[required] text-input non_grid_input black_border"   type="text">
                                        <input placeholder="Board/university" name="board" id="board" class="validate[required] text-input non_grid_input black_border"   type="text">-->
										
										<select name="school" id="school" class="non_grid_input black_border" >
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_schools ORDER BY school_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->school_id ?>"><?= $r->school_name ?></option>
										<?php
											}
										?>
										</select>
										
										<select name="board" id="board" class="non_grid_input black_border">
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_academic_board ORDER BY academic_board_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->academic_board_id ?>"><?= $r->academic_board ?></option>
										<?php
											}
										?>
										</select>
										
										
										
                                        <input placeholder="Standard/year" name="standard" id="standard" class="validate[required] text-input non_grid_input black_border"   type="text">
                                     </div>
                                     
                                     <div class="col-sm-12 subject_type">
                                     	<p class="text-center option_heading">Select your preferred option</p>
                                        <ul class="place_option">
                                        	<li><input type="checkbox" name="myhome" id="myhome"><label for="MyHome">My Home</label></li>
                                            <li><input type="checkbox" name="tutorhome" id="tutorhome"><label for="TutorHome">Tutor's Home</label></li>
                                            <li><input type="checkbox" name="institute" id="institute"><label for="Institute">Institute</label></li>
                                        </ul>
                                        
										<!--<textarea class="validate[required] text-input mentions_localitites black_border" id="localities" name="localities" placeholder="Mention localities separate with commas."></textarea>-->
										
										<input type="hidden" name="localities" id="localities">
										<select  id="localitiesx" class="non_grid_input black_border form-control" multiple>
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_locality ORDER BY locality_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<option value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
										<?php
											}
										?>
										</select>
										
										
										
										
										
                                        <input placeholder="When do you want the classes to begin" class="non_grid_input black_border no_upper" type="text" id="start_date" name="start_date" readonly="true">
                                        <textarea class="validate[required] text-input mentions_localitites black_border min_height_130" id="requirement" name="requirement" placeholder="Please mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching."></textarea>
                                     </div>
                                     
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <button id="submit_academicnaboth<?= $x ?>"   class="data_submit blue_background color_white">Submit</button>
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>

                       </aside>
					   
					   <?php
					   
							}
							
						?>
						
						
<!-------------------------------------------------edit query box----------------------------------->                  
                       <aside class="model_wrapper modal-contentx" id="modal-contentx">
                                              
                       	
                                 <!-- Modal content-->
                                  
                             

                       </aside>						
						
                     
 <!------------------------------------------------academic and non academic ends------------------------------------->
 
                       <aside class="model_wrapper">
                                              
                       		<div class="modal fade" id="nameaddress" name="nameaddress" role="dialog">
                                <div class="modal-dialog modal-dialog-next">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">
                                    
                                    <form method="post" id="frmnameemail">
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
                                     <div class="col-sm-12 subject_type address_type">
                                     	<input placeholder="Name" id="studname" name="studname" class="validate[required] text-input non_grid_input black_border"   type="text" >
                                        <input placeholder="Phone Number" id="phonenum" name="phonenum" class="validate[required,custom[phone]] text-input non_grid_input black_border"   type="text">
                                        <input placeholder="Email Id"  id="emailid" name="emailid" class="validate[required,custom[email]] text-input non_grid_input black_border"   type="text">
										
										 <input id="myuserid" type="hidden">
										 
                                     </div>
                                     
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                   
									<button id="goback"  data-dismiss="modal"  class="data_submit blue_background color_white">Go Back</button>
									
                                     
									 <button id="submitnameemail"   class="data_submit blue_background color_white">Submit</button>
									 
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>

                       </aside>
					   
					   



<!--------------------------------------------delete or activate box starts------------------------------------->

					   <aside class="model_wrapper">
                                              
                       		<div class="modal fade" id="deleteoractivate" name="deleteoractivate" role="dialog">
                                <div class="modal-dialog modal-dialog-next">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">                                 
                                    
                                    <input type="hidden" id="temp">
                                    <div class="modal-footer no_padding_top_bottom">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
									<button id="deletearchive"   class="data_submit blue_background color_white">Delete</button>
									
                                     
									 <button id="editactivate" data-dismiss="modal"  class="data_submit blue_background color_white">Edit and Activate</button>
									 
									 
									 
                                    </div>
                                   
                                  </div>
                               </div>
                          </div>

                       </aside>
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>
   