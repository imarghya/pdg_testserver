<main class="padding-fix center-block">
	<!--section-info-->
	<section class="section-box white-text login-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="logo-wrapper hidden-xs">
						<img class="logo" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='logo-box'">
					</div>
					<h1 class="main-heading text-bold">Login</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="div_login_result" id="div_login_result"></div>
					<div class="div_login_signup" id="div_login_signup"></div>
					<form class="login_form" name="login_form" id="login_form">
						<div class="form-group">
							<input type="email" id="txt_login_email_address" class="form-control validate[required]" name="txt_login_email_address" placeholder="Username or Registered Email">
							<input type="hidden" id="nexturl" value="<?php echo $nexturl; ?>">
							<input type="password" name="txt_login_password" id="txt_login_password" class="form-control validate[required]" value="" placeholder="Password">
						</div>
						<div class="form-group nomargin text-left">
							<div class="checkbox">
								<label for="chk_login_show_password">
									<input type="checkbox" id="chk_login_show_password" value="no">&nbsp;Show
									Password</label>
							</div>
							<div class="checkbox">
								<label for="chk_login_remember_me">
									<input type="checkbox" name="chk_login_remember_me" id="chk_login_remember_me" value="no">&nbsp;Remember
									me</label>
							</div>
						</div>
						<div class="form-group nomargin">
							<input type="button" id="cmd_login" class="btn white-text text-bold btn-lg btn-block cmd_login" data-loading-text="Please wait ..." value="Login"/>
							<p class="white-text text-bold text-center">-- OR CONTINUE WITH --</p>
							<input type="button" id="cmd_login_google" class=" btn white-text text-bold btn-lg btn-block red-goog-btn disabled" disabled="disabled" data-loading-text="Please wait ..." value="Google+ Login"/>
							<!-- <div class="col-xs-12">
								<div class="col-sm-12">
									<input type="button" id="cmd_login_google" class=" btn white-text text-bold btn-lg btn-block red-goog-btn disabled" disabled="disabled" data-loading-text="Please wait ..." value="Google+ Login"/>
								</div>
								<div class="col-sm-6">
									<input type="button" id="cmd_login_facebook" class="btn white-text text-bold btn-lg btn-block blue-face-btn disabled" disabled="disabled" data-loading-text="Please wait ..." value="Facebook Login"/>
								</div>
							</div> -->
						</div>
						<div class="col-xs-12 pdg_social_login_icons hide">
							<div class="col-xs-12 text-center center-block">
								<!-- <fb:login-button size="" scope="public_profile,email" onlogin="fn_check_fb_login_status();" id="fb_login_button">
									Sign In
								</fb:login-button> -->
							</div>
							<div class="col-xs-12 text-center center-block">
								<div class="g-signin2" data-onsuccess="onSignIn"></div>
							</div>
						</div>
						<a href="<?php echo home_url( '/reset' ) ?>" class="display-block text-center text-sm cmd_login_forgot_password">Forgot
							Password?</a>
						<p class="text-center dont-have-account margin-top-10">Do not have an account?</p>
						<a href="<?php echo home_url( '/register' ); ?>/" title="Pedagoge Signup" id="cmd_show_register_form" class="btn white-text text-bold btn-lg btn-block cmd_show_register_form">Create
							an account</a>
					</form>

					<div class="row">
						<span class="text-sm"><?php echo date( 'Y' ) ?>&nbsp;&copy; Pedagoge.</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/section-info-->
</main>