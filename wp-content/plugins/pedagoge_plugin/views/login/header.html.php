<html>
	<head>
		<title><?php echo $this->title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>		
		<?php //echo $this->fn_header_scripts(); ?>
		<?php echo $this->load_view('header_scripts'); ?>
		
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->        
    </head>
    <body class="<?php echo $this->body_class; ?>">
		<div class="navbar">
			<div class="navbar-inner">                    
				<div class="logo-box">
					<a href="<?php echo home_url('/'); ?>" class="logo-text"><span><img src="http://pedagoge.com/logo.png" width="99%"></span></a>
				</div><!-- Logo Box -->                    
				<div class="topmenu-outer">
					<div class="top-menu">
							<br>
							<center></center>
                            <ul class="nav navbar-nav navbar-left">                                
                            </ul>
					</div><!-- Top Menu -->
				</div>
			</div>
		</div><!-- Navbar -->	
        
		<main class="page-content">
			<div class="page-inner">
				<div id="main-wrapper">