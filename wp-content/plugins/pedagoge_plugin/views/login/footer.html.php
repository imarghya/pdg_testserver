				</div><!-- Main Wrapper -->
			</div><!-- Page Inner -->
		</main><!-- Page Content -->
		
		 <!-- Footer starts -->
		<footer id="foot">
			<hr>
			<div class="container">
				<div class="row centered text-center">
					<div class = "col-md-4 centered">
						<h3>CONNECT</h3>
						<br/>
						<a href="<?php echo home_url('/about'); ?>">About Us</a><br/>
						<a href="<?php echo home_url('/#contact'); ?>">Get In Touch</a>
					</div>
					<div class = "col-md-4 centered">
						<h3>LEGAL</h3>
						<br/>
						<a href="<?php echo home_url('/privacypolicy'); ?>">Privacy Policy</a><br/>
						<a href="<?php echo home_url('/terms'); ?>">Terms &amp; Condition</a>			
					</div>
					<div class = "col-md-4 centered">
						<h3>SERVICE</h3>
						<br/>
						<a href="<?php echo home_url('/teachers'); ?>">Pedagoge For Teacher</a><br/>
						<a href="<?php echo home_url('/#features1'); ?>">Pedagoge For Students</a>		
					</div>			
				</div>
				<br/>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<center><img src = "<?php echo PEDAGOGE_ASSETS_URL; ?>/images/10kstartup.png" class="img-responsive"></center>
					</div>	
				</div>				
                <p class="text-center no-s"><?php echo date('Y'); ?> &copy; Pedagoge.</p>
			</div>
		</footer>
		<!-- Footer Ends -->
		
		<!-- Custom Javascript Variables -->
		<script type="text/javascript" charset="utf-8">
			var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        	var $ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
        	var $pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>';
        	var $pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler';
        	var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
		</script>
		
		<!-- Load Scripts below -->
		<?php 
			
			echo $this->load_view('footer_scripts');
			
			if( isset($this->app_data['dynamic_js']) ) {
				echo $this->app_data['dynamic_js'];
			} 
		?>
		
		<div>
			<?php echo get_num_queries(); ?> queries in	<?php timer_stop(1); ?> seconds.
		</div>
	</body>
</html>