<section class="site-section site-section-top">
	<div class="row pedagoge_login_form">
		<div class="col-md-4 col-xs-12 center">                               
			<div class="login-box panel panel-white">
	            <div class="panel-body">
					<div class="text-center m-t-md">Please login into your account</div>
					<div class="row pdg_social_login_icons well">
						<div class="col-md-1"></div>
						<div class="col-md-5">
							<fb:login-button size="xlarge" scope="public_profile,email" onlogin="fn_check_fb_login_status();" id="fb_login_button">Sign In</fb:login-button>
						</div>
						<div class="col-md-5">
							<div class="g-signin2" data-onsuccess="onSignIn"></div>							
						</div>
						<div class="col-md-1"></div>						
					</div>
					<div style="text-align: center;">-------------------or-------------------</div>
					
					<div class="row">				
						<div class="col-md-12" id="div_login_result"></div>
					</div>
					<div id="div_login_signup">
						<form class="m-t-md" name="login_form" id="login_form">
	
							<!-- <div id="status">
							</div> -->
	
							<div class="form-group">
								<input type="email" id="txt_login_email_address" class="form-control validate[required]" name="txt_login_email_address" placeholder="Username or Registered Email" value="">
								<input type="hidden" id="nexturl" value="<?php echo $nexturl; ?>">
							</div>
							<div class="form-group">
								<input type="password" name="txt_login_password" id="txt_login_password" class="form-control validate[required]" value="" placeholder="Password">
								<br>
								<label><input type="checkbox" id="chk_login_show_password" class="" value="no"> Show Password</label>
								<label class="remember"><input type="checkbox" name="chk_login_remember_me" id="chk_login_remember_me" class="" value="no"> Remember me</label>
							</div>
							<input type="button" id="cmd_login" class="btn btn-success btn-block"  data-loading-text="Please wait ..." value="Login" /> 												
							<a href="<?php echo home_url('/reset') ?>" class="display-block text-center m-t-md text-sm cmd_login_forgot_password">Forgot Password?</a>
							<p class="text-center">Do not have an account?</p>
							<a href="<?php echo home_url('/register'); ?>/" title="Pedagoge Signup" id="cmd_show_register_form" class="btn btn-default btn-block m-t-md ">Create an account</a>
						</form>
					</div>
					<center>
						<span class="m-t-xs text-sm">2016 &copy; Pedagoge.</span>
					</center>
				</div>
			</div>
	    </div>
	</div><!-- Row -->
	<!-- FB Social Login new user modal -->
	<div id="new_user_modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">	    
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Modal Header</h4>
	      </div>

	      <div class="modal-body">
	        <p>Some text in the modal.</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

</section>