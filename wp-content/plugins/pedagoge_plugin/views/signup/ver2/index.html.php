<?php
$str_user_roles = '';
if ( isset( $template_vars['user_role'] ) ) {
	$user_roles = $template_vars['user_role'];
	foreach ( $user_roles as $user_role ) {
		$str_user_roles .= '<option value="' . $user_role->user_role_name . '">' . $user_role->user_role_display . '</option>';
	}
}

?>
<main class="padding-fix center-block register-block">
	<!--section-info-->
	<section class="section-box white-text">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="logo-wrapper hidden-xs">
						<img class="logo" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='logo-box'">
					</div>
					<h1 class="main-heading text-bold">Signup</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="row">
						<div class="col-xs-12" id="div_signup_result"></div>
					</div>
					<div class="div_login_signup" id="div_login_signup">
						
						<div class="form-group nomargin">
							<!-- <input type="button" id="cmd_login" class="btn white-text text-bold btn-lg btn-block cmd_login" data-loading-text="Please wait ..." value="Login"/> -->
							<!-- <p class="white-text text-bold text-center">-- OR CONTINUE WITH --</p> -->
							<input type="button" id="cmd_login_google" class=" btn white-text text-bold btn-lg btn-block red-goog-btn disabled" disabled="disabled" data-loading-text="Please wait ..." value="Signup with Google"/>
							<!-- <div class="col-xs-12">
								<div class="col-sm-12">
									<input type="button" id="cmd_login_google" class=" btn white-text text-bold btn-lg btn-block red-goog-btn disabled" disabled="disabled" data-loading-text="Please wait ..." value="Google+ Login"/>
								</div>
								<div class="col-sm-6">
									<input type="button" id="cmd_login_facebook" class="btn white-text text-bold btn-lg btn-block blue-face-btn disabled" disabled="disabled" data-loading-text="Please wait ..." value="Facebook Login"/>
								</div>
							</div> -->
						</div>
						<div class="col-xs-12 pdg_social_login_icons hide">
							<div class="col-xs-12 text-center center-block">
								<!-- <fb:login-button size="" scope="public_profile,email" onlogin="fn_check_fb_login_status();" id="fb_login_button">
									Sign In
								</fb:login-button> -->
							</div>
							<div class="col-xs-12 text-center center-block">
								<div class="g-signin2" data-onsuccess="onSignIn"></div>
							</div>
						</div>
						<p class="white-text text-bold text-center">-- Or Fillup the form below --</p>
						<form class="registration_form" name="registration_form" id="registration_form">
							<div class="form-group">
								<input type="text" name="txt_register_full_name" id="txt_register_full_name" class="form-control validate[required] txt_register_full_name" placeholder="Your Full Name">								
								<input type="email" name="txt_register_email_address" id="txt_register_email_address" class="form-control validate[required, custom[email]] completer txt_register_email_address" placeholder="Your Email Address" />
								<input type="text" maxlength="10" name="txt_register_mobile_number" id="txt_register_mobile_number" class="form-control validate[required] txt_register_mobile_number positive" placeholder="10 Digits Mobile Number" />
								<input type="password" name="txt_register_password" id="txt_register_password" class="form-control validate[required] txt_register_password" placeholder="Password">
							</div>
							<div class="form-group select_user_role margin-fix">
								<select type="role" name="select_user_role" id="select_user_role" class="form-control select_user_role">
									<option selected="selected" disabled value="">Sign Up as</option>
									<?php echo $str_user_roles; ?>
								</select>
								<input type="hidden" name="hidden_register_user_role" value="" id="hidden_register_user_role"/>
							</div>
							
							<div class="form-group nomargin text-left">
								<p> By clicking signup, you are agreeing to the
									<a href="<?php echo home_url( '/terms' ); ?>">terms and conditions</a> and the
									<a href="<?php echo home_url( '/privacy-policy' ); ?>">privacy policy</a> set by
									Pedagoge. </p>
								<div class="checkbox">
									<label for="chk_register_show_password">
										<input type="checkbox" class="" id="chk_register_show_password" value="no"> Show Password 
									</label>
								</div>
							</div>
							<input type="button" id="cmd_register_user" class="btn white-text text-bold btn-lg btn-block cmd_register_user dark-green-btn-shade" value="Register" data-loading-text="Registering your account..." />

							<a href="<?php echo home_url( '/reset' ) ?>" class="display-block text-center text-sm cmd_login_forgot_password">Forgot
								Password?</a>
							<p class="text-center dont-have-account margin-top-10">Do you already own an account?</p>
							<a href="<?php echo home_url( '/login' ); ?>?nexturl=/" title="Pedagoge Login" id="cmd_show_register_form" class="btn white-text text-bold btn-lg btn-block light-green-btn-shade">Login</a>
						</form>
					</div>
					<div class="row">
						<span class="text-sm"><?php echo date( 'Y' ) ?>&nbsp;&copy; Pedagoge.</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/section-info-->
</main>