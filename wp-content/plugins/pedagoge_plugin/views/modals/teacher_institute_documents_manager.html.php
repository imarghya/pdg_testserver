
<div class="modal fade" id="modal_teacher_institute_documents_manager" tabindex="-1" role="dialog" aria-labelledby="modal_teacher_institute_documents_manager_title">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_teacher_institute_documents_manager_title">Documents Manager</h4>
			</div>
			<div class="modal-body">
				
				<div class="row">									
					<div class="col-md-3">
						<button class="btn btn-warning col-md-12 col-sm-12 col-xs-12" id="cmd_reset_document_selection"><i class="fa fa-refresh" aria-hidden="true"></i> Reset Selection</button>
					</div>
					
					<div class="col-md-6">
						<input type="text" class="form-control" disabled value="" id="txt_teacher_institute_document_file_name"/>
					</div>
					
					<div class="col-md-3">
						<input id="fileinput_teacher_institute_document" name="fileinput_teacher_institute_document" type="file" style="display:none;" />
						<button class="btn btn-default col-md-12 col-sm-12 col-xs-12" id="cmd_select_teacher_institute_document" data-loading-text="Loading...">
							<i class="fa fa-folder-open-o" aria-hidden="true"></i> <span id="span_select_document">Select Document</span>
						</button>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-2"><label for="txt_teacher_institute_document_title">Document Title</label></div>
					<div class="col-md-7">
						<input type="text" name="txt_teacher_institute_document_title" id="txt_teacher_institute_document_title" class="form-control" />
					</div>
					<div class="col-md-3">
						<button class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="cmd_upload_teacher_institute_document" data-loading-text="Loading...">
							<i class="fa fa-upload" aria-hidden="true"></i> Upload Document
						</button>
					</div>
				</div>
				<hr />						
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<strong>Documents List</strong>
					</div>
					<div class="col-md-4">
						<button class="btn btn-info col-md-12" id="cmd_reload_teacher_institute_documents_list" data-loading-text="Loading..."><i class="fa fa-spinner" aria-hidden="true"></i> Reload List</button>
					</div>
				</div>
				<hr />
				<div id="div_teacher_institute_documents_list" style="max-height: 200px; min-height: 200px;">
					<h3>Click Reload List button to load the documents list</h3>
				</div>
				
			</div>
			<div class="modal-footer">
				<input type="hidden" id="hidden_input_teacher_institute_user_id" />
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
			</div>
		</div>
	</div>
</div>