<!--
| \ | | ___ | |_ ___  ___  |  \/  | ___   __| | ___| |
|  \| |/ _ \| __/ _ \/ __| | |\/| |/ _ \ / _` |/ _ \ |
| |\  | (_) | ||  __/\__ \ | |  | | (_) | (_| |  __/ |
|_| \_|\___/ \__\___||___/ |_|  |_|\___/ \__,_|\___|_|
-->
<?php if($is_current_user_admin) : ?>
	<div class="modal fade" id="modal_teachers_notes" tabindex="-1" role="dialog" aria-labelledby="ViewTeacherNotesLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ViewTeacherNotesLabel">Teacher/Institute Notes</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-sm btn-raised white-text blue-shade style-fix" data-loading="Reloading Notes" id="cmd_reload_teachers_notes">
									<i class="fa fa-refresh" aria-hidden="true"></i> Reload Notes
								</button>
								</div>
							</div>
							
							<label for="txt_teacher_notes">Notes:</label>
							<textarea id="txt_teacher_notes" class="form-control" placeholder="Write your notes"></textarea>
							<label for="txt_teacher_note_maker" class="col-md-6">Your Name</label>
							<input type="text" class="form-control col-md-6" id="txt_teacher_note_maker" placeholder="Input your name"/>
							<input type="hidden" value="<?= $teacher_user_id; ?>" id="hidden_teacher_user_id"/>
							<button class="btn btn-raised white-text green-shade style-fix" data-loading="Saving Notes" id="cmd_save_teachers_notes">
								<i class="fa fa-floppy-o" aria-hidden="true"></i>
								Save Notes
							</button>
							<br />
							<hr />
							<div id="div_teacher_notes_save_result_area"></div>
						</div>
						<div class="col-md-6">
							<div id="div_teacher_notes" style="overflow:auto; max-height: 400px;">
								<?= $str_teacher_notes; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>