<!--Modal request callback-->
<div id="request-callback-profile-modal" data-callback-name="profile_callback" class="callback_init modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tell us about your requirements</h4>
			</div>
			<form id="profile_callback" class="bs-component">
				<div class="modal-body">
					<div class="col-xs-12">
						<div class="show_result"></div>
						<h5>Contact us: +91-9073922835</h5>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_teachername">Teacher's Name</label>
							<input class="form-control" readonly="readonly" id="callback_teachername" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_sub">Tuition Required For*</label>
							<input class="form-control validate[required]" id="callback_sub" type="text"
							       autocomplete="off">
							<span class="help-block">Enter subjects and separate using commas</span>
						</div>
						<br/>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_tut_loc">Tuition location*</label>
							<select id="callback_tut_loc" class="form-control validate[required]">
								<option value="teacher">Teacher's Place</option>
								<option value="student">Student's Place</option>
								<option value="institute">Institute</option>
							</select>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_pref_mode_comm">Preferred mode of
								communication*</label>
							<div class="checkbox">
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="email"> Email </label>&nbsp;
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="whatsapp"> Whatsapp </label>&nbsp;
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="message"> Message </label>&nbsp;
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="phone"> Phone </label>
							</div>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_pref_time_comm">Preferred time of
								communication*</label>
							<div class="checkbox">
								<label> <input type="checkbox" value="1" class="validate[minCheckbox[1]] pref_time_comm"
								               name="group[group_pref_time_comm]"> 10 - 12 PM </label>&nbsp; <label>
									<input type="checkbox" value="2" class="validate[minCheckbox[1]] pref_time_comm"
									       name="group[group_pref_time_comm]"> 12 - 3 PM </label>&nbsp; <label>
									<input type="checkbox" value="3" class="validate[minCheckbox[1]] pref_time_comm"
									       name="group[group_pref_time_comm]"> 3 - 6 PM </label>&nbsp; <label>
									<input type="checkbox" value="4" class="validate[minCheckbox[1]] pref_time_comm"
									       name="group[group_pref_time_comm]"> 6 - 9 PM </label>
							</div>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_loc">Locality*</label>
							<input class="form-control validate[required]" id="callback_loc" type="text"
							       autocomplete="off">
							<span class="help-block">Enter multiple localities using commas</span>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_name">Name*</label>
							<input class="form-control validate[required]" id="callback_name" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_phn">Phone Number*</label>
							<input class="form-control validate[required, custom[phone]]" id="callback_phn" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_email">Email ID</label>
							<input class="form-control validate[custom[email]]" id="callback_email" type="text"
							       autocomplete="off">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary text-bold submit" data-loading-text="Processing...">
						Submit
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!--/Modal request callback-->

<!--Modal take this course callback-->
<div id="takethiscourse-callback-profile-modal" data-callback-name="profile_takethiscourse_callback" class="callback_init modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Take this course</h4>
			</div>
			<form id="profile_takethiscourse_callback" class="bs-component">
				<div class="modal-body">
					<div class="col-xs-12">
						<div class="show_result"></div>
						<h5>Contact us: +91-9073922835</h5>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_teachername-takethiscourse">Teacher's
								Name</label>
							<input class="form-control" readonly="readonly" id="callback_teachername-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_pageurl-takethiscourse">Page URL</label>
							<input class="form-control" id="callback_pageurl-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_sub-takethiscourse">Class Required For*</label>
							<input class="form-control" id="callback_sub-takethiscourse" type="text"
							       autocomplete="off">
							<span class="help-block">Enter subjects and separate using commas</span>
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_fees-takethiscourse">Fees</label>
							<input readonly class="form-control" id="callback_fees-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_tut_loc-takethiscourse">Type of Location</label>
							<input readonly class="form-control" id="callback_tut_loc-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_loc-takethiscourse">Locality</label>
							<input readonly class="form-control readonly" id="callback_loc-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_batchsize-takethiscourse">Batch Size</label>
							<input readonly class="form-control readonly" id="callback_batchsize-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_length-takethiscourse">Length</label>
							<input readonly class="form-control readonly" id="callback_length-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_time-takethiscourse">Time</label>
							<input readonly class="form-control readonly" id="callback_time-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_name-takethiscourse">Name*</label>
							<input class="form-control validate[required]" id="callback_name-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_phn-takethiscourse">Phone Number*</label>
							<input class="form-control validate[required, custom[phone]]" id="callback_phn-takethiscourse" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_email-takethiscourse">Email ID</label>
							<input class="form-control validate[custom[email]]" id="callback_email-takethiscourse" type="text"
							       autocomplete="off">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary text-bold submit" data-loading-text="Processing...">
						Submit
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!--/Modal take this course callback-->

<!--Modal Login in | Review-->
<div id="no_login_review-profile-modal" data-callback-name="profile_no_login_review_callback" class="callback_init modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Rating and Reviews</h4>
			</div>
			<form id="profile_no_login_review_callback" class="bs-component">
				<div class="modal-body">
					<div class="col-xs-12">
						<div class="show_result"></div>
						<div class="text-center">
							<label>You need to login first as a Guardian/Student.</label>
						</div>

						<div class="text-center">
							<label>For Login,</label>
							<?php
							$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
							$url=explode("/",$actual_link);
							?>
							<a href="<?= home_url( '/login' ); ?>?nexturl=<?= $url[count($url)-3].'/'.$url[count($url)-2]; ?>"> Click Here</a>
						</div>

						<div class="text-center">
							<label>For SignUp,</label> <a href="<?php echo home_url( '/signup' ); ?>"> Click Here </a>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal Review Success-->
<div class="modal fade rating_model" id="modal_review_success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rating and Reviews</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="review_holder">
        	<h4>Success</h4>
            <p class="bullet_decoration">Greate Your review was submitted successfully! It will be plublished after verification!</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/Modal Login in | Review-->

<!--Modal share | Review-->
<div id="share-profile-modal" data-callback-name="profile_share_callback" class="callback_init modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Share this profile</h4>
			</div>
			<form id="profile_share_callback" class="bs-component">
				<div class="modal-body">
					<div class="row valign-wrapper min-height-fix">
						<div class="addthis_inline_share_toolbox valign center-block text-center"></div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!--/Modal share | Review-->

<?php if ( isset( $allow_demo_modal ) && $allow_demo_modal ) { ?>
	<!--Modal take a demo callback-->
	<div id="takeademo-callback-profile-modal" data-callback-name="profile_takeademo_callback" class="callback_init modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Take a Demo</h4>
				</div>
				<form id="profile_takeademo_callback" class="bs-component">
					<div class="modal-body">
						<div class="col-xs-12">
							<div class="show_result"></div>
							<h5>Contact us: +91-9073922835</h5>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_teachername-takeademo">Teacher's Name</label>
								<input class="form-control" readonly="readonly" id="callback_teachername-takeademo" type="text"
								       autocomplete="off">
							</div>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_sub-takeademo">Demo Required For*</label>
								<input class="form-control validate[required]" id="callback_sub-takeademo" type="text"
								       autocomplete="off"> <span class="help-block">Enter subjects and separate using commas</span>
							</div>
							<br/>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_tut_loc-takeademo">Tuition location*</label>
								<select id="callback_tut_loc-takeademo" class="form-control validate[required]">
									<option value="teacher">Teacher's Place</option>
									<option value="student">Student's Place</option>
									<option value="institute">Institute</option>
								</select>
							</div>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_loc-takeademo">Locality*</label>
								<input class="form-control validate[required]" id="callback_loc-takeademo" type="text"
								       autocomplete="off"> <span class="help-block">Enter multiple localities using commas</span>
							</div>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_name-takeademo">Name*</label>
								<input class="form-control validate[required]" id="callback_name-takeademo" type="text"
								       autocomplete="off">
							</div>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_phn-takeademo">Phone Number*</label>
								<input class="form-control validate[required, custom[phone]]" id="callback_phn-takeademo" type="text"
								       autocomplete="off">
							</div>
							<div class="form-group label-floating">
								<label class="control-label" for="callback_email-takeademo">Email ID</label>
								<input class="form-control validate[custom[email]]" id="callback_email-takeademo" type="text"
								       autocomplete="off">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary text-bold submit" data-loading-text="Processing...">
							Submit
						</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--/Modal take a demo callback-->
<?php } ?>

<?php if ( isset( $review_institute_allowed ) && $review_institute_allowed ) { ?>
	<!--Modal institute review | Review-->
	<div id="review-profile-modal" data-callback-name="profile_review_callback" class="callback_init modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Rating and Reviews</h4>
				</div>
				<form id="profile_review_callback" class="bs-component">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="show_result"></div>
							</div>
							<div class="col-xs-3 col-md-3 text-left text-bold inline hide_after_success"><h5 class="">
									Overall rating*</h5></div>
							<div class="col-xs-9 col-md-9 inline hide_after_success">
								<div id="overall-rating"></div>
							</div>
							<div class="col-xs-12 hide_after_success">
								<label><input type="checkbox" name="chk_recommend_teacher" class="chk_recommend_teacher" id="chk_recommend_teacher">
									Would you like to recommend this teacher</label>
							</div>
						</div>
						<div class="row hide_after_success">
							<div class="detailed_review-panel-sub-wrapper">
								<!--Modal_detailed_review-->
								<div id="detailed_review-panel-encloser" class="detailed_review-panel-encloser" role="tablist" aria-multiselectable="true">
									<div class="panel-group" id="detailed_review-panel-accordion">
										<!--detailed_review_item-->
										<div class="panel panel-profile panel-modal">
											<div class="panel-heading border-bottom-fix collapsed" role="button" data-toggle="collapse" data-parent="#detailed_review-panel-accordion" href="#content_review-1" aria-expanded="false" aria-controls="content_review-1" id="heading_review-1">
												<h5 class="panel-title">
													<a class="accordion-toggle collapsed truncate no-select" data-toggle="collapse" data-parent="#detailed_review-panel-accordion" href="#content_review-1" aria-expanded="false" aria-controls="content_review-1">Click
														here for Detailed Ratings</a>
												</h5>
											</div>
											<div id="content_review-1" class="panel-collapse collapse panel-body" role="tabpanel" aria-labelledby="heading_review-1" aria-expanded="false">
												<div class="form-group nomargin-top-10">
													<div class="col-xs-12 detailed_review_item_block_wrapper">
														<ul class="list-group">
															<li class="list-group-item margin-top-10">
																<div class="row">
																	<div class="col-xs-12">
																		<span class="text-bold">Quality*</span>
																		<h6>(Do you clearly understand the
																			teacher?)</h6>
																	</div>
																	<div class="col-xs-12">
																		<div id="star1-rating"></div>

																	</div>
																</div>
															</li>
															<li class="list-group-item margin-top-20">
																<div class="row">
																	<div class="col-xs-12">
																		<span class="text-bold">Cooperativeness*</span>
																		<h6>(Is the institute helpful and flexible when
																			needed?)</h6>
																	</div>
																	<div class="col-xs-12">
																		<div id="star2-rating"></div>
																	</div>
																</div>
															</li>
															<li class="list-group-item margin-top-20">
																<div class="row">
																	<div class="col-xs-12">
																		<span class="text-bold">Infrastructure*</span>
																		<h6>(Consider factors such as coaching area,
																			washroom, parking space, equipments used
																			etc)</h6>
																	</div>
																	<div class="col-xs-12">
																		<div id="star3-rating"></div>
																	</div>
																</div>
															</li>
															<li class="list-group-item margin-top-20">
																<div class="row">
																	<div class="col-md-12">
																		<span class="text-bold">Delight*</span>
																		<h6>(Would you like to go back to the class
																			again?)</h6>
																	</div>
																	<div class="col-md-12">
																		<div id="star4-rating"></div>
																	</div>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!--/detailed_review_item-->
									</div>
								</div>
								<!--/Modal_detailed_review-->
							</div>
						</div>
						<div class="row hide_after_success">
							<div class="col-xs-12">Write your review!*</div>
							<div class="col-xs-12 form-group margin-top-10">
								<textarea rows="4" class="form-control" id="txt_review_field"></textarea>
								<h6 class="txt_review_field_counter"></h6>
							</div>
							<div class="col-xs-12">
								<label><input type="checkbox" name="chk_anonymous_review" class="chk_anonymous_review" id="chk_anonymous_review">
									Review anonymously</label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary text-bold submit hide_after_success" data-loading-text="Processing...">
							Submit
						</button>
						<button type="button" class="btn btn-default hide_after_success" data-dismiss="modal">Cancel
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--/Modal institute review | Review-->
<?php } ?>

<?php if ( isset( $review_allowed ) && $review_allowed ) { ?>
	<!--Modal teacher's review | Review-->
	<div id="review-profile-modal" data-callback-name="profile_review_callback" class="callback_init modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Rating and Reviews</h4>
				</div>
				<form id="profile_review_callback" class="bs-component">
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="show_result"></div>
							</div>
							<div class="col-xs-3 col-md-3 text-left text-bold inline hide_after_success"><h5 class="">
									Overall rating*</h5></div>
							<div class="col-xs-9 col-md-9 inline hide_after_success">
								<div id="overall-rating"></div>
							</div>
							<div class="col-xs-12 hide_after_success">
								<label><input type="checkbox" name="chk_recommend_teacher" class="chk_recommend_teacher" id="chk_recommend_teacher">
									Would you like to recommend this teacher</label>
							</div>
						</div>
						<div class="row hide_after_success">
							<div class="detailed_review-panel-sub-wrapper">
								<!--Modal_detailed_review-->
								<div id="detailed_review-panel-encloser" class="detailed_review-panel-encloser" role="tablist" aria-multiselectable="true">
									<div class="panel-group" id="detailed_review-panel-accordion">
										<!--detailed_review_item-->
										<div class="panel panel-profile panel-modal">
											<div class="panel-heading border-bottom-fix collapsed" role="button" data-toggle="collapse" data-parent="#detailed_review-panel-accordion" href="#content_review-1" aria-expanded="false" aria-controls="content_review-1" id="heading_review-1">
												<h5 class="panel-title">
													<a class="accordion-toggle collapsed truncate no-select" data-toggle="collapse" data-parent="#detailed_review-panel-accordion" href="#content_review-1" aria-expanded="false" aria-controls="content_review-1">Click
														here for Detailed Ratings</a>
												</h5>
											</div>
											<div id="content_review-1" class="panel-collapse collapse panel-body" role="tabpanel" aria-labelledby="heading_review-1" aria-expanded="false">
												<div class="form-group nomargin-top-10">
													<div class="col-xs-12 detailed_review_item_block_wrapper">
														<ul class="list-group">
															<li class="list-group-item margin-top-10">
																<div class="row">
																	<div class="col-xs-12">
																		<span class="text-bold">Cooperative*</span>
																		<h6>(Is this teacher helpful when needed?)</h6>
																	</div>
																	<div class="col-xs-12">
																		<div id="star1-rating"></div>

																	</div>
																</div>
															</li>
															<li class="list-group-item margin-top-20">
																<div class="row">
																	<div class="col-xs-12">
																		<span class="text-bold">Clarity*</span>
																		<h6>(Is this teacher clear about the class
																			requirements and subject matter?)</h6>
																	</div>
																	<div class="col-xs-12">
																		<div id="star2-rating"></div>
																	</div>
																</div>
															</li>
															<li class="list-group-item margin-top-20">
																<div class="row">
																	<div class="col-xs-12">
																		<span class="text-bold">Composure</span>
																		<h6>(Is the teacher able to handle the class
																			well?)</h6>
																	</div>
																	<div class="col-xs-12">
																		<div id="star3-rating"></div>
																	</div>
																</div>
															</li>
															<li class="list-group-item margin-top-20">
																<div class="row">
																	<div class="col-md-12">
																		<span class="text-bold">Coolness</span>
																		<h6>(Do you look forward to the next
																			class?)</h6>
																	</div>
																	<div class="col-md-12">
																		<div id="star4-rating"></div>
																	</div>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<!--/detailed_review_item-->
									</div>
								</div>
								<!--/Modal_detailed_review-->
							</div>
						</div>
						<div class="row hide_after_success">
							<div class="col-xs-12">Write your review!*</div>
							<div class="col-xs-12 form-group margin-top-10">
								<textarea rows="4" class="form-control" id="txt_review_field"></textarea>
								<h6 class="txt_review_field_counter"></h6>
							</div>
							<div class="col-xs-12">
								<label><input type="checkbox" name="chk_anonymous_review" class="chk_anonymous_review" id="chk_anonymous_review">
									Review anonymously</label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary text-bold submit hide_after_success" data-loading-text="Processing...">
							Submit
						</button>
						<button type="button" class="btn btn-default hide_after_success" data-dismiss="modal">Cancel
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--/Modal teacher's review | Review-->
<?php } ?>