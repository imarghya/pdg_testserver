<?php 
/**
  * Internal Rating System 
  * @author Pritam Ghosh
  */

   if($is_current_user_admin) : 
	?>
	<div class="modal fade" id="modal_teachers_internal_rating" tabindex="-1" role="dialog" aria-labelledby="ViewTeacherNotesLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ViewTeacherNotesLabel">Internal Rating of Teacher</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
						    <div class="row">
								<div class="col-md-12">
									<button class="btn btn-sm btn-raised white-text blue-shade style-fix" data-loading="Reloading Notes" id="cmd_reload_teachers_internal_rating">
									<i class="fa fa-refresh" aria-hidden="true"></i> Reload Rating
								</button>
								</div>
							</div>
							<center><h4 class="" id="ViewTeacherNotesLabel">Rating</h4></center>

							<!-- Rating dropdown -->

							<label for="punctuality_rating">Punctuality</label>
							<select id="punctuality_rating">
						    	<option value="1">1</option>
						    	<option value="2">2</option>
						    	<option value="3">3</option>
						    	<option value="4">4</option>
						    	<option value="5">5</option>
						    </select><br>
						    <label for="responsiveness_rating">Responsiveness</label>
						    <select id="responsiveness_rating">
						    	<option value="1">1</option>
						    	<option value="2">2</option>
						    	<option value="3">3</option>
						    	<option value="4">4</option>
						    	<option value="5">5</option>
						    </select><br>
						    <label for="behaviour_rating">Behaviour</label>
						    <select id="behaviour_rating">
						    	<option value="1">1</option>
						    	<option value="2">2</option>
						    	<option value="3">3</option>
						    	<option value="4">4</option>
						    	<option value="5">5</option>
						    </select><br>
						    <label for="communication_skills_rating">Communication Skills</label>
						    <select id="communication_skills_rating">
						    	<option value="1">1</option>
						    	<option value="2">2</option>
						    	<option value="3">3</option>
						    	<option value="4">4</option>
						    	<option value="5">5</option>
						    </select>
							
							<textarea id="txt_teacher_review" class="form-control" placeholder="Write your review" maxlength="450"></textarea>

							<input type="text" class="form-control col-md-6" id="txt_teacher_rating_maker" placeholder="Input your name"/>

							<input type="hidden" value="<?= $teacher_user_id; ?>" id="hidden_teacher_user_id"/>

							<button class="btn btn-raised white-text green-shade style-fix" data-loading="Saving Ratings" id="cmd_save_teachers_internal_rating">
								<i class="fa fa-floppy-o" aria-hidden="true"></i>
								Save Rating
							</button>
							<br />
							<hr />
							<div id="div_teacher_internal_rating_save_result_area"></div>
						</div>
						<div class="col-md-6">
							<div id="div_teacher_internal_rating" style="overflow:auto; max-height: 400px;">
								<?= $str_teacher_internal_rating; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
