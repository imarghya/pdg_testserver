<!-- Create Student/Guardian Modal -->
<div class="modal fade" id="modal_single_table_cud" tabindex="-1" role="dialog" aria-labelledby="modal_single_table_cud_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_table_cud_title">Modal Title</h4>
			</div>
			<div class="modal-body">
				
				<div class="row">
					<div class="col-md-4">
						<label for="txt_single_table_modal_text_field" id="lbl_single_table_modal_textfield_label">Field Name</label>								
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="txt_single_table_modal_text_field"/>
					</div>
				</div>				
				<hr />
				<div id="div_single_table_modal_result_area"></div>
				
				
				<input type="hidden" id="hidden_single_table_modal_record_id"/>
				<input type="hidden" id="hidden_single_table_modal_save_action"/>
				<input type="hidden" id="hidden_single_table_modal_delete_action"/>
				<input type="hidden" id="hidden_single_table_modal_save_controller"/>
				<input type="hidden" id="hidden_single_table_modal_delete_controller"/>
				<input type="hidden" id="hidden_single_table_modal_delete_callback"/>
				<input type="hidden" id="hidden_single_table_modal_save_callback"/>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-loading-text="Deleting..." id="cmd_single_table_modal_delete"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
				<button type="button" class="btn btn-primary" data-loading-text="Saving..." id="cmd_single_table_modal_save"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
			</div>
		</div>
	</div>
</div>