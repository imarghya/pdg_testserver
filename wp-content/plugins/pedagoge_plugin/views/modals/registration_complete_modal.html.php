<!-- Create Student/Guardian Modal -->
<div class="modal fade" id="modal_profile_finished" tabindex="-1" role="dialog" aria-labelledby="modal_profile_finished_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_profile_finished_title">Congratulations!</h4>
			</div>
			<div class="modal-body">
				<h2>Congratulations!</h2>
				<p class="alert alert-success"><strong>Your profile has been updated and the Pedagoge Team been notified! If your profile is new, then it needs to be verified before being published!</strong></p>
				<p class="alert alert-info">You can create and publish your workshop at our website. <br />
					<a href="<?= home_url('/manageworkshop'); ?>" target="_blank">Create a Workshop</a> <br />
					<a href="<?= home_url('/workshop'); ?>" target="_blank">View Workshops</a> <br />
				</p>
				<p class="alert alert-success">
					<strong>Rest Assured! You will be notified as soon as we publish your profile! In the meanwhile, you can read some of our interesting articles @ our blog.</strong>
					<strong><a href="http://blog.pedagoge.com" target="_blank">Visit Our Blog!</a></strong>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
			</div>
		</div>
	</div>
</div>