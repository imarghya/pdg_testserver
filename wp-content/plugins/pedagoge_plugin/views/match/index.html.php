    <section class="body_wrapper margin_top_20">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
                <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
                    
                    	<div class="row">
                        	<div class="col-xs-12 blue_background top_radius h1_heading teacher_query_filter">
                                <h1 class="color_white text-center">Queries 
                                    <div class="dropdownfilter4">
                                        <button onclick="filterFunction4()" class="dropdownfilter4">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
                                             <div id="filterFunctionDropdown4" class="dropdown-content-filter4">
                                                  <h5 class="blue_color text-upppercase">Locality</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Garia <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">Jadavpur <span>X</span></p>
                                                  </div>
                                                  <h5 class="blue_color text-upppercase">Subject</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Hindi <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">English <span>X</span></p>
                                                  </div>
                                                  <h5 class="blue_color">Place of Teaching</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<label for="teacher_check1">Student Place</label> <input type="checkbox" name="teacher_check1" id="teacher_check1">
                                                  	<label for="teacher_check2">Teacher Place</label> <input type="checkbox" name="teacher_check2" id="teacher_check2">
                                                  	<label for="teacher_check3">Institutions</label> <input type="checkbox" name="teacher_check3" id="teacher_check3">
                                                  </div>
                                             </div>
                                    </div>
                                </h1>
                            </div>
                        </div>
                        
                        <div class="row margin_top_20">
                        	<div class="col-xs-12 navigation_menu navigation_menu_media no_padding_left_right">
                                <ul class="parent_tab">
                                	<li><a href="http://localhost/pedagoge/teacherdashboard/">Discover</a></li>
                                    <li><a href="http://localhost/pedagoge/saved/">Saved</a></li>
                                    <li><a href="http://localhost/pedagoge/interested/">Interested</a></li>
                                    <li><a href="http://localhost/pedagoge/referrals/">Referrals</a></li>
                                    <li><a href="http://localhost/pedagoge/tarchived/">Archived</a></li>
                                    <li class="menu_active"><a href="http://localhost/pedagoge/match/">Match</a></li>
                                </ul>
                            </div>
                      	</div>
                        
                        <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-8 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:</p>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<li><a href="#">Expand</a></li>
                                                <li><a href="#">Unmatch</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div> 
                       
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-6 col-sm-6 col-xs-12 localities">
                                            	<p>Year/Standard:</p>
                                                <p>Board/University:</p>
                                                <div class="preferred_location">
                                                    <p>Locality Type:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="blue_background">My Home</a></li>
                                                        <li><a href="#" class="blue_background">Tutor’s Home</a></li>
                                                        <li><a href="#" class="blue_background">Institue</a></li>
                                                    </ul>
                                                </div>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                                <div class="preferred_location">
                                                    <p class="width_100">Date of commencement: <strong>In a week</strong></p> 
                                                </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
                                         	<h5>Preferences: </h5>
                                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                            <ul class="pull-right side_right_menu side_right_menu_media text-center">
                                            	<li><a href="#">Collapse</a></li>
                                                <li><a href="#">Unmatch</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-8 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:</p>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<li><a href="#">Expand</a></li>
                                                <li><a href="#">Unmatch</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-8 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:</p>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<li><a href="#">Expand</a></li>
                                                <li><a href="#">Unmatch</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-8 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:</p>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                        <li><a href="#" class="blue_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<li><a href="#">Expand</a></li>
                                                <li><a href="#">Unmatch</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>
    