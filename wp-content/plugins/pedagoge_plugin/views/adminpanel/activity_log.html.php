

<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32">-->
      <title>Pedagoge</title>
      
      <link href="css/jodit.min.css" rel="stylesheet" type="text/css" media="screen">
      <link href="css/styleboard.css" rel="stylesheet" type="text/css" media="screen">
      <link href="css/sb-admin.css" rel="stylesheet" type="text/css" media="screen">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      
     
   </head>
   <body>
      <div id="wrapper">
         <!-- Navigation -->
         <nav class="navbar navbar-inverse navbar-fixed-top admin_white_background no_border" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header admin_blue_background admin_header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand color_white" href="index.html"><i class="glyphicon glyphicon-flash"></i> Pedagoge <span>Cp</span></a>
            </div>
            <!-- Menu Second -->
            <ul class="nav navbar-left top-nav admin_top_navigation">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation width_auto no_border" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#">Profile</a>
                     </li>
                     <li>
                        <a href="#">Inbox</a>
                     </li>
                     <li>
                        <a href="#">Settings</a>
                     </li>
                     <li>
                        <a href="#">Log Out</a>
                     </li>
                  </ul>
               </li>
               <li>
                  <p class="inline_block">
                     <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 293.129 293.129" style="enable-background:new 0 0 293.129 293.129;" xml:space="preserve">
                        <g>
                           <path d="M162.179,140.514c3.377-1.727,7.139-2.64,11.042-2.64c6.468,0,12.549,2.511,17.133,7.071l9.868-9.867 c24.42,8.56,52.664,3.08,72.186-16.441c16.426-16.426,22.904-39.026,19.446-60.329c-0.381-2.346-2.042-4.281-4.303-5.011 c-2.261-0.731-4.743-0.137-6.423,1.544l-14.652,14.652c-11.932,11.932-31.279,11.932-43.211,0 c-11.933-11.932-11.933-31.279,0-43.211l14.652-14.652c1.681-1.681,2.28-4.163,1.548-6.425c-0.731-2.263-2.669-3.92-5.016-4.301 c-21.302-3.458-43.903,3.02-60.328,19.446c-19.812,19.812-25.144,48.604-16.032,73.269l-21.402,21.402L162.179,140.514z" fill="#394263"/>
                           <path d="M123.179,179.296l-25.385-25.385L9.029,242.675c-11.542,11.542-11.542,30.255,0,41.797 c11.542,11.542,30.255,11.542,41.797,0l76.521-76.52C119.629,200.193,118.238,188.479,123.179,179.296z" fill="#394263"/>
                           <path d="M179.795,155.597c-1.815-1.815-4.195-2.723-6.574-2.723s-4.759,0.908-6.574,2.723l-5.299,5.299L66.956,66.504l4.412-4.412 c4.02-4.019,3.521-10.686-1.061-14.06L31.795,19.669c-3.701-2.725-8.837-2.338-12.087,0.912L3.356,36.934 c-3.25,3.25-3.637,8.387-0.912,12.087l28.362,38.512c3.374,4.581,10.037,5.085,14.06,1.061l4.412-4.413l94.392,94.392l-5.672,5.672 c-3.631,3.631-3.631,9.517,0,13.148l87.079,87.079c11.542,11.542,30.255,11.542,41.797,0c11.542-11.542,11.542-30.255,0-41.797 L179.795,155.597z" fill="#394263"/>
                        </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                     </svg>
                     Admin Control Panel :<strong>Queries</strong>
                  </p>
                  <!--<form class="navbar-form inline_block query_search" role="search">
                     <div class="input-group blue_border border_radius_30 overflow_hidden">
                        <input type="text" class="form-control border_none" placeholder="Search" name="srch-term" id="srch-term">
                        <div class="input-group-btn">
                           <button class="btn blue_background color_white" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                     </div>
                  </form>-->
               </li>
               <li class="margin_left_150">
                  <ul class="add_edit_icon">
                     <li class="two_box"><a href="#" title="New Query"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                     <li><a href="#" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                     <li class="two_box"><a href="#" title="Clone"><i class="fa fa-clone" aria-hidden="true" style="opacity:0;"></i></a></li>
                     <li><a href="#" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a></li>
                     <li><a href="#" title="Disapprove"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                  </ul>
               </li>
               <li>
                   <select id="assign" name="assign" class="text-uppercase text-center assign apperence_none">
                      <option value="Assign">Assign</option>
                      <option value="SL-3">SL-3</option>
                      <option value="SL-3">SL-3</option>
                      <option value="SL-3">SL-3</option>
                    </select> 
               </li>
            </ul>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav admin_top_navigation">
            	<li class="dropdown">
                  <a href="#" class="dropdown-toggle admin_user_id no_padding_top_bottom" data-toggle="dropdown"><strong><i class="fa fa-bell-o" aria-hidden="true"></i></strong></a>
                  <ul class="dropdown-menu notification_admin">
                     <li><!--For Show and hide notification "hide_notification" and "show_notification"-->
                        <a href="#" class="active">
                        	<div>
                        		<span>12</span>
                        	</div>
                            <div>
								<svg version="1.1" id="Layer_1" x="0px" y="0px"
                                     viewBox="0 0 87.954 63.442" style="enable-background:new 0 0 87.954 63.442;"
                                     xml:space="preserve">
                                <g>
                                    <rect x="78.811" y="33.275"  width="2.783" height="2.626"/>
                                    <rect x="78.811" y="40.63"  width="2.783" height="2.627"/>
                                    <rect x="78.811" y="48.001" width="2.783" height="2.627"/>
                                    <path d="M6.362,47.203l30.061,15.252c2.258,1.183,4.904,1.32,7.285,0.521l25.295-8.016h3.449v2.765h15.502
                                        V26.303H72.452v3.548H62.52c-0.79,0-1.718,0.137-2.521,0.399l-9.004,4.605c-0.402,0.138-0.929,0.261-1.33,0.261H35.881
                                        c-3.449,0-6.358,2.765-6.358,6.174c0,0.523,0,0.921,0.14,1.444l-18.535-6.174c-2.91-1.06-6.096,0.261-7.412,3.024
                                        C2.387,42.336,3.579,45.636,6.362,47.203 M75.098,54.96V29.852v-1.06h10.209v26.306H75.112V54.96H75.098z M6.084,40.63
                                        c0.791-1.581,2.521-2.241,4.239-1.705l21.859,7.234c1.067,0.659,2.258,1.183,3.712,1.183h11.387c1.33,0,2.384,1.045,2.384,2.365
                                        h2.645c0-2.765-2.258-4.992-5.028-4.992H35.756c-1.98,0-3.573-1.596-3.573-3.548c0-1.966,1.606-3.547,3.573-3.547h13.907
                                        c0.791,0,1.594-0.138,2.258-0.399l9.004-4.605c0.527-0.139,1.054-0.262,1.455-0.262h9.933v19.979h-3.712l-25.683,8.154
                                        c-1.717,0.523-3.713,0.399-5.306-0.397L7.678,44.702C6.084,44.054,5.434,42.075,6.084,40.63"/>
                                    <path d="M21.867,35.865c-6.496,0-11.78-5.246-11.78-11.694c0-6.447,5.284-11.693,11.78-11.693
                                        s11.781,5.246,11.781,11.693C33.648,30.619,28.363,35.865,21.867,35.865 M21.867,15.031c-5.076,0-9.207,4.101-9.207,9.14
                                        c0,5.04,4.131,9.139,9.207,9.139c5.077,0,9.207-4.1,9.207-9.139C31.074,19.132,26.944,15.031,21.867,15.031"/>
                                    <path d="M7.199,14.291C3.23,14.291,0,11.085,0,7.145C0,3.205,3.23,0,7.199,0c3.97,0,7.199,3.205,7.199,7.145
                                        C14.398,11.085,11.168,14.291,7.199,14.291 M7.199,2.553c-2.55,0-4.625,2.061-4.625,4.591c0,2.532,2.075,4.591,4.625,4.591
                                        s4.625-2.059,4.625-4.591C11.824,4.614,9.749,2.553,7.199,2.553"/>
                                </g>
                                </svg>
                        	</div>
						</a>
                     </li>
                     <li>
                        <a href="#">
                        	<div>
                                <span>12</span>
                            </div>
                            <div>
								<svg version="1.1" id="Layer_1" x="0px" y="0px"
                                 viewBox="0 0 84.781 84.742" style="enable-background:new 0 0 84.781 84.742;"
                                 xml:space="preserve">
                            <rect x="34.174" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="41.059" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="47.957" y="75.477"  width="2.646" height="2.645"/>
                            <polygon points="84.776,52.951 84.776,84.742 0,84.742 0,52.951 27.69,52.951 27.69,55.598 2.646,55.598 
                                2.646,82.096 82.131,82.096 82.131,55.598 57.086,55.598 57.086,52.951 "/>
                            <rect x="34.174" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="41.059" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="47.957" y="75.477"  width="2.646" height="2.645"/>
                            <rect x="6.358" y="27.781"  width="2.784" height="2.646"/>
                            <rect x="6.358" y="20.371"  width="2.784" height="2.646"/>
                            <rect x="6.358" y="12.945"  width="2.784" height="2.646"/>
                            <path d="M81.591,16.395L51.531,1.032c-2.259-1.191-4.904-1.33-7.286-0.526L18.95,8.582h-3.449V5.797H0v31.654
                                h15.501v-3.574h9.932c0.79,0,1.719-0.139,2.521-0.402l9.004-4.641c0.402-0.139,0.928-0.263,1.33-0.263h13.783
                                c3.449,0,6.358-2.785,6.358-6.22c0-0.527,0-0.928-0.139-1.455l18.535,6.22c2.909,1.067,6.095-0.263,7.411-3.047
                                C85.566,21.298,84.375,17.974,81.591,16.395 M12.854,8.582v25.294v1.066H2.646v-26.5h10.195v0.139H12.854z M81.868,23.016
                                c-0.79,1.593-2.521,2.258-4.239,1.718L55.77,17.447c-1.066-0.665-2.258-1.191-3.712-1.191H40.671c-1.33,0-2.383-1.053-2.383-2.383
                                h-2.646c0,2.785,2.259,5.029,5.028,5.029h11.525c1.98,0,3.573,1.607,3.573,3.574c0,1.981-1.605,3.574-3.573,3.574H38.289
                                c-0.79,0-1.593,0.138-2.258,0.402l-9.004,4.639c-0.527,0.141-1.054,0.264-1.455,0.264H15.64V11.228h3.712l25.683-8.215
                                c1.718-0.526,3.713-0.402,5.306,0.402l29.936,15.501C81.868,19.567,82.519,21.561,81.868,23.016"/>
                            <path d="M58.674,57.207c-0.69-8.279-7.505-15.018-15.957-15.018c-8.453,0-15.578,6.738-16.269,15.018h-2.806
                                v1.762h36.856v-1.762H58.674z M27.622,57.207c1.028-7.262,7.262-13.33,15.011-13.33c7.262,0,13.493,6.068,14.522,13.33H27.622z"/>
                            </svg>
                        </div>
                        </a>
                     </li>
                  </ul>
               </li>
            	
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation" data-toggle="dropdown"> New <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#">New</a>
                     </li>
                     <li>
                        <a href="#">Assigned</a>
                     </li>
                     <li>
                        <a href="#">Pending for Approval</a>
                     </li>
                  </ul>
               </li>
               
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle admin_user_id no_padding_top_bottom" data-toggle="dropdown"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <i class="fa fa-chevron-down down_arrow" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                     </li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                     </li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                     </li>
                  </ul>
               </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse admin_controler">
               <figure>
                  <a href="#" class="user_admin"><i class="fa fa-user-o" aria-hidden="true"></i></a>
                  <figcaption>
                     <p>Pankaj Sharma</p>
                     <ul>
                        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-inbox" aria-hidden="true"></i></a></li>
                     </ul>
                  </figcaption>
               </figure>
               <ul class="nav navbar-nav side-nav admin_menu_background admin_navigation">
                  <li><a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
                  <li class="active"><a href="#"><i class="fa fa-list-ul" aria-hidden="true"></i> Queries</a></li>
                  <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a></li>
                  <li><a href="#"><i class="fa fa-male" aria-hidden="true"></i> Teacher</a></li>
                  <li><a href="#"><i class="fa fa-university" aria-hidden="true"></i> Institute</a></li>                     
                  <li><a href="#"><i class="fa fa-bookmark" aria-hidden="true"></i> Workshop</a></li>
                  <li><a href="#"><i class="fa fa-rebel" aria-hidden="true"></i> Reviews</a></li>   
                  <li><a href="#"><i class="fa fa-book" aria-hidden="true"></i> Course Management</a></li>
                  <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Localities</a></li>
                  <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i> SEO</a></li>
                  <li><a href="#"><i class="fa fa-crosshairs" aria-hidden="true"></i> Import/Export</a></li>
                  <li><a href="#"><i class="fa fa-flag" aria-hidden="true"></i> Reports</a></li>
                  <li><a href="#"><i class="fa fa-wrench" aria-hidden="true"></i> Settings</a></li>
               </ul>
            </div>
            <!-- /.navbar-collapse -->
         </nav>
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row">
                  <aside class="col-xs-12 blue_border border_radius_12 query_model">
                     <div class="inner_container margin_top_10">
                        <div class="row">
                        	<div class="col-xs-12 communication_log">
                            	
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs text-center tab_decoration " role="tablist">
                                    <li role="presentation" class="active"><a href="#communication" aria-controls="communication" role="tab" data-toggle="tab">Communication</a></li>
                                    <li role="presentation"><a href="#activelog" aria-controls="activelog" role="tab" data-toggle="tab">Activity Log</a></li>
                                  </ul>
                                
                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                  
                                    <div role="tabpanel" class="tab-pane active mail_sms_decoration text-center" id="communication"><!--Communication-->
                                            <input id="mailPart" type="radio" name="tabs" checked> <label for="mailPart">Email</label>
                                            <input id="smsPart" type="radio" name="tabs"> <label for="smsPart">SMS</label>
                                        
                                         <div id="content1" class="mail_sms">
                                         	<div class="row">
                                  			<div class="col-xs-10 col-xs-offset-1 text-left">
                                           	<figure>
                                            	<ul class="email_button">
                                                	<li class="active"><button type="button">Send</button></li>
                                                	<li><button type="button">Save Now</button></li>
                                                    <li><button type="button">Discard</button></li>
                                                    <li>
                                                    	<select>
                                                            <option value="">Labels</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                        </select> 
                                                    </li>
                                                </ul>
                                            	<figcaption>
                                                	<ul class="email_option">
                                                    	<li>
                                                        	<label class="text-uppercase">To</label>
                                                            <select>
                                                                <option value="">Query Holder</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">Template</label>
                                                            <select>
                                                                <option value="">Confarmation Mail</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">&nbsp;</label>
                                                            <button type="button">Add Cc</button>
                                                            <button type="button">Add Bcc</button>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">Subject</label>
                                                            <input type="text" name="email_subject" id="email_subject">
                                                        </li>
                                                         <li>
                                                        	<label class="text-uppercase">&nbsp;</label>
                                                            <button type="button">Attach a file</button>
                                                            <button type="button"><span>Insert:</span> Invitation</button>
                                                        </li>
                                                    </ul>
												
                                                    <!--For Editor-->
                                                    
                                                    <div id="editor"></div>
                                                    
                                                    <!--For Editor-->
                                        
                                            </figcaption>
                                            </figure>
                                            
                                            </div>
                                            </div>
                                         </div>
                                            
                                         <div id="content2" class="mail_sms">
                                            <div class="row">
                                            	<div class="col-xs-10 col-xs-offset-1 sms_decoration">
                                                	<h5>Compose <span class="text-uppercase">Sms</span> Message</h5>
                                                    <ul class="sms_option">
                                                    	<li>
                                                        	<label class="text-uppercase">Compose form template</label>
                                                            <select>
                                                                <option value="">Confirmation</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">To</label>
                                                            <select>
                                                                <option value="">Query Holder</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                            </select>
                                                        </li>
                                                         <li>
                                                        	<label class="text-uppercase">Message Box <sup>*</sup></label>
                                                            <textarea name="sms" id="sms"></textarea>
                                                        </li>
                                                        <li>
                                                        	<button type="button">Cancel</button>
                                                            <button type="button">Send</button>
                                                        </li>
                                                    </ul>
                                                    
                                                </div>
                                            </div>
                                         </div>
                                        
                                    </div><!--Communication-->
                                    
                                    <div role="tabpanel" class="tab-pane" id="activelog"><!--Active Log-->
                                    	<div class="row">
                                        	<div class="col-xs-8 col-xs-offset-2">
                                                <ul class="active_log">
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder.</p> <button type="button" class="admin_blue_background color_white text-uppercase border_none border_radius_12 pull-right">View</button><div class="clearfix"></div></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Finlized Teacher.</p> <button type="button" class="admin_blue_background color_white text-uppercase border_none border_radius_12 pull-right">View Profile</button><div class="clearfix"></div></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder.</p> <button type="button" class="admin_blue_background color_white text-uppercase border_none border_radius_12 pull-right">View</button><div class="clearfix"></div></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder. <a href="#">Hot</a> to <a href="#" class="">Baked</a></p></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder. <a href="#">Warm</a> to <a href="#" class="">Hot</a></p></li>
                                                </ul>
                                             </div>
                                             <div class="col-xs-2 active_log_plus_button">
                                             	<button type="button" data-toggle="modal" data-target="#admin_add_notes"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                             </div>
                                        </div>
                                    </div><!--Active Log-->
                                 
                                  </div>
                                <a href="view_query.html" class="text-uppercase admin_blue_background color_white goto_viewquery">Back</a>
                            </div>
                        </div>
                     </div>
                  </aside>
                  
                  
                  
                  <aside class="model_wrapper">
                       
                       		<div class="modal fade" id="admin_add_notes" role="dialog">
                                <div class="modal-dialog width_600">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border admin_blue_background color_white admin_add_notes">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Add Notes</h4>
                                    </div>
                                    <form action="" method="post">
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
                                     <div class="col-sm-12 subject_type">
                                        <textarea id="reasonCloser" name="reasonCloser" class="non_grid_input black_border" ></textarea>
                                     </div>
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <input type="button" value="Submit" class="data_submit color_white pull-right"  data-toggle="modal" data-target="#subjectSelect">
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>

                       </aside>
                  
                  
                  
               </div>
            </div>
         </div>
      </div>
      <script src="js/jquery-2.1.1.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.slimscroll.min.js"></script>
      <script src="js/jodit.min.js"></script>
      <script src="js/style.js"></script>        
   </body>
</html>

