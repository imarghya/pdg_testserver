<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32">-->
      <title>Pedagoge</title>
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css_admin/styleboard.css" rel="stylesheet" type="text/css" media="screen">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css_admin/datepicker.css" rel="stylesheet" type="text/css" media="screen">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css_admin/sb-admin.css" rel="stylesheet" type="text/css" media="screen">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div id="wrapper">
         <!-- Navigation -->
         <nav class="navbar navbar-inverse navbar-fixed-top admin_white_background no_border" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header admin_blue_background admin_header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand color_white" href="index.html"><i class="glyphicon glyphicon-flash"></i> Pedagoge <span>Cp</span></a>
            </div>
            <!-- Menu Second -->
            <ul class="nav navbar-left top-nav admin_top_navigation">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation width_auto no_border" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#">Profile</a>
                     </li>
                     <li>
                        <a href="#">Inbox</a>
                     </li>
                     <li>
                        <a href="#">Settings</a>
                     </li>
                     <li>
                        <a href="#">Log Out</a>
                     </li>
                  </ul>
               </li>
               <li>
                  <p class="inline_block">
                     <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 293.129 293.129" style="enable-background:new 0 0 293.129 293.129;" xml:space="preserve">
                        <g>
                           <path d="M162.179,140.514c3.377-1.727,7.139-2.64,11.042-2.64c6.468,0,12.549,2.511,17.133,7.071l9.868-9.867 c24.42,8.56,52.664,3.08,72.186-16.441c16.426-16.426,22.904-39.026,19.446-60.329c-0.381-2.346-2.042-4.281-4.303-5.011 c-2.261-0.731-4.743-0.137-6.423,1.544l-14.652,14.652c-11.932,11.932-31.279,11.932-43.211,0 c-11.933-11.932-11.933-31.279,0-43.211l14.652-14.652c1.681-1.681,2.28-4.163,1.548-6.425c-0.731-2.263-2.669-3.92-5.016-4.301 c-21.302-3.458-43.903,3.02-60.328,19.446c-19.812,19.812-25.144,48.604-16.032,73.269l-21.402,21.402L162.179,140.514z" fill="#394263"/>
                           <path d="M123.179,179.296l-25.385-25.385L9.029,242.675c-11.542,11.542-11.542,30.255,0,41.797 c11.542,11.542,30.255,11.542,41.797,0l76.521-76.52C119.629,200.193,118.238,188.479,123.179,179.296z" fill="#394263"/>
                           <path d="M179.795,155.597c-1.815-1.815-4.195-2.723-6.574-2.723s-4.759,0.908-6.574,2.723l-5.299,5.299L66.956,66.504l4.412-4.412 c4.02-4.019,3.521-10.686-1.061-14.06L31.795,19.669c-3.701-2.725-8.837-2.338-12.087,0.912L3.356,36.934 c-3.25,3.25-3.637,8.387-0.912,12.087l28.362,38.512c3.374,4.581,10.037,5.085,14.06,1.061l4.412-4.413l94.392,94.392l-5.672,5.672 c-3.631,3.631-3.631,9.517,0,13.148l87.079,87.079c11.542,11.542,30.255,11.542,41.797,0c11.542-11.542,11.542-30.255,0-41.797 L179.795,155.597z" fill="#394263"/>
                        </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                     </svg>
                     Admin Control Panel :<strong>Queries</strong>
                  </p>
                  <form class="navbar-form inline_block query_search" role="search">
                     <div class="input-group blue_border border_radius_30 overflow_hidden">
                        <input type="text" class="form-control border_none" placeholder="Search" name="srch-term" id="srch-term">
                        <div class="input-group-btn">
                           <button class="btn blue_background color_white" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                     </div>
                  </form>
               </li>
               <li>
                  <ul class="add_edit_icon">
                     <li class="two_box"><a href="#" title="New Query"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                     <li><a href="#" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
                     <li class="two_box"><a href="#" title="Clone"><i class="fa fa-clone" aria-hidden="true" style="opacity:0;"></i></a></li>
                     <li><a href="#" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a></li>
                     <li><a href="#" title="Disapprove"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                  </ul>
               </li>
               <li>
                  <select id="assign" name="assign" class="text-uppercase text-center assign apperence_none">
                      <option value="Assign">Assign</option>
                      <option value="SL-3">SL-3</option>
                      <option value="SL-3">SL-3</option>
                      <option value="SL-3">SL-3</option>
                    </select>
               </li>
            </ul>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav admin_top_navigation">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation" data-toggle="dropdown"> New <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#">New</a>
                     </li>
                     <li>
                        <a href="#">Assigned</a>
                     </li>
                     <li>
                        <a href="#">Pending for Approval</a>
                     </li>
                  </ul>
               </li>
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle admin_user_id no_padding_top_bottom" data-toggle="dropdown"><span><i class="fa fa-user-o" aria-hidden="true"></i></span> <i class="fa fa-chevron-down down_arrow" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu">
                     <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                     </li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                     </li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                     </li>
                  </ul>
               </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse admin_controler">
               <figure>
                  <a href="#" class="user_admin"><i class="fa fa-user-o" aria-hidden="true"></i></a>
                  <figcaption>
                     <p>Pankaj Sharma</p>
                     <ul>
                        <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-inbox" aria-hidden="true"></i></a></li>
                     </ul>
                  </figcaption>
               </figure>
               <ul class="nav navbar-nav side-nav admin_menu_background admin_navigation">
                  <li>
                     <a href="index.html"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                  </li>
                  <li class="active">
                     <a href="#"><i class="fa fa-list-ul" aria-hidden="true"></i> Queries</a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-male" aria-hidden="true"></i> Teacher</a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-university" aria-hidden="true"></i> Institute</a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-bookmark" aria-hidden="true"></i> Workshop</a>
                  </li>
                  <li>
                     <a href="#"><i class="fa fa-rebel" aria-hidden="true"></i> Reviews</a>
                  </li>
                  <li>
                     <a href="blank-page.html"><i class="fa fa-book" aria-hidden="true"></i> Course Management</a>
                  </li>
                  <li>
                     <a href="index-rtl.html"><i class="fa fa-map-marker" aria-hidden="true"></i> Localities</a>
                  </li>
                  <li>
                     <a href="index-rtl.html"><i class="fa fa-google" aria-hidden="true"></i> SEO</a>
                  </li>
                  <li>
                     <a href="index-rtl.html"><i class="fa fa-crosshairs" aria-hidden="true"></i> Import/Export</a>
                  </li>
                  <li>
                     <a href="index-rtl.html"><i class="fa fa-flag" aria-hidden="true"></i> Reports</a>
                  </li>
                  <li>
                     <a href="index-rtl.html"><i class="fa fa-wrench" aria-hidden="true"></i> Settings</a>
                  </li>
               </ul>
            </div>
            <!-- /.navbar-collapse -->
         </nav>
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row">
                  <aside class="col-xs-12 table_content_settings blue_border border_radius_12">
                  	<div class="table-responsive">
                     	<table class="table admin_query_table_1905">
                        <thead>
                           <tr class="text-uppercase">
                           		<th></th>
                           		<th class="blank_space"></th>
                              	<th class="admin_blue_background color_white cell_decorate">Id</th>
                              	<th class="admin_blue_background color_white cell_decorate">Sales Rep</th>
                              	<th class="admin_blue_background color_white cell_decorate">Fast Pass</th>
                              	<th class="admin_blue_background color_white cell_decorate">Status</th>
                              	<th class="admin_blue_background color_white cell_decorate">Flag</th>
                                <th class="blank_space"></th>
                                <th class="admin_blue_background color_white cell_decorate">Query Dt</th>
                                <th class="admin_blue_background color_white cell_decorate">Lead Source</th>
                                <th class="admin_blue_background color_white cell_decorate">Name</th>
                                <th class="admin_blue_background color_white cell_decorate">Class/Age</th>
                                <th class="admin_blue_background color_white cell_decorate">Category</th>
                                <th class="admin_blue_background color_white cell_decorate">Subject</th>
                                <th class="admin_blue_background color_white cell_decorate">Locality</th>
                                <th class="admin_blue_background color_white cell_decorate">Follow up dt</th>
                                <th class="admin_blue_background color_white cell_decorate">Details</th>
                                <th class="admin_blue_background color_white cell_decorate">Roc</th>
                                <th class="admin_blue_background color_white cell_decorate">Review</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr class="active_query wenk-align--center wenk-length--large" data-wenk="8 Querries assigned to SL-3">
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query1" name="query1"><label for="query1"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="http://localhost/pedagoge/viewquery" class="admin_blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign" data-toggle="modal" data-target="#roc"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query2" name="query2"><label for="query2"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query3" name="query3"><label for="query3"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query4" name="query4"><label for="query4"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query5" name="query5"><label for="query5"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query6" name="query6"><label for="query6"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query7" name="query7"><label for="query7"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                           <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query8" name="query8"><label for="query8"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                            <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query9" name="query9"><label for="query9"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                            <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query10" name="query10"><label for="query10"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                            <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query11" name="query11"><label for="query11"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                            <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query12" name="query12"><label for="query12"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                            <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query13" name="query13"><label for="query13"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                            <tr>
                           		<td class="cell_decorate squere_width"><input type="checkbox" id="query14" name="query14"><label for="query14"></label></td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate">2015</td>
                                <td class="text-uppercase text-center cell_decorate">Sl-1</td>
                                <td class="text-uppercase text-center cell_decorate">Yes</td>
                                <td class="text-uppercase text-center cell_decorate"></td>
                                <td class="text-center cell_decorate">Urgent</td>
                                <td class="blank_space"></td>
                                <td class="text-uppercase text-center cell_decorate"><input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/></td>
                                <td class="text-center cell_decorate">Flyers</td>
                                <td class="text-center cell_decorate">Anita</td>
                                <td class="text-center cell_decorate">5 Years</td>
                                <td class="text-center cell_decorate">Non - Academic</td>
                                <td class="text-uppercase text-center cell_decorate">Dance</td>
                                <td class="text-uppercase text-center cell_decorate">Garia</td>
                                <td class="text-uppercase text-center cell_decorate">9-5-17</td>
                                <td class="text-center cell_decorate"><a href="view_query.html" class="blue_background color_white border_radius_12">View</a></td>
                                <td class="text-center cell_decorate"><input type="text" class="assign"></td>
                                <td class="text-uppercase text-center cell_decorate">No</td>
                           </tr>
                        </tbody>
                     </table>
                     
                     <aside class="model_wrapper">
                       
                       		<!--<button data-toggle="modal" data-target="#roc">New</button>-->
                       
                       		<div class="modal fade" id="roc" role="dialog">
                                <div class="modal-dialog">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border">
                                    <div class="modal-header">
                                      <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                      <h4 class="modal-title text-center text-uppercase">Roc</h4>
                                    </div>
                                    <form action="" method="post">
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
                                     <div class="col-sm-12 subject_type">
                                        <textarea id="reasonCloser" name="reasonCloser" placeholder="Reason of closer" class="non_grid_input black_border" ></textarea>
                                     </div>
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <input type="button" value="Submit" class="data_submit blue_background color_white"  data-toggle="modal" data-target="#subjectSelect">
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>

                       </aside>
                       
                        	
                     </div>
                     
                  </aside>
                  
                  <div class="col-xs-6 margin_top_20 copy_right">
                      <p>2017 &copy; All Right Reserved by <a href="http://www.pedagoge.com/" target="_blank">Pedagoge</a></p>
                  </div>
                  <div class="col-xs-6 margin_top_20 text-right copy_right">
                      <p>30-06-2017 <i class="fa fa-clock-o" aria-hidden="true"></i> 6.26 PM</p>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/jquery-2.1.1.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/bootstrap.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/jquery.slimscroll.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/datepicker.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/js_admin/style.js"></script>
      
      <script type="text/javascript">
      	/* Date Picker */

	$(document).ready(function(){
			var date_input=$('input[name="date"]'); //our date input has the name "date"
			var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
			date_input.datepicker({
				format: 'mm/dd/yyyy',
				container: container,
				todayHighlight: true,
				autoclose: true,
			})
		})

/* Date Ppicker */
      </script>
      
   </body>
</html>

