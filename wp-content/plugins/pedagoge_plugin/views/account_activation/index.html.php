
<section class="site-section site-section-top">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-success">
				<div class="panel-heading">Send profile activation Link</div>
				<div class="panel-body">
					<br />
					<p>Please check your inbox for Account Activation Email. If you have not received the mail then click on the button below to resend the Account Activation Email.</p>
					<div class="row">
						<div class="col-md-12">
							<input type="text" id="diff_email_addr" class="form-control" placeholder="Enter alternate Email Address"/></br>
							<button type="button" class="btn btn-success col-md-12" id="cmd_profile_activation_link" data-loading-text="Sending ...">Send Profile Activation Link</button></br>
							<button type="button" class="btn btn-success col-md-12" id="cmd_alt_profile_activation_link" data-loading-text="Sending ...">Send Profile Activation Link to Alternate Address</button></br>
							</br></br><a id="diff_email" href="#">Send Activation Link to a different Email Address</a>
							<input type="hidden" value="<?php echo $template_vars['email_address']; ?>" id="hidden_email_address"/>
							<input type="hidden" value="<?php echo $template_vars['activation_key']; ?>" id="hidden_account_activation_key"/>
						</div>
						<div class="col-md-12"><img class="center-block hidden_item" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" id="img_profile_activate_loader" /></div>
						<div class="col-md-12" id="div_profile_activate_result"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
