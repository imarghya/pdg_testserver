    <section class="body_wrapper margin_top_20">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
                <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
                    
                    	<div class="row">
                        	<div class="col-xs-12 blue_background top_radius h1_heading teacher_query_filter">
                                <h1 class="color_white text-center">Queries 
                                    <div class="dropdownfilter4">
                                        <button onclick="filterFunction4()" class="dropdownfilter4">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
                                             <div id="filterFunctionDropdown4" class="dropdown-content-filter4">
                                                  <h5 class="blue_color text-upppercase">Locality</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Garia <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">Jadavpur <span>X</span></p>
                                                  </div>
                                                  <h5 class="blue_color text-upppercase">Subject</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Hindi <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">English <span>X</span></p>
                                                  </div>
                                                  <h5 class="blue_color">Place of Teaching</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<label for="teacher_check1">Student Place</label> <input type="checkbox" name="teacher_check1" id="teacher_check1">
                                                  	<label for="teacher_check2">Teacher Place</label> <input type="checkbox" name="teacher_check2" id="teacher_check2">
                                                  	<label for="teacher_check3">Institutions</label> <input type="checkbox" name="teacher_check3" id="teacher_check3">
                                                  </div>
                                             </div>
                                    </div>
                                </h1>
                            </div>
                        </div>
                        
                        <div class="row margin_top_20">
                        	<div class="col-xs-12 navigation_menu navigation_menu_media no_padding_left_right">
                                <ul class="parent_tab">
                                	<li><a href="http://localhost/pedagoge/teacherdashboard/">Discover</a></li>
                                    <li><a href="http://localhost/pedagoge/saved/">Saved</a></li>
                                    <li><a href="http://localhost/pedagoge/interested/">Interested</a></li>
                                    <li class="menu_active"><a href="http://localhost/pedagoge/referrals/">Referrals</a></li>
                                    <li><a href="http://localhost/pedagoge/tarchived/">Archived</a></li>
                                    <li><a href="http://localhost/pedagoge/match/">Match</a></li>
                                </ul>
                            </div>
                      	</div>
                        
                       
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="referral text-center">
                                    	<li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                       <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                        <li>
                                        	<div class="row">
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-4 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#">Suresh Kumar</a>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>[1234567891]</p>
                                                        </div>
                                                         <div class="col-md-4 col-sm-12 col-xs-12">
                                                            <p>user@domain.com</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-8 col-sm-12 col-xs-12">
                                                            <p>was referred by you on <span>25/06/2017</span></p>
                                                        </div>
                                                         <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
                                                            <a href="#">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
                                        </li>
                                    </ul>
                                </div>
                           </div>
                       </div>
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>