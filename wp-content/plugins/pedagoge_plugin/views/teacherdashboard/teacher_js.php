<?php
$query_model = new ModelQuery();
$sub_tab=$query_model->get_subject_list_tag();
$loc_tab=$query_model->get_locality_list_tag();
?>
<script type="text/javascript">

/*function closest(e, t){
  return !e? false : e === t ? true : closest(e.parentNode, t);
}
var filter_pointer = document.getElementById("map_pointer");*/
$(document).ready(function(){
 //For get notification----------------------------------
 setInterval(function(){
  get_notification();
  get_ajax_notification_count();
  }, 30000);
 //-------------------------------------------------------

/*$('body').click(function() {
  if($("#filterFunctionDropdown4").hasClass("show"))
  {
    $("#filterFunctionDropdown4").removeClass('show');
   //$(".show").css("display","none");
  }
});*/

  //$(document).on('click','.dropdownfilter4',function(e){
  /*$('.dropdownfilter4').click(function(e){
    //e.preventDefault();
    //e.stopPropagation();
    $("#filterFunctionDropdown4").toggleClass("show");
    e.stopPropagation();
  });*/
  
$(document).click(function(e) 
{
    var container = $(".dropdownfilter4");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
    	if($("#filterFunctionDropdown4").hasClass("show"))
      	{
        	$("#filterFunctionDropdown4").removeClass("show");
        }
    }
    else{
    	//$("#myDropdown").toggleClass("show");
    }
});

$(document).click(function(e) 
{
    var container = $("#noti_btn");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
    	if($("#myDropdown").hasClass("show"))
      	{
        	$("#myDropdown").removeClass("show");
        }
    }
    else{
    	//$("#myDropdown").toggleClass("show");
    }
});

  $(document).on('click','#interestInu',function(e){
  //e.preventDefault();
  $("#query_type").val('interested_in_you');
  });
  
  $(document).on('click','#interestByu',function(e){
  //e.preventDefault();
  $("#query_type").val('interested');
  });
  
  $(document).on('click','#refInu',function(e){
  //e.preventDefault();
  $("#query_type").val('referrals_in_you');
  });
  
  $(document).on('click','#refByu',function(e){
  //e.preventDefault();
  $("#query_type").val('referrals');
  });
  
  $(document).on("click",".view_query",function(){
  var idd=$(this).attr("id");
  //$("#show"+idd).toggle();
  //console.log(parseInt(idd));
  var query_id  = parseInt(idd);

  $("#show"+idd).fadeIn(1000);

  var submit_data = {
      nonce: $ajaxnonce,
      action: $pedagoge_visitor_ajax_handler,
      pedagoge_callback_function: 'save_query_view',
      pedagoge_callback_class: 'ControllerTeacherdashboard',
      query_id : query_id,
      };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
    })
    .fail(function(jqxhr, status, error){ 
       //alert(error);
    });

  });
  
  $(document).on("click",".query_to_collapse",function(e){
    e.preventDefault();
    var id=$(this).attr("q_id");
    $("#show"+id+"in").fadeOut();
  });

  $(document).on("click",".query_by_collapse",function(e){
    e.preventDefault();
    var id=$(this).attr("q_id");
    $("#show"+id+"by").fadeOut();
  });

  //Expand Collapse Function--------
  $(document).on('click', '.expand', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   //alert(query_id);
   $("#q_show_"+query_id).hide();
   $("#q_hide_"+query_id).fadeIn(1000);
   
   $("#q_show2_"+query_id).hide();
   $("#q_hide2_"+query_id).fadeIn(1000);

   var submit_data = {
      nonce: $ajaxnonce,
      action: $pedagoge_visitor_ajax_handler,
      pedagoge_callback_function: 'save_query_view',
      pedagoge_callback_class: 'ControllerTeacherdashboard',
      query_id : query_id,
      };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
    })
    .fail(function(jqxhr, status, error){ 
       //alert(error);
    });

  });
  
  $(document).on('click', '.collapse', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   $("#q_show_"+query_id).fadeIn(1000);
   $("#q_hide_"+query_id).hide();
   
   $("#q_show2_"+query_id).fadeIn(1000);
   $("#q_hide2_"+query_id).hide();
  });
  //-----------------------------------------------------
 //For Save query-----------------------------------------
 $(document).on('click', '.save', function(e){
  e.preventDefault();
   var query_id=$(this).attr("q_id");
    var result = confirm("Are you sure?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'save_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id_"+query_id).hide();
                   alertify.success("Success: Query Saved Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------
      }
 });
 //-------------------------------------------------------
 //For UnSave query-----------------------------------------
 $(document).on('click', '.unsave', function(e){
  e.preventDefault();
   var query_id=$(this).attr("q_id");
    var result = confirm("Are you sure?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'unsave_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id_"+query_id).hide();
                   alertify.success("Success: Query Unsaved Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
      }
 });
 //-------------------------------------------------------
 //For interested-----------------------------------------
 $(document).on('click', '.showInterest', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   var student_id=$(this).attr("student-id");
   var idd=$(".showInterestBtn"+query_id).attr('idd');
   $("#query_id").val(query_id);
   $("#student_id").val(student_id);
   //alert(idd);
    if (idd==0) {
     $("#minimum_fees").val('');
     $("#maximum_fees").val('');
     $("#fees_date").val('');
     $("#fees_month").val('');
     $("#fees_year").val('');
     $("#teacher_fees_mobile_number").val('');
     $("#teacher_fees_email_id").val('');
     $("#teacher_fees_message").val('');
    $("#showInterest").modal("show");
    }
 });
 //-------------------------------------------------------
 //For submit show interest form--------------------------
  $(document).on('submit', '#form_intersted', function(event){
   event.preventDefault();
   var query_id=$("#query_id").val();
   var student_id=$("#student_id").val();
   var min_fees=$("#minimum_fees").val();
   var max_fees=$("#maximum_fees").val();
   var dd=$("#fees_date").val();
   var mm=$("#fees_month").val();
   var yy=$("#fees_year").val();
   var fees_date=yy+'-'+mm+'-'+dd;
   var mobile_number=$("#teacher_fees_mobile_number").val();
   var email_id=$("#teacher_fees_email_id").val();
   var message=$("#teacher_fees_message").val();
   
    var chkd=checkDate();
   if (chkd==1) {
    var result = confirm("Are you sure to you want show interest against this Query ?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'show_intersted',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        student_id : student_id,
        min_fees : min_fees,
        max_fees : max_fees,
        fees_date : fees_date,
        mobile_number : mobile_number,
        email_id : email_id,
        message : message,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   //$("#qspan_id_"+query_id).hide();
                   $(".showInterestBtn"+query_id).text('Interested');
                   $(".showInterestBtn"+query_id).attr('idd',1);
                   $("#showInterest").modal("hide");
                   alertify.success("Success: Query Interested Submitted Successfully."); 
                }
                else if(response=='fail'){
                  alertify.error("Failure: The session got expired please relogin again.");
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
      }
   }
   else{
    alertify.error("Error: Invalid Date!!."); 
   }
  });
 //----------------------------------------------------------------
  //For Un interested-----------------------------------------
 $(document).on('click', '.uninterest', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   var student_id=$(this).attr("student-id");
   $("#query_id").val(query_id);
    var result = confirm("Are you sure?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'un_intersted',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        student_id : student_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id_"+query_id).hide();
                   alertify.success("Success: Query UnInterested Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
    }
 });
 //----------------------------------------------------------------
   //For Ignore Responses-----------------------------------------
 $(document).on('click', '.ignore', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   var student_id=$(this).attr("student-id");
   $("#query_id").val(query_id);
    var result = confirm("Are you sure?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'ignore_response',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        student_id : student_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id2_"+query_id).hide();
                   alertify.success("Success: Query Ignored Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
    }
 });
 //----------------------------------------------------------------
 //Reffer Teacher--------------------------------------------------
 $(document).on('click', '.referSomeone', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   $("#query_id").val(query_id);
   $("#referSomeone").modal("show");
 });
  //For Submit Refeer form--------------------------
  $(document).on('submit', '#form_reffer', function(event){
   event.preventDefault();
   var query_id=$("#query_id").val();
   var name=$("#teacher_fees_name").val();
   var mobile_number=$("#teacher_fees_phone").val();
   var email_id=$("#teacher_fees_email").val();
  // var message=$("#teacher_fees_message").val();
    var result = confirm("Are you sure you want to refer this person ?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'reffer_teacher',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        name : name,
        mobile_number : mobile_number,
        email_id : email_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                if (response=='success') {
                   $("#teacher_fees_name").val('');
                   $("#teacher_fees_phone").val('');
                   $("#teacher_fees_email").val('');
                   $("#referSomeone").modal("hide");
                   alertify.success("Success: Query Referred Successfully.");
                }
                else if (response=='reffer_exist') {
                 alertify.error("This user has already been referred.");
                }
                 else if (response=='user_not_exist') {
                 alertify.error("Error: Reffer teacher not exist!!.");
                }
                else{
                 alertify.error("Error: Reffer by And Reffer to can not be same!!.");
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 //For Archive query-----------------------------------------------
 $(document).on('click', '.archive', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   var student_id=$(this).attr("student-id");
   $("#query_id").val(query_id);
    var result = confirm("Are you sure you want to archive this Query ?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_archive',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id_"+query_id).hide();
                   alertify.success("Success: Query Archived Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
    }
 });
 //----------------------------------------------------------------
  //For UnArchive query-----------------------------------------------
 $(document).on('click', '.unarchive', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   $("#query_id").val(query_id);
    var result = confirm("Are you sure you want to unarchive this Query ?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_unarchive',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id_"+query_id).hide();
                   alertify.success("Success: Query UnArchived Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
    }
 });
 //----------------------------------------------------------------
 //For Unmatch query-----------------------------------------------
 $(document).on('click', '.unmatch', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   $("#query_id").val(query_id);
    var result = confirm("Are you sure you want to unmatch this Query card ?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_unmatch',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#qspan_id_"+query_id).hide();
                   alertify.success("Success: Query Unmatched Successfully."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
    }
 });
 //----------------------------------------------------------------
 //For Ask Review--------------------------------------------------
 $(document).on('click', '.ask_review', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   var student_id=$(this).attr("student-id");
   $("#query_id").val(query_id);
    var result = confirm("Are you sure?");
    if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'ask_review',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        query_id : query_id,
        student_id : student_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   alertify.success("Success: Ask review Done."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
    }
 });
 //----------------------------------------------------------------
 //Get Filter queries----------------------------------------------
 $(document).on("change",".q_filter",function(){
  var locality=$("#locality_tags").val();
  var subject=$("#subject_tags").val();
  var query_type=$("#query_type").val();
  var place1='';
  var place2='';
  var place3='';
      if($('#teacher_check1').prop("checked") == true){
          place1=1;
      }
      else if($(this).prop("checked") == false){
          place1=0;
      }
  //---------------
      if($('#teacher_check2').prop("checked") == true){
          place2=1;
      }
      else if($(this).prop("checked") == false){
          place2=0;
      }
   //-------------
      if($('#teacher_check3').prop("checked") == true){
          place3=1;
      }
      else if($(this).prop("checked") == false){
          place3=0;
      }
    //-----------------
    get_filter_queries(query_type,locality,subject,place1,place2,place3);
  });
 //----------------------------------------------------------------
 
 //For Delete notification-----------------------------------------
  $(document).on('click', '.delete_noti', function(e){
   var noti_id=$(this).attr("idd");
  // alert(noti_id);
    $("#noti"+noti_id).hide();
     //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'delete_notification',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        noti_id : noti_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               alertify.success("Success: Notification successfully deleted."); 
              // $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
   });
 //----------------------------------------------------------------
 //View notification-----------------------------------------------
   $(document).on('click', '#noti_btn', function(e){
   //$("#count_noti").html('0');
     //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'view_notification',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
              // alertify.success("Success: Notification successfully deleted."); 
              // $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
   });
 //----------------------------------------------------------------
});
//Get Filtered Queries--------------------------------------------
function get_filter_queries(query_type,locality,subject,place1,place2,place3) {
 var query_sub_type='';
 //Query filter for discover tab----------------------------------
 if (query_type=='discover') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_discover_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
 //Query filter for saved tab----------------------------------
 if (query_type=='saved') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_saved_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
 
 //Query filter for Interested tab----------------------------------
 if (query_type=='interested') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_interested_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#interestByuI").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
 
  //Query filter for Interested tab----------------------------------
 if (query_type=='interested_in_you') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_interested_in_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#interestInuI").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
 //Query filter for Refeereals tab----------------------------------
 if (query_type=='referrals') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_referrals_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#refByu_content").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //---------------------------------------------------------------
 if (query_type=='referrals_in_you') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_referrals_inyou_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#refInu_content").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
 //Get Filter Archived queries-------------------------------------
  if (query_type=='archived') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_archived_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#arch_id").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
 
 //Get Filter Archived queries-------------------------------------
  if (query_type=='match') {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_match_filter_query',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        locality : locality,
        subject : subject,
        place1 : place1,
        place2 : place2,
        place3 : place3,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 }
 //----------------------------------------------------------------
}
//----------------------------------------------------------------
$(function(){  	
var country_list=[<?php echo $loc_tab; ?>];		
$('#locality_tags').tagit({
availableTags: country_list,              
singleField: true,
singleFieldNode: $('#mySingleField'),
allowSpaces: true,
placeholderText: 'Type Locality'
});

var subject_list=[<?php echo $sub_tab; ?>];		
$('#subject_tags').tagit({
availableTags: subject_list,              
singleField: true,
singleFieldNode: $('#subject_singleFieldTags'),
allowSpaces: true,
placeholderText: 'Type Subject'
});
//$('#readOnlyTags').tagit({
//	
//readOnly: true
//});

});

function checkDate(){
   var current_date = moment().format('YYYY-M-D');
   var dd=$("#fees_date").val();
   var mm=$("#fees_month").val();
   var yy=$("#fees_year").val();
   var fees_date=yy+'-'+mm+'-'+dd;
   var msec = Date.parse(current_date);
   var msec1 = Date.parse(fees_date);
   //alert(current_date);
   //alert(msec+','+msec1+' Date:'+current_date+','+fees_date);
   if (msec1 < msec) {
    //alert("Invalid Date!!");
    return 0;
   }
   else{
    return 1;
   }
}
//Get notification Ajax----------------------------------------
function get_notification() {
 //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_ajax_notification',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#head_notifacition").html(response);
            })
            .fail(function(jqxhr, status, error){ 
              // alert(error);
            });
         //--------------------------------------------------------
}
//-------------------------------------------------------------

//Get notification Count Ajax----------------------------------------
function get_ajax_notification_count() {
 //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_ajax_notification_count',
        pedagoge_callback_class: 'ControllerTeacherdashboard',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#count_noti").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
}
//-------------------------------------------------------------
</script>
<?php
if(isset($_GET['query_id'])){
?>
 <script type="text/javascript">
  $(document).ready(function(){
   $("#refInu").trigger("click");
   $("#<?php echo $_GET['query_id'];?>in").trigger("click");
  });
 </script>
<?php
}
?>