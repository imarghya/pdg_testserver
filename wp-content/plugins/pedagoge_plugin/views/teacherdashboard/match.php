<?php include("nav.php"); ?>
<?php
$teacher_id=$_SESSION['member_id'];
$teacher_model = new ModelTeacher();
$query_model = new ModelQuery();
$border_class='blue_background';
echo "<span id='result_query'>";
$queries=$teacher_model->get_matched_queries($teacher_id);
if(count($queries)>0){
//echo "<pre>"; print_r($queries); echo "</pre>";
foreach($queries AS $res){
$text='';
$localities = $query_model->get_q_locality($res->localities);
if($res->past!=''){
$border_class='gray_background';
$text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
}
else
{
	$border_class='blue_background';
}
$queryDate = $res->querydate;
if($res->archived != '1')
{
?>
<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
		    <div class="inner_container margin_top_10" id="q_show_<?php echo $res->id; ?>">
			<div class="row">
			    <div class="col-xs-12 inner_container_text blue_border">
				<div class="row">
				     <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
				     </div>
				</div>
				<div class="row margin_top_10">
				     <div class="col-md-8 col-sm-8 col-xs-7 localities">
					    <p>Year/Standard: <?php echo $res->standard; ?></p>
					    <div class="preferred_location">
						<p>Preferred Localities:</p> 
						<ul class="area">
						    <?php foreach($localities AS $loc){ ?>
							<li><a href="#" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
			                             <?php } ?>
						</ul>
					    </div>
				     </div>
				     <div class="col-md-4 col-sm-4 col-xs-5">
					<ul class="pull-right side_right_menu text-center">
					    <li><a href="#" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
					    <?php if($res->past==''){ ?>
					    <li><a href="#" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> unmatch">Unmatch</a></li>
					    <?php }else{?>
					    <li class="gray"><a href="#" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
					    <?php }?>
					</ul>
				     </div>
				</div>
				
			     </div>
			    </div>
			    </div> 
                     <!--  Expand View -->
			    <div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
				 <div class="row">
				     <div class="col-xs-12 inner_container_text blue_border">
				     
					 <div class="row">
					      <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
						 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
					      </div>
					 </div>
					 <div class="row margin_top_10">
					      <div class="col-md-6 col-sm-6 col-xs-12 localities">
						     <p>Year/Standard: <?php echo $res->standard; ?></p>
						     <p>Board/University: <?php echo $res->academic_board; ?></p>
						     <div class="preferred_location">
							 <p>Locality Type:</p> 
							 <ul class="area">
							<?php if($res->myhome==1){ ?>
							<li><a href="#" class="<?php echo $border_class; ?>">My Home</a></li>
							<?php } ?>
							<?php if($res->tutorhome==1){ ?>
							<li><a href="#" class="<?php echo $border_class; ?>">Tutor's Home</a></li>
							<?php } ?>
							<?php if($res->institute==1){ ?>
							<li><a href="#" class="<?php echo $border_class; ?>">Institue</a></li>
							<?php } ?> 
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p>Preferred Localities:</p> 
							 <ul class="area">
							     <?php foreach($localities AS $loc){ ?>
							     <li><a href="#" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
			                                     <?php } ?>
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
						     </div>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
						     <h5>Preferences: </h5>
						 <p class="text-justify"><?php echo trim($res->requirement); ?></p>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12">
						 <ul class="pull-right side_right_menu side_right_menu_media text-center">
						     <li><a href="#" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
						     <?php if($res->past==''){ ?>
						     <li><a href="#" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> unmatch">Unmatch</a></li>
						     <li><a href="#" class="<?php echo $border_class; ?> ask_review" student-id="<?php echo $res->fk_student_id; ?>">Ask Reviews</a></li>
						     <?php }else{?>
						     <li class="gray"><a href="#" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
						     <?php }?>
						 </ul>
					      </div>
					 </div>
				      </div>
				</div>
			    </div>
			</span>
<?php }}}else{ echo "<h6>No Query Found!</h6>"; }?>
<?php echo "</span>"; ?>                     
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>
