<?php include("nav.php"); ?>
<?php
$teacher_id=$_SESSION['member_id'];
$teacher_model = new ModelTeacher();
$query_model = new ModelQuery();
$teacher_arch_queries=$teacher_model->teacher_arch_queries($teacher_id);
?>
<span id='arch_id'>
	  <?php
	    if(count($teacher_arch_queries)>0){
	    foreach($teacher_arch_queries AS $res){
	    	if($res->id != ""){
	    $localities = $query_model->get_q_locality($res->localities);
	    ?>
	    <span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
	    <div class="inner_container margin_top_10">
	       <div class="row">
		   <div class="col-xs-12 inner_container_text gray_border">
		       <div class="row">
			    <div class="col-xs-12 gray_background top_radius h1_heading">
			       <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span> <strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong></h2>
			    </div>
		       </div>
		       <div class="row margin_top_10">
			    <div class="col-md-8 col-sm-8 col-xs-7 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="gray_background"><?php echo $loc; ?></a></li>
			                <?php } ?>
				     </ul>
				 </div>
			  </div>
			    <div class="col-md-4 col-sm-4 col-xs-5">
			       <ul class="pull-right side_right_menu text-center">
				   <li class="gray margin_top_20"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="gray_background unarchive">Unarchive</a></li>
			       </ul>
			    </div>
		       </div>
		       
		    </div>
	      </div>
	  </div>
	  </span>
	  <?php }}}else{ echo "<h6>No Query Found!</h6>"; }?>
        </span>
     </div>
     <div class="col-sm-1 col-md-2 col-xs-12 ad_section"></div>
</div>
</div>
</section>
