<?php include("nav.php"); ?>
<?php
$teacher_id=$_SESSION['member_id'];
$teacher_model = new ModelTeacher();
$query_model = new ModelQuery();

$int_in_queries=$teacher_model->get_interest_inyou_queries($teacher_id);
//echo "<pre>"; print_r($int_in_queries); echo "</pre>";
$int_in_you_queries=$teacher_model->get_interest_inv_queries($teacher_id);
?>
<!--This Section For Tab-->
<div class="tab_decoration_teacher">
    <div role="tabpanel" class="tab-pane active mail_sms_decoration text-center triangel_decoration" id="communication"><!--Communication--> 
	 <input id="interestByu" class="interestByu" type="radio" name="tabs" checked> <label for="interestByu">Shown interest by you</label>
	 <input id="interestInu" class="interestInu" type="radio" name="tabs"> <label for="interestInu">Showed interest in you</label>
<!-- ---------------------------------------------------Start Interest by You------------------------------- -->
	<div id="content1" class="mail_sms text-left">
	    <?php
	    echo "<span id='interestByuI'>";
	    if(count($int_in_queries)>0){
	    foreach($int_in_queries AS $res){
	    if($res->id != ""){
	    $localities = $query_model->get_q_locality($res->localities);
	    $border_class='blue_background';
	    $button_class='gray';
	    $text='';
	    $interest_green_color = '';
	    if($res->matched!=''){
	    $border_class='interest_green_background';
	    $button_class='interest_background';
	    $interest_green_color = 'interest_green_color';
	    }
	    if($res->past!=''){
        $border_class='gray_background';
	    $text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
        }

        $board_university_name = '';
		if($res->board != 0)
		{
			$board_university_name = $teacher_model->get_board_name($res->board);
		}
		if($res->university != 0){
			$board_university_name = $teacher_model->get_university_name($res->university);
		}
        
	    ?>
	 <span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
	    <div class="inner_container margin_top_20" id="q_show_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text blue_border">
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-8 col-sm-8 col-xs-7 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                <?php } ?>
				     </ul>
				 </div>
			  </div>
			  <div class="col-md-4 col-sm-4 col-xs-5">
			     <ul class="pull-right side_right_menu text-center">
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
				 <!--<li><a href="javascript:void(0);">Save this</a></li>-->
				 <?php if($res->past==''){ ?>
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->fk_student_id; ?>" class="<?php echo $border_class; ?> uninterest">Uninterest</a></li>
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
				 <?php } ?>
			     </ul>
			  </div>
		     </div>
		  </div>
	    </div>
	</div> 
       <!--  Expand View -->
	<div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text interest_green_border">
		 
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-6 col-sm-6 col-xs-12 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <p>Board/University: <?php echo $board_university_name; ?></p>
				 <div class="preferred_location">
				     <p>Locality Type:</p> 
				     <ul class="area">
					<?php if($res->myhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?= $border_class;?>">My Home</a></li>
					<?php } ?>
					<?php if($res->tutorhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?= $border_class;?>">Tutor's Home</a></li>
					<?php } ?>
					<?php if($res->institute==1){ ?>
					<li><a href="javascript:void(0);" class="<?= $border_class;?>">Institue</a></li>
					<?php } ?> 
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?= $border_class;?>"><?php echo $loc; ?></a></li>
					<?php } ?>
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p class="width_100">Date of commencement: <strong class="<?= $interest_green_color;?>"><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
				 </div>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 interest preference">
			    <h5>Preferences: </h5>
			     <p class="text-justify"><?php echo trim($res->requirement); ?></p>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12">
			     <ul class="pull-right side_right_menu side_right_menu_media text-center">
				 <li class="<?php echo $button_class; ?>"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
				  <?php if($res->past==''){ ?>
				 <li class="<?php echo $button_class; ?>"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->fk_student_id; ?>" class="<?php echo $border_class; ?> uninterest">Uninterest</a></li>
				 <?php }else{ ?>
				 <li class="<?php echo $button_class; ?>"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
				 <?php } ?>
				 <?php if($res->matched!='' && $res->past==''){ ?>
				 <li class="<?php echo $button_class; ?>"><a href="javascript:void(0);" class="<?php echo $border_class; ?> ask_review" student-id="<?php echo $res->fk_student_id; ?>">Ask Reviews</a></li>
				 <?php } ?>
				 <!--<li><a href="javascript:void(0);" data-toggle="modal" data-target="javascript:void(0);referSomeone">Refer someone</a></li>-->
			     </ul>
			  </div>
		     </div>
		     
		  </div>
	    </div>
	</div>
    </span>
    <?php }}}else{ echo "<h6>No Query Found!</h6>"; }?>
    <?php echo "</span>"; ?>
    
    <aside class="model_wrapper">
	<div class="modal fade" id="ask_Review" role="dialog">
	   <div class="modal-dialog show_interest">
	      <!-- Modal content-->
	      <div class="modal-content blue_border">
		 <div class="modal-header blue_background color_white fees">
		    <h4 class="modal-title text-center text-uppercase">Ask Review <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
		 </div>
		 <form action="" method="post" id="form_reffer">
		  <input type="hidden" value="" id="query_id"/>
		    <div class="modal-body no_padding_top_bottom">
		       <div class="row">
			  <div class="col-sm-12 subject_type fees">
			     <ul class="fees_address_details">
				<li><input type="text" value="" required id="teacher_fees_name" name="teacher_fees_name" placeholder="Name :" required> </li>
				<li><input type="text" value="" required id="teacher_fees_phone" name="teacher_fees_phone" placeholder="Phone Number :" required> </li>
				<li><input type="email" value="" required id="teacher_fees_email" name="teacher_fees_email" placeholder="Email Id :" required> </li>
			     </ul>
			     <div class="approve_unapprove no_padding_top_bottom">
				<input value="Submit" class="name_address blue_background color_white" type="submit">
			     </div>
			     <div class="clearfix"></div>
			  </div>
		       </div>
		    </div>
		 </form>
	      </div>
	   </div>
	</div>
     </aside>
    </div>
    <!-- End Interest by You-->
	
    <!--Start Interest In You -->
    <div id="content2" class="mail_sms text-left">
	    <?php
	    //echo "<pre>"; print_r($int_in_you_queries); echo "</pre>";
	    echo "<span id='interestInuI'>";
	    if(count($int_in_you_queries)>0){
	    foreach($int_in_you_queries AS $res){
	    $localities = $query_model->get_q_locality($res->localities);
	    
	    $is_interested=$teacher_model->chk_int($res->id,$teacher_id);
	    $idd=0;
	    $int_text='Show Interest';
	    if($is_interested==1){
	    	$idd=1;
		$int_text='Interested';			    
	    }
	    
	    
	    $border_class='blue_background';
	    $text='';
	    if($res->matched!=''){
	    $border_class='interest_green_background';
	    }
	    if($res->past!=''){
            $border_class='gray_background';
	    $text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
            }
	    ?>
	 <span id="qspan_id2_<?php echo $res->id; ?>" style="display: block;">
	    <div class="inner_container margin_top_20" id="q_show2_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text blue_border">
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-8 col-sm-8 col-xs-7 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
			                <?php } ?>
				     </ul>
				 </div>
			  </div>
			  <div class="col-md-4 col-sm-4 col-xs-5">
			     <ul class="pull-right side_right_menu text-center">
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
				 <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
				 <!--<li><a href="javascript:void(0);">Save this</a></li>-->
				 <?php if($res->past==''){ ?>
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->studentid; ?>" class="<?php echo $border_class; ?> ignore">Ignore</a></li>
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
				 <?php } ?>
			     </ul>
			  </div>
		     </div>
		  </div>
	    </div>
	</div> 
       <!--  Expand View -->
	<div class="inner_container margin_top_10" style="display: none;" id="q_hide2_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text interest_green_border">
		 
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-6 col-sm-6 col-xs-12 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <p>Board/University: <?php echo $res->academic_board; ?></p>
				 <div class="preferred_location">
				     <p>Locality Type:</p> 
				     <ul class="area">
					<?php if($res->myhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?= $border_class;?>">My Home</a></li>
					<?php } ?>
					<?php if($res->tutorhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?= $border_class;?>">Tutor's Home</a></li>
					<?php } ?>
					<?php if($res->institute==1){ ?>
					<li><a href="javascript:void(0);" class="<?= $border_class;?>">Institue</a></li>
					<?php } ?> 
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
					<?php } ?>
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p class="width_100">Date of commencement: <strong class="<?= $interest_green_color;?>"><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
				 </div>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 interest preference">
			    <h5>Preferences: </h5>
			     <p class="text-justify"><?php echo trim($res->requirement); ?></p>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12">
			     <ul class="pull-right side_right_menu side_right_menu_media text-center">
				 <li class="interest_background"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
				 <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
				  <?php if($res->past==''){ ?>
				 <li class="interest_background"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->studentid; ?>" class="<?php echo $border_class; ?> ignore">Ignore</a></li>
				 <!--<li class="interest_background"><a href="javascript:void(0);" data-toggle="modal" data-target="javascript:void(0);showInterest">Ask Reviews</a></li>-->
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
				 <?php } ?>
				 <!--<li><a href="javascript:void(0);" data-toggle="modal" data-target="javascript:void(0);referSomeone">Refer someone</a></li>-->
			     </ul>
			  </div>
		     </div>
		     
		  </div>
	    </div>
	</div>
    </span>
    <?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
    <?php echo "</span>"; ?>
    </div>
    
</div>
</div>


	<aside class="model_wrapper">
                                    <div class="modal fade" id="showInterest" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Show Interest <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post" id="form_intersted">
			                       <input type="hidden" value="" id="query_id"/>
					       <input type="hidden" value="" id="student_id"/>
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                      	<h4 class="modal-title text-center padding10">Fees</h4>
                                                         <ul class="student_fees">
                                                            <li><input type="text" value="" class="validate[required,custom[integer]]" name="minimum_fees" id="minimum_fees" placeholder="Min"></li>
                                                            <li><input type="text" value="" class="validate[required,funcCall[checkHELLO]]" name="maximum_fees" id="maximum_fees" placeholder="Max"></li>
                                                         </ul>
                                                         <h4 class="modal-title text-center padding10">Available From</h4>
                                                         <ul class="dd_mm_yy">
                                                            <li>
                                                               <select name="fees_date" id="fees_date" class="validate[required]">
                                                                  <option value="">DD</option>
								  <?php for($i=1;$i<=31;$i++){?>
                                                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								  <?php } ?>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_month" id="fees_month" class="validate[required]" >
                                                                  <option value="">MM</option>
                                                                  <option value="01">01</option>
                                                                  <option value="02">02</option>
                                                                  <option value="03">03</option>
                                                                  <option value="04">04</option>
                                                                  <option value="05">05</option>
                                                                  <option value="06">06</option>
                                                                  <option value="07">07</option>
                                                                  <option value="08">08</option>
                                                                  <option value="09">09</option>
                                                                  <option value="10">10</option>
                                                                  <option value="11">11</option>
                                                                  <option value="12">12</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_year" id="fees_year" class="validate[required]" >
							         <?php
								 $to_year = date('Y');
								 $end_year = $to_year + 7;
								 //$to_year=date('Y')+1;
								 ?>
                                                                  <option value="">YY</option>
								  <?php 
								  //for($y=1980;$y<=$to_year;$y++){
								  for($y=$to_year;$y<=$end_year;$y++){
								  ?>
                                                                  <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                                                  <?php } ?>
                                                               </select>
                                                            </li>
                                                         </ul>
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" value="" class="validate[required,custom[integer],maxSize[10],minSize[10]]" id="teacher_fees_mobile_number" name="teacher_fees_mobile_number" placeholder="Phone Number"> </li>
                                                            <li><input type="email" value="" class="validate[required,custom[email]]" id="teacher_fees_email_id" name="teacher_fees_email_id" placeholder="Email Id"> </li>
                                                            <li><textarea id="teacher_fees_message" name="teacher_fees_message" placeholder="Tell us why student's love you..." class="min_height_130 validate[required]"></textarea> </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>


                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>
