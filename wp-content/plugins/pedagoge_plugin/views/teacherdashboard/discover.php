<?php include("nav.php"); ?>
<?php
$teacher_id=$_SESSION['member_id'];
$teacher_model = new ModelTeacher();
$query_model = new ModelQuery();
$teachers=$teacher_model->get_user_data_teacher($teacher_id);
//print_r($teachers);

$subject1='';
$subject2='';
$subject3='';
if($teachers[0]->subject_taught1!=''){
$subject1=$teacher_model->get_subject_id($teachers[0]->subject_taught1);			    
}
if($teachers[0]->subject_taught2!=''){
$subject2=$teacher_model->get_subject_id($teachers[0]->subject_taught2);			    
}
if($teachers[0]->subject_taught3!=''){
$subject3=$teacher_model->get_subject_id($teachers[0]->subject_taught3);			    
}

$locality1='';
$locality2='';
$locality3='';
if($teachers[0]->locality1!=''){
$locality1=$teacher_model->get_locality_id($teachers[0]->locality1);				    
}

if($teachers[0]->locality2!=''){
$locality2=$teacher_model->get_locality_id($teachers[0]->locality2);				    
}

if($teachers[0]->locality3!=''){
$locality3=$teacher_model->get_locality_id($teachers[0]->locality3);				    
}

$hometutor=$teachers[0]->hometutor;
$tutorhome='';
$myhome='';
$institute='';
if($hometutor=='Own Location'){
$tutorhome=1;			    
}
else{
$tutorhome=0;			    
}
if($hometutor=='Student\'s Location'){
$myhome=1;			    
}
else{
$myhome=0;			    
}
if($hometutor=='Institute'){
$institute=1;			    
}
else{
$institute=0;			    
}

if($hometutor=='Both own location and student\'s location'){
$myhome=1;
$tutorhome=1;
}
echo "<span id='result_query'>";
$queries=$teacher_model->get_discovered_queries($teacher_id,$subject1,$subject2,$subject3,$locality1,$locality2,$locality3,$myhome,$tutorhome,$institute);
if(count($queries)>0){
//echo "<pre>"; print_r($queries); echo "</pre>";
foreach($queries AS $res){
$localities = $query_model->get_q_locality($res->localities);
$is_interested=$teacher_model->chk_int($res->id,$teacher_id);
$idd=0;
$int_text='Show Interest';
//if($is_interested==1){
if($is_interested > 0){
$idd=1;
$int_text='Interested';			    
}
$queryDate = $res->querydate;
if($res->query_status != "CLOSED" && $res->approved == 1){
if(strtotime($queryDate) < strtotime('3 month ago') === false)
{
//echo $queryDate;
	if($res->id != ""){
	//echo $teacher_model->get_university_name($res->university);
		$course_type_id_arr=$query_model->course_type_id_arr($res->subject);
		
		$board_university_name = '';
		if($res->board != 0)
		{
			$board_university_name = $teacher_model->get_board_name($res->board);
		}
		if($res->university != 0){
			$board_university_name = $teacher_model->get_university_name($res->university);
		}
		//echo "Board_University : ".$board_university_name;
		
?>
<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
			    <div class="inner_container margin_top_10" id="q_show_<?php echo $res->id; ?>">
			    <div class="row">
			    <div class="col-xs-12 inner_container_text blue_border">
			    
				<div class="row">
				     <div class="col-xs-12 blue_background top_radius h1_heading">
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
				     </div>
				</div>
				<div class="row margin_top_10">
				     <div class="col-md-8 col-sm-8 col-xs-7 localities">
				     	<?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
							<p>Age: <?php echo $res->age; ?> Years</p>
							<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
							 <p>Year/Standard: <?php if($res->age!=0){ echo $res->age.' Years / '; } ?><?php echo $standard=$res->standard; ?></p>
							 <p>Board/University: <?php echo $board_university_name; ?></p>
							 <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
							 <p>Year/Standard: <?php echo $standard=$res->standard; ?></p>
							 <!--<p>Board/University: <?php echo $res->academic_board; ?></p>-->
							 <p>Board/University: <?php echo $board_university_name; ?></p>
							 <p>Age: <?php echo $res->age; ?> Years</p>
						<?php } ?>
					    <!-- <p>Year/Standard: <?php if($res->age!=0){ echo $res->age.' Years / '; } ?><?php echo $res->standard; ?></p> -->
					    <div class="preferred_location">
						<p>Preferred Localities:</p> 
						<ul class="area">
						    <?php foreach($localities AS $loc){ ?>
							<li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
			                             <?php } ?>
						</ul>
					    </div>
				     </div>
				     <div class="col-md-4 col-sm-4 col-xs-5">
					<ul class="pull-right side_right_menu text-center">
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="expand blue_background">Expand</a></li>
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="save blue_background">Save this</a></li>
					    <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="blue_background showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
					</ul>
				     </div>
				</div>
				
			     </div>
			    </div>
			    </div> 
                     <!--  Expand View -->
			    <div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
				 <div class="row">
				     <div class="col-xs-12 inner_container_text blue_border">
				     
					 <div class="row">
					      <div class="col-xs-12 blue_background top_radius h1_heading">
						 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
					      </div>
					 </div>
					 <div class="row margin_top_10">
					      <div class="col-md-6 col-sm-6 col-xs-12 localities">
						     <!-- <p>Year/Standard: <?php if($res->age!=0){ echo $res->age.' Years / '; } ?><?php echo $res->standard; ?></p>
						     <p>Board/University: <?php echo $board_university_name; ?></p> -->
						     <?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
							<p>Age: <?php echo $res->age; ?> Years</p>
							<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
							 <p>Year/Standard: <?php if($res->age!=0){ echo $res->age.' Years / '; } ?><?php echo $standard=$res->standard; ?></p>
							 <p>Board/University: <?php echo $board_university_name; ?></p>
							 <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
							 <p>Year/Standard: <?php echo $standard=$res->standard; ?></p>
							 <p>Board/University: <?php echo $board_university_name; ?></p>
							 <p>Age: <?php echo $res->age; ?> Years</p>
							 <?php } ?>
						     <div class="preferred_location">
							 <p>Locality Type:</p> 
							 <ul class="area">
							<?php if($res->myhome==1){ ?>
							<li><a href="javascript:void(0);" class="blue_background">My Home</a></li>
							<?php } ?>
							<?php if($res->tutorhome==1){ ?>
							<li><a href="javascript:void(0);" class="blue_background">Tutor's Home</a></li>
							<?php } ?>
							<?php if($res->institute==1){ ?>
							<li><a href="javascript:void(0);" class="blue_background">Institue</a></li>
							<?php } ?> 
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p>Preferred Localities:</p> 
							 <ul class="area">
							     <?php foreach($localities AS $loc){ ?>
							     <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
			                                     <?php } ?>
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
						     </div>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
						     <h5>Preferences: </h5>
						 <p class="text-justify"><?php echo trim($res->requirement); ?></p>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12">
						 <ul class="pull-right side_right_menu side_right_menu_media text-center">
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background collapse">Collapse</a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background save">Save this</a></li>
						     <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="blue_background showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background referSomeone">Refer someone</a></li>
						 </ul>
					      </div>
					 </div>
					 
				      </div>
				</div>
			    </div>
</span>
<?php }}}}}else{ echo "<h6>No Query Found!</h6>"; }?>
<?php echo "</span>"; ?>                     
                        <aside class="model_wrapper">
                                    <div class="modal fade" id="showInterest" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Show Interest <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post" id="form_intersted">
			                       <input type="hidden" value="" id="query_id"/>
					       <input type="hidden" value="" id="student_id"/>
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                      	<h4 class="modal-title text-center padding10">Fees</h4>
                                                         <ul class="student_fees">
                                                            <li><input type="text" value="" class="validate[required,custom[integer]]" name="minimum_fees" id="minimum_fees" placeholder="Min"></li>
                                                            <li><input type="text" value="" class="validate[required,funcCall[checkHELLO]]" name="maximum_fees" id="maximum_fees" placeholder="Max"></li>
                                                         </ul>
                                                         <h4 class="modal-title text-center padding10">Available From</h4>
                                                         <ul class="dd_mm_yy">
                                                            <li>
                                                               <select name="fees_date" id="fees_date" class="validate[required]">
                                                                  <option value="">DD</option>
								  <?php for($i=1;$i<=31;$i++){?>
                                                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								  <?php } ?>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_month" id="fees_month" class="validate[required]" >
                                                                  <option value="">MM</option>
                                                                  <option value="01">01</option>
                                                                  <option value="02">02</option>
                                                                  <option value="03">03</option>
                                                                  <option value="04">04</option>
                                                                  <option value="05">05</option>
                                                                  <option value="06">06</option>
                                                                  <option value="07">07</option>
                                                                  <option value="08">08</option>
                                                                  <option value="09">09</option>
                                                                  <option value="10">10</option>
                                                                  <option value="11">11</option>
                                                                  <option value="12">12</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_year" id="fees_year" class="validate[required]" >
							         <?php
								 $to_year = date('Y');
								 $end_year = $to_year + 7;
								 //$to_year=date('Y')+1;
								 ?>
                                                                  <option value="">YY</option>
								  <?php 
								  //for($y=1980;$y<=$to_year;$y++){
								  for($y=$to_year;$y<=$end_year;$y++){
								  ?>
                                                                  <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                                                  <?php } ?>
                                                               </select>
                                                            </li>
                                                         </ul>
                                                         <ul class="fees_address_details">
                                                            <!--<li><input type="text" value="" class="validate[required,custom[integer],maxSize[10],minSize[10]]" id="teacher_fees_mobile_number" name="teacher_fees_mobile_number" placeholder="Phone Number"> </li>
                                                            <li><input type="email" value="" class="validate[required,custom[email]]" id="teacher_fees_email_id" name="teacher_fees_email_id" placeholder="Email Id"> </li>
                                                            <li><textarea id="teacher_fees_message" name="teacher_fees_message" placeholder="Tell us why student's love you..." class="min_height_130 validate[required]"></textarea> </li>-->
                                                            
                                                            <li><input type="text" value="" class="validate[funcCall[checkPhone]]" id="teacher_fees_mobile_number" name="teacher_fees_mobile_number" placeholder="Phone Number" maxlength=13> </li>
                                                            <li><input type="email" value="" class="validate[funcCall[checkEmail]]" id="teacher_fees_email_id" name="teacher_fees_email_id" placeholder="Email Id"> </li>
                                                            <li><textarea id="teacher_fees_message" name="teacher_fees_message" placeholder="Tell us why student's love you..." class="min_height_130 validate[required]"></textarea> </li>
                                                            
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                                 
                                 <aside class="model_wrapper">
                                    <div class="modal fade" id="referSomeone" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Refer someone <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post" id="form_reffer">
			                      <input type="hidden" value="" id="query_id"/>
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" class="validate[required]" value="" id="teacher_fees_name" name="teacher_fees_name" placeholder="Name :"> </li>
                                                            <li><input type="text" maxlength="13" class="validate[funcCall[checkPhone]]" data-errormessage="Not a valid Contact no." value="" id="teacher_fees_phone" name="teacher_fees_phone" placeholder="Phone Number :" > </li>
                                                            <li><input type="email" class="validate[funcCall[checkEmail]]" value="" id="teacher_fees_email" name="teacher_fees_email" placeholder="Email Id :" > </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>