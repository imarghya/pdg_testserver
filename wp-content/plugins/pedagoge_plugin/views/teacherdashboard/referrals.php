<?php include("nav.php"); ?>

<?php

$teacher_id=$_SESSION['member_id'];

$teacher_model = new ModelTeacher();

$query_model = new ModelQuery();



$referr_by_you_queries=$teacher_model->get_referr_by_you($teacher_id);

$user_email=$teacher_model->get_user_email_byid($teacher_id);

$referr_in_you_queries=$teacher_model->get_referr_in_you($user_email);

?>

<div class="tab_decoration_teacher referral_tab_decoration">

                       <div role="tabpanel" class="tab-pane active mail_sms_decoration text-center triangel_decoration" id="communication"><!--Communication--> 

                            <input id="refByu" class="interestByu" type="radio" name="tabs" checked> <label for="refByu">Referred by You</label>

                            <input id="refInu" class="interestInu" type="radio" name="tabs"> <label for="refInu">Referred to You</label>

                            <div id="content1" class="mail_sms text-left">      

                            <div class="inner_container margin_top_10">

                            <div class="row">

                                <div class="col-xs-12">

                            <ul class="referral text-center" id="refByu_content">

			    <?php

			    if(count($referr_by_you_queries)>0){

			    foreach($referr_by_you_queries AS $res){

			    $localities = $query_model->get_q_locality($res->localities);
			    //echo "<pre>"; print_r($localities); echo "</pre>";
			    ?>		

                                    	<li>

                                           <div class="row">

                                                <div class="col-md-7 col-sm-7 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">

                                                	<div class="row">

                                                    	<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 referral_name">

                                                            <a href="javascript:void(0);"><?php echo $res->name; ?></a>

                                                        </div>

                                                         <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">

                                                            <p>[<?php echo $res->phone; ?>]</p>

                                                        </div>

                                                         <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                                                            <p><?php echo $res->email; ?></p>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="col-md-5 col-sm-5 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">

                                                	<div class="row">

                                                    	<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">

                                                            <p>was referred by you on <span><?php echo date('d-m-Y',strtotime($res->created_at)); ?></span></p>

                                                        </div>

                                                         <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 referral_query">

                                                            <a href="javascript:void(0);" id="<?php echo $res->id; ?>by" class="view_query">View Query</a>

                                                        </div>

                                                    </div>

                                                </div>

                                             </div>

					   

					   <!--	-----------New Style ---------  -->

			                   <div id="show<?php echo $res->id; ?>by" class="col-xs-12 margin_top_20 inner_container_text blue_border admin_referial_expend text-left" style="display: none;">

                                                    <div class="row">

                                                         <div class="col-xs-12 blue_background top_radius h1_heading">

                                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>

                                                         </div>

                                                    </div>

                                                    <div class="row margin_top_10">

                                                         <div class="col-md-9 col-sm-9 col-xs-12 localities">

                                                                <p>Year/Standard: <?php echo $res->standard; ?></p>

                                                                <div class="preferred_location">

                                                                    <p>Preferred Localities:</p>

                                                                    <ul class="area">

							                 <?php foreach($localities AS $loc){ ?>

							                   <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>

							                 <?php } ?>

							            </ul>

                                                                </div>

                                                         </div>

                                                         <div class="col-md-3 col-sm-3 col-xs-12">
														    <ul class="pull-right side_right_menu side_right_menu_media text-center">
														     	<li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background query_by_collapse">Collapse</a></li>
														    </ul>
														</div>

                                                    </div>

                                                    

                                          </div>

			                 <!--	------------------------	-->

                                        </li>

			                <?php }}else{ ?>

			                <li>

							<div class="row">

							     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">No Referral Found!</div>

							</div>

							</li>

			                <?php } ?>

                                    </ul>

                                </div>

                           </div>

                       </div>                                                                                       

                   </div>

			    

<!--                        Tab Content 2                -->

			    <div id="content2" class="mail_sms text-left">

				   <div class="inner_container margin_top_10">

							<div class="row">

							<div class="col-xs-12">

							<ul class="referral text-center" id="refInu_content">

							<?php
							//echo "<pre>"; print_r($referr_in_you_queries);echo "</pre>";
							if(count($referr_in_you_queries)>0){
								//echo "<pre>"; print_r($referr_in_you_queries);echo "</pre>";
							foreach($referr_in_you_queries AS $res){

								$is_interested = $teacher_model->chk_int($res->id,$teacher_id);
							    $idd=0;
							    $int_text = 'Show Interest';
							    if($is_interested == 1){
							    	$idd = 1;
									$int_text = 'Interested';
							    }
							    $border_class='blue_background';
							    
							    $localities = $query_model->get_q_locality($res->localities);
							?>		

							<li>

							   <div class="row">

								<div class="col-md-7 col-sm-7 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">

									<div class="row">

									<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 referral_name">

									    <a href="javascript:void(0);"><?php echo $res->display_name; ?></a>

									</div>

									 <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">

									    <p>[<?php echo $res->mobile_no; ?>]</p>

									</div>

									 <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

									    <p><?php echo $res->user_email; ?></p>

									</div>

								    </div>

								</div>

								<div class="col-md-5 col-sm-5 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">

									<div class="row">

									<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">

									    <p>referred you on<br /><span><?php echo date('d-m-Y',strtotime($res->created_at)); ?></span></p>

									</div>

									 <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 referral_query">

									    <a href="javascript:void(0);" id="<?php echo $res->id; ?>in" class="view_query">View Query</a>

									</div>

								    </div>

								</div>

							     </div>

							    <!--	-----------New Style ---------  -->

							<div id="show<?php echo $res->id; ?>in" class="col-xs-12 margin_top_20 inner_container_text blue_border admin_referial_expend text-left" style="display: none;">

								 <div class="row">

								      <div class="col-xs-12 blue_background top_radius h1_heading">

									 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>

								      </div>

								 </div>

										    <div class="row margin_top_10">

										       <div class="col-md-6 col-sm-6 col-xs-12 localities">

											      <p>Year/Standard: <?php echo $res->standard; ?></p>

											      <p>Board/University: <?php echo $res->academic_board; ?></p>

											      <div class="preferred_location">

												  <p>Locality Type:</p> 

												  <ul class="area">

												 <?php if($res->myhome==1){ ?>

												 <li><a href="javascript:void(0);" class="blue_background">My Home</a></li>

												 <?php } ?>

												 <?php if($res->tutorhome==1){ ?>

												 <li><a href="javascript:void(0);" class="blue_background">Tutor's Home</a></li>

												 <?php } ?>

												 <?php if($res->institute==1){ ?>

												 <li><a href="javascript:void(0);" class="blue_background">Institue</a></li>

												 <?php } ?> 

												  </ul>

											      </div>

											      <div class="preferred_location">

												  <p>Preferred Localities:</p> 

												  <ul class="area">

												      <?php 
												      //echo "<pre>"; print_r($localities); echo "</pre>";
												      if(!empty($localities))
												      {
												      foreach($localities AS $loc){ ?>

												      <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>

												      <?php } } ?>

												  </ul>

											      </div>

											      <div class="preferred_location">

												  <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 

											      </div>

										       </div>

										       <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">

											      <h5>Preferences: </h5>

											  <p class="text-justify"><?php echo trim($res->requirement); ?></p>

										       </div>
										       <div class="col-md-3 col-sm-3 col-xs-12">
											     <ul class="pull-right side_right_menu side_right_menu_media text-center">
											     	<li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background query_to_collapse">Collapse</a></li>
												 	<li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
											     </ul>
											  </div>
										       

										  </div>



								 

						       </div>

						      <!--	------------------------	-->

							</li>

							<?php }}else{ ?>

							<li>

								       <div class="row">

									    <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">No Referral Found!</div>

								       </div>

							</li>

							<?php } ?>

							    </ul>

							</div>

			                    </div>

			           </div>   

			    </div>

                        </div>

                   </div>

                <aside class="model_wrapper">
                        <div class="modal fade" id="showInterest" role="dialog">
                        	<div class="modal-dialog show_interest">
                            <!-- Modal content-->
                            	<div class="modal-content blue_border">
                                	<div class="modal-header blue_background color_white fees">
                                    	<h4 class="modal-title text-center text-uppercase">Show Interest <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                    </div>
                                    <form action="" method="post" id="form_intersted">
			                       		<input type="hidden" value="" id="query_id"/>
					       				<input type="hidden" value="" id="student_id"/>
                                        <div class="modal-body no_padding_top_bottom">
                                        	<div class="row">
                                            	<div class="col-sm-12 subject_type fees">
                                                	<h4 class="modal-title text-center padding10">Fees</h4>
                                                    	<ul class="student_fees">
                                                            <li><input type="text" value="" class="validate[required,custom[integer]]" name="minimum_fees" id="minimum_fees" placeholder="Min"></li>
                                                            <li><input type="text" value="" class="validate[required,funcCall[checkHELLO]]" name="maximum_fees" id="maximum_fees" placeholder="Max"></li>
                                                        </ul>
                                                        <h4 class="modal-title text-center padding10">Available From</h4>
                                                        <ul class="dd_mm_yy">
                                                        	<li>
                                                            	<select name="fees_date" id="fees_date" class="validate[required]">
                                                                	<option value="">DD</option>
								  									<?php for($i=1;$i<=31;$i++){?>
                                                                	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								  									<?php } ?>
                                                               	</select>
                                                            </li>
                                                            <li>
                                                            	<select name="fees_month" id="fees_month" class="validate[required]" >
                                                                	<option value="">MM</option>
                                                                	<option value="01">01</option>
                                                                	<option value="02">02</option>
																	<option value="03">03</option>
																	<option value="04">04</option>
																	<option value="05">05</option>
																	<option value="06">06</option>
																	<option value="07">07</option>
																	<option value="08">08</option>
																	<option value="09">09</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
                                                               	</select>
                                                            </li>
                                                            <li>
                                                            	<select name="fees_year" id="fees_year" class="validate[required]" >
							        							<?php
																$to_year = date('Y');
																$end_year = $to_year + 7;
																?>
                                                                	<option value="">YY</option>
																	<?php 
																	//for($y=1980;$y<=$to_year;$y++){
																	for($y=$to_year;$y<=$end_year;$y++){
																	?>
                                                                  	<option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                                                  	<?php } ?>
                                                               	</select>
                                                            </li>
                                                        </ul>
                                                        <ul class="fees_address_details">
                                                            <li><input type="text" value="" class="validate[required,custom[integer],maxSize[10],minSize[10]]" id="teacher_fees_mobile_number" name="teacher_fees_mobile_number" placeholder="Phone Number"> </li>
                                                            <li><input type="email" value="" class="validate[required,custom[email]]" id="teacher_fees_email_id" name="teacher_fees_email_id" placeholder="Email Id"> </li>
                                                            <li><textarea id="teacher_fees_message" name="teacher_fees_message" placeholder="Tell us why student's love you..." class="min_height_130 validate[required]"></textarea> </li>
                                                        </ul>
                                                        <div class="approve_unapprove no_padding_top_bottom">
                                                        	<input value="Submit" class="name_address blue_background color_white" type="submit">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </aside>


              </div>

           <div class="col-sm-1 col-md-2 col-xs-12 ad_section"></div>

      </div>

</div>

</section>