<?php include("nav.php"); ?>
<?php
$teacher_id=$_SESSION['member_id'];
$teacher_model = new ModelTeacher();
$query_model = new ModelQuery();
$border_class='blue_background';
echo "<span id='result_query'>";
$queries=$teacher_model->get_saved_queries($teacher_id);
if(count($queries)>0){
//echo "<pre>"; print_r($queries); echo "</pre>";
foreach($queries AS $res){
$text='';
$localities = $query_model->get_q_locality($res->localities);
if($res->past!=''){
$border_class='gray_background';
$text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
}
else
{
   $border_class='blue_background';
   $text='';
}
$is_interested=$teacher_model->chk_int($res->id,$teacher_id);
$idd=0;
$int_text='Show Interest';
if($is_interested==1){
$idd=1;
$int_text='Interested';			    
}
if($res->archived != '1')
{
	if($res->id != ""){
?>
<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
		    <div class="inner_container margin_top_10" id="q_show_<?php echo $res->id; ?>">
			<div class="row">
			    <div class="col-xs-12 inner_container_text blue_border">
				<div class="row">
				     <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
				     </div>
				</div>
				<div class="row margin_top_10">
				     <div class="col-md-8 col-sm-8 col-xs-7 localities">
					    <p>Year/Standard: <?php echo $res->standard; ?></p>
					    <div class="preferred_location">
						<p>Preferred Localities:</p> 
						<ul class="area">
						    <?php foreach($localities AS $loc){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                             <?php } ?>
						</ul>
					    </div>
				     </div>
				     <div class="col-md-4 col-sm-4 col-xs-5">
					<ul class="pull-right side_right_menu text-center">
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
					    <?php if($res->past==''){ ?>
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> unsave">UNSAVE</a></li>
					    <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
					    <?php
					    }
					    else
					    {
					    ?>
					    <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
					    <?php
					    }
					    ?>
					</ul>
				     </div>
				</div>
				
			     </div>
			    </div>
			    </div> 
                     <!--  Expand View -->
			    <div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
				 <div class="row">
				     <div class="col-xs-12 inner_container_text blue_border">
				     
					 <div class="row">
					      <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
						 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
					      </div>
					 </div>
					 <div class="row margin_top_10">
					      <div class="col-md-6 col-sm-6 col-xs-12 localities">
						     <p>Year/Standard: <?php echo $res->standard; ?></p>
						     <p>Board/University: <?php echo $res->academic_board; ?></p>
						     <div class="preferred_location">
							 <p>Locality Type:</p> 
							 <ul class="area">
							<?php if($res->myhome==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?>">My Home</a></li>
							<?php } ?>
							<?php if($res->tutorhome==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?>">Tutor's Home</a></li>
							<?php } ?>
							<?php if($res->institute==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?>">Institue</a></li>
							<?php } ?> 
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p>Preferred Localities:</p> 
							 <ul class="area">
							     <?php foreach($localities AS $loc){ ?>
							     <li><a href="javascript:void(0);" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
			                                     <?php } ?>
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
						     </div>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
						     <h5>Preferences: </h5>
						 <p class="text-justify"><?php echo trim($res->requirement); ?></p>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12">
						 <ul class="pull-right side_right_menu side_right_menu_media text-center">
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
						     <?php if($res->past==''){ ?>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> unsave">UNSAVE</a></li>
						     <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> referSomeone">Refer someone</a></li>
						     <?php }else{ ?>
						     <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
						     <?php } ?>
						 </ul>
					      </div>
					 </div>
					 
				      </div>
				</div>
			    </div>
</span>
<?php }}}}else{ echo "<h6>No Query Found!</h6>"; }?>
<?php echo "</span>"; ?>
                       
    
		       
                        <aside class="model_wrapper">
                                    <div class="modal fade" id="showInterest" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Show Interest <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post" id="form_intersted">
			                       <input type="hidden" value="" id="query_id"/>
					       <input type="hidden" value="" id="student_id"/>
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                      	<h4 class="modal-title text-center padding10">Fees</h4>
                                                         <ul class="student_fees">
                                                            <li><input type="text" value="" class="validate[required,custom[integer]]" name="minimum_fees" id="minimum_fees" placeholder="Min"></li>
                                                            <li><input type="text" value="" class="validate[required,funcCall[checkHELLO]]" name="maximum_fees" id="maximum_fees" placeholder="Max"></li>
                                                         </ul>
                                                         <h4 class="modal-title text-center padding10">Available From</h4>
                                                         <ul class="dd_mm_yy">
                                                            <li>
                                                               <select name="fees_date" id="fees_date" class="validate[required]">
                                                                  <option value="">DD</option>
								  <?php for($i=1;$i<=31;$i++){?>
                                                                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								  <?php } ?>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_month" id="fees_month" class="validate[required]" >
                                                                  <option value="">MM</option>
                                                                  <option value="01">01</option>
                                                                  <option value="02">02</option>
                                                                  <option value="03">03</option>
                                                                  <option value="04">04</option>
                                                                  <option value="05">05</option>
                                                                  <option value="06">06</option>
                                                                  <option value="07">07</option>
                                                                  <option value="08">08</option>
                                                                  <option value="09">09</option>
                                                                  <option value="10">10</option>
                                                                  <option value="11">11</option>
                                                                  <option value="12">12</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_year" id="fees_year" class="validate[required]" >
							         <?php
								 $to_year=date('Y');
                         $end_year = $to_year + 7;
								 ?>
                                                                  <option value="">YY</option>
								  <?php for($y=$to_year;$y<=$end_year;$y++){ ?>
                                                                  <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                                                  <?php } ?>
                                                               </select>
                                                            </li>
                                                         </ul>
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" value="" class="validate[required,custom[integer],maxSize[10],minSize[10]]" id="teacher_fees_mobile_number" name="teacher_fees_mobile_number" placeholder="Phone Number"> </li>
                                                            <li><input type="email" value="" class="validate[required,custom[email]]" id="teacher_fees_email_id" name="teacher_fees_email_id" placeholder="Email Id"> </li>
                                                            <li><textarea id="teacher_fees_message" name="teacher_fees_message" placeholder="Tell us why student's love you..." class="min_height_130 validate[required]"></textarea> </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                                 
                                 <aside class="model_wrapper">
                                    <div class="modal fade" id="referSomeone" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Refer someone <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post" id="form_reffer">
			                      <input type="hidden" value="" id="query_id"/>
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" class="validate[required]" value="" required id="teacher_fees_name" name="teacher_fees_name" placeholder="Name :" required> </li>
                                                            <li><input type="text" value="" required id="teacher_fees_phone" name="teacher_fees_phone" maxlength="13" class="validate[funcCall[checkPhone]]" data-errormessage="Not a valid Contact no." placeholder="Phone Number :" required> </li>
                                                            <li><input type="email" class="validate[funcCall[checkEmail]]" value="" required id="teacher_fees_email" name="teacher_fees_email" placeholder="Email Id :" required> </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>