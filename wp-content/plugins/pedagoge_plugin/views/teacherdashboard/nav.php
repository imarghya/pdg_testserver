<section class="body_wrapper margin_top_20">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
                <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
                    
                    	<div class="row">
				<?php
				if($page_type==''){
				$query_type='discover';
				}
				elseif($page_type=='saved'){
				$query_type='saved';	
				}
				elseif($page_type=='interested'){
				$query_type='interested';	
				}
				elseif($page_type=='referrals'){
				$query_type='referrals';	
				}
				elseif($page_type=='archived'){
				$query_type='archived';	
				}
				elseif($page_type=='match'){
				$query_type='match';	
				}
				?>
                        	<div class="col-xs-12 blue_background top_radius h1_heading teacher_query_filter">
				<input type="hidden" id="query_type" value="<?php echo $query_type; ?>"/>
                                <h1 class="color_white text-center">Queries 
                                    <div class="dropdownfilter4">
                                        <button onclick="filterFunction4()" class="dropdownfilter4">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
                                             <div id="filterFunctionDropdown4" class="dropdown-content-filter4">
                                                  <h5 class="blue_color text-upppercase">Locality</h5>
                                                  <!--<div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Garia <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">Jadavpur <span>X</span></p>
                                                  </div>-->
						   <input name="mytags" class="q_filter" id="locality_tags" type="hidden">
		                                   <ul id="singleFieldTags"></ul>
                                                  <h5 class="blue_color text-upppercase">Subject</h5>
                                                  <!--<div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Hindi <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">English <span>X</span></p>
                                                  </div>-->
						  <input name="subject_tag" class="q_filter" id="subject_tags" type="hidden">
		                                   <ul id="subject_singleFieldTags"></ul>
                                                  <h5 class="blue_color">Place of Teaching</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<label for="teacher_check1">Student Place</label> <input type="checkbox" value="0" class="q_filter" name="teacher_check1" id="teacher_check1">
                                                  	<label for="teacher_check2">Teacher Place</label> <input type="checkbox" value="0" class="q_filter" name="teacher_check2" id="teacher_check2">
                                                  	<label for="teacher_check3">Institutions</label> <input type="checkbox" value="0" class="q_filter" name="teacher_check3" id="teacher_check3">
                                                  </div>
                                             </div>
                                    </div>
                                </h1>
                            </div>
                        </div>
                        
                        <div class="row margin_top_20">
                        	<div class="col-xs-12 navigation_menu navigation_menu_media no_padding_left_right">
                                <ul class="parent_tab">
                                    <li <?php if($page_type==''){ echo 'class="menu_active"';} ?> ><a href="<?= home_url() ?>/teacherdashboard/">Discover</a></li>
                                    <li <?php if($page_type=='saved'){ echo 'class="menu_active"';} ?>><a href="<?= home_url() ?>/teacherdashboard/?page_type=saved">Saved</a></li>
                                    <li <?php if($page_type=='interested'){ echo 'class="menu_active"';} ?>><a href="<?= home_url() ?>/teacherdashboard/?page_type=interested">Interested</a></li>
                                    <li <?php if($page_type=='referrals'){ echo 'class="menu_active"';} ?>><a href="<?= home_url() ?>/teacherdashboard/?page_type=referrals">Referrals</a></li>
                                    <li <?php if($page_type=='archived'){ echo 'class="menu_active"';} ?>><a href="<?= home_url() ?>/teacherdashboard/?page_type=archived">Archived</a></li>
                                    <li <?php if($page_type=='match'){ echo 'class="menu_active"';} ?>><a href="<?= home_url() ?>/teacherdashboard/?page_type=match">Matched</a></li>
                                </ul>
                            </div>
                      	</div>