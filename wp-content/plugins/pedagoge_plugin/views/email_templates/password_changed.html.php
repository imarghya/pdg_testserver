<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					Hello <?php echo isset($template_vars['display_name'])?$template_vars['display_name']:'there'; ?>,<br><br>
					This email is to confirm that your Pedagoge Account's password has been changed successfully!<br><br>					
					<strong>Team Pedagoge</strong></strong><br>
				</td>
            </tr>
        </table>
    </td>
</tr>

<!-- Content section ends -->