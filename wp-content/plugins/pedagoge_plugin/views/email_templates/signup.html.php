<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					Hello there,<br>
					
					Welcome to Pedagoge and thanks for signing up to receive notification from us as soon as we go 
					
					live.We are in our beta phase right now, and would like to invite you to come check us out!<br><br>
					
					Pedagoge is an online platform for students, to search and connect with private coaches 
					
					(academic & non-academic), based on individual needs. Reviews and recommendations given by 
					
					organically built trusted community of students and teachers facilitate well-informed decisions.<br><br>
					
					Please use this email address to help us identify you as one of our first members.<br><br>
					
					We hope you enjoy this opportunity to take Pedagoge for a spin. Feel free to test it out and get 
					
					acquainted with no limits and no obligation on our free-to-use platform.<br><br>
					
					Welcome aboard!<br>
					
					Team Pedagoge<br>
				</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <!-- BULLETPROOF BUTTON -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
        	<center>    
            	<tr>
                    <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                        <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                            <tr>
                                <td align="center">
                                	<a href="<?php echo $template_vars['account_activation_link']; ?>" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #1DB198; border-top: 15px solid #1DB198; border-bottom: 15px solid #1DB198; border-left: 25px solid #1DB198; border-right: 25px solid #1DB198; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Click Here to Verify &rarr;</a>
                                </td>
								
							</tr>
							<tr><br><br></tr>
							<tr>
								<td><br/><br/><br/>Or just go to this link:  <?php echo $template_vars['account_activation_link']; ?></td>
							</tr>
                        </table>
                    </td>
                </tr>
            </center>
        </table>
    </td>
</tr>

<!-- Content section ends -->