<?php
	$teacher_name='';
	$teacher_email = '';
	$teacher_contact_number = '';
	$teacher_role = '';
	
	$student_name = '';
	$student_email = '';
	$student_contact_number = '';
	$student_role = '';
	
	$review_new_id = isset($template_vars['review_id']) ? $template_vars['review_id'] : '';
	
	$review_text = '';
	$rating1 = '';
	$rating2 = '';
	$rating3 = '';
	$rating4 = '';
	$average_rating = '';
	$overall_rating = '';
	$is_anonymous = '';
	$secret_key = '';
		
	if(isset($template_vars['teacher_data'])) {
		foreach($template_vars['teacher_data'] as $teacher_info) {
			$teacher_name = $teacher_info->teacher_name;
			$teacher_email = $teacher_info->user_email;
			$teacher_contact_number = $teacher_info->mobile_no;
			$teacher_role = $teacher_info->user_role_display;
		}
	}
	
	if(isset($template_vars['student_data'])) {
		foreach($template_vars['student_data'] as $student_info) {
			$student_name = $student_info->first_name.' '.$student_info->last_name;
			$student_email = $student_info->user_email;
			$student_contact_number = $student_info->mobile_no;
			$student_role = $student_info->user_role_display;
		}
	}
	if(isset($template_vars['review_data'])) {
		$review_data = $template_vars['review_data'];
		$review_text = $review_data['review_text'];
		$rating1 = $review_data['rating1'];
		$rating2 = $review_data['rating2'];
		$rating3 = $review_data['rating3'];
		$rating4 = $review_data['rating4'];
		$average_rating = $review_data['average_rating'];
		$overall_rating = $review_data['overall_rating'];
		$is_anonymous = $review_data['is_anonymous'];
	}
	
	$accept_link = add_query_arg( array(
		'id'=>$review_new_id,
		'action'=>'accept'
	), home_url('/reviews') );
	
	$reject_link = add_query_arg( array(
		'id'=>$review_new_id,
		'action'=>'reject'
	), home_url('/reviews') );
	
	$green_button = "
		background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 15px 32px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;";
	
	$red_button = "
		background-color: #f44336;
	    border: none;
	    color: white;
	    padding: 15px 32px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;
	";
?>

<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					<h3>Hello!</h3>
					<p>Some one just submitted a review form! Below are the details:</p>
					<h4>Teacher/Institute's Details</h4>
					<table width="100%" border="1">
						<tr>
							<td><strong>Name</strong></td>
							<td><?php echo $teacher_name; ?></td>
							<td><strong>Role</strong></td>
							<td><?php echo $teacher_role; ?></td>
						</tr>
						<tr>
							<td><strong>Email</strong></td>
							<td><?php echo $teacher_email; ?></td>
							<td><strong>Contact</strong></td>
							<td><?php echo $teacher_contact_number; ?></td>
						</tr>
					</table>
					<hr />
					<h4>Student/Guardian's Details</h4>
					<table width="100%" border="1">
						<tr>
							<td><strong>Name</strong></td>
							<td><?php echo $student_name; ?></td>
							<td><strong>Role</strong></td>
							<td><?php echo $student_role; ?></td>
						</tr>
						<tr>
							<td><strong>Email</strong></td>
							<td><?php echo $student_email; ?></td>
							<td><strong>Contact</strong></td>
							<td><?php echo $student_contact_number; ?></td>
						</tr>
					</table>
					<hr />
					<h4>Review Details</h4>
					<table width="100%" border="1">
						<tr>
							<td colspan="8" align="center"><strong>Review Text</strong></td>
						</tr>
						<tr>
							<td colspan="8"><?php echo $review_text; ?></td>
						</tr>
						<tr>
							<td><strong>Rating 1</strong></td>
							<td><?php echo $rating1; ?></td>
							<td><strong>Rating 2</strong></td>
							<td><?php echo $rating2; ?></td>
							<td><strong>Rating 3</strong></td>
							<td><?php echo $rating3; ?></td>
							<td><strong>Rating 4</strong></td>
							<td><?php echo $rating4; ?></td>
						</tr>
						<tr>
							<td><strong>Average Rating</strong></td>
							<td><?php echo $average_rating; ?></td>
							<td><strong>Overall Rating</strong></td>
							<td><?php echo $overall_rating; ?></td>
							<td></td>
							<td colspan="2"><strong>Is Anonymous</strong></td>							
							<td><?php echo $is_anonymous; ?></td>
						</tr>						
					</table>
					<hr />
					<table width="100%" border="0">
						<tr>
							<td width="50%">
								<a href="<?php echo $reject_link; ?>" style="<?php echo $red_button; ?>">Reject Review</a>
							</td>
							<td width="50%">
								<a href="<?php echo $accept_link; ?>" style="<?php echo $green_button; ?>">Accept Review</a>
							</td>
						</tr>
					</table>										
				</td>
            </tr>
        </table>
    </td>
</tr>

<!-- Content section ends -->