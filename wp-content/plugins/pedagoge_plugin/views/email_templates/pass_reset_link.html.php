<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					Hello <?php echo isset($template_vars['display_name'])?$template_vars['display_name']:'there'; ?>,<br><br>
					Someone has requested to reset the password of your account at Pedagoge.<br/><br/>
					If you have not requested the password reset then do not worry! Your password will remain the same and your account and personal information is safe.<br/><br/>
					If you did request password reset then click on link given below to initiate the password resetting process!<br><br>					
					<strong>Team Pedagoge</strong></strong><br>
				</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <!-- BULLETPROOF BUTTON -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
        	<center>    
            	<tr>
                    <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                        <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                            <tr>
                                <td align="center">
                                	<a href="<?php echo $template_vars['password_reset_url']; ?>" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #1DB198; border-top: 15px solid #1DB198; border-bottom: 15px solid #1DB198; border-left: 25px solid #1DB198; border-right: 25px solid #1DB198; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Click Here to reset your password!</a>
                                </td>
								
							</tr>
							<tr><br><br></tr>
							<tr>
								<td><br/><br/><br/>Or just go to this link:  <?php echo $template_vars['password_reset_url']; ?></td>
							</tr>
                        </table>
                    </td>
                </tr>
            </center>
        </table>
    </td>
</tr>

<!-- Content section ends -->