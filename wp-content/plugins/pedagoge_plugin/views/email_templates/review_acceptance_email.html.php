<?php
	$teacher_name='';
	$teacher_email = '';
	$teacher_contact_number = '';
	$teacher_role = '';
	
	$student_name = '';
	$student_email = '';
	$student_contact_number = '';
	$student_role = '';
	
	$review_text = '';
	$rating1 = '';
	$rating2 = '';
	$rating3 = '';
	$rating4 = '';
	$average_rating = '';
	
	$is_anonymous = '';
	
	$sendemailto = '';
	if(isset($template_vars['emailto'])) {
		$sendemailto = $template_vars['emailto'];
	}
	
	$rejection_text = isset($template_vars['rejection_text'])? $template_vars['rejection_text']: ''; 
	
	if(isset($template_vars['review_data'])) {
		foreach($template_vars['review_data'] as $review_info) {
			$teacher_name = $review_info->teacher_name;
			$teacher_email = $review_info->teacher_email;
			$teacher_contact_number = 'N/A';
			$teacher_role = $review_info->teacher_role;
			
			$student_contact_number = 'N/A';
			$student_role = $review_info->student_role;
			$student_name = $review_info->student_name;
			$student_email = $review_info->student_email;
			
			$review_text = $review_info->review_text;
			$rating1 = $review_info->rating1;
			$rating2 = $review_info->rating2;
			$rating3 = $review_info->rating3;
			$rating4 = $review_info->rating4;
			$average_rating = $review_info->overall_rating;
			$is_anonymous = $review_info->is_anonymous;
			if($is_anonymous=='yes' && $sendemailto=='teacher') {
				$student_name = 'Anonymous';
				$student_email = 'Anonymous';
			}
		}
	}
	
	$str_greetings_message = '';
	switch($sendemailto) {
		case 'admin':
			$str_greetings_message = "<p><strong>Review Approval confirmation. Reviews was approved successfully!</strong></p>";
			break;
		case 'teacher':
			$str_greetings_message = "<p><strong>You have received a review!</strong></p>";
			break;
		case 'student':
			$str_greetings_message = "<p><strong>Your review was approved!</strong></p>";
			break;
	}
	//pedagoge_applog($sendemailto);
?>

<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					<h3>Hello!</h3>
					
					<?php echo $str_greetings_message; ?>
					<p><strong>Please find the details below:</strong></p>
					<hr />									
					<h4>Teacher/Institute's Details</h4>
					<table width="100%" border="1">
						<tr>
							<td><strong>Name</strong></td>
							<td><?php echo $teacher_name; ?></td>
							<td><strong>Role</strong></td>
							<td><?php echo $teacher_role; ?></td>
						</tr>
						<tr>
							<td><strong>Email</strong></td>
							<td><?php echo $teacher_email; ?></td>
							<td><strong>Contact</strong></td>
							<td><?php echo $teacher_contact_number; ?></td>
						</tr>
					</table>
					<hr />
					<h4>Student/Guardian's Details</h4>
					<table width="100%" border="1">
						<tr>
							<td><strong>Name</strong></td>
							<td><?php echo $student_name; ?></td>
							<td><strong>Role</strong></td>
							<td><?php echo $student_role; ?></td>
						</tr>
						<tr>
							<td><strong>Email</strong></td>
							<td><?php echo $student_email; ?></td>
							<td><strong>Contact</strong></td>
							<td><?php echo $student_contact_number; ?></td>
						</tr>
					</table>
					<hr />					
					<h4>Review Details</h4>
					<table width="100%" border="1">
						<tr>
							<td colspan="8" align="center"><strong>Review Text</strong></td>
						</tr>
						<tr>
							<td colspan="8"><?php echo $review_text; ?></td>
						</tr>
						<tr>
							<td><strong>Rating 1</strong></td>
							<td><?php echo $rating1; ?></td>
							<td><strong>Rating 2</strong></td>
							<td><?php echo $rating2; ?></td>
							<td><strong>Rating 3</strong></td>
							<td><?php echo $rating3; ?></td>
							<td><strong>Rating 4</strong></td>
							<td><?php echo $rating4; ?></td>
						</tr>
						<tr>
							<td><strong>Average Rating</strong></td>
							<td><?php echo $average_rating; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td colspan="2"><strong>Is Anonymous</strong></td>							
							<td><?php echo $is_anonymous; ?></td>
						</tr>						
					</table>
					<hr />									
				</td>
            </tr>
        </table>
    </td>
</tr>

<!-- Content section ends -->