<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					Hello there,<br>
					
					You had requested Account Activation Link for your Pedagoge Account. Please click on the button below to activate your account.<br/>
					
					Team Pedagoge<br>
				</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <!-- BULLETPROOF BUTTON -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
        	<center>    
            	<tr>
                    <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                        <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                            <tr>
                                <td align="center">
                                	<a href="<?php echo $template_vars['account_activation_link']; ?>" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #1DB198; border-top: 15px solid #1DB198; border-bottom: 15px solid #1DB198; border-left: 25px solid #1DB198; border-right: 25px solid #1DB198; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Click Here to Activate your Account &rarr;</a>
                                </td>
								
							</tr>
							<tr><br><br></tr>
							<tr>
								<td><br/><br/><br/>Or just go to this link:  <?php echo $template_vars['account_activation_link']; ?></td>
							</tr>
                        </table>
                    </td>
                </tr>
            </center>
        </table>
    </td>
</tr>

<!-- Content section ends -->