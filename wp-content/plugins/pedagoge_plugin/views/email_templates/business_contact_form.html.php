<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					<h3>Hello!</h3>
					<p>Some one just dropped a business email!</p>
					<p><strong>Company Name</strong> : <?php echo $template_vars['contact_companyName']; ?></p>
					<p><strong>Name</strong> : <?php echo $template_vars['contact_name']; ?></p>
					<p><strong>Email Address</strong> : <?php echo $template_vars['contact_email']; ?> </p>
					<p><strong>Contact No</strong> : <?php echo $template_vars['contact_number']; ?> </p>
					<p><strong>Designation</strong> : <?php echo $template_vars['contact_designation']; ?> </p><br />
					<strong>Team Pedagoge</strong></strong><br>
				</td>
            </tr>
        </table>
    </td>
</tr>

<!-- Content section ends -->