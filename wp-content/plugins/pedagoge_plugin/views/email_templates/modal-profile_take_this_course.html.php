<!-- Content section starts -->
<tr>
    <td>
        <!-- COPY -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Welcome To Pedagoge!</td>
            </tr>
            <tr>
                <td  style="align: justify; padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">
					<h3>Hello!</h3>
					<p>Someone has requested a callback</p>
					<p><strong>Name</strong> : <?php echo $template_vars['name']; ?></p>
					<p><strong>Email</strong> : <?php echo $template_vars['email']; ?></p>
					<p><strong>Phone</strong> : <?php echo $template_vars['phone']; ?></p>
					<p><strong>Tuition Locations</strong> : <?php echo $template_vars['tuition_location']; ?>'s Place </p>
					<p><strong>Localities</strong> : <?php echo $template_vars['locality']; ?> </p>
					<p><strong>Subjects</strong> : <?php echo $template_vars['subject']; ?> </p><br />
					<p><strong>Fees</strong> : <?php echo $template_vars['fees']; ?> </p><br />
					<p><strong>Batch Size</strong> : <?php echo $template_vars['batchsize']; ?> </p><br />
					<p><strong>Length</strong> : <?php echo $template_vars['length']; ?> </p><br />
					<p><strong>Time</strong> : <?php echo $template_vars['time']; ?> </p><br />
					<strong>Team Pedagoge</strong></strong><br>
				</td>
            </tr>
        </table>
    </td>
</tr>

<!-- Content section ends -->