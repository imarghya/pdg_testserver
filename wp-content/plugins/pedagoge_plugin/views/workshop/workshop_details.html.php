<?php	
	$str_workshop_category_options = '';
	$str_city_options = '';
	
	if(isset($pdg_city)) {
		foreach($pdg_city as $city_info) {
			$str_city_options .= '
				<option value="'.$city_info->city_id.'">'.$city_info->city_name.'</option>
			';
		}
	}
	
	if(isset($pdg_workshop_categories)) {
		foreach($pdg_workshop_categories as $workshop_category_info) {
			$str_workshop_category_options .= '
				<option value="'.$workshop_category_info->workshop_category_id.'">'.$workshop_category_info->workshop_category.'</option>
			';
		}
	}
	 
?>
<form id="frm_workshop_info">	
	<div class="panel panel-warning">		
		<div class="panel-body">			
			<fieldset>
				<legend class="text-info">Workshop General Information</legend>
				<div class="row">
					<div class="col-md-12">
						<label for="txt_workshop_name" class="text-warning">Workshop Name (*)</label>
						<input type="text" class="form-control validate[required,minSize[5]]" id="txt_workshop_name" name="txt_workshop_name" maxlength="255"/>
						<br />			
					</div>				
					<div class="col-md-3">
						<fieldset>
							<legend class="workshop_fieldset">Start Date (DD-MM-YYYY)</legend>
							<div class="row">
								<div class="col-md-3">
									<input type="text" class="form-control positive" id="txt_workshop_start_date_dd" name="txt_workshop_start_date_dd" maxlength="2" placeholder="dd" />
								</div>
								<div class="col-md-3">
									<input type="text" class="form-control positive" id="txt_workshop_start_date_mm" name="txt_workshop_start_date_mm" maxlength="2" placeholder="mm" />
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control positive" id="txt_workshop_start_date_yyyy" name="txt_workshop_start_date_yyyy" maxlength="4" placeholder="yyyy" />
								</div>
								<div class="col-md-1"></div>
							</div>					 
						</fieldset>
					</div>
					<div class="col-md-3">
						<fieldset>
							<legend class="workshop_fieldset">End Date (DD-MM-YYYY)</legend>					
							<div class="row">
								<div class="col-md-3">
									<input type="text" class="form-control positive" id="txt_workshop_end_date_dd" name="txt_workshop_end_date_dd" maxlength="2" placeholder="dd" />
								</div>
								<div class="col-md-3">
									<input type="text" class="form-control positive" id="txt_workshop_end_date_mm" name="txt_workshop_end_date_mm" maxlength="2" placeholder="mm" />
								</div>
								<div class="col-md-5">
									<input type="text" class="form-control positive" id="txt_workshop_end_date_yyyy" name="txt_workshop_end_date_yyyy" maxlength="4" placeholder="yyyy" />
								</div>
								<div class="col-md-1"></div>
							</div>
						</fieldset>
					</div>
					<div class="col-md-6">
						<label for="select_workshop_category" class="text-warning">Workshop Category (*)</label>
						<select id="select_workshop_category" name="select_workshop_category" class="form-control select_control_with_title validate[required]" title="Select Workshop Category">
							<option></option>
							<?= $str_workshop_category_options; ?>
						</select>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-6">
						<label for="txt_about_workshop" class="text-warning">About Workshop (*)</label>
						<textarea class="form-control validate[required,minSize[5]]" id="txt_about_workshop" name="txt_about_workshop" row="4"></textarea>
					</div>				
					<div class="col-md-6">					
						<label for="txt_workshop_prerequisits">Instructions for Workshop Attendees</label>
						<textarea class="form-control" id="txt_workshop_prerequisits" name="txt_workshop_prerequisits" row="4"></textarea>
					</div>
					
							
				</div>
			</fieldset>
			
			<fieldset>
				<legend class="text-info">Workshop Venue Information</legend>
				<div class="row">				
						<div class="col-md-4">
							<label for="txt_workshop_address" class="text-warning">Workshop Address (*)</label>
							<textarea class="form-control validate[required,minSize[5]]" id="txt_workshop_address" name="txt_workshop_address" rows="4"></textarea>
						</div>
						
						<div class="col-md-4">
							<label for="select_workshop_city" class="text-warning">City (*)</label>
							<select id="select_workshop_city" name="select_workshop_city" class="form-control select_control_with_title validate[required]" title="Select City" data-allow-clear = "true">
								<option></option>
								<?= $str_city_options; ?>						
							</select>
							<div><br /></div>
							<label for="txt_workshop_location_pincode" class="text-warning">Pincode (*)</label>
							<input type="text" name="txt_workshop_location_pincode" class="form-control positive validate[required, number, minSize[6], maxSize[6]]" id="txt_workshop_location_pincode" maxlength="6" />
						</div>
						
						<div class="col-md-4">
							<label for="select_workshop_locality">Locality</label>
							<select id="select_workshop_locality" name="select_workshop_locality" class="form-control select_control_with_title" title="Select Locality" data-allow-clear = "true">
								<option></option>
							</select>
							<div><br /></div>
							<label for="txt_workshop_landmark">Landmark</label>
							<input type="text" name="txt_workshop_landmark" class="form-control" id="txt_workshop_landmark"/>
						</div>
					</div>			   
			</fieldset>
			<fieldset>
				<legend class="text-info">Workshop Seats Availability / Pricing Information</legend>
				<div class="row">
						<div class="col-md-3">
							<label for="txt_workshop_seats" class="text-warning">Available Seats (*)</label>
							<input type="text" class="form-control positive-integer validate[required]" id="txt_workshop_seats" name="txt_workshop_seats" maxlength="4" />
						</div>
						<div class="col-md-3">
							<label for="select_workshop_type" class="text-warning">Workshop Type (*)</label>
							<select id="select_workshop_type" name="select_workshop_type" class="form-control select_control_with_title validate[required]" title="Select Workshop Type" data-allow-clear = "true">
								<option></option>
								<option value="free">Free</option>
								<option value="paid">Paid</option>
							</select>
						</div>
						<div class="col-md-3 span_paid_workshop">
							<label for="txt_workshop_price">Workshop Price</label>
							<input type="text" class="form-control positive" id="txt_workshop_price" name="txt_workshop_price" maxlength="10"/>
						</div>
						<div class="col-md-3 span_paid_workshop">
							<label>Payment Methods</label>
							<div>
								<label><input type="checkbox" id="chk_workshop_pay_online" name="chk_workshop_pay_online" /> Pay Online</label> &nbsp;&nbsp;&nbsp;&nbsp;
								<label><input type="checkbox" id="chk_workshop_pay_at_venue" name="chk_workshop_pay_at_venue" /> Pay at Venue</label>
							</div>
						</div>
					</div>
			</fieldset>
			<hr style="margin: 5px 0 5px 0;" />	
			<div class="row">
				<!-- <div class="col-md-6" id="div_workshop_result_area">
				</div> -->
				<div class="col-md-12">
					<div class="row div_profile_save_button_section">
						<div class="col-md-12">
							<label for="txt_workshop_tags">#tags</label><input type="text" class="form-control" id="txt_workshop_tags" name="txt_workshop_tags" maxlength="255"/>
							<br />
						</div>
						<div class="col-md-6 text-right">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="chk_workshop_request_featured" name="chk_workshop_request_featured"> Request to be featured
								</label>
							</div>
						</div>
						
						<div class="col-md-6">
							<input type="hidden" id="hidden_workshop_id" name="hidden_workshop_id"/>
							<buttton class="btn btn-success col-md-12" id="cmd_update_workshop_details" data-loading-text="Saving Workshop"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update Workshop Details</buttton>
						</div>		
					</div>
				</div>
			</div>
			<hr style="margin: 5px 0 5px 0;" />
			<div class="row div_workshop_images_area">
				<div class="col-md-4">
					<fieldset>
						<legend class="text-info workshop_fieldset_legend">Workshop Display Image <span class="text-warning"> 160px(h) x 250px(w)</span></legend>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<button class="btn btn-success col-md-12" id="cmd_upload_workshop_display_image" data-loading-text="Uploading...">
									<i class="fa fa-upload" aria-hidden="true"></i> <span id="span_select_image">Select and Upload</span>
								</button>
							</div>
						</div>
						<div style="width:100%;">
							<div class="div_workshop_profile_image center-block">
								<img src="<?= PEDAGOGE_ASSETS_URL ?>/images/250pxw160pxh.png" id="img_workshop_display_pic" />
							</div>  
						</div>						
						<input id="fileinput_workshop_display_image" type="file" style="display:none;" accept="image/*" />
						<input type="hidden" value="<?= PEDAGOGE_ASSETS_URL ?>/images/250pxw160pxh.png" id="hidden_blank_profile_url"/>
					</fieldset>
				</div>
				<div class="col-md-8">
					<fieldset>
						<legend class="text-info">Workshop Gallery Images <span class="text-warning"> Preferred Image Dimentions - 400px(h) x 1024px(w).</span></legend>
						<div class="row">
							<div class="col-md-4">
								<button class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="cmd_upload_workshop_image" data-loading-text="Uploading...">
									<i class="fa fa-upload" aria-hidden="true"></i> <span id="span_select_image">Select and Upload Images</span>
								</button>
							</div>
							<div class="col-md-8" id="div_image_result_area"></div>				
							<input id="fileinput_workshop_image" type="file" style="display:none;" accept="image/*" />
						</div>
						<hr />
						<div class="well row" id="div_workshop_images_list" style="margin-left: 4px; margin-right: 4px;"></div>
					</fieldset>
				</div>
			</div>
			
			
		</div>
	</div>
</form>
