<?php
$hidden_user_id = 0;
$hidden_user_email = '';

if(isset($pdg_current_user)) {
	$hidden_user_id = $pdg_current_user->ID;
	$hidden_user_email = $pdg_current_user->user_email;	
	$hidden_user_email = wp_hash_password($hidden_user_email);
} 

$localities_array = array();

if(isset($found_localities) && !empty($found_localities)) {
	foreach($found_localities as $found_locality) {
		$locality_name = $found_locality->locality;
		$locality_id = $found_locality->locality_id;
		$city_id = $found_locality->city_id;
		
		$str_localities_options = '<option value="'.$locality_id.'" data-city_id="'.$city_id.'">'.$locality_name.'</option>';
		
		if(!array_key_exists($city_id, $localities_array)) {
			$localities_array[$city_id] = $str_localities_options;
		} else {
			$localities_array[$city_id] .= $str_localities_options;
		}
	}
}

?>
<main>	
	<section class="site-section site-section-top">
		
		<div class="panel panel-danger">
			
			<div class="panel-body">
				<div class="row">
					<div class="col-md-9"></div>
					<div class="col-md-3">
						<button class="btn btn-info col-md-12 cmd_add_workshop"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Create Workshop</button>
					</div>
				</div>
				<ul class="nav nav-tabs" role="tablist">				
					<li role="presentation" class="active">
						<a href="#tab_personal_info" class="" aria-controls="tab_personal_info" role="tab" data-toggle="tab">
							<i class="fa fa-info-circle" aria-hidden="true"></i> Trainer Info
						</a>
					</li>
					<li role="presentation" id="li_workshop_details">
						<a href="#tab_workshop_details" class="" aria-controls="tab_workshop_details" role="tab" data-toggle="tab">
							<i class="fa fa-crosshairs" aria-hidden="true"></i> Workshop Details
						</a>
					</li>
					<li role="presentation">
						<a href="#tab_workshop_list" class="" aria-controls="tab_workshop_list" role="tab" data-toggle="tab">
							<i class="fa fa-list-alt" aria-hidden="true"></i> Workshop List
						</a>
					</li>					
					<li role="presentation" class="pull-right">
						<div class="text-danger"><br />Note - Please keep the Trainer Profile information updated!</div>
					</li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">				
					<div class="tab-pane active" id="tab_personal_info">
						<br />
						<?php require_once('personal_info_tab.html.php'); ?>
					</div>
					<div class="tab-pane" id="tab_workshop_list">
						<?php require_once('workshop_list.html.php'); ?>
					</div>
					<div class="tab-pane" id="tab_workshop_details">
						<?php require_once('workshop_details.html.php'); ?>
					</div> 
					<input type="hidden" value="<?php echo $hidden_user_id; ?>" id="hidden_dynamic_user_var"/>
					<input type="hidden" value="<?php echo $hidden_user_email; ?>" id="hidden_dynamic_profile_edit_secret_key"/>
				</div>
				
			</div>
			
		</div>
	</section>
</main>

<!-- Localities in javascript -->
<script type="text/javascript" charset="utf-8">
	var $localities_data = <?php echo json_encode($localities_array); ?>;
</script>