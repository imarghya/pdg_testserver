<hr />
<div class="row">
	<div class="col-md-9"></div>
	<div class="col-md-3">
		<button class="btn btn-warning col-md-12" id="cmd_reload_workshop_list" data-loading-text="Loading..."><i class="fa fa-refresh" aria-hidden="true"></i> Reload</button>
	</div>
	<div class="col-md-12" id="div_workshop_list_result_area"></div>
</div>
<div id="div_workshops_list">
	<?php echo $workshop_list_html; ?>
</div>
