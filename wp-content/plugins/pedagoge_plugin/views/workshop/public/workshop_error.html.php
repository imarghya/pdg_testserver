<?php
$str_workshop_error = $workshop_data['message'];
?>
<main>
	<!-- scroll to top -->
	<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
	<!--/scroll to top -->
	<div class="hidden-xs hidden-sm" id="increase_header_height_md">
	</div>
	<div class="visible-xs visible-sm" id="increase_header_height_sm">
	</div>
	
	<section class="container-fluid ">
		<div class="row workshop_header_image">
			<h2 class="text-center white_text shadow_text">WORKSHOPS AT PEDAGOGE</h2>
			<div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12">							
				<a href="<?= home_url('/manageworkshop'); ?>" class="btn btn-raised btn-success noroundcorner col-md-12 col-xs-12 col-sm-12">Register your workshop</a>
			</div>
		</div>		
	</section>	
	<section class="container-fluid">
		<div class="row well">
			<div class="col-md-12 bottom_border alert alert-danger">				
				<h4 class="text-center"><?= $workshop_data['message']; ?></h4>								
			</div>
			
			<div class="col-md-4 col-md-offset-4">
				<div class="alert alert-info">
					<h4 class="text-center"><a href="<?php echo home_url('/workshop'); ?>">Show Workshops!</a></h4>
				</div>
			</div>
		</div>
	</section>
	<br />
	<br />
</main>