<?php

$str_workshop_categories_options = '';
if(isset($pdg_workshop_categories) && !empty($pdg_workshop_categories)) {
	foreach($pdg_workshop_categories as $workshop_category) {
		$str_workshop_categories_options .= '
		<option value="'.$workshop_category->workshop_category_id.'">'.$workshop_category->workshop_category.'</option>
		';
	}
}

$str_city_options = '';
if(isset($pdg_city) && !empty($pdg_city)) {
	foreach($pdg_city as $city_info) {
		$str_city_options .='
		<option value="'.$city_info->city_id.'">'.$city_info->city_name.'</option>
		';
	}
}

$localities_array = array();

if(isset($pdg_locality) && !empty($pdg_locality)) {
	foreach($pdg_locality as $locality_info) {
		$locality_name = $locality_info->locality;
		$locality_id = $locality_info->locality_id;
		$city_id = $locality_info->city_id;
		$str_localities_options = '<option value="'.$locality_id.'" data-city_id="'.$city_id.'">'.$locality_name.'</option>';
		
		if(!array_key_exists($city_id, $localities_array)) {
			$localities_array[$city_id] = $str_localities_options;
		} else {
			$localities_array[$city_id] .= $str_localities_options;
		}
	}
}

$str_featured_workshops = '';
if(isset($featured_workshops) && !empty($featured_workshops)) {
	$str_featured_workshops = $featured_workshops;
}

$str_workshops_list = '
<div class="bs-component">
	<div class="alert alert-dismissible alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<h3>Oops! Nobody has registered any workshop yet!</h3>
		<p>Don\'t worry! Visit this page again! We will populte this page with more data.</p>
	</div>       
</div>
';
if(isset($workshop_list_html) && !empty($workshop_list_html)) {
	$str_workshops_list = $workshop_list_html;
}
$workshop_list_page_no = isset($workshop_list_page_no) ? $workshop_list_page_no : 0;


$new_featured_workshops = isset($new_featured_list) ? $new_featured_list : '';

?>

<main class="public_workshop">
	<!-- scroll to top -->
	<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
	<!--/scroll to top -->
	<div class="hidden-xs hidden-sm" id="increase_header_height_md">
	</div>
	<div class="visible-xs visible-sm" id="increase_header_height_sm">
	</div>
	
	<section class="container-fluid ">
		<div class="row workshop_header_image">
			<h2 class="text-center white_text shadow_text">WORKSHOPS AT PEDAGOGE</h2>
			<div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12">							
				<a href="<?= home_url('/manageworkshop'); ?>" class="btn btn-raised btn-info noroundcorner col-md-12 col-xs-12 col-sm-12">Register your workshop</a>
			</div>
		</div>		
	</section>
	
	
	<section class="container-fluid">
		<div class="" style="margin-bottom: 0; padding: 1em;">		
			<div class="bxslider row">
			<?php
				foreach($new_featured_workshops as $featured_item) {
					echo '<div>'.$featured_item.'</div>';
				}
			?>
			</div>
		</div>
	</section>
	
	
	<section class="container-fluid">
		<div class="row workshop_search_bar well" style="margin-bottom: 0;">
			<div class="col-md-12 col-xs-12 col-sm-12 text-center">
				<h5 class="bottom_border">SEARCH FOR WORKSHOPS</h5>
			</div>
			<div class="col-md-2 col-xs-12 col-sm-12">
				<label for="select_fltr_workshop_status" class="black_text">Status</label>
				<select id="select_fltr_workshop_status" class=" col-md-12 col-xs-12 col-sm-12 workshop_filter" title="Filter by Status">
					<option value="active">Active</option>
					<option value="inactive">Inactive</option>
					<option value="all">All</option>
				</select>
			</div>
			<div class="col-md-4 col-xs-12 col-sm-12">
				<label for="select_fltr_workshop_category" class="black_text">Category</label>
				<select id="select_fltr_workshop_category" class="col-md-12 col-xs-12 col-sm-12 select_control_with_title workshop_filter" title="Filter by category">
					<option value=""></option>
					<?= $str_workshop_categories_options; ?>
				</select>
			</div>
			<div class="col-md-2 col-xs-12 col-sm-12">
				<label for="select_fltr_workshop_type" class="black_text">Type</label>
				<select id="select_fltr_workshop_type" class="col-md-12 col-xs-12 col-sm-12 select_control_with_title workshop_filter" title="Filter by type">
					<option></option>
					<option value="paid">Paid</option>
					<option value="free">Free</option>
				</select>
			</div>
			<div class="col-md-2 col-xs-12 col-sm-12">
				<label for="select_fltr_workshop_city" class="black_text">City</label>
				<select id="select_fltr_workshop_city" class="col-md-12 col-xs-12 col-sm-12 select_control_with_title workshop_filter" title="Filter by city">
					<option></option>
					<?= $str_city_options; ?>
				</select>
			</div>
			<div class="col-md-2 col-xs-12 col-sm-12">
				<label for="select_fltr_workshop_locality" class="black_text">Locality</label>
				<select id="select_fltr_workshop_locality" class="col-md-12 col-xs-12 col-sm-12 select_control_with_title workshop_filter" title="Filter by locality">
					<option></option>					
				</select>
			</div>
			<!-- <div class="col-md-2">
				<br />
				<button type="button" class="btn btn-raised btn-danger btn-sm col-md-12 col-sm-12 col-xs-12" id="cmd_reset_filter">Reset</button>
			</div> -->
		</div>
		<!-- <hr class="bottom_border"/> --> <br />
		<h3>Popuplar Workshops</h3>
		<br />
		<div class="row" id="div_workshops_list">
			<section class="wrapper">
				<div class="content">
					<div class="container">
						<div class="row">
							<?= $str_workshops_list; ?>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<button type="button" class="btn btn-raised workshop_darkblue btn-sm col-md-12 col-sm-12 col-xs-12" id="btnWorkshopPagination" data-loading-text="Loading...">See more...</button>
				<input type="hidden" name="hiddenWorkshopPageNo" value="<?= $workshop_list_page_no; ?>" id="hiddenWorkshopPageNo"/>	
			</div>
			
		</div>
	</section>

	<?php include_once('book_now_modal.html.php'); ?>
</main>
<!-- Localities in javascript -->
<script type="text/javascript" charset="utf-8">
	var $localities_data = <?php echo json_encode($localities_array); ?>;
</script>