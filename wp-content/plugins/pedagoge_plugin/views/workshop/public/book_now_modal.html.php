<div class="modal fade" id="modal_workshop_booking" tabindex="-1" role="dialog" aria-labelledby="WorkshopBookingLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="WorkshopBookingLabel">Book your seats now!</h4>
				<hr />
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="frm_workshop_ticket_booking">
					<div class="form-group workshop_modal_first_field">
				    	<label for="txt_participant_full_name" class="col-sm-2 control-label">Full Name</label>
				    	<div class="col-sm-10">
				      		<input type="text" class="form-control validate[required,minSize[5]]" name="txt_participant_full_name" id="txt_participant_full_name" placeholder="Your Full Name" maxlength="100">
				    	</div>
				  	</div>
				  
				  	<div class="form-group">
				    	<label for="txt_participant_email_address" class="col-sm-2 control-label">Email</label>
				    	<div class="col-sm-10">
				      		<input type="email" class="form-control validate[required, custom[email]]" name="txt_participant_email_address" id="txt_participant_email_address" placeholder="Your Email" maxlength="100">
				    	</div>
				  	</div>
				  	
				  	<div class="form-group">
				    	<label for="txt_participant_mobile_no" class="col-sm-2 control-label">Mobile No</label>
				    	<div class="col-sm-10">
				      		<input type="text" class="form-control positive validate[required,minSize[10]]" name="txt_participant_mobile_no" id="txt_participant_mobile_no" placeholder="Your 10 digit mobile no" maxlength="10" max="9999999999">
				    	</div>
				  	</div>
				  	
				  	<div class="form-group">
				    	<label for="txt_participant_seat_no" class="col-sm-2 control-label">Total Seats</label>
				    	<div class="col-sm-4">
				      		<input type="number" class="form-control positive validate[required]" name="txt_participant_total_seats" id="txt_participant_total_seats" placeholder="Seats" maxlength="3" max="999">
				    	</div>
				    	<div class="col-sm-6">
				      		<button class="btn btn-raised white-text btn-info style-fix col-sm-12" data-loading="Booking seats..." id="cmd_modal_reserve_workshop_seats">
								<i class="fa fa-floppy-o" aria-hidden="true"></i>
								Reserve
							</button>
							<input type="hidden" name="modal_hidden_workshop_id" value="" id="modal_hidden_workshop_id"/>
				    	</div>
					</div>
					<div class="alert alert-warning">You will receive Email confirmation about your seats booking! Till then your reservation will be on hold! </div>
					<div class="alert alert-warning">For cancellation drop a mail at <strong>workshop@pedagoge.com</strong> or call at <strong>(+91) 8033172719</strong> <br />
						We should get cancellation request atleast 72hrs prior to workshop start date.
					</div>
				</form>
				
			</div>
			
		</div>
	</div>
</div>
