<?php
	$str_qualifications = isset($trainer_info['data']['qualifications']) ? $trainer_info['data']['qualifications'] : '';
	
	$str_about_trainer = '';
	$str_trainer_experience = 'N/A';
	$str_trainer_name = 'N/A';
	
	if(isset($trainer_info['data']['trainer_data'])) {
		foreach($trainer_info['data']['trainer_data'] as $trainer_data) {
			$str_about_trainer = $trainer_data->about_trainer;
			$str_trainer_experience = $trainer_data->teaching_xp;
			$str_trainer_name = $trainer_data->trainer_name;
		}
	}
	
	$hosted_by = '';
	if(isset($profile_data) && !empty($profile_data)) {
		$hosted_by = '
			Hosted by : <a href="'.$profile_data['profile_link'].'">'.$profile_data['profile_name'].'</a>
		';
	}

?>
<?php if($is_approved == 'yes'): ?>
	<h3 class="text-center text-danger">
		<div class="sharethis-inline-share-buttons"></div>	
	</h3>
<?php endif; ?>

<hr style="margin : 2px;" />
<h4 class="text-center">Need Help?</h4>
<p class="text-center">(+91) 8033172719</p>
<p class="text-center"><a href="mailto:workshop@pedagoge.com" class="black_text">workshop@pedagoge.com</a></p>
<hr style="margin : 2px;" />
<h4 class="text-center">Trainer Information</h4>
<div class="text-center"><strong><?= $str_trainer_name; ?></strong></div>
<p class="text-center"> 
	<i><?= $str_qualifications; ?></i>
</p>
<p class="text-center"><strong>Experience :</strong> <?= $str_trainer_experience; ?></p>
<p class="text-center">
	<?= nl2br($str_about_trainer); ?>
</p>
<hr style="margin : 2px;" />
<p class="text-center">
	<?= $hosted_by; ?>
</p>