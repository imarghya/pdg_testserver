<?php

$workshop_name = 'Workshop Name is not available!';
$workshop_id = '';
$user_id = '';
$start_date = 'NA';
$end_date = 'NA';
$workshop_address = 'NA';
$workshop_location_landmark = 'NA';
$workshop_pincode = 'NA';
$about_workshop = 'NA';
$workshop_instructions = 'NA';
$workshop_type = 'NA';
$workshop_seats = 'NA';
$workshop_price = 'NA';
$pay_online = 'NA';
$pay_at_venue = 'NA';
$tags = 'NA';
$is_approved = 'NA';
$is_active = 'NA';
$is_featured = 'NA';
$created_at = '';
$workshop_category = 'NA';
$city_name = 'NA';
$locality = 'NA';
$seats_available = isset($workshop_seats_available) ? $workshop_seats_available : 0;
if(isset($workshop_data['data']['workshop_data'])) {
	foreach($workshop_data['data']['workshop_data'] as $workshop_info) {
		
		$workshop_name = $workshop_info->workshop_name;
		$workshop_id = $workshop_info->workshop_id;
		$user_id = $workshop_info->user_id;
		$start_date = $workshop_info->start_date;
		$end_date = $workshop_info->end_date;
		$workshop_address = $workshop_info->workshop_address;
		$workshop_location_landmark = $workshop_info->workshop_location_landmark;
		$workshop_pincode = $workshop_info->workshop_pincode;
		$about_workshop = $workshop_info->about_workshop;
		$workshop_instructions = $workshop_info->workshop_instructions;
		$workshop_type = $workshop_info->workshop_type;
		$workshop_seats = $workshop_info->workshop_seats;
		$workshop_price = $workshop_info->workshop_price;
		$pay_online = $workshop_info->pay_online;
		$pay_at_venue = $workshop_info->pay_online;
		$tags = $workshop_info->tags;
		$is_approved = $workshop_info->is_approved;
		$is_active = $workshop_info->is_active;
		$is_featured = $workshop_info->is_featured;
		$created_at = $workshop_info->created_at;
		$workshop_category = $workshop_info->workshop_category;
		$city_name = $workshop_info->city_name;
		$locality = $workshop_info->locality;
	}
}

$slider_images = '';
if(isset($workshop_data['data']['slider'])) {
	$slider_images = $workshop_data['data']['slider'];
}

$str_workshop_active_inactive = '<div class="col-md-2 text-center label-primary"><i class="fa fa-times" aria-hidden="true"></i><h5>Inactive</h5></div>';
$booking_button_disabled = ' disabled';
if($is_active == 'yes' && $is_approved =='yes') {
	$str_workshop_active_inactive = '<div class="col-md-2 text-center label-primary"><h5><i class="fa fa-check" aria-hidden="true"></i>Active</h5></div>';
	$booking_button_disabled = '';
}

$str_seats_available = '<div class="col-md-3 text-center label-warning"><h5>0 Seats Available</h5></div>';
if($seats_available > 0) {
	$str_seats_available = '<div class="col-md-3 text-center label-success"><h5>'.$seats_available.' Seats available</h5></div>';
} else {
	$booking_button_disabled = ' disabled';
}

if(!empty($start_date)) {
	$date = date_create($start_date);
	$start_date = date_format($date,"D, d M Y");
}
if(!empty($end_date)) {
	$date = date_create($end_date);
	$end_date = date_format($date,"D, d M Y");
}

$str_locality = empty($locality) ? '' : $locality.', ';
$str_pincode = empty($workshop_pincode) ? '' : ' - '.$workshop_pincode;

$str_workshop_location_info = $str_locality.$city_name.$str_pincode; 

$str_tags = '';
if(!empty($tags)) {
	$str_tags = '<h4 class="bottom_border">Tags</h4>'.$tags;
}

$str_workshop_price = '';
if($workshop_type == 'free') {
	$str_workshop_price = '<i class="class="fa fa-smile-o""></i><strong>Price: </strong> <span class="text-success">Free</span>';
} else if($workshop_type == 'paid') {
	$str_workshop_price = '<i class="fa fa-inr"></i><strong>Price: </strong> Rs. '.$workshop_price.'/- per Seat';
}
?>
<main class="public_workshop">
	<section class="container-fluid workshop_light_grey">
		<div class="row">
			<div class="col-md-12 black_text">
				<?= $slider_images; ?>
			</div>
		</div>
		<div class="row">
			<i></i>
			<?= $str_workshop_active_inactive; ?>
			<div class="col-md-5 text-center label-success"><h5><i class="fa fa-tag" aria-hidden="true"></i><?= $workshop_category; ?></h5></div>
			<div class="col-md-2 text-center label-primary"><h5>
				
			<?php echo ucfirst($workshop_type); ?></h5></div>
			<?= $str_seats_available; ?>
		</div>
	</section>
	<section class="container-fluid margin-bottom-20">		
		<div class="row">
			<div class="col-md-9">
				<div class="text-center">
					<h2><?= $workshop_name; ?></h2>
					<div class="top">
                        <div><strong><i class="fa fa-calendar-o" aria-hidden="true"></i> From</strong> <?= $start_date; ?> | <strong>To</strong> <?= $start_date; ?></div>
						<p>
							<strong><i class="fa fa-map-marker" aria-hidden="true"></i></strong>
								<?= $str_workshop_location_info; ?><br />
								<?php echo empty($workshop_location_landmark) ? '' : $workshop_location_landmark; ?>
						</p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<h4 class="bottom_border">About Workshop</h4>
						<?php echo stripslashes(nl2br($about_workshop)); ?>
						
						<h4 class="bottom_border">Instructions</h4>
						<?php echo stripslashes(nl2br($workshop_instructions)); ?>
						
						<h4 class="bottom_border">Pricing, Seats Availability and Booking</h4>
						<div class="row">
							<div class="col-md-6 text-center"><br /><strong>Total Seats: </strong> <?= $workshop_seats; ?> Seats | <strong>Available Seats: </strong> <?= $seats_available; ?> Seats</div>
							<div class="col-md-3  text-center"><br /><?= $str_workshop_price; ?></div>
							<div class="col-md-3">
								<button type="button" data-workshop_id="<?= $workshop_id; ?>" class="btn btn-raised btn-sm btn-success btn-md col-md-12 cmd_book_workshop" <?= $booking_button_disabled; ?> >Reserve</button>
							</div>
						</div>
						<?= $str_tags; ?>
					</div>
				</div>				
			</div>
			<div class="col-md-3 label-primary white_text" id="div_workshop_sidebar">
				<?php include_once('workshop_sidebar.html.php'); ?>
				<input type="hidden" value="<?= $workshop_id; ?>" id="hidden_workshop_id"/>
			</div>
		</div>
	</section>
</main>
<?php
	$str_workshop_footer_bar = '';
	$str_workshop_approval_notice = '<h5 class="text-center">Approval is pending for this workshop! Workshop will be published only after approval!</h5>';
	$approved_checked = '';
	if($is_approved == 'yes') {
		$str_workshop_approval_notice = '';
		$approved_checked = ' checked';
	}
	$featured_checked = '';
	if($is_featured == 'yes') {
		$featured_checked = ' checked';
	}
	
	$active_checked = '';
	if($is_active == 'yes') {
		$active_checked = ' checked';	
	}
	if(current_user_can('manage_options') || current_user_can('pdg_cap_workshop_edit')) {
		$workshop_edit_link = home_url('/manageworkshop/');
		$workshop_edit_link = $workshop_edit_link."?edit&user_id=$user_id&workshop_id=$workshop_id";
		$str_workshop_footer_bar = '
			<div class="footer navbar-fixed-bottom workshop_coral text-center div_workshop_footer" style="margin-bottom: 0;">
				'.$str_workshop_approval_notice.' <hr />
				<div class="row">
					<div class="col-md-4 div_workshop_approve_result"></div>
					<div class="col-md-2">
						<a href="'.$workshop_edit_link.'" target="_blank" class="btn btn-raised btn-success btn-sm col-xs-12 col-md-12">Edit</a>
					</div>
					<div class="col-md-6">
						
						<div class=" row">
							<div class="col-md-4 togglebutton"><label class="white_text"><input type="checkbox" class="chk_workshop_admin" data-type="approve" '.$approved_checked.'>Approved</label></div>
							<div class="col-md-4 togglebutton"><label class="white_text"><input type="checkbox" class="chk_workshop_admin" data-type="featured" '.$featured_checked.'> Featured</label></div>
							<div class="col-md-4 togglebutton"><label class="white_text"><input type="checkbox" class="chk_workshop_admin" data-type="active" '.$active_checked.'> Active</label></div>
							
						</div>
					</div>
					
				</div>
			</div>
		';
	} else {
		if($is_approved != 'yes') {
			$str_workshop_footer_bar = '
				<div class="footer navbar-fixed-bottom alert alert-warning text-center" style="margin-bottom: 0;">
					'.$str_workshop_approval_notice.'
				</div>
			';
		}
	}
	echo $str_workshop_footer_bar;
?>

<?php if($is_approved == 'yes'): 
	include_once('book_now_modal.html.php'); ?>	
	<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5901c3e3e056200014ee5928&product=inline-share-buttons' async='async'></script>
<?php endif; ?>