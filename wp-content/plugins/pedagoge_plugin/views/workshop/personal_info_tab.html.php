<?php

$trainer_name = '';
$trainer_contact_no = '';
$trainer_email = '';
$trainer_experience = '';
$about_trainer = '';
$present_place_of_work = '';
$past_place_of_work = '';
$str_gender = '';

if(isset($trainer_information)) {
	foreach($trainer_information as $trainer_info) {
		$trainer_name = $trainer_info->trainer_name;
		$trainer_contact_no = $trainer_info->trainer_contact_no;
		$trainer_email = $trainer_info->trainer_email;
		$trainer_experience = $trainer_info->trainer_experience;
		$about_trainer = $trainer_info->about_trainer;
		$present_place_of_work = $trainer_info->present_place_of_work;
		$past_place_of_work = $trainer_info->past_place_of_work;
		$str_gender = $trainer_info->gender;
	}
}

// trainer qualifications
$trainer_qualifications_array = array();
if(isset($trainer_qualification)) {
	foreach($trainer_qualification as $qulification) {
		$trainer_qualifications_array[] = $qulification->qualification_id;
	}
}

$str_grad_qualification_options = '';
$str_post_grad_qualification_options = '';
$str_professional_courses = '';


if(isset($qualification_data)) {
	foreach($qualification_data as $qualification) {
		$qualification_name= $qualification->qualification;			
		$qualification_id = $qualification->qualification_id;			
		$qualification_type = $qualification->qualification_type;			
		$str_selected = '';
		switch($qualification_type) {
			case 'bachelor':
				if(in_array($qualification_id, $trainer_qualifications_array)){
					$str_selected = 'selected';
				}
				$str_grad_qualification_options .= '<option value="'.$qualification_id.'" '.$str_selected.'>'.$qualification_name.'</option>';
				break;
			case 'postgrad':				
				if(in_array($qualification_id, $trainer_qualifications_array)){
					$str_selected = 'selected';
				}
				$str_post_grad_qualification_options .= '<option value="'.$qualification_id.'" '.$str_selected.'>'.$qualification_name.'</option>';
				break;
			case 'professional_course':
				if(in_array($qualification_id, $trainer_qualifications_array)){
					$str_selected = 'selected';
				}
				$str_professional_courses .= '<option value="'.$qualification_id.'" '.$str_selected.'>'.$qualification_name.'</option>';
				break;
		}		
	}
}

//avg_teaching_xp_id	
$str_teaching_xp_options = '';
if(isset($teaching_xp_data)) {
	foreach($teaching_xp_data as $teaching_xp) {
		$teaching_xp_id = $teaching_xp->teaching_xp_id;
		$str_selected = '';
		if($teaching_xp_id == $trainer_experience) {
			$str_selected = 'selected';
		}
		$str_teaching_xp_options .= '<option value="'.$teaching_xp_id.'" '.$str_selected.'>'.$teaching_xp->teaching_xp.'</option>';
	}
}

$str_male = '';
$str_female = '';
switch($str_gender) {
	case 'male':
		$str_male = 'selected';
		break;
	case 'female':
		$str_female = 'selected';
		break;	
}


?>
<form id="frm_workshop_trainer_info">
	<div class="well">
		
		<div class="row">
			<div class="col-md-2">
				<label for="txt_trainer_name">Trainer Name (*)</label>
			</div>
			<div class="col-md-4">
				<input type="text" class="form-control validate[required,minSize[5]]" id="txt_trainer_name" name="txt_trainer_name" value="<?php echo $trainer_name; ?>" />		
			</div>
			<div class="col-md-2">
				<label for="txt_trainer_contact_no">Mobile No (*)</label>
			</div>		
			<div class="col-md-4">
				<input type="text" class="form-control positive validate[required,minSize[10]]" id="txt_trainer_contact_no" name="txt_trainer_contact_no" maxlength="10" value="<?php echo $trainer_contact_no; ?>" />		
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<label for="txt_trainer_email_address">Email (*)</label>
			</div>
			<div class="col-md-4">
				<input type="text" class="form-control validate[required]" id="txt_trainer_email_address" name="txt_trainer_email_address" value="<?php echo $trainer_email; ?>"/>
			</div>
			<div class="col-md-2">
				<label for="select_average_teaching_experience">Workshop Experience (*)</label>
			</div>
			<div class="col-md-4">
				<select id="select_average_teaching_experience" name="select_average_teaching_experience" class="select_control_with_title form-control validate[required]" title="--Select Workshop Experience--">
					<option></option>
					<?php echo $str_teaching_xp_options; ?>
				</select>		
			</div>
		</div>
		<br />
		<div class="row">
			<div class="col-md-2">
				<label for="select_workshop_trainer_gender">Gender (*)</label>
			</div>
			<div class="col-md-4">
				<select id="select_workshop_trainer_gender" name="select_workshop_trainer_gender" class="select_control_with_title form-control validate[required]" title="--Select gender--">
					<option></option>
					<option value="male" <?php echo $str_male; ?>>Male</option>
					<option value="female" <?php echo $str_female; ?>>Female</option>
				</select>		
			</div>
		</div>
		
		<br />
		<div class="panel panel-info">
			<div class="panel-heading">Trainer's Educational Qualification (*)</div>
			<div class="panel-body">
				
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<label for="select_graduation">Graduation&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( If you have more than one, mention the most relevant)"></i></label>
							</div>
							<div class="col-md-8 form-group">
								<select id="select_graduation" name="select_graduation" class="form-control select_control_with_title" title="Select Graduation" data-allow-clear = "true">
									<option></option>
									<?php echo $str_grad_qualification_options; ?>
								</select>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-4">
								<label for="select_postgraduation">Post Graduation&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( If you have more than one, mention the most relevant)"></i></label>
							</div>
							<div class="col-md-8 form-group">
								<select id="select_postgraduation" name="select_postgraduation" class="form-control select_control_with_title" title="Select Post Graduation" data-allow-clear = "true">
									<option></option>
									<?php echo $str_post_grad_qualification_options; ?>
								</select>
							</div>
						</div>	
					</div>
					
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2">
								<label for="select_professional_qualification"> Professional Qualification&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( Any certificate or diploma courses that add to your credentials as a trainer )"></i></label>
							</div>
							<div class="col-md-10 form-group">
								<select id="select_professional_qualification" name="select_professional_qualification" title="Select Professional qualifications" class="form-control select_control_with_title_multiple" multiple="multiple">							
									<?php echo $str_professional_courses; ?>
								</select>
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-6">
				<label for="txt_about_trainer">About the Trainer (*)</label>
				<textarea class="form-control validate[required, minSize[5]]" id="txt_about_trainer" name="txt_about_trainer" row="6"><?php echo $about_trainer; ?></textarea>
			</div>
			<div class="col-md-6">
				<label for="txt_present_place_of_work">Present place of work</label>
				<input type="text" class="form-control" id="txt_present_place_of_work" name="txt_present_place_of_work" value="<?php echo $present_place_of_work; ?>"/>
				
				<label for="txt_past_place_of_work">Past Place of Work</label>
				<input type="text" class="form-control" id="txt_past_place_of_work" name="txt_past_place_of_work" value="<?php echo $past_place_of_work; ?>"/>			
			</div>		
		</div>
		<br />
		<div class="row">
			<div class="col-md-9" id="div_trainer_info_result_area"></div>
			<div class="col-md-3">
				<button class="btn btn-success form-control" id="cmd_save_personal_info" data-loading-text="Updating info..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Update Trainer Info</button>
			</div>
		</div>		
	</div>
</form>
