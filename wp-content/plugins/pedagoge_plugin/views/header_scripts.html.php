<?php 

global $wp_query;

$pdg_current_page_url = trim($wp_query->virtual_page->getUrl(), '/');

$should_combine = FALSE;

if(PEDAGOGE_COMBINE_MINIFY) {
	switch($pdg_current_page_url) {
		case 'search':
		//case 'privateinstitute':
			$should_combine = TRUE;
			break;
	}
}

if($should_combine) {
	//Load Google Fonts
	if(!empty($this->google_fonts) && is_array($this->google_fonts)) {
		foreach( $this->google_fonts as $css ) {
			echo '
				<link rel="stylesheet" type="text/css" href="'.$css.'" />
			';
		}	
	}
	
	$all_css_array = array();
	
	//Load Page specific CSS
	if(!empty($this->css_assets) && is_array($this->css_assets)) {
		foreach( $this->css_assets as $css ) {
			if(!in_array($css, $all_css_array)) {
				$all_css_array[] = $css;
			}
		}	
	}
	
	//Load Common CSS
	if(!empty($this->common_css) && is_array($this->common_css)) {
		foreach( $this->common_css as $css ) {
			if(!in_array($css, $all_css_array)) {
				$all_css_array[] = $css;
			}
		}	
	}
	
	if(!empty($all_css_array)) {
		
		$searialised_css_file_name = substr(md5(serialize($all_css_array)), 0, 10);
	
		$cached_css_file_path = PEDAGOGE_PLUGIN_DIR.'storage/cache/'.$searialised_css_file_name.'.css';
		$cached_css_file_url = PEDAGOGE_PLUGIN_URL.'/storage/cache/'.$searialised_css_file_name.'.css';
		if(!file_exists($cached_css_file_path)) {					
			//create cache file.
			$minifier = new MatthiasMullie\Minify\CSS();
			
			foreach($all_css_array as $css ) {
				$processed_file_urlpath = parse_url($css, PHP_URL_PATH);
							
				$processed_file_path = str_replace('pedagoge_plugin/', '', substr($processed_file_urlpath, strpos($processed_file_urlpath, 'pedagoge_plugin')));
				$processed_file_path = PEDAGOGE_PLUGIN_DIR.$processed_file_path;
				
				$minifier->add($processed_file_path);
			}
			$minifier->minify($cached_css_file_path);
		}
		
		echo '<link rel="stylesheet" type="text/css" href="'.$cached_css_file_url.'" />';
	}
	
	//Load custom CSS
	if(!empty($this->app_css) && is_array($this->app_css)) {
		foreach( $this->app_css as $css ) {
			echo '
				<link rel="stylesheet" type="text/css" href="'.$css.'" />
			';
		}	
	}
	
	$header_js_array = array();
	
	if(!empty($this->header_js) && is_array($this->header_js)) {
		foreach( $this->header_js as $js_file ) {
			if(!in_array($js_file, $header_js_array)) {
				$header_js_array[] = $js_file;
			}				
		}	
	}
	if(!empty($header_js_array)) {
		
		$searialised_js_file_name = substr(md5(serialize($header_js_array)), 0, 10);
	
		$cached_js_file_path = PEDAGOGE_PLUGIN_DIR.'storage/cache/'.$searialised_js_file_name.'.js';
		$cached_js_file_url = PEDAGOGE_PLUGIN_URL.'/storage/cache/'.$searialised_js_file_name.'.js';
		if(!file_exists($cached_js_file_path)) {					
			//create cache file.
			$minifier = new MatthiasMullie\Minify\JS();
			
			foreach($header_js_array as $jsfile ) {
				$processed_file_urlpath = parse_url($jsfile, PHP_URL_PATH);
							
				$processed_file_path = str_replace('pedagoge_plugin/', '', substr($processed_file_urlpath, strpos($processed_file_urlpath, 'pedagoge_plugin')));
				$processed_file_path = PEDAGOGE_PLUGIN_DIR.$processed_file_path;
				
				$minifier->add($processed_file_path);
			}
			$minifier->minify($cached_js_file_path);
		}
		
		echo '
				<script type="text/javascript" src="'.$cached_js_file_url.'"></script>
			';	
	}
	
	
} else {
	//Load Common CSS
	if(!empty($this->common_css) && is_array($this->common_css)) {
		foreach( $this->common_css as $css ) {
			echo '
				<link rel="stylesheet" type="text/css" href="'.$css.'" />
			';
		}	
	}
	
	//Load Page specific CSS
	if(!empty($this->css_assets) && is_array($this->css_assets)) {
		foreach( $this->css_assets as $css ) {
			echo '
				<link rel="stylesheet" type="text/css" href="'.$css.'" />
			';
		}	
	}
			
	//Load custom CSS
	if(!empty($this->app_css) && is_array($this->app_css)) {
		foreach( $this->app_css as $css ) {
			echo '
				<link rel="stylesheet" type="text/css" href="'.$css.'" />
			';
		}	
	}
	
	//Load Google Fonts
	if(!empty($this->google_fonts) && is_array($this->google_fonts)) {
		foreach( $this->google_fonts as $css ) {
			echo '
				<link rel="stylesheet" type="text/css" href="'.$css.'" />
			';
		}	
	}
	
	//<!--load header js -->
	if(!empty($this->header_js) && is_array($this->header_js)) {
		foreach( $this->header_js as $js_file ) {
			echo '
				<script type="text/javascript" src="'.$js_file.'"></script>
			';					
		}	
	}
}

		
		