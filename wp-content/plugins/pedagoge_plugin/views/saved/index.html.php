    <section class="body_wrapper margin_top_20">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
                <div class="col-sm-10 col-md-8 col-xs-12 admin_container queries">
                    
                    	<div class="row">
                        	<div class="col-xs-12 blue_background top_radius h1_heading teacher_query_filter">
                                <h1 class="color_white text-center">Queries 
                                    <div class="dropdownfilter4">
                                        <button onclick="filterFunction4()" class="dropdownfilter4">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
                                             <div id="filterFunctionDropdown4" class="dropdown-content-filter4">
                                                  <h5 class="blue_color text-upppercase">Locality</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Garia <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">Jadavpur <span>X</span></p>
                                                  </div>
                                                  <h5 class="blue_color text-upppercase">Subject</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<p class="blue_background color_white border_radius_12 text-uppercase">Hindi <span>X</span></p>
                                                    <p class="blue_background color_white border_radius_12 text-uppercase">English <span>X</span></p>
                                                  </div>
                                                  <h5 class="blue_color">Place of Teaching</h5>
                                                  <div class="border_radius_12 text-left blue_border teacher_locality_filter">
                                                  	<label for="teacher_check1">Student Place</label> <input type="checkbox" name="teacher_check1" id="teacher_check1">
                                                  	<label for="teacher_check2">Teacher Place</label> <input type="checkbox" name="teacher_check2" id="teacher_check2">
                                                  	<label for="teacher_check3">Institutions</label> <input type="checkbox" name="teacher_check3" id="teacher_check3">
                                                  </div>
                                             </div>
                                    </div>
                                </h1>
                            </div>
                        </div>
						
						<?php
							function convertidtosub($str){
								global $wpdb;
								$sql="select subject_name from pdg_subject_name where subject_name_id='$str'";
								$rec=$wpdb->get_results($sql);
								return $rec[0]->subject_name;
							}
							
							
							function convertboardid($str){
								global $wpdb;
								$sql="select academic_board from pdg_academic_board 

where academic_board_id='$str'";
								$rec=$wpdb->get_results($sql);
								return $rec[0]->academic_board;
							}
							
							
							function getlocality($id){
								global $wpdb;
								$sql="select locality from pdg_locality where locality_id='$id'";
								$recloc=$wpdb->get_results($sql);
								foreach ( $recloc as $loc ) return $loc->locality;
								
								
								
							}


							
						?>
                        
                        <div class="row margin_top_20">
                        	<div class="col-xs-12 navigation_menu navigation_menu_media no_padding_left_right">
                                <ul class="parent_tab">
                                	<li><a href="<?= home_url() ?>/teacherdashboard/">Discover</a></li>
                                    <li class="menu_active"><a href="<?= home_url() ?>/saved/">Saved</a></li>
                                    <li><a href="<?= home_url() ?>/interested/">Interested</a></li>
                                    <li><a href="<?= home_url() ?>/referrals/">Referrals</a></li>
                                    <li><a href="<?= home_url() ?>/tarchived/">Archived</a></li>
                                    <li><a href="<?= home_url() ?>/match/">Match</a></li>
                                </ul>
                            </div>
                      	</div>
                        
                                               
                       <div class="inner_container margin_top_10">
					   <?php
										global $wpdb;
										$userid=$_SESSION['member_id'];
										
										$sql="select * from pdg_query where id in (select queryid from pdg_saved where teacherinstid=$userid and status=1)";
										
										$recs=$wpdb->get_results($sql);
										
										
										foreach($recs as $rec) {
										
											
									
							?>
							
                            <div class="row margin_top_10">				
							
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>
											
											<?= convertidtosub($rec->subject) ?>
											
											</span></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-6 col-sm-6 col-xs-12 localities">
                                            	<p>Year/Standard: <?= $rec->standard ?></p>
                                                <p style="display:none" id="board<?= $rec->id ?>">Board/University:<?= convertboardid($rec->board) ?></p>
                                                <div class="preferred_location" style="display:none" id="prefloc<?= $rec->id ?>">
                                                    <p>Locality Type:</p> 
                                                    <ul class="area">
                                                        <!--
														<li><a href="#" class="blue_background">My Home</a></li>
                                                        <li><a href="#" class="blue_background">Tutor’s Home</a></li>
                                                        <li><a href="#" class="blue_background">Institue</a></li>
														-->
													
													<?php
														$teachplace="";
														if($rec->myhome)$teachplace="Student's Location";
														
														else if($rec->tutorhome)$teachplace="Own Location";
														
														else $teachplace="Both own location and student's 

location";
														
														$teachplace="'".$teachplace."'";
													
													
													?>
                                                        <li>
														
														<a 

href="#" class="blue_background"><?php echo $rec->myhome? "My Home":""  ?></a>
														
														
														</li>
                                                        <li>
														
														<a 

href="#" class="blue_background"><?php echo $rec->tutorhome? "Tutor's Home":""  ?></a>
														
														</li>
                                                        <li>
														
														<a 

href="#" class="blue_background"><?php echo $rec->institute? "Institute":""  ?></a>
														
														</li>
													
														
                                                    </ul>
                                                </div>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                       
													   <?php
															
															$localities=explode(",",$rec->localities);
															
															foreach($localities as $loc){
														?>
															<li><a href="#" class="blue_background"><?= getlocality($loc) ?></a></li>	
																
														<?php

														
															}
														
														
														?>
														
                                                    </ul>
                                                </div>
                                                <div class="preferred_location" style="display:none" id="doc<?= $rec->id ?>">
                                                    <p class="width_100">Date of commencement: <strong><?= $rec->start_date ?></strong></p> 
                                                </div>
                                         </div>
										 <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
                                         <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference" style="display:none" id="preferences<?= $rec->id ?>">
                                         	<h5>Preferences: </h5>
                                            <p class="text-justify">
											
											<?= $rec->requirement ?>
											
											</p>
                                         </div>
										 </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                            <ul class="pull-right side_right_menu side_right_menu_media text-center">
                                            	<!--<li><a href="#">Collapse</a></li>-->
												
												<li><a href="#" id="savedqueryexpand<?= $rec->id ?>" rel="<?= $rec->id ?>" class="savedqueryexpand">Expand</a></li>
                                            	<li><a href="#" id="savedquerycollapse<?= $rec->id ?>" style="display:none" rel="<?= $rec->id ?>" class="savedquerycollapse">Collapse</a></li>
																							
                                                <!--<li><a href="#">Unsave</a></li>-->
												
												<li><a href="#" id="savethis<?= $rec->id ?>" rel="<?= $rec->id ?>" class="savethis" data-class="privatetutor">												
												<?php
																				$qid=$rec->id;
																				$sqlx="select status from pdg_saved where queryid=$qid";
																				
																				//echo $sqlx;

																				$recsx=$wpdb->get_results($sqlx);
																				
																				$rowcount = $wpdb->num_rows;
																				
																				if($rowcount>0){
																				
																				foreach ( $recsx as $recx ) {
																					
																					$status=$recx->status;
																					
																				}
																				}else{
																					
																					$status=0;
																					
																				}
																				echo $status==0? 'Save':'Unsave';
																				//echo $status;
																				
																				?>
												
												
												</a></li>
												
												
												
												
												
												
												
												
												
												
                                                <li><a href="#" data-toggle="modal" data-target="#showInterest">Show interest</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#referSomeone">Refer someone</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>								
                           </div>						    
								 <?php
								 
										}
								 
								 ?>								 
                       </div>
                       <!--
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text gray_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 gray_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span> <strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-8 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:</p>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="gray_background">salt lake</a></li>
                                                        <li><a href="#" class="gray_background">salt lake</a></li>
                                                        <li><a href="#" class="gray_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<li class="gray"><a href="#">Expand</a></li>
                                                <li class="gray"><a href="#">Unsave</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       
                       <div class="inner_container margin_top_10">
                            <div class="row">
                                <div class="col-xs-12 inner_container_text blue_border">
                                
                                    <div class="row">
                                         <div class="col-xs-12 gray_background top_radius h1_heading">
                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span>English</span> <strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong></h2>
                                         </div>
                                    </div>
                                    <div class="row margin_top_10">
                                         <div class="col-md-8 col-sm-8 col-xs-7 localities">
                                            	<p>Year/Standard:</p>
                                                <div class="preferred_location">
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
                                                        <li><a href="#" class="gray_background">salt lake</a></li>
                                                        <li><a href="#" class="gray_background">salt lake</a></li>
                                                        <li><a href="#" class="gray_background">salt lake</a></li>
                                                    </ul>
                                                </div>
                                         </div>
                                         <div class="col-md-4 col-sm-4 col-xs-5">
                                            <ul class="pull-right side_right_menu text-center">
                                            	<li class="gray"><a href="#" class="gray">Expand</a></li>
                                                <li class="gray"><a href="#" class="gray">Unsave</a></li>
                                            </ul>
                                         </div>
                                    </div>
                                    
                                 </div>
                           </div>
                       </div>
                       -->
                       
                        <aside class="model_wrapper">
                                    <div class="modal fade" id="showInterest" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Show Interest <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post">
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                      	<h4 class="modal-title text-center padding10">Fees</h4>
                                                         <ul class="student_fees">
                                                            <li><input type="text" name="minimum_fees" id="minimum_fees" placeholder="Min"></li>
                                                            <li><input type="text" name="maximum_fees" id="maximum_fees" placeholder="Max"></li>
                                                         </ul>
                                                         <h4 class="modal-title text-center padding10">Available From</h4>
                                                         <ul class="dd_mm_yy">
                                                            <li>
                                                               <select name="fees_date" id="fees_date">
                                                                  <option value="DD">DD</option>
                                                                  <option value="1">1</option>
                                                                  <option value="2">2</option>
                                                                  <option value="3">3</option>
                                                                  <option value="4">4</option>
                                                                  <option value="5">5</option>
                                                                  <option value="6">6</option>
                                                                  <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                  <option value="9">9</option>
                                                                  <option value="10">10</option>
                                                                  <option value="11">11</option>
                                                                  <option value="12">12</option>
                                                                  <option value="13">13</option>
                                                                  <option value="14">14</option>
                                                                  <option value="15">15</option>
                                                                  <option value="16">16</option>
                                                                  <option value="17">17</option>
                                                                  <option value="18">18</option>
                                                                  <option value="19">19</option>
                                                                  <option value="20">20</option>
                                                                  <option value="21">21</option>
                                                                  <option value="23">22</option>
                                                                  <option value="24">24</option>
                                                                  <option value="25">25</option>
                                                                  <option value="26">26</option>
                                                                  <option value="27">27</option>
                                                                  <option value="28">28</option>
                                                                  <option value="29">29</option>
                                                                  <option value="30">30</option>
                                                                  <option value="31">31</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_month" id="fees_month">
                                                                  <option value="MM">MM</option>
                                                                  <option value="January">January</option>
                                                                  <option value="February">February</option>
                                                                  <option value="March">March</option>
                                                                  <option value="April">April</option>
                                                                  <option value="May">May</option>
                                                                  <option value="June">June</option>
                                                                  <option value="July">July</option>
                                                                  <option value="August">August</option>
                                                                  <option value="September">September</option>
                                                                  <option value="October">October</option>
                                                                  <option value="November">November</option>
                                                                  <option value="December">December</option>
                                                               </select>
                                                            </li>
                                                            <li>
                                                               <select name="fees_year" id="fees_year">
                                                                  <option value="YY">YY</option>
                                                                  <option value="1980">1980</option>
                                                                  <option value="1981">1981</option>
                                                                  <option value="1982">1982</option>
                                                                  <option value="1983">1983</option>
                                                                  <option value="1984">1984</option>
                                                                  <option value="1985">1985</option>
                                                                  <option value="1986">1986</option>
                                                                  <option value="1987">1987</option>
                                                                  <option value="1988">1988</option>
                                                                  <option value="1989">1989</option>
                                                                  <option value="1990">1990</option>
                                                                  <option value="1990">1990</option>
                                                                  <option value="1991">1991</option>
                                                                  <option value="1992">1992</option>
                                                                  <option value="1993">1993</option>
                                                                  <option value="1994">1994</option>
                                                                  <option value="1995">1995</option>
                                                                  <option value="1996">1996</option>
                                                                  <option value="1997">1997</option>
                                                                  <option value="1999">1999</option>
                                                                  <option value="1999">1999</option>
                                                                  <option value="2000">2000</option>
                                                                  <option value="2001">2001</option>
                                                                  <option value="2002">2002</option>
                                                                  <option value="2003">2003</option>
                                                                  <option value="2004">2004</option>
                                                                  <option value="2005">2005</option>
                                                                  <option value="2006">2006</option>
                                                                  <option value="2007">2007</option>
                                                                  <option value="2008">2008</option>
                                                                  <option value="2009">2009</option>
                                                                  <option value="2010">2010</option>
                                                                  <option value="2011">2011</option>
                                                                  <option value="2012">2012</option>
                                                                  <option value="2013">2013</option>
                                                                  <option value="2014">2014</option>
                                                                  <option value="2015">2015</option>
                                                                  <option value="2016">2016</option>
                                                                  <option value="2017">2017</option>
                                                               </select>
                                                            </li>
                                                         </ul>
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" id="teacher_fees_mobile_number" name="teacher_fees_mobile_number" placeholder="Phone Number"> </li>
                                                            <li><input type="email" id="teacher_fees_email_id" name="teacher_fees_email_id" placeholder="Email Id"> </li>
                                                            <li><textarea id="teacher_fees_message" name="teacher_fees_message" placeholder="Tell us why student’s love you..." class="min_height_130"></textarea> </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                                 
                                 <aside class="model_wrapper">
                                    <div class="modal fade" id="referSomeone" role="dialog">
                                       <div class="modal-dialog show_interest">
                                          <!-- Modal content-->
                                          <div class="modal-content blue_border">
                                             <div class="modal-header blue_background color_white fees">
                                                <h4 class="modal-title text-center text-uppercase">Refer someone <button type="button" class="close pull-right" data-dismiss="modal">&times;</button></h4>
                                             </div>
                                             <form action="" method="post">
                                                <div class="modal-body no_padding_top_bottom">
                                                   <div class="row">
                                                      <div class="col-sm-12 subject_type fees">
                                                         <ul class="fees_address_details">
                                                            <li><input type="text" id="teacher_fees_name" name="teacher_fees_name" placeholder="Name :" required> </li>
                                                            <li><input type="text" id="teacher_fees_phone" name="teacher_fees_phone" placeholder="Phone Number :" required> </li>
                                                            <li><input type="email" id="teacher_fees_email" name="teacher_fees_email" placeholder="Email Id :" required> </li>
                                                         </ul>
                                                         <div class="approve_unapprove no_padding_top_bottom">
                                                            <input value="Submit" class="name_address blue_background color_white" type="submit">
                                                         </div>
                                                         <div class="clearfix"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </aside>
                       
                </div>
                <div class="col-sm-1 col-md-2 col-xs-12 ad_section">
                	
                </div>
            </div>
        </div>
    </section>