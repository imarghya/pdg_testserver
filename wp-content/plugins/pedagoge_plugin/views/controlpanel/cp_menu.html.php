<?php
$str_email_visit = '';
if(isset($this->app_data['pedagoge_email'])) {
	$str_email_visit = '
		<li><a href="'.$control_panel_url.'?page=email" class="'.$this->menu_list['email'].'"><i class="gi gi-envelope sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Email</span></a></li>
	';
}
?>

<li>
    <a href="<?php echo $control_panel_url; ?>" class="<?php echo $this->menu_list['dashboard']; ?>"><i class="fa fa-tachometer sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
</li>
<li>
    <a href="<?php echo $control_panel_url; ?>?page=queries" class="<?php echo $this->menu_list['queries']; ?>"><i class="fa fa-tachometer sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Queries</span></a>
</li>
<?= $str_email_visit; ?>
<li><a href="<?php echo $control_panel_url; ?>?page=teacher" class="<?php echo $this->menu_list['teacher']; ?>"><i class="fa fa-male sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Teachers</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=institute" class="<?php echo $this->menu_list['institute']; ?>"><i class="fa fa-university sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Institute</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=workshop" class="<?php echo $this->menu_list['workshop']; ?>"><i class="fa fa-bookmark sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Workshop</span></a></li>
<!-- <li><a href="<?php echo $control_panel_url; ?>?page=student" class="<?php echo $this->menu_list['student']; ?>"><i class="fa fa-graduation-cap sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Students</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=guardian" class="<?php echo $this->menu_list['guardian']; ?>"><i class="fa fa-malefa fa-female sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Guardians</span></a> --></li>
<li><a href="<?php echo $control_panel_url; ?>?page=reviews" class="<?php echo $this->menu_list['reviews']; ?>"><i class="fa fa-rebel sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reviews</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=course" class="<?php echo $this->menu_list['course']; ?>"><i class="fa fa-book sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Course Management</span></a></li>
<!-- <li><a href="<?php echo $control_panel_url; ?>?page=entities" class="<?php echo $this->menu_list['entities']; ?>"><i class="fa fa-hand-paper-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Entities Management</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=users" class="<?php echo $this->menu_list['users']; ?>"><i class="fa fa-user sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Users</span></a></li> -->
<li><a href="<?php echo $control_panel_url; ?>?page=locality" class="<?php echo $this->menu_list['locality']; ?>"><i class="fa fa-map-marker sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Localities</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=seo" class="<?php echo $this->menu_list['seo']; ?>"><i class="fa fa-google sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">SEO</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=impex" class="<?php echo $this->menu_list['impex']; ?>"><i class="fa fa-crosshairs sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Import/Export</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=reports" class="<?php echo $this->menu_list['reports']; ?>"><i class="fa fa-flag sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reports</span></a></li>
<li><a href="<?php echo $control_panel_url; ?>?page=referral" class="<?php echo $this->menu_list['referral']; ?>"><i class="fa fa-user-plus sidebar-nav-icon" aria-hidden="true"></i><span class="sidebar-nav-mini-hide">Referral</span></a></li>
<li>
    <a href="#!" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-key sidebar-nav-icon" aria-hidden="true"></i> Manage star users <i class="fa fa-fw fa-angle-down show_side_menu pull-right"></i></a>
      <ul id="submenu-1" class="collapse">
           <li><a href="<?php echo $control_panel_url; ?>?page=manage_teacher" class="<?php echo $this->menu_list['manage_teacher']; ?>"><i class="fa fa-angle-double-right"></i> Manage teachers</a></li>
           <li><a href="<?php echo $control_panel_url; ?>?page=manage_institute" class="<?php echo $this->menu_list['manage_institute']; ?>"><i class="fa fa-angle-double-right"></i> Manage institutes</a></li>
      </ul>
</li>
<li><a href="<?php echo $control_panel_url; ?>?page=settings" class="<?php echo $this->menu_list['settings']; ?>">
		<i class="fa fa-wrench sidebar-nav-icon"></i>
		<span class="sidebar-nav-mini-hide">Settings</span>
	</a>
</li>