<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_review_import')): ?>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		    <label class="col-md-3 control-label" for="example-file-input">File input</label>
		    <div class="col-md-9">
		        <input type="file" id="fileinput_review_import" name="fileinput_review_import" accept=".xls" />
		    </div>
		</div>
	</div>
	<div class="col-md-6">
		<button class="btn btn-info col-md-12" id="cmd_upload_review_file" data-loading-text="Uploading file...">Upload</button>
	</div>
	<hr />
	<div class="col-md-12 div_excel_upload_result_area"></div>	
</div>
<hr />
<div class="row">
	<div class="col-md-6 div_review_process_result_area"></div>
	<div class="col-md-6">
		<button class="btn btn-success col-md-12" id="cmd_process_reviews" data-loading-text="Processing Reviews...">Process Reviews</button>
	</div>
</div>
<?php else: ?>
	<div class="alert alert-error">Error! You do not have the permission to view this Information!</div>
<?php endif; ?>