<div class="block">		    
    <div class="block-title">
    	<div class="block-options pull-right">
    		<button class="btn btn-alt btn-sm btn-default cmd_add_reviews" data-toggle="modal" data-target="#modal_add_review" title="Add a review"><i class="fa fa-plus"></i></button>
            <button class="btn btn-alt btn-sm btn-default cmd_refresh_reviews" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
        </div>
        <div class="block-options pull-right">
            <img class="center-block hidden img_reviews_loader" style="margin-top: 8px;" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
        </div>
        <h2><strong>Reviews List</strong> </h2>
    </div>
    <div class="row well">	    	
    	<div class="col-md-3">
    		<label for="txt_fltr_review_student_name">Student Name</label><input type="text" class="form-control fltr_reviews" data-column="3" id="txt_fltr_review_student_name"/>
    	</div>
    	<div class="col-md-3">
    		<label for="txt_fltr_review_student_email">Student Email</label><input type="text" class="form-control fltr_reviews" data-column="1" id="txt_fltr_review_student_email"/>
    	</div>
    	<div class="col-md-3">
    		<label for="txt_fltr_review_teacher_name">Teacher Name</label><input type="text" class="form-control fltr_reviews" data-column="6" id="txt_fltr_review_teacher_name"/>
    	</div>
    	<div class="col-md-3">
    		<label for="txt_fltr_review_teacher_email">Teacher Email</label><input type="text" class="form-control fltr_reviews" data-column="4" id="txt_fltr_review_teacher_email"/>
    	</div>
    	<div class="col-md-4">
    		<label for="select_filter_reviews_anonymous">Anonymous</label>
    		<select class="form-control select_review_filter" id="select_filter_reviews_anonymous" data-column="16">
    			<option value="">Select</option>
    			<option value="yes">Yes</option>
    			<option value="no">No</option>	    				    			
    		</select>
    	</div>
    	<div class="col-md-4">
    		<label for="select_filter_reviews_approved">Approved</label>
    		<select class="form-control select_review_filter" id="select_filter_reviews_approved" data-column="17">
    			<option value="">Select</option>
    			<option value="yes">Yes</option>
    			<option value="no">No</option>    			
    		</select>
    	</div>
    	<div class="col-md-4">
    		<label for="select_filter_reviews_rejected">Rejected</label>
    		<select class="form-control select_review_filter" id="select_filter_reviews_rejected" data-column="18">
    			<option value="">Select</option>
    			<option value="yes">Yes</option>
    			<option value="no">No</option>    			
    		</select>
    	</div>
    </div>
    
    
    <hr />
    <table id="reviews_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
            	<td>ID</td>	            	
            	<td>Reviewer Email</td>
            	<td>Reviewer Role</td>
            	<td>Reviewer Name</td>
            	<td>Teacher Email</td>
            	<td>Teacher Role</td>
            	<td>Teacher Name</td>	            	
            	<td>review_text</td>
            	<td>reviewer_user_id</td>
            	<td>tutor_institute_user_id</td>	            	
            	<td>Rating1</td>
            	<td>Rating2</td>
            	<td>Rating3</td>
            	<td>Rating4</td>
            	<td>average_rating</td>
            	<td>All Ratings</td>
            	<td>Anonymous</td>
            	<td>Approved</td>
            	<td>Rejected</td>
            	<td>Deleted</td>	            	
            	<td>Action</td>
            </tr>
        </thead>
        <tfoot>
            <tr>
            	<td colspan="21"></td>	            	
            </tr>
        </tfoot>
    </table>
    
    <!-- Add Review Modal -->
	<div class="modal fade" id="modal_add_review" tabindex="-1" role="dialog" aria-labelledby="AddReviewModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="AddReviewModalLabel">Add a review</h4>
				</div>
				
				<div class="modal-body">
					<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_review_update') || current_user_can('pdg_cap_cp_review_delete')):?>
					<div class="row">
						<div class="col-md-6">
							<button class="btn btn-info col-md-12 cmd_modal_find_teacher" data-toggle="modal" data-target="#modal_find_teacher"><i class="fa fa-search" aria-hidden="true"></i> Find Teacher/Institute</button>
							
						</div>
						<div class="col-md-6">								
							<button class="btn btn-info col-md-12 cmd_modal_find_student" data-toggle="modal" data-target="#modal_find_student"><i class="fa fa-search" aria-hidden="true"></i> Find/Add Student/Guardian</button>
						</div>
					</div>
					<hr />
					<div class="row">
						<div class="col-md-4">
							<div class="well">
								<label for="txt_modal_teacher_name">Teacher Name</label>
								<input class="form-control" type="text" value="" id="txt_modal_teacher_name" readonly="readonly" />
								<input type="hidden" value="" id="hidden_modal_teacher_user_id"/>
								
								<label for="txt_modal_teacher_email">Teacher Email</label>
								<input class="form-control" type="text" value="" id="txt_modal_teacher_email" readonly="readonly" />	
							</div>
						</div>
						<div class="col-md-4">
							<div class="well">
								<label for="txt_modal_student_name">Student Name</label>
								<input class="form-control" type="text" value="" id="txt_modal_student_name" readonly="readonly" />
								<input type="hidden" value="" id="hidden_modal_student_user_id"/>
								
								<label for="txt_modal_student_email">Student Email</label>
								<input class="form-control" type="text" value="" id="txt_modal_student_email" readonly="readonly" />	
							</div>
							
						</div>
						<div class="col-md-4">
							<label for="txt_modal_review_text">Review Text</label>
							<textarea id="txt_modal_review_text" class="form-control" rows="5"></textarea>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-2">
							<label for="txt_modal_star1">Star1</label>
							<input type="text" class="form-control positive star_count" value="" id="txt_modal_star1" maxlength="4"/>								
						</div>
						<div class="col-md-2">
							<label for="txt_modal_star2">Star2</label>
							<input type="text" class="form-control positive star_count" value="" id="txt_modal_star2" maxlength="4"/>
						</div>
						<div class="col-md-2">
							<label for="txt_modal_star3">Star3</label>
							<input type="text" class="form-control positive star_count" value="" id="txt_modal_star3" maxlength="4"/>
						</div>
						<div class="col-md-2">
							<label for="txt_modal_star4">Star4</label>
							<input type="text" class="form-control positive star_count" value="" id="txt_modal_star4" maxlength="4"/>
						</div>
						<div class="col-md-2">
							<label for="txt_modal_star_average">Overall Ratings</label>
							<input type="text" class="form-control positive" value="" id="txt_modal_overall_star" maxlength="4"/>
						</div>
						<div class="col-md-1"></div>
					</div>
					<hr />
					<div class="row">							
						<div class="col-md-3"></div>
						
						<div class="col-md-2">
							<div class="checkbox">
								<label><input type="checkbox" id="chk_modal_anonymous" > Anonymous</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox">
								<label><input type="checkbox" id="chk_modal_approved" checked="checked"> Approved</label>
							</div>								 
						</div>
						<div class="col-md-2">
							<div class="checkbox">
								<label><input type="checkbox" id="chk_modal_rejected" > Rejected</label>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<hr />
					<div id="div_modal_result_area"></div>
					<input type="hidden" id="hidden_review_id" />
					<?php else: ?>
						<div class="alert alert-error">Error! You do not have the permission to update the Information!</div>
					<?php endif; ?>
				</div>
				
				<div class="modal-footer">
					<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_review_delete')): ?>
					<button type="button" class="btn btn-danger cmd_delete_modal_review" data-loading-text="Deleting review..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Delete Review</button>
					<?php endif; ?>
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
					<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_review_update')): ?>
					<button type="button" class="btn btn-primary cmd_save_modal_review" data-loading-text="Loading..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Save changes</button>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!-- Find Teacher/Institute Modal -->
	<div class="modal fade" id="modal_find_teacher" tabindex="-1" role="dialog" aria-labelledby="ViewFindTeacherModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ViewFindTeacherModalLabel">Find Teacher/Institute</h4>
				</div>
				<div class="modal-body">
					
					<div class="row">
						
						<div class="col-md-4">
							<label for="txt_filter_teacher_by_name">Filter by Name</label><input type="text" class="form-control fltr_teacher" id="txt_filter_teacher_by_name" data-column="1" />								
						</div>
						<div class="col-md-4">
							<label for="txt_filter_teacher_by_email">Filter by Email</label><input type="text" class="form-control fltr_teacher" id="txt_filter_teacher_by_email"  data-column="2"/>
						</div>
						<div class="col-md-4">
							<label for="txt_filter_teacher_by_mobile">Filter by Mobile No</label><input type="text" class="form-control fltr_teacher" id="txt_filter_teacher_by_mobile"  data-column="3"/>
						</div>
					</div>
					
					<table id="tbl_teachers_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				            	<td>ID</td>
				            	<td>Name</td>	            	
				            	<td>Email</td>
				            	<td>Mobile</td>
				            	<td>Role ID</td>
				            	<td>Role Name</td>
				            	<td>Role</td>					            	
				            	<td>Select</td>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				            	<td colspan="8"></td>	            	
				            </tr>
				        </tfoot>
				    </table>
				    <hr />
				    <div class="row">
				    	<div class="col-md-12 text-center"><strong>Selected Teacher/Institute</strong></div>
				    	<div class="col-md-3">
					    	<label for="txt_fltr_selected_teacher_name">Name</label><input type="text" class="form-control" id="txt_fltr_selected_teacher_name" readonly="readonly"/>							
					    </div>
					    <div class="col-md-3">
					    	<label for="txt_fltr_selected_teacher_email">Email</label><input type="text" class="form-control" id="txt_fltr_selected_teacher_email" readonly="readonly"/>
					    </div>
					    <div class="col-md-3">
					    	<label for="txt_fltr_selected_teacher_role">Role</label><input type="text" class="form-control" id="txt_fltr_selected_teacher_role" readonly="readonly"/>
					    </div>
					    <div class="col-md-3">
					    	<label for="txt_fltr_selected_teacher_mobile">Mobile</label><input type="text" class="form-control" id="txt_fltr_selected_teacher_mobile" readonly="readonly"/>
					    	<input type="hidden" id="hidden_fltr_selected_teacher_user_id" />
					    </div>
					    <div class="col-md-12 div_selected_teacher_result_area"></div>	
				    </div>					    
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
					<button type="button" class="btn btn-primary cmd_select_filtered_teacher"><i class="fa fa-check-square-o" aria-hidden="true"></i> Select Teacher/Institute</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Find Student/Guardian Modal -->
	<div class="modal fade" id="modal_find_student" tabindex="-1" role="dialog" aria-labelledby="ViewFindStudentModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ViewFindStudentModalLabel">Find Student/Guardian</h4>
				</div>
				<div class="modal-body">
					
					<div class="row">
						
						<div class="col-md-4">
							<label for="txt_filter_student_by_name">Filter by Name</label><input type="text" class="form-control fltr_student" id="txt_filter_student_by_name" data-column="1" />								
						</div>
						<div class="col-md-4">
							<label for="txt_filter_student_by_email">Filter by Email</label><input type="text" class="form-control fltr_student" id="txt_filter_student_by_email"  data-column="2"/>
						</div>
						<div class="col-md-4">
							<label for="txt_filter_student_by_mobile">Filter by Mobile No</label><input type="text" class="form-control fltr_student" id="txt_filter_student_by_mobile"  data-column="3"/>
						</div>
					</div>
					
					<table id="tbl_students_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<td>ID</td>
								<td>Name</td>	            	
								<td>Email</td>
								<td>Mobile</td>
								<td>Role ID</td>
								<td>Role Name</td>
								<td>Role</td>					            	
								<td>Select</td>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="8"></td>	            	
							</tr>
						</tfoot>
					</table>
					<hr />
					<div class="row">
						<div class="col-md-12 text-center"><strong>Selected Student/Guardian</strong></div>
						<div class="col-md-3">
							<label for="txt_fltr_selected_student_name">Name</label><input type="text" class="form-control" id="txt_fltr_selected_student_name" readonly="readonly"/>							
						</div>
						<div class="col-md-3">
							<label for="txt_fltr_selected_student_email">Email</label><input type="text" class="form-control" id="txt_fltr_selected_student_email" readonly="readonly"/>
						</div>
						<div class="col-md-3">
							<label for="txt_fltr_selected_student_role">Role</label><input type="text" class="form-control" id="txt_fltr_selected_student_role" readonly="readonly"/>
						</div>
						<div class="col-md-3">
							<label for="txt_fltr_selected_student_mobile">Mobile</label><input type="text" class="form-control" id="txt_fltr_selected_student_mobile" readonly="readonly"/>
							<input type="hidden" id="hidden_fltr_selected_student_user_id" />
						</div>
						<div class="col-md-12 div_selected_student_result_area"></div>	
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success cmd_create_student_guardian" data-toggle="modal" data-target="#modal_create_student"><i class="fa fa-plus" aria-hidden="true"></i>Add Student/Guardian</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
					<button type="button" class="btn btn-primary cmd_select_filtered_student"><i class="fa fa-check-square-o" aria-hidden="true"></i> Select Student/Guardian</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Create Student/Guardian Modal -->
	<div class="modal fade" id="modal_create_student" tabindex="-1" role="dialog" aria-labelledby="ViewStudentCreateLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-lg">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ViewStudentCreateLabel">Create Student/Guardian</h4>
				</div>
				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-6">
							<label for="txt_student_create_first_name">First Name</label><input type="text" class="form-control" value="" id="txt_student_create_first_name"/>								
						</div>
						<div class="col-md-6">
							<label for="txt_student_create_last_name">Last Name</label><input type="text" class="form-control" value="" id="txt_student_create_last_name"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label for="txt_student_create_email">Email</label><input type="text" class="form-control" value="" id="txt_student_create_email"/>
						</div>
						<div class="col-md-6">
							<label for="txt_student_create_mobile">Mobile</label><input type="text" class="form-control positive-integer" value="" id="txt_student_create_mobile" maxlength="10"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="txt_student_create_age">Age</label><input type="text" class="form-control positive-integer" value="" id="txt_student_create_age"/>
						</div>
						<div class="col-md-4">
							<label for="select_student_create_gender">Gender</label>
							<select id="select_student_create_gender" class="form-control">
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
						</div>
						<div class="col-md-4">
							<label for="select_student_create_role">Register as</label>
							<select id="select_student_create_role" class="form-control">
								<option value="student">Student</option>
								<option value="guardian">Guardian</option>
							</select>
						</div>
						<div class="col-md-12 div_create_student_result_area">				
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
					<button type="button" class="btn btn-primary cmd_create_student" data-loading-text="Loading..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
</div>