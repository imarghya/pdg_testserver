<?php
$str_tab_manage_reviews = '';
$str_tab_reviews_import = '';
$str_tab_manage_reviews_content = '';
$str_tab_reviews_import_content = '';


?>


<!-- Page content -->
<div id="page-content"> 
	
	<!-- Block Tabs -->
    <div class="block full">
        <!-- Block Tabs Title -->
        <div class="block-title">            
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#tab_manage_review">Manage Reviews</a></li>
                <li><a href="#tab_import_reviews">Import Reviews</a></li>                
            </ul>
        </div>
        <!-- END Block Tabs Title -->

        <!-- Tabs Content -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_manage_review"><?php include_once('reviews_manage.html.php'); ?></div>
            <div class="tab-pane" id="tab_import_reviews"><?php include_once('reviews_import.html.php'); ?></div>            
        </div>
        <!-- END Tabs Content -->
    </div>
    <!-- END Block Tabs -->
	
</div>
<!-- END Page Content -->