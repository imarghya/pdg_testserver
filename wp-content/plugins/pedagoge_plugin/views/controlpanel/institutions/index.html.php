<div id="page-content">
   <!-- Block Tabs -->
    <div class="block full">        
        <div class="block-title">
        	<h3>Teachers List</h3>
        	<input type="hidden" value="<?php echo home_url('/institute'); ?>" id="hidden_institutes_url"/>
        	<input type="hidden" value="<?php echo home_url('/edit'); ?>" id="hidden_institute_edit_url"/>
        </div>
        <div class="well">
        	<div class="row">
        		<div class="col-md-4">
	        		<label for="txt_institute_fltr_email">Email</label>
	        		<input type="text" class="form-control txt_fltr_institute" id="txt_institute_fltr_email" data-column="6"/>				
	        	</div>
	        	<div class="col-md-4">
	        		<label for="txt_institute_fltr_name">Institute Name</label>
	        		<input type="text" class="form-control txt_fltr_institute" id="txt_institute_fltr_name" data-column="1"/>
	        	</div>        	
	        	<div class="col-md-4">
	        		<label for="txt_institute_fltr_mobile">Mobile</label>
	        		<input type="text" class="form-control txt_fltr_institute" id="txt_institute_fltr_mobile" data-column="7"/>
	        	</div>
        	</div>
        	<div class="row">
        		<div class="col-md-3">
        			<label for="txt_institute_fltr_locality">Locality</label>
	        		<input type="text" data-column="17" class="form-control txt_fltr_institute" name="txt_institute_fltr_locality" id="txt_institute_fltr_locality" />
        		</div>
        		<div class="col-md-3">
        			<label for="txt_institute_fltr_subject">Subject</label>
	        		<input type="text" data-column="18" class="form-control txt_fltr_institute" name="txt_institute_fltr_subject" id="txt_institute_fltr_subject" />
        		</div>
        		<div class="col-md-2">
        			<label for="txt_institute_fltr_class">Class</label>
	        		<input type="text" data-column="19" class="form-control txt_fltr_institute" name="txt_institute_fltr_class" id="txt_institute_fltr_class" />
        		</div>
        		<div class="col-md-2">
        			<label for="txt_institute_fltr_board">Board</label>
	        		<input type="text" data-column="20" class="form-control txt_fltr_institute" name="txt_institute_fltr_board" id="txt_institute_fltr_board" />
        		</div>
        		<div class="col-md-2">
        			<label for="txt_institute_fltr_age">Age</label>
	        		<input type="text" data-column="21" class="form-control txt_fltr_institute" name="txt_institute_fltr_age" id="txt_institute_fltr_age" />
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-2">
	        		<label for="select_institute_fltr_approved">Approved</label>
	        		<select id="select_institute_fltr_approved" class="form-control select_fltr_institute" data-column="10">
	        			<option value="">Select</option>
	        			<option value="yes">Yes</option>
	        			<option value="no">No</option>
	        		</select>
	        	</div>
	        	<div class="col-md-2">
	        		<label for="select_institute_fltr_active">Active</label>
	        		<select id="select_institute_fltr_active" class="form-control select_fltr_institute" data-column="11">
	        			<option value="">Select</option>
	        			<option value="yes">Yes</option>
	        			<option value="no">No</option>
	        		</select>
	        	</div>
	        	<div class="col-md-2">
	        		<label for="select_institute_fltr_verified">Verified</label>
	        		<select id="select_institute_fltr_verified" class="form-control select_fltr_institute" data-column="12">
	        			<option value="">Select</option>
	        			<option value="yes">Yes</option>
	        			<option value="no">No</option>
	        		</select>
	        	</div>
	        	<div class="col-md-2">
	        		<label for="select_institute_fltr_deleted">Deleted</label>
	        		<select id="select_institute_fltr_deleted" class="form-control select_fltr_institute" data-column="13">
	        			<option value="">Select</option>
	        			<option value="yes">Yes</option>
	        			<option value="no">No</option>
	        		</select>
	        	</div>
	        	<!-- city field added -->
	        	<div class="col-md-4">
	        		<label for="txt_institute_fltr_city">City Name</label>
	        		<input type="text" class="form-control txt_fltr_institute" id="txt_institute_fltr_city" data-column="22"/>
	        	</div>
	        	<!-- city field added -->
	        	<div class="col-md-4">
	        		<br />
	        		<button class="btn btn-warning col-md-12" id="cmd_reset_institute_filters">Reset</button>
	        	</div>
        	</div>
        </div>
        <div class="row">
        	
        	
        	
        </div>
        <div class="table-responsive">
	    	<table id="table_institutes_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		            	<th>ID</th>
		            	<th>Name</th>
		            	<th>profile_slug</th>
		            	<th>user_id</th>
		            	<th>personal_info_id</th>
		            	<th>user_name</th>
		            	<th>Email</th>
		            	<th>Mobile</th>
		            	<th>Contact Person</th>
		            	<th>Primary Contact</th>
		            	<th>Approved</th>
		            	<th>Active</th>
		            	<th>Verified</th>
		            	<th>Deleted</th>
		            	<th>View/Edit/Update/Delete</th>
		            	<th>Created</th>
		            	<th>Updated</th>
		            	
		            	<th>Locality</th>
		            	<th>Subject</th>
		            	<th>Class</th>
		            	<th>Board</th>
		            	<th>Age</th>		            	
		            	<th>City</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<tr>
		        		<td colspan="20">Loading Data</td>
		        	</tr>
		        </tbody>			        
		    </table>
	    </div>
	    
	    <!-- update Teacher User Modal -->
		<div class="modal fade" id="modal_update_institute_user_info" tabindex="-1" role="dialog" aria-labelledby="lbl_modal_update_institute_user_info">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="lbl_modal_update_institute_user_info">Update Basic Info of Institute</h4>
					</div>
					<div class="modal-body">
						<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_institute_user_info_edit')):?>
						<div class="row">
							<div class="col-md-6">
								<label for="txt_institute_name">Institute Name</label>
								<input type="text" id="txt_institute_name" class="form-control" />						
							</div>
							<div class="col-md-6">
								<label for="txt_institute_mobile_no">Mobile No</label>
								<input type="text" id="txt_institute_mobile_no" class="form-control positive-integer" />
							</div>
							<div class="col-md-6">
								<label for="txt_institute_contact_person_name">Contact Person</label>
								<input type="text" id="txt_institute_contact_person_name" class="form-control" />						
							</div>
							<div class="col-md-6">
								<label for="txt_institute_primary_contact_no">Primary Contact No</label>
								<input type="text" id="txt_institute_primary_contact_no" class="form-control positive-integer" />
							</div>							
						</div>
						<hr />
						<div class="row">
							<div class="col-md-6">
								<label for="txt_institute_user_name">User Name</label>
								<input type="text" id="txt_institute_user_name" class="form-control" />
							</div>
							<div class="col-md-6">
								<label for="txt_institute_password">Password (Leave it empty for no change)</label>
								<input type="password" id="txt_institute_password" class="form-control" />
							</div>
							<div class="col-md-12">
								<label for="txt_institute_email_address">Email Address</label>
								<input type="text" id="txt_institute_email_address" class="form-control" />						
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-md-3">
								<label for="select_approve_institute">Approved</label>
								<select id="select_approve_institute" class="form-control">
									<option value="">Select</option>
									<option value="yes">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="select_active_institute">Activated</label>
								<select id="select_active_institute" class="form-control">
									<option value="">Select</option>
									<option value="yes">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="select_verify_institute">Verified</label>
								<select id="select_verify_institute" class="form-control">
									<option value="">Select</option>
									<option value="yes">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
							<div class="col-md-3">
								<label for="select_deleted_institute">Deleted</label>
								<select id="select_deleted_institute" class="form-control">
									<option value="">Select</option>
									<option value="yes">Yes</option>
									<option value="no">No</option>
								</select>
							</div>
						</div>
						
						<div id="div_update_institute_result_area"></div>
						<input type="hidden" id="hidden_institute_id"/>
						<input type="hidden" id="hidden_institute_user_id"/>
						<input type="hidden" id="hidden_institute_profile_id"/>
						<?php else: ?>
							<div class="alert alert-error">Error! You do not have the permission to update the Information!</div>
						<?php endif; ?>
					</div>
					<div class="modal-footer">				
						<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
						<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_institute_user_info_edit')): ?>
						<button type="button" class="btn btn-success cmd_save_updated_institute_info" data-loading-text="Loading..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Update Teacher Info</button>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	    
	    
    </div>
    <!-- END Block Tabs -->
</div>

