<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>Unauthorized Access!<br>
                <small> 
                	You do not have permission to access this page!
                	Please return to <a href="<?php echo home_url('/'); ?>">home page</a>!
                </small>
            </h1>
        </div>
    </div>
    

    
</div>
<!-- END Page Content -->