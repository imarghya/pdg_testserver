<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            
            <div class="row">
            	<div class="col-md-8 col-md-offset-2">
            		<div class="row">
            			<div class="col-md-6">
            				<select id="select_pdg_impex_type" class="form-control">
            					<option value="">Select a data dump type</option>
            					<option value="teacher_dump" >Teachers data dump</option>
            					<option value="institute_dump">Institute data dump</option>
            				</select>
            			</div>
            			<div class="col-md-6">
            				<button class="btn btn-success col-md-12 cmd_generate_impex" data-loading-text="Generating dumps...">Generate Dump and Email</button>
            			</div>            			
            		</div>
            	</div>
            </div>
        </div>
    </div>
    
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2 class="impex_title_area">Institute/Teacher Data Dump</h2>
        </div>
        <!-- END Example Title -->
		<img class="center-block hidden img_impex_loader" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
		
		<div id="div_impex_dump_area"></div>        
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->