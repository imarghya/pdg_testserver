<div class="block">		    
    <div class="block-title">
    	<!-- <div class="block-options pull-right">    		
            <button class="btn btn-alt btn-sm btn-default cmd_reset_workshop_filters" data-toggle="tooltip" title="Reset Filters"><i class="fa fa-refresh"></i></button>
        </div> -->
        
        <h2><strong>Workshops List</strong> </h2>
    </div>
    
    <div class="row">	    	
    	<div class="col-md-6">
    		<label for="txt_fltr_workshop_name">Workshop Name</label>
    		<input type="text" class="form-control fltr_workshop" data-column="2" id="txt_fltr_workshop_name"/>
    	</div>
    	<div class="col-md-6">
    		<label for="txt_fltr_trainer_name">Trainer Name</label>
    		<input type="text" class="form-control fltr_workshop" data-column="3" id="txt_fltr_trainer_name"/>
    	</div>
    </div>
    <br/>
    <div class="row">    	
    	<div class="col-md-4">
    		<label for="select_fltr_workshop_category">Category</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_fltr_workshop_category" data-column="6" title="Select Category">
    			<option value="">Select</option>
    			<?= $str_workshop_categories_options; ?>	    				    			
    		</select>
    	</div>
    	<div class="col-md-4">
    		<label for="select_fltr_workshop_city">City</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_fltr_workshop_city" data-column="8" title="Select City">
    			<option value="">Select</option>
    			<?= $str_city_options; ?>   			
    		</select>
    	</div>
    	<div class="col-md-4">
    		<label for="select_fltr_workshop_locality">Locality</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_fltr_workshop_locality" data-column="10" title="Select Locality">
    			<option value="">Select</option>
    		</select>
    	</div>
    </div>
    <br/>
	<div class="row">
		<div class="col-md-3">
    		<label for="select_fltr_workshop_type">Type</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_fltr_workshop_type" data-column="12" title="Select Type">
    			<option value="">Select</option>
    			<option value="free">Free</option>
    			<option value="paid">Paid</option>	    				    			
    		</select>
    	</div>
    	<div class="col-md-3">
    		<label for="select_filter_workshop_approved">Approved</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_filter_workshop_approved" data-column="17" title="Select Appoval Status">
    			<option value="">Select</option>
    			<option value="yes">Yes</option>
    			<option value="no">No</option>    			
    		</select>
    	</div>
    	<div class="col-md-3">
    		<label for="select_filter_workshop_featured">Featured</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_filter_workshop_featured" data-column="19" title="Select Featured">
    			<option value="">Select</option>
    			<option value="yes">Yes</option>
    			<option value="no">No</option>    			
    		</select>
    	</div>
    	<div class="col-md-3">
    		<label for="select_fltr_workshop_status">Active</label>
    		<select class="form-control select_workshop_filter select_control_with_title" id="select_fltr_workshop_status" data-column="18" title="Select Active Status">
    			<option value="">Select</option>
    			<option value="yes">Yes</option>
    			<option value="no">No</option>    			
    		</select>
    	</div>
	</div>    
    <hr />
    <table id="table_workshops_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
            	<th>ID</th>
            	<th>User ID</th>
				<th>Name</th>
				<th>Trainer</th>
				<th>Start</th>
				<th>End</th>
				<th>workshop_category_id</th>
				<th>Category</th>			
				<th>workshop_city_id</th>
				<th>City</th>
				<th>workshop_locality_id</th>
				<th>Locality</th>
				<th>Type</th>
				<th>Seats</th>
				<th>price</th>
				<th>Pay Online</th>
				<th>Pay @ Venue</th>
				<th>Approved</th>
				<th>Active</th>
				<th>Featured</th>
				<th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
            	<td colspan="20"></td>	            	
            </tr>
        </tfoot>
    </table>
	<input type="hidden" value="<?= home_url('/manageworkshop'); ?>" id="hidden_manage_workshop_url"/>
	<input type="hidden" value="<?= home_url('/workshop'); ?>" id="hidden_workshop_url"/>
</div>
