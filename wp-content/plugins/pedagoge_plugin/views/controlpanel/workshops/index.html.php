<?php
$str_workshop_categories_options = '';
if(isset($pdg_workshop_categories) && !empty($pdg_workshop_categories)) {
	foreach($pdg_workshop_categories as $workshop_category) {
		$str_workshop_categories_options .= '
			<option value="'.$workshop_category->workshop_category_id.'">'.$workshop_category->workshop_category.'</option>
		';
	}
}

$str_city_options = '';
if(isset($pdg_city) && !empty($pdg_city)) {
	foreach($pdg_city as $city_info) {
		$str_city_options .='
			<option value="'.$city_info->city_id.'">'.$city_info->city_name.'</option>
		';
	}
}

$localities_array = array();

if(isset($pdg_locality) && !empty($pdg_locality)) {
	foreach($pdg_locality as $locality_info) {
		$locality_name = $locality_info->locality;
		$locality_id = $locality_info->locality_id;
		$city_id = $locality_info->city_id;
		$str_localities_options = '<option value="'.$locality_id.'" data-city_id="'.$city_id.'">'.$locality_name.'</option>';
		
		if(!array_key_exists($city_id, $localities_array)) {
			$localities_array[$city_id] = $str_localities_options;
		} else {
			$localities_array[$city_id] .= $str_localities_options;
		}
	}
}
?>


<div id="page-content">
	<?php include_once('workshops.html.php'); ?>   
    <!-- <div class="block full">        
        <div class="block-title">        	
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#tab_workshops" data-toggle="tab"><i class="fa fa-bookmark"></i> Workshops</a></li>
                <li><a href="#tab_workshop_bookings" data-toggle="tab"><i class="fa fa-money"></i> Bookings</a></li>
                <li><a href="#tab_workshop_settings" data-toggle="tab"><i class="fa fa-wrench"></i> Settings</a></li>
            </ul>
        </div>
                
        <div class="tab-content">
            <div class="tab-pane active" id="tab_workshops"><?php include_once('workshops.html.php'); ?></div>
            <div class="tab-pane" id="tab_workshop_bookings"><?php include_once('bookings.html.php'); ?></div>
            <div class="tab-pane" id="tab_workshop_settings"><h3>Coming soon!</h3></div>
        </div>        
    </div> -->
    
</div>

<!-- Localities in javascript -->
<script type="text/javascript" charset="utf-8">
	var $localities_data = <?php echo json_encode($localities_array); ?>;
</script>