<?php
$landing_model = new ModelLanding();
//for insert--------------------------------------
if($_POST['teacher']){
$teacher_id=$_POST['teacher'];
$succ = $landing_model->insert_star_inst($teacher_id);
if($succ=='success'){
 echo '<script>
   $(document).ready(function(){
   alertify.success("Success: Records have been added successfully.");
   });
  </script>';	
 }   
}
//------------------------------------------------
//For delete--------------------------------------
if($_POST['id']){
$id=$_POST['id'];
$succ = $landing_model->delete_star_inst($id);
if($succ=='success'){
 echo '<script>
   $(document).ready(function(){
   alertify.success("Success: Records have been deleted successfully.");
   });
  </script>';	
 }   
}
//------------------------------------------------
?>
<script>
function del(){
  return confirm('Are You Sure?');
  }
</script>
<div id="page-wrapper">
   <div class="container-fluid">
      <div class="row">
         <aside class="col-xs-12 table_content_settings border_radius_12">
         	<div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
                    <div class="manage_role select_holder">
                        <div class="row">
                            <div class="col-xs-12 manage_teacher">
			      <form method="post">
                                <div class="input-group">
                                <span class="triangel_decoratiton">
				 <?php
				 $res = $landing_model->get_inst_list();
				 ?>
                                      <select required name="teacher" class="selectpicker search_awesome_text" data-live-search="true" data-live-search-style="begins" title="Select option..">
                                            <option value="">Select Teacher</option>
					    <?php foreach($res AS $teach){?>
                                            <option value="<?php echo $teach->new_user_id; ?>"><?php echo $teach->name; ?></option>
					    <?php } ?>
                                     </select>
				      
                                  </span>
                                  <span class="input-group-btn">
                                    <button type="submit" class="btn manage_teacher_btn" type="button">Add</button>
                                  </span>
                                </div>
				</form>
                            </div>
                        </div>
                    </div>
                    
                    <div class="margin_top_30 manage_role_table manage_member_table manage_delete">
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped">
                                   <thead>
                                      <tr class="admin_blue_background color_white">
                                         <th scope="col">Name</th>
                                         <th scope="col">ID</th>
                                         <th scope="col">Action</th>
                                      </tr>
                                   </thead>
                                   <tbody>
				    <?php
				    $res_star = $landing_model->get_star_inst_list();
				    foreach($res_star AS $star_tech){
				    ?>
                                      <tr>
                                         <th scope="row"><?php echo $star_tech->name; ?></th>
                                         <td>#<?php echo $star_tech->inst_id; ?></td>
                                         <td>
					  <form method="post">
					  <input type="hidden" name="id" value="<?php echo $star_tech->id; ?>">
					  <button type="submit" onclick='return del();'><i class="fa fa-trash-o" aria-hidden="true"></i></button>
					  </form>
					 </td>
                                      </tr>
                                 <?php } ?>
                                   </tbody>
                                </table>
                            </div>
                        </div>
            </div>
                    
                    
                </div>
            </div>
         </aside>
      </div>
   </div>
</div>