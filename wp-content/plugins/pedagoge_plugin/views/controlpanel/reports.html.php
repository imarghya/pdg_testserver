<?php

$report_type = '';

$str_reports_content = '';

$empty_localities_selected = '';
$empty_subjects_selected = '';
$check_title = "Select a report";

if (isset($_GET['report_type'])) {
	$report_type = $_GET['report_type'];

	switch($report_type) {
		case 'empty_localities' :
			$check_title = " Empty Locality report";
			$empty_localities_selected = ' selected';
			$return_data = ControlPanelPedagogeReports::fn_empty_locality_report();
			$str_reports_content = $return_data['data'];
			break;
		case 'empty_subjects' :
			$check_title = "Empty Subjects report";
			$empty_subjects_selected = ' selected';
			$return_data = ControlPanelPedagogeReports::fn_empty_subjects_report();
			$str_reports_content = $return_data['data'];
			break;
	}
}

$str_subject_name_option = '';
$str_localities_options = '';

$subject_names_db = PDGManageCache::fn_load_cache('pdg_subject_name');
if(empty($subject_names_db)) {
	$str_subject_name_option = '<option>Data not available</option>';
} else {
	foreach($subject_names_db as $subject_names) {
		$str_subject_name_option.= '<option value="'.$subject_names->subject_name_id.'">'.$subject_names->subject_name.'</option>';
	}
}

$found_localities = PDGManageCache::fn_load_cache('pdg_locality');
foreach($found_localities as $found_locality) {
	$locality_name = $found_locality->locality;
	$locality_id = $found_locality->locality_id;
	$str_localities_options .= '<option value="'.$locality_id.'">'.$locality_name.'</option>';
}

?>
<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush">
                	
                </i>
                <br>
                <small>
                	
                </small>
            </h1>
            <hr />
            <div class="row">
            	<div class="col-md-8 col-md-offset-2">
            		<div class="row">
            			<div class="col-md-6">
            				<select id="select_pdg_report_type" class="form-control">
            					<option value="">Select a report type</option>
                                <option value="master_report">Master Report</option>
            					<option value="empty_localities" <?php echo $empty_localities_selected; ?>>Empty Localities</option>
            					<option value="empty_subjects" <?php echo $empty_subjects_selected; ?>>Empty Subjects</option>
            					<option value="empty_course">Empty Courses</option>
            					<option value="teachers_in_subjects">Teachers in Subjects</option>
            					<option value="reviews_reports">Reviews Reports</option>
            					<option value="locality_and_subject">Locality + Subjects</option>
            					<option value="subjects_not_in_locality">Subjects not available in Locality</option>
            					<option value="localities_not_having_subject">Localities not having Subject</option>
            					<option value="teacher_registration">Teacher Registration</option>
            					<option value="institute_registration">Institute Registration</option>
            					<option value="social_signups">Social Signups</option>
            					<option value="unapproved_teachers">Unapproved Teachers</option>
            					<option value="unapproved_institutes">Unapproved Institutes</option>
            				</select>
            			</div>
            			<div class="col-md-3">
            				<button class="btn btn-success col-md-12 cmd_generate_reports" data-loading-text="Generating reports...">Generate Report</button>
            			</div>
            			<div class="col-md-3">
            				<a class="btn btn-success col-md-12 cmd_generate_excfiles" href="#">Generate Excel File</a>
            			</div>
            		</div>
            		<hr />
            		<div class="row" id="div_locality_and_subject_list" style="display: none;">
            			<div class="col-md-6">            				
            				<select class="form-control select_control_with_title col-md-12" id="select_subject_for_report" title="Subject Name">
            					<option></option>
            					<?php echo $str_subject_name_option; ?>
            				</select>
            			</div>
            			<div class="col-md-6">            				
            				<select class="form-control select_control_with_title col-md-12" id="select_locality_for_report" title="Locality Name">
            					<option></option>
            					<?php echo $str_localities_options; ?>
            				</select>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
    
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2 class="report_title_area"><?php echo $check_title; ?></h2>
        </div>
        <!-- END Example Title -->
		<img class="center-block hidden img_reports_loader" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
		
		<div id="div_report_result_area"><?php echo $str_reports_content; ?></div>
		        
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->