<?php
	$pdg_teacher_seo_title =  ControlPanelSeo::fn_return_seo_options('teacher_title');
	$pdg_institute_seo_title = ControlPanelSeo::fn_return_seo_options('institute_title');
	
	$pdg_teacher_seo_meta =  ControlPanelSeo::fn_return_seo_options('teacher_meta');
	$pdg_institute_seo_meta = ControlPanelSeo::fn_return_seo_options('institute_meta');
?>

<div id="page-content">
	
   <!-- Block Tabs -->
    <div class="block full">
        <!-- Block Tabs Title -->
        <div class="block-title">
        	
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#tab_seo_options" data-toggle="tab">SEO Options</a></li>
                <li><a href="#tab_dynamic_pages_list" data-toggle="tab">Pages List</a></li>
                                
            </ul>
        </div>
        <!-- END Block Tabs Title -->

        <!-- Tabs Content -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_seo_options"><?php include_once('seo/seo_options.html.php'); ?></div>
            <div class="tab-pane" id="tab_dynamic_pages_list"><?php include_once('seo/pages_list.html.php'); ?></div>
        </div>
        <!-- END Tabs Content -->
    </div>
    <!-- END Block Tabs -->
</div>