<?php

$str_course_type_option='';	
if(isset($course_type_data)) {
	foreach($course_type_data as $course_type_row) {
		$course_type_id = $course_type_row->course_type_id;
		$course_type = $course_type_row->course_type;
		$str_course_type_option .= '
			<option value="'.$course_type_id.'">'.$course_type.'</option>
		';
	}
}

$str_subject_type_option='';	
if(isset($subject_type_data)) {
	foreach($subject_type_data as $subject_type_row) {
		//pedagoge_applog(print_r($subject_type_row,TRUE));
		$subject_type_id = $subject_type_row->subject_type_id;
		$subject_type = $subject_type_row->subject_type;
		$str_subject_type_option .= '
			<option value="'.$subject_type_id.'">'.$subject_type.'</option>
		';
	}		
}

$str_subject_name_option='';
if(isset($subject_name_data)) {
	foreach($subject_name_data as $subject_name_row) {
		$subject_name_id = $subject_name_row->subject_name_id;
		$subject_name = $subject_name_row->subject_name;
		$str_subject_name_option .= '
			<option value="'.$subject_name_id.'">'.$subject_name.'</option>
		';
	}
}

$str_academic_board_option='';	
if(isset($academic_board_data)) {
	foreach($academic_board_data as $academic_board_row) {
		$academic_board_id = $academic_board_row->academic_board_id;
		$academic_board = $academic_board_row->academic_board;
		$str_academic_board_option .= '
			<option value="'.$academic_board_id.'">'.$academic_board.'</option>
		';
	}
}

$str_course_age_option='';
if(isset($course_age_category_data)) {
	foreach($course_age_category_data as $course_age_category_row) {
		$course_age_category_id = $course_age_category_row->course_age_category_id;
		$course_age = $course_age_category_row->course_age;
		$str_course_age_option .= '
			<option value="'.$course_age_category_id.'">'.$course_age.'</option>
		';
	}
}
?>

<div class="block">		    
    <div class="block-title">
    	<div class="block-options pull-right">
            <button class="btn btn-alt btn-sm btn-default cmd_add_course" data-toggle="modal" data-target="#modal_create_course" title="Add a course"><i class="fa fa-plus"></i></button>
            <button class="btn btn-alt btn-sm btn-default cmd_refresh_course_list" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
        </div>
        <div class="block-options pull-right">
            <img class="center-block hidden img_course_list_loader" style="margin-top: 8px;" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
        </div>
        <h2><strong>Course List</strong> </h2>
    </div>
    <div class="">
    	<div class="row">
    		<div class="col-md-4">
    			<label for="select_fltr_subject_name">Subject Name</label>
    			<select id="select_fltr_subject_name" class="form-control col-md-12 fltr_course" title="Select Subject Name" data-column="1">
    				<option></option>
    				<?php echo $str_subject_name_option; ?>
    			</select>
    		</div>
    		<div class="col-md-4">
    			<label for="select_fltr_subject_type">Subject Type</label>
    			<select id="select_fltr_subject_type" class="form-control fltr_course" title="Select Subject Type" data-column="3">
    				<option></option>
    				<?php echo $str_subject_type_option; ?>
    			</select>
    		</div>
    		<div class="col-md-4">
    			<label for="select_fltr_course_type">Course Type</label>
    			<select id="select_fltr_course_type" class="form-control fltr_course" title="Select Course Type" data-column="5">
    				<option></option>
    				<?php echo $str_course_type_option; ?>
    			</select>
    		</div>
    		<div class="col-md-4">
    			<label for="select_fltr_academic_board">Academic Board</label>
    			<select id="select_fltr_academic_board" class="form-control fltr_course" title="Select Academic Board" data-column="7">
    				<option></option>
    				<?php echo $str_academic_board_option; ?>
    			</select>
    		</div>
    		<div class="col-md-4">
    			<label for="select_fltr_age">Age</label>
    			<select id="select_fltr_age" class="form-control fltr_course" title="Course Age" data-column="9">
    				<option></option>
    				<?php echo $str_course_age_option; ?>
    			</select>
    		</div>
    		<div class="col-md-4">
    			<br />
    			<!-- <button class="col-md-12 btn btn-warning cmd_reset_course_filter">Reset Filter</button> -->
    		</div>
    	</div>
    	<hr />
    	<div class="table-responsive">
	    	<table id="table_course_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		            	<th>ID</th>
		            	<th>subject_name_id</th>
		            	<th>Subject Name</th>
		            	<th>subject_type_id</th>
		            	<th>Subject Type</th>
		            	<th>course_type_id</th>
		            	<th>Course Type</th>
		            	<th>academic_board_id</th>
		            	<th>Board</th>
		            	<th>course_age_category_id</th>
		            	<th>Age</th>
		            	<th>Show</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<tr>
		        		<td colspan="12">Loading Data</td>
		        	</tr>
		        </tbody>			        
		    </table>
	    </div>
    </div>
    
</div>

<!-- Create course Modal -->
<div class="modal fade" id="modal_create_course" tabindex="-1" role="dialog" aria-labelledby="lbl_modal_create_course">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="lbl_modal_create_course">Create Course</h4>
			</div>
			<div class="modal-body">
				<!--
					1. select subject name
					2. Subject Type (Multiple)
					3. academic_board (multiple)
					4. course_age (multiple)  
				-->
				<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update')):?>
				<div class="row">
					<div class="col-md-3">
						<label for="select_create_course_subject_name">Select Subject Name</label>
					</div>
					<div class="col-md-8">						
						<select id="select_create_course_subject_name" class="form-control select_control_with_title" title="Subject Name">
							<option></option>
							<?php echo $str_subject_name_option; ?>
						</select>
					</div>
					<div class="col-md-1">
						<button class="btn btn-alt btn-sm btn-default cmd_add_subject_names" data-toggle="tooltip" title="Create Subject Name"><i class="fa fa-plus"></i></button>
					</div>
					<hr />
					<br />
					<div class="col-md-3">
						<label for="select_create_course_subject_type">Select Subject Type</label>
					</div>
					<div class="col-md-8">
						<select id="select_create_course_subject_type" class="form-control select_control_with_title_multiple" title="Subject Type" multiple="multiple">
							<?php echo $str_subject_type_option; ?>
						</select>
					</div>
					<div class="col-md-1">
						<button class="btn btn-alt btn-sm btn-default cmd_add_subject_type" data-toggle="tooltip" title="Create Subject Type"><i class="fa fa-plus"></i></button>
					</div>
					<hr />
					<br />
					<div class="col-md-6">
						<label for="select_create_course_academic_board">Select Academic Board</label>
						<select id="select_create_course_academic_board" class="form-control select_control_with_title_multiple" title="Academic Board" multiple="multiple">
							<?php echo $str_academic_board_option; ?>
						</select>
					</div>
					
					<div class="col-md-6">
						<label for="select_create_course_age">Select Course Age</label>
						<select id="select_create_course_age" class="form-control select_control_with_title_multiple col-md-12" title="Course Age" multiple="multiple">
							<?php echo $str_course_age_option; ?>
						</select>
					</div>
				</div>
				<div id="div_create_course_result_area"></div>
				<?php else: ?>
					<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
				<?php endif; ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
				<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update')):?>
				<button type="button" class="btn btn-primary cmd_save_new_course" data-loading-text="Loading..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Create Course</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<!-- update course Modal -->
<div class="modal fade" id="modal_update_course" tabindex="-1" role="dialog" aria-labelledby="lbl_modal_update_course">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="lbl_modal_update_course">Update Course</h4>
			</div>
			<div class="modal-body">
				<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update')):?>
				<div class="row">
					<div class="col-md-3">
						<label for="select_update_course_name">Select Subject Name</label>
					</div>
					<div class="col-md-8">						
						<select id="select_update_course_name" class="form-control select_course_update" title="Subject Name">
							<option></option>
							<?php echo $str_subject_name_option; ?>
						</select>
					</div>
					<div class="col-md-1">
						<button class="btn btn-alt btn-sm btn-default cmd_add_subject_names" data-toggle="tooltip" title="update Subject Name"><i class="fa fa-plus"></i></button>
					</div>
					<hr />
					<br />
					<div class="col-md-3">
						<label for="select_update_course_subject_type">Select Subject Type</label>
					</div>
					<div class="col-md-8">
						<select id="select_update_course_subject_type" class="form-control select_course_update" title="Subject Type">
							<option></option>
							<?php echo $str_subject_type_option; ?>
						</select>
					</div>
					<div class="col-md-1">
						<button class="btn btn-alt btn-sm btn-default cmd_add_subject_type" data-toggle="tooltip" title="update Subject Type"><i class="fa fa-plus"></i></button>
					</div>
					<hr />
					<br />
					<div class="col-md-4">
		    			<label for="select_update_course_type">Course Type</label>
		    			<select id="select_update_course_type" class="form-control select_course_update" title="Select Course Type">
		    				<option></option>
		    				<?php echo $str_course_type_option; ?>
		    			</select>
		    		</div>
					<div class="col-md-4">
						<label for="select_update_course_academic_board">Select Academic Board</label>
						<select id="select_update_course_academic_board" class="form-control select_course_update" title="Academic Board">
							<option></option>
							<?php echo $str_academic_board_option; ?>
						</select>
					</div>
					
					<div class="col-md-4">
						<label for="select_update_course_age">Select Course Age</label>
						<select id="select_update_course_age" class="form-control select_course_update col-md-12" title="Course Age">
							<option></option>
							<?php echo $str_course_age_option; ?>
						</select>
					</div>
				</div>
				<div id="div_update_course_result_area"></div>
				<input type="hidden" id="hidden_update_course_id"/>
				<?php else: ?>
					<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
				<?php endif; ?>
			</div>
			<div class="modal-footer">
				<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update')):?>
				<button type="button" class="btn btn-danger" data-loading-text="Deleting..." id="cmd_delete_course"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
				<?php endif; ?>
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
				<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update')):?>
				<button type="button" class="btn btn-primary cmd_save_updated_course" data-loading-text="Loading..."><i class="fa fa-floppy-o" aria-hidden="true"></i> update Course</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>