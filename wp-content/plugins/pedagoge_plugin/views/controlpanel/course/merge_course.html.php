<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_merge')):?>
<div class="block">
    
    <div class="block-title">
        <h2><strong>Select Courses for merging</strong> </h2>
    </div>
    
    <div class="table-responsive">
    	Course1
    	<input type="hidden" value="" id="hidden_merge_selected_course1"/>
    	<table class="table table-bordered" id="tbl_selected_course1">
    		<thead>
    			<tr>
    				<td>ID</td>
    				<td>Subject</td>
    				<td>Type</td>
    				<td>Course</td>
    				<td>Board</td>
    				<td>Age</td>
    			</tr>
    		</thead>
    		<tbody>
    			
    		</tbody>
    	</table>
    </div>
    
    <div class="table-responsive">
    	<input type="hidden" value="" id="hidden_merge_selected_course2"/>
    	Course2
    	<table class="table table-bordered" id="tbl_selected_course2">
    		<thead>
    			<tr>
    				<td>ID</td>
    				<td>Subject</td>
    				<td>Type</td>
    				<td>Course</td>
    				<td>Board</td>
    				<td>Age</td>
    			</tr>
    		</thead>
    		<tbody>
    			
    		</tbody>
    	</table>
    </div>
    <div class="row">
    	<div class="col-md-4"></div>
    	<div class="col-md-4">
    		<button class="btn btn-danger col-md-12" id="cmd_reset_merge_course"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
    	</div>
    	<div class="col-md-4">
    		<button class="btn btn-success col-md-12" id="cmd_merge_course" ><i class="fa fa-compress" aria-hidden="true"></i> Merge Course</button>
    	</div>
    </div>    
	<br />
	<div id="course_merge_result_area"></div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="block">		    
		    <div class="block-title">
		    	<div class="block-options pull-right">
		            <button class="btn btn-alt btn-sm btn-default cmd_refresh_course_list1" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
		        </div>		        
		        <h2><strong>Course List 1</strong> </h2>
		    </div>
		    <div class="row">
	    		<div class="col-md-6">
	    			<label for="select_merge_course_fltr_subject_name1">Subject Name</label>
	    			<select id="select_merge_course_fltr_subject_name1" class="form-control col-md-12 fltr_course_merge1" title="Subject Name" data-column="1">
	    				<option></option>
	    				<?php echo $str_subject_name_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-6">
	    			<label for="select_merge_course_fltr_subject_type1">Subject Type</label>
	    			<select id="select_merge_course_fltr_subject_type1" class="form-control fltr_course_merge1" title="Subject Type" data-column="3">
	    				<option></option>
	    				<?php echo $str_subject_type_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-4">
	    			<label for="select_merge_course_fltr_course_type1">Course Type</label>
	    			<select id="select_merge_course_fltr_course_type1" class="form-control fltr_course_merge1" title="Course Type" data-column="5">
	    				<option></option>
	    				<?php echo $str_course_type_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-4">
	    			<label for="select_merge_course_fltr_academic_board1">Academic Board</label>
	    			<select id="select_merge_course_fltr_academic_board1" class="form-control fltr_course_merge1" title="Academic Board" data-column="7">
	    				<option></option>
	    				<?php echo $str_academic_board_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-4">
	    			<label for="select_merge_course_fltr_age1">Age</label>
	    			<select id="select_merge_course_fltr_age1" class="form-control fltr_course_merge1" title="Age" data-column="9">
	    				<option></option>
	    				<?php echo $str_course_age_option; ?>
	    			</select>
	    		</div>	    		
	    	</div>
	    	<hr />
		    <div class="table-responsive">
		    	<table id="tbl_course_list1" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>ID</th>
			            	<th>subject_name_id</th>
			            	<th>Subject</th>
			            	<th>subject_type_id</th>
			            	<th>Type</th>
			            	<th>course_type_id</th>
			            	<th>Course</th>
			            	<th>academic_board_id</th>
			            	<th>Board</th>
			            	<th>course_age_category_id</th>
			            	<th>Age</th>
			            	<th>Show</th>			                
			            </tr>
			        </thead>
			                
			    </table>
		    </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="block">		    
		    <div class="block-title">
		    	<div class="block-options pull-right">                    
		            <button class="btn btn-alt btn-sm btn-default cmd_refresh_course_list2" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
		        </div>		        
		        <h2><strong>Course List 2</strong> </h2>
		    </div>
		    <div class="row">
	    		<div class="col-md-6">
	    			<label for="select_merge_course_fltr_subject_name2">Subject Name</label>
	    			<select id="select_merge_course_fltr_subject_name2" class="form-control col-md-12 fltr_course_merge2" title="Subject Name" data-column="1">
	    				<option></option>
	    				<?php echo $str_subject_name_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-6">
	    			<label for="select_merge_course_fltr_subject_type2">Subject Type</label>
	    			<select id="select_merge_course_fltr_subject_type2" class="form-control fltr_course_merge2" title="Subject Type" data-column="3">
	    				<option></option>
	    				<?php echo $str_subject_type_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-4">
	    			<label for="select_merge_course_fltr_course_type2">Course Type</label>
	    			<select id="select_merge_course_fltr_course_type2" class="form-control fltr_course_merge2" title="Course Type" data-column="5">
	    				<option></option>
	    				<?php echo $str_course_type_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-4">
	    			<label for="select_merge_course_fltr_academic_board2">Academic Board</label>
	    			<select id="select_merge_course_fltr_academic_board2" class="form-control fltr_course_merge2" title="Academic Board" data-column="7">
	    				<option></option>
	    				<?php echo $str_academic_board_option; ?>
	    			</select>
	    		</div>
	    		<div class="col-md-4">
	    			<label for="select_merge_course_fltr_age2">Age</label>
	    			<select id="select_merge_course_fltr_age2" class="form-control fltr_course_merge2" title="Age" data-column="9">
	    				<option></option>
	    				<?php echo $str_course_age_option; ?>
	    			</select>
	    		</div>	    		
	    	</div>
	    	<hr />
		    <div class="table-responsive">
		    	<table id="tbl_course_list2" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>ID</th>
			            	<th>subject_name_id</th>
			            	<th>Subject</th>
			            	<th>subject_type_id</th>
			            	<th>Type</th>
			            	<th>course_type_id</th>
			            	<th>Course</th>
			            	<th>academic_board_id</th>
			            	<th>Board</th>
			            	<th>course_age_category_id</th>
			            	<th>Age</th>
			            	<th>Show</th>		                
			            </tr>
			        </thead>
			        
			    </table>
		    </div>
		</div>
	</div>
</div>

<?php else: ?>
	<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
<?php endif; ?>


