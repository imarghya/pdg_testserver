<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_merge')):?>
<div class="block">
    
    <div class="block-title">
        <h2><strong>Select Subjects for merging</strong> </h2>
    </div>
    
    <div class="row">
    	<div class="col-md-1"><label for="txt_selected_subject_1">Subject 1</label></div>
    	<div class="col-md-3">
    		<input type="hidden" id="hidden_merge_selected_subject_name1_id"/>
    		<input type="text" class="form-control" id="txt_selected_subject_1" readonly="readonly" />			
    	</div>
    	<div class="col-md-1"><label for="txt_selected_subject_2">Subject 2</label></div>
    	<div class="col-md-3">
    		<input type="hidden" id="hidden_merge_selected_subject_name2_id"/>
    		<input type="text" class="form-control" id = "txt_selected_subject_2" readonly="readonly"/>
    	</div>
    	<div class="col-md-2">
    		<button class="btn btn-success cmd_merge_subjects col-md-12" data-text-loading="Merging subjects..."><i class="fa fa-compress" aria-hidden="true"></i> Merge</button>
    	</div>
    	<div class="col-md-2">
    		<button class="btn btn-warning cmd_reset_selected_subjects col-md-12" ><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
    	</div>
    </div>
	<br />
	<div id="subject_merge_result_area"></div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="block">		    
		    <div class="block-title">
		    	<div class="block-options pull-right">
		            <button class="btn btn-alt btn-sm btn-default cmd_refresh_subject_list1" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
		        </div>
		        <div class="block-options pull-right">
		            <img class="center-block hidden img_subjectlist1_loader" style="margin-top: 8px;" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
		        </div>
		        <h2><strong>Subject Names list 1</strong> </h2>
		    </div>
		    <div class="div_content_subject_list1">
		    	<table id="subject_list1" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>ID</th>
			                <th>Subject name</th>
			                <th>Select</th>			                
			            </tr>
			        </thead>			        
			    </table>
		    </div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="block">		    
		    <div class="block-title">
		    	<div class="block-options pull-right">                    
		            <button class="btn btn-alt btn-sm btn-default cmd_refresh_subject_list2" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
		        </div>
		        <div class="block-options pull-right">
		            <img class="center-block hidden img_subjectlist2_loader" style="margin-top: 8px;" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
		        </div>
		        <h2><strong>Subject Names list 2</strong> </h2>
		    </div>
		    <div class="div_content_subject_list2">
		    	<table id="subject_list2" class="table table-striped table-bordered" cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>ID</th>
			                <th>Subject name</th>
			                <th>Select</th>			                
			            </tr>
			        </thead>			        
			    </table>
		    </div>
		</div>
	</div>
</div>

<?php else: ?>
	<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
<?php endif; ?>


