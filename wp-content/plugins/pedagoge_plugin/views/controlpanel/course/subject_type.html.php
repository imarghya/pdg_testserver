<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_type')):?>
<div class="block">
    <div class="block-title">
    	<div class="block-options pull-right">
    		<button class="btn btn-alt btn-sm btn-default cmd_add_subject_type" data-toggle="tooltip" title="Add Subject Type"><i class="fa fa-plus"></i></button>
            <button class="btn btn-alt btn-sm btn-default cmd_refresh_subject_type" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
        </div>
        <div class="block-options pull-right">
            <img class="center-block hidden img_subject_type_list_loader" style="margin-top: 8px;" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
        </div>
        <h2><strong>Subject Types</strong> </h2>
    </div>
    <div class="">
    	<table id="table_subject_types" class="table table-striped table-bordered" cellspacing="0" width="100%">
	        <thead>
	            <tr>
	                <th>ID</th>
	                <th>Subject Type</th>
	                <th>Select</th>			                
	            </tr>
	        </thead>
	        <tbody>
	        	<tr>
	        		<td colspan="3">Loading Data</td>
	        	</tr>
	        </tbody>
	    </table>
    </div>
</div>
<?php else: ?>
	<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
<?php endif; ?>