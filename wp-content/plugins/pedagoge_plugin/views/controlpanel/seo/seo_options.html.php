<div class="well">
	<h3>SEO template phrases should contain {{{subject}}} and {{{city}}}</h3>
	Note : Do not change the phrases too frequently!
	<hr />
	<div class="row">
		<div class="col-md-6">
			<label for="txt_teacher_title_tag">Teacher title template</label>
			<input type="text" value="<?php echo $pdg_teacher_seo_title; ?>" id="txt_teacher_title_tag" class="form-control"/>
		</div>
		<div class="col-md-6">
			<label for="txt_institute_title_tag">Institute title template</label>
			<input type="text" value="<?php echo $pdg_institute_seo_title; ?>" id="txt_institute_title_tag" class="form-control"/>        			
		</div>
		<div class="col-md-6">
			<label for="txt_teacher_meta_tag">Teacher meta tag template</label>
			<textarea class="form-control" id="txt_teacher_meta_tag"><?php echo $pdg_teacher_seo_meta; ?></textarea>
		</div>
		<div class="col-md-6">
			<label for="txt_institute_meta_tag">Institute meta tag template</label>
			<textarea class="form-control" id="txt_institute_meta_tag"><?php echo $pdg_institute_seo_meta; ?></textarea>        			
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-4">
			<!-- <button class="btn col-md-12 btn-success" data-loading-text="Saving Options..." id="cmd_update_seo_urls"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update all URLs</button> -->
			
		</div>		
		<div class="col-md-4"></div>
		<div class="col-md-4"><button class="btn col-md-12 btn-success" data-loading-text="Saving Options..." id="cmd_save_seo_options"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button></div>
	</div>
	<div class="row">
		<div class="col-md-12" id="div_seo_update_result_area"></div>
	</div>
</div>
<div class="well">
	<h3>Generate the combination and create SEO URLS</h3>
	<p>Note: Combination generation and URL creation is a time consuming process... please have some patience. You may also have to redo the steps multiple times.</p>
	<div class="row">
		<div class="col-md-4">
			<button class="btn btn-info col-md-12" data-loading-text="Generating combination..." id="cmd_generate_seo_combination">Create Combination</button>
		</div>
		<div class="col-md-4">
			<button class="btn col-md-12 btn-success" data-loading-text="Saving Options..." id="cmd_update_subject_slugs"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update Subjects Slug</button>
		</div>
		<div class="col-md-4">
			<button class="btn btn-info col-md-12" data-loading-text="Deleting URL cache..." id="cmd_delete_seo_urls_cache">Update Cache</button>
		</div>		
	</div>
	<hr />
	<div class="row"><div class="col-md-12 div_seo_generate_url_result_area"></div></div>
</div>