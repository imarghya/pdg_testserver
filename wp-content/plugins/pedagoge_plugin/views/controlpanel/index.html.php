<?php
$total_users = 0;
$total_recommendations = 0;
$total_reviews = 0;
$total_mails = 0;
$total_institutes = 0;
$total_teachers = 0;
$total_guardians = 0;
$total_students = 0;

$empty_localities = 0;
$empty_subjects = 0;

if(isset($stats_data)) {
	
	foreach($stats_data as $stats_info) {
		$total_users = $stats_info->total_users;
		$total_recommendations = $stats_info->total_recommendations;
		$total_reviews = $stats_info->total_reviews;
		$total_mails = $stats_info->total_mails;
		$total_institutes = $stats_info->total_institutes;
		$total_teachers = $stats_info->total_teachers;
		$total_guardians = $stats_info->total_guardians;
		$total_students = $stats_info->total_students;
		$empty_localities = $stats_info->empty_localities;
		$empty_subjects = $stats_info->empty_subjects;
	}
}


$reports_page = home_url('/dashboard/?page=reports');
$empty_localities_report_url = add_query_arg( array('report_type'=>'empty_localities'), $reports_page);
$empty_subjects_report_url = add_query_arg( array('report_type'=>'empty_subjects'), $reports_page);

?>


<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                    <h1>Welcome <strong>Admin</strong><br><small>You Look Awesome!</small></h1>
                </div>
                <!-- END Main Title -->

                <!-- Top Stats -->
                <div class="col-md-8 col-lg-6">
                    <div class="row text-center">
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong><?php echo $total_recommendations; ?></strong><br>
                                <small><i class="fa fa-thumbs-o-up"></i> Likes</small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong><?php echo $total_reviews; ?></strong><br>
                                <small><i class="fa fa-heart-o"></i> Reviews</small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong><?php echo $total_mails; ?></strong><br>
                                <small><i class="fa fa-envelope-o"></i> Mails</small>
                            </h2>
                        </div>
                        <!-- We hide the last stat to fit the other 3 on small devices -->
                        <div class="col-sm-3 hidden-xs">
                            <h2 class="animation-hatch">
                                <strong><?php echo $total_users; ?></strong><br>
                                <small><i class="fa fa-users"></i> Members</small>
                            </h2>
                        </div>
                    </div>
                </div>
                <!-- END Top Stats -->
            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="<?php echo PEDAGOGE_CP_ASSETS_URL; ?>/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="#" class="widget widget-hover-effect1 not_clickable">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-user-secret"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong><?php echo $total_teachers; ?></strong><br>
                        <small>Approved Teachers</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="#" class="widget widget-hover-effect1 not_clickable">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="fa fa-university"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong><?php echo $total_institutes; ?></strong><br>
                        <small>Approved Institutes</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="#" class="widget widget-hover-effect1 not_clickable">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="fa fa-graduation-cap"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong><?php echo $total_students; ?></strong>
                        <small>Students</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="#" class="widget widget-hover-effect1 not_clickable">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="fa fa-female"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong><?php echo $total_guardians; ?></strong>
                        <small>Guardians</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="<?php echo $empty_localities_report_url; ?>" class="widget widget-hover-effect1" target="_blank">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong><?php echo $empty_localities; ?></strong>
                        <small>Empty Localities</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="<?php echo $empty_subjects_report_url; ?>" class="widget widget-hover-effect1" target="_blank">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="fa fa-book"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong><?php echo $empty_subjects; ?></strong>
                        <small>Empty Subjects</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        
        <div class="col-md-6">
            
            <!-- Charts Widget -->
            <div class="widget">
                <div class="widget-advanced widget-advanced-alt">
                    <!-- Widget Header -->
                    <div class="widget-header text-center themed-background">
                        <h3 class="widget-content-light text-left pull-left animation-pullDown">
                            <strong>Sales</strong> &amp; <strong>Earnings</strong><br>
                            <small>Last Year</small>
                        </h3>
                        <!-- Flot Charts (initialized in js/pages/index.js), for more examples you can check out http://www.flotcharts.org/ -->
                        <div id="dash-widget-chart" class="chart"></div>
                    </div>
                    <!-- END Widget Header -->

                    <!-- Widget Main -->
                    <div class="widget-main">
                        <div class="row text-center">
                            <div class="col-xs-4">
                                <h3 class="animation-hatch"><strong>7.500</strong><br><small>Clients</small></h3>
                            </div>
                            <div class="col-xs-4">
                                <h3 class="animation-hatch"><strong>10.970</strong><br><small>Sales</small></h3>
                            </div>
                            <div class="col-xs-4">
                                <h3 class="animation-hatch">$<strong>31.230</strong><br><small>Earnings</small></h3>
                            </div>
                        </div>
                    </div>
                    <!-- END Widget Main -->
                </div>
            </div>
            <!-- END Charts Widget -->            
        </div>
    </div>
    <!-- END Mini Top Stats Row -->
</div>
<!-- END Page Content -->