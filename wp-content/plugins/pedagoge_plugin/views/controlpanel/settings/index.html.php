<div id="page-content">
	
   <!-- Block Tabs -->
    <div class="block full">
        <!-- Block Tabs Title -->
        <div class="block-title">
        	
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#tab_system_settings" data-toggle="tab"><i class="fa fa-cogs" aria-hidden="true"></i> System Settings</a></li>
                <li><a href="#tab_emails_mgmt" data-toggle="tab"><i class="fa fa-envelope-o" aria-hidden="true"></i> Emails Management</a></li>
                <!-- <li><a href="#tab_roles_responsbilities" data-toggle="tab"><i class="fa fa-users" aria-hidden="true"></i> User Roles and Responsbilities</a></li> -->
                <li><a href="#tab_documents_manager" data-toggle="tab"><i class="fa fa-users" aria-hidden="true"></i> Documents Manager</a></li>
            </ul>
            
        </div>
        <!-- END Block Tabs Title -->

        <!-- Tabs Content -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_system_settings"><?php require_once('system_settings.html.php'); ?></div>
            <div class="tab-pane" id="tab_emails_mgmt"><?php require_once('email_settings.html.php'); ?></div>
            <!-- <div class="tab-pane" id="tab_roles_responsbilities">Test3</div> -->
            <div class="tab-pane" id="tab_documents_manager"><?php require_once('documents_manager.html.php'); ?></div>
        </div>
        <!-- END Tabs Content -->
    </div>
    <!-- END Block Tabs -->	
</div>
