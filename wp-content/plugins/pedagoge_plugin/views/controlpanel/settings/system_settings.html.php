<?php

$default_sender_email = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_default_email_sender');
if($default_sender_email == FALSE) {
	$default_sender_email = '';
}
$str_minification_enabled = '';
$str_email_queue_checked = '';
$str_email_async_checked = '';
$str_email_logs = '';
$minification_enabled = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_minification_enabled');
$email_queue = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_email_queue');
$email_async = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_email_async');
$pdg_email_logs = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_email_log');

if($minification_enabled == 'yes') {
	$str_minification_enabled = 'checked';
}

if($email_queue == 'yes') {
	$str_email_queue_checked = 'checked';
}

if($email_async == 'yes') {
	$str_email_async_checked = 'checked';
}
if($pdg_email_logs == 'yes') {
	$str_email_logs = 'checked';
}
?>
<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_settings_main')):?>
	<div class="row">
		<div class="col-md-8">		
			
			<div class="block">
				<!-- Block Title -->
				<div class="block-title">
			        <h2><strong>Cache Management System</strong></h2>
			    </div>
			    <div class="row">
			    	<div class="col-md-6">
			    		<button class="btn btn-info col-md-12" id="cmd_clear_data_cache" data-loading-text="Clearing Data cache..."><i class="fa fa-refresh" aria-hidden="true"></i> Clear Data Cache</button>
			    	</div>
			    	<div class="col-md-6">
			    		<button class="btn btn-info col-md-12" id="cmd_clear_scripts_cache" data-loading-text="Clearing scripts cache..."><i class="fa fa-refresh" aria-hidden="true"></i> Clear Scripts Cache</button> 
			    	</div>		    	
			    </div>
			    <br />		       
			</div>
			
		</div>
		
		<div class="col-md-4">
			
			<div class="block">
				<!-- Block Title -->
				<div class="block-title">
			        <h2><strong>CSS/JS Minification</strong></h2>
			    </div>
			    <div class="checkbox">
	                <label for="chk_enable_minification">
	                    <input type="checkbox" id="chk_enable_minification" name="chk_enable_minification" <?php echo $str_minification_enabled; ?> /> Minification enabled?
	                </label>
	            </div>
			    <br />
			</div>
		</div>	
	</div>
	
	<div class="block">
		<!-- Block Title -->
		<div class="block-title">
	        <h2><strong>Email Settings</strong></h2>
	    </div>
	    
		<div class="row">
			<div class="col-md-3">
				<label for="txt_default_email_sender">Default Sender</label>    				
				<input type="text" class="form-control" value="<?php echo $default_sender_email; ?>" id="txt_default_email_sender"/>
			</div>
			<div class="col-md-3">
				<br />
				<div class="checkbox">
	                <label for="chk_enable_email_queue">
	                    <input type="checkbox" id="chk_enable_email_queue" <?php echo $str_email_queue_checked; ?> /> Email Queue enabled?
	                </label>
	            </div>
			</div>
			<div class="col-md-3">
				<br />
				<div class="checkbox">
	                <label for="chk_enable_asynchronous_emails">
	                    <input type="checkbox" id="chk_enable_asynchronous_emails" <?php echo $str_email_async_checked; ?> /> Send Emails Asynchronously?
	                </label>
	            </div>
			</div>
			<div class="col-md-3">
				<br />
				<div class="checkbox">
	                <label for="chk_log_emails">
	                    <input type="checkbox" id="chk_log_emails" <?php echo $str_email_logs; ?> /> Log Emails?
	                </label>
	            </div>
			</div>	
			
		</div>
		<div class="row">
			<div class="col-md-8"></div>
			<div class="col-md-4">
				<button class="btn btn-info col-md-12" id="cmd_update_default_sender_email"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
			</div>
		</div>
	</div>
	
	<div class="block">
		<!-- Block Title -->
		<div class="block-title">
	        <h2><strong>Reviews Settings</strong></h2>
	    </div>	    
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-info col-md-12" id="cmd_fix_reviews_average" data-loading-text="Fixing..."><i class="fa fa-wrench" aria-hidden="true"></i> Fix Reviews Average</button>
			</div>
			<div class="col-md-6">
				<button class="btn btn-info col-md-12" id="cmd_fetch_duplicate_reviews" data-loading-text="Fetching Duplicates..."><i class="fa fa-chain-broken" aria-hidden="true"></i> Fetch Duplicate Reviews</button>
			</div>
		</div>
		<hr />
		<div id="div_duplicate_reviews_result_area"></div>
	</div>
<?php else: ?>
	<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
<?php endif; ?>