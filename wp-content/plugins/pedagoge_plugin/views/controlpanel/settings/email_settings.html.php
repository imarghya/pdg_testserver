<?php
$str_email_types_option = '';
if(isset($email_types_data)) {
	foreach($email_types_data as $email_type) {
		$str_email_types_option .= '
			<option value="'.$email_type->email_settings_id.'">'.$email_type->email_label.'</option>
		';
	}
}
?>
<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_settiings_mail')):?>
<div class="panel panel-default">
	<div class="panel-heading">Create Mail Type</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2"><label for="txt_create_mail_type_label">Mail Type Label</label></div>
			<div class="col-md-7">				
				<input type="text" class="form-control" id="txt_create_mail_type_label"/>				
			</div>
			<div class="col-md-3">
				<button class="btn btn-info col-md-12" id="cmd_create_mail_type" button-text-loading="Creating...">
					<i class="fa fa-plus-square-o" aria-hidden="true"></i> Create Mail Type
				</button>
			</div>
		</div>		
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Update Mail Type</div>
	<div class="panel-body">
		
		<div class="row">
			<div class="col-md-2">
				<img class="center-block hidden_item" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" id="img_mail_type_loader" />
			</div>
			<div class="col-md-2">
				<label for="select_mail_type">Select Mail Type</label>
			</div>
			<div class="col-md-8">
				<select id="select_mail_type" class="form-control">
					<option value="">Select a Mail Type</option>
					<?php echo $str_email_types_option; ?>
				</select>
			</div>
		</div>
		<hr />		
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<label for="txt_mail_type_name">Mail Type Name</label>
						<input type="text" class="form-control" id="txt_mail_type_name" readonly="readonly" />
					</div>
					<div class="col-md-6">
						<label for="txt_mail_type_label">Mail Type Label</label>
						<input type="text" class="form-control" id="txt_mail_type_label" />
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2"><label for="txt_email_subject">Email Subject</label></div>
					<div class="col-md-10">
						<input type="text" class="form-control" id="txt_email_subject" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label for="txt_mail_type_sender_email">Sender Email</label></div>
					<div class="col-md-5">
						<input type="text" class="form-control" id="txt_mail_type_sender_email" />
					</div>
					<div class="col-md-3">
						<div class="checkbox">
			                <label for="chk_send_to_subscriber">
			                    <input type="checkbox" id="chk_send_to_subscriber" /> Send to subscriber?
			                </label>
			            </div>
					</div>
					<div class="col-md-2">
						<div class="checkbox">
			                <label for="chk_send_to_user">
			                    <input type="checkbox" id="chk_send_to_user" /> Send to user?
			                </label>
			            </div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">							
							<div class="col-md-9">
								<label for="txt_subscriber_email_address">List of Subscribers Email Address</label>
								<input type="text" class="form-control" id="txt_subscriber_email_address"/>
							</div>
							<div class="col-md-3">
								<br />
								<button class="btn btn-success col-md-12" id="cmd_add_subscriber_email" data-loading-text="Saving and Loading..."><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Email</button>
							</div>
						</div>
						<br />
						<div class="well" id="div_list_subscribers">
							Please input Subscriber email address
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<label for="select_email_sending_priority">Email priority : </label>
							</div>
							<div class="col-md-8">
								<select class="form-control" id="select_email_sending_priority">
									<option value="immediate">Send Immediately</option>
									<option value="queue">Push to queue</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<button class="btn btn-warning col-md-12" id="cmd_email_template_editor" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Template</button>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success col-md-12" id="cmd_update_email_settings" data-loading-text="Updating settings..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Update Settings</button>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<?php else: ?>
	<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
<?php endif; ?>

<!-- update course Modal -->
<div class="modal fade" id="modal_update_email_template" tabindex="-1" role="dialog" aria-labelledby="lbl_modal_update_email_template">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="lbl_modal_update_email_template">Update Email Template</h4>
			</div>
			<div class="modal-body">
				<strong>Template Name: <span id="span_email_template_name"></span></strong>
				<pre id="editor" style="height:406px;">This is a test</pre>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" id="cmd_delete_email_template" data-loading-text="Deleting..."><i class="fa fa-trash-o" aria-hidden="true"></i> Delete Template</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>				
				<button type="button" class="btn btn-primary" id="cmd_update_email_template" data-loading-text="Loading..."><i class="fa fa-floppy-o" aria-hidden="true"></i> Update Template</button>
				
			</div>
		</div>
	</div>
</div>