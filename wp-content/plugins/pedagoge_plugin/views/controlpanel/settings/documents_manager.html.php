<?php if(current_user_can('manage_options') || current_user_can('pdg_cap_cp_settings_documents')):?>

<div class="row">
	<div class="col-md-3">
		<label for="select_fltr_document_role_name">Role Type</label>
		<select id="select_fltr_document_role_name" class="form-control col-md-12" title="Select User Role" data-column="3">
			<option></option>
			<option value="teacher">Teacher</option>
			<option value="institution">Institute</option>
		</select>
	</div>
	<div class="col-md-3">
		<label for="txt_doument_teacher_institute_name">Teacher/Institute Name</label>
		<input type="text" class="form-control txt_fltr_documents" id="txt_doument_teacher_institute_name" data-column="2"/>
	</div>	
	<div class="col-md-3">
		<label for="txt_doument_name">Document Name</label>
		<input type="text" class="form-control txt_fltr_documents" id="txt_doument_name" data-column="5"/>
	</div>
	<div class="col-md-3">
		<label for="txt_doument_title">Document Title</label>
		<input type="text" class="form-control txt_fltr_documents" id="txt_doument_title" data-column="7"/>
	</div>
</div>
<hr />
<div class="table-responsive">
	<table id="table_documents_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
            	<th>File ID</th>       
            	<th>user_id</th>
            	<th>Teacher Name</th>
            	<th>role_name</th>
            	<th>Role</th>
            	<th>File Name</th>
            	<th>new_file_name</th>
            	<th>Title</th>
            	<th>created</th>
            	<th>Action</th>            	
            </tr>
        </thead>
        <tbody>
        	<tr>
        		<td colspan="10">Loading Data</td>
        	</tr>
        </tbody>			        
    </table>
</div>
<input type="hidden" value="<?php echo PEDAGOGE_PLUGIN_URL; ?>/storage/uploads/documents/" id="hidden_document_manager_path"/>
<?php else: ?>
	<div class="alert alert-error">Sorry! You do not have the permission to view this Information!</div>
<?php endif; ?>