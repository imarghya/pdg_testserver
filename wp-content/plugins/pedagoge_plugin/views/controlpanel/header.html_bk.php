<?php
	$control_panel_url = home_url('/dashboard');
?>

<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $this -> title; ?></title>
        <meta name="author" content="pedagoge">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        
        <?php echo $this -> load_view('header_scripts'); ?>
        
    </head>
    <body>
        <!-- Page Wrapper -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!--
            Available classes:

            'page-loading'      enables page preloader
        -->
        <div id="page-wrapper">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
                </div>
            </div>
            
            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations footer-fixed ">
                

                <!-- Main Sidebar -->
                <div id="sidebar">
                    <!-- Wrapper for scrolling functionality -->
                    <div id="sidebar-scroll">
                        <!-- Sidebar Content -->
                        <div class="sidebar-content">
                            <!-- Brand -->
                            <a href="<?php echo $control_panel_url; ?>" class="sidebar-brand">
                                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Pedagoge</strong> CP</span>
                            </a>
                            <!-- END Brand -->

                            <!-- User Info -->
                            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                                <div class="sidebar-user-avatar">
                                    <a href="#" class="not_clickable">
                                        <img src="<?php echo PEDAGOGE_CP_ASSETS_URL; ?>/img/placeholders/avatars/avatar2.jpg" alt="avatar">
                                    </a>
                                </div>
                                <div class="sidebar-user-name"><?php echo $this -> app_data['pdg_current_user'] -> user_login; ?></div>
                                <div class="sidebar-user-links">
                                    <a href="#" class="not_clickable" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                                    <a href="#" class="not_clickable" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                    <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                                    <a href="<?php echo wp_logout_url(); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                                </div>
                            </div>
                            <!-- END User Info -->

                          	

                            <!-- Sidebar Navigation -->
                            <ul class="sidebar-nav">
                                <?php include_once('cp_menu.html.php'); ?>
                            </ul>
                            <!-- END Sidebar Navigation -->

                            <!-- Sidebar Notifications -->
                            <div class="sidebar-header sidebar-nav-mini-hide">
                                <span class="sidebar-header-options clearfix">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
                                </span>
                                <span class="sidebar-header-title">Activity</span>
                            </div>
                            <div class="sidebar-section sidebar-nav-mini-hide">
                                <div class="alert alert-success alert-alt">
                                    <small>5 min ago</small><br>
                                    <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                </div>
                                <div class="alert alert-info alert-alt">
                                    <small>10 min ago</small><br>
                                    <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                </div>
                                <div class="alert alert-warning alert-alt">
                                    <small>3 hours ago</small><br>
                                    <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                </div>
                                <div class="alert alert-danger alert-alt">
                                    <small>Yesterday</small><br>
                                    <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)"><strong>New bug submitted</strong></a>
                                </div>
                            </div>
                            <!-- END Sidebar Notifications -->
                        </div>
                        <!-- END Sidebar Content -->
                    </div>
                    <!-- END Wrapper for scrolling functionality -->
                </div>
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    <!-- Header -->
                    
                    <header class="navbar navbar-default">
                        <!-- Left Header Navigation -->
                        <ul class="nav navbar-nav-custom admin_top_navigation">
                            <!-- Main Sidebar Toggle Button -->
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-bars fa-fw"></i>
                                </a>
                            </li>
                            <!-- END Main Sidebar Toggle Button -->

                            <!-- Template Options -->
                            <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="gi gi-settings"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-options">
                                    <li class="dropdown-header text-center">Header Style</li>
                                    <li>
                                        <div class="btn-group btn-group-justified btn-group-sm">
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                                        </div>
                                    </li>
                                    <li class="dropdown-header text-center">Page Style</li>
                                    <li>
                                        <div class="btn-group btn-group-justified btn-group-sm">
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <li>
                                <h4><?php echo $this->title; ?></h4>
                                      
				
                            </li>
                            <!-- END Template Options -->
			    <?php if(isset($_GET['page']) && $_GET['page']=='queries'){ ?>
			     <!--Start Queries Top menu-->
			    <li>
				<form class="navbar-form inline_block query_search" role="search">
					<div class="input-group blue_border border_radius_30 overflow_hidden">
					   <input type="text" class="form-control border_none" placeholder="Search" name="srch-term" id="srch-term">
					   <div class="input-group-btn">
					      <button class="btn blue_background color_white" type="submit"><i class="glyphicon glyphicon-search"></i></button>
					   </div>
					</div>
				</form>
			    </li>
			    <li>
				   <ul class="add_edit_icon">
				      <li class="two_box"><a href="#" title="New Query"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
				      <li class="disabled" id="query_edit_btn"><a href="#" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
				      <li class="two_box disabled" id="query_clone_btn"><a href="#" title="Clone"><i class="fa fa-clone" aria-hidden="true" style="opacity:0;"></i></a></li>
				      <li class="disabled" id="query_approve_btn"><a href="#" id="query_approve" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a></li>
				      <li class="disabled" id="query_disapprove_btn"><a href="#"  data-toggle="modal" data-target="#modal_query_disapprove" title="Disapprove"><i class="fa fa-times" aria-hidden="true"></i></a></li>
				   </ul>
				</li>
				<li>
				   <select id="sales_rep" name="assign" class="text-uppercase text-center assign apperence_none">
				       <option value="">Assign</option>
				       <?php
				       $sales_rep_model = new ModelSalesRep();
				       $sales_reps = $sales_rep_model->get_sales_rep();
				       foreach($sales_reps AS $sales_rep ){
				       ?>
				       <option value="<?php echo $sales_rep->ID; ?>"><?php echo $sales_rep->display_name; ?></option>
				       <?php } ?>
				     </select>
				</li>
				 <li class="dropdown">
				<a href="#" class="dropdown-toggle text-uppercase blue_color new_admin_navigation" data-toggle="dropdown"> New <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></a>
				<ul class="dropdown-menu">
				   <li>
				      <a href="#">New</a>
				   </li>
				   <li>
				      <a href="#">Assigned</a>
				   </li>
				   <li>
				      <a href="#">Pending for Approval</a>
				   </li>
				</ul>
			     </li>
			    <!--End Queries Top menu-->
			    <?php } ?>
                        </ul>
			<!--Start Queries Top menu-->
                        <!-- END Left Header Navigation -->

                        

                        <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
                            
                            <!-- END Alternative Sidebar Toggle Button -->

                            <!-- User Dropdown -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo PEDAGOGE_CP_ASSETS_URL; ?>/img/placeholders/avatars/avatar2.jpg" alt="avatar"> <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li class="dropdown-header text-center">Account</li>
                                    
                                    <li>
                                        <a href="#" class="not_clickable">
                                            <i class="fa fa-user fa-fw pull-right"></i>
                                            Profile
                                        </a>
                                        <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                        <a href="#modal-user-settings" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Settings
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#" class="not_clickable"><i class="fa fa-lock fa-fw pull-right"></i> Lock Account</a>
                                        <a href="<?php echo wp_logout_url(); ?>"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                                    </li>
                                    <li class="dropdown-header text-center">Activity</li>
                                    <li>
                                        <div class="alert alert-success alert-alt">
                                            <small>5 min ago</small><br>
                                            <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                        </div>
                                        <div class="alert alert-info alert-alt">
                                            <small>10 min ago</small><br>
                                            <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                        </div>
                                        <div class="alert alert-warning alert-alt">
                                            <small>3 hours ago</small><br>
                                            <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                        </div>
                                        <div class="alert alert-danger alert-alt">
                                            <small>Yesterday</small><br>
                                            <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)" class="alert-link">New bug submitted</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- END User Dropdown -->
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    <!-- END Header -->