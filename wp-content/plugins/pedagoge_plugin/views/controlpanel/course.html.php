<div id="page-content">
	
   <!-- Block Tabs -->
    <div class="block full">
        <!-- Block Tabs Title -->
        <div class="block-title">
        	
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#tab_subject_names" data-toggle="tab">Subject Names</a></li>
                <li><a href="#tab_subject_types" data-toggle="tab">Subject Types</a></li>
                <li><a href="#tab_course_list" data-toggle="tab">Course List</a></li>
                <li><a href="#tab_merge_subjects" data-toggle="tab">Merge Subjects</a></li>
                <li><a href="#tab_merge_courses" data-toggle="tab">Merge Courses</a></li>                
            </ul>
        </div>
        <!-- END Block Tabs Title -->

        <!-- Tabs Content -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_subject_names"><?php include_once('course/subject_name.html.php'); ?></div>
            <div class="tab-pane" id="tab_subject_types"><?php include_once('course/subject_type.html.php'); ?></div>
            <div class="tab-pane" id="tab_course_list"><?php include_once('course/course_list.html.php'); ?></div>
            <div class="tab-pane" id="tab_merge_subjects"><?php include_once('course/merge_subjects.html.php'); ?></div>
            <div class="tab-pane" id="tab_merge_courses"><?php include_once('course/merge_course.html.php'); ?></div>
        </div>
        <!-- END Tabs Content -->
    </div>
    <!-- END Block Tabs -->
	<?php echo $this->load_view('modals/single_table_cud_modal'); ?>  
	
</div>
