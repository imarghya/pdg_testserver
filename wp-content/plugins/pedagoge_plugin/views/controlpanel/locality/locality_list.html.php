<?php
	$str_city = '';
	if(isset($pdg_city)) {
		foreach($pdg_city as $city) {
			$str_city .= '
				<option value="'.$city->city_id.'">'.$city->city_name.'</option>
			';
		}
	}
?>

<div class="block">		    
    <div class="block-title">
    	<div class="block-options pull-right">
            <button class="btn btn-alt btn-sm btn-default cmd_create_locality" data-toggle="modal" data-target="#modal_locality_updates" title="Add a Locality"><i class="fa fa-plus"></i></button>
            <button class="btn btn-alt btn-sm btn-default cmd_refresh_locality_list" data-toggle="tooltip" title="Reload"><i class="fa fa-refresh"></i></button>
        </div>
        <div class="block-options pull-right">
            <img class="center-block hidden img_locality_list_loader" style="margin-top: 8px;" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif" alt="" />
        </div>
        <h2><strong>Locality List</strong> </h2>
    </div>
    <div class="">
    	<div class="row">
    		<div class="col-md-4">
    			<label for="select_fltr_city_name">City Name</label>
    			<select id="select_fltr_city_name" class="form-control col-md-12 fltr_locality select_control_with_title" title="Select City Name" data-column="5">
    				<option></option>
    				<?= $str_city; ?>
    			</select>
    		</div>
    		<div class="col-md-8">
    			<label for="txt_fltr_locality_name">Locality Name</label>
    			<input type="text" id="txt_fltr_locality_name" class="form-control"  data-column="7" />
    		</div>
    	</div>
    	<hr />
    	<div class="table-responsive">
	    	<table id="table_locality_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
		        <thead>
		            <tr>
		            	<th>ID</th>
		            	<th>country_id</th>
		            	<th>Country</th>
		            	<th>state_name_id</th>
		            	<th>State</th>
		            	<th>city_id</th>
		            	<th>City Name</th>		            	
		            	<th>Locality Name</th>
		            	<th>Action</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<tr>
		        		<td colspan="6">Loading Data</td>
		        	</tr>
		        </tbody>			        
		    </table>
	    </div>
	    
    </div>    
</div>

<div class="modal fade" id="modal_locality_updates" tabindex="-1" role="dialog" aria-labelledby="ViewLocalityModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="ViewLocalityModalLabel">Add / Update Locality</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">						
						<label for="select_locality_modal_city_name">City Name</label>
		    			<select id="select_locality_modal_city_name" class="form-control col-md-12 fltr_locality select_control_with_title" title="Select City Name">
		    				<option></option>
		    				<?= $str_city; ?>
		    			</select>
					</div>
					<div class="col-md-8">
						<label for="txt_locality_modal_locality_name">Locality Name</label>
    					<input type="text" id="txt_locality_modal_locality_name" class="form-control"  data-column="7" />
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12" id="div_locality_save_result_area"></div>
					
					<input type="hidden" id="hidden_locality_modal_locality_id"/>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success" data-loading-text="Saving locality" id="cmd_save_locality">
					<i class="fa fa-floppy-o" aria-hidden="true"></i>
					Save Locality
				</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
			</div>
		</div>
	</div>
</div>
