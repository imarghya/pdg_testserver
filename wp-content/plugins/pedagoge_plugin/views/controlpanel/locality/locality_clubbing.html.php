<?php
	$str_locality_options = "";
	if(isset($locality_list_db)) {
		foreach($locality_list_db as $locality_info) {
			$str_locality_name = $locality_info->locality;
			$locality_id = $locality_info->locality_id;
			$str_locality_options .='
				<option value="'.$locality_id.'">'.$str_locality_name.'</option>
			';
		}
	}
?>

<div class="well">
	<div class="row">
		<div class="col-md-3">
			<label for="select_master_locality">Select Locality</label>
			<select class="form-control select_control_with_title " id="select_master_locality" title="Select Master Locality">
				<option></option>
				<?php echo $str_locality_options; ?>
			</select>
		</div>
		<div class="col-md-6">
			<label for="select_children_localities">Select Localities to merge</label>
			<select class="form-control select_control_with_title_multiple" id="select_children_localities" title="Select children localities" multiple="multiple">
				<option></option>
				<?php echo $str_locality_options; ?>
			</select>
		</div>
		<div class="col-md-3">
			<br />
			<button class="btn btn-info col-md-12" id="cmd_club_localities" data-loading-text="Updating...."><i class="fa fa-cubes" aria-hidden="true"></i> Club</button>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">List of Clubbed Localities</div>
	<div class="panel-body" id="div_clubbed_localities_list_area"></div>
</div>
