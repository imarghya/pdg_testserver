
<!-- Page content -->
<div id="page-content"> 
	
	<!-- Block Tabs -->
    <div class="block full">
        <!-- Block Tabs Title -->
        <div class="block-title">            
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#tab_localities_list"><i class="fa fa-list" aria-hidden="true"></i> Localities List</a></li>
                <li><a href="#tab_club_localities"><i class="fa fa-cubes" aria-hidden="true"></i> Club Localities</a></li>                
            </ul>
        </div>
        <!-- END Block Tabs Title -->

        <!-- Tabs Content -->
        <div class="tab-content">
            <div class="tab-pane active" id="tab_localities_list"><?php include_once('locality_list.html.php'); ?></div>
            <div class="tab-pane" id="tab_club_localities"><?php include_once('locality_clubbing.html.php'); ?></div>            
        </div>
        <!-- END Tabs Content -->
    </div>
    <!-- END Block Tabs -->
	
</div>
<!-- END Page Content -->