<?php
//error_reporting(0);
?>
					<!-- Footer -->
                    <footer class="clearfix">
                        <div class="pull-right">
                            <span title="<?php echo date('D, M'); ?>"><?php echo date('d-m-Y') ?></span> 
                            <i class="fa fa-clock-o fa-fw"></i> 
                            <span id="timeclock"></span>
                        </div>
                        <div class="pull-left">
                            <span id="year-copy"></span> &copy; <a href="<?php echo home_url('/'); ?>" target="_blank">Pedagoge</a>
                        </div>
                    </footer>
                    <!-- END Footer -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
                            <fieldset>
                                <legend>Vital Info</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Username</label>
                                    <div class="col-md-8">
                                        <p class="form-control-static">Admin</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-email">Email</label>
                                    <div class="col-md-8">
                                        <input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
                                    <div class="col-md-8">
                                        <label class="switch switch-primary">
                                            <input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>Password Update</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                                    <div class="col-md-8">
                                        <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->

        <!-- Load Scripts below -->
        
        <script type="text/javascript">
			var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
        	var $ajaxnonce = '';
        	var $pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>';
        	var $pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler';
        	var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
        	
        	var $ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
			
			
		</script>
        
		<?php 
			
			echo $this->load_view('footer_scripts');
			
			if( isset($this->app_data['dynamic_js']) ) {
				echo $this->app_data['dynamic_js'];
			}
		?>	
    </body>
    <script type="text/javascript">
    
    /*$(document).ready(function() {
    //console.log("ABC");
    	var timezone_offset_minutes = new Date().getTimezoneOffset();
	timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
	console.log("TimeZone : "+timezone_offset_minutes);
	$.ajax({
        	type: 'POST',
	        url: '<?php echo get_site_url();?>/wp-content/plugins/pedagoge_plugin/time.php',
	        data: { timezone_offset_minutes: timezone_offset_minutes },
	        cache: false,
	        dataType: 'JSON',
	        success: function(response) { 
	            //$("#current-timezone").text('Your browser\'s timezone is ' + response.timezone_name);
	            //$("#get-timezone-name").remove();
	        }
    	});
    });*/
    
    
        $(document).on("click",'.email_send',function(e) {
            //alert("Hi");
            
            var query_id = $("#query_id").val();
            var member_type = $(".email_member option:selected").attr('data-type');
            var member_id = $(".email_member option:selected").attr('data-member');
            var log = "An Email has been sent to the "+member_type;


            var email_subject = $('#email_subject').val(); 
            var to_email      = $("#ename").val();
            var cc            = $("#cc1").val();
            var bcc           = $("#bcc1").val();
            var attach_file   = $('input[name=attachement_file]')[0].files[0];
            var user_message  = $(".jodit_editor").html();

            //alert(user_message);
            var template = $(".templates").val();

            if(template == null){
                alertify.error("Error: Please select the template first");
                return false;
            }

            if(!email_subject.trim())
            {
                alertify.error("Error: Please insert the subject fields");
                return false;
            }

            var post_data = new FormData();    
            post_data.append( 'email_subject', email_subject );
            post_data.append( 'to_email', to_email );
            post_data.append( 'cc', cc );
            post_data.append( 'bcc',bcc);
            post_data.append( 'attach_file', attach_file );
            post_data.append( 'user_message', user_message );

            post_data.append( 'query_id', query_id);
            post_data.append( 'member_id', member_id);
            post_data.append( 'log', log );
            e.preventDefault();
            $.ajax({
                url: "<?php echo get_site_url();?>/wp-content/plugins/pedagoge_plugin/communication_email.php",
                type: "POST",
                data:  post_data,
                contentType: false,
                processData:false,
                success: function(data)
                {
                    console.log(data);
                    if(data == "success")
                    {
                        alertify.success("Success: Mail has been sent successfully");
                    }
                    else
                    {
                        alertify.error("Error: Mail sent failed");
                    }
                    /*$("#cv_img").html(data);
                    var cv=$(".cv_img").attr("img-name");
                    $("#current_cv").val(cv);*/
                },
                error: function() 
                {
                }             
            });
        });
    </script>
<?php
$user_id = get_current_user_id();
$user = new WP_User( $user_id );
if($user->roles[0] == "finance")
{
?>
<script type="text/javascript">
$(document).ready(function(){
$(".exp_payment_date").prop('disabled',true);
});
</script>
<?php
}
?>
</html>