<?php
$ref_model = new ModelReferral();
$query_model = new ModelQuery();

$referrals=$ref_model->get_referrals();

?>
<div id="page-wrapper">
   <div class="container-fluid">
      <div class="row">
                  <aside class="col-xs-12 blue_border border_radius_12 query_model">
                     <div class="inner_container margin_top_10">
                        <div class="row">
                        <!--Start Referral Design-->
                        	<div class="col-xs-10 col-xs-offset-1">
                        		<ul class="referral text-center">
					 <?php
				       if(count($referrals)>0){
                        //echo "<pre>"; print_r($referrals); echo "</pre>";
				       foreach($referrals AS $res){
				       $localities = $query_model->get_q_locality($res->localities);
				       ?>		 
                                    	<li>
                                        	<div class="row">
                                                <div class="col-sm-7 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-3 col-sm-12 col-xs-12 referral_name">
                                                            <a href="#"><?php echo $res->name; ?></a>
                                                        </div>
                                                         <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <p>[<?php echo $res->phone; ?>]</p>
                                                        </div>
                                                         <div class="col-md-6 col-sm-12 col-xs-12">
                                                            <p style="word-break: break-word;"><?php echo $res->email; ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
                                                	<div class="row">
                                                    	<div class="col-md-7 col-sm-12 col-xs-12">
                                                            <p><span style="display: block;">was referred by</span> <?php echo $res->display_name; ?> on <span><?php echo date('d-m-Y',strtotime($res->created_at)); ?></span></p>
                                                        </div>
                                                         <div class=" col-md-5 col-sm-12 col-xs-12 referral_query">
                                                            <a href="javascript:void(0);" id="<?php echo $res->ref_id; ?>" class="admin_referial_collapse view_query">View Query</a>
                                                        </div>
                                                    </div>
                                                </div>
                                        	</div>
						
						<div id="show<?php echo $res->ref_id; ?>" class="col-xs-12 margin_top_20 inner_container_text blue_border admin_referial_expend text-left" style="display:none;">
                                                    <div class="row">
                                                         <div class="col-xs-12 blue_background top_radius h1_heading">
                                                            <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
                                                         </div>
                                                    </div>
                                                    <div class="row margin_top_10">
                                                         <div class="col-md-6 col-sm-6 col-xs-12 localities">
                                                                <p>Year/Standard: <?php echo ($res->standard != ""?$res->standard:$res->age." Years"); ?></p>
                                                                <p>Board/University: <?php echo $res->academic_board; ?></p>
                                                                <div class="preferred_location">
                                                                    <p>Locality Type:</p> 
                                                                    <ul class="area">
                                                                        <?php if($res->myhome==1){ ?>
									<li><a href="#" class="blue_background">My Home</a></li>
									<?php } ?>
									<?php if($res->tutorhome==1){ ?>
									<li><a href="#" class="blue_background">Tutor's Home</a></li>
									<?php } ?>
									<?php if($res->institute==1){ ?>
									<li><a href="#" class="blue_background">Institue</a></li>
									<?php } ?> 
                                                                    </ul>
                                                                </div>
                                                                <div class="preferred_location">
                                                                    <p>Preferred Localities:</p> 
                                                                    <ul class="area">
                                                                        <?php foreach($localities AS $loc){ ?>
									   <li><a href="#" class="blue_background"><?php echo $loc; ?></a></li>
									<?php } ?>
                                                                    </ul>
                                                                </div>
                                                                <p>Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
                                                            <h5>Preferences: </h5>
                                                            <p class="text-justify"><?php echo trim($res->requirement); ?></p>
                                                         </div>
                                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <ul class="pull-right side_right_menu text-center">
							       <?php
							       $ap_txt='Approve';
							       $is_approve=0;
							       if($res->is_approve==1){
								$ap_txt='Unapprove';
								$is_approve=1;
							       }
							       ?>
                                                                <li><a id="ref_btn<?php echo $res->ref_id; ?>" href="#" ref-by-id="<?php echo $res->reffer_by_id; ?>" is_approve="<?php echo $is_approve; ?>" ref-id="<?php echo $res->ref_id; ?>" class="approv_disapp_ref"><?php echo $ap_txt;  ?></a></li>
                                                            </ul>
                                                         </div>
                                                    </div>
                                                    
                                                 </div>                                            
                                            
                                        </li>
					<?php }}else{ ?>
			                <li>
							<div class="row">
							     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">No Referral Found!</div>
							</div>
							</li>
			                <?php } ?>
					
                                    </ul>
                                    
                                </div>
                                <!--Start Referral Design-->
                        </div>
                     </div>
                  </aside>
               </div>
   </div>
</div>

<script type="text/javascript">
//      	$(".admin_referial_collapse").click(function(){
//			$(".admin_referial_expend").toggle();
//		});
	
    $(document).on("click",".view_query",function(){
  var idd=$(this).attr("id");
  $("#show"+idd).toggle();
  });
      </script>