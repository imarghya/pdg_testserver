<?php
$str_email_client_url = "https://mail.pedagoge.com/";
if(isset($this->app_data['current_user_email'])) {
    $str_email_client_url .= '?username='.$this->app_data['current_user_email'];
}
pedagoge_applog($str_email_client_url);
?>



<div id="page-content">
    <iframe src="<?php echo $str_email_client_url; ?>" style="width:100%; height:100%; min-height:600px;" height="100%" width="100%">
        <p>Your browser does not support iframes.</p>
    </iframe>
</div>