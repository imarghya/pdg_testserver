<?php
	$control_panel_url = home_url('/dashboard');

    $user_id = get_current_user_id();
    $user = new WP_User( $user_id );
    $user_role = $user->roles[0];
    $query_model = new ModelQuery();
    $payment_update_notification_count = 0;
    $payment_receive_notification_count = 0;
    $payment_transfer_notification_count = 0;
    
    $notification_count = 0;
    
    if(isset($_GET['query_id']))
    {
        $query_id=$_GET['query_id'];
        //$notifications=$query_model->get_all_notification($teacher_id);
    }

    if($user_role == "sales"){
        $a_payment_receive_notification = $query_model->payment_received_count_notification($user_id);

        if(!empty($a_payment_receive_notification['query_id']))
        {
            $r_query_ids = implode(',', $a_payment_receive_notification['query_id']);
        }
        $payment_receive_notification_count = $a_payment_receive_notification['count'];


        $a_payment_transfer_notification = $query_model->payment_transfer_count_notification($user_id);
        if(!empty($a_payment_transfer_notification['query_id']))
        {
            $t_query_ids = implode(',', $a_payment_transfer_notification['query_id']);
        }
        $payment_transfer_notification_count = $a_payment_transfer_notification['count'];
        //$payment_transfer_notification_count = $a_payment_receive_notification['count'];
        $notification_count = ($payment_receive_notification_count + $payment_transfer_notification_count);

    }
    if($user_role == "finance"){
        $a_payment_update_notification = $query_model->payment_update_count_notification();
        if(!empty($a_payment_update_notification['query_id']))
        {
        $query_ids = implode(',', $a_payment_update_notification['query_id']);
        }
        $payment_update_notification_count = $a_payment_update_notification['count'];
        
        $notification_count = $payment_update_notification_count;
    }
    
    if($user_role == "administrator"){
        $a_notification = $query_model->get_all_admin_notification();
        usort($a_notification, function ($item1, $item2) {
            if ($item1['created_at'] == $item2['created_at']) return 0;
            return $item1['created_at'] > $item2['created_at'] ? -1 : 1;
        });
        
        $notification_count=$query_model->get_admin_count_notification();
        //$arr_notification = $a_notification['notification'];
       //echo "<pre>"; print_r($a_notification);echo "</pre>";
    }
?>

<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $this -> title; ?></title>
        <meta name="author" content="pedagoge">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        
        <?php echo $this -> load_view('header_scripts'); ?>
        <style type="text/css">
            /* Bell Icon ================================================
==============================================================================*/

.notification_admin{
    left: auto;
    right: 0;
}
.notification_bell_admin strong{
    display: inline-block;
    background: #394262;
    border: none;
    color: #fff;
    width: 30px;
    height: 30px;
    line-height: 28px;
    font-size: 16px;
    margin-top: 10px;
    border-radius: 100%;
}
.notification_bell_admin:hover{
    background-color: transparent;
}
.notification_admin{
    width:300px;
    padding:0;
}
.notification_admin li{
    width:149px;
    height:150px;
    float:left;
    text-align:center;
}
.notification_admin li div{
    float:left;
    width:100%;
}
.notification_admin li a{
    width:100%;
    height:100%;
}
.notification_admin span{
    display:block;
    width:30px;
    height:30px;
    color:#313854;
    border:2px solid #313854;
    background:#fff;
    border-radius:50%;
    line-height:26px;
    float:right;
}
.notification_admin li svg{
    width:80px;
    height:80px;
}
.notification_admin li a svg rect,.notification_admin li a svg polygon,.notification_admin li a svg path{
    fill:#313854;
}
.notification_admin li a.active svg rect,.notification_admin li a.active svg polygon,.notification_admin li a.active svg path{
    fill:#ffffff;
}
.hide_notification{
    display:none !important;
}
.show_notification{
    width:100% !important;
}
.notification_admin li a.active{
    background:#313854;
}
.notification_bell_admin strong{
	position:relative;
}
.notification_bell_admin strong sup{
	top: -2px;
   position: absolute;
   right: -10px;
   color: #394262;
   font-weight: bold;
}

/* Bell Icon ================================================
==============================================================================*/


/* Notification for student and teacher */

.notification_bg{
    background:#edf1fa;
    min-width:400px;
    position: absolute;
    right: 0;
    width:auto;
    text-transform:none;
    border:1px solid #2e3847;
    z-index:9999;
    display: none;
}
.notification_bg::before,.notification_bg::after{
    content:"";
    position:absolute;
    right:20px;
    width:0;
    height:0;
    border-style:solid;
    border-width:0 15px 15px;
    display:block;
}
.notification_bg::before{
    top:-15px;
    border-color:transparent transparent #2e3847;
}
.notification_bg::after{
    top:-14px;
    border-color:transparent transparent #fff;
}
.head_notifacition{
    padding: 0;
}
.head_notifacition li div{
    display:inline-block;
    vertical-align: top;
}
.notification_bg h2{
    background-color: #fff;
    box-shadow: 0 1px 8px rgba(0, 0, 0, 0.1);
    color: #2e3847;
    font-size: 16px;
    font-weight: 500;
    margin: 0;
    padding: 10px;
    text-align: center;
    border-radius:12px 12px 0 0;
}
.head_notifacition li{
    border-bottom: 1px solid #bdbfc2;
    padding: 10px 15px;
    transition:all ease-in-out 0.3s;
}
.head_notifacition li.active{
    background:#fff;
}
.bell_icon{
    width:45px;
    height:45px;
    line-height:45px;
    text-align:center;
    color:#fff;
    font-size:24px;
    background:#2e3847;
    border-radius:10px;
    margin-right:10px;
}
.notification_details_para{
    width:auto;
    max-width:70%;
}
.notification_details_para p{
    font-size:14px;
    color:#2e3847;
    margin-top: 5px;
}
.navigation_delate{
    float: right;
    padding: 0 10px;
    text-align: center;
    vertical-align: top;
    width: 40px;
    cursor:pointer;
}
.navigation_delate button{
    background:none;
    border:none;
    box-shadow:none;
    color:#000;
}
.unic_link li p{
    font-size:14px;
    line-height:24px;
    margin-top:30px;
}
.unic_link li a{
    font-size:14px;
    margin-top:15px;
    border-radius:4px;
    padding:5px 10px;
    background-color:#183a46;
    color:#fff;
}
.unic_link li a{
    font-size:14px;
    margin-top:15px;
    border-radius:4px;
    padding:5px 10px;
    background-color:#183a46;
    color:#fff;
}
.head_notifacition li:hover {
    background: #fff;
}
.head_notifacition .bell_icon .fa{
    font-size: 24px;
}



        </style>
        
    </head>
    <body>
        <!-- Page Wrapper -->
        <!-- In the PHP version you can set the following options from inc/config file -->
        <!--
            Available classes:

            'page-loading'      enables page preloader
        -->
        <div id="page-wrapper">
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
                </div>
            </div>
            
            <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations footer-fixed ">
                

                <!-- Main Sidebar -->
                <div id="sidebar">
                    <!-- Wrapper for scrolling functionality -->
                    <div id="sidebar-scroll">
                        <!-- Sidebar Content -->
                        <div class="sidebar-content">
                            <!-- Brand -->
                            <a href="<?php echo $control_panel_url; ?>" class="sidebar-brand">
                                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Pedagoge</strong> CP</span>
                            </a>
                            <!-- END Brand -->

                            <!-- User Info -->
                            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                                <div class="sidebar-user-avatar">
                                    <a href="#" class="not_clickable">
                                        <img src="<?php echo PEDAGOGE_CP_ASSETS_URL; ?>/img/placeholders/avatars/avatar2.jpg" alt="avatar">
                                    </a>
                                </div>
                                <div class="sidebar-user-name"><?php echo $this -> app_data['pdg_current_user'] -> user_login; ?></div>
                                <div class="sidebar-user-links">
                                    <a href="#" class="not_clickable" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                                    <a href="#" class="not_clickable" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                    <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                                    <a href="<?php echo wp_logout_url(); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                                </div>
                            </div>
                            <!-- END User Info -->

                          	

                            <!-- Sidebar Navigation -->
                            <ul class="sidebar-nav">
                                <?php include_once('cp_menu.html.php'); ?>
                            </ul>
                            <!-- END Sidebar Navigation -->

                            <!-- Sidebar Notifications -->
                            <div class="sidebar-header sidebar-nav-mini-hide">
                                <span class="sidebar-header-options clearfix">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
                                </span>
                                <span class="sidebar-header-title">Activity</span>
                            </div>
                            <div class="sidebar-section sidebar-nav-mini-hide">
                                <div class="alert alert-success alert-alt">
                                    <small>5 min ago</small><br>
                                    <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                </div>
                                <div class="alert alert-info alert-alt">
                                    <small>10 min ago</small><br>
                                    <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                </div>
                                <div class="alert alert-warning alert-alt">
                                    <small>3 hours ago</small><br>
                                    <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                </div>
                                <div class="alert alert-danger alert-alt">
                                    <small>Yesterday</small><br>
                                    <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)"><strong>New bug submitted</strong></a>
                                </div>
                            </div>
                            <!-- END Sidebar Notifications -->
                        </div>
                        <!-- END Sidebar Content -->
                    </div>
                    <!-- END Wrapper for scrolling functionality -->
                </div>
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    <!-- Header -->
                    
                    <header class="navbar navbar-default">
                        <!-- Left Header Navigation -->
                        <ul class="nav navbar-nav-custom admin_top_navigation">
                            <!-- Main Sidebar Toggle Button -->
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-bars fa-fw"></i>
                                </a>
                            </li>
                            <!-- END Main Sidebar Toggle Button -->

                            <!-- Template Options -->
                            <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="gi gi-settings"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-options">
                                    <li class="dropdown-header text-center">Header Style</li>
                                    <li>
                                        <div class="btn-group btn-group-justified btn-group-sm">
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                                        </div>
                                    </li>
                                    <li class="dropdown-header text-center">Page Style</li>
                                    <li>
                                        <div class="btn-group btn-group-justified btn-group-sm">
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                                            <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            
                            <li>
                                <h4><?php echo $this->title; ?></h4>
                                      
				
                            </li>
                            <!-- END Template Options -->
			    <?php if(isset($_GET['page']) && $_GET['page']=='queries'){ ?>
			     <!--Start Queries Top menu-->
			    <li>
				<span class="navbar-form inline_block query_search" role="search">
					<div class="input-group blue_border border_radius_30 overflow_hidden">
					   <input type="text" class="form-control border_none" placeholder="Search" name="srch-term" id="srch-term">
					   <div class="input-group-btn">
					      <button class="btn blue_background color_white admin_search_btn" type="button"><i class="glyphicon glyphicon-search"></i></button>
					   </div>
					</div>
				</span>
			    </li>
                </ul>
                 <?php } ?>
                  <?php if(isset($_GET['page']) && $_GET['page']=='queries'){ ?>
                  <ul class="nav navbar-nav-custom admin_top_navigation navbar-middle">
                  <?php
                    if($user_role == "administrator"){
                    ?>
			    <li>
				   <ul class="add_edit_icon">
				      <li class="two_box"><a href="#" title="New Query" data-toggle="modal" data-target="#subjectChoose"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
				      <li class="disabled" id="query_edit_btn"><a href="#" title="Edit" id="query_edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></li>
				      <li class="two_box disabled" id="query_clone_btn"><a href="#" id="query_clone" title="Clone"><i class="fa fa-clone" aria-hidden="true" style="opacity:0;"></i></a></li>
				      <li class="disabled" id="query_approve_btn"><a href="#" id="query_approve" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></a></li>
				      <li class="disabled" id="query_disapprove_btn"><a href="#"  data-toggle="modal" data-target="#modal_query_disapprove" title="Disapprove"><i class="fa fa-times" aria-hidden="true"></i></a></li>
				   </ul>
				</li>
                
				<li>
				   <select id="sales_rep" name="assign" class="text-uppercase text-center assign apperence_none">
				       <option value="">Assign</option>
				       <?php
				       $sales_rep_model = new ModelSalesRep();
				       $sales_reps = $sales_rep_model->get_sales_rep();
				       foreach($sales_reps AS $sales_rep ){
				       ?>
				       <option value="<?php echo $sales_rep->ID; ?>"><?php echo $sales_rep->display_name; ?></option>
				       <?php } ?>
				     </select>
				</li>
                <?php }?>
			    <!--End Queries Top menu-->
			    <?php } ?>
                        </ul>
			<!--Start Queries Top menu-->
                        <!-- END Left Header Navigation -->
						
                        <!-- Middle Header Navigation Start -->
                        
                        <!-- Middle Header Navigation End -->
                        

                        <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
			 <?php if(isset($_GET['page']) && $_GET['page']=='queries'){ 
                if($user_role == "administrator"){
                    ?>
                            <li class="header_top_new">
					  <select id="header_query_ststus" autocomplete="off">
						<option value="1">New</option>
						<option value="2">Assigned</option>
						<option value="0" selected="selected" >Pending</option>
					</select> 
			     </li>
			 <?php }} ?>
                            <!-- END Alternative Sidebar Toggle Button -->
                           
                           <li class="dropdown">
                              <a href="#" id="admin_notification" class="dropdown-toggle admin_user_id no_padding_top_bottom notification_bell_admin" data-toggle="dropdown"><strong><i class="fa fa-bell-o" aria-hidden="true"></i>
                                <?php
                                if($user_role == 'administrator')
                                {
                                ?>
                                <sup id="count_noti_admin"><?= $notification_count;?></sup>
                                <?php
                                }
                                ?>
                                <?php
                                if($user_role == 'sales')
                                {
                                ?>
                                <sup id="count_noti_sales"><?= $notification_count;?></sup>
                                <?php
                                }
                                if($user_role == 'finance')
                                {
                                ?>
                                <sup id="count_noti_finance"><?= $notification_count;?></sup>
                                <?php
                                }
                                ?>
                                
                            </strong></a>
                              <?php
                               if($user_role != 'administrator')
                               {
                               ?>
                              <ul class="dropdown-menu notification_admin">
                                 <li data_id="<?= ($user_role == 'finance'?$query_ids:$r_query_ids);?>" class="<?= ($user_role == 'finance'?'show_notification':'receive_payment')?>"><!--For Show and hide notification "hide_notification" and "show_notification"-->
                                    <a href="javascript:void(0)" class="active">
                                        <div>
                                            <span id="<?= ($user_role == 'finance'?'payment_update_noti':'payment_receive_noti');?>"><?= ($user_role == 'finance'?$payment_update_notification_count:$payment_receive_notification_count);?></span>
                                        </div>
                                        <div>
                                            <svg version="1.1" id="Layer_1" x="0px" y="0px"
                                                 viewBox="0 0 87.954 63.442" style="enable-background:new 0 0 87.954 63.442;"
                                                 xml:space="preserve">
                                            <g>
                                                <rect x="78.811" y="33.275"  width="2.783" height="2.626"/>
                                                <rect x="78.811" y="40.63"  width="2.783" height="2.627"/>
                                                <rect x="78.811" y="48.001" width="2.783" height="2.627"/>
                                                <path d="M6.362,47.203l30.061,15.252c2.258,1.183,4.904,1.32,7.285,0.521l25.295-8.016h3.449v2.765h15.502
                                                    V26.303H72.452v3.548H62.52c-0.79,0-1.718,0.137-2.521,0.399l-9.004,4.605c-0.402,0.138-0.929,0.261-1.33,0.261H35.881
                                                    c-3.449,0-6.358,2.765-6.358,6.174c0,0.523,0,0.921,0.14,1.444l-18.535-6.174c-2.91-1.06-6.096,0.261-7.412,3.024
                                                    C2.387,42.336,3.579,45.636,6.362,47.203 M75.098,54.96V29.852v-1.06h10.209v26.306H75.112V54.96H75.098z M6.084,40.63
                                                    c0.791-1.581,2.521-2.241,4.239-1.705l21.859,7.234c1.067,0.659,2.258,1.183,3.712,1.183h11.387c1.33,0,2.384,1.045,2.384,2.365
                                                    h2.645c0-2.765-2.258-4.992-5.028-4.992H35.756c-1.98,0-3.573-1.596-3.573-3.548c0-1.966,1.606-3.547,3.573-3.547h13.907
                                                    c0.791,0,1.594-0.138,2.258-0.399l9.004-4.605c0.527-0.139,1.054-0.262,1.455-0.262h9.933v19.979h-3.712l-25.683,8.154
                                                    c-1.717,0.523-3.713,0.399-5.306-0.397L7.678,44.702C6.084,44.054,5.434,42.075,6.084,40.63"/>
                                                <path d="M21.867,35.865c-6.496,0-11.78-5.246-11.78-11.694c0-6.447,5.284-11.693,11.78-11.693
                                                    s11.781,5.246,11.781,11.693C33.648,30.619,28.363,35.865,21.867,35.865 M21.867,15.031c-5.076,0-9.207,4.101-9.207,9.14
                                                    c0,5.04,4.131,9.139,9.207,9.139c5.077,0,9.207-4.1,9.207-9.139C31.074,19.132,26.944,15.031,21.867,15.031"/>
                                                <path d="M7.199,14.291C3.23,14.291,0,11.085,0,7.145C0,3.205,3.23,0,7.199,0c3.97,0,7.199,3.205,7.199,7.145
                                                    C14.398,11.085,11.168,14.291,7.199,14.291 M7.199,2.553c-2.55,0-4.625,2.061-4.625,4.591c0,2.532,2.075,4.591,4.625,4.591
                                                    s4.625-2.059,4.625-4.591C11.824,4.614,9.749,2.553,7.199,2.553"/>
                                            </g>
                                            </svg>
                                        </div>
                                    </a>
                                 </li>
                                 <li data_id="<?= ($user_role == 'finance'?'0':$t_query_ids);?>" class="<?= ($user_role == 'finance'?'hide_notification':'transfer_payment')?>">
                                    <a href="javascript:void(0)">
                                        <div>
                                            <span id="payment_transfer_noti"><?= ($user_role == 'finance'?'0':$payment_transfer_notification_count);?></span>
                                        </div>
                                        <div>
                                            <svg version="1.1" id="Layer_1" x="0px" y="0px"
                                             viewBox="0 0 84.781 84.742" style="enable-background:new 0 0 84.781 84.742;"
                                             xml:space="preserve">
                                        <rect x="34.174" y="75.477"  width="2.646" height="2.645"/>
                                        <rect x="41.059" y="75.477"  width="2.646" height="2.645"/>
                                        <rect x="47.957" y="75.477"  width="2.646" height="2.645"/>
                                        <polygon points="84.776,52.951 84.776,84.742 0,84.742 0,52.951 27.69,52.951 27.69,55.598 2.646,55.598 
                                            2.646,82.096 82.131,82.096 82.131,55.598 57.086,55.598 57.086,52.951 "/>
                                        <rect x="34.174" y="75.477"  width="2.646" height="2.645"/>
                                        <rect x="41.059" y="75.477"  width="2.646" height="2.645"/>
                                        <rect x="47.957" y="75.477"  width="2.646" height="2.645"/>
                                        <rect x="6.358" y="27.781"  width="2.784" height="2.646"/>
                                        <rect x="6.358" y="20.371"  width="2.784" height="2.646"/>
                                        <rect x="6.358" y="12.945"  width="2.784" height="2.646"/>
                                        <path d="M81.591,16.395L51.531,1.032c-2.259-1.191-4.904-1.33-7.286-0.526L18.95,8.582h-3.449V5.797H0v31.654
                                            h15.501v-3.574h9.932c0.79,0,1.719-0.139,2.521-0.402l9.004-4.641c0.402-0.139,0.928-0.263,1.33-0.263h13.783
                                            c3.449,0,6.358-2.785,6.358-6.22c0-0.527,0-0.928-0.139-1.455l18.535,6.22c2.909,1.067,6.095-0.263,7.411-3.047
                                            C85.566,21.298,84.375,17.974,81.591,16.395 M12.854,8.582v25.294v1.066H2.646v-26.5h10.195v0.139H12.854z M81.868,23.016
                                            c-0.79,1.593-2.521,2.258-4.239,1.718L55.77,17.447c-1.066-0.665-2.258-1.191-3.712-1.191H40.671c-1.33,0-2.383-1.053-2.383-2.383
                                            h-2.646c0,2.785,2.259,5.029,5.028,5.029h11.525c1.98,0,3.573,1.607,3.573,3.574c0,1.981-1.605,3.574-3.573,3.574H38.289
                                            c-0.79,0-1.593,0.138-2.258,0.402l-9.004,4.639c-0.527,0.141-1.054,0.264-1.455,0.264H15.64V11.228h3.712l25.683-8.215
                                            c1.718-0.526,3.713-0.402,5.306,0.402l29.936,15.501C81.868,19.567,82.519,21.561,81.868,23.016"/>
                                        <path d="M58.674,57.207c-0.69-8.279-7.505-15.018-15.957-15.018c-8.453,0-15.578,6.738-16.269,15.018h-2.806
                                            v1.762h36.856v-1.762H58.674z M27.622,57.207c1.028-7.262,7.262-13.33,15.011-13.33c7.262,0,13.493,6.068,14.522,13.33H27.622z"/>
                                        </svg>
                                    </div>
                                    </a>
                                 </li>
                              </ul>
                              <?php
                                }
                                else
                                {
                                    //if(isset($_GET['page']) && $_GET['page']=='queries'){
                                ?>
                                <div id="myDropdown" class="dropdown-content notification_bg">
                                    <h2>Notification</h2>
                                        <ul id="head_notifacition" class="head_notifacition" style="overflow: hidden; width: 100%; height: 300px;">
                                        <?php if(count($a_notification)>0){ foreach($a_notification AS $noti){ ?>
                                            <li id="noti<?php echo $noti->id; ?>">
                                                <div class="bell_icon"><i class="fa fa-bell-o" aria-hidden="true"></i></div>
                                                <div class="notification_details_para">
                                                    <p><?php echo $noti['notification']; ?></p>
                                                </div>
                                                <!--<div class="navigation_delate">
                                                    <button class="delete_noti" idd="<?php echo $noti->id; ?>" type="button">X</button>
                                                </div>-->
                                            </li>
                                        <?php } }else{?>
                                            <li>
                                                <div class="bell_icon"><i class="fa fa-bell-o" aria-hidden="true"></i></div>
                                                <div class="notification_details_para">
                                                    <p>You have no notification to show!</p>
                                                </div>
                                                <div class="navigation_delate">
                                                    <!--<button type="button">X</button>-->
                                                </div>
                                            </li>
                                    <?php } ?>
                                        </ul>

                                </div>
                                <?php
                            //}
                                }
                               ?>
                           </li>
                           

                            <!-- User Dropdown -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo PEDAGOGE_CP_ASSETS_URL; ?>/img/placeholders/avatars/avatar2.jpg" alt="avatar"> <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li class="dropdown-header text-center">Account</li>
                                    
                                    <li>
                                        <a href="#" class="not_clickable">
                                            <i class="fa fa-user fa-fw pull-right"></i>
                                            Profile
                                        </a>
                                        <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                        <a href="#modal-user-settings" data-toggle="modal">
                                            <i class="fa fa-cog fa-fw pull-right"></i>
                                            Settings
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#" class="not_clickable"><i class="fa fa-lock fa-fw pull-right"></i> Lock Account</a>
                                        <a href="<?php echo wp_logout_url(); ?>"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                                    </li>
                                    <li class="dropdown-header text-center">Activity</li>
                                    <li>
                                        <div class="alert alert-success alert-alt">
                                            <small> 5 min ago</small><br>
                                            <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                        </div>
                                        <div class="alert alert-info alert-alt">
                                            <small>10 min ago</small><br>
                                            <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                        </div>
                                        <div class="alert alert-warning alert-alt">
                                            <small>3 hours ago</small><br>
                                            <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                        </div>
                                        <div class="alert alert-danger alert-alt">
                                            <small>Yesterday</small><br>
                                            <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)" class="alert-link">New bug submitted</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- END User Dropdown -->
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    <!-- END Header -->

                    <script type="text/javascript">
                        
                        $(function(){

                    $('.head_notifacition').slimScroll({

                        height: '300px',

                        size: '5px',

                        width:'100%',

                        alwaysVisible: true,

                        distance: '4px',

                        color: '#2e3847',

                        opacity: 1
                    });
                });
            </script>
            <?php
            if($user_role == 'administrator')
            {
            ?>
            <script type="text/javascript">
                $(document).on("click","#admin_notification",function(){
                    document.getElementById("myDropdown").classList.toggle("show");
                });

                $(document).click(function(e) 
                {
                    var container = $(".dropdown-content");

                    // if the target of the click isn't the container nor a descendant of the container
                    if (!container.is(e.target) && container.has(e.target).length === 0) 
                    {
                      if($("#myDropdown").hasClass("show"))
                        {
                          $("#myDropdown").removeClass("show");
                        }
                    }
                    else{
                    }
                });

            </script>
            <?php
            }
            ?>
