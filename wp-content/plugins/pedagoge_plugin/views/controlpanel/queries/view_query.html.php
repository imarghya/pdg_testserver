<?php
$query_id=$_GET['query_id'];


error_reporting(0);
?>
<div id="page-wrapper">
   <input type="hidden" id="view_query_id" value="<?php echo $query_id; ?>"/>
   <div class="container-fluid">
      <div class="row">
         <aside class="col-xs-12 border_radius_12 query_model">
         	<div class="row">
            	<div class="col-xs-1 pull-right text-center return_query_page">
                	<a href="<?= home_url('/dashboard/?page=queries');?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="inner_container margin_top_10">
               <div class="row">
                  <div class="col-xs-12 text-uppercase">
                     <h4 class="blue_border">Id <?php echo $query_id; ?></h4>
                  </div>
                  <div class="col-xs-8 col-xs-offset-2 margin_top_20 inner_container_text blue_border white_background">
                     <?php foreach($queries AS $query){ ?>
                     <div class="row">
                        <div class="col-xs-12 admin_menu_background top_radius h1_heading">
                           <h2 class="color_white">Looking for a <span class="text-uppercase"><?php echo $query->subject_name; ?></span></h2>
                        </div>
                     </div>
                     <?php
                     $query_model = new ModelQuery();
                     $query_details = $query_model->get_query_details($query_id);
                     $sale_id = $query_details[0]->current_sales_rep_id;
                     //echo "<pre>"; print_r($query_details); echo "</pre>";
                     $is_approved = $query->approved;
                     $q_disable = '';
                     if($is_approved != 1)
                     {
                      $q_disable = 'disabled';
                     }
                     
                     $disabled = '';
                     $readonly = '';
                     if($query_details[0]->query_status == 'CLOSED') 
                     {
                       $disabled = 'disabled';
                       $readonly = 'readonly';
                     }
                     if($role=='finance'){
                        $readonly = 'readonly';
                     }
                     if($role=='sales'){
                        $readonly = '';
                     }
                     //echo $sale_id;
                     $course_type_id_arr=$query_model->course_type_id_arr($query->subject_name_id);
                     $localities = $query_model->get_q_locality($query->localities);

                     if($query->contact_no != "")
                     {
                      $contact_no = $query->contact_no;
                     }
                     else{
                      $contact_no = $query->mobile_no;
                     }

                     if($query->contact_email != "")
                     {
                      $contact_email = $query->contact_email;
                     }
                     else{
                      $contact_email = $query->user_email;
                     }


                     ?>
                     <div class="row margin_top_10">
                        <div class="col-md-9 col-sm-10 col-xs-6 localities">
                           <p>Name : <span><?php echo $query->student_name; ?></span></p>
                           <p>Phone : <span><?php echo $contact_no; ?></span></p>
                           <p>Email Id : <span><?php echo $contact_email; ?></span></p>
                           <?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
                           <p>Age : <span><?php echo $query->age; ?> Years</span></p>
                           <?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
                           <p>Year/Standard: <span><?php echo $query->standard; ?></span></p>
                           <p>Board/University : <span><?php echo $query->academic_board; ?></span></p>
                           <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
                           <p>Year/Standard: <span><?php echo $query->standard; ?></span></p>
                           <p>Board/University : <span><?php echo $query->academic_board; ?></span></p>
                            <p>Age : <span><?php echo $query->age; ?> Years</span></p>
                           <?php } ?>
                           <div class="preferred_location">
                              <p>Locality Type:</p>
                              <ul class="area">
                                 <?php if($query->myhome==1){ $myhome=1; ?>
                                 <li><a href="#" class="admin_menu_background">My Home</a></li>
                                 <?php } ?>
		                 <?php if($query->tutorhome==1){ $tutorhome=1; ?>
                                 <li><a href="#" class="admin_menu_background">Tutor’s Home</a></li>
                                 <?php } ?>
		                 <?php if($query->institute==1){ $institute=1; ?>
                                 <li><a href="#" class="admin_menu_background">Institue</a></li>
                                 <?php } ?>
                              </ul>
                           </div>
                           <div class="preferred_location">
                              <p>Preferred Localities:</p>
                              <ul class="area">
                                 <?php foreach($localities AS $loc){ ?>
                                 <li><a href="#" class="admin_menu_background"><?php echo $loc; ?></a></li>
                                 <?php } ?>
                                 <!--<li><a href="#" class="admin_menu_background">salt lake</a></li>
                                 <li><a href="#" class="admin_menu_background">salt lake</a></li>-->
                              </ul>
                           </div>
                           <div class="preferred_location">
                              <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($query->start_date), "d/m/Y"); ?></strong></p>
                           </div>
                        </div>
                        <div class="col-md-3 col-sm-2 col-xs-12 margin_bottom_20 preference white_space_normal">
                           <h5>Preferences: </h5>
                           <p class="text-justify"><?php echo trim($query->requirement); ?></p>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
                  <div class="col-xs-2">
                     <div class="admin_logo pull-right">
                        <a href="<?php echo home_url('/dashboard').'?page=view_log&query_id='.$query_id;?>">
                           <svg version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 588.601 588.6" style="enable-background:new 0 0 588.601 588.6;" xml:space="preserve">
                              <g>
                                 <path d="M157.13,263.919c-16.938,0.264-26.702,17.052-26.702,39.293c0,22.417,10.114,38.306,26.871,38.475 c17.112,0.18,27.005-16.675,27.005-39.308C184.304,281.464,174.588,263.643,157.13,263.919z" fill="#FFFFFF"/>
                                 <path d="M577.373,86.999c0-20.838-16.954-37.8-37.8-37.8h-178.2c-0.786,0-1.561,0.076-2.342,0.124V0L11.228,46.417v494.564 L359.031,588.6v-50.814c0.781,0.053,1.551,0.115,2.342,0.115h178.2c20.846,0,37.8-16.964,37.8-37.8V86.999z M359.031,375.369 c0.417,0.259,0.881,0.448,1.403,0.448h138.602c1.492,0,2.7-1.213,2.7-2.7c0-1.497-1.208-2.7-2.7-2.7H360.435 c-0.522,0-0.986,0.19-1.403,0.448v-27c0.417,0.259,0.881,0.448,1.403,0.448h138.602c1.492,0,2.7-1.202,2.7-2.699 c0-1.487-1.208-2.7-2.7-2.7H360.435c-0.522,0-0.986,0.189-1.403,0.448v-32.4c0.417,0.259,0.881,0.448,1.403,0.448h138.602 c1.492,0,2.7-1.202,2.7-2.7c0-1.486-1.208-2.699-2.7-2.699H360.435c-0.522,0-0.986,0.189-1.403,0.447v-26.989 c0.417,0.262,0.881,0.448,1.403,0.448h138.602c1.492,0,2.7-1.215,2.7-2.7c0-1.495-1.208-2.7-2.7-2.7H360.435 c-0.522,0-0.986,0.189-1.403,0.448v-26.099h166.557v162H359.031V375.369z M101.161,359.617l-65.509-1.055V247.751l23.459-0.514 v90.495l42.05,0.369V359.617z M155.965,362.401c-32.648-0.554-51.435-26.282-51.435-58.604c0-33.998,20.735-59.891,53.259-60.626 c34.61-0.783,53.85,25.819,53.85,58.248C211.634,339.968,188.85,362.96,155.965,362.401z M240,347.098 c-10.773-10.305-16.659-25.762-16.482-43.09c0.158-39.21,28.561-62.261,67.927-63.152c15.749-0.345,28,2.434,34.059,5.194 l-5.906,22.162c-6.771-2.803-15.146-5.002-28.54-4.796c-22.723,0.355-39.675,13.323-39.675,39.121 c0,24.553,15.364,39.182,37.692,39.402c6.357,0.063,11.391-0.601,13.584-1.661v-25.328l-18.778-0.011v-21.252l44.608-0.258v64.609 c-8.87,2.727-24.438,6.145-39.928,5.891C266.557,363.551,250.847,357.808,240,347.098z M555.773,500.101 c0,8.928-7.268,16.2-16.2,16.2h-178.2c-0.796,0-1.571-0.116-2.342-0.232v-93.013h163.852c8.949,0,16.2-7.245,16.2-16.2v-210.6 c0-8.941-7.251-16.2-16.2-16.2h-15.299v26.104c0,4.725-3.828,8.554-8.548,8.554c-4.725,0-8.548-3.829-8.548-8.554v-26.104h-27 v26.104c0,4.725-3.829,8.554-8.549,8.554c-4.725,0-8.548-3.829-8.548-8.554v-26.104H413.09v26.104c0,4.725-3.829,8.554-8.549,8.554 c-4.725,0-8.548-3.829-8.548-8.554v-26.104h-27v26.104c0,4.725-3.828,8.554-8.548,8.554c-0.48,0-0.944-0.063-1.403-0.14v-34.518 h9.951v-2.7c0-4.714-3.828-8.543-8.548-8.543c-0.48,0-0.944,0.066-1.403,0.14V71.042c0.771-0.114,1.54-0.243,2.342-0.243h178.2 c8.933,0,16.199,7.27,16.199,16.2v413.103H555.773z" fill="#FFFFFF"/>
                                 <path d="M404.536,168.813c-4.725,0-8.548,3.829-8.548,8.543v2.7h17.102v-2.7C413.084,172.642,409.256,168.813,404.536,168.813z" fill="#FFFFFF"/>
                                 <path d="M454.935,168.813c-4.726,0-8.549,3.829-8.549,8.543v2.7h17.103v-2.7C463.482,172.642,459.654,168.813,454.935,168.813z" fill="#FFFFFF"/>
                                 <path d="M499.036,168.813c-4.725,0-8.548,3.829-8.548,8.543v2.7h17.102v-2.7C507.584,172.642,503.756,168.813,499.036,168.813z" fill="#FFFFFF"/>
                              </g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                              <g></g>
                           </svg>
                        </a>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 text-center text-uppercase down_border_decoration admin_tree">
                     	<div class="row">
                        	<!--<div class="col-xs-10 col-xs-offset-1">
                                <div class="select_plan margin_top_20">
                                    <p class="plan_box">
                                        Plan
                                    </p>
                                    <div class="plan_select">
                                        <select class="blue_border border_radius_12 plan_select_append">
                                            <option value="Select">Select</option>
                                            <option value="Demo">Demo</option>
                                            <option value="FC1">FC1</option>
                                            <option value="FC2">FC2</option>
                                            <option value="">&nbsp;</option>
                                        </select> 
                                    </div>
                                    <div class="add_plan_checkbox">
                                        <label for="add_plan">Fast Pass</label><input type="checkbox" name="add_plan_box" id="add_plan">
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-xs-8 col-xs-offset-2 margin_top_20 cash_paytm_bank">
                                    	<ul class="date_of_payment">
                                             <li class="text-uppercase">Plan</li>
                                            <li class="plan_select">
                                              <?php
                                              if($role=='finance'){
                                              ?>
                                              <select id="query_plan" class="blue_border border_radius_12 plan_select_append" disabled <?= $disabled;?>>
                                                    <option <?php if($query->query_plan==''){echo 'selected';} ?> value="">Select</option>
                                                    <option <?php if($query->query_plan=='Demo'){echo 'selected';} ?> value="Demo">Demo</option>
                                                    <option <?php if($query->query_plan=='FC1'){echo 'selected';} ?> value="FC1">FC1</option>
                                                    <option <?php if($query->query_plan=='FC2'){echo 'selected';} ?> value="FC2">FC2</option>
                                              </select>
                                              <?php
                                              }
                                              else
                                              {
                                              ?>
                                              <select id="query_plan" class="blue_border border_radius_12 plan_select_append" <?= $q_disable;?> <?= $disabled;?>>
                                                    <option <?php if($query->query_plan==''){echo 'selected';} ?> value="">Select</option>
                                                    <option <?php if($query->query_plan=='Demo'){echo 'selected';} ?> value="Demo">Demo</option>
                                                    <option <?php if($query->query_plan=='FC1'){echo 'selected';} ?> value="FC1">FC1</option>
                                                    <option <?php if($query->query_plan=='FC2'){echo 'selected';} ?> value="FC2">FC2</option>
                                              </select>
                                              <?php
                                              }
                                              ?>
                                            </li>
                                        </ul>
                                        <ul class="date_of_payment padding_top_10">
                                            <li class="text-uppercase"><label for="add_plan">Fast Pass</label></li>
                                            <?php
                                            if($role=='finance'){
                                            ?>
                                            <li><input <?php if($query->fast_pass==1){echo 'checked';} ?> type="checkbox" disabled <?= $disabled;?> name="add_plan_box" id="add_plan"></li>
                                            <?php
                                            }
                                            else{
                                            ?>
                                            <li><input <?php if($query->fast_pass==1){echo 'checked';} ?> type="checkbox" <?= $q_disable;?> <?= $disabled;?> name="add_plan_box" id="add_plan"></li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                        </div>
                     	
                     
                        <h2 class="blue_color">Teacher Assignment</h2>
                        <span class="tree_divider blue_background tree_divider_three"><span class="blue_background"></span></span>

                        <div class="row">
                           <div class="col-xs-4 text-center text-uppercase down_border_decoration admin_tree child_tree">
                              <h2 class="blue_color">Query Responses</h2>
                              <ul class="query_respons query_response_teacher">
                                  <!-- Start Tracher List -->
                                <?php
                                $teacher_responses=$query_model->get_teacher_responses($query_id,$role);
                                $teacher_responses = json_decode(json_encode($teacher_responses), true);
                                uasort($teacher_responses, 'sort_by_time');
                                function sort_by_time($a, $b)
                                {
                                  return strtotime($b['internal_rating_date_time']) - strtotime($a['internal_rating_date_time']);
                                }

                                $_data = array();
                                foreach ($teacher_responses as $v) {
                                  if (isset($_data[$v['teacherinst_id']])) {
                                    // found duplicate
                                    continue;
                                  }
                                  // remember unique item
                                  $_data[$v['teacherinst_id']] = $v;
                                }
                                //if you need a zero-based array, otheriwse work with $_data
                                $teacher_responses = array_values($_data);
                                $teacher_responses =  json_decode(json_encode($teacher_responses));
                                foreach($teacher_responses AS $tech_resp){
                                $external_avg_rat=$query_model->get_ratings($tech_resp->teacherinst_id);
                                $profile_slug_teacher = $query_model->get_teacher_profile_slug($tech_resp->teacherinst_id);
                                $teacher_link = home_url()."/teacher/".$profile_slug_teacher;
                                // echo $external_avg_rat['avarage_rating'];
                                ?>
                                 <li id="respid_<?php echo $tech_resp->int_it; ?>" class="active">
                                    <ul>
                                       <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
                                       <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                       <li class="respons_red">
                                          <p><a href="<?= $teacher_link;?>" target="_blank"><?php echo $tech_resp->teacherinst_id; ?></a></p>
                                       </li>
                                       <li class="respons_recommend_match"><a href="#" role="<?php echo $role; ?>" invt="0" intid="<?php echo $tech_resp->int_it; ?>" type="t" ti-id="<?php echo $tech_resp->teacherinst_id; ?>" class="border_radius_12 blue_background color_white fees_modal">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo $tech_resp->average_rating; ?>)</span></a></li>
                                    </ul>
                                 </li>
                                 <?php } ?>
                                 <!-- End Tracher List -->
                                 <!-- Start Student List -->
                                 <?php
                                 $student_responses=$query_model->get_student_responses($query_id,$role);

                                 $student_responses = json_decode(json_encode($student_responses), true);
                                 uasort($student_responses, 'sort_by_time');
                                  
                                  $_data = array();
                                  foreach ($student_responses as $v) {
                                    if (isset($_data[$v['teacherinstid']])) {
                                      // found duplicate
                                      continue;
                                    }
                                    // remember unique item
                                    $_data[$v['teacherinstid']] = $v;
                                  }
                                  //if you need a zero-based array, otheriwse work with $_data
                                  $student_responses = array_values($_data);
                                  $student_responses =  json_decode(json_encode($student_responses));

                                 foreach($student_responses AS $tech_resp){
                                 $external_avg_rat=$query_model->get_ratings($tech_resp->teacherinstid);
                                 $profile_slug_teacher = $query_model->get_teacher_profile_slug($tech_resp->teacherinstid);
                                 $teacher_link = home_url()."/teacher/".$profile_slug_teacher;
                                 ?>
                                 <li id="respid_<?php echo $tech_resp->id; ?>">
                                    <ul>
                                       <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
                                       <!--<li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
                                       <li>
                                          <p><a href="<?= $teacher_link;?>" target="_blank"><?php echo $tech_resp->teacherinstid; ?></a></p>
                                       </li>
                                       <li class="respons_recommend_match"><a href="#" invt="<?php echo $tech_resp->id; ?>" intid="0" type="s" ti-id="<?php echo $tech_resp->teacherinstid; ?>" class="border_radius_12 blue_background color_white fees_modal">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo $tech_resp->average_rating; ?>)</span></a></li>
                                    </ul>
                                 </li>
                                 <?php } ?>
                                 <!-- End Student List -->
                                 </ul>
                              </div>
                     
                              <!-----------------Satart Recomended---------------->
                                 <div class="col-xs-4 text-center text-uppercase down_border_decoration admin_tree child_tree">
                                       <h2 class="blue_color">Recommended</h2>
                                       <ul class="query_respons">
                                          <?php
                                          $prefloc='';
                                          if(@$myhome==1) @$prefloc="Student\'s Location";
                                          if(@$tutorhome==1) @$prefloc="Own Location";
                                          if(@$institute==1 ) @$prefloc="Institute";
                                          if(@$myhome==1 && @$tutorhome==1) @$prefloc="Both own location and student\'s location";
                                          
                                          $teacher_recomended=$query_model->get_recomended_teacher_on_view($query_id,$query->subject_name,$query->standard,$prefloc,$query->student_id);
                                          // print_r($teacher_recomended);
                                          foreach($teacher_recomended AS $res){
                                            $external_avg_rat=$query_model->get_ratings($res->user_id);
                                            $internal_avg_rat=$query_model->get_teacher_rating($res->user_id);
                                            if($res->user_type == "teacher")
                                            {
                                              $profile_slug_teacher = $query_model->get_teacher_profile_slug($res->user_id);
                                              $teacher_link = home_url()."/teacher/".$res->profile_slug;  
                                            }
                                            else if($res->user_type == "institution")
                                            {
                                              //$profile_slug_teacher = $query_model->get_teacher_profile_slug($res->user_id);
                                              $teacher_link = home_url()."/institute/".$res->profile_slug;  
                                            }
                                            
                                          ?>
                                          <li ><!--class="active"-->
                                             <ul>
                                                <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
                                                <!--<li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
                                                <li>
                                                   <p><a href="<?= $teacher_link;?>" target="_blank"><?php echo $res->user_id; ?></a></p>
                                                </li>
                                                <li class="respons_recommend_match"><a href="#" type="t" ti-id="<?php echo $res->new_user_id; ?>" class="get_rating border_radius_12 blue_background color_white">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo @$internal_avg_rat[0]->average_rating; ?>)</span></a></li>
                                             </ul>
                                          </li>
                                          <?php } ?>
                                          <!--  For Institute   -->
                                          <?php /* ?>
                                          <?php
                                          $institute_recomended=$query_model->get_recomended_institute_on_view($query_id,$query->subject_name,$query->student_id);
                                          foreach($institute_recomended AS $res){
                                           $external_avg_rat=$query_model->get_ratings($res->userid);
                                           $internal_avg_rat=$query_model->get_teacher_rating($res->userid);
                                          ?>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
                                                <!--<li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
                                                <li>
                                                   <p><?php echo $res->userid; ?></p>
                                                </li>
                                                <li class="respons_recommend_match"><a href="#" type="s" ti-id="<?php echo $res->userid; ?>" class="get_rating border_radius_12 blue_background color_white">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo @$internal_avg_rat[0]->average_rating; ?>)</span></a></li>
                                             </ul>
                                          </li>
                                          <?php } ?>
                                          <?php */ ?>
                                       </ul>
                                    </div>
                                 <!-----------------------End Recomended---------------->
                                 <!-----------------------Start Matched---------------->
                                    <div class="col-xs-4 text-center text-uppercase down_border_decoration admin_tree child_tree">
                                       <h2 class="blue_color">Matched</h2>
                                       <ul class="query_respons match_response">
                                          <!--<li class="active">
                                             <ul>
                                                <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
                                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                <li>
                                                   <p>1136</p>
                                                </li>
                                                <li><a href="#" data-toggle="modal" data-target="#fees" class="border_radius_12 blue_background color_white">(4)/(4.5)</span></a></li>
                                             </ul>
                                          </li>-->
                                           <?php
                                          $matched=$query_model->get_matched_teacher_ids($query_id,$query->student_id);

                                            $matched = json_decode(json_encode($matched), true);
                                           uasort($matched, 'sort_by_time');
                                            
                                            $_data = array();
                                            foreach ($matched as $v) {
                                              if (isset($_data[$v['teacherinst_id']])) {
                                                // found duplicate
                                                continue;
                                              }
                                              // remember unique item
                                              $_data[$v['teacherinst_id']] = $v;
                                            }
                                            //if you need a zero-based array, otheriwse work with $_data
                                            $matched = array_values($_data);
                                            $matched =  json_decode(json_encode($matched));

                                           foreach($matched AS $res){
                                           $external_avg_rat=$query_model->get_ratings($res->teacherinst_id);
                                           $internal_avg_rat=$query_model->get_teacher_rating($res->teacherinst_id);
                                          ?>
                                          <li>
                                             <ul>
                                                <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
                                                <!--<li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
                                                <li>
                                                   <p><?php echo $res->teacherinst_id; ?></p>
                                                </li>
                                                <li class="respons_recommend_match"><a href="#" type="t" ti-id="<?php echo $res->teacherinst_id; ?>" class="get_rating border_radius_12 blue_background color_white">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo $res->average_rating; ?>)</span></a></li>
                                             </ul>
                                          </li>
                                          <?php } ?>
                                       </ul>
                                    </div>
                                   <!-----------------------Start Matched---------------->
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12">
                              <!----------------------------------------------------------------Modal For View Query Fees for Teacher------------------------------->
                              <aside class="model_wrapper">
                                 <!--<button data-toggle="modal" data-target="#fees">New</button>-->
                                 <div class="modal fade" id="fees" role="dialog">
                                    <div class="modal-dialog width_400">
                                       <!-- Modal content-->
                                       <div class="modal-content blue_border">
                                          <div class="modal-header fees">
                                             <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                             <h4 class="modal-title text-center">Fees</h4>
                                          </div>
                                          <form action="" method="post" id="fees_content">
                                             <!--    Ajax Content    -->
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </aside>
                              <!------------------------------------------------------------------------------------------------------------------------------------------------->
                              <!---------------------------------------------------------------------------Responses Disapprove Modal-------------------------------------------->
                              <aside class="model_wrapper">
                                 <div class="modal fade" id="response_disapprove_modal" role="dialog" >
                                    <div class="modal-dialog">
                                    <!-- Modal content-->
                                       <div class="modal-content blue_border">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title text-center text-uppercase">Responses Disapprove</h4>
                                          </div>
                                          <div class="modal-body no_padding_top_bottom">
                                             <!--<div class="row">				     -->
                                                <form action="" method="post">
                                                   <input id="idd" type="hidden" value="">
                                                   <input id="ttype" type="hidden" value="">
                                                   <input id="res_email" type="hidden" value="">
                                                   <input id="student_id" type="hidden" value="">
                                                   <input id="teacher_id" type="hidden" value="">
                                                   <input id="role" type="hidden" value="">
                                                   <input id="query_id" type="hidden" value="<?php echo $query_id;?>">
                                                      <!----------use this field ----->
                                                   <textarea style="width:100%;" id="res_disapprove_message" placeholder="Message" class="min_height_130"></textarea>
                                                   <div class="modal-footer no_padding_top_bottom">
                                                   <div id="errortagmsg"></div>
                                                   <input type="button" value="Submit" class="data_submit blue_background color_white" id="subject_frm_rsp_disapprove" >
                                                   </div>
                                                </form>
                                              <!--</div>-->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </aside>
                              <!------------------------------------------------------------------------------------------------------------------------------------------------->
                              <!-------------------------------------------------Get Rating Popup---------------------------------------------------------->
                               <aside class="model_wrapper">
                                 <!--<button data-toggle="modal" data-target="#fees">New</button>-->
                                 <div class="modal fade" id="get_ratings_popup" role="dialog">
                                    <div class="modal-dialog width_400">
                                       <!-- Modal content-->
                                       <div class="modal-content blue_border">
                                          <div class="modal-header fees">
                                             <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                             <!--<h4 class="modal-title text-center">Rating</h4>-->
                                          </div>
                                          <form action="" method="post" id="tech_rating">
                                             <!--    Ajax Content    -->
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </aside>
                              <!---->
                              </div>
                           </div>
                           
                            <!--Start  Finalized Teacher-->
                           <input type="hidden" id="f_student_id" value="<?php echo $query->student_id; ?>"/>
                           <?php
                           $trole='';
                           $profile_slug='';
                           $fina_tech='';
                           $fina_tech=$query_model->check_finalazie_teacher($query_id);
			   //$fina_tech=$fina_techs[0]->teacherinst_id;
                           if($fina_tech!=0){
                            $user = new WP_User( $fina_tech );
                           if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
                               foreach ( $user->roles as $trole );	
                           }
                           
                           if($trole=='teacher'){
                           $profile_slug=home_url().'/teacher/'.$query_model->get_teacher_profile_slug($fina_tech);
			   $plan=$query_model->get_teacher_plan($fina_tech);
                           }
                           else{
                            $profile_slug==home_url().'/institute/'.$query_model->get_institute_profile_slug($fina_tech);
			    $plan='';
                           }
                           }
                           else{
                           $fina_tech=0;   
                           }
                           
                           ?>
                           <input type="hidden" id="inb_student_id" value="<?php echo $query->student_id; ?>"/>
                           <input type="hidden" id="inb_teacher_id" value="<?php if($fina_tech!=0){ echo @$fina_tech; } ?>"/>
                           <input type="hidden" id="sale_id" value="<?php if($sale_id!=0){ echo $sale_id; } ?>"/>
                           
                           <div class="row hide_profile" <?php if($fina_tech!=0){echo 'style="display:none;"';} ?>>
                              <div class="col-xs-12 text-center text-uppercase margin_top_50 down_border_decoration child_tree admin_tree">
                                 <h2 class="blue_color">Finalized Teacher</h2>
                                 <div class="white_background blue_border border_radius_12 text-center finalized_teacher">
                                 	<div class="padding20">
                                        <input type="text" id="f_teacher_id" name="enter_query_id" placeholder="Enter ID here" value="<?php if($fina_tech!=0){ echo @$fina_tech; } ?>" class="blue_border admin_white_background border_radius_12 text-center" <?= $readonly;?>>  
                                        <?php if($role=='administrator' || $role=='sales'){ ?>
					<button <?= $disabled;?> <?= $q_disable;?> type="button" class="blue_background color_white text-uppercase border_none border_radius_12" id="final_teacher">Update</button>
                                        <?php } ?>
					</div>
                                 </div>
                              </div>
                           </div>
                           <div class="row show_profile" <?php if($fina_tech!=0){echo 'style="display:block;"';}else{echo 'style="display:none;"';} ?>>
                              <div class="col-xs-12 text-center text-uppercase margin_top_50 down_border_decoration child_tree admin_tree">
                                 <h2 class="blue_color">Finalized Teacher</h2>
                                 <div class="white_background blue_border border_radius_12 text-center finalized_teacher" id="finalized_teacher_content">
                                 	<div class="h1_heading overflow_hidden">
                                    	<h5 class="admin_menu_background top_radius">Teacher ID - <?php echo $fina_tech; ?></h5>
                                        <p class="plan_ab_otp">
                                             <strong>Cycle:</strong>
                                             <select t-type="<?php echo $trole; ?>" ti-id="<?php echo $fina_tech; ?>" class="blue_border border_radius_12 <?php if($role=='administrator' || $role=='sales'){ ?>teacher_plan <?php } ?>" <?= $disabled;?>>
                                              <option <?php if($plan=='Plan A'){echo "selected";} ?> value="Plan A">Plan A</option>
                                              <option <?php if($plan=='Plan B'){echo "selected";} ?> value="Plan B">Plan B</option>
                                              <option <?php if($plan=='Plan C'){echo "selected";} ?> value="Plan C">Plan C</option>
                                              <option <?php if($plan=='Plan D'){echo "selected";} ?> value="Plan D">Plan D</option>
                                              <option <?php if($plan=='Plan OTP'){echo "selected";} ?> value="Plan OTP">Plan OTP</option>
                                            </select> 
                                        </p>
                                        <ul class="finalized_teacher_profile_details margin_top_10">
                                        	<li><a href="<?php echo $profile_slug; ?>" class="admin_menu_background color_white text-center border_radius_12" <?= $disabled;?>>View Profile</a></li>
						<?php if($role=='administrator' || $role=='sales'){ ?>
                                                <li><a href="#" id="change_final_teacher" class="admin_menu_background color_white text-center border_radius_12" <?= $q_disable;?> <?= $disabled;?>>Change</a></li>
						<?php } ?>
                                        </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                            <!--End  Finalized Teacher-->
                            <!--Start Inbound Payment-->
                          <form method="post" id="inbound_form">
                           <?php
                           $payment_type_class='';
                           $inbound_data = $query_model->get_update_inbound_payment($query_id);
                           //echo "<pre>"; print_r($inbound_data); echo "</pre>";
                           //if(count($inbound_data)==0){
                           // $inbound_data=array();  
                           //
                           ?>
                           <div class="row">
                              <div class="col-xs-10 col-xs-offset-1 text-center text-uppercase margin_top_50 down_border_decoration child_tree admin_tree">
                                 <h2 class="blue_color">Inbound Payment</h2>
                                 <span class="tree_divider blue_background"><span class="blue_background"></span><strong class="blue_background"></strong></span>
                                 <div class="row">
                                    <input type="hidden" value="<?php echo $inbound_data[0]->payment_by; ?>" id="inbound_payment_type"/>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="1" class="border_radius_30 blue_border <?php if($inbound_data[0]->payment_by==1){echo 'active';} ?> text-uppercase inbound_payment_type cash_box_show_hide">Cash</button>
                                        <div class="approve_box_with_button cash_box" <?php if($inbound_data[0]->payment_by==1){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?> >
                                             <textarea id="inbound_cash" type="text" placeholder="Distributor's Name" class="border_radius_12 blue_border" <?= $readonly;?>><?php if($inbound_data[0]->payment_by==1){echo $inbound_data[0]->payment_by_details; } ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border show_hide_1">Save</button>-->
                                        </div>
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="2" class="border_radius_30 blue_border <?php if($inbound_data[0]->payment_by==2){echo 'active';} ?> text-uppercase inbound_payment_type paytm_box_show_hide">Paytm</button>
                                        <div class="approve_box_with_button paytm_box" <?php if($inbound_data[0]->payment_by==2){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea id="inbound_pytm" type="text" placeholder="Mobile Number" class="border_radius_12 blue_border" <?= $readonly;?>><?php if($inbound_data[0]->payment_by==2){echo $inbound_data[0]->payment_by_details;} ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border">Save</button>-->
                                        </div>
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="3" class="border_radius_30 blue_border <?php if($inbound_data[0]->payment_by==3){echo 'active';} ?> text-uppercase inbound_payment_type bank_box_show_hide">Bank</button>
                                        <div class="approve_box_with_button bank_box" <?php if($inbound_data[0]->payment_by==3){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea id="inbound_bank" type="text" placeholder="Bank Details" class="border_radius_12 blue_border" <?= $readonly;?>><?php if($inbound_data[0]->payment_by==3){echo $inbound_data[0]->payment_by_details;} ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border">Save</button>-->
                                        </div>
                                    </div>
                                     <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="4" class="border_radius_30 blue_border <?php if($inbound_data[0]->payment_by==4){echo 'active';} ?> text-uppercase inbound_payment_type card_box_show_hide">Card</button>
                                        <div class="approve_box_with_button card_box" <?php if($inbound_data[0]->payment_by==4){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea id="inbound_card" type="text" placeholder="Card Details" class="border_radius_12 blue_border" <?= $readonly;?>><?php if($inbound_data[0]->payment_by==4){echo $inbound_data[0]->payment_by_details; } ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border">Save</button>-->
                                        </div>
                                    </div>
                                 </div>
                                 
                                 <?php /*?><div class="row margin_top_20">
                                    <input type="hidden" value="<?php echo @$inbound_data[0]->fees_type; ?>" id="inbound_payment_fee"/>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_demo">Demo</label><input class="inbound_fees" <?php if(@$inbound_data[0]->fees_type==1){echo 'checked';} ?> type="radio" required value="1" id="inbound_payment_demo" name="inbound_fees">
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_fees">Fees</label><input class="inbound_fees" <?php if(@$inbound_data[0]->fees_type==2){echo 'checked';} ?> type="radio" required value="2" id="inbound_payment_fees" name="inbound_fees">
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_instant">Instant</label><input class="inbound_fees" <?php if(@$inbound_data[0]->fees_type==3){echo 'checked';} ?> type="radio" required value="3" id="inbound_payment_instant" name="inbound_fees">
                                    </div>
                                     <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<label for="inbound_payment_fastpass">Fast Pass</label><input class="inbound_fees" <?php if(@$inbound_data[0]->fees_type==4){echo 'checked';} ?> type="radio" required value="4" id="inbound_payment_fastpass" name="inbound_fees">
                                    </div>
                                 </div><?php */?>
                                 
                                 <div class="row margin_top_110">
                                 	<div class="col-xs-12 margin_top_20 cash_paytm_bank">
                                    	<ul class="date_of_payment">
                                             <li class="text-uppercase">Expected Date of Payment</li>
                                            <li>
                                              <input <?= $readonly;?> type="text" required value="<?php if($inbound_data[0]->exp_date_payment!=''){ echo date('d M Y',strtotime($inbound_data[0]->exp_date_payment)); }?>" id="exp_date_payment" style="border: none;background: #eaedf2;"/>
                                             <!--<button type="button" <?= $disabled;?>><i class="fa fa-calendar" id="exp_date_payment_but" aria-hidden="true"></i></button>-->
                                            </li>
                                        </ul>
                                        <ul class="date_of_payment">
                                             <li class="text-capitalize">Fees</li>
                                            <li><input <?= $readonly;?> type="text" id="payment_fees" value="<?php echo $inbound_data[0]->fees; ?>" required class="assign" autocomplete="off"></li>
                                        </ul>
                                        <?php /*?><ul class="date_of_payment">
                                        	<li class="text-capitalize">Cycle</li>
                                            <li>
                                            	<select id="inbound_payment_cycle" required name="payment_cycle" class="assign apperence_none">
                                                    <option value=""></option>
                                                    <option <?php if($inbound_data[0]->cycle=='Monthly'){echo 'selected';} ?> value="Monthly">Monthly</option>
                                                    <option <?php if($inbound_data[0]->cycle=='50-50'){echo 'selected';} ?> value="50-50">50-50</option>
                                                    <option <?php if($inbound_data[0]->cycle=='One-Time'){echo 'selected';} ?> value="One-Time">One-Time</option>
                                                    <option <?php if($inbound_data[0]->cycle=='Other'){echo 'selected';} ?> value="Other">Other</option>
                                                 </select>
                                            </li>
                                        </ul><?php */?>
                                    </div>
                                 </div>
                                 
                                 <div class="row margin_top_20">
                                    <div class="col-xs-12 margin_top_20 cash_paytm_bank_update">
                                    	<p>Update on <span id="show_update_on_date"><?php if(trim($inbound_data[0]->updated_on)!=''){ echo date('Y-m-d h:i:s',strtotime(trim($inbound_data[0]->updated_on))); } ?></span><!--26th June 2017 11.33 <span class="text-uppercase">AM</span>--></p>
                                        <div class="border_radius_12">
					  <?php if($role=='administrator' || $role=='finance'){ ?>
					  <input <?= $disabled;?> type="checkbox" <?php if($inbound_data[0]->is_transferd==1){echo 'checked';} ?> id="cash_trans" name="cash_receive">
					  <?php } elseif($role=='sales'){ ?>
					  <input  disabled type="checkbox" <?php if($inbound_data[0]->is_transferd==1){echo 'checked';} ?> >
					  <?php } ?>
					  <label for="cash_trans">RECEIVED</label></div>
                                    </div>
				    <?php if($role=='administrator' || $role=='finance'){ ?>
                                    <input <?= $disabled;?> type="text" name="invoice_number_update" id="vendor_invoice_number" value="<?php echo $inbound_data[0]->invoice_no; ?>" class="invoice_number_update blue_border blue_color text-center text-uppercase" placeholder="Vendor Invoice Number">
                                    <?php } elseif($role=='sales'){ ?>
				    <input disabled type="text" name="" id="" disabled value="<?php echo $inbound_data[0]->invoice_no; ?>" class="invoice_number_update blue_border blue_color text-center text-uppercase" placeholder="Vendor Invoice Number">
				    <?php } ?>
                                 </div>
                                 <div class="row margin_top_20">
                                    <button <?= $q_disable;?> <?= $disabled;?> type="submit" class="blue_background color_white text-uppercase border_none border_radius_12 button_150" id="update_inbound">Update</button>
                                 </div>
                                 </form>
                                 <!--End Inbound Payment-->
                                 <!--End Outbound Payment-->
                                 <?php
                                 $payment_type_class='';
                                 $outbound_data=$query_model->get_update_outbound_payment($query_id);
                                 if($role=='administrator' || $role=='finance'){
                                  $readonly = '';
                                 }
                                 else{
                                  $readonly = 'readonly';
                                 }
                                 if($query_details[0]->query_status == 'CLOSED'){
                                    $readonly = 'readonly';
                                    $disabled = 'disabled';
                                 }
                                 ?>
                                 <form method="post" id="outbound_form2">

                                    <div class="row margin_top_20">
                                           <div class="col-xs-12 outbound_payment down_border_decoration child_tree">
                                           <h2 class="blue_color">Outbound Payment</h2>
                                           
                                           
                                    <span class="tree_divider blue_background"><span class="blue_background"></span><strong class="blue_background"></strong></span>

				 <div class="row">
                                    <input type="hidden" value="<?php echo $outbound_data[0]->payment_type; ?>" id="outbound_payment_type"/>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="1" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==1){echo 'active';} ?> text-uppercase outbound_payment_type cash_box_show_hide1">Cash</button>
                                        <div class="approve_box_with_button cash_box1" <?php if($outbound_data[0]->payment_type==1){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea <?= $readonly;?> id="outbound_cash" type="text" placeholder="Distributor's Name" class="border_radius_12 blue_border"><?php if($outbound_data[0]->payment_type==1){echo $outbound_data[0]->payment_by_details; }?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border show_hide_1">Save</button>-->
                                        </div>
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="2" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==2){echo 'active';} ?> text-uppercase outbound_payment_type paytm_box_show_hide1">Paytm</button>
                                        <div class="approve_box_with_button paytm_box1" <?php if($outbound_data[0]->payment_type==2){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea <?= $readonly;?> id="outbound_pytm" type="text" placeholder="Mobile Number" class="border_radius_12 blue_border"><?php if($outbound_data[0]->payment_type==2){echo $outbound_data[0]->payment_by_details; } ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border">Save</button>-->
                                        </div>
                                    </div>
                                    <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="3" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==3){echo 'active';} ?> text-uppercase outbound_payment_type bank_box_show_hide1">Bank</button>
                                        <div class="approve_box_with_button bank_box1" <?php if($outbound_data[0]->payment_type==3){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea <?= $readonly;?> id="outbound_bank" type="text" placeholder="Bank Details" class="border_radius_12 blue_border"><?php if($outbound_data[0]->payment_type==3){echo $outbound_data[0]->payment_by_details; } ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border">Save</button>-->
                                        </div>
                                    </div>
                                     <div class="col-xs-3 margin_top_20 cash_paytm_bank">
                                    	<button type="button" value="4" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==4){echo 'active';} ?> text-uppercase outbound_payment_type card_box_show_hide1">Card</button>
                                        <div class="approve_box_with_button card_box1" <?php if($outbound_data[0]->payment_type==4){echo 'style="display:none;"';}else{echo 'style="display:none;"';} ?>>
                                             <textarea <?= $readonly;?> id="outbound_card" type="text" placeholder="Card Details" class="border_radius_12 blue_border"><?php if($outbound_data[0]->payment_type==4){echo $outbound_data[0]->payment_by_details;} ?></textarea>
                                            <!--<button type="button" class="border_radius_30 blue_border">Save</button>-->
                                        </div>
                                    </div>
                                 </div>
                                           
                                           
                                           <ul class="our_teacher_share margin_top_120">
                                               <li><label>Our Share</label> <input <?= $readonly;?> type="number" value="<?php echo @$outbound_data[0]->our_share; ?>" required id="our_share" name="our_share" class="blue_border"></li>
					       <li><label>GST</label> <input <?= $readonly;?> type="number" value="<?php echo @$outbound_data[0]->gst; ?>" required id="gst" name="gst" class="blue_border"></li>
                                               <li><label>Teacher Share</label> <input <?= $readonly;?> type="number" value="<?php echo @$outbound_data[0]->teacher_share; ?>" required id="teacher_share" name="teacher_share" class=" blue_border"></li>
                                           </ul>
                                           <div class="blue_border white_background padding15 margin_top_30 outbond_money_payment hidden">
                                                   <!--<h6 class="border_radius_30 text-uppercase blue_color blue_border">Cash</h6>-->
                                                    <input type="hidden" value="<?php echo $outbound_data[0]->payment_type; ?>" id="outbound_payment_type"/>
                                                   <div class="col-xs-12 margin_top_10 margin_bottom_10 cash_paytm_bank">
                                                       <button type="button" value="1" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==1){echo 'active';} ?> text-uppercase outbound_payment_type">Cash</button>
                                                   </div>
                                               <div class="border_radius_12 blue_color blue_border distributor_name">
                                                   <input type="text" id="distri_name" value="<?php echo @$outbound_data[0]->distributors_name; ?>" placeholder="Distributor's Name">
                                               </div>
                                               <!--<h6 class="border_radius_30 text-uppercase blue_color blue_border">Paytm</h6>-->
                                               <div class="col-xs-12 margin_top_10 margin_bottom_10 cash_paytm_bank">
                                                       <button type="button" value="2" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==2){echo 'active';} ?> text-uppercase outbound_payment_type">Paytm</button>
                                                </div>
                                                <div class="border_radius_12 blue_color blue_border distributor_name">
                                                   <input type="hidden" id="paytm_mob_no" value="<?php echo $outbound_data[0]->paytm_no; ?>" />
                                                   <span id="mob_span"><!--<p class="margin_bottom_10">1234567891 <button type="button" class="border_none blue_color"><i id="paytm_delete" class="fa fa-trash" aria-hidden="true"></i></button></p>--></span>
                                                   <input type="number" id="paytm_no" autocomplete="off" placeholder="Mobile Number">
                                               </div>
                                               <!--<h6 class="border_radius_30 text-uppercase blue_color blue_border">Bank</h6>-->
                                               <div class="col-xs-12 margin_top_10 margin_bottom_10 cash_paytm_bank">
                                                       <button type="button" value="3" class="border_radius_30 blue_border <?php if($outbound_data[0]->payment_type==3){echo 'active';} ?> text-uppercase outbound_payment_type">Bank</button>
                                                </div>
                                               <?php
                                               $banks=$query_model->get_outbound_bank_details($query_id);
                                               $bank_count=count($banks);
                                               if($bank_count==0){
                                                $bank_count=1;
                                               }
                                               ?>
                                               <input type="hidden" id="bank_count" value="<?php echo $bank_count; ?>"/>
                                               <div id="bank">
                                                <?php
                                                $i=1;
                                                if(count($banks) > 0){
                                                foreach($banks as $bank){
                                                ?>
                                                   <div class="border_radius_12 blue_color blue_border distributor_name" id="bul<?php echo $i; ?>">
                                                      <ul>
                                                          <li><label>Bank Name:</label> <input type="text" value="<?php echo $bank->bank_name; ?>" name="bank_name<?php echo $i; ?>" id="bank_name"></li>
                                                          <li><label>Account holder's  Name:</label> <input type="text" value="<?php echo $bank->acc_holder_name; ?>" name="acc_holder_name<?php echo $i; ?>" id="acc_holder_name"></li>
                                                          <li><label>Account Number:</label> <input type="text" value="<?php echo $bank->acc_number; ?>" name="acc_number<?php echo $i; ?>" id="acc_number"></li>
                                                          <li><label>IFSC Code:</label> <input type="text" value="<?php echo $bank->ifse_code; ?>" name="ifsc_code" id="ifsc_code<?php echo $i; ?>"></li>
                                                          <li><button idd="<?php echo $i; ?>" type="button" class="admin_blue_background border_none border_radius_30 color_white bank_delete">Delete</button></li>
                                                      </ul>
                                                  </div>
                                                <?php $i++; } }else{ ?>
                                                 <div class="border_radius_12 blue_color blue_border distributor_name" id="bul1">
                                                      <ul>
                                                          <li><label>Bank Name:</label> <input type="text" name="bank_name1" id="bank_name"></li>
                                                          <li><label>Account holder's  Name:</label> <input type="text" name="acc_holder_name1" id="acc_holder_name"></li>
                                                          <li><label>Account Number:</label> <input type="text" name="acc_number1" id="acc_number"></li>
                                                          <li><label>IFSC Code:</label> <input type="text" name="ifsc_code" id="ifsc_code1"></li>
                                                          <li><button idd="1" type="button" class="admin_blue_background border_none border_radius_30 color_white bank_delete">Delete</button></li>
                                                      </ul>
                                                  </div>
                                                <?php } ?>
                                                </div>
                                                <ul>
                                                      <li><button type="button" id="add_bank" class="pull-right admin_blue_background border_none border_radius_30 color_white">Add another Bank</button></li>
                                                </ul>
                                               <?php /*?><div class="row margin_top_20">
                                                   <div class="col-xs-12 cash_paytm_bank_update">
                                                       <div class="border_radius_12"><input type="checkbox" <?php if($outbound_data[0]->is_received==1){echo "checked";} ?> id="cash_receive" name="cash_receive"> <label for="cash_receive">Transferred</label></div>
                                                       <p class="margin_bottom_10">Update on <span id="outbound_update_on"><?php echo date('Y M d h:i a',strtotime($outbound_data[0]->updated_on)); ?></span></p>
                                                   </div>
                                                   <input type="text" name="invoice_number_update" id="customer_invoice_numberate" class="invoice_number_update blue_border blue_color text-center text-uppercase" value="<?php echo $outbound_data[0]->customer_invoice_no; ?>" placeholder="Invoice Number">
                                              </div><?php */?>
                                               
                                           </div>
                                            
                                       </div>
                                          
                                       <!--<button type="button" class="admin_blue_background color_white border_radius_30 border_none text-uppercase margin_top_20 update_query">Update Query</button>-->
                                    </div>
                                    <div class="row margin_top_20">
                                                   <div class="col-xs-12 cash_paytm_bank_update">
                                                       <div class="border_radius_12">
							 <?php if($role=='administrator' || $role=='finance'){ ?>
							 <input <?= $disabled;?> type="checkbox" <?php if($outbound_data[0]->is_received==1){echo "checked";} ?> id="cash_receive" name="cash_receive" required>
							 <?php }else{?>
							 <input disabled type="checkbox" <?php if($outbound_data[0]->is_received==1){echo "checked";} ?> id="" name="">
							 <?php } ?>
							 <label for="cash_receive">Transferred</label></div>
                                                       <p class="margin_bottom_10"> <span id="outbound_update_on"><?php if(trim($outbound_data[0]->updated_on)!='') echo "Update on ".date('Y M d h:i a',strtotime($outbound_data[0]->updated_on)); ?></span></p>
                                                   </div>
						   <?php if($role=='administrator' || $role=='finance'){ ?>
                                                   <input <?= $disabled;?> type="text" name="invoice_number_update" id="customer_invoice_numberate" class="invoice_number_update blue_border blue_color text-center text-uppercase" value="<?php echo $outbound_data[0]->customer_invoice_no; ?>" placeholder="Invoice Number" required>
                                                   <?php }else{?>
						   <input disabled type="text" name="" id="" class="invoice_number_update blue_border blue_color text-center text-uppercase" value="<?php echo $outbound_data[0]->customer_invoice_no; ?>" placeholder="Invoice Number">
						   <?php } ?>
				    </div>
                                    <div class="row margin_top_20">
				       <?php if($role=='administrator' || $role=='finance'){ ?>
                                       <button <?= $q_disable;?> <?= $disabled;?> type="submit" class="blue_background color_white text-uppercase border_none border_radius_12 button_150" id="update_outbound">Update</button>
                                       <?php }else{?>
				       <button disabled type="submit" class="blue_background color_white text-uppercase border_none border_radius_12 button_150" id="">Update</button>
				       <?php } ?>
				    </div>
                                </form>
                                 <!--Start Outbound Payment-->
                              </div>
                           </div>
                        </div>
                     </div>
                  </aside>
               </div>
            </div>
         </div>
      </div>
      
      


    <script type="text/javascript">
		/* For Inbound Payment Show Hide */
    $(".exp_payment_date").prop('disabled',true);
		$(".cash_box_show_hide").click(function(){
			$(".cash_box").show();
			$(".paytm_box").hide();
			$(".bank_box").hide();
			$(".card_box").hide();
		});
		$(".paytm_box_show_hide").click(function(){
			$(".cash_box").hide();
			$(".paytm_box").show();
			$(".bank_box").hide();
			$(".card_box").hide();
		});
		$(".bank_box_show_hide").click(function(){
			$(".cash_box").hide();
			$(".paytm_box").hide();
			$(".bank_box").show();
			$(".card_box").hide();
		});
		$(".card_box_show_hide").click(function(){
			$(".cash_box").hide();
			$(".paytm_box").hide();
			$(".bank_box").hide();
			$(".card_box").show();
		});

/* For Outbound Pament Show Hide */

		$(".cash_box_show_hide1").click(function(){
			$(".cash_box1").show();
			$(".paytm_box1").hide();
			$(".bank_box1").hide();
			$(".card_box1").hide();
		});
		$(".paytm_box_show_hide1").click(function(){
			$(".cash_box1").hide();
			$(".paytm_box1").show();
			$(".bank_box1").hide();
			$(".card_box1").hide();
		});
		$(".bank_box_show_hide1").click(function(){
			$(".cash_box1").hide();
			$(".paytm_box1").hide();
			$(".bank_box1").show();
			$(".card_box1").hide();
		});
		$(".card_box_show_hide1").click(function(){
			$(".cash_box1").hide();
			$(".paytm_box1").hide();
			$(".bank_box1").hide();
			$(".card_box1").show();
		});
		
      </script>