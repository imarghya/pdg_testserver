<?php
$user_id = get_current_user_id();
$user = new WP_User( $user_id );
//echo "ROLE : ".$role;


?>
<div id="page-wrapper">
   <div class="container-fluid">
      <div class="row">
         <aside class="col-xs-12 table_content_settings border_radius_12">
               <div class="table-responsive" id="table_data">
               <table class="table admin_query_table_1905" id="table_query">
               <thead>
                  <tr class="text-uppercase">
                               <th></th>
                               <th class="blank_space"></th>
                       <th class="admin_blue_background color_white cell_decorate">Id</th>
                       <th class="admin_blue_background color_white cell_decorate">Sales Rep</th>
                       <th class="admin_blue_background color_white cell_decorate">Fast Pass</th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                               Status 
                               <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                 <form>
                                   <ul class="flag_option_details text-uppercase">
                                       <li class="dropdown-item"><label for="status_option_1">Hot</label><input class="fl_query_status" type="checkbox" name="query_status[]" value="HOT" id="status_option_1"></li>
                                       <li class="dropdown-item"><label for="status_option_2">Baked</label><input class="fl_query_status" type="checkbox" name="query_status[]" value="BAKED" id="status_option_2"></li>
                                       <li class="dropdown-item"><label for="status_option_3">pay_exp_dt</label><input class="fl_query_status" type="checkbox" name="query_status[]" value="PAY_EXP_DT" id="status_option_3"></li>
                                       <li class="dropdown-item"><label for="status_option_4">Pay_rec</label><input class="fl_query_status" type="checkbox" name="query_status[]" value="PAY_REC" id="status_option_4"></li>
                                       <li class="dropdown-item"><label for="status_option_5">paid_to</label><input class="fl_query_status" type="checkbox" name="query_status[]" value="PAID_TO" id="status_option_5"></li>
                                       <li class="dropdown-item"><label for="status_option_6">closed</label><input class="fl_query_status" type="checkbox" name="query_status[]" value="CLOSED" id="status_option_6"></li>
                                   </ul>
                                  </form>
                             </div>
                           </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                           Flag 
                           <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                 <form method="post">
                                   <ul class="flag_option_details">
                                       <li class="dropdown-item"><label for="flag_option_1">Very Urgent</label><input class="fl_flag_status" type="checkbox" name="flag_option[]" value="Very Urgent" id="flag_option_1"></li>
                                       <li class="dropdown-item"><label for="flag_option_2">Urgent</label><input class="fl_flag_status" type="checkbox" name="flag_option[]" value="Urgent" id="flag_option_2"></li>
                                       <li class="dropdown-item"><label for="flag_option_3">Normal</label><input class="fl_flag_status" type="checkbox" name="flag_option[]" value="Normal" id="flag_option_3"></li>
                                       <li class="dropdown-item"><label for="flag_option_4">Expired</label><input class="fl_flag_status" type="checkbox" name="flag_option[]" value="Expired" id="flag_option_4"></li>
                                   </ul>
                                  </form>
                             </div>
                           </div>
                       </th>
                       <th class="blank_space"></th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                       Query Dt 
                       <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                 <form method="post">
                                    <ul class="query_date_filter">
                                       <li class="dropdown-item"><label>Start Date : </label><input class="form-control date" id="query_st_date" name="query_st_date" placeholder="MM/DD/YYYY" type="text"/></li>
                                       <li class="dropdown-item"><label>End Date : </label><input class="form-control date" id="query_end_date" name="query_end_date" placeholder="MM/DD/YYYY" type="text"/></li>
                                       <li class="dropdown-item text-center clearfix"><button id="filter_query_date_btn" type="button" class="green">Filter</button><button type="button"  class="red fl_cancel">Cancel</button></li>
                                    </ul>
                                 </form>
                             </div>
                           </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">Lead Source
                        <div class="dropdown">
                        <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                           <form>
                             <ul class="flag_option_details text-uppercase">
                                 <li class="dropdown-item"><label for="">Others</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Others" id="lead_option_1"></li>
                                 <li class="dropdown-item"><label for="">Website</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Website" id="lead_option_2"></li>
                                 <li class="dropdown-item"><label for="status_option_3">Flyers</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Flyers" id="lead_option_3"></li>
                                 <li class="dropdown-item"><label for="">Customer.Ref</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Customer.Ref" id="lead_option_4"></li>
                                 <li class="dropdown-item"><label for="">Teacher.Ref</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Teacher.Ref" id="lead_option_5"></li>
                                 <li class="dropdown-item"><label for="">Exhibition</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Exhibition" id="lead_option_6"></li>
                                 <li class="dropdown-item"><label for="">TRP</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="TRP" id="lead_option_7"></li>
                                 <li class="dropdown-item"><label for="">SMS</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="SMS" id="lead_option_8"></li>
                                 <li class="dropdown-item"><label for="">Social Media</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Social Media" id="lead_option_9"></li>
                                 <li class="dropdown-item"><label for="">Old Requirement</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Old Requirement" id="lead_option_10"></li>
                                 <li class="dropdown-item"><label for="">Facebook Groups</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Facebook Groups" id="lead_option_11"></li>
                                 <li class="dropdown-item"><label for="">Returning Cust</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Returning Cust" id="lead_option_12"></li>
                                 <li class="dropdown-item"><label for="">Posters</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Posters" id="lead_option_13"></li>
                                 <li class="dropdown-item"><label for="">Employee Ref</label><input class="fl_query_lead" type="checkbox" name="query_lead[]" value="Employee Ref" id="lead_option_14"></li>
                             </ul>
                            </form>
                        </div>
                        </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate">Name</th>
                       <th class="admin_blue_background color_white cell_decorate">Contact No</th>
                       <th class="admin_blue_background color_white cell_decorate">Class/Age</th>
                       <th class="admin_blue_background color_white cell_decorate">Category</th>
                       <th class="admin_blue_background color_white cell_decorate subject_line_break">Subject</th>
                       <th class="admin_blue_background color_white cell_decorate">Locality</th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                               Follow up dt 
                               <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                 <form method="post">
                                               <ul class="query_date_filter">
                                               <li class="dropdown-item"><label>Start Date : </label><input class="form-control date" id="follow_up_st_date" name="follow_up_st_date" placeholder="MM/DD/YYYY" type="text"/></li>
                                           <li class="dropdown-item"><label>End Date : </label><input class="form-control date" id="follow_up_end_date" name="follow_up_end_date" placeholder="MM/DD/YYYY" type="text"/></li>
                                           <li class="dropdown-item text-center clearfix"><button id="filter_followup_date_btn" type="button" class="green">Filter</button><button type="button"  class="red fl_cancel">Cancel</button></li>
                                       </ul>
                                 </form>
                             </div>
                           </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate">Details</th>
                       <th class="admin_blue_background color_white cell_decorate">Roc</th>
                       <th class="admin_blue_background color_white cell_decorate">Review</th>
                  </tr>
               </thead>
               <tbody id="queru_list">
               <input type="text" id="query_selected_ids" value="0" style="display: none;" />
               <!--        Start Queries Loop        -->
               <?php //print_r($queries); ?>
               <?php foreach($queries AS $query){
                 // echo $query->days;
                  $fast_pass=$query->fast_pass;
                  
                  $lead_source='Flyers';
                  if($query->lead_source==0){
                   $lead_source='';  
                  }
                  //For flag status---------------------------------
                  $flag='';
                  $system_date=time();
                  $query_stsrt_date=strtotime($query->start_date);
                  $datediff = $query_stsrt_date - $system_date;
                  $days= floor($datediff / (60 * 60 * 24));
                 // echo $days;
                  if($days <=3 && $query_stsrt_date > $system_date ){
                   $flag='Very Urgent';  
                  }
                  elseif($days > 3 && $days <= 7 && $query_stsrt_date > $system_date){
                   $flag='Urgent';   
                  }
                  elseif($days > 7 && $query_stsrt_date > $system_date){
                   $flag='Normal';   
                  }
                  else{
                   $flag='Expired';     
                  }
                  //------------------------------------------------
                  //For Review Status-------------------------------
                  $is_review=$query->is_review;
                  //------------------------------------------------
                  //Get Locality------------------------------------
                  $query_model = new ModelQuery();
	          $res = $query_model->get_q_locality($query->localities);
                  //$locality=implode(",",$res);
                  $locality=$res[0];
                  //------------------------------------------------
                  $standard_age='';
                  if($query->standard!='' && $query->age!=0){
                   $standard_age= $query->standard.'/'.$query->age.' Years';
                  }
                  elseif($query->standard!='' && $query->age==0){
                   $standard_age= $query->standard;  
                  }
                  else{
                    $standard_age= $query->age.' Years';   
                  }
                ?>
               <!--      active_query         -->
                  <tr id="query_tr_<?php echo $query->id; ?>" class="wenk-align--center wenk-length--large query_tr" data-wenk="">
                       <td class="cell_decorate squere_width"><input type="checkbox" class="query_id" id="query<?php echo $query->id; ?>" name="query_id[]" value="<?php echo $query->id; ?>" /><label for="query<?php echo $query->id; ?>"></label></td>
                       <td class="blank_space"></td>
                       <td class="text-uppercase text-center cell_decorate"><?php echo $query->id; ?></td>
                       <td class="text-uppercase text-center cell_decorate" id="sales_rep_name_<?php echo $query->id; ?>"><?php echo $query->sales_rep_name; ?></td>
                       <td class="text-uppercase text-center cell_decorate fast_pass_select">
                        <select class="fast_pass" query-id="<?php echo $query->id; ?>">
                           <option <?php if($fast_pass=='2'){echo "selected";} ?> value="1" class="it"></option>
                           <option <?php if($fast_pass==1){echo "selected";} ?> value="1" class="it">Yes</option>
                           <option <?php if($fast_pass==0){echo "selected";} ?> value="0" class="it">No</option>
                        </select>
                       </td>
                       <td class="text-uppercase text-center cell_decorate fast_pass_select">
                        <?php 
                        //$query->query_status;
                        if($user->roles[0] == "administrator")
                        {
                        ?>
                        <select class="query_ststus" query-id="<?php echo $query->id; ?>">
                           <option <?php if($query->query_status==''){echo "selected";} ?> value="" class="it"></option>
                           <option <?php if($query->query_status=='HOT'){echo "selected";} ?> value="HOT" class="it">Hot</option>
                           <option <?php if($query->query_status=='BAKED'){echo "selected";} ?> value="BAKED" class="it">Baked</option>
                           <option <?php if($query->query_status=='PAY_EXP_DT'){echo "selected";} ?> value="PAY_EXP_DT" class="it">Pay_exp_dt</option>
                           <option <?php if($query->query_status=='PAY_REC'){echo "selected";} ?> value="PAY_REC" class="it">Pay_rec</option>
                           <option <?php if($query->query_status=='PAID_TO'){echo "selected";} ?> value="PAID_TO" class="it">Paid_to</option>
                           <option <?php if($query->query_status=='CLOSED'){echo "selected";} ?> value="CLOSED" class="it">Closed</option>
                        </select>
                        <?php
                        }
                        else
                        {
                          echo $query->query_status;
                        }
                        ?>
                       </td>
                       <td class="text-center cell_decorate"><?php echo $flag; ?><?php //echo $days; ?></td>
                       <td class="blank_space"></td>
                       <td class="text-uppercase text-center cell_decorate">
                       <?php
                       	//echo $query->querydate;
                       	$utctime = strtotime('+7 hour',strtotime($query->querydate .' UTC'));
                        $dateInLocal = date("Y-m-d H:i:s", $utctime);
                        echo $query_date = date("Y-m-d H:i:s A",strtotime($dateInLocal));
                       ?>
                       <!--<input class="form-control" name="date" placeholder="07/28/2017" type="text" value="<?php echo $query->start_date; ?>"/>--></td>
                       <td class="text-center cell_decorate">
		
						<?php /*?><?php echo $lead_source; ?><?php */?>
                        
                        <select class="lead_source" query-id="<?php echo $query->id; ?>">
                           <option <?php if($query->lead_source=='Others'){echo "selected";} ?> value="Others">Others</option>
                           <option <?php if($query->lead_source=='Website'){echo "selected";} ?> value="Website">Website</option>
                           <option <?php if($query->lead_source=='Flyers'){echo "selected";} ?> value="Flyers">Flyers</option>
                           <option <?php if($query->lead_source=='Customer.Ref'){echo "selected";} ?> value="Customer.Ref">Customer.Ref</option>
                           <option <?php if($query->lead_source=='Teacher.Ref'){echo "selected";} ?> value="Teacher.Ref">Teacher.Ref</option>
                           <option <?php if($query->lead_source=='Exhibition'){echo "selected";} ?> value="Exhibition">Exhibition</option>
                           <option <?php if($query->lead_source=='TRP'){echo "selected";} ?> value="TRP">TRP</option>
                           <option <?php if($query->lead_source=='SMS'){echo "selected";} ?> value="SMS">SMS</option>
                           <option <?php if($query->lead_source=='Social Media'){echo "selected";} ?> value="Social Media">Social Media</option>
                           <option <?php if($query->lead_source=='Old Requirement'){echo "selected";} ?> value="Old Requirement">Old Requirement</option>
                           <option <?php if($query->lead_source=='Facebook Groups'){echo "selected";} ?> value="Facebook Groups">Facebook Groups</option>
                           <option <?php if($query->lead_source=='Returning Cust'){echo "selected";} ?> value="Returning Cust">Returning Cust</option>
                           <option <?php if($query->lead_source=='Posters'){echo "selected";} ?> value="Posters">Posters</option>
                           <option <?php if($query->lead_source=='Employee Ref'){echo "selected";} ?> value="Employee Ref">Employee Ref</option>
                        </select>
                        </td>
                       <td class="text-center cell_decorate"><?php echo $query->student_name; ?></td>
                       <td class="text-center cell_decorate"><?= ($query->contact_no != ""?$query->contact_no:$query->mobile_no);?></td>
                       <td class="text-center cell_decorate"><?php echo $standard_age; ?></td>
                       <td class="text-center cell_decorate"><?php echo $query->course_type; ?></td>
                       <td class="text-uppercase text-center cell_decorate subject_line_break"><?php echo $query->subject_name; ?></td>
                       <td class="text-uppercase text-center cell_decorate"><?php echo $locality; ?></td>
                       <td class="text-uppercase text-center cell_decorate follow_up_date_td"><input class="form-control follow_up_date" query-id="<?php echo $query->id; ?>" sales-rep="<?php echo $query->current_sales_rep_id; ?>" current-date="<?php echo $query->follow_up_date; ?>" value="<?php echo $query->follow_up_date; ?>" type="text"/></td>
                       <td class="text-center cell_decorate"><a href="<?php echo home_url('/dashboard').'?page=view_query&query_id='.$query->id; ?>" class="admin_blue_background color_white border_radius_12">View</a></td>
                       <td class="text-center cell_decorate"><?php //if($query->roc==''){ ?>
                        <?php
                        if($user->roles[0] == "administrator")
                        {
                        ?>
                        <input type="text" q-id="<?php echo $query->id; ?>" value="<?php echo $query->roc; ?>" readonly class="assign roc_assign" data-toggle="modal" data-target="#roc">
                        <?php
                        }
                        else
                        {
                        ?>
                        <input type="text" q-id="<?php echo $query->id; ?>" value="<?php echo $query->roc; ?>" readonly class="assign roc_assign">
                        <?php
                        }
                        ?>
                        <?php //}else{echo $query->roc; } ?></td>
                       <td class="text-uppercase text-center cell_decorate fast_pass_select">
                        <select class="review" query-id="<?php echo $query->id; ?>">
                           <option <?php if($is_review==0){echo "selected";} ?> value="0" class="it">No</option>
                           <option <?php if($is_review==1){echo "selected";} ?> value="1" class="it">Yes</option></select>
                       </td>
                  </tr>
                  <?php } ?>
               </tbody>
            </table>
            
              
                 <script>
                  $(document).ready(function(){
                   //Show datable on qyery list----------------------------------
                   var table=$('#table_query').DataTable({
                  "ordering": false,
                   "sDom":"ltipr"
                  });
                  $('#srch-term').on('keyup',function(){
                        table.search($(this).val()).draw();
                  });
                  //------------------------------------------------------------ 
                  });
                 </script>      
            </div>
            
         </aside>
      </div>
   </div>
       
            <?php echo $this->load_view('controlpanel/queries/query_modal'); ?>
</div>
