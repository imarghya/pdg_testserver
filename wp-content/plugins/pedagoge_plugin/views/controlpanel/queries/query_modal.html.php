<!------------------Query Closer Model------------------>
<aside class="model_wrapper">
      <div class="modal fade" id="roc" role="dialog">
      <div class="modal-dialog">
       <!-- Modal content-->
	<div class="modal-content blue_border">
	  <div class="modal-header">
	    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
	    <h4 class="modal-title text-center text-uppercase">Roc</h4>
	  </div>
	  <form action="#" method="post" id="form_roc">
	  <div class="modal-body no_padding_top_bottom">
	  <div class="row">
	   <div class="col-sm-12 subject_type">
	      <textarea id="reasonCloser" name="reasonCloser" placeholder="Reason of closer" required class="non_grid_input black_border" ></textarea>
	   </div>
	   </div>
	  </div>
	  <div class="modal-footer no_padding_top_bottom">
	    <input type="submit" value="Submit" class="data_submit blue_background color_white">
	  </div>
	  </form>
	</div>
     </div>
</div>
</aside>
<!----------------------------------------------->
<!--     Start Modal For Query Disapprove      -->
<aside class="model_wrapper">
<div class="modal fade" id="modal_query_disapprove" role="dialog">
<div class="modal-dialog">
 <!-- Modal content-->
  <div class="modal-content blue_border">
    <div class="modal-header">
      <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      <h4 class="modal-title text-center text-uppercase">Query Disapprove</h4>
    </div>
    <form action="" method="post" id="form_query_disapprove">
    <div class="modal-body no_padding_top_bottom">
    <div class="row">
     <div class="col-sm-12 subject_type">
	<textarea id="query_disapprove_message" name="query_disapprove_message" required placeholder="Message for disapprove." class="non_grid_input black_border" ></textarea>
     </div>
     </div>
    </div>
    <div class="modal-footer no_padding_top_bottom">
      <input type="submit" value="Submit" class="data_submit blue_background color_white">
    </div>
    </form>
  </div>
</div>
</div>
</aside>
<!--     End Modal For Query Disapprove      -->
<!--------------------Edit Query Modal-------------------------->
<aside class="model_wrapper edit_query">
   <!--<button data-toggle="modal" data-target="#fees">New</button>-->
   <div class="modal fade" id="edit_query_modal" role="dialog">
      <div class="modal-dialog width_600">
	 <!-- Modal content-->
	 <div class="modal-content blue_border">
	   
	    <form method="post" id="edit_query_form">
	       <div class="modal-body no_padding_top_bottom">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  <div class="row" id="edit_query_data">
		     <!-------Edit Query Content Here--------->
		  </div>
	       </div>
	    </form>
	 </div>
      </div>
   </div>
</aside>
<!------------------------------------------------------>
<!----------------------Clone Query---------------------> 
<aside class="model_wrapper edit_query">
   <!--<button data-toggle="modal" data-target="#fees">New</button>-->
   <div class="modal fade" id="clone_query_modal" role="dialog">
      <div class="modal-dialog width_600">
	 <!-- Modal content-->
	 <div class="modal-content blue_border">
	    <form method="post" id="clone_query_form">
	       <div class="modal-body no_padding_top_bottom">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		  <div class="row" id="clone_query_data">
                    <!-------Clone Query Content Here--------->
		  </div>
	       </div>
	    </form>
	 </div>
      </div>
   </div>
</aside>
<!--------------------------------------------------->

<!----------------New Query Start-------------------->                            
<!--------------------------------------------tags start------------------------------------------------------>
<aside class="model_wrapper">
   <div class="modal fade" id="subjectChoose" role="dialog" >
      <div class="modal-dialog">
      <!-- Modal content-->
	 <div class="modal-content blue_border">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title text-center text-uppercase">Choose the subjects/activities you're looking Tutor for</h4>
	    </div>
	    <div class="modal-body no_padding_top_bottom">
	       <div class="row">				     
		  <form action="" method="post">
		     <input name="mytags" id="mySingleField" type="hidden"><!----------use this field ----->
		     <ul id="singleFieldTags"></ul>
		     <div class="modal-footer no_padding_top_bottom">
		     <div id="errortagmsg"></div>
		     <input type="button" value="Submit" class="data_submit blue_background color_white"  data-toggle="modal"  data-dismiss="modal" id="subject_frm_button" >
		     </div>
		  </form>
		</div>
	    </div>
	 </div>
      </div>
   </div>
</aside>

<span id="new_query_modal_item">
<!--New query modal content here-->
</span>				   
<!-----------------------------------------------------------------New Query End------------------------------------------------------>

<?php
$query_model = new ModelQuery();
$sub_tab=$query_model->get_subject_list_tag();
?>
 <script>	
$(function() {   
var country_list=[<?php echo $sub_tab; ?>];		
$('#singleFieldTags').tagit({
    availableTags: country_list,              
    singleField: true,
    singleFieldNode: $('#mySingleField'),
    allowSpaces: true
});			
$('#readOnlyTags').tagit({
    readOnly: true
});
	    
	    
   var currentlyValidTags = [<?php echo $sub_tab; ?>];
   $('#singleFieldTags').tagit({
    availableTags: currentlyValidTags,
    autocomplete: { source: function( request, response ) {        
        var filter = removeDiacritics(request.term.toLowerCase());
        response( $.grep(currentlyValidTags, function(element) {
						
                        return (element.toLowerCase().indexOf(filter) === 0);
						
                    }));
    }},  
    beforeTagAdded: function(event, ui) {
      if ($.inArray(ui.tagLabel, currentlyValidTags) == -1) {
		$("#errortagmsg").html("<font color='red'>Subject not exist</font>");
        return false;
      }else{
		  
		 $("#errortagmsg").html("");
	  }
    }          
  });
   
 //------------------------------------
});    
</script>	
<script>
      $(function() {
	      $('input').keyup(function() {					
		      
		      $txt=this.value;
		      
		      if($txt.length==0){
			      $("#errortagmsg").html("");
		      }else{
			      
			      $("#errortagmsg").html("<font color='red'>Subject not exist</font>");
		      }
      
	      });
      });
</script>