<?php
$query_id=$_GET['query_id'];
$query_model = new ModelQuery();
$sms_template = $query_model->get_sms_template();
$sms_member = $query_model->get_member_contact_by_query($query_id);

//echo "<pre>"; print_r($sms_member['student']); print_r($sms_member['teacher']);echo "</pre>";
?>
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row">
                  <aside class="col-xs-12 border_radius_12 query_model">
                     <div class="inner_container margin_top_10">
                        <div class="row">
                        	<div class="col-xs-12 communication_log">
                            	
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs text-center tab_decoration " role="tablist">
                                    <li role="presentation" class="active"><a href="#communication" aria-controls="communication" role="tab" data-toggle="tab">Communication</a></li>
                                    <li role="presentation"><a href="#activelog" aria-controls="activelog" role="tab" data-toggle="tab">Activity Log</a></li>
                                  </ul>
                                
                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                  
                                    <div role="tabpanel" class="tab-pane active mail_sms_decoration text-center" id="communication"><!--Communication-->
                                            <input id="mailPart" type="radio" name="tabs" checked> <label for="mailPart">Email</label>
                                            <input id="smsPart" type="radio" name="tabs"> <label for="smsPart">SMS</label>
                                        
                                         <div id="content1" class="mail_sms">
                                         	<div class="row">
                                  			<div class="col-xs-10 col-xs-offset-1 text-left">
                                           	<figure>
                                            	<ul class="email_button">
                                                	<li class="active"><button type="button">Send</button></li>
                                                	<li><button type="button">Save Now</button></li>
                                                    <li><button type="button">Discard</button></li>
                                                    <li>
                                                    	<select>
                                                            <option value="">Labels</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                            <option value="lorem">lorem</option>
                                                        </select> 
                                                    </li>
                                                </ul>
                                            	<figcaption>
                                                	<ul class="email_option">
                                                    	<li>
                                                        	<label class="text-uppercase">To</label>
                                                            <select>
                                                                <option value="">Query Holder</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">Template</label>
                                                            <select>
                                                                <option value="">Confarmation Mail</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                                <option value="lorem">lorem</option>
                                                            </select>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">&nbsp;</label>
                                                            <button type="button">Add Cc</button>
                                                            <button type="button">Add Bcc</button>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">Subject</label>
                                                            <input type="text" name="email_subject" id="email_subject">
                                                        </li>
                                                         <li>
                                                        	<label class="text-uppercase">&nbsp;</label>
                                                            <button type="button">Attach a file</button>
                                                            <button type="button"><span>Insert:</span> Invitation</button>
                                                        </li>
                                                    </ul>
												
                                                    <!--For Editor-->
                                                    
                                                    <div id="editor"></div>
                                                    
                                                    <!--For Editor-->
                                        
                                            </figcaption>
                                            </figure>
                                            
                                            </div>
                                            </div>
                                         </div>
                                            
                                         <div id="content2" class="mail_sms">
                                            <div class="row">
                                            	<div class="col-xs-10 col-xs-offset-1 sms_decoration">
                                                	<h5>Compose <span class="text-uppercase">Sms</span> Message</h5>
                                                    <ul class="sms_option">
                                                    	<li>
                                                        	<label class="text-uppercase">Compose form template</label>
                                                            <select class="sms_template">
                                                                <option disabled selected value>Select SMS Template</option>
                                                                <?php
                                                                foreach($sms_template as $s_template){
                                                                ?>
                                                                  <option value="<?= $s_template->id;?>"><?= $s_template->template_name;?></option>  
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </li>
                                                        <li>
                                                        	<label class="text-uppercase">To</label>
                                                            <select class="sms_member">
                                                                <option disabled selected value>Choose Member</option>
                                                                <?php
                                                                if(count($sms_member['student']) > 0)
                                                                {
                                                                ?>
                                                                <option value="<?= $sms_member['student']['mobile_no'];?>">Query Holder(<?= $sms_member['student']['name'];?>)</option>
                                                                <?php
                                                                }
                                                                if(count($sms_member['teacher']) > 0)
                                                                {
                                                                ?>
                                                                  <option value="<?= $sms_member['teacher']['mobile_no'];?>">Finalize Teacher(<?= $sms_member['teacher']['name'];?>)</option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </li>
                                                         <li>
                                                        	<label class="text-uppercase">Message Box <sup>*</sup></label>
                                                            <textarea name="sms" class="sms_text" id="sms"></textarea>
                                                        </li>
                                                        <li>
                                                        	<button type="button">Cancel</button>
                                                            <button type="button" class="sms_send">Send</button>
                                                        </li>
                                                    </ul>
                                                    
                                                </div>
                                            </div>
                                         </div>
                                        
                                    </div><!--Communication-->
                                    
                                    <div role="tabpanel" class="tab-pane" id="activelog"><!--Active Log-->
                                    	<div class="row">
                                        	<div class="col-xs-8 col-xs-offset-2">
                                                <ul class="active_log">
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder.</p> <button type="button" class="admin_blue_background color_white text-uppercase border_none border_radius_12 pull-right">View</button><div class="clearfix"></div></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Finlized Teacher.</p> <button type="button" class="admin_blue_background color_white text-uppercase border_none border_radius_12 pull-right">View Profile</button><div class="clearfix"></div></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder.</p> <button type="button" class="admin_blue_background color_white text-uppercase border_none border_radius_12 pull-right">View</button><div class="clearfix"></div></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder. <a href="#">Hot</a> to <a href="#" class="">Baked</a></p></li>
                                                    <li><span>30 May 17 <strong>11:45 Am</strong></span> <p>An SMS was sent to the Query Holder. <a href="#">Warm</a> to <a href="#" class="">Hot</a></p></li>
                                                </ul>
                                             </div>
                                             <div class="col-xs-2 active_log_plus_button">
                                             	<button type="button" data-toggle="modal" data-target="#admin_add_notes"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                             </div>
                                        </div>
                                    </div><!--Active Log-->
                                 
                                  </div>
                                <a href="<?= get_site_url();?>/dashboard/?page=view_query&query_id=<?= $query_id;?>" class="text-uppercase admin_blue_background color_white goto_viewquery">Back</a>
                            </div>
                        </div>
                     </div>
                  </aside>
                  
                  
                  
                  <aside class="model_wrapper">
                       
                       		<div class="modal fade" id="admin_add_notes" role="dialog">
                                <div class="modal-dialog width_600">
                                 <!-- Modal content-->
                                  <div class="modal-content blue_border admin_blue_background color_white admin_add_notes">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Add Notes</h4>
                                    </div>
                                    <form action="" method="post">
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
                                     <div class="col-sm-12 subject_type">
                                        <textarea id="reasonCloser" name="reasonCloser" class="non_grid_input black_border" ></textarea>
                                     </div>
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <input type="button" value="Submit" class="data_submit color_white pull-right"  data-toggle="modal" data-target="#subjectSelect">
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>

                       </aside>
                  
                  
                  
               </div>
            </div>
         </div>
      </div>
