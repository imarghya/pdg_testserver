<?php
	$str_country_option = '';
	$str_state_option = '';
	$str_city_option = '';
	if(isset($city_data)) {
		foreach($city_data as $city) {
			$str_country_option = '<option value="'.$city->country_id.'">'.$city->country_name.'</option>';
			$str_state_option = '<option value="'.$city->state_id.'">'.$city->state_name.'</option>';
			$str_city_option = '<option value="'.$city->city_id.'">'.$city->city_name.'</option>';
		}
	}

	$student_user_id = '';
	$student_profile_info_id = '';
	$first_name = '';
	$last_name = '';
	if(isset($student_info)) {
		$student_user_id = $student_info->user_id;
		$student_profile_info_id = $student_info->personal_info_id;
		//pedagoge_applog(print_r($student_info, TRUE));
		$first_name = $student_info->first_name;
		$last_name = $student_info->last_name;
	}
?>


<div class="panel panel-success">
	<div class="panel-heading">
		Update your information!
	</div>
	<div class="panel-body">
		<br />
		<form id="frm_students_profile">
			<div class="row form-group">
				<div class="col-md-2">
					<label for="txt_first_name">First Name</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control validate[required]" name="txt_first_name" id="txt_first_name" placeholder="First Name" value="<?php echo $first_name; ?>" />

					<input type="hidden" id="hidden_user_id" value="<?php echo $student_user_id; ?>" />
					<input type="hidden" id="hidden_profile_info_id" value="<?php echo $student_profile_info_id; ?>" />

				</div>
				<div class="col-md-2">
					<label for="txt_last_name">Last Name</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control validate[required]" name="txt_last_name" id="txt_last_name" placeholder="Last Name" value="<?php echo $last_name; ?>" />
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-2">
					<label for="txt_mobile">Mobile</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control validate[required]" name="txt_mobile" id="txt_mobile" placeholder="mobile no" maxlength="10">
				</div>
				<div class="col-md-2">
					<label for="txt_alternative_contact_no">Alternative No</label>
				</div>
				<div class="col-md-4">
					<input type="text" class="form-control" name="txt_alternative_contact_no" id="txt_alternative_contact_no" placeholder="Alternative contact no">
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-2">
					<label for="txt_dob">Date of Birth</label>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-4 col-xs-4">
							<input type="text" id="dob_year" class="form-control positive" placeholder="YYYY" MaxLength="4"/>
						</div>
						<div class="col-md-1 col-xs-1" style="padding-top: 5px;">
							/
						</div>
						<div class="col-md-3 col-xs-3">
							<input type="text" id="dob_month" class="form-control positive" placeholder="MM" MaxLength="2"/>
						</div>
						<div class="col-md-1 col-xs-1" style="padding-top: 5px;">
							/
						</div>
						<div class="col-md-3 col-xs-3">
							<input type="text" id="dob_day" class="form-control positive" placeholder="DD" MaxLength="2"/>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<label for="select_gender">Gender</label>
				</div>
				<div class="col-md-4">
					<select name="select_gender" id="select_gender" class="form-control select_control validate[required]">
						<option value="">Select</option>
						<option value="male">Male</option>
						<option value="female">Female</option>
						<option value="other">Other</option>
					</select>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-4"></div>
				<div class="col-md-4"></div>
				<div class="col-md-4"></div>
			</div>

			<div class="row form-group">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<img id="img_student_picture" src="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/250x250.png" alt="Profile" height="250" width="250" class="img-responsive img-rounded thumb" style="visibility: visible;">
							<input type="hidden" value="<?php echo PEDAGOGE_ASSETS_URL; ?>/images/250x250.png" id="img_student_place_holder" />
						</div>
						<div class="col-md-6">
							<input id="fileinput_student_profile_image" name="fileinput_student_profile_image" type="file" style="display:none;" accept="image/*" />
							<button class="btn btn-info cmd_load_student_image col-md-12"><i class="fa fa-fw fa-upload"></i> Load Image</button>
							<br />
							<hr />
							<div class="col-md-12" id="div_student_profile_img_status">Image Info</div>
							<div id="modal_student_img_cropping" class="hidden_item">
						      <div class="modal_student_img_cropping">
						            <h4>(Scroll on Image to ZoomIn/ZoomOut)</h4>
						            <div>
						              <img id="img_student_cropping" src="" alt="Picture">
						            </div>
						      </div>
						    </div>
						</div>
					</div>
				</div>
				<div class="col-md-6">

					<div class="row form-group">
						<div class="col-md-4"><label for="txt_address">Current Address</label></div>
						<div class="col-md-8"><textarea id="txt_address" name="txt_address" rows="3" class="form-control"></textarea></div>
					</div>

					<div class="row form-group">
						<div class="col-md-4"><label for="select_country">Country</label></div>
						<div class="col-md-8">
							<select name="select_country" id="select_country" class="form-control select_control">
								<?php echo $str_country_option; ?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4"><label for="select_state">State</label></div>
						<div class="col-md-8">
							<select name="select_state" id="select_state" class="form-control select_control">
								<?php echo $str_state_option; ?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4"><label for="select_city">City</label></div>
						<div class="col-md-8">
							<select name="select_city" id="select_city" class="form-control select_control">
								<?php echo $str_city_option; ?>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<img class="center-block hidden_item" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" id="img_password_reset_loader" />
				</div>
				<div class="col-md-6">
					<button class="btn btn-success col-md-12" id="cmd_save_profile"><i class="fa fa-save"></i> Save</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" id="div_save_result"></div>
			</div>
		</form>

	</div>
</div>