
 <!--review recommendation.....-->
      <!--<aside class="container">
         <div class="row">
            <div class="col-xs-12 hidden-xs"><br><br></div>
            <div class="col-xs-12 col-sm-12 col-md-12  wow fadeInDown">
               <div class="col-xs-12 col-sm-3 col-md-2 no_padd mobile_view_review">
                  <p class="col-xs-12 review_new no_padd text-uppercase text-center">Reviews</p>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-1 no_padd mobile_view_review">
                  <p class="col-xs-12 review_new no_padd">40 Recommendation</p>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 no_padd wow fadeInDown mobile_view_review">
                  <p class="col-xs-12 review_new no_padd">80 Review</p>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 no_padd wow fadeInDown mobile_view_review">
                  <p class="col-xs-12 star_new ">
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                     <i class="fa fa-star" aria-hidden="true"></i>
                  </p>
               </div>
            </div>
            <div class="col-xs-12"><br><br></div>
            <div class="clearfix"></div>
         </div>
      </aside>-->
      <!--/review recommendation.....-->
      <!--student review-->
      <!--<aside class="container hidden">
         <div class="row">
            <div class="col-xs-4 col-sm-2 col-md-2 col-xs-offset-4 col-sm-offset-0 col-md-offset-0  wow fadeInDown review_icon">
               <p><img src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/student.png" alt="" style="width:100%;"><label>Student 1</label></p>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-10 student_review_txt text-justify margin_top_20  wow fadeInDown">
               Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </div>
            <div class="col-xs-12"><br><br></div>
            <div class="clearfix"></div>
         </div>
      </aside>-->
      <!--/student review-->
      <!--ask for review-->
      <!--<aside class="container">
         <div class="row">
            <div class="col-xs-12 text-center  wow fadeInDown">
               <div id="myDIV3" style="display:none;" class="review_model_box">
                  <h4>Share</h4>
                  <textarea placeholder="Please Add a Review on my profile. http://profile.pedagoge/teacher"></textarea>
                  <ul>
                     <li><a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/ln.png" alt="Linkdin"></a></li>
                     <li><a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/wt_app.png" alt="Whats App"></a></li>
                     <li><a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/fb.png" alt="Face Book"></a></li>
                     <li><a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/g_plus.png" alt="Google Plus"></a></li>
                  </ul>
               </div>
               <button type="button" class="btn btn-link upload_photo_file" onclick="myFunction3()">+ Ask for reviews</button>
            </div>
            <div class="col-xs-12 hidden-xs"><br><br></div>
            <div class="clearfix"></div>
         </div>
      </aside>-->
      <!--/ask for review-->
 <!--footer-->
      <footer class="pedagoge_footer text-center">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 pedagoge_footer_link">
                  <ul>
                     <li><a href="#">Terms &amp; Conditions</a></li>
                     <li><a href="#">Privacy Policy</a></li>
                     <li><a href="#">Site map</a></li>
                     <li><a href="#">About Us</a></li>
                     <li><a href="#">Business</a></li>
                     <li><a href="#">Contact Us</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Powered by</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l1.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Selected for</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l2.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Selected for</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l3.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Backed by</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l4.png" alt="LOGO"></a>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social">
                  <h3>Follow Us</h3>
                  <ul>
                     <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social pull-right">
                  <h3>Subscribe to our Newsletter</h3>
                  <div class="input-group">
                     <input type="email" class="form-control" placeholder="Enter your email address">
                     <span class="input-group-btn">
                     <button class="btn btn-secondary" type="button">Go</button>
                     </span>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social">
                  <h3 class="hidden-xs">&nbsp;</h3>
                  <p>&copy; Pedagoge 2017</p>
               </div>
            </div>
         </div>
      </footer>
      <?php echo $this->load_view('profile/pvtteacher/modal'); ?>
       <!-- Modal Review Success-->

   </body>
</html>

<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/ie-emulation-modes-warning.js"></script>
<!--<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<!--<script src="<?php //echo plugins_url() ?>/pedagoge_plugin/assets/js/tag-it.min.js"></script>-->
<!--<script type="text/javascript" src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/autoSearchCity.js"></script>-->
<script type="text/javascript" src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.tinyscrollbar.js"></script>

<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.slimscroll.min.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.meanmenu.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/base.js"></script>
<!--checkbox search for Additional Information attachments  -->

<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/holder.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/ie10-viewport-bug-workaround.js"></script>
<!--chosen search attachments-->
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/chosen.jquery.js" type="text/javascript"></script>

<!--  cropper attachments-->
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/cropper.min.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/main.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/fSelect.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/owl.carousel.min.js"></script>
<!--home popover attachmnets-->
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery-popover-0.0.3.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.2/languages/jquery.validationEngine-en.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.2/jquery.validationEngine.min.js"></script>-->
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.charcounter.js"></script>
<script>
   //Custom validation-----------------------------------
   function validateForm() {
      //for profile heading-------------------------------------------
      var ph=document.forms["f_teacher_registration"]["profile_heading"].value;
       if (ph == "") {
        document.getElementById("ph_div").style.border="1px solid red";
        return false;
       }
       else{
        document.getElementById("ph_div").style.border="1px solid green";
       }
      //--------------------------------------------------------------
      //for first name------------------------------------------------
      var fn = document.forms["f_teacher_registration"]["fst_nm"].value;
      if (fn == "") {
          document.getElementById("teacher_reg_name").style.border="1px solid red";
          document.getElementById("fst_nm").style.border="1px solid red";
          return false;
      }
      else{
       document.getElementById("teacher_reg_name").style.border="1px solid green";
       document.getElementById("fst_nm").style.border="1px solid green"; 
      }
    //----------------------------------------------------------------
    //for last name------------------------------------------------
      var ln = document.forms["f_teacher_registration"]["lst_nm"].value;
      if (ln == "") {
          document.getElementById("teacher_reg_name").style.border="1px solid red";
          document.getElementById("lst_nm").style.border="1px solid red";
          return false;
      }
      else{
       document.getElementById("teacher_reg_name").style.border="1px solid green";
       document.getElementById("lst_nm").style.border="1px solid green";
      }
    //----------------------------------------------------------------
    //for gender------------------------------------------------------
    var gen = document.forms["f_teacher_registration"]["teacher_reg_gen"].value;
    if (gen == "") {
          document.getElementById("teacher_reg_gen").style.border="1px solid red";
          return false;
      }
      else{
       document.getElementById("teacher_reg_gen").style.border="1px solid green";
      }
    //----------------------------------------------------------------
    //for Education---------------------------------------------------
    //gad---
    var deg = document.forms["f_teacher_registration"]["gd_degree1"].value;
    if (deg == "") {
          document.getElementById("gd_degree1").style.border="1px solid red";
          document.getElementById("edu_gra").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
      }
      else{
          document.getElementById("gd_degree1").style.border="1px solid green";
          document.getElementById("edu_gra").style.border="1px solid green";
          document.getElementById("teacher_reg_edu").style.border="1px solid green";
      }
      
    var deg_sh = document.forms["f_teacher_registration"]["gd_college1"].value;
    if (deg_sh == "") {
          document.getElementById("gd_college1").style.border="1px solid red";
          document.getElementById("edu_gra").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
      }
      else{
          document.getElementById("gd_college1").style.border="1px solid green";
          document.getElementById("edu_gra").style.border="1px solid green";
          document.getElementById("teacher_reg_edu").style.border="1px solid green";
      }
      
    var deg_yr = document.forms["f_teacher_registration"]["gd_year_of_comp1"].value;
    if (deg_yr == "") {
          document.getElementById("gd_year_of_comp1").style.border="1px solid red";
          document.getElementById("edu_gra").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
      }
      else{
          document.getElementById("gd_year_of_comp1").style.border="1px solid green";
          document.getElementById("edu_gra").style.border="1px solid green";
          document.getElementById("teacher_reg_edu").style.border="1px solid green";
      }
    //------
    //pdg---
    var pdeg = document.forms["f_teacher_registration"]["pgd_degree1"].value;
    var pdeg_sh = document.forms["f_teacher_registration"]["pgd_college1"].value;
    var pdeg_yr = document.forms["f_teacher_registration"]["pgd_year_of_comp1"].value;
    if (pdeg!='' || pdeg_sh!='' || pdeg_yr!='') {
      if (pdeg == "") {
          document.getElementById("pgd_degree1").style.border="1px solid red";
          document.getElementById("edu_pst_gra").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
      }
      else{
          document.getElementById("pgd_degree1").style.border="1px solid green";
          document.getElementById("edu_pst_gra").style.border="1px solid green";
          document.getElementById("teacher_reg_edu").style.border="1px solid green";
      }
     if (pdeg_sh == "") {
          document.getElementById("pgd_college1").style.border="1px solid red";
          document.getElementById("edu_pst_gra").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
      }
      else{
          document.getElementById("pgd_college1").style.border="1px solid green";
          document.getElementById("edu_pst_gra").style.border="1px solid green";
          document.getElementById("teacher_reg_edu").style.border="1px solid green";
      }
      
    var pdeg_yr = document.forms["f_teacher_registration"]["gd_year_of_comp1"].value;
    if (pdeg_yr == "") {
          document.getElementById("pgd_year_of_comp1").style.border="1px solid red";
          document.getElementById("edu_pst_gra").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
      }
      else{
          document.getElementById("pgd_year_of_comp1").style.border="1px solid green";
          document.getElementById("edu_pst_gra").style.border="1px solid green";
          document.getElementById("teacher_reg_edu").style.border="1px solid green";
      }
    }
    //------
    //for other deg--
    var othdeg=document.forms["f_teacher_registration"]["other_quali_valid"].value;
    var othlb=document.forms["f_teacher_registration"]["other_quali"].value;
    if (othdeg!='' && othlb=='') {
          document.getElementById("other_quali").style.border="1px solid red";
          document.getElementById("teacher_reg_edu").style.border="1px solid red";
          return false;
    }
    else{
     document.getElementById("other_quali").style.border="1px solid green";
     document.getElementById("teacher_reg_edu").style.border="1px solid green";
    }
    //---------------
    //----------------------------------------------------------------
    //For City--------------------------------------------------------
    var city=document.forms["f_teacher_registration"]["pdg_city"].value;
    if (city=='') {
      document.getElementById("pdg_city").style.border="1px solid red";
      return false;
    }
    else{
    document.getElementById("pdg_city").style.border="1px solid green";  
    }
    //----------------------------------------------------------------
    //For address of corrspendence------------------------------------
    //house no--
    var house_no=document.forms["f_teacher_registration"]["add_st_hno"].value;
    if (house_no=='') {
      document.getElementById("add_st_hno").style.border="1px solid red";
      document.getElementById("teacher_reg_add").style.border="1px solid red";
      return false;
    }
    else{
    document.getElementById("add_st_hno").style.border="1px solid green";
    document.getElementById("teacher_reg_add").style.border="1px solid green";
    }
    
    //locality--
    var locality=document.forms["f_teacher_registration"]["add_loc"].value;
    if (locality=='') {
      document.getElementById("add_loc").style.border="1px solid red";
      document.getElementById("teacher_reg_add").style.border="1px solid red";
      return false;
    }
    else{
    document.getElementById("add_loc").style.border="1px solid green";
    document.getElementById("teacher_reg_add").style.border="1px solid green";
    }
    
    //Pincode--
    var pincode=document.forms["f_teacher_registration"]["add_pin"].value;
    var pinlen=pincode.toString().length;
    if (pincode=='' || !pincode.match(/^\d+$/)) {
      document.getElementById("add_pin").style.border="1px solid red";
      document.getElementById("teacher_reg_add").style.border="1px solid red";
      return false;
    }
    else if (pinlen > 7) {
      document.getElementById("add_pin").style.border="1px solid red";
      document.getElementById("teacher_reg_add").style.border="1px solid red";
      return false; 
    }
    else{
    document.getElementById("add_pin").style.border="1px solid green";
    document.getElementById("teacher_reg_add").style.border="1px solid green";
    }
    
    //State--
    var state=document.forms["f_teacher_registration"]["state"].value;
    if(state=='') {
      document.getElementById("state").style.border="1px solid red";
      document.getElementById("teacher_reg_add").style.border="1px solid red";
      return false;
    }
    else{
    document.getElementById("state").style.border="1px solid green";
    document.getElementById("teacher_reg_add").style.border="1px solid green";
    }
    //----------------------------------------------------------------
    //For subjects----------------------------------------------------
   var subjj=$("#subjects").val();
   if (subjj!=null) {
    var size_sub1=0;
     var size_sub1=subjj.toString().split(",");
      var size_sub2 = size_sub1.length;
     // alert(size_sub2);
         if(size_sub2 > 8){
            $(".fs-label").css("border", "red solid 1px");
           
            $(".fs-label").val('Only 8 subjects are allowed.');
            return false;
         }
         else {
            $(".fs-label").css("border", "green solid 1px");
            $(".fs-label").val(subjj);
         }
         
     }
     if (subjj==null){
            $(".fs-label").css("border", "red solid 1px");
            $(".fs-label").val(' ');
            return false;
         }
    //----------------------------------------------------------------
    //for Fees--------------------------------------------------------
    var fees=document.forms["f_teacher_registration"]["teacher_reg_fees"].value;
    if (fees=='') {
      document.getElementById("teacher_reg_fees").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("teacher_reg_fees").style.border="1px solid green"; 
    }
    //----------------------------------------------------------------
    //For Contact-----------------------------------------------------
    //contact
    var contact_no=document.forms["f_teacher_registration"]["cnct_no"].value;
    var cont_len=contact_no.toString().length;
    if (contact_no=='' || cont_len!=10 || !contact_no.match(/^\d+$/)) {
      document.getElementById("cnct_no").style.border="1px solid red";
      document.getElementById("teacher_reg_con").style.border="1px solid red";
      return false;  
    }
    else{
     document.getElementById("cnct_no").style.border="1px solid green";
     document.getElementById("teacher_reg_con").style.border="1px solid green"; 
    }
    //alt no--
    var alt_no=document.forms["f_teacher_registration"]["alt_no"].value;
    if (alt_no!='') {
       var alt_no_len=alt_no.toString().length;
     if (!alt_no.match(/^\d+$/)) {
      document.getElementById("alt_no").style.border="1px solid red";
      document.getElementById("teacher_reg_con").style.border="1px solid red";
      return false;  
     }
     else{
      document.getElementById("alt_no").style.border="1px solid green";
      document.getElementById("teacher_reg_con").style.border="1px solid green";
     }
    }
    else{
     document.getElementById("alt_no").style.border="";
     document.getElementById("teacher_reg_con").style.border="1px solid green"; 
    }
    //----------------------------------------------------------------
    //For birth date--------------------------------------------------
    var ddt=document.forms["f_teacher_registration"]["teacher_reg_dob"].value;
    var dd1=document.forms["f_teacher_registration"]["birth_date"].value;
    var mm1=document.forms["f_teacher_registration"]["birth_month"].value;
    var yy1=document.forms["f_teacher_registration"]["birth_year"].value;
    var pattern =/^([0-9]{2})\-([0-9]{2})\-([0-9]{4})$/;
    
    var cd = new Date();
    var fullYear = cd.getFullYear();
    var fullMonth = cd.getMonth()+1;
    var fullDay = cd.getDay();
    
    if (ddt=='' || !pattern.test(ddt) || dd1 > 31 || mm1 > 12 || yy1 > fullYear || yy1=='0000' || dd1=='00' || mm1=='00') {
      document.getElementById("teacher_reg_dob").style.border="1px solid red";
      document.getElementById("teacher_reg_dob").value='';
       return false;
    }
    else{
     document.getElementById("teacher_reg_dob").style.border="1px solid green";
     document.getElementById("teacher_reg_dob").value=ddt;
    }
    //----------------------------------------------------------------
    //For Language----------------------------------------------------
    var lang=document.forms["f_teacher_registration"]["teacher_reg_lang"].value;
    if (lang=='') {
     document.getElementById("teacher_reg_lang").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("teacher_reg_lang").style.border="1px solid green"; 
    }
    //----------------------------------------------------------------
    //For about me----------------------------------------------------
    var about=document.forms["f_teacher_registration"]["teacher_reg_abt"].value;
    if (about=='') {
      document.getElementById("teacher_reg_abt").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("teacher_reg_abt").style.border="1px solid green"; 
    }
    //----------------------------------------------------------------
    //For experience--------------------------------------------------
    var exp=document.forms["f_teacher_registration"]["teacher_reg_exp"].value;
    if (exp=='') {
      document.getElementById("teacher_reg_exp").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("teacher_reg_exp").style.border="1px solid green"; 
    }
    //----------------------------------------------------------------
    //Localities------------------------------------------------------
    var locc=document.forms["f_teacher_registration"]["teacher_reg_locality"].value;
    var loc=document.forms["f_teacher_registration"]["teacher_location"].value;
    if (locc=='') {
     document.getElementById("teacher_reg_locality").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("teacher_reg_locality").style.border="1px solid green"; 
    }
    if($("#willingTravel").prop("checked") == true && loc==''){
      document.getElementById("teacher_reg_locality").style.border="1px solid red";
      document.getElementById("teacher_location").style.border="1px solid red";
      return false;
    }
    else{
      document.getElementById("teacher_reg_locality").style.border="1px solid green";
      document.getElementById("teacher_location").style.border="1px solid green"; 
    }
    if ($("#willingOnline").prop("checked") == true) {
     document.getElementById("teacher_reg_locality").style.border="1px solid green";
    }
    //----------------------------------------------------------------
    //For Class Age---------------------------------------------------
    var class_age=document.forms["f_teacher_registration"]["teacher_reg_classorage"].value;
    var cls_strt=document.forms["f_teacher_registration"]["cls_strt"].value;
    var cls_stop=document.forms["f_teacher_registration"]["cls_stop"].value;
    
    var colg_strt=document.forms["f_teacher_registration"]["colg_strt"].value;
    var colg_stop=document.forms["f_teacher_registration"]["colg_stop"].value;
    
    var registration_age1=document.forms["f_teacher_registration"]["registration_age1"].value;
    var registration_age2=document.forms["f_teacher_registration"]["registration_age2"].value;
    
    if (cls_strt=='' && cls_stop=='' && colg_strt=='' && colg_stop=='' && registration_age1=='' && registration_age2=='') {
     document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("teacher_reg_classorage").style.border="1px solid green"; 
    }
    //school--------
    if (cls_strt!='' && cls_stop=='') {
      document.getElementById("cls_stop").style.border="1px solid red";
      document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("cls_stop").style.border="1px solid green";
     document.getElementById("teacher_reg_classorage").style.border="1px solid green";
    }
    
    if (cls_strt=='' && cls_stop!='') {
      document.getElementById("cls_strt").style.border="1px solid red";
      document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("cls_strt").style.border="1px solid green";
     document.getElementById("teacher_reg_classorage").style.border="1px solid green";
    }
    //--------
    //college----
    if (colg_strt!='' && colg_stop=='') {
      document.getElementById("colg_stop").style.border="1px solid red";
      document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("colg_stop").style.border="1px solid green";
     document.getElementById("teacher_reg_classorage").style.border="1px solid green";
    }
    
    if (colg_strt=='' && colg_stop!='') {
      document.getElementById("colg_strt").style.border="1px solid red";
      document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("colg_strt").style.border="1px solid green";
     document.getElementById("teacher_reg_classorage").style.border="1px solid green";
    }
    //-------
    //Age----
    if (registration_age1!='' && registration_age2=='') {
      document.getElementById("registration_age2").style.border="1px solid red";
      document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("registration_age2").style.border="1px solid green";
     document.getElementById("teacher_reg_classorage").style.border="1px solid green";
    }
    
    if (registration_age1=='' && registration_age2!='') {
      document.getElementById("registration_age1").style.border="1px solid red";
      document.getElementById("teacher_reg_classorage").style.border="1px solid red";
      return false;
    }
    else{
     document.getElementById("registration_age1").style.border="1px solid green";
     document.getElementById("teacher_reg_classorage").style.border="1px solid green";
    }
    //----------------------------------------------------------------
    
    //if (flag==1) {
    // return true
    //}
   }
   //--------------------------------------------------------------------------
   
</script>
<script>
   $(document).ready(function(){
     $("html").click(function(){
      validateForm();
      });
     
     
     //for prevent charceter
     $('#add_pin').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
    });
     
     $('#teacher_fees1,#teacher_fees2,#teacher_fees3').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
    });
     
     $('#cnct_no').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
    });
     
     $('#alt_no').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
    });
     
     $('#birth_date').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
    });
     
     $('#birth_month').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
     });
     
     $('#birth_year').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
     });
     
     $('#registration_age1').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
     });
     
     $('#registration_age2').on('keypress', function(e){
      return e.metaKey || // cmd/ctrl
        e.which <= 0 || // arrow keys
        e.which == 8 || // delete key
        /[0-9]/.test(String.fromCharCode(e.which)); // numbers
     });
     
   });
   $(".multy_school_search").click(function(){
      $(".update_school_prominant_holder").toggle();
   });
   
//$("#teacher_reg_abt").keyup(function()
$(document).ready(function (){					
   $('textarea.charcounter-control').charcounter({
           placement: 'top-d'
   });
});
$('charcounter-control').charcounter({
  normalClass: 'charcounter-success ',
  warningClass: 'charcounter-warning ',
  limitReachedClass: 'charcounter-danger ', 
  createDefaultClasses: true,
  placement: 'bottom',
  fontfamily: 'Tahoma',
  fontsize: '10px',
  fontbold: true,
  fontcolor: '#FFFFFF',
  separator: ' / ',
  ccDebug: false,
  changeBorderColor: false,
  warningBorderColorClass: 'charcounter-warning-border',
  errorBorderColorClass: 'charcounter-error-border',
  showLimitOrWarning: 'limit'
});

</script>
<script type="text/javascript" charset="utf-8">
var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
var $ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
var $pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>';
var $pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler';
var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
</script>
<script type="text/javascript">
   $(document).ready(function(){
      //Update teacher name------------------------
      $("#update_teacher_name").click(function(){
         var first_name=$("#fst_nm").val();
         var last_name=$("#lst_nm").val();
         var nick_name=$("#nck_nm").val();
         
         if (nick_name!='') {
           var full_name=first_name+' '+last_name+' ('+nick_name+')';
         }
         else{
           var full_name=first_name+' '+last_name; 
         }
         $("#teacher_reg_name").val(full_name);
         $("#teacher_reg_name").trigger("click");
         return validateForm();
      });
      //-------------------------------------------
      //Update teacher gender----------------------
      $(".gender").click(function(){
       var gen=$(this).val();
       var gender='';
       if (gen=='male') {
         gender='Male';
       }
       else if (gen=='female') {
         gender='Female';
       }
       else{
        gender='Others'; 
       }
       $("#teacher_reg_gen").val(gender);
       //$("#teacher_reg_gen").trigger("click");
       validateForm();
      });
      //-------------------------------------------
      //Update Address-----------------------------
      $("#update_address").click(function(){
       var street=$("#add_st_hno").val();
       var locality=$("#add_loc").val();
       var land=$("#add_land").val();
       var pin=$("#add_pin").val();
       var state=$("#state").val();
       if (land!='') {
         var address=street+','+locality+','+land+','+state+','+pin;
       }
       else{
        var address=street+','+locality+','+state+','+pin; 
       }
       var addres = address.replace(",,", ",");
       addres = address.replace(/,\s*$/, "");
       $("#teacher_reg_add").val(addres);
       $("#teacher_reg_add").trigger("click");
       return validateForm();
      });
      //-------------------------------------------
      //Teacher Fees Structure---------------------
      $(".teacher_fees").keypress(function(e){
         if(e.which == 13) {
          var idd=$(this).attr("id");
          var fees=$(this).val();
          $("#teacher_reg_fees").val(fees);
          $("#teacher_reg_fees").trigger("click");
         }
      });
      //-------------------------------------------
      //Update Contact number----------------------
      $("#update_contact_no").click(function(){
      var contact_no=$("#cnct_no").val();
      var alt_contact_no=$("#alt_no").val();
      if (alt_contact_no!='') {
         var contact=contact_no+','+alt_contact_no;
      }
      else{
        var contact=contact_no;  
      }
     
      $("#teacher_reg_con").val(contact);
      $("#teacher_reg_con").trigger("click");
      return validateForm();
      });
      //-------------------------------------------
      //Update Birth Date--------------------------
      $(".birth_day_in").change(function(){
        var dd=+$("#birth_date").val();
        if (dd < 10) {
         dd='0'+dd;
        }
        var mm=+$("#birth_month").val();
        if (mm < 10) {
         mm='0'+mm;
        }
        var yy=$("#birth_year").val();
        var birth_day=dd+'-'+mm+'-'+yy;
        $("#teacher_reg_dob").val(birth_day);
        //$("#teacher_reg_dob").trigger("click");
        validateForm();
      });
      //-------------------------------------------
      //Update Language----------------------------
      lang=[];
      $(".lang").click(function(){
         var val='';
         var idd=$( this ).attr( "idd");
          if ( idd==0) {
          val=$(this).attr("val");
          $( this ).attr( "idd",1);
          }
          else{
         $( this ).attr( "idd",0);
          }
          $( ".lang" ).each(function( index ) {
            idd=$( this ).attr( "idd");
            val=$(this).attr("val");
            if ( idd==1) {
            lang.push(val);
            }
            else{
            val=$(this).attr("val");
            index = lang.indexOf(val);
            if (index > -1) {
               lang.splice(index, 1);
             } 
            }
          });
          $.unique(lang.sort());
          $("#teacher_reg_lang").val(lang);
          validateForm();
      });
      //-------------------------------------------
      //Update Experience--------------------------
      $("#exp_date").change(function(){
        var exp=$(this).val();
       // alert(exp);
        //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_teacher_exp',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        exp : exp,
        };
     if (exp!='') {
       $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            //alert(response);
            var expp=response.split(",");
            $("#teacher_reg_exp").val(expp[0]);
            $("#exp_id").val(expp[1]);
           // $("#teacher_reg_exp").trigger("click");
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
     }
     
        
      //--------------------------------------------------------
      });
      //--------------------------------------------------------
      //Update Localities---------------------------------------
       $("#search_st_loc").keyup(function(){
         var search_str=$(this).val();
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_ajax_locality',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        search_str : search_str,
        lcc:'student_location',
        lcc2:'st_loc',
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            //alert(response);
            $("#st_loc_list").html(response);
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------
       });
       //For student loc----------------------------
       st_loc=[];
       te_loc=[];
       $(document).on("click",".st_loc",function(e){
         //e.preventDefault();
        var val=$(this).val();
        var id=$(this).attr("id");
        if ($("#"+id).prop("checked") == true) {
        st_loc.push(val);
        }
        else{
          st_loc.splice($.inArray(val, st_loc),1);
        }
         size = st_loc.filter(function(value) { return value !== undefined }).length;
        if (size > 3) {
         $("#"+id).prop("checked",false);
         st_loc.splice($.inArray(val, st_loc),1);
         alert("Only 3 locations are allowed");
        }
        else{
         //$("#teacher_reg_locality").val('Student Location: '+st_loc+' Teacher Location: '+te_loc);
        }
       });
       //------------------------------------
       //For Teacher loc---------------------
       $("#search_te_loc").keyup(function(){
         var search_str=$(this).val();
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_ajax_locality',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        search_str : search_str,
        lcc:'teacher_location',
        lcc2:'te_loc',
        };
   
       $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            //alert(response);
            $("#te_loc_list").html(response);
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
       });
       
       $(document).on("click",".te_loc",function(e){
        // e.preventDefault();
        var val=$(this).val();
        var id=$(this).attr("id");
        if ($("#"+id).prop("checked") == true) {
        te_loc.push(val);
        }
        else{
          te_loc.splice($.inArray(val, te_loc),1);
        }
         size2 = te_loc.filter(function(value) { return value !== undefined }).length;
        if (size2 > 3) {
         $("#"+id).prop("checked",false);
         te_loc.splice($.inArray(val, te_loc),1);
         alert("Only 3 locations are allowed");
        }
        else{
         //$("#teacher_reg_locality").val('Student Location: '+st_loc+' Teacher Location: '+te_loc);
        }
       });
       //------------------------------------
       //$("#addButton2").click(function(){
       // $("#add_loc_modal").modal("show"); 
       //});
       //$("#st_add_loc_btn").click(function(){
       //  var locc=$("#st_add_loc").val();
       //  if (locc!='') {
       //     var str_loc='<li><input class="st_loc" type="checkbox" value="'+locc+'" name="student_location_choose" id="student_location<?php echo $locr=rand(); ?>"><label for="student_location<?php echo $locr; ?>">'+locc+'</label></li>';
       //  }
       //   $("#add_loc_modal").modal("hide");
       //   $("#st_loc_list").prepend(str_loc);
       //   //$("example-popover-2").modal("show");
       //});
       
       $("#add_location").click(function(){
         //alert();
        $("#add_loc_modal2").modal("show"); 
       });
       $("#te_add_loc_btn").click(function(){
         var locc=$("#te_add_loc").val();
         //$('#teacher_location').prepend( '<option value="'+locc+'">'+locc+'</option>' );
         //$(".options").prepend( '<li class="opt"><span><i></i></span><label>'+locc+'</label></li>' );
         $('select#teacher_location')[0].sumo.add(locc,locc,0);
         $("#add_loc_modal2").modal("hide");
         
       });
       
       
       $("#update_loc").click(function(){
        var tl='';
        var wl='';
        var ol='';
        te_loc=$("#teacher_location").val();
        if (te_loc==null) {
         tl='';
        }
        else{
         tl='Teacher Location :'+te_loc;
        }
        
         if($("#willingTravel").prop("checked") == true){
              if (te_loc==null) {
               wl='Willing to Travel';    
               }
               else{
                wl=' ,Willing to Travel';   
               }
            }
            else if($("#willingTravel").prop("checked") == false){
                wl='';
            }
            
            if($("#willingOnline").prop("checked") == true){
               if (te_loc==null && wl==='') {
               ol='Online';    
               }
               else{
                ol=' ,Online';   
               }
              
            }
            else if($("#willingOnline").prop("checked") == false){
                ol='';
            }
        var location=tl+wl+ol;
        $("#teacher_reg_locality").val(location);
        $("#teacher_reg_locality").trigger("click");
        validateForm();
       });
      //--------------------------------------------------------
      //Update subject------------------------------------------
       
      // $(document).on("click",".fs-checkbox, .fs-option-label",function(){
      //   var subj=$(this).text();
      //   subj_arr.push(subj);
      //    size_sub = subj_arr.filter(function(value) { return value !== undefined }).length;
      //   //alert(size_sub);
      //   if(size_sub > 4){
      //     alert('Only 4 subjects are allowed.');
      //     //$(".fs-option-label").trigger("click");
      //   }
      //   return validateForm();
      //});
      //--------------------------------------------------------
      //Update Education----------------------------------------
      gdidd=1;
      $("#add_grd_btn").click(function(e){
         e.preventDefault();
        gdidd=gdidd+parseInt($("#gd_count").val());
        var htm=dynamic_grad(gdidd);
        $("#graduation_det").append(htm);
         $("#gd_count").val(gdidd);
        $(".selectpicker").selectpicker('refresh');
      });
       pgdidd=1;
      $("#add_postgrd_btn").click(function(e){
         e.preventDefault();
         pgdidd=pgdidd+parseInt($("#pgd_count").val());
        var htm=postdynamic_grad(pgdidd);
        $("#post_graduation_det").append(htm);
        $("#pgd_count").val(pgdidd);
        
        $(".selectpicker").selectpicker('refresh');
      });
      
      $("#update_graduation").click(function(e){
       e.preventDefault(e);
       var gd_count=$("#gd_count").val();
       var str='';
       var college='';
       for(count=1;count <=gd_count; count++){
        var degree=$(".gd_deg option:selected").attr("val");
        //var discip=$("#gd_discip"+count).val();
        college=$("#gd_college"+count+" option:selected").text();
        var year_of_comp=$("#gd_year_of_comp"+count).val();
        if (count==1) {
         str=str+degree+','+college+','+year_of_comp;
        }
        else{
         str=str+degree+','+college+','+year_of_comp+'|';
        }
       }
       $("#edu_gra").val(str);
       $("#edu_gra").trigger("click");
       return validateForm();
      });
      
      $("#pgd_update_btn").click(function(){
        var pgd_count=$("#pgd_count").val();
        var str='';
        var gdcollege='';
        for(count=1;count <=pgd_count; count++){
        var degree=$(".pgd_deg option:selected").attr("val");
       // var discip=$("#pgd_discip"+count).val();
        gdcollege=$("#pgd_college"+count+" option:selected").text();
        var year_of_comp=$("#pgd_year_of_comp"+count).val();
         if (count==1) {
          str=str+degree+','+gdcollege+','+year_of_comp;  
         }
         else{
          str=str+degree+','+gdcollege+','+year_of_comp+'|';  
         }
        
       }
      
       $("#edu_pst_gra").val(str);
       $("#edu_pst_gra").trigger("click");
       return validateForm();
      });
      
      //other qualification
      otgqq=[];
     // $(".other_qualification").click(function(){
      $(document).on("click",".other_qualification",function(){
        var oth_q1='';
         $(".other_qualification").each(function(){
         if ($(this).prop("checked") == true) {
          oth_q1=$(this).attr("text");
          otgqq.push(oth_q1);
         }
         else{
             oth_q1=$(this).attr("text");
            index = otgqq.indexOf(oth_q1);
            if (index > -1) {
               otgqq.splice(index, 1);
             }
         }
         });
         $.unique(otgqq.sort());
        // alert(otgqq);
      });
      
      $("#update_other_q_btn").click(function(e){
       e.preventDefault();
         //var oth_q='';
         //$(".other_qualification").each(function(){
         //if ($(this).prop("checked") == true) {
         // oth_q=$(this).attr("text")+','+oth_q;
         //}  
         //});
         //alert(oth_q);
         $("#other_quali").val(otgqq);
         $("#other_quali").trigger("click");
         return validateForm();
      });
      //--------------------------------------------------------
      $("#finish").click(function(){
        $("#payment_plan").modal("hide");
        var valid=validateForm();
        if (valid==false) {
          $("html, body").animate({
            scrollTop: 350
        }, 600);
        }
      });
      
      //Remove graduation-----------------------------
      $(document).on("click",".close_popup",function(){
         $(this).parents(".grd_remove").remove();
         $("#teacher_reg_edu")[0].trigger("click");
      });
      //----------------------------------------------
   });
   
   function garadu_cer(idd){
   //alert(idd);
   $("#selectedFile"+idd).trigger("click");
   }
   function post_garadu_cer(idd){
   //alert(idd);
   $("#post_selectedFile"+idd).trigger("click");
   }
   
  //function checkHELLO(field, rules, i, options){
  //    var panVal = field.val();
  //    var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
  //    if(regpan.test(panVal)){
  //       // valid pan card number
  //    }else{
  //       return 'PAN Invalid!';
  //       } // invalid pan card number
  //  }
</script>
<script>
   $('[data-role="popover"]').popover({trigger: 'click'});
   $('[data-role="popover2"]').popover({trigger: 'hover'});
</script>
<!--tooltip attachments-->
<script>
   $(document).ready(function(){
   $('[data-toggle="tooltip"]').tooltip();
   
   $(document).on('show.bs.tooltip', function (e) {
     setTimeout(function() {   //calls click event after a certain time
     $('[data-toggle="tooltip"]').tooltip('hide');
    }, 5000);
    });
   });
</script>

<!-- scroll attachments --> 
<!-- Bootstrap core JavaScript
   ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="js/jquery.min.js"></script>-->
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->

<script type="text/javascript">
   var config = {
     '.chosen-select'           : {},
     '.chosen-select-deselect'  : {allow_single_deselect:true},
     '.chosen-select-no-single' : {disable_search_threshold:10},
     '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
     '.chosen-select-width'     : {width:"95%"}
   }
   for (var selector in config) {
     $(selector).chosen(config[selector]);
   }
</script>



   
<!--star rating attachments-->  
<script>  
   var __slice = [].slice;
   
   (function($, window) {
   var Starrr;
   
   Starrr = (function() {
     Starrr.prototype.defaults = {
       rating: void 0,
       numStars: 5,
       change: function(e, value) {}
     };
   
     function Starrr($el, options) {
       var i, _, _ref,
         _this = this;
   
       this.options = $.extend({}, this.defaults, options);
       this.$el = $el;
       _ref = this.defaults;
       for (i in _ref) {
         _ = _ref[i];
         if (this.$el.data(i) != null) {
           this.options[i] = this.$el.data(i);
         }
       }
     this.createStars();
       this.syncRating();
       this.$el.on('mouseover.starrr', 'span', function(e) {
         return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
       });
       this.$el.on('mouseout.starrr', function() {
         return _this.syncRating();
       });
       this.$el.on('click.starrr', 'span', function(e) {
         return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
       });
       this.$el.on('starrr:change', this.options.change);
     }
   
     Starrr.prototype.createStars = function() {
       var _i, _ref, _results;
   
       _results = [];
       for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
         _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
       }
       return _results;
     };
   Starrr.prototype.setRating = function(rating) {
       if (this.options.rating === rating) {
         rating = void 0;
       }
       this.options.rating = rating;
       this.syncRating();
       return this.$el.trigger('starrr:change', rating);
     };
   
     Starrr.prototype.syncRating = function(rating) {
       var i, _i, _j, _ref;
   
       rating || (rating = this.options.rating);
       if (rating) {
         for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
           this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
         }
       }
       if (rating && rating < 5) {
         for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
           this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
         }
       }
       if (!rating) {
         return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
       }
    };
   
     return Starrr;
   
   })();
   return $.fn.extend({
     starrr: function() {
       var args, option;
   
       option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
       return this.each(function() {
         var data;
   
         data = $(this).data('star-rating');
         if (!data) {
           $(this).data('star-rating', (data = new Starrr($(this), option)));
         }
         if (typeof option === 'string') {
           return data[option].apply(data, args);
         }
       });
        }
   });
   })(window.jQuery, window);
   
   $(function() {
   return $(".starrr").starrr();
   });
   
   $( document ).ready(function() {
       
   $('#stars').on('starrr:change', function(e, value){
     $('#count').html(value);
   });
   
   $('#stars-existing').on('starrr:change', function(e, value){
     $('#count-existing').html(value);
   });
   });
</script>

<!--instant edit & save-->
<script>
   //left side institute name
   $('#edit_ins_nm').click(function() {
    var text = $('.text_ins_nm').text();
    var input = $('<input id="attribute_ins_nm" style="padding-bottom:0;" type="text" value="' + text + '" />')
    $('.text_ins_nm').text('').append(input);
    input.select();
   
    input.blur(function() {
      var text = $('#attribute_ins_nm').val();
      $("#profile_heading").val(text);
      $('#attribute_ins_nm').parent().text(text);
      $('#attribute_ins_nm').remove();
    });
   });
   
   //for email id heading
   $('#edit_em_id_hdng').click(function() {
    var text = $('.text_email_nm').text();
    var input = $('<input id="attribute_em_nm" type="text" value="' + text + '" />')
    $('.text_email_nm').text('').append(input);
    input.select();
   
    input.blur(function() {
      var text = $('#attribute_em_nm').val();
      $("#email_id").val(text);
      $('#attribute_em_nm').parent().text(text);
      $('#attribute_em_nm').remove();
    });
   });
</script>
<!--accordiaon for fees-->
<script>
   $(document).ready(function() {
   	$('#teacher_fees_val').click(function() {
   		$(".teacher_fees_valc").slideToggle('600');
                $(".teacher_fees2_valc").hide('600');
                $(".teacher_fees3_valc").hide('600');
   	});
        $('#teacher_fees2_val').click(function() {
   		$(".teacher_fees2_valc").slideToggle('600');
                $(".teacher_fees_valc").hide('600');
                $(".teacher_fees3_valc").hide('600');
   	});
        
        $('#teacher_fees3_val').click(function() {
   		$(".teacher_fees3_valc").slideToggle('600');
                $(".teacher_fees_valc").hide('600');
                $(".teacher_fees2_valc").hide('600');
   	});
   	//$('.accordion-toggle').on('click', function() {
   	//	$(this).toggleClass('active').siblings().removeClass('active');
   	//});
   });
</script>
<!-- subject auto search-->


<!--Comming Form Head Section-->

<!-- search attachments -->  
      <script>
         $(document).ready(function(e){
             $('.search-panel .dropdown-menu').find('a').click(function(e) {
                 e.preventDefault();
                 var param = $(this).attr("href").replace("#","");
                 var concept = $(this).text();
                 $('.search-panel span#search_concept').text(concept);
                 $('.input-group #search_param').val(param);
             });
         })
      </script>
      
      
      <script type="text/javascript">
         $(document).ready(function()
         {
             var $scrollbar = $("#scrollbar1");
         
             $scrollbar.tinyscrollbar();
         });
      </script>
      
      <!--achivement taxtarea attachmnets-->
      <script type="text/javascript">
         $(document).ready(function(){
             var counter = 2;
             $("#addButton0").click(function () {
         	if(counter>5){
                     alert("Only 5 achivements allowed");
                     return false;
         	}
         	var newTextBoxDiv0 = $(document.createElement('div'))
         	     .attr("id", 'TextBoxDiv0' + counter);
         	newTextBoxDiv0.after().html(
         	      '<p class="col-xs-12 margin_bottom_10"><textarea class="form-control" rows="1" placeholder="Achivements #'+ counter + ' " name="achivments[]" id="textbox' + counter + '" value=""  style="resize:none;"></textarea></p>');
         	newTextBoxDiv0.appendTo("#TextBoxesGroup0");
         	$("#textbox"+counter).focus();
                
               //var scrollTo_val = $('.slimScrollDiv').prop('scrollHeight') + 'px';
               //$('.slimScrollDiv').slimScroll({ scrollTo : scrollTo_val });
              //newEnd = $("#textbox"+counter).height()
            //$("#TextBoxesGroup0").slimScroll({scrollTo: newEnd});
              
                counter++;
                
              });
              $("#removeButton0").click(function () {
         	if(counter==2){
                   alert("No more textbox to remove");
                   return false;
                }
         	counter--;
                 $("#TextBoxDiv0" + counter).remove();
              });
              $("#getButtonValue0").click(function () {
         	var msg0 = '';
         	for(i=1; i<counter; i++){
            	  msg0 += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
         	}
             	  alert(msg0);
              });
           });
      </script>
      <!--localities taxtbox attachmnets-->
      <!--student location-->
      <script type="text/javascript">
         $(document).ready(function(){
             var counter = 2;
             $("#addButton2").click(function () {
         	if(counter>3){
                     alert("Only 3 locations are allowed");
                     return false;
         	}
         	var newTextBoxDiv2 = $(document.createElement('div'))
         	     .attr("id", 'TextBoxDiv2' + counter);
         	newTextBoxDiv2.after().html(
         	      '<p><input type="text"  class="form-control input-sg" placeholder="student location '+ counter + ' " name="textbox' + counter +
         	      '" id="textbox' + counter + '" value=""></p>');
         	newTextBoxDiv2.appendTo("#TextBoxesGroup2");
         	counter++;
              });
              $("#removeButton2").click(function () {
         	if(counter==2){
                   alert("No more textbox to remove");
                   return false;
                }
         	counter--;
                 $("#TextBoxDiv2" + counter).remove();
              });
              $("#getButtonValue2").click(function () {
         	var msg2 = '';
         	for(i=1; i<counter; i++){
            	  msg2 += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
         	}
             	  alert(msg2);
              });
           });
      </script>
      <!--teacher location-->
      <script type="text/javascript">
         //$(document).ready(function(){
         //    var counter = 2;
         //    $("#addButton3").click(function () {
         //	if(counter>3){
         //            alert("Only 3 locations are allowed");
         //            return false;
         //	}
         //	var newTextBoxDiv3 = $(document.createElement('div'))
         //	     .attr("id", 'TextBoxDiv3' + counter);
         //	newTextBoxDiv3.after().html(
         //	      '<p><input type="text"  class="form-control input-sg" placeholder="teacher location '+ counter + ' " name="textbox' + counter +
         //	      '" id="textbox' + counter + '" value=""></p>');
         //	newTextBoxDiv3.appendTo("#TextBoxesGroup3");
         //	counter++;
         //     });
         //     $("#removeButton3").click(function () {
         //	if(counter==2){
         //          alert("No more textbox to remove");
         //          return false;
         //       }
         //	counter--;
         //        $("#TextBoxDiv3" + counter).remove();
         //     });
         //     $("#getButtonValue3").click(function () {
         //	var msg3 = '';
         //	for(i=1; i<counter; i++){
         //   	  msg3 += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
         //	}
         //    	  alert(msg3);
         //     });
         //  });
      </script>
      <!--classes school-->
      <script type="text/javascript">
         $(document).ready(function(){
             var counter = 2;
             $("#addButton5").click(function () {
			 
         	if(counter>3){
                     alert("Only 3 schools are allowed");
                     return false;
         	}
         	var newTextBoxDiv5 = $(document.createElement('div'))
         	     .attr("id", 'TextBoxDiv5' + counter);
         	newTextBoxDiv5.after().html(
         	      '<div class="class_age_right_10"><p class="col-xs-5 no_padd classes_age"><select class="clss"><option value="1">1</option><option value="2">2</option></select><p class="col-xs-2 margin_top_10"><label style="color:#33acdb;">TO</label></p><p class="col-xs-5 no_padd classes_age"><select class="clss"><option value="1">1</option><option value="2">2</option></select></p></div>');
         	newTextBoxDiv5.appendTo("#TextBoxesGroup5");
         	counter++;
              });
              $("#removeButton5").click(function () {
         	if(counter==2){
                   alert("No more values to remove");
                   return false;
                }
         	counter--;
                 $("#TextBoxDiv5" + counter).remove();
              });
              $("#getButtonValue5").click(function () {
         	var msg5 = '';
         	for(i=1; i<counter; i++){
            	  msg5 += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
         	}
             	  alert(msg5);
              });
           });
      </script>
      <!--classes college-->
      <script type="text/javascript">
         $(document).ready(function(){
             var counter = 2;
             $("#addButton6").click(function () {
         	if(counter>3){
                     alert("Only 3 colleges are allowed");
                     return false;
         	}
         	var newTextBoxDiv6 = $(document.createElement('div'))
         	     .attr("id", 'TextBoxDiv6' + counter);
         	newTextBoxDiv6.after().html(
         	      '<div class="class_age_right_10"><p class="col-xs-5 no_padd classes_age"><select class="clss"><option value="1st Sem">1st Sem</option><option value="2nd Sem">2nd Sem</option></select><p class="col-xs-2 margin_top_10"><label style="color:#33acdb;">TO</label></p><p class="col-xs-5 no_padd classes_age"><select class="clss"><option value="1st Sem">1st Sem</option><option value="2nd Sem">2nd Sem</option></select></p></div>');
         	newTextBoxDiv6.appendTo("#TextBoxesGroup6");
         	counter++;
              });
              $("#removeButton6").click(function () {
         	if(counter==2){
                   alert("No more values to remove");
                   return false;
                }
         	counter--;
                 $("#TextBoxDiv6" + counter).remove();
              });
              $("#getButtonValue6").click(function () {
         	var msg6 = '';
         	for(i=1; i<counter; i++){
            	  msg6 += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
         	}
             	  alert(msg6);
              });
           });
      </script>
      
      
      <!--auto complete state in address for Correspondence-->
      <script>
         var dataList = document.getElementById('json-datalist');
         var input = document.getElementById('ajax');
         
         var request = new XMLHttpRequest();
         request.onreadystatechange = function(response) {
           if (request.readyState === 4) {
             if (request.status === 200) {
               var jsonOptions = JSON.parse(request.responseText);
           
               jsonOptions.forEach(function(item) {
                 var option = document.createElement('option');
                 option.value = item;
                 dataList.appendChild(option);
               });
               input.placeholder = "e.g. datalist";
             } else {
               input.placeholder = "Couldn't load datalist options";
             }
           }
         };
         //input.placeholder = "Loading options...";
         
      </script>

<!--Comming Form Head Section-->

<script>
   $(function() {
           $('.demo').fSelect();
       });
</script>
<script>
   $(function(){
   
   					$('.fs-options').slimScroll({
   						height: '260px',
   						size: '7px',
   						width:'100%',
   						alwaysVisible: true,
   						distance: '0',
   						color: '#33acdb',
   						allowPageScroll: true,
   						railVisible: true,
       					railColor: '#337087',
       					railOpacity: 1,
   						wheelStep: 50,
   						distance: '5px',
   						opacity: 1
   					});
   
   				});
   				
   				$(function(){
   
   					$('.category_item').slimScroll({
   						height: '110px',
   						size: '7px',
   						width:'100%',
   						alwaysVisible: true,
   						distance: '0',
   						color: '#33acdb',
   						allowPageScroll: true,
   						railVisible: true,
       					railColor: '#337087',
       					railOpacity: 1,
   						wheelStep: 50,
   						opacity: 1
   					});
   
   				});
   				$(function(){
   
   					$('#TextBoxesGroup0').slimScroll({
   						height: '70px',
   						size: '7px',
   						width:'100%',
   						alwaysVisible: true,
   						distance: '5px',
   						color: '#33acdb',
   						allowPageScroll: true,
                                                start:'top',
   //						railVisible: true,
   //    					railColor: '#337087',
   //    					railOpacity: 1,
   						wheelStep: 50,
   						opacity: 1
   					});
   
   				});
				
				
				
				if(screen.width < 767){
						$("#targetLayer").slimScroll({destroy: true});
					}
					else
					{
						$(function(){
							$('#targetLayer').slimScroll({
								height: '130px',
								size: '7px',
								width:'100%',
								alwaysVisible: true,
								distance: '5px',
								color: '#33acdb',
								allowPageScroll: true,
								wheelStep: 50,
								opacity: 1
							});
		   
						});
						
					}
					
					
					if(screen.width < 767){
						$("#target_video").slimScroll({destroy: true});
					}
					else
					{
						$(function(){
							$('#target_video').slimScroll({
								height: '130px',
								size: '7px',
								width:'100%',
								alwaysVisible: true,
								distance: '5px',
								color: '#33acdb',
								allowPageScroll: true,
								wheelStep: 50,
								opacity: 1
							});
		   
						});
						
					}

				
				
</script>
<script>
   function myFunction() {
       var x = document.getElementById('myDIV');
       var x1 = document.getElementById('myDIV1');
       var x2 = document.getElementById('myDIV2');
       if (x.style.display === 'none') {
            x.style.display = 'block';
            x1.style.display = 'none';
            x2.style.display = 'none';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction1() {
       var x = document.getElementById('myDIV1');
       var x1 = document.getElementById('myDIV');
       var x3 = document.getElementById('myDIV2');
       if (x.style.display === 'none') {
           x.style.display = 'block';
           x1.style.display = 'none';
           x3.style.display = 'none';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction2() {
       var x = document.getElementById('myDIV2');
        var x1 = document.getElementById('myDIV');
       var x2 = document.getElementById('myDIV1');
       if (x.style.display === 'none') {
           x.style.display = 'block';
            x1.style.display = 'none';
           x2.style.display = 'none';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction3() {
       var x = document.getElementById('myDIV3');
      
       if (x.style.display === 'none') {
           x.style.display = 'block';
          
       } else {
           x.style.display = 'none';
       }
   }
   
</script>

<script>
//Update Education--------------------------------------
$("#update_edu").click(function(){
 var edu_gra=$("#edu_gra").val();
 var edu_pst_gra=$("#edu_pst_gra").val();
 var other_quali=$("#other_quali").val();
 var education=edu_gra+','+edu_pst_gra+','+other_quali;
 education = education.replace(/,\s*$/, "");
 $("#teacher_reg_edu").val(education);
 $("#teacher_reg_edu").trigger("click");
 return validateForm();
});
//------------------------------------------------------
$(".category_item :checkbox").on('click', function(){
   if($(this).is(":checked")) {
        $(this).closest('li').addClass("active");
    } else {
        $(this).closest('li').removeClass("active");
    }
});

$(".teacher_fees").keyup(function(){
   var fees=$(this).val();
  $("#teacher_reg_fees").val(fees);  
});

$("#same_loc").click(function(){
      if($(this).prop("checked") == true){
      var loc=$("#add_loc").val();
      $("#teacher_loc_hidden").val(loc);
      }
      else if($(this).prop("checked") == false){
          //alert("Checkbox is unchecked.");
          $("#teacher_loc_hidden").val('');
      }
});

//$("#id_photoid").click(function(){
//   $("#teacher_add_id")[0].trigger("click");
//});
  //var owl = $('.owl-photo');
  //    owl.owlCarousel({
  //      margin: 0,
  //      autoplay: false,
  //      autoplayTimeout: 2000,
  //      loop: false,
  //  nav: true,
  //  navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>images/myprevimage1.png'>","<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>images/mynextimage1.png'>"],
  //      responsive: {
  //        0: {
  //          items: 1
  //        },
  //    600: {
  //          items: 2
  //        },
  //    730: {
  //          items: 3
  //        },
  //    1024: {
  //          items: 5
  //        }
  //      }
  //    });



        var owl = $('.owl-video');
      owl.owlCarousel({
        margin: 0,
        autoplay: false,
        autoplayTimeout: 2000,
        loop: false,
    nav: true,
    navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>images/myprevimage1.png'>","<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>images/myprevimage1.png'>"],
        responsive: {
          0: {
            items: 1
          },
      600: {
            items: 2
          },
      730: {
            items: 3
          },
      1024: {
            items: 5
          }
        }
      })
    
    $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
})

//$(document).on("click",".upload_photo_file",function(){
// $("#up_img").trigger("click");  
//});

function showPreview(objFileInput) {
   //$("#targetLayer").html('');
   var numFiles = objFileInput.files.length;
   //alert(numFiles);
  for(count = 0; count < numFiles; count++){
    if (objFileInput.files[count]) {
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            $("#targetLayer").prepend('<div class="item" style="float: left;"><span class="img_del">X</span><div class="thumb_box"><img src="'+e.target.result+'" width="100px" height="100px" alt=""></div></div>');
        }
		fileReader.readAsDataURL(objFileInput.files[count]);
    }
   }
   $(".upload_photo_file").append('<input type="file" name="up_img[]" id="up_img" onChange="showPreview(this);">');
   //$("#img_add_text").text('Change Photo');
   //$(this).hide();
}

//Teacher add ID Card-----------------------------------------
function addid_photo(objFileInput) {
   var file = objFileInput.files[0];
   var fileType = file["type"];
   var fileSize = file["size"];
   fileSize = fileSize / 1048576; //size in mb 
   var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
   if (fileSize > 2) {
     alert("File size must not exceed 2MB");
   }
   else if ($.inArray(fileType, ValidImageTypes) == 0) {
     alert("Invalid file type.Please choose gif/jpeg/png"); 
    
   }
   else{
    if (objFileInput.files[0]) {
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            $("#id_photoid").html('<img src="'+e.target.result+'" width="100px" height="100px" alt="">');
        }
		fileReader.readAsDataURL(objFileInput.files[0]);
                $("#uploadCardForm").submit();
    }  
   }
}
$("#uploadCardForm").on('submit',(function(e) {
      e.preventDefault();
      $.ajax({
      url: "<?php echo get_site_url();?>/wp-content/plugins/pedagoge_plugin/upload_card.php",
              type: "POST",
              data:  new FormData(this),
              contentType: false,
               processData:false,
              success: function(data)
          {
              //$("#targetLayer").html(data);
              //$("#targetLayer").css('opacity','1');
              //setInterval(function() {$("#body-overlay").hide(); },500);
             // alert(data);
              },
              error: function() 
      {
      } 	        
 });
}));
//-----------------------------------------------------
//Teacher add CV---------------------------------------
function add_cv(objFileInput) {
   var file = objFileInput.files[0];
   var fileType = file["type"];
   var fileSize = file["size"];
   fileSize = fileSize / 1048576; //size in mb 
   var fl1="application/vnd.openxmlformats-officedocument.wordprocessingml.document";
   var fl2="application/pdf";
   //var ValidImageTypes = ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/pdf"];
   if (fileSize > 2) {
     alert("File size must not exceed 2MB");
   }
   else if (fileType==fl1 || fileType==fl2) {
    if (objFileInput.files[0]) {
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
           // $("#id_photoid").html('<img src="'+e.target.result+'" width="100px" height="100px" alt="">');
        }
		fileReader.readAsDataURL(objFileInput.files[0]);
                $("#uploadCvForm").submit();
    }
   }
   else{
      alert("Invalid file type.Please choose docs/pdf");
   }
    
}
$("#uploadCvForm").on('submit',(function(e) {
      e.preventDefault();
      $.ajax({
      url: "<?php echo get_site_url();?>/wp-content/plugins/pedagoge_plugin/upload_cv.php",
              type: "POST",
              data:  new FormData(this),
              contentType: false,
               processData:false,
              success: function(data)
          {
              $("#cv_img").html(data);
              var cv=$(".cv_img").attr("img-name");
              $("#current_cv").val(cv);
              },
              error: function() 
      {
      } 	        
 });
}));
//-----------------------------------------------------
$(document).on("click",".img_del",function(){
 $(this).parent().find('div').remove();
 $(this).remove();
});

$(document).on("click","#add_video_btn",function(){
   var vid=$("#video_upload").val();
   var video=vid.split("v=");
   var vhtm='<div class="item" style="float: left;"><input type="hidden" name="video_upload[]" value="'+video[1]+'">'+
            '<span class="vid_del">X</span>'+
            '<div class="thumb_box">'+
            '<iframe width="180" height="120" src="https://www.youtube.com/embed/'+video[1]+'" frameborder="0" allowfullscreen></iframe>'+
            '</div>'+
            '</div>';
            
   $("#target_video").prepend(vhtm);
   $("#video_upload").val('');      
});

$(document).on("click",".vid_del",function(){
 $(this).parent().find('div').remove();
 $(this).parent().find('input').remove();
 $(this).remove();
});

//On change subject-------------------------------
$(document).on("click",".fs-checkbox, .fs-label, .fs-option-label",function(){
 var subj=$("#subjects").val();
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_cource_type',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        subjects:subj,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
           // alert(response);
            $("#teaching_category").val(response);
            return validateForm();
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------
     
 });
//--------------------------------------------------------------
//Get Ajax other qualification----------------------------------
$(document).on("keyup","#other_qualifications",function(){
 var str=$(this).val();
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_ajax_other_qualification',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        search_str:str,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            //alert(response);
            $("#other_quali_li").html(response);
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------
 });
//--------------------------------------------------------------
//Update Teaching of Class--------------------------------------
 $(document).on("click","#age_class",function(){
   var class_from=$("#cls_strt").val();
   var class_to =$("#cls_stop").val();
   var class_str='School : '+class_from+' to '+class_to;
   if (class_from=='' || class_to=='') {
      class_str='';
   }
   var college_from=$("#colg_strt").val();
   var college_to=$("#colg_stop").val();
   var college='College : '+college_from+' to '+college_to;
   if (college_from=='' || college_to=='') {
     college='';
   }
   var age_from=$("#registration_age1").val();
   var age_to=$("#registration_age2").val();
   var age='Age : '+age_from+' to '+age_to+' years';
   if (age_from=='' || age_to=='') {
      age='';
   }
   var cllg=[];
   if (class_str!='') {
    cllg.push(class_str);
   }
   if (college!='') {
   cllg.push(college);
   }
   if (age!='') {
   cllg.push(age);
   }
   var class_age=cllg;
   
   $("#teacher_reg_classorage").val(class_age);
   $("#teacher_reg_classorage").trigger("click");
   validateForm();
 });

  $(document).on('focus', '.date-own', function(){
     $(this).datepicker({ minViewMode: 2, format: 'yyyy',autoclose: true}); 
   });
  $(document).on('click', '#teacher_reg_exp', function(){
     $("#exp_date").datepicker({ minViewMode: 2, format: 'yyyy',autoclose: true});
     $("#exp_date").datepicker('show');
   });
//---------------------------------------------------------------
//get past school check------------------------------------------

$(document).on('click', '#update_school', function(){
var past_school_arr = [];
$('input.past_school').each(function () {
   if($(this).prop("checked") == true){
    past_school_arr.push($(this).val());
   }
   else{
    val=$(this).val();
      index = past_school_arr.indexOf(val);
      if (index > -1) {
         past_school_arr.splice(index, 1);
       }   
   }
});

$("#past_taught_school").val(past_school_arr);
$("#school_select_multypel").val(past_school_arr);
$("#school_select_multypel").attr('placeholder',past_school_arr);
$("#school_select_multypel").trigger("click");
});
$(document).on('click', '#cancel_school', function(){
  $('input.past_school').removeAttr('checked');
  $("#past_taught_school").val('');
  $("#school_select_multypel").attr('placeholder','Select School');
  $("#school_select_multypel").trigger("click"); 
});
//$(document).on('click', '#school_select_multypel', function(){
//$("#school_select_multypel").val('');
// $("#school_select_multypel").attr('placeholder','Search Here...');
//});
//$("html").click(function(){
// $("#school_select_multypel").val($("#past_taught_school").val(past_school_arr));  
//});
//Get Ajax Past School list----------------------------------
$(document).on("keyup","#school_select_multypel",function(){
 var str=$(this).val();
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_ajax_past_school_list',
        pedagoge_callback_class: 'ControllerPrivateteacher',
        search_str:str,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            //alert(response);
            $("#past_school_list").html(response);
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------
 });
//--------------------------------------------------------------
//---------------------------------------------------------------
</script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.sumoselect.js"></script>
<script type="text/javascript">
   $(document).ready(function () {
       window.asd = $('.SlectBox').SumoSelect({ csvDispCount: 3, selectAll:true, captionFormatAllSelected: "Yeah, OK, so everything." });
       window.test = $('.testsel').SumoSelect({okCancelInMulti:false, search: true, captionFormatAllSelected: "Yeah, OK, so everything." });
        window.test = $('.teacher_location').SumoSelect({okCancelInMulti:true, search: true, captionFormatAllSelected: "Yeah, OK, so everything." });
       window.testSelAll = $('.testSelAll').SumoSelect({okCancelInMulti:true, selectAll:true });
       window.testSelAll2 = $('.testSelAll2').SumoSelect({selectAll:true});
       window.testSelAlld = $('.SlectBox-grp').SumoSelect({okCancelInMulti:true, selectAll:true, isClickAwayOk:true });
       window.Search = $('.search-box').SumoSelect({ csvDispCount: 3, search: true, searchText:'Enter here.' });
       window.sb = $('.SlectBox-grp-src').SumoSelect({ csvDispCount: 3, search: true, searchText:'Enter here.', selectAll:true });
       window.searchSelAll = $('.search-box-sel-all').SumoSelect({ csvDispCount: 3, selectAll:true, search: true, searchText:'Enter here.', okCancelInMulti:true });
       window.searchSelAll = $('.search-box-open-up').SumoSelect({ csvDispCount: 3, selectAll:true, search: false, searchText:'Enter here.', up:true });
       window.groups_eg_g = $('.groups_eg_g').SumoSelect({selectAll:true, search:true });
       //$('.SlectBox').on('sumo:opened', function(o) {
       //  console.log("dropdown opened", o)
       //});
   
   });
   
   
   var owl = $('.owl-payment');
      owl.owlCarousel({
        margin: 0,
        autoplay: false,
        autoplayTimeout: 2000,
        loop: false,
    nav: true,
    navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>images/myprevimage1.png'>'>","<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>images/mynextimage1.png'>"],
        responsive: {
          0: {
            items: 1
          },
      480: {
            items: 2
          },
      640: {
            items: 3
          },
      1024: {
            items: 5
          }
        }
      })
    
   $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
});
</script>

<script type="text/javascript">
/* Mean Menu */
	jQuery(document).ready(function () {
		jQuery('.site-navigation').meanmenu();
	});
</script>


</body>
</html>

