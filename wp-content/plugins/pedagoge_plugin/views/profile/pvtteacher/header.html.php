<?php
error_reporting(0);
$teacher_id=$_SESSION['member_id'];
$teacher_model = new ModelTeacher();
$profile_slug =$teacher_model->get_teacher_slug($teacher_id);
$user_data_teacher=$teacher_model->get_user_data_teacher($teacher_id);
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Pedagoge</title>
         
      <!-- Bootstrap core CSS -->
      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/bootstrap.css" rel="stylesheet">
      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/ie10-viewport-bug-workaround.css" rel="stylesheet">
      
      <!-- my attachments -->
      <link type="text/css" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/font-awesome.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/tinyscrollbar.css" type="text/css" media="screen"/>
      <link type="text/css" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/main.css" rel="stylesheet" media="screen">
      <link type="text/css" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/my_css.css" rel="stylesheet" media="screen">
      <!--home popover attachmnets-->
      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/jquery-popover-0.0.3.css" rel="stylesheet" type="text/css">
      
      <!-- subject auto search-->
      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/fSelect.css" rel="stylesheet" type="text/css">
      <!--cropper attachments-->
      <link rel="stylesheet" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/cropper.min.css">
      <!--auto complete search for city-->
      <!--<link href="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/jquery-ui.css" rel="stylesheet">-->
      <!--<link href="<?php //echo plugins_url() ?>/pedagoge_plugin/assets/css/jquery.tagit.css" rel="stylesheet" type="text/css">
      <link href="<?php //echo plugins_url() ?>/pedagoge_plugin/assets/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">-->
      <!-- animation attachments-->


      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/owl.carousel.min.css" rel="stylesheet" type="text/css" media="screen">
      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css" media="screen">
      <!--<link rel="stylesheet" href="css/libs/animate.css">-->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.css">
	 
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/css/ankitesh/validationEngine.jquery.css" rel="stylesheet" type="text/css">
         <style type="text/css">
         /*.wow:first-child {ask for
         
           visibility: hidden;
         }*/
		 .popover-modal .popover-body { overflow:hidden; padding:1em;}
         .experience_model .popover-body{padding:0;}
         .tin a[data-role="popover"],.tin a[data-role="popover2"] { display:inline-block; padding:1em; margin:5px;/* <!--background-color:#2980B9; color:#fff;-->*/ text-decoration:none;}
		 
         </style>
      <link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/sumoselect.css" rel="stylesheet">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/css/alertify.core.css" rel="stylesheet" type="text/css">
      <link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/css/alertify.default.css" rel="stylesheet" type="text/css">
      <link type="text/css" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/menu.css" rel="stylesheet" media="screen">
      <link type="text/css" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/teacher_registration.css" rel="stylesheet" media="screen">
      <script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery-2.1.1.min.js"></script>
      <script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/bootstrap.min.js"></script>
      <script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/js/alertify.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.js"></script>
   </head>
   <body>
      <!--complete header-->
      <section>
         <!--header menu and search-->
         <?php /*?><aside class="container">
            <div class="row">
               <div class="col-xs-12"><br><br></div>
               <div class="col-xs-8 col-sm-3 col-xs-offset-2 col-sm-offset-0 col-md-4 col-lg-4"><a href="<?php echo get_site_url(); ?>"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/logo_black.png" alt="" style="width:100%;"></a></div>
               <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6 col-lg-offset-2 col-md-offset-1 col-xs-offset-0 col-sm-offset-0 no_padd">
                  <div class="col-xs-12 no_dis_tab"><br></div>
                  <p class="col-xs-6 col-sm-3 col-md-3 col-xs-offset-0 col-sm-offset-0 no_padd mobile_view_oneline registration_main_navigation"><a href="#" class="col-xs-12 hdr_links">Careers</a></p>
                  <p class="col-xs-6 col-sm-3 col-md-3 no_padd mobile_view_oneline registration_main_navigation"><a href="#" class="col-xs-12 hdr_links">Blog</a></p>
                  <p class="col-xs-6 col-sm-3 col-md-3 no_padd mobile_view_oneline registration_main_navigation"><a href="#" class="col-xs-12 hdr_links hdr_links_sign">Contact Us</a></p>
                  <p class="col-xs-6 col-sm-3 col-md-3 no_padd mobile_view_oneline registration_main_navigation"><a href="<?php echo wp_logout_url(); ?>" class="col-xs-12 hdr_links hdr_links_sign text-uppercase">Logout</a></p>
               </div>
               <div class="clearfix"></div>
            </div>
         </aside><?php */?>
         
         
         <header class="header_wrapper teacher_registration_header_wrapper">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 logo">
                	<a href="<?php echo get_site_url(); ?>"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/logo_black.png" alt="Logo"></a>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8 navigation_role">
                	<div class="main-menu-section">
                        <div class="container_12">
                            <nav class="site-navigation main-navigation" role="navigation">
                                <ul class="menu">
                                    <li><a href="career.html">Careers</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                   
                                    <li>
                                     <?php if(is_user_logged_in()) { ?>
                                    <a href="<?php echo wp_logout_url(); ?>">Logout <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <?php }else{ ?>
                                    <a href="<?php echo get_site_url(); ?>/login?nexturl=/">Login</a>
                                    <?php } ?>
                                    	<ul class="sub-menu">
					   <?php if($user_data_teacher[0]->approved=='yes'){ ?>
                                            <li class="menu-item"><a href="<?php echo get_site_url(); ?>/teacherdashboard/">Dashboard</a></li>
					    <?php } ?>
                                            <li class="menu-item"><a href="<?php echo get_site_url(); ?>/teacher/<?php echo $profile_slug; ?>/">Profile</a></li>
                                            <li class="menu-item"><a href="#">Change Password</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
         
         
         <!--header menu and search-->
      </section>
      <!--/complete header-->