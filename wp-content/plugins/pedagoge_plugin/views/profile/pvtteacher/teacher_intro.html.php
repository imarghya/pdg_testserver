<?php
	$teacher_id = '';
	$teacher_user_id = '';
	$teacher_first_name = '';
	$teacher_last_name = '';
	$demo_allowed = '';
	$about_fees_structure = '';
	$allow_installments = '';
	$profile_heading = '';
	$other_qualification = '';	
	$profile_heading = '';
	$other_communication_mode = '';
	$operation_hours_from = '';
	$operation_hours_to = '';
	$about_coaching = '';
	$avg_teaching_xp_id = '';
	$teacher_nickname = '';
	$teacher_present_place_of_work = '';
	//$teacher_date_of_birth = '';
	$teacher_birth_year = '';
	$teacher_birth_month = '';
	$teacher_birth_day = '';
	$teacher_city_id = '';
	$str_city_options = '';
	$correspondance_address = '';
	$registration_reference = '';
	

	if(isset($view_teachers_data)) {
		foreach($view_teachers_data as $view_teachers_info) {
			$teacher_first_name = $view_teachers_info->first_name;
			$teacher_last_name = $view_teachers_info->last_name;
			
			$teacher_id = $view_teachers_info->teacher_id;
			$teacher_user_id = $view_teachers_info->user_id;
			$other_qualification = $view_teachers_info->other_qualification;
			$profile_heading = $view_teachers_info->profile_heading;
			$operation_hours_from = $view_teachers_info->operation_hours_from;
			$operation_hours_to = $view_teachers_info->operation_hours_to;
			$about_coaching = $view_teachers_info->about_coaching;
			$avg_teaching_xp_id = $view_teachers_info->teaching_xp_id;
			$allow_installments = $view_teachers_info->installment_allowed;
			$about_fees_structure = $view_teachers_info->about_fees_structure;
			$demo_allowed = $view_teachers_info->demo_allowed;	
			$teacher_nickname = $view_teachers_info->students_konws_by;
			$teacher_present_place_of_work	= $view_teachers_info->present_place_of_work;
			$correspondance_address = $view_teachers_info->current_address;
			$teacher_city_id = $view_teachers_info->current_address_city;
			$registration_reference = $view_teachers_info->registration_reference;
		}
		
	}
	
	if(isset($city_data)) {
		foreach($city_data as $city_row) {
			
			$str_selected = '';			
			$city_id = $city_row->city_id;
			if($teacher_city_id == $city_id) {
				$str_selected = 'selected';
			}
			
			$str_city_options.='<option value="'.$city_id.'" '.$str_selected.'>'.$city_row->city_name.'</option>';
			
		}
		
	}
	
	$teacher_contact_modes_array = array();
	if(isset($teacher_contact_modes)) {
		foreach($teacher_contact_modes as $contact_mode) {
			$contact_mode_id = $contact_mode->contact_mode_id;
			if(!in_array($contact_mode_id,$teacher_contact_modes_array)) {
				$teacher_contact_modes_array[] = $contact_mode_id;
			}
		}
	}

	$str_contact_mode_options = '';
	
	if(isset($preferred_mode_of_contact)) {
		foreach($preferred_mode_of_contact as $contact_mode) {
			$contact_mode_id = $contact_mode->contact_mode_id;
			$str_selected = '';
			if(in_array($contact_mode_id, $teacher_contact_modes_array)) {
				$str_selected = 'selected';
			}
			$str_contact_mode_options.='<option value="'.$contact_mode_id.'" '.$str_selected.'>'.$contact_mode->contact_mode.'</option>';
		}
	}

	if(isset($pdg_teacher_user_info)){
		foreach($pdg_teacher_user_info as $user_data){
			$primary_contact_no = $user_data->mobile_no;
			$alternate_contact_no = $user_data->alternative_contact_no;
			$date_of_birth = $user_data->date_of_birth;
			if($user_data->gender == "male"){
				$gender = '<option value="male">Male</option><option value="female">Female</option><option value="other">Other</option>';
			}
			else if($user_data->gender == "female"){
				$gender = '<option value="female">Female</option><option value="male">Male</option><option value="other">Other</option>';	
			}
			else if($user_data->gender == "other"){
				$gender = '<option value="other">Other</option><option value="male">Male</option><option value="female">Female</option>';
			}
			else{
				$gender = '<option value="" disabled selected>--Select Gender--</option><option value="male">Male</option><option value="female">Female</option><option value="other">Other</option>';
			}
		}
		$teacher_birth_year = substr($date_of_birth,0,4);
		$teacher_birth_month = substr($date_of_birth,5,2);
		$teacher_birth_day = substr($date_of_birth,8,2);
	}
	else{
		$gender = '<option value="" disabled selected>--Select Gender--</option><option value="male">Male</option><option value="female">Female</option><option value="other">Other</option>';
	}

	$str_contact_mode_options = '';
	$str_grad_qualification_options = '';
	$str_post_grad_qualification_options = '';
	$str_professional_courses = '';
	
	$teacher_contact_modes_array = array();
	if(isset($teacher_contact_modes)) {
		foreach($teacher_contact_modes as $contact_mode) {
			$contact_mode_id = $contact_mode->contact_mode_id;
			if(!in_array($contact_mode_id,$teacher_contact_modes_array)) {
				$teacher_contact_modes_array[] = $contact_mode_id;
			}
		}
	}
	
	if(isset($preferred_mode_of_contact)) {
		foreach($preferred_mode_of_contact as $contact_mode) {
			$contact_mode_id = $contact_mode->contact_mode_id;
			$str_selected = '';
			if(in_array($contact_mode_id, $teacher_contact_modes_array)) {
				$str_selected = 'selected';
			}
			$str_contact_mode_options.='<option value="'.$contact_mode_id.'" '.$str_selected.'>'.$contact_mode->contact_mode.'</option>';
		}
	}

	$teacher_qualification_modes_array = array();
	if(isset($teacher_qualification_modes)) {
		foreach($teacher_qualification_modes as $qualification_mode) {
			$qualification_mode_id = $qualification_mode->qualification_id;			
			if(!in_array($qualification_mode_id,$teacher_qualification_modes_array)) {
				$teacher_qualification_modes_array[] = $qualification_mode_id;
			}
		}
	}

	if(isset($qualification_data)) {
		foreach($qualification_data as $qualification) {
			$qualification_name= $qualification->qualification;			
			$qualification_id = $qualification->qualification_id;			
			$qualification_type = $qualification->qualification_type;			
			$str_selected = '';
			switch($qualification_type) {
				case 'bachelor':
					if(in_array($qualification_id, $teacher_qualification_modes_array)){
						$str_selected = 'selected';
					}	
					$str_grad_qualification_options .= '<option value="'.$qualification_id.'" '.$str_selected.'>'.$qualification_name.'</option>';
					break;
				case 'postgrad':				
					if(in_array($qualification_id, $teacher_qualification_modes_array)){
						$str_selected = 'selected';
					}
					$str_post_grad_qualification_options .= '<option value="'.$qualification_id.'" '.$str_selected.'>'.$qualification_name.'</option>';
					break;
				case 'professional_course':
					if(in_array($qualification_id, $teacher_qualification_modes_array)){
						$str_selected = 'selected';
					}
					$str_professional_courses .= '<option value="'.$qualification_id.'" '.$str_selected.'>'.$qualification_name.'</option>';
					break;
			}
			
		}
	}	
	
	//avg_teaching_xp_id	
	$str_teaching_xp_options = '';
	if(isset($teaching_xp_data)) {
		foreach($teaching_xp_data as $teaching_xp) {
			$teaching_xp_id = $teaching_xp->teaching_xp_id;
			$str_selected = '';
			if($teaching_xp_id == $avg_teaching_xp_id) {
				$str_selected = 'selected';
			}
			$str_teaching_xp_options .= '<option value="'.$teaching_xp_id.'" '.$str_selected.'>'.$teaching_xp->teaching_xp.'</option>';
		}
	}
	
	$str_faculty_size_options = '';
	if(isset($faculty_size_data)) {
		foreach($faculty_size_data as $faculty_size) {
			$numbers_of_faculty = $faculty_size->size_of_faculty_id;
			$str_selected = '';
			if($numbers_of_faculty == $size_of_faculty) {
				$str_selected = 'selected';
			}
			$str_faculty_size_options .= '<option value="'.$numbers_of_faculty.'" '.$str_selected.'>'.$faculty_size->size_of_faculty.'</option>';
		}
	}
	
	
?>

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Teacher Details</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<label for="txt_teacher_first_name">First Name*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" value="<?php echo $teacher_first_name; ?>" id="txt_teacher_first_name" class="col-md-12 form-control validate[required]" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<label for="txt_teacher_last_name">Last Name*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" value="<?php echo $teacher_last_name; ?>" id="txt_teacher_last_name" class="col-md-12 form-control validate[required]" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<label for="txt_mobile_no">Mobile No.*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_mobile_no" value="<?php echo $primary_contact_no; ?>" class="col-md-12 form-control positive validate[required,minSize[10],maxSize[10]]" maxlength="10" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_alternate_contact_no">Alternate Contact No.</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_alternate_contact_no" value="<?php echo $alternate_contact_no; ?>" class="col-md-12 form-control" maxlength="10"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_address_of_correspondance">Address of Correspondance*</label>
					</div>
					<div class="col-md-8 form-group">
						<textarea id="txt_address_of_correspondance" rows="4" cols="50" class="col-md-12 form-control validate[required]" /><?php echo $correspondance_address; ?></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_teacher_city">City*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_teacher_city" class="form-control select_control_with_title validate[required]" title="Select a City">
							<option></option>
							<?php echo $str_city_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_prefered_mode_of_contact">Preferred mode of Contact*<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Select based on where you would like people from Pedagoge to contact you. You may choose multiple entries)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_preffered_mode_of_contact" class="form-control select_control_with_title_multiple validate[required]" multiple="multiple" title="Select Preferred Modes of Contact">
							<?php echo $str_contact_mode_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="Gender">Gender*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_gender" class="form-control validate[required]">
							<?php echo $gender; ?>
						</select>
					</div>
				</div>


				<div class="row">
					<div class="col-md-4">
						<label for="date_of_birth">Date of Birth (YYYY/MM/DD)</label>
					</div>
					<div class="col-md-8 form-group">
						<div class="row">
							<div class="col-md-4 col-xs-4">	
							<input type="text" id="dob_year" class="form-control positive" placeholder="YYYY" value = "<?php echo $teacher_birth_year; ?>" maxlength="4"/>
							</div>
							<div class="col-md-1 col-xs-1" style="padding-top: 5px;">
								/
							</div>
							<div class="col-md-3 col-xs-3">
								<input type="text" id="dob_month" class="form-control positive" placeholder="MM" value = "<?php echo $teacher_birth_month; ?>" maxlength="2"/>
							</div>
							<div class="col-md-1 col-xs-1" style="padding-top: 5px;">
								/
							</div>
							<div class="col-md-3 col-xs-3">
								<input type="text" id="dob_day" class="form-control positive" placeholder="DD" value = "<?php echo $teacher_birth_day; ?>" maxlength="2"/>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<strong>Academic Qualification</strong>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4">
						<label for="select_graduation">Graduation&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( If you have more than one, mention the most relevant)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_graduation" class="form-control select_control_with_title" title="--Select Graduation--" data-allow-clear = "true">
							<option></option>
							<?php echo $str_grad_qualification_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_post graduation">Post Graduation</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_postgraduation" class="form-control select_control_with_title" title="--Select Post Graduation--" data-allow-clear = "true">
							<option></option>
							<?php echo $str_post_grad_qualification_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_professional_qualification"> Professional Qualification&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( Any certificate or diploma courses that add to your credentials as a teacher)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_professional_qualification" class="form-control select_control_with_title_multiple" multiple="multiple" style="width: 75%">							
							<?php echo $str_professional_courses; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_other_qualification">Other Qualification&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Something out of the regular or super specialized that backs up your teaching capability)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_other_qualification" value= "<?php echo $other_qualification; ?>" class="col-md-12 form-control" />
					</div>
				</div>
				
				<br>
			</div>
		</div>
	</div>
	
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Coaching Details</div>
			<div class="panel-body">
				<br />
				
				
				<div class="row">
					<div class="col-md-4">
						<label for="hours_of_operation">Hours of Operation*&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(The range of time you are available to communicate or meet)"></i></label>
					</div>
					<div class="col-md-4 form-group">
						From:	<input type="text" id="from_hours_of_operation" value="<?php echo $operation_hours_from; ?>" class="col-md-12 form-control validate[required]" />
					</div>
					<div class="col-md-4 form-group">
						To:	<input type="text" id="to_hours_of_operation" value="<?php echo $operation_hours_to; ?>" class="col-md-12 form-control validate[required]" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<label for="select_average_teaching_experience">Teaching Experience</br>(in years)*&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(The average experience of your entire teaching faculty combined.)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_average_teaching_experience" class="select_control_with_title form-control validate[required]" title="--Select Average Teaching Experience--">
							<option></option>
							<?php echo $str_teaching_xp_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_present_place_of_work">Present Place of Work</br>(if any)<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(If you work in a position that adds to your credibility as a teacher. Eg: College Professor, Professional Player/Artist, etc.)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_present_place_of_work" class="col-md-12 form-control" value="<?php echo $teacher_present_place_of_work ?>" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_heading_of_profile">Heading of Profile*<br>(max 40 characters)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( Something that clearly communicates your area of work. Eg: Patrick Sir English Classes or Patrick Centre for English Language)"></i></label>
					</div>
					<div class="col-md-12 form-group character_count_heading_profile">
						<textarea id="txt_heading_of_profile" class="col-md-12 form-control validate[required] character_count" MaxLength="40" /><?php echo $profile_heading; ?></textarea>
						<div id="textarea_feedback_heading_profile" class="div_character_count"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_about_the_coaching">About The Coaching*<br>(max 2000 characters)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Let students know your teaching methodology, as well as what you expect of them. Other relevant details about taking classes with you.)"></i></label>
					</div>
					<div class="col-md-12 form-group character_count_about_the_coaching">
						<textarea id="txt_about_the_coaching" class="col-md-12 form-control validate[required] character_count"  MaxLength="2000" /><?php echo $about_coaching; ?></textarea>
						<div id="textarea_feedback_about_the_coaching" class="div_character_count"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_students_know_you_by">Students know You By(Nickname)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(What are you popularly known as? Many times students don’t know your actual name. Eg: Poltu Sir, Mithai Sir etc)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_students_know_you_by" class="col-md-12 form-control" value = "<?php echo $teacher_nickname; ?>" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<label for="txt_registration_reference">Reference#</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_registration_reference" class="col-md-12 form-control" value = "<?php echo $registration_reference; ?>" />
					</div>
				</div>

			</div>
		</div>
	</div>
</div>