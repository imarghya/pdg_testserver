<?php
	
	$str_demo_checked = '';
	$str_installments_checked = '';
	$str_achievements = '';
	$str_fees_description = '';	
	
	if(isset($view_teachers_data)) {		
		foreach($view_teachers_data as $teacher_info) {
			$demo_checked = $teacher_info->demo_allowed;
			$price_per_demo_class = $teacher_info->price_per_demo_class;			
			$installments_checked = $teacher_info->installment_allowed;
			$str_fees_description = $teacher_info->about_fees_structure;
			$str_demo_checked = $demo_checked == 'yes' ? ' checked ' : '';
			$str_installments_checked = $installments_checked == 'yes' ? ' checked ' : '';
		}
	}
	
	if(isset($achievements_data)) {
		foreach($achievements_data as $achievements) {
			$str_achievements .= '<input type="text" class="form-control txt_class_achievements" placeholder="Achievements" value="'.$achievements->achievement.'" />';
		}
	}
	
	$teacher_user_id = $pdg_current_user->ID;

	$str_gallery_list = '';
	$profile_image_path = 'storage/uploads/images/'; 
	$gallary_dir = PEDAGOGE_PLUGIN_DIR.$profile_image_path.$teacher_user_id.'/gallery/';
    
    $gallery_url = PEDAGOGE_PLUGIN_URL.'/'.$profile_image_path.$teacher_user_id.'/gallery/';
    
    if(is_dir($gallary_dir)) {
        
        $scanned_directory = array_diff(scandir($gallary_dir), array('..', '.'));
        foreach($scanned_directory as $gallery_file) {            
            $str_gallery_list .= '
            	<div class="col-sm-6 col-md-4">
					<div class="thumbnail">						
						<img src="'.$gallery_url.$gallery_file.'" class="img-responsive thumbbox" />
						<hr />
						<div class="caption">							
							<p>
								<button class="btn btn-danger col-xs-12 col-md-12 cmd_remove_gallery_file" data-file_name="'.$gallery_file.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>
							</p>
						</div>
					</div>
				</div>
            ';
        }
    }
	
	$teacher_profile_image_path = PEDAGOGE_PLUGIN_DIR.$profile_image_path.$teacher_user_id.'/profile.jpg';
    if(file_exists($teacher_profile_image_path)) {
    	$file_timestamp = date('U',filemtime($teacher_profile_image_path));
        $teacher_profile_image_url = PEDAGOGE_PLUGIN_URL.'/'.$profile_image_path.$teacher_user_id.'/profile.jpg?'.$file_timestamp;
    } else {
        $teacher_profile_image_url = PEDAGOGE_ASSETS_URL.'/images/sample.jpg';
    }
	
?>

<div class="panel panel-info">
	<div class="panel-heading">Final Details</div>
	<div class="panel-body">
				
		<div class="row">
			<div class="col-md-6 form-group">				
				<input type="checkbox" id="chk_payment_installments" <?php echo $str_installments_checked; ?>>
				<label for="chk_payment_installments">Please check if you allow payment in Installments</label>
				<hr />
				<input type="checkbox" id="chk_allow_demo_classes" <?php echo $str_demo_checked; ?>>
				<label for="chk_allow_demo_classes">Please check if you provide Demo Classes</label></br></br>
				<label for="price_per_demo_class" id="price_per_demo_class">Price per Demo Class</label>
				<input type="text" id="txt_price_per_demo_class" class="positive form-control" maxlength="5" value="<?php echo $price_per_demo_class ?>"/>				
			</div>
			<div class="col-md-6">
				<div class="form-group character_count_fee_description">
					<label for="txt_fees_structure_description">Description About Fee Structure(max 1000 characters)&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( If you want give details about scholarships, group discounts or fee breakdown, this field is open for that. If your fee is negotiable, mention the same here as well.)"></i></label>
					<textarea id="txt_fees_structure_description" class="col-md-12 form-control" MaxLength="1000" /><?php echo $str_fees_description; ?></textarea>
					<div id="textarea_feedback_fee_description"></div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="panel panel-info">
	<div class="panel-heading panel-heading-responsive">Achievements Of Your Classes&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( If you have any achievements of your students in the recent past and would help you’re students with decision making. Fill in the fields accordingly.)"></i></div>
	<div class="panel-body" style="margin-left: 5px; margin-right: 5px;">		
		<div class="row">			
			<div class="col-md-12" id="div_achievements">
				<div class="row">
					<?php echo $str_achievements; ?>
					<input type="text" class="form-control txt_class_achievements" placeholder="Achievements" />
				</div>
			</div>
		</div>		
	</div>
</div>

<div class="panel panel-info">
	<div class="panel-heading panel-heading-responsive">Logo and Class Images</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				
				<div class="panel panel-info">
					<div class="panel-heading panel-heading-responsive">Profile Image/Logo</div>
					<div class="panel-body">
						
						<div class="" id="crop-avatar"> <!-- crop-avatar -->

						    <!-- Current avatar -->
							<div class="avatar-view" title="Change the avatar">
						      <img src="<?php echo $teacher_profile_image_url; ?>" alt="Avatar">
							</div>
						    
						    <!-- Cropping modal -->
						    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
						      <div class="modal-dialog modal-lg">
						        <div class="modal-content">
						          <form class="avatar-form" action="" enctype="multipart/form-data" method="post">
						            <div class="modal-header">
						              <button type="button" class="close" data-dismiss="modal">&times;</button>
						              <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
						            </div>
						            <div class="modal-body">
						              <div class="avatar-body">
						
						                <!-- Upload image and data -->
						                <div class="avatar-upload">
						                  <input type="hidden" class="avatar-src" name="avatar_src">
						                  <input type="hidden" class="avatar-data" name="avatar_data">
						                  <label for="avatarInput">Local upload</label>
						                  <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
						                </div>
						
						                <!-- Crop and preview -->
						                <div class="row">
						                  <div class="col-md-9">
						                    <div class="avatar-wrapper"></div>
						                  </div>
						                  <div class="col-md-3">
						                    <div class="avatar-preview preview-lg"></div>
						                    <div class="avatar-preview preview-md"></div>
						                    <div class="avatar-preview preview-sm"></div>
						                  </div>
						                </div>
						
						                <div class="row avatar-btns">
						                  <div class="col-md-9 crop_result_area">
						                  </div>
						                  <div class="col-md-3">
						                    <button type="button" class="btn btn-primary btn-block avatar-save" data-loading-text="Uploading image..."><i class="fa fa-cloud-upload" aria-hidden="true"></i> Crop &amp; Upload</button>
						                  </div>
						                </div>
						              </div>
						            </div>
						            <!-- <div class="modal-footer">
						              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						            </div> -->
						          </form>
						        </div>
						      </div>
						    </div><!-- /.modal -->
						    
						</div> <!-- crop-avatar -->
					</div>
				</div>
				
				<div class="panel panel-info">
					<div class="panel-heading panel-heading-responsive">Documents Manager</div>
					<div class="panel-body">
						<button class="btn btn-warning col-md-12" id="cmd_teacher_institute_documents_manager" data-teacher_institute_userid="<?php echo $teacher_user_id; ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Load Documents Manager</button>
					</div>
				</div>
				
				
			</div>
			
			<div class="col-md-9">
				<div class="panel panel-info">
					<div class="panel-heading panel-heading-responsive">Class Images</div>
					<div class="panel-body">
						<div class="panel panel-warning">
							<div class="panel-body">
								
								<div class="row">									
									<div class="col-md-6">
										<input type="text" class="form-control" disabled value="" id="txt_teacher_class_image_file_name"/>
									</div>
									<div class="col-md-3">
										<button class="btn btn-warning col-md-12 col-sm-12 col-xs-12" id="cmd_reset_class_image_selection"><i class="fa fa-refresh" aria-hidden="true"></i> Reset Selection</button>
									</div>
									<div class="col-md-3">
										<input id="fileinput_teacher_class_image" name="fileinput_teacher_class_image" type="file" style="display:none;" accept="image/*" />
										<button class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="cmd_upload_class_image" data-loading-text="Loading...">
											<i class="fa fa-upload" aria-hidden="true"></i> <span id="span_select_image">Select Image</span>
										</button>
									</div>
								</div>
								<br />
								<!-- <div class="progress">
								  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 2%;">
								    0% Complete
								  </div>
								</div> -->
								<div id="div_class_image_result_area"></div>
							</div>
						</div>
						
						<div class="row" id="div_class_images_list">
							<?php echo $str_gallery_list; ?>
						</div>
					</div>
				</div>
			</div>
			
		</div>	    
	</div>
</div>

