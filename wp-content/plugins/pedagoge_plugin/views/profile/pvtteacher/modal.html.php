<div class="modal fade rating_model" id="tech_reg_success" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Teacher Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="review_holder">
        	<h4>Success</h4>
            <p class="bullet_decoration">Thankyou for Registering With pedagoge . Your Account Will be Activated after admin approval.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="add_loc_modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
       <div class="modal-content">
         
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h5 class="modal-title" id="avatar-modal-label">Add Location</h5>
             </div>
              <form class="avatar-form" enctype="multipart/form-data" method="post">
             <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Location:</label>
                    <input type="text" class="form-control" id="st_add_loc">
                  </div>
             </div>
             </form>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="st_add_loc_btn" class="btn btn-primary">Add</button>
              </div>
          
       </div>
    </div>
</div>

<div class="modal fade" id="add_loc_modal2" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
       <div class="modal-content">
         
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h5 class="modal-title" id="avatar-modal-label">Add Location</h5>
             </div>
              <form class="avatar-form" enctype="multipart/form-data" method="post">
             <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Location:</label>
                    <input type="text" class="form-control" id="te_add_loc">
                  </div>
             </div>
             </form>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="te_add_loc_btn" class="btn btn-primary">Add</button>
              </div>
          
       </div>
    </div>
</div>
