<?php

$hidden_user_id = 0;
$hidden_teacher_id = 0;
$hidden_user_email = '';

$is_active = '';
$is_approved = '';
$is_verified = '';

if(isset($pdg_current_user)) {
	$hidden_user_id = $pdg_current_user->ID;
	$hidden_user_email = $pdg_current_user->user_email;
	$hidden_user_email = wp_hash_password($hidden_user_email);
}
if(isset($view_teachers_data)) {
	foreach($view_teachers_data as $teacher) {
		$hidden_teacher_id = $teacher->teacher_id;
		$is_active = $teacher->active;
		$is_approved = $teacher->approved;
		$is_verified = $teacher->verified;
	}
}

$user_personal_info_id = '';
if(isset($pdg_teacher_user_info)) {
	foreach($pdg_teacher_user_info as $user_info) {
		$user_personal_info_id = $user_info->personal_info_id;
	}
}

$str_active = '';
$str_verified = '';
$str_approved = '';
if($is_active == 'no') {
	$str_active = '<div class="alert alert-warning">Your profile is not active! You need to input correct data in your profile.</div>';
}
if( $is_verified == 'no') {
	$str_verified = '<div class="alert alert-info">Your profile information is not verified yet!</div>';
}
if($is_approved == 'no') {
	$str_approved = '<div class="alert alert-warning">Your profile is not approved! You need to input correct data in your profile.</div>';
}
?>

<section class="site-section site-section-top"  id="top">
	<h3 class="text-center">Update your information!</h3>	
	<div class="alert alert-info text-center">Note - Rest assured! Your personal information (Contact No, Address, DOB etc.) is secured and will never be made public.</div>
	<div class="well">
		<div id="div_teacher_form_wizard">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" id="li_teacher_intro" class="active"><a href="#teacher_intro" class="insti_tabs" aria-controls="teacher_intro" ><i class="fa fa-info-circle" aria-hidden="true"></i> Introduction</a></li>
				<li role="presentation" id="li_teacher_class_details"><a href="#teacher_class_details" class="insti_tabs" aria-controls="teacher_class_details"><i class="fa fa-list-alt" aria-hidden="true"></i> Batch Details</a></li>
				<li role="presentation" id="li_teacher_form_finished"><a href="#teacher_form_finished" class="insti_tabs" aria-controls="teacher_form_finished"><i class="fa fa-crosshairs" aria-hidden="true"></i> Finish</a></li>			
			</ul>
			
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="teacher_intro">
					<div class="row">														
						<div class="col-md-9"><img class="center-block hidden_item img_teacher_save_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-success cmd_show_class_details col-md-12 col-sm-12 col-xs-12">Next <i class="fa fa-hand-o-right" aria-hidden="true"></i></button>
						</div>
					</div>
					<div class="row"><div class="col-md-12 div_teacher_save_result_area"></div></div>
					<form id="frm_teacher_intro">
						<?php echo $this->load_view('profile/pvtteacher/teacher_intro'); ?>
					</form>
					<div class="row"><div class="col-md-12 div_teacher_save_result_area"></div></div>
					<div class="row">														
						<div class="col-md-9"><img class="center-block hidden_item img_teacher_save_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-success cmd_show_class_details col-md-12 col-sm-12 col-xs-12">Next <i class="fa fa-hand-o-right" aria-hidden="true"></i></button>
						</div>
					</div>
				</div>
				
				<div class="tab-pane" id="teacher_class_details">
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-danger cmd_show_teacher_intro col-md-12 col-sm-12 col-xs-12">Previous <i class="fa fa-hand-o-left" aria-hidden="true"></i></button>
						</div>
						<div class="col-md-6"><img class="center-block hidden_item img_teacher_save_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
						<div class="col-md-3 col-sm-12">
							<button data-loading-text="Saving..." class="btn btn-success cmd_show_teacher_form_finished col-md-12 col-sm-12 col-xs-12">Next <i class="fa fa-hand-o-right" aria-hidden="true"></i></button>
						</div>
					</div>
					<div class="row"><div class="col-md-12 div_teacher_save_result_area"></div></div>
					<form id="frm_teacher_class_details">
						<?php echo $this->load_view('profile/pvtteacher/teacher_class_details'); ?>
					</form>
					<div class="row"><div class="col-md-12 div_teacher_save_result_area"></div></div>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-danger cmd_show_teacher_intro col-md-12 col-sm-12 col-xs-12">Previous <i class="fa fa-hand-o-left" aria-hidden="true"></i></button>
						</div>
						<div class="col-md-6"><img class="center-block hidden_item img_teacher_save_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-success cmd_show_teacher_form_finished col-md-12 col-sm-12 col-xs-12">Next <i class="fa fa-hand-o-right" aria-hidden="true"></i></button>
						</div>
					</div>
				</div>
				
				<div class="tab-pane" id="teacher_form_finished">
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-danger cmd_show_class_details2 col-md-12 col-sm-12 col-xs-12">Previous <i class="fa fa-hand-o-left" aria-hidden="true"></i></button>
						</div>
						<div class="col-md-6"><img class="center-block hidden_item img_teacher_save_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-success cmd_submit_teacher_details col-md-12 col-sm-12 col-xs-12"><i class="fa fa-floppy-o" aria-hidden="true"></i> Submit</button>
						</div>
					</div>
					<div class="row"><div class="col-md-12 div_teacher_save_result_area"></div></div>
					<form id="frm_teacher_form_finished">
						<?php echo $this->load_view('profile/pvtteacher/teacher_form_finish'); ?>
					</form>
					<div class="row"><div class="col-md-12 div_teacher_save_result_area"></div></div>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-danger cmd_show_class_details2 col-md-12 col-sm-12 col-xs-12">Previous <i class="fa fa-hand-o-left" aria-hidden="true"></i></button>
						</div>
						<div class="col-md-6"><img class="center-block hidden_item img_teacher_save_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<button data-loading-text="Saving..." class="btn btn-success cmd_submit_teacher_details col-md-12 col-sm-12 col-xs-12"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Submit</button>
						</div>
					</div>
				</div>
				<input type="hidden" value="<?php echo $hidden_teacher_id; ?>" id="hidden_dynamic_insti_var"/> 
				<input type="hidden" value="<?php echo $hidden_user_id; ?>" id="hidden_dynamic_user_var"/>
				<input type="hidden" value="<?php echo $hidden_user_email; ?>" id="hidden_dynamic_profile_edit_secret_key"/>
				<input type="hidden" value="<?php echo $user_personal_info_id; ?>" id="hidden_dynamic_user_info_var"/>
			</div>
		</div>
		
	</div>
	
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/modals/registration_complete_modal.html.php'); ?>
	<?php include_once(PEDAGOGE_PLUGIN_DIR.'views/modals/teacher_institute_documents_manager.html.php'); ?>
</section>