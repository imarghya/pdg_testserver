<?php

	$institute_id = '';
	$institute_name = '';
	$institute_user_id = '';
	$contact_person_name = '';
	
	$primary_contact_no = '';
	$alternate_contact_no = '';
	$other_communication_mode = '';
	$correspondance_address = '';
	$insti_city_id = '';
	$profile_heading = '';
	$size_of_faculty = '';
	$operation_hours_from = '';
	$operation_hours_to = '';
	$about_coaching = '';
	$avg_teaching_xp_id = '';
	$allow_installments = '';
	$about_fees_structure = '';
	$demo_allowed = '';
	$no_of_demo_class = '';
	$price_per_demo_class = '';
	$registration_reference = '';
	
	if(isset($institute_data)) {		
		foreach($institute_data as $institute_info) {
			$institute_id = $institute_info->institute_id;
			$institute_name = $institute_info->institute_name;
			$institute_user_id = $institute_info->user_id;
			$contact_person_name = $institute_info->contact_person_name;
			$registration_reference = $institute_info->registration_reference;
			$primary_contact_no = $institute_info->primary_contact_no;
			$alternate_contact_no = $institute_info->alternate_contact_no;
			$other_communication_mode = $institute_info->other_communication_mode;
			$correspondance_address = $institute_info->correspondance_address;
			$insti_city_id = $institute_info->city_id;
			$profile_heading = $institute_info->profile_heading;
			$size_of_faculty = $institute_info->size_of_faculty_id;
			$operation_hours_from = $institute_info->operation_hours_from;
			$operation_hours_to = $institute_info->operation_hours_to;
			$about_coaching = $institute_info->about_coaching;
			$avg_teaching_xp_id = $institute_info->avg_teaching_xp_id;
			$allow_installments = $institute_info->allow_installments;
			$about_fees_structure = $institute_info->about_fees_structure;
			$demo_allowed = $institute_info->demo_allowed;
			$no_of_demo_class = $institute_info->no_of_demo_class;
			$price_per_demo_class = $institute_info->price_per_demo_class;
		}
	}


	$str_contact_mode_options = '';
	$str_grad_qualification_options = '';
	$str_post_grad_qualification_options = '';
	$str_professional_courses = '';
	$str_city_options = '';
	
	$institute_contact_modes_array = array();
	if(isset($institute_contact_modes)) {
		foreach($institute_contact_modes as $contact_mode) {
			$contact_mode_id = $contact_mode->contact_mode_id;
			if(!in_array($contact_mode_id,$institute_contact_modes_array)) {
				$institute_contact_modes_array[] = $contact_mode_id;
			}
		}
	}
	
	if(isset($preferred_mode_of_contact)) {
		foreach($preferred_mode_of_contact as $contact_mode) {
			$contact_mode_id = $contact_mode->contact_mode_id;
			$str_selected = '';
			if(in_array($contact_mode_id, $institute_contact_modes_array)) {
				$str_selected = 'selected';
			}
			$str_contact_mode_options.='<option value="'.$contact_mode_id.'" '.$str_selected.'>'.$contact_mode->contact_mode.'</option>';
		}
	}


	if(isset($city_data)) {
		foreach($city_data as $city_row) {
			$str_selected = '';			
			$city_id = $city_row->city_id;
			if($insti_city_id == $city_id) {
				$str_selected = 'selected';
			}
			
			$str_city_options.='<option value="'.$city_id.'" '.$str_selected.'>'.$city_row->city_name.'</option>';
		}
	}
	//avg_teaching_xp_id	
	$str_teaching_xp_options = '';
	if(isset($teaching_xp_data)) {
		foreach($teaching_xp_data as $teaching_xp) {
			$teaching_xp_id = $teaching_xp->teaching_xp_id;
			$str_selected = '';
			if($teaching_xp_id == $avg_teaching_xp_id) {
				$str_selected = 'selected';
			}
			$str_teaching_xp_options .= '<option value="'.$teaching_xp_id.'" '.$str_selected.'>'.$teaching_xp->teaching_xp.'</option>';
		}
	}
	
	$str_faculty_size_options = '';
	if(isset($faculty_size_data)) {
		foreach($faculty_size_data as $faculty_size) {
			$numbers_of_faculty = $faculty_size->size_of_faculty_id;
			$str_selected = '';
			if($numbers_of_faculty == $size_of_faculty) {
				$str_selected = 'selected';
			}
			$str_faculty_size_options .= '<option value="'.$numbers_of_faculty.'" '.$str_selected.'>'.$faculty_size->size_of_faculty.'</option>';
		}
	}
	
	
?>

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Institution Details</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<label for="txt_institution_name">Institution Name*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" value="<?php echo $institute_name; ?>" id="txt_institution_name" class="col-md-12 form-control validate[required]" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<label for="txt_contact_person">Contact Person&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( This is the person who will be the point of communication on behalf of the institute.)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_contact_person" value="<?php echo $contact_person_name; ?>" class="col-md-12 form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_mobile_no">Mobile No.*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_mobile_no" value="<?php echo $primary_contact_no; ?>" class="col-md-12 form-control positive validate[required,minSize[10],maxSize[10]]" maxlength="10" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_alternate_contact_no">Alternate Contact No.</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_alternate_contact_no" value="<?php echo $alternate_contact_no; ?>" class="col-md-12 form-control" maxlength="10"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_prefered_mode_of_contact">Prefered mode of Contact*<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Select based on where you would like us to contact you. You may choose multiple)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_preffered_mode_of_contact" class="form-control select_control_with_title_multiple validate[required]" multiple="multiple" title="Select Preffered Modes of Contact">
							<?php echo $str_contact_mode_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_address_of_correspondance">Address of Correspondance*</label>
					</div>
					<div class="col-md-8 form-group">
						<textarea id="txt_address_of_correspondance" rows="4" cols="50" class="col-md-12 form-control validate[required]" /><?php echo $correspondance_address; ?></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_institute_city">City*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_institute_city" class="form-control select_control_with_title validate[required]" title="Select a City">
							<option></option>
							<?php echo $str_city_options; ?>
						</select>
					</div>
				</div>
				
				<br>
			</div>
		</div>
	</div>
	
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Coaching Details</div>
			<div class="panel-body">
				<br />
				
				<div class="row">
					<div class="col-md-4">
						<label for="select_size_of_faculty">Size of Faculty*&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(The size of your teaching faculty only.)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_size_of_faculty" class="select_control_with_title form-control validate[required]" title="Select size of faculty">
							<option></option>
							<?php echo $str_faculty_size_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="hours_of_operation">Hours of Operation*&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(The range of time you are available to communicate or meet)"></i></label>
					</div>
					<div class="col-md-4 form-group">
						From:	<input type="text" id="from_hours_of_operation" value="<?php echo $operation_hours_from; ?>" class="col-md-12 form-control validate[required]" />
					</div>
					<div class="col-md-4 form-group">
						To:	<input type="text" id="to_hours_of_operation" value="<?php echo $operation_hours_to; ?>" class="col-md-12 form-control validate[required]" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<label for="select_average_teaching_experience">Average Teaching Experience(in years)*&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(The average experience of your entire teaching faculty combined.)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_average_teaching_experience" class="select_control_with_title form-control validate[required]" title="Select Average Teaching Experience">
							<option></option>
							<?php echo $str_teaching_xp_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_heading_of_profile">Heading of Profile*<br>(max 40 characters)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( Something that clearly communicates your area of work. Eg: Patrick Sir English Classes or Patrick Centre for English Language)"></i></label>
					</div>
					<div class="col-md-12 form-group character_count_heading_profile">
						<textarea id="txt_heading_of_profile" class="col-md-12 form-control validate[required] character_count" MaxLength="40" /><?php echo $profile_heading; ?></textarea>
						<div id="textarea_feedback_heading_profile" class="div_character_count"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_about_the_coaching">About The Coaching*<br>(max 2000 characters)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Let students know your teaching methodology, as well as what you expect of them. Other relevant details about taking classes with you.)"></i></label>
					</div>
					<div class="col-md-12 form-group character_count_about_the_coaching">
						<textarea id="txt_about_the_coaching" class="col-md-12 form-control validate[required] character_count"  MaxLength="2000" /><?php echo $about_coaching; ?></textarea>
						<div id="textarea_feedback_about_the_coaching" class="div_character_count"></div>
					</div>					
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<label for="txt_registration_reference">Reference#</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_registration_reference" class="col-md-12 form-control" value = "<?php echo $registration_reference; ?>" />
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>