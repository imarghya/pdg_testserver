<?php
	
	$str_class_timing_options = '';
	if(isset($class_timing_data)) {
		foreach($class_timing_data as $class_timing) {
			$class_timing_id = $class_timing->class_timing_id;
			$class_timing_name = $class_timing->class_timing;
			$str_class_timing_options .= '<option value="'.$class_timing_id.'">'.$class_timing_name.'</option>';
		}
	}
	
	$str_fees_type_options = '';	
	if(isset($fees_type_data)) {
		foreach($fees_type_data as $fees_type) {
			$fees_type_id = $fees_type->fees_type_id;
			$fees_type_name = $fees_type->fees_type;
			$str_fees_type_options .= '<option value="'.$fees_type_id.'">'.$fees_type_name.'</option>';
		}
	}
	
	$str_schools_options = '';
	
	if(isset($schools_data)) {
		foreach($schools_data as $schools_db) {
			$school_id = $schools_db->school_id;
			$school_name = $schools_db->school_name;
			$str_schools_options .= '<option value="'.$school_id.'">'.$school_name.'</option>';
		}
	}

	$str_class_capacity_options = '';
	if(isset($class_capacity_data)) {
		foreach($class_capacity_data as $class_capacity_db) {
			$str_class_capacity_options .= '<option value="'.$class_capacity_db->pdg_class_capacity_id.'">'.$class_capacity_db->pdg_class_capacity.'</option>';
		}
	}

	/*Location Details: Locality Data Generation*/
	
	$str_localities_options = '';
	if(isset($found_localities) && !empty($found_localities)) {
		foreach($found_localities as $found_locality) {
			$locality_name = $found_locality->locality;
			$locality_id = $found_locality->locality_id;
			$city_name = $found_locality->city_name;
			$str_localities_options .= '<option value="'.$locality_id.'">'.$locality_name.' ('.$city_name.')</option>';
		}
	} 
	/*Location Details: Locality Data Generation ends*/
	
	$str_subject_name_options = '';
	if(isset($subject_names_data) && !empty($subject_names_data)) {
		foreach($subject_names_data as $subject_names_info) {
			$subject_name = $subject_names_info->subject_name;
			$subject_name_id = $subject_names_info->subject_name_id;
			$str_subject_name_options .= '<option value="'.$subject_name_id.'">'.$subject_name.'</option>';
		}
	} 
?>

<div class="panel panel-success">
	<div class="panel-heading">Batch List</div>
	<div class="panel-body">	
		<div class="div_batches_data table-responsive">
			<table id="tbl_batches_list" class="table table-striped table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>ID</th>
						<th>Subjects</th>
						<th>Category</th>
						<th>Board</th>
						<th>Age</th>
						<th>Locations</th>
						<th>Delete</th>
						<th>Copy</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
					<?php echo ControllerPrivateinstitute::fn_return_batches_list($pdg_current_user->ID); ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-info">
	<div class="panel-heading">
		Subjects List (You can add multiple subjects in a batch)
		<span class="span_subjects_loader_area pull-right"></span>
		<span class="pull-right"><img class="hidden_item img_subjects_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></span>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="select_subject_name">Select Subjects</label>
				    <select id="select_subject_name" class="form-control">
				    	<option></option>
						<?php echo $str_subject_name_options; ?>							
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				    <label for="select_subject_category">Subject Category</label>
				    <select name="" id="select_subject_category" class="form-control select_control" multiple="multiple"></select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				    <label for="select_subject_age_category">Age Category</label>
				    <select name="" id="select_subject_age_category" class="form-control select_control" multiple="multiple"></select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
				    <label for="select_subject_academic_board">Academic Boards</label>
				    <select name="" id="select_subject_academic_board" class="form-control select_control" multiple="multiple"></select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-12 col-xs-12 div_add_subject_result"></div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<button class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="cmd_add_more_subjects"><i class="fa fa-plus-square" aria-hidden="true"></i> Add More Subjects</button>
			</div>
		</div><hr />
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-6 alert-info">Course List for the batch</div>
					<div class="col-md-6 alert-danger">(Uncheck the courses you do not want)</div>
				</div>
			</div>
			<div class="panel-body">		
				<div class="table-responsive">
					<table id="tbl_course_list" class="table table-striped table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th class="hidden_item">Course ID</th>
								<th>Subject Name</th>
								<th>Subject Category</th>
								<th>Age Category</th>
								<th>Academic Board</th>
								<th>Selected</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row institute_class_details"> <!-- institute_class_details -->
	<div class="col-md-12">
		<button class="btn btn-warning col-md-4" id="cmd_reset_batch_details_form">Reset Form</button>
	</div>
	<div class="col-md-6"> <!-- First part of the class details -->
		<div class="panel panel-info">
			<div class="panel-heading">
				Batch Details <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" 
				title="(Fill in the details considering the batch is designated to a subject. If you have multiple courses you would be required to create multiple batches.)"></i>
			</div>
			<div class="panel-body">
				<input type="hidden" id="hidden_batch_id" />
				<br />
				<div class="row form-group">
					<div class="col-md-4">
						<label for="select_course_days">Batch Days*</label>
					</div>
					<div class="col-md-8">
						<select id="select_course_days" class="form-control select_control_with_title_multiple" multiple="multiple" title="Select tuition Days">
							<option value="Monday">Monday</option>
							<option value="Tuesday">Tuesday</option>
							<option value="Wednesday">Wednesday</option>
							<option value="Thursday">Thursday</option>
							<option value="Friday">Friday</option>
							<option value="Saturday">Saturday</option>
							<option value="Sunday">Sunday</option>
						</select>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-4">
						<label for="select_no_of_days">No.of Classes per batch*<br>(per week)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(How many classes would a student be expected to attend in a week.
						)"></i></label>
					</div>
					<div class="col-md-8">
						<select id="select_no_of_days" class="form-control select_control_with_title" title="Select No.of Classes per batch (per week)">
							<option></option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_no_of_hours">Duration per Batch*<br>(in hours)&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(How long would a single class be (for a student).
						)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_no_of_hours" class="form-control select_control_with_title" title="Select Duration per Batch(in hours)">
							<option></option>
							<option value="0.5">0.5</option>
							<option value="1">1</option>
							<option value="1.5">1.5</option>
							<option value="2">2</option>
							<option value="2.5">2.5</option>
							<option value="3">3</option>
							<option value="3.5">3.5</option>
							<option value="4">4</option>
							<option value="4.5">4.5</option>
							<option value="5">5</option>
							<option value="5.5">5.5</option>
							<option value="6">6</option>
							<option value="6.5">6.5</option>
							<option value="7">7</option>
							<option value="flexible">Flexible</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_class_timing_slot">Batch Timing Slot*&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(What time during the day would this class take place? Multiple can be chosen. (This need not be exact, students only want to know the tentative timings.
						)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_class_timing_slot" class="form-control select_control_with_title_multiple" multiple="multiple" title="Select Class Timing">							
							<?php echo $str_class_timing_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_class_type">Batch Type*&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Do you teach a single student or multiple students in a batch? If multiple, how many students do you teach in a batch?
						)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_class_type" class="form-control select_control_with_title" title="Select Batch Type">
							<option></option>
							<option value="single">Single Student</option>
							<option value="multiple">Multiple Student</option>
							<option value="both">Both</option>
						</select>
					</div>
				</div>
				<div class="row select_max_students_per_class">
					<div class="col-md-4">
						<label for="select_max_students_per_class">Maximum Students<br>Per Class</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_max_students_per_class" class="form-control select_control_with_title" title="Maximum students per class">
							<option></option>
							<?php echo $str_class_capacity_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_length_of_course">Length of Batch&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(If your batch has a definite elapse time then mentions it. Else leave it empty.
						)"></i></label>
					</div>
					<div class="col-md-2">
						<input type="text" id="txt_length_of_course" class="col-md-12 positive form-control" MaxLength="2"/>
					</div>
					<div class="col-md-6 form-group">
						<select id="select_length_of_course" class="form-control select_control_with_title" title="Select Length of Batch">
							<option></option>
							<option value="days">Days</option>
							<option value="weeks">Weeks</option>
							<option value="months">Months</option>
							<option value="years">Years</option>
						</select>
					</div>
				</div>
				
				<div class="row select_school_students">
					<div class="col-md-12">
						<label for="select_school_students">Students Of Which School Do You Generally Teach?&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Do you have a preference of schools? Students from which school(s) do you teach now?)"></i></label>
					</div>
					<br />
					<div class="col-md-12 form-group">
						<select id="select_school_students" class="form-control select_control_with_title_multiple" multiple="multiple" title="Select Schools">
							<?php echo $str_schools_options; ?>
						</select>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_about_the_course">About The Batch*&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="(Any details about the batch you would like to provide the students. This could vary from mock tests to level of teaching followed at the batch.)"></i><br>(max. 1000 characters)</label>
					</div>
					<div class="col-md-12 form-group character_count_about_the_batch">
						<textarea id="txt_about_the_course" class="col-md-12 form-control character_count" MaxLength="1000" /></textarea>
						<div id="textarea_feedback_about_the_batch" class="div_character_count"></div>
					</div>
				</div>
				
			</div>
			
		</div>
		
		
	</div> <!-- First part of the class details -->
	
	<div class="col-md-6">  <!-- second part of the class details -->
		<div class="panel panel-info">
			<div class="panel-heading">Fee Details</div>
			<div class="panel-body">
				<br />
				<div class="row">
					<div class="col-md-4">
						<label for="select_fees_type">Fee Collected(in rupees)*&nbsp;&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up" title="( Please enter your chosen frequency of fee collection. Multiple can be selected. Admission fee must be exclusive of other fee(s).
						)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select class="form-control select_control_with_title_multiple" id="select_fees_type" multiple="multiple" title="Select Fees Types">
							<?php echo $str_fees_type_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 div_fees_colletion_fields"></div>
				</div>
			</div>
		</div>	
		
		<div class="panel panel-info">
			<!--location details-->
			<div class="panel-heading">Please select locations where you teach...</div>
			<div class="panel-body div_locations_area">
				<br />
				
				<div class="row">
					<div class="col-md-4">
						<label for="select_location_own">Own Location</label>
					</div>
					<div class="col-md-8">
						<select class="form-control select_control_with_title_multiple" id="select_location_own" multiple="multiple" title="Select Own Locations">
	      					<?php echo $str_localities_options; ?>	        
	      				</select>
	      				<div class="div_own_location_fees"></div>
					</div>
				</div><br />
				<div class="row">
					<div class="col-md-4">
						<label for="select_location_student">Student's Location</label>
					</div>
					<div class="col-md-8">
						<select class="form-control select_control_with_title_multiple" id="select_location_student" multiple="multiple" title="Select Students Locations">
	      					<?php echo $str_localities_options; ?>	        
	      				</select>
	      				<div class="div_student_location_fees"></div>
					</div>
				</div><br />
				<div class="row">
					<div class="col-md-4">
						<label for="select_location_institute">Institute's Location</label>
					</div>
					<div class="col-md-8">
						<select class="form-control select_control_with_title_multiple" id="select_location_institute" multiple="multiple" title="select Institute's Locations">
	      					<?php echo $str_localities_options; ?>	        
	      				</select>
	      				<div class="div_institute_location_fees"></div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- second part of the class details -->
	
</div> <!-- institute_class_details ends -->

<div class="row">
	<div class="col-md-8">
		<div class="div_add_batch_result_area"></div>
		<div class="pull-right"><img class="hidden_item img_batches_loader" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" /></div>
	</div>
	<div class="col-md-4">
		<button class="btn btn-success cmd_add_more_batch col-md-12 col-sm-12 col-xs-12" data-loading-text="Adding batch..."><i class="fa fa-plus-circle" aria-hidden="true"></i> Save and add more Batch</button>
	</div>
</div>
<hr />