<?php

/*$array_val = array ( 'phone_no' => array ( 0, '01' ), 'age2' => '0', 'age3' => '130' );
$vObj      = new ValidatorPedagoge( $array_val );

var_dump( $vObj->set_rules( array (
	'name'         => 'phone_no',
	//'matches'     => array ( 'match', 4 ),
	'placeholder'  => 'Phone Number',
	//'differs'     => array ( 'first',  4),
	//'is_unique'   => array ( 'pdg_user_info', 'mobile_no' ),
	//'min_length'  => 4,
	//'exact_length'  => 4,
	//'max_length'  => 4,
	//'max_item_limit'   => 4,
	//'min_item_limit'   => 3,
	//'exact_item_limit' => 4,
	'trim'         => true,
	'unique_items' => true,
	'type'         => 'custom',
	//'trim'           => true,
	'required'     => true,
	'allow_array'  => true,
	'is_array'     => true,
	//'sanitize_input' => true,
	//'in_list'     => array ( 20 => "hi", 54 => "hi", 22 => "hi" ),
	//'in_list'          => array ( 111, 222, 333 ),

) ) );

var_dump( $vObj->set_rules( array (
	'name'           => 'age2',
	//	'matches'     => array ( 'match val', 44245554 ),
	//'differs'     => array ( 'second',  4424555),
	//'is_unique'   => array ( 'pdg_user_info', 'mobile_no' ),
	//'min_length'  => 5,
	//'max_length'  => 5,
	//'exact_length'  => 5,
	'placeholder'    => 'Age 2',
	'type'           => 'custom',
	//'trim'             => true,
	'numeric'        => true,
	//'required'         => true,
	'sanitize_input' => true,
	//'in_list'          => array ( 20, 21, 22 ),
	//'in_list'          => array ( 111, 222, 333, 44 ),
) ) );

var_dump( $vObj->set_rules( array (
	'name'           => 'age3',
	//	'matches'     => array ( 'match val', 44245554 ),
	//'differs'     => array ( 'second',  4424555),
	//'is_unique'   => array ( 'pdg_user_info', 'mobile_no' ),
	//'min_length'  => 5,
	//'max_length'  => 5,
	//'exact_length'  => 5,
	'placeholder'    => 'Age 3',
	'type'           => 'custom',
	//'trim'             => true,
	'numeric'        => true,
	//'required'         => true,
	'sanitize_input' => true,
	//'in_list'          => array ( 20, 21, 22 ),
	//'in_list'          => array ( 111, 222, 333, 44 ),
) ) );


$vObj->group_set_rules( array (
	//'custom_error'      => 'group error',
	'group_placeholder' => 'Personal info',
	'group_name'        => 'personal_info',
	'items'             => array ( 'age2', 'phone_no', 'age3' ),
	'min_item_selected' => '',
) );

$vObj->group_set_rules( array (
	//'custom_error'      => 'group error',
	'group_placeholder' => 'Personal info 1',
	'group_name'        => 'personal_info_1',
	'items'             => array ( 'phone_no', 'age3' ),
	'min_item_selected' => '4',
) );

echo $vObj->show_all_errors();
exit;*/
$trainer_id                             = '';
$trainer_user_id                        = '';
$trainer_first_name                     = '';
$trainer_last_name                      = '';
$demo_allowed                           = '';
$about_fees_structure                   = '';
$allow_installments                     = '';
$profile_heading                        = '';
$other_qualification                    = '';
$past_experiences                       = '';
$other_domains_technical                = '';
$other_domains_nontechnical             = '';
$trainer_domain_list_trainer_xp_id      = '';
$trainer_domain_list_trainer_experience = '';
$trainer_domain_list_domain             = '';
$trainer_domain_list_domain_type        = '';
$user_email                             = '';
$alternate_contact_no                   = '';
$primary_contact_no                     = '';
$trainer_nickname                       = '';
$trainer_present_place_of_work          = '';
$trainer_birth_year                     = '';
$trainer_birth_month                    = '';
$trainer_birth_day                      = '';
$trainer_city_id                        = '';
$str_city_options                       = '';
$correspondance_address                 = '';
$total_xp_id                            = '';
$trainer_frequency                      = '';
$training_level                         = '';
$trainer_keywords                       = '';
$company_logo_options                   = '';
$training_reference_items               = '';
$user_video_options                     = '';
$sample_course_material_path            = '';
$cv_path                                = '';
$companies_logos_array                  = array ();
$trainer_references_contact_no1         = '';
$trainer_references_contact_name1       = '';
$trainer_references_contact_no2         = '';
$trainer_references_contact_name2       = '';
$trainer_references_contact_no3         = '';
$trainer_references_contact_name3       = '';
$trainer_references_contact_no4         = '';
$trainer_references_contact_name4       = '';
$trainer_references_contact_no5         = '';
$trainer_references_contact_name5       = '';
$trainer_references_contact_no6         = '';
$trainer_references_contact_name6       = '';
if ( isset( $companies_logos ) && ! empty( $companies_logos ) ) {
	foreach ( $companies_logos as $company_data ) {
		if ( $company_data->company_name === null || trim( $company_data->company_name ) === '' ) {
			continue;
		}
		$companyName                                  = trim( $company_data->company_name );
		$companies_logos_array[ md5( $companyName ) ] = '<option>' . $companyName . '</option>';
	}
}
if ( isset( $view_trainer_data ) && is_array( $view_trainer_data ) ) {
	foreach ( $view_trainer_data as $view_trainer_info ) {
		$trainer_first_name                     = $view_trainer_info->first_name;
		$trainer_last_name                      = $view_trainer_info->last_name;
		$trainer_id                             = $view_trainer_info->trainer_id;
		$user_email                             = $view_trainer_info->user_email;
		$profile_heading                        = $view_trainer_info->trainer_heading;
		$past_experiences                       = $view_trainer_info->trainer_xp_comments;
		$trainer_user_id                        = $view_trainer_info->user_id;
		$total_xp_id                            = $view_trainer_info->total_xp_id;
		$correspondance_address                 = $view_trainer_info->current_address;
		$primary_contact_no                     = $view_trainer_info->mobile_no;
		$alternate_contact_no                   = $view_trainer_info->alternative_contact_no;
		$trainer_frequency                      = $view_trainer_info->trainer_frequency;
		$trainer_keywords                       = $view_trainer_info->trainer_keywords;
		$training_level                         = $view_trainer_info->training_level;
		$date_of_birth                          = $view_trainer_info->date_of_birth;
		$other_qualification                    = $view_trainer_info->other_qualification;
		$other_domains_technical                = $view_trainer_info->other_domains_technical;
		$other_domains_nontechnical             = $view_trainer_info->other_domains_nontechnical;
		$trainer_domain_list_trainer_xp_id      = $view_trainer_info->trainer_domain_list_trainer_xp_id;
		$trainer_domain_list_trainer_experience = $view_trainer_info->trainer_domain_list_trainer_experience;
		$trainer_domain_list_domain             = $view_trainer_info->trainer_domain_list_domain;
		$trainer_domain_list_domain_type        = $view_trainer_info->trainer_domain_list_domain_type;
		$sample_course_material_path            = $view_trainer_info->sample_course_material_path;
		$cv_path                                = $view_trainer_info->cv_path;
		$trainer_birth_year                     = substr( $date_of_birth, 0, 4 );
		$trainer_birth_month                    = substr( $date_of_birth, 5, 2 );
		$trainer_birth_day                      = substr( $date_of_birth, 8, 2 );

		if ( isset( $companies_logos_array ) && ! empty( $companies_logos ) ) {
			$trainer_company_served_company_name = trim( $view_trainer_info->trainer_company_served_company_name );
			if ( isset( $companies_logos_array[ md5( $trainer_company_served_company_name ) ] ) ) {
				$companies_logos_array[ md5( $trainer_company_served_company_name ) ] = '<option selected="selected">' . $trainer_company_served_company_name . '</option>';
			}
		}
		if ( $view_trainer_info->gender == "male" ) {
			$gender = '<option value="male">Male</option><option value="female">Female</option><option value="other">Other</option>';
		} else if ( $view_trainer_info->gender == "female" ) {
			$gender = '<option value="female">Female</option><option value="male">Male</option><option value="other">Other</option>';
		} else if ( $view_trainer_info->gender == "other" ) {
			$gender = '<option value="other">Other</option><option value="male">Male</option><option value="female">Female</option>';
		} else {
			$gender = '<option value="" disabled selected>--Select Gender--</option><option value="male">Male</option><option value="female">Female</option><option value="other">Other</option>';
		}
	}
} else {
	$gender = '<option value="" disabled selected>--Select Gender--</option><option value="male">Male</option><option value="female">Female</option><option value="other">Other</option>';
}

$trainer_references_contact_array = array ();
if ( isset( $view_pdg_trainer_references ) && is_array( $view_pdg_trainer_references ) && ! empty( $view_pdg_trainer_references ) ) {

	foreach ( $view_pdg_trainer_references as $view_pdg_trainer_reference_item ) {
		if ( $view_pdg_trainer_reference_item ) {
			$trainer_references_contact_array[ $view_pdg_trainer_reference_item->reference_type_id ] = array (
				'contact_no'   => $view_pdg_trainer_reference_item->contact_no,
				'contact_name' => $view_pdg_trainer_reference_item->contact_name
			);
		}
	}
}

$str_contact_mode_options            = '';
$str_grad_qualification_options      = '';
$str_experience_options              = '';
$str_post_grad_qualification_options = '';
$str_professional_courses            = '';

$trainer_qualification_modes_array = array ();
if ( isset( $trainer_qualification_modes ) && is_array( $trainer_qualification_modes ) && ! empty( $trainer_qualification_modes ) ) {
	foreach ( $trainer_qualification_modes as $qualification_mode ) {
		$qualification_mode_id = $qualification_mode->qualification_id;
		if ( ! in_array( $qualification_mode_id, $trainer_qualification_modes_array ) ) {
			$trainer_qualification_modes_array[] = $qualification_mode_id;
		}
	}
}
if ( isset( $user_video_result ) && is_array( $user_video_result ) && ! empty( $user_video_result ) ) {
	foreach ( $user_video_result as $user_video_value ) {
		$user_video_options .= '<option selected="selected">' . $user_video_value->video_url . '</option>';
	}
}

if ( isset( $qualification_data ) && is_array( $qualification_data ) && ! empty( $qualification_data ) ) {
	foreach ( $qualification_data as $qualification ) {
		$qualification_name = $qualification->qualification;
		$qualification_id   = $qualification->qualification_id;
		$qualification_type = $qualification->qualification_type;
		$str_selected       = '';
		switch ( $qualification_type ) {
			case 'bachelor':
				if ( in_array( $qualification_id, $trainer_qualification_modes_array ) ) {
					$str_selected = 'selected';
				}
				$str_grad_qualification_options .= '<option value="' . $qualification_id . '" ' . $str_selected . '>' . $qualification_name . '</option>';
				break;
			case 'postgrad':
				if ( in_array( $qualification_id, $trainer_qualification_modes_array ) ) {
					$str_selected = 'selected';
				}
				$str_post_grad_qualification_options .= '<option value="' . $qualification_id . '" ' . $str_selected . '>' . $qualification_name . '</option>';
				break;
			case 'professional_course':
				if ( in_array( $qualification_id, $trainer_qualification_modes_array ) ) {
					$str_selected = 'selected';
				}
				$str_professional_courses .= '<option value="' . $qualification_id . '" ' . $str_selected . '>' . $qualification_name . '</option>';
				break;
		}

	}
}

//trainer domain list
$trainer_domain_nontechnical_selected_array = array ();
$trainer_domain_technical_selected_array    = array ();

if ( isset( $trainer_domain_list ) && is_array( $trainer_domain_list ) && ! empty( $trainer_domain_list ) ) {
	foreach ( $trainer_domain_list as $trainer_domain_list_item ) {
		$trainer_domain_type = $trainer_domain_list_item->domain_type;
		$trainer_domain_id   = $trainer_domain_list_item->domain_id;
		$str_selected        = '';
		switch ( $trainer_domain_type ) {
			case  'technical':
				$trainer_domain_technical_selected_array[] = $trainer_domain_id;
				break;
			case 'nontechnical':
				$trainer_domain_nontechnical_selected_array[] = $trainer_domain_id;
				break;
		}
	}
}

//domain training
$str_domain_technical_options    = '';
$str_domain_nontechnical_options = '';
if ( isset( $domain_list ) && is_array( $domain_list ) && ! empty( $domain_list ) ) {
	foreach ( $domain_list as $domains ) {
		$domain_type  = $domains->domain_type;
		$domain_id    = $domains->domain_id;
		$domain       = $domains->domain;
		$str_selected = '';
		switch ( $domain_type ) {
			case  'technical':
				if ( in_array( $domain_id, $trainer_domain_technical_selected_array ) ) {
					$str_selected = 'selected';
				}
				$str_domain_technical_options .= '<option value="' . $domain_id . '" ' . $str_selected . '>' . $domain . '</option>';
				break;
			case 'nontechnical':
				if ( in_array( $domain_id, $trainer_domain_nontechnical_selected_array ) ) {
					$str_selected = 'selected';
				}
				$str_domain_nontechnical_options .= '<option value="' . $domain_id . '" ' . $str_selected . '>' . $domain . '</option>';
				break;
		}
	}
}
//experience
if ( isset( $training_experience ) && is_array( $training_experience ) && ! empty( $training_experience ) ) {
	foreach ( $training_experience as $experience ) {
		$experience_id   = $experience->trainer_xp_id;
		$experience_name = $experience->trainer_experience;
		$str_selected    = '';

		if ( ! empty( $total_xp_id ) && $experience_id == $total_xp_id ) {
			$str_selected = 'selected';
		}
		$str_experience_options .= '<option value="' . $experience_id . '" ' . $str_selected . '>' . $experience_name . '</option>';
	}
}


?>

<div class="row">
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Trainer Details</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<label for="txt_trainer_first_name">First Name*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" value="<?php echo $trainer_first_name; ?>" id="txt_trainer_first_name"
						       class="col-md-12 form-control validate[required, minSize[2], maxSize[100]]"
						       maxlength="100"/>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<label for="txt_trainer_last_name">Last Name*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" value="<?php echo $trainer_last_name; ?>" id="txt_trainer_last_name"
						       class="col-md-12 form-control validate[required, minSize[1], maxSize[100]]"
						       maxlength="100"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_email_id">Email ID*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_email_id" value="<?php echo $user_email; ?>"
						       class="col-md-12 form-control validate[required,custom[email],maxSize[200]]"
						       maxlength="200"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_mobile_no">Mobile No*</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_mobile_no" value="<?php echo $primary_contact_no; ?>"
						       class="col-md-12 form-control positive validate[required,custom[phone],minSize[10],maxSize[10]]"
						       maxlength="10"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_alternate_contact_no">Alternate Contact No.</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_alternate_contact_no" value="<?php echo $alternate_contact_no; ?>"
						       class="col-md-12 form-control validate[custom[phone],minSize[10],maxSize[15]]"
						       maxlength="15"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_address_of_correspondance">Address of Correspondance*</label>
					</div>
					<div class="col-md-8 form-group">
						<textarea id="txt_address_of_correspondance" rows="4" cols="50"
						          class="col-md-12 form-control validate[required,minSize[2],maxSize[1000]]"/><?php echo $correspondance_address; ?></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_gender">Gender*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_gender" class="form-control validate[required]">
							<?php echo $gender; ?>
						</select>
					</div>
				</div>


				<div class="row">
					<div class="col-md-4">
						<label for="date_of_birth">Date of Birth (YYYY/MM/DD)*</label>
					</div>
					<div class="col-md-8 form-group">
						<div class="row">
							<div class="col-md-3 col-xs-4">
								<input type="text" id="dob_year"
								       class="form-control positive validate[required, onlyNumber, minSize[4], maxSize[4], min[<?= date( "Y" ) - 100; ?>], max[<?= date( "Y" ) - 10; ?>]]"
								       placeholder="YYYY"
								       value="<?php echo $trainer_birth_year; ?>" maxlength="4"/>
							</div>
							<div class="col-md-1 col-xs-1" style="padding-top: 10px;">
								/
							</div>
							<div class="col-md-3 col-xs-3">
								<input type="text" id="dob_month"
								       class="form-control positive validate[required, onlyNumber, minSize[2], maxSize[2], min[1], max[12]]"
								       placeholder="MM"
								       value="<?php echo $trainer_birth_month; ?>" maxlength="2"/>
							</div>
							<div class="col-md-1 col-xs-1" style="padding-top: 10px;">
								/
							</div>
							<div class="col-md-3 col-xs-3">
								<input type="text" id="dob_day"
								       class="form-control positive validate[required, onlyNumber, minSize[2], maxSize[2]], min[1], max[31]]"
								       placeholder="DD"
								       value="<?php echo $trainer_birth_day; ?>" maxlength="2"/>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<strong>Academic Qualification</strong>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-4">
						<label for="select_graduation">Graduation*&nbsp;&nbsp;
							<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up"
							   title="( If you have more than one, mention the most relevant)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_graduation" class="form-control select_control_with_title validate[required]"
						        title="--Select Graduation--" data-allow-clear="true">
							<option></option>
							<?php echo $str_grad_qualification_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_post graduation">Post Graduation</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_postgraduation" class="form-control select_control_with_title"
						        title="--Select Post Graduation--" data-allow-clear="true">
							<option></option>
							<?php echo $str_post_grad_qualification_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_professional_qualification">Professional Qualification&nbsp;&nbsp;<i
								class="fa fa-question-circle" data-toggle="tooltip" data-placement="up"
								title="( Any certificate or diploma courses that add to your credentials as a trainer)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_professional_qualification"
						        class="form-control select2-hidden-accessible" multiple="multiple"
						        style="width: 75%" title="--Select Professional Qualification--"
						        data-placeholder="--Select Professional Qualification--"
						        data-allow-clear="true">
							<?php echo $str_professional_courses; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_other_qualification">Other Qualification&nbsp;&nbsp;<i
								class="fa fa-question-circle" data-toggle="tooltip" data-placement="up"
								title="(Something out of the regular or super specialized that backs up your training capability)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="txt_other_qualification" value="<?php echo $other_qualification; ?>"
						       class="col-md-12 form-control validate[minSize[2], maxSize[200]]" maxlength="200"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_companies_served">Companies Served*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_companies_served"
						        class="form-control select2-hidden-accessible validate[required]"
						        multiple="multiple"
						        tabindex="-1" aria-hidden="true">
							<?php echo implode( " ", $companies_logos_array ); ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_heading_of_profile">Heading of Profile*<br/>(max 140 characters)&nbsp;&nbsp;
						</label>
					</div>
					<div class="col-md-12 form-group character_count_heading_profile">
						<textarea id="txt_heading_of_profile"
						          class="col-md-12 form-control validate[required, maxSize[140]] character_count"
						          MaxLength="140"/><?php echo $profile_heading; ?></textarea>
						<div id="textarea_feedback_heading_profile" class="div_character_count"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_user_video">Video URL(s)<br/>(Press enter to add new url) &nbsp;&nbsp;
						</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_user_video"
						        class="form-control select2-hidden-accessible"
						        multiple="multiple"
						        tabindex="-1" aria-hidden="true">
							<?= $user_video_options; ?>
						</select>
					</div>
				</div>
				<br/>
				<div class="row blo">
					<div class="col-xs-12 col-md-4">
						<label for="file_upload_profile_picture">Upload Profile Picture*
						</label>
					</div>
					<div class="col-xs-12 col-md-12 form-group">
						<div class="col-xs-12 col-md-8 form-group">
							<?php
							$profile_pic_exist = file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/images/' . $trainer_user_id . '/profile.jpg' ) ? true : false;
							?>
							<input type="file" id="file_upload_profile_picture"
							       class="validate[<?= $profile_pic_exist ? '' : 'required,' ?>checkFileType[jpg|jpeg|gif|JPG|JPEG|png|PNG]]">
							<p class="help-block">(jpg, jpeg, png and bmp allowed)</p>
						</div>
						<div class="col-xs-12 col-md-4 trainer_profile_pic_block">
							<?= $profile_pic_exist ? "<img style='width:140px;height:auto;' src='" . PEDAGOGE_PLUGIN_URL . "/storage/uploads/images/" . $trainer_user_id . "/profile.jpg'>" : '' ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<label for="file_upload_resume*">Upload Resume*
						</label>
					</div>
					<div class="col-xs-12 col-md-12 form-group">
						<div class="col-xs-12 col-md-8 form-group">
							<?php
							$resume_exist = ! empty( $cv_path ) && file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/resume/' . $cv_path ) ? true : false;
							?>
							<input type="file" id="file_upload_resume"
							       class="validate[<?= $resume_exist ? '' : 'required,' ?>checkFileType[doc|docx|pdf|DOC|DOCX|PDF]]">
							<p class="help-block">(doc, docx and pdf allowed)</p>
						</div>
						<div class="col-xs-12 col-md-4 trainer_resume_block">
							<?= $resume_exist ? "<a href='" . PEDAGOGE_PLUGIN_URL . "/storage/uploads/resume/" . $cv_path . "'>Your Resume</a>" : '' ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<label for="file_upload_sample_course_material">Upload Sample Course Material*
						</label>
					</div>
					<div class="col-xs-12 col-md-12 form-group">
						<div class="col-xs-12 col-md-8 form-group">
							<?php
							$sample_course_material_exists = ! empty( $sample_course_material_path ) && file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/sample_course_material/' . $sample_course_material_path ) ? true : false;
							?>
							<input type="file" id="file_upload_sample_course_material"
							       class="validate[<?= $sample_course_material_exists ? '' : 'required,' ?>checkFileType[ppt|pptx|doc|docx|pdf|DOC|DOCX|PDF|PPT|PPTX]]">
							<p class="help-block">(ppt, pptx, pdf, doc and docx allowed)</p>
						</div>
						<div class="col-xs-12 col-md-4 trainer_sample_course_material_block">
							<?= $sample_course_material_exists ? "<a href='" . PEDAGOGE_PLUGIN_URL . "/storage/uploads/sample_course_material/" . $sample_course_material_path . "'>Sample Course Material</a>" : '' ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--Training Details-->


	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">Training Details*</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<label for="select_technical_domain">Training domain - Technical</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_technical_domain"
						        class="form-control select_control_with_title_multiple"
						        multiple="multiple"
						        title="--Select domains of training--">
							<option></option>
							<?php echo $str_domain_technical_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_other_technical_domain">Others, specify<br/>Seperated by comma(s)</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_other_technical_domain"
						        class="form-control select2-hidden-accessible"
						        multiple="multiple"
						        tabindex="-1" aria-hidden="true">
							<?php
							if ( isset( $other_domains_technical ) || ! empty( $other_domains_technical ) ) {
								foreach ( explode( ",", $other_domains_technical ) as $domains_technical ) {
									echo '<option selected="selected">' . $domains_technical . '</option>';
								}
							}
							?>
						</select>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-4">
						<label for="select_nontechnical_domain">Training domain - Non Technical</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_nontechnical_domain"
						        class="form-control select_control_with_title_multiple"
						        multiple="multiple"
						        title="--Select domains of training--">
							<option></option>
							<?php echo $str_domain_nontechnical_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_other_nontechnical_domain">Others, specify<br/>Seperated by comma(s)</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_other_nontechnical_domain"
						        class="form-control select2-hidden-accessible"
						        multiple="multiple"
						        tabindex="-1" aria-hidden="true">
							<?php
							if ( isset( $other_domains_nontechnical ) || ! empty( $other_domains_nontechnical ) ) {
								foreach ( explode( ",", $other_domains_nontechnical ) as $domains_nontechnical ) {
									echo '<option selected="selected">' . $domains_nontechnical . '</option>';
								}
							}
							?>
						</select>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-4">
						<label for="select_average_training_experience">Training Experience<br/>(in
							years)*&nbsp;&nbsp;<i class="fa fa-question-circle" data-toggle="tooltip"
							                      data-placement="up"
							                      title="(The average experience of your entire training combined.)"></i></label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_average_training_experience"
						        class="select_control_with_title form-control validate[required]"
						        title="--Select Average Training Experience--">
							<option></option>
							<?php echo $str_experience_options; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_training_frequency">Training frequency?*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_training_frequency"
						        class="select_control_with_title form-control validate[required]"
						        title="--Select Training Frequency--">
							<option></option>
							<option value="fulltime" <?= $trainer_frequency == 'fulltime' ? 'selected' : '' ?>>
								Full Time
							</option>
							<option value="parttime" <?= $trainer_frequency == 'parttime' ? 'selected' : '' ?>>Part
								Time
							</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label for="select_training_level">Hierarchy of professionals trained*</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_training_level"
						        class="select_control_with_title form-control validate[required]"
						        title="--Select the hierarchical level--">
							<option></option>
							<option value="beginner" <?= $training_level == 'beginner' ? 'selected' : '' ?>>
								Beginner
							</option>
							<option value="intermediate" <?= $training_level == 'intermediate' ? 'selected' : '' ?>>
								Intermediate
							</option>
							<option value="advanced" <?= $training_level == 'advanced' ? 'selected' : '' ?>>Advanced
							</option>
						</select>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-4">
						<label for="select_keywords">Keywords to describe your training*<br/>Seperated by
							comma(s)</label>
					</div>
					<div class="col-md-8 form-group">
						<select id="select_keywords"
						        class="form-control select2-hidden-accessible validate[required]"
						        multiple="multiple"
						        tabindex="-1" aria-hidden="true">
							<?php
							if ( isset( $trainer_keywords ) || ! empty( $trainer_keywords ) ) {
								foreach ( explode( ",", $trainer_keywords ) as $keywords ) {
									echo '<option selected="selected">' . $keywords . '</option>';
								}
							}
							?>
						</select>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-4">
						<label for="txt_training_experiences_of_profile">Past training experiences*<br/>(max 500
							characters)&nbsp;&nbsp;
							<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="up"
							   title="Write about some of your past training experiences, like organisations, training topics and how those organisations have benefited - Maximum: 500 characters"></i></label>
					</div>
					<div class="col-md-12 form-group character_count_heading_profile">
						<textarea id="txt_training_experiences_of_profile"
						          class="col-md-12 form-control validate[required, maxSize[500]] character_count"
						          MaxLength="500"/><?php echo $past_experiences; ?></textarea>
						<div id="textarea_feedback_training_experiences_profile" class="div_character_count"></div>
					</div>
				</div>
				<!--References-->
				<div class="row">
					<div class="col-md-4">
						<label for="trainer_references_ClientReference_1_contact_no">Client Reference - 1</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_ClientReference_1_contact_no"
						       class="form-control validate[minSize[10],maxSize[15],custom[phone]]"
						       placeholder="Contact Number"
						       value="<?= isset( $trainer_references_contact_array[1] ) ? $trainer_references_contact_array[1]['contact_no'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_ClientReference_1_contact_name">Client Reference - 1</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_ClientReference_1_contact_name"
						       class="form-control validate[minSize[2],maxSize[25]]]" placeholder="Contact Name"
						       value="<?= isset( $trainer_references_contact_array[1] ) ? $trainer_references_contact_array[1]['contact_name'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_ClientReference_2_contact_no">Client Reference - 2</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_ClientReference_2_contact_no"
						       class="form-control validate[minSize[10],maxSize[15],custom[phone]]"
						       placeholder="Contact Number"
						       value="<?= isset( $trainer_references_contact_array[2] ) ? $trainer_references_contact_array[2]['contact_no'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_ClientReference_2_contact_name">Client Reference - 2</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_ClientReference_2_contact_name"
						       class="form-control validate[minSize[2],maxSize[25]]]" placeholder="Contact Name"
						       value="<?= isset( $trainer_references_contact_array[2] ) ? $trainer_references_contact_array[2]['contact_name'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PeerColleagueReference_1_contact_no">Peer/Colleague Reference -
							1</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PeerColleagueReference_1_contact_no"
						       class="form-control validate[minSize[10],maxSize[15],custom[phone]]"
						       placeholder="Contact Number"
						       value="<?= isset( $trainer_references_contact_array[3] ) ? $trainer_references_contact_array[3]['contact_no'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PeerColleagueReference_1_contact_name">Peer/Colleague Reference -
							1</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PeerColleagueReference_1_contact_name"
						       class="form-control validate[minSize[2],maxSize[25]]]" placeholder="Contact Name"
						       value="<?= isset( $trainer_references_contact_array[3] ) ? $trainer_references_contact_array[3]['contact_name'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PeerColleagueReference_2_contact_no">Peer/Colleague Reference -
							2</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PeerColleagueReference_2_contact_no"
						       class="form-control validate[minSize[10],maxSize[15],custom[phone]]"
						       placeholder="Contact Number"
						       value="<?= isset( $trainer_references_contact_array[4] ) ? $trainer_references_contact_array[4]['contact_no'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PeerColleagueReference_2_contact_name">Peer/Colleague Reference -
							2</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PeerColleagueReference_2_contact_name"
						       class="form-control validate[minSize[2],maxSize[25]]]" placeholder="Contact Name"
						       value="<?= isset( $trainer_references_contact_array[4] ) ? $trainer_references_contact_array[4]['contact_name'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PersonalReference_1_contact_no">Personal Reference - 1</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PersonalReference_1_contact_no"
						       class="form-control validate[minSize[10],maxSize[15],custom[phone]]"
						       placeholder="Contact Number"
						       value="<?= isset( $trainer_references_contact_array[5] ) ? $trainer_references_contact_array[5]['contact_no'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PersonalReference_1_contact_name">Personal Reference - 1</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PersonalReference_1_contact_name"
						       class="form-control validate[minSize[2],maxSize[25]]]" placeholder="Contact Name"
						       value="<?= isset( $trainer_references_contact_array[5] ) ? $trainer_references_contact_array[5]['contact_name'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PersonalReference_2_contact_no"> Personal Reference - 2</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PersonalReference_2_contact_no"
						       class="form-control validate[minSize[10],maxSize[15],custom[phone]]"
						       placeholder="Contact Number"
						       value="<?= isset( $trainer_references_contact_array[6] ) ? $trainer_references_contact_array[6]['contact_no'] : ''; ?>"/>
					</div>
					<div class="col-md-4">
						<label for="trainer_references_PersonalReference_2_contact_name"> Personal Reference - 2</label>
					</div>
					<div class="col-md-8 form-group">
						<input type="text" id="trainer_references_PersonalReference_2_contact_name"
						       class="form-control validate[minSize[2],maxSize[25]]]" placeholder="Contact Name"
						       value="<?= isset( $trainer_references_contact_array[6] ) ? $trainer_references_contact_array[6]['contact_name'] : ''; ?>"/>
					</div>
				</div>
				<!--end references-->

			</div>
		</div>
	</div>
</div>