<main class="padding-fix center-block register-block">
	<!--section-info-->
	<section class="section-box white-text">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="logo-wrapper hidden-xs">
						<img class="logo" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='logo-box'">
					</div>
					<h1 class="main-heading text-bold">Reset Password</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="row margin-top-10">
						<div class="col-xs-12 padding-fix" id="div_pass_reset_result"></div>
					</div>
					<form id="frm_change_password" class="frm_change_password" name="frm_change_password">
						<div id="div_reset_email">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<input type="password" class="form-control class_password text-input validate[required]" name="txt_password" id="txt_password" placeholder="New Password">
										<input type="password" class="form-control class_password text-input validate[required, equals[txt_password]]" name="txt_retype_password" id="txt_retype_password" placeholder="Retype Password">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group nomargin text-left">
							<div class="checkbox">
								<label for="chk_register_show_password">
									<input type="checkbox" id="chk_register_show_password" value="no"> Show
									Password </label>
							</div>
						</div>
						<div class="form-group nomargin text-left">
							<input type="hidden" value="<?php echo $template_vars['pdg_user_id']; ?>" id="hidden_user_id"/>
							<input type="hidden" value="<?php echo wp_hash_password( $template_vars['pdg_user_email'] ); ?>" id="hidden_user_email_encrypted"/>
							<button type="button" class="btn white-text text-bold btn-lg btn-block cmd_change_password light-green-btn-shade" id="cmd_change_password" data-loading-text="Changing ...">
								Change Password
							</button>
						</div>
						<div class="row margin-top-10 margin-bottom-10">
							<div class="col-xs-12">
								<a href="<?php echo home_url( '/signup' ) ?>" class="cmd_login_forgot_password">Signup</a> |
								<a href="<?php echo home_url( '/login' ) ?>" class="cmd_login_forgot_password">Login</a>
							</div>
						</div>
						<div class="row">
							<span class="text-sm"><?php echo date( 'Y' ) ?>&nbsp;&copy; Pedagoge.</span>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!--/section-info-->
</main>