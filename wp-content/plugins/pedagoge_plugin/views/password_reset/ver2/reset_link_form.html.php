<main class="padding-fix center-block register-block">
	<!--section-info-->
	<section class="section-box white-text">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="logo-wrapper hidden-xs">
						<img class="logo" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='logo-box'">
					</div>
					<h1 class="main-heading text-bold">Password Reset</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 center-block text-center">
					<div class="row margin-top-10">
						<?php if ( isset( $template_vars['invalid_key_contemt'] ) ) {
							echo $template_vars['invalid_key_contemt'];
						} ?>
						<div class="col-xs-12 padding-fix" id="div_pass_reset_result"></div>
					</div>
					<div class="div_login_signup" id="div_login_signup">
						<div id="div_reset_email">
							<div class="row">
								<div class="col-xs-12">
									<form id="frm_send_password_reset_link" class="frm_send_password_reset_link nomargin-top-20" name="frm_send_password_reset_link">
										<div class="form-group">
											<input type="text" class="form-control validate[required, custom[email]]" id="txt_pass_reset_email" placeholder="Email address" value="">
										</div>
										<button type="button" class="btn white-text text-bold btn-lg btn-block cmd_password_reset dark-green-btn-shade" id="cmd_password_reset" data-loading-text="Sending ...">
											Reset
										</button>
										<div class="col-xs-12">
											<p>
												<a href="#" id="cmd_show_user_form" class="white-text">Don't remember
													email address?</a>
											</p>
										</div>
									</form>
								</div>
							</div>
						</div>


						<div id="div_reset_username" class="hide">
							<div class="row">
								<div class="col-xs-12">
									<form id="frm_username_send_password_reset_link" class="frm_username_send_password_reset_link" name="frm_username_send_password_reset_link">
										<div class="form-group">
											<input type="text" class="form-control validate[required] nomargin-top-20" id="txt_pass_reset_username" placeholder="User name" value="">
										</div>

										<button type="button" class="btn white-text text-bold btn-lg btn-block cmd_username_password_reset dark-green-btn-shade" id="cmd_username_password_reset" data-loading-text="Sending ...">
											Reset
										</button>
										<div class="col-xs-12">
											<p>
												<a href="#" id="cmd_show_email_form" class="white-text">Don't remember
													user name?</a>
											</p>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row margin-top-10 margin-bottom-10">
						<div class="col-xs-12">
							<a href="<?php echo home_url( '/signup' ) ?>" class="cmd_login_forgot_password">Signup</a> |
							<a href="<?php echo home_url( '/login' ) ?>" class="cmd_login_forgot_password">Login</a>
						</div>
					</div>
					<div class="row">
						<span class="text-sm"><?php echo date( 'Y' ) ?>&nbsp;&copy; Pedagoge.</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/section-info-->
</main>