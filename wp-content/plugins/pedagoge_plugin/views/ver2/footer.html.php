<?php
/**
 * The template for displaying the footer - version 2 theme..
 */

?>
			<footer class="col-xs-12 site-footer margin-top-20 navbar-static-bottom hide-footer">
				<div class="container-fluid">
					<div class="row logos">
						<div class="col-xs-12 col-sm-4 pedagoge_logo"><a class="navbar-brand" href="<?= home_url() ?>"><img
									src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
									onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='valign responsive-png'"></a>
						</div>
						<div class="col-xs-12 col-sm-4 pedagoge_sm"></div>
						<div class="col-xs-12 col-sm-4 nasscom_logo">
							<div class="row">
								<div class="col-xs-12">
									<?php get_template_part( 'template-parts/ver2/social_media_home' ); ?>
								</div>
							</div>
						</div>
					</div>
	
					<div class="row margin-top-20 text-center center-block">
						<div class="col-md-2"><a href="<?= home_url( 'terms' ) ?>" class="white-text notext-decoration">Terms and
								Conditions</a></div>
						<div class="col-md-2"><a href="<?= home_url( 'privacy-policy' ) ?>" class="white-text notext-decoration">Privacy
								Policy</a></div>
						<div class="col-md-2"><a href="<?= home_url( 'site-map' ) ?>" class="white-text notext-decoration">Site
								Map</a></div>
						<div class="col-md-2"><a href="<?= home_url( 'careers' ) ?>"
												class="white-text notext-decoration">Careers</a></div>
						<div class="col-md-2"><a href="<?= home_url( 'business' ) ?>"
												class="white-text notext-decoration">Business</a></div>
						<div class="col-md-2"><a href="<?= home_url( 'forteachers' ) ?>" class="white-text notext-decoration">Register as
								Teacher</a></div>
					</div>
					<div class="row text-center center-block margin-top-10">
	
					</div>
					<div class="row text-center center-block margin-top-10">
						&copy; Pedagoge <?= date( 'Y' ) ?>
					</div>
				</div>
			</footer>
		</div>

		<!-- Custom Javascript Variables -->
		<script type="text/javascript">
			var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>',
				$pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>',
				$pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler',
				$pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler',
				$ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
		</script>

		<!-- Load Scripts below -->
		<?php
		wp_footer();

		if ( isset( $this->app_data['dynamic_js'] ) ) {
			echo $this->app_data['dynamic_js'];
		}

		echo $this->load_view( 'footer_scripts' );
		wp_footer();
		?>
	</body>
	<?php
		global $is_production_environment;
		if($is_production_environment) {
			get_template_part( 'template-parts/google', 'tag' );	
		}
	?>
</html>