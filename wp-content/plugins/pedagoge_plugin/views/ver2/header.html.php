<?php
/**
 * The header for pedagoge version 2 theme.
 *
 */
/*
@description: This will route the home page according to the device
*/

global $isVersion2, $post, $excludeVersion2HeaderMobile, $slug, $is_production_environment;
$isVersion2 = true;

$is_production_environment = false;
$server_name = $_SERVER['SERVER_NAME'];
switch ( $server_name ) {
	case 'www.pedagoge.com':
	//case 'www.pedagog.in':
	//case 'www.pedagoge.net':
	case 'dev.pedagoge.com':
	case 'test.pedagoge.com':
	$is_production_environment = true;
		break;
}

$slug = '';
if ( isset( $post ) ) {
	$current_post = get_post( $post );
	if ( ! empty( $current_post ) ) {
		$slug = $current_post->post_name;
	}
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php get_template_part( 'template-parts/google_conversion_10052017'); ?>
	<?php if ( $is_production_environment ): ?>		
		<!-- Google Tag Manager -->
		<script>(function ( w, d, s, l, i ) {
				w[l] = w[l] || [];
				w[l].push( {
					'gtm.start': new Date().getTime(), event: 'gtm.js'
				} );
				var f = d.getElementsByTagName( s )[0],
					j = d.createElement( s ), dl = l != 'dataLayer' ? '&l=' + l : '';
				j.async = true;
				j.src =
					'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
				f.parentNode.insertBefore( j, f );
			})( window, document, 'script', 'dataLayer', 'GTM-PPFH97' );</script>
		<!-- End Google Tag Manager -->
	<?php endif; ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta id="viewport" name="viewport"
	      content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta name="HandheldFriendly" content="true"/>


	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<title><?php echo $this->title; ?></title>
	<?php
	if ( isset( $this->app_data['seo_meta'] ) ) {
		echo '<meta name="description" content="' . $this->app_data['seo_meta'] . '">';
	}
	?>
	<meta name="author" content="pedagoge">
	<link rel="shortcut icon"
	      href="<?php echo PEDAGOGE_THEME_URL; ?>/assets/img/fav_icon_logo_orb_small.png">
	<?php
	remove_action( 'wp_head', '_wp_render_title_tag', 1 );
	wp_head();
	?>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>    <![endif]-->
	<!--  Android 5+ material Color-->
	<meta name="theme-color" content="#4d738a">
	<script type="text/javascript">
		var site_var = {};
		site_var.url = "<?=site_url();?>/";
	</script>
	<?php echo $this->load_view( 'header_scripts' ); ?>
	<?php
	$server_name = $_SERVER['SERVER_NAME'];
	switch ( $server_name ) {
		case 'www.pedagoge.com':
			get_template_part( 'template-parts/hot', 'jar' );
			get_template_part( 'template-parts/fb', 'pixel' );
			break;
	}
	?>
	<script>
		(function ( i, s, o, g, r, a, m ) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push( arguments )
				}, i[r].l = 1 * new Date();
			a = s.createElement( o ),
				m = s.getElementsByTagName( o )[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore( a, m )
		})( window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga' );

		ga( 'create', 'UA-70113984-1', 'auto' );
		ga( 'send', 'pageview' );

	</script>
</head>
<body <?php	body_class( "body-container" );	echo pedagoge_body_tags(); ?>	>
<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPFH97"
	        height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="body-wrapper">
<?php
	$excludeVersion2Header = array ( '' );
	$excludeVersion2HeaderMobile = array ( '' );
	if (! in_array( $slug, $excludeVersion2Header ) ) {
		get_template_part( 'template-parts/ver2/navbar' );
	}