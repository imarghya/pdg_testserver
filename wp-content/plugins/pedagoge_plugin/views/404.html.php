<div class="row pedagoge_login_form">
	<div class="col-md-6 center">                               
		<div class="login-box">
			<h2 class="center-block">Page not found!</h2>
			<p><strong>Oops! Looks like rats ate the page you're looking for!</strong></p>
			<img src="<?php echo PEDAGOGE_ASSETS_URL ?>/images/404.png" class="center-block" alt="" />
		</div>
    </div>
</div><!-- Row -->