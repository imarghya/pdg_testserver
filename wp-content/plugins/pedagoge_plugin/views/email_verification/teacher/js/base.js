
jQuery(document).ready(function(){
	"use strict";

	

	

	/* Mean Menu */
	jQuery(document).ready(function () {
		jQuery('.site-navigation').meanmenu();
	});

	

	
	

	/* Go Top */
	jQuery(".go-top").click(function() {
	  jQuery("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});

	
	/* Toggle */
	jQuery(".toggle-content .expand-button").click(function() {
		jQuery(this).toggleClass('close').parent('div').find('.expand').slideToggle(250);
	});

});


/* Smooth Scrolling */

$('a[href*="#"]:not([href="#"])').click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  }
});
/* Smooth Scrolling */

/* Drag off */
//document.addEventListener("contextmenu", function(e){
//    e.preventDefault();
//}, false);
/* Drag off */

/* For menu Active */
$(document).ready(function () {
    $('.menu li a').click(function(e) {

        $('.menu li a').removeClass('menu_active');

        var $this = $(this);
        if (!$this.hasClass('menu_active')) {
            $this.addClass('menu_active');
        }
        //e.preventDefault();
    });
});
/* For menu active */



/* Radio Click and Item Show */

//$(".perkdrop").change(function() {
//    $('.book_category').slideUp();
//    if ($(this).is(':checked')) {
//        $(this).parent().next().slideToggle("normal");
//    }
//});

/* Radio Click and Item Show */

/* Slim Scroll For Admin */

				$(function(){
					$('.public_review_box').slimScroll({
						height: '330px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						allowPageScroll: true,
						axis: 'y'
					});
				});
				
				$(function(){
					$('.private_review_box').slimScroll({
						height: '110px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						allowPageScroll: true,
						axis: 'y'
					});
				});
				

/* Slim Scroll For Admin */

/* For Profile Page */

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});


/* For Profile Page */

/* Show Hide Profile */

$("#hide").click(function(){
    $(".my_profile").hide();
	$(".edit_profile").show();
});

$("#show").click(function(){
    $(".my_profile").show();
	$(".edit_profile").hide();
});

/* Show Hide Profile */


/* Show Hide Chat */

$(".reply_show").click(function(){
	$(".chat_answer_profile").show();
});

/* Show Hide Chat */


<!-- Multyple Image Slider -->

var owl = $('.owl1');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: true,
		nav: true,
		navText : ["<img src='images/myprevimage.png'>","<img src='images/mynextimage.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  600: {
            items: 2
          },
		  730: {
            items: 3
          },
		  960: {
            items: 4
          },
		  1200: {
            items: 5
          }
        }
      })
	  
	  $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
})






var owl = $('.owl2');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: true,
		nav: true,
		navText : ["<img src='images/myprevimage1.png'>","<img src='images/mynextimage1.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  600: {
            items: 2
          },
		  730: {
            items: 3
          },
		  1024: {
            items: 4
          }
        }
      })
	  
	  $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
})

<!-- Multyple Image Slider -->

/* Alert Drop Down */

	function myFunction1() {
       var x = document.getElementById('myDIV1');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction2() {
       var x = document.getElementById('myDIV2');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction3() {
       var x = document.getElementById('myDIV3');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }

/* Alert Drop Down */

/* Drop down */

$('#all').click(function () {
    if ($(this).text().indexOf('Show') >= 0) {
        $('.panel-collapse').collapse('show');
        $(this).text('Hide All');
    } else {
        $('.panel-collapse').collapse('hide');
        $(this).text('Show All');
    }
});

/* Drop Down */

<!-- For Light Box -->
$(document).ready(function ($) {
                // delegate calls to data-toggle="lightbox"
                $(document).on('click', '[data-toggle="lightbox"]:not([data-gallery="navigateTo"]):not([data-gallery="example-gallery-11"])', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function() {
                            if (window.console) {
                                return console.log('Checking our the events huh?');
                            }
                        },
						onNavigate: function(direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                            }
						}
                    });
                });

                // disable wrapping
                $(document).on('click', '[data-toggle="lightbox"][data-gallery="example-gallery-11"]', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        wrapping: false
                    });
                });

                //Programmatically call
                $('#open-image').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });
                $('#open-youtube').click(function (e) {
                    e.preventDefault();
                    $(this).ekkoLightbox();
                });

				// navigateTo
                $(document).on('click', '[data-toggle="lightbox"][data-gallery="navigateTo"]', function(event) {
                    event.preventDefault();

                    return $(this).ekkoLightbox({
                        onShown: function() {

							this.modal().on('click', '.modal-footer a', function(e) {

								e.preventDefault();
								this.navigateTo(2);

                            }.bind(this));

                        }
                    });
                });

                //anchors.options.placement = 'left';
               // anchors.add('h3');
                $('code[data-code]').each(function() {

                    var $code = $(this),
                        $pair = $('div[data-code="'+$code.data('code')+'"]');

                    $code.hide();
                    var text = $code.text($pair.html()).html().trim().split("\n");
                    var indentLength = text[text.length - 1].match(/^\s+/)
                    indentLength = indentLength ? indentLength[0].length : 24;
                    var indent = '';
                    for(var i = 0; i < indentLength; i++)
                        indent += ' ';
                    if($code.data('trim') == 'all') {
                        for (var i = 0; i < text.length; i++)
                            text[i] = text[i].trim();
                    } else  {
                        for (var i = 0; i < text.length; i++)
                            text[i] = text[i].replace(indent, '    ').replace('    ', '');
                    }
                    text = text.join("\n");
                    $code.html(text).show();

                });
            });
			
			
			
  
<!-- For Light Box -->