<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/favicon.png">
<link rel="icon" type="image/png" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/favicon.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/favicon.png" sizes="16x16">

<title>Pedagoge Profile Page</title>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
		var site_var = {};
		site_var.url = "<?=site_url();?>/";
	</script>
<?php //echo $this->load_view( 'header_scripts' ); ?>
	<?php
	$server_name = $_SERVER['SERVER_NAME'];
	switch ( $server_name ) {
		case 'www.pedagoge.com':
			get_template_part( 'template-parts/hot', 'jar' );
			get_template_part( 'template-parts/fb', 'pixel' );
			break;
	}
	?>
<!--<link rel="stylesheet" type="text/css" href="<?php //echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/css/global-all.css?ver=0.50" />-->
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/menu.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/owl.carousel.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/owl.theme.default.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/ekko-lightbox.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/teacher_public_style.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>css/media.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/css/alertify.core.css" rel="stylesheet" type="text/css">
<link href="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/css/alertify.default.css" rel="stylesheet" type="text/css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo PEDAGOGE_THEME_URL; ?>/assets/css/validationEngine/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/js/ver2_pluginshome.min.js?ver=0.50"></script>
</head>

<body>

	<header class="header_wrapper">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 logo">
                	<a href="<?php echo get_site_url(); ?>"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/logo_black.png" alt="Logo"></a>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8 navigation_role">
                	<div class="main-menu-section">
                        <div class="container_12">
                            <nav class="site-navigation main-navigation" role="navigation">
                                <ul class="menu">
                                    <li><a href="career.html">Careers</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Contact Us</a>
                                        <!--<ul class="sub-menu">
                                                <li class="menu-item"><a title="Trainer single" href="take.html">Dine In Menu</a>
                                                	<ul class="sub-menu">
                                                <li class="menu-item"><a title="Trainer single" href="take.html">Dine In Menu</a></li>
                                                <li class="menu-item"><a title="Trainer single" href="dine.html">Take Away Menu</a></li>
                                            </ul>
                                                </li>
                                                <li class="menu-item"><a title="Trainer single" href="dine.html">Take Away Menu</a></li>
                                            </ul>-->
                                    </li>
                                    <?php if(is_user_logged_in()) { ?>
                                    <li><a href="<?php echo wp_logout_url(); ?>">Logout</a></li>
                                    <?php }else{ ?>
                                    <li><a href="<?php echo get_site_url(); ?>/login?nexturl=/">Login</a></li>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>