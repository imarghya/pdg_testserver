 <footer class="pedagoge_footer text-center">
         <div class="container">
            <div class="row">
               <div class="col-xs-12 pedagoge_footer_link">
                  <ul>
                     <li><a href="#">Terms & Conditions</a></li>
                     <li><a href="#">Privacy Policy</a></li>
                     <li><a href="#">Site map</a></li>
                     <li><a href="#">About Us</a></li>
                     <li><a href="#">Business</a></li>
                     <li><a href="#">Contact Us</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Powered by</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l1.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Selected for</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l2.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Selected for</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l3.png" alt="LOGO"></a>
               </div>
               <div class="col-xs-12 col-sm-3 col-md-3 text-center footer_pedagoge_logo">
                  <h5>Backed by</h5>
                  <a href="#"><img src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/l4.png" alt="LOGO"></a>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social">
                  <h3>Follow Us</h3>
                  <ul>
                     <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social pull-right">
                  <h3>Subscribe to our Newsletter</h3>
                  <div class="input-group">
                     <input type="email" class="form-control" placeholder="Enter your email address">
                     <span class="input-group-btn">
                     <button class="btn btn-secondary" type="button">Go</button>
                     </span>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-4 col-md-4 pedagoge_social">
                  <h3 class="hidden-xs">&nbsp;</h3>
                  <p>&copy; Pedagoge 2017</p>
               </div>
            </div>
         </div>
      </footer>


<div class="modal fade" id="email_verifi_modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
       <div class="modal-content">
         
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h5 class="modal-title" id="avatar-modal-label">Add Location</h5>
             </div>
              <form class="avatar-form" enctype="multipart/form-data" method="post">
             <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Location:</label>
                    <input type="text" class="form-control" id="st_add_loc">
                  </div>
             </div>
             </form>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="st_add_loc_btn" class="btn btn-primary">Add</button>
              </div>
          
       </div>
    </div>
</div>


<!--<script src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery-2.1.1.min.js"></script>-->
<script src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/js/global-all.js?"></script>
<script src="<?php echo PEDAGOGE_THEME_URL."/assets/js/validationEngine/jquery.validationEngine.js"; ?>"></script>
<script src="<?php echo PEDAGOGE_THEME_URL."/assets/js/validationEngine/jquery.validationEngine-en.js"; ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
<script src="<?php echo plugins_url() ?>/pedagoge_plugin/assets/cp_assets/js/alertify.min.js"></script>
<!--<script src="<?php //echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/bootstrap.min.js"></script>-->
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.meanmenu.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/owl.carousel.min.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/jquery.slimscroll.min.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/ekko-lightbox.js"></script>
<script src="<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>js/base.js"></script>
<!--<script src="<?php //echo PEDAGOGE_ASSETS_URL."/js/social_login.js"; ?>"></script>-->
<script src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL."/js/modals_callback.js"; ?>"></script>

<script src="<?php //echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL."/js/profile.js"; ?>"></script>

<script src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585b9d7903b68dc8"></script>
<script>
$(document).ready(function(){
$("#email_verifi").click(function(){
var email_id=$(this).attr("email-id");
//call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_teacher_email_verification',
        pedagoge_callback_class: 'ControllerTeacher',
        email_id : email_id,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            alertify.success("Success: Verification mail has been sent.");
        })
        .fail(function(jqxhr, status, error){ 
           alert(error);
        });
      //--------------------------------------------------------	
});
});	
	
    $(function(){
					$('.public_review_box').slimScroll({
						height: '330px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						allowPageScroll: true,
						axis: 'y'
					});
				});
				
				$(function(){
					$('.private_review_box').slimScroll({
						height: '110px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#3c6b89',
						opacity: 1,
						allowPageScroll: true,
						axis: 'y'
					});
				});
                                
                                
    var owl = $('.owl1');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: true,
		nav: true,
		navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/myprevimage.png'>","<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/mynextimage.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  600: {
            items: 2
          },
		  730: {
            items: 3
          },
		  960: {
            items: 4
          },
		  1200: {
            items: 5
          }
        }
      })
	  
	  $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
});

    var owl = $('.owl2');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: true,
		nav: true,
		navText : ["<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/myprevimage1.png'>","<img src='<?php echo PEDAGOGE_ASSETS_URL."/teacher/"; ?>img/mynextimage1.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  600: {
            items: 2
          },
		  730: {
            items: 3
          },
		  1024: {
            items: 4
          }
        }
      })
	  
   $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
});
$(document).ready(function(){
	$('#takeademo-callback-profile-modal').on('shown.bs.modal', function (e) {
	  var teacher_name=$("#te_name").text();
	  $("#callback_teachername-takeademo").val(teacher_name);
	});
	
	$('#request-callback-profile-modal').on('shown.bs.modal', function (e) {
	  var teacher_name=$("#te_name").text();
	  $("#callback_teachername").val(teacher_name);
	});
});

/* Alert Drop Down */

	function myFunction1() {
       var x = document.getElementById('myDIV1');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }
   
   function myFunction3() {
       var x = document.getElementById('myDIV3');
       if (x.style.display === 'none') {
           x.style.display = 'block';
       } else {
           x.style.display = 'none';
       }
   }
</script>
<!-- Custom Javascript Variables -->
<script type="text/javascript">
var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>',
	$pedagoge_callback_class = '<?php echo $this->pedagoge_callback_class; ?>',
	$pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler',
	$pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler',
	$ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
</script>
<?php
if ( isset( $this->app_data['dynamic_js'] ) ) {
echo $this->app_data['dynamic_js'];
}
?>
</body>
</html>
