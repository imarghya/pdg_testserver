<?php
$teacher_model = new ModelTeacher();
$teacher_id=$_GET['teacher'];
$status=$teacher_model->email_verification($teacher_id);
$link= get_site_url();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Pedagoge</title>

<style type="text/css">

/*-=-=-=-==-==-=-=-=-=-=-=-=-==-==-==-= Common Library -=-=-------------=-=-*/

.no_margin
{
	margin:0 !important;
}
.no_padding
{
	padding:0 !important;
}
.no_margin_left_right
{
	margin-left:0 !important;
	margin-right:0 !important;
}
.no_margin_top_bottom
{
	margin-top:0 !important;
	margin-bottom:0 !important;
}
.no_padding_left_right
{
	padding-left:0 !important;
	padding-right:0 !important;
}
.no_padding_top_bottom
{
	padding-top:0 !important;
	padding-bottom:0 !important;
}
*
{
	outline:none;
	padding:0;
	margin:0;
}
.html
{
	font-size:14px;
	-ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}
body
{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
a
{
	text-decoration:none;
	color:#555;
}
a:hover,
a:focus {
  color: #23527c;
}
a:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px;
}
p
{
	margin:0;
}
img
{
	border:0;
	vertical-align:middle;
	max-width:100%;
}
ul,ol,dl
{
	list-style:none;
}
header,footer,nav,aside,article,address,section
{
	display:block;
}
.wrapper
{
	width:60%;
	margin:0 auto;
	max-width:1366px
}
.wrapper_fluid
{
	width:100%;
	margin:0 auto;
	max-width:1366px;
}
.margin_top_10
{
	margin-top:10px;
}
.margin_top_20
{
	margin-top:20px;
}
.margin_top_30
{
	margin-top:30px;
}
.margin_top_40
{
	margin-top:40px;
}
.margin_top_50
{
	margin-top:50px;
}
.margin_top_60
{
	margin-top:60px;
}
.margin_top_70
{
	margin-top:70px;
}
.margin_top_80
{
	margin-top:80px;
}
.margin_top_90
{
	margin-top:90px;
}
.margin_top_100
{
	margin-top:100px;
}
.margin_top_110
{
	margin-top:100px;
}
.margin_top_120
{
	margin-top:120px;
}
.margin_top_130
{
	margin-top:130px;
}


/*-=-=-=-==-==-=-=-=-=-=-=-=-==-==-==-= Common Library -=-=-------------=-=-*/
.success_holder h1{
	font-size:30px;
	margin:15px 0;
	font-weight:bold;
}
.success_holder p{
	font-size:14px;
	line-height:24px;
}

@media (min-width:320px) and (max-width:767px)
{
.wrapper
{
	width:90%;
}
.success_holder p{
	font-size:14px;
	line-height:24px;
	margin-bottom:10px;
}
.success_holder h1{
	font-size:20px;
}
}
@media (min-width:480px) and (max-width:767px)
{
.wrapper
{
	width:80%;
}
.success_holder h1{
	font-size:24px;
}
}
@media (min-width:768px) and (max-width:1024px)
{
.wrapper
{
	width:80%;
}
.success_holder h1{
	font-size:24px;
}
}


</style>


</head>

<body>


		<div class="wrapper">
        	<div class="success_holder">
                <?php if($status==3){ ?> 
                <div class="alert alert-info text-center" style="margin: 50px 0 0;">
                    <h1><i class="fa fa-check fa-2x" aria-hidden="true"></i><br>Your email address is verified.</h1>
                    <p>Thank you for your confirmation.</p>
                  </div>
                <script>
                    $(document).ready(function(){
                       setTimeout(function(){
                        window.location.href='<?= $link;?>';
                        }, 5000); 
                    });
                </script>
                <?php }elseif($status==2){ ?>
                    <div class="alert alert-success text-center" style="margin: 50px 0 0;">
                    <h1><i class="fa fa-check fa-2x" aria-hidden="true"></i><br>Email already verified.</h1>
                  </div>
                <?php }else{ ?>
                <div class="alert alert-danger text-center" style="margin: 50px 0 0;">
                    <h1><i class="fa fa-times" aria-hidden="true" style="color: red;"></i> <br>Teacher not found.</h1>
                  </div>
                <?php } ?>
               </div>
        </div>

</body>
</html>
