PEDAGOGE Plugin URL and DIR Constant
	PEDAGOGE_PLUGIN_URL
	PEDAGOGE_PLUGIN_DIR
	
PEDAGOGE Assets URL Constant
	PEDAGOGE_ASSETS_URL
	
PEDAGOGE Bower Components URL and DIR Constant
	BOWER_ROOT_URL
	BOWER_ROOT_DIR
	
PEDAGOGE MVC DIR Constant	
	PEDAGOGE_MVC_DIR

Pedagoge Application Endpoint - www.example.com/apps/
	PEDAGOGE_ENDPOINT_URL
	
//CSS Containers
// $common_css
// $css_assets
// $app_css
// $google_fonts

//JS Containers
// $header_js
// $footer_js
// $footer_common_js
// $app_js

var $ajaxurl
var $ajaxnonce
var $pedagoge_callback_class
var $pedagoge_users_ajax_handler
var $pedagoge_visitor_ajax_handler

//testing
