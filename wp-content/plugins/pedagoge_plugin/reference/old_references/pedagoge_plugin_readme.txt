--------------------------------------
	Notes: 
--------------------------------------
* Plugin uses Composer and Bower to download dependent components.

* Before uploading and activating the plugin, please run the following commands in the plugin root Directory.
	-> Bower install (For installing required Bower components and their dependencies)
	-> Composer install (For installing required components and their dependencies)
	
* All controllers should be prefixed with 'Controller'.

* All Models should be prefixed with 'Model'.

* All views files(templates) should have '.html.php' extension.

* All CSS/JS assets should be first registed in pedagoge_plugin/swpmvc/controllers/ControllerAssetsLoader.php

* All Controllers should inherit from 'ControllerMaster' and implement the interface 'ControllerInterface'

* Reserved keywords - ajax, assetloader, master, interface - should not be used in the root routes

--------------------------------------
	Directory Structure
--------------------------------------
pedagoge_plugin/ - Plugin Root Directory

pedagoge_plugin/assets - Custom Assets (Images, CSS/JS Files). Constants for this Directory is PEDAGOGE_ASSETS_URL 
pedagoge_plugin/assets/css - Custom CSS Files
pedagoge_plugin/assets/images - Theme/Template specific images will be stored here.
pedagoge_plugin/assets/js - Custom JS Files
pedagoge_plugin/assets/plugins - JS Libraries which are not available via Bower

pedagoge_plugin/Bower_components - Bower Components are stored in this directory and contents updated dynamically via Bower. Constants are Bower_ROOT_URL and Bower_ROOT_DIR. This Directory will not be included in the versioning system.

pedagoge_plugin/database - This directory will contain DB Migration and Seeders. Currently it will store .sql files.

pedagoge_plugin/includes - This directory contains custom PHP/Library utilities which are not available via Composer.
pedagoge_plugin/includes/helpers - This directory contains helper utilities (i.e. custom logger, or autoloader)
pedagoge_plugin/includes/pedagoge_virtual_pages - This directory contains files which allows for creating virtual pages a.k.a - routes.
pedagoge_plugin/includes/gm-virtual-pages - Contains library files related to Virtual Pages/Routing.

pedagoge_plugin/languages - This directory will contain .mo, .po files for transliteration. 

pedagoge_plugin/references - This directory contains files for developers reference.
pedagoge_plugin/references/view_fields - This directory contains list of files related to views containing list of HTML component's Name/ID/Class

pedagoge_plugin/storage - This directory will contain all the files which can be modified via public/scripts.
pedagoge_plugin/storage/cache - Scripts (CSS/JS), Template Cache, JSON Cached and other cached files will be stored here.
pedagoge_plugin/storage/logs - Log files generated via applications can be stored here.
pedagoge_plugin/storage/uploads - All uploads (Images/Videos/Documents uploaded by users) will be stored here.

pedagoge_plugin/swpmvc - This is the root Directory of the custom MVC Framework.
pedagoge_plugin/swpmvc/controllers - This directory stores all the Controllers of the application.
pedagoge_plugin/swpmvc/models - This directory stores all the Models of the application.
pedagoge_plugin/swpmvc/requests - This directory will store all the middlewares related to a Route's requests (GET/POST).

pedagoge_plugin/views - This is the root directory of HTML/PHP Templates. All template files ends with '.html.php' extension.
pedagoge_plugin/views/email_templates - This directory will store templates designed for emails. Whenever an Email will be send through the application, specific email templates from this Directory will be loaded.
pedagoge_plugin/views/message_strings - All validation, or process related messages will be stored in files in this directory. It will help in transliteration.

pedagoge_plugin/vendor - All Composer Packages are stored here and contents updated dynamically via Composer. This Directory will not be included in the versioning system.


--------------------------------------
	Application Constants
--------------------------------------
PEDAGOGE Plugin URL and DIR Constant
	PEDAGOGE_PLUGIN_URL
	PEDAGOGE_PLUGIN_DIR
	
PEDAGOGE Assets URL Constant
	PEDAGOGE_ASSETS_URL
	
PEDAGOGE Bower Components URL and DIR Constant
	Bower_ROOT_URL
	Bower_ROOT_DIR
	
PEDAGOGE MVC DIR Constant	
	PEDAGOGE_MVC_DIR

Pedagoge Application Endpoint - www.example.com/api/v1/
	PEDAGOGE_ENDPOINT_URL

--------------------------------------
	Application Global PHP Variables for Templates
--------------------------------------	
Assets Containers
	CSS Containers
		$common_css - All common libraries css files should be included in this variable. Files of this variable will be combined, compressed and cached. This can be accessed via $this->common_css
		$css_assets - Library css files which are specific to a page/route should be included in this variable. Files of this variable will be combined, compressed and cached. Access is similar to $common_css
		$app_css - All custom CSS files should be included in this variable. Files from this variable will never be cached. Access is similar to $common_css
		$google_fonts - All google fonts should be included in this variable.

	JS Containers
		$header_js - All common libraries JS files which needs to be inserted in the header section of an HTML page should be included in this variable. Files of this variable will be combined, compressed and cached.
		$footer_common_js - All common libraries JS files should be included in this variable. Files of this variable will be combined, compressed and cached.
		$footer_js - All Library JS files specific to a page/route should be included in this variable. Files will be inserted in the footer section of the HTML page.
		$app_js - Custom JS files should be included in this variable. These files will be inserted at the very last and will never be cached.

Global Variable Register
	$app_data - Variables stored here have global scope.

--------------------------------------
	Application Global JavaScript Variables
--------------------------------------
var $ajaxurl - Exposes ajax processor url for ajax operation.
var $ajaxnonce -  Exposes nonce variable for validating ajax operation. This variable must always be included in all ajax calls.
var $pedagoge_callback_class - Exposes class name where ajax handling function is stored.
var $pedagoge_users_ajax_handler - Ajax variable specific to logged in user.
var $pedagoge_visitor_ajax_handler - Ajax variable specific to visitors who are not logged in.

--------------------------------------
	Important Files explained
--------------------------------------
pedagoge_plugin/pedagoge_plugin.php - Main plugin file containing definition of the plugin. This file bootstraps the complete application.
pedagoge_plugin/includes/includes.php - PHP files in swpmvc and vendor directory are autoloaded. Other required php files are included in this file.
pedagoge_plugin/swpmvc/controllers/ControllerAjax.php - This is the AJAX bootstrap file. It controls and redirects all the AJAX requests to their respective ajax handling functions.
pedagoge_plugin/swpmvc/controllers/ControllerAssetsLoader.php - All assets (CSS/JS) files are and should be registered here. MVC Master controller uses assets registry of this file to resolve an asset.
pedagoge_plugin/swpmvc/controllers/ControllerMaster.php - This is the master controller class. It initializes all the required variables, and prepares templates for a basic page. All Route handling controllers must extend(inherit) this.
pedagoge_plugin/swpmvc/controllers/ControllerInterface.php - All Route handline controllers must implement this interface.

--------------------------------------
	Code Walk (Create an Example)
--------------------------------------
Example website is www.pedagoge.net. It is a wordpress website and pedagoge_plugin is installed and activated. Let's assume
that you have updated Bower and Composer for the dependencies in the Directory.

We need an Endpoint to define a page. Based on this endpoint we will be creating our views and controllers.
Let's define an endpoint /settings . So, the url would be www.pedagoge.net/settings 

In order to setup this page we need to do the following
1. Create a Route
	- Routes are defined in the plugin bootstrap file in the $routes variable. Find the $routes array and add the endpoint 'settings'
	- If you need nested route i.e. '/settings/backup/' then include the endpoint as 'settings/backup'
3. Create Views
	- You can skip creating header and footer section of your page if your header and footer contains codes similar to other pages.
	- Create a directory 'settings' in the 'swpmvc/views' directory to store your template files for this page.
	- You can name your template file anything you want, for this example create index.html.php in the 'settings' directory.
	- This template/view can be loaded via its relative path i.e. $this->load_view('settings/index'); wherever required.
	- You can reuse views created for other pages just by referencing its relative path i.e. $this->load_view('login/header');
2. Create a Controller
	- Go to controllers directory in the 'swpmvc' directory and create a Route Handler Class (ControllerSettings) inside the file ControllerSettings.php
	- Note 
		-- Class name must match the file name and should be CamelCase
		-- for nested routes i.e. '/settings/backup' your controller file should be named as ControllerSettingsBackup.php
		
Examples --- ControllerSettings.php
<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerSettings extends ControllerMaster implements ControllerInterface {
	public function __construct() {
		// nothing to do here.
		// Only master controller gets autoconstructed.		 
	}
	
	public function fn_construct_class() {
		//Initialize Master Controller
		parent::__construct(); 
		
		//Page Title
		$this->title = 'Settings Page:';
		
		//You can load your custom assets (css/js) specific to this page. 
		//Master page has already loaded all the required scripts for a blank page.
		$this->fn_load_scripts(); 
		
		//Set this variable to define body class of your page.
		$this->body_class .= ' page-register login-alt page-header-fixed';
		
		//Register variables required in the global scope. 
		//Variables are stored in $this->app_data array variable.
		$this->fn_register_common_variables();		
	}
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		return $this->load_view('login/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		return $this->load_view('login/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		$template_vars['example_string'] = 'Hello World!';
		$template_vars['example_number'] = 4;
		
		$content = $this->load_view('settings/index', $template_vars);		
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		//Available for the page at global scope.
		$this->app_data['global_example_string'] = 'example string';
		
		//If you wish to include a dynamic inline javascript code then use the variable 'inline_js' available in the $app_data variable.
		// This variable is a string, so append your script string to it. e.g.
		$this->app_data['inline_js'] .= '<script type="text/javascript">console.log("Testing");</script>';
	}
	
	// Example of Ajax request processing
	public static function settings_example_ajax() {
		
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => ''
		);	
		
	    $is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! Security Code was not correct! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$return_message['error'] = false;
		$return_message['message'] = 'Success! Settings Ajax Request processed successfully!';		
		echo json_encode($return_message );
		
		die();
	}
}
		
		
		
	




