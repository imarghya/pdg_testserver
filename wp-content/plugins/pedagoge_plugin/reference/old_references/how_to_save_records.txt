
/////////////////////////////////////////////////////////////////////////////
		Deprecated Information. Do not use this
/////////////////////////////////////////////////////////////////////////////


in order to save data
create and instance of master model
	$master_model = new ModelMaster('table_name');
Set its primary key
	$master_model->set_primary_key('primary_key_field_name');
Set its Columns (Optional)
	$master_model->set_columns(array $columns_array);
Set its Columns DataType
	$master_model->set_columns_datatype(array $columns_array)
Save the dataset (include not_duplicate columns array)
	$result = $master_model->save(array $values_array, array $not_duplicate = array())
	
$result array will contain following key=>value pairs
	array(
		'error' => FALSE, // TRUE/FALSE
		'error_desc' => '', // Error Description
		'result' => '', // Successful operation result
		'insert_id' => '' //insert id 
	);

Example
-----------------------------------
$testing_model = new ModelMaster('testing');
$testing_model->set_primary_key('id');

$testing_model->set_columns(array('test_name'=>null))
			->set_columns_datatype(array('test_name'=>'%s'));

$result = $testing_model->save(array('test_name'=>'Another Test'), array('test_name'=>'Another Test'));

