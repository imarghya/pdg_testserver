(function(pedagogeJquery){
	pedagogeJquery(window.jQuery, window, document);
})(function($, window, document){
	var PdgWorkshop = function(){
		var mainObj = this, settingsObj;
		
		mainObj.settings = function() {
			var select_control_with_title_multiple = $(".select_control_with_title_multiple"),
			select_control_with_title = $(".select_control_with_title"), 
			select_fltr_workshop_locality = $("#select_fltr_workshop_locality"),
			select_fltr_workshop_city = $("#select_fltr_workshop_city"),
			select_fltr_workshop_type = $("#select_fltr_workshop_type"),
			select_fltr_workshop_status = $("#select_fltr_workshop_status"),
			select_fltr_workshop_category = $("#select_fltr_workshop_category"),
			table_workshops_list = $("#table_workshops_list"),
			hidden_manage_workshop_url = $("#hidden_manage_workshop_url").val(),
			hidden_workshop_url = $("#hidden_workshop_url").val(),
			select_workshop_filter = $(".select_workshop_filter"),
			fltr_workshop = $(".fltr_workshop");
			
			return {
				select_control_with_title_multiple : select_control_with_title_multiple,
				select_control_with_title : select_control_with_title,
				select_fltr_workshop_locality : select_fltr_workshop_locality,
				select_fltr_workshop_city : select_fltr_workshop_city,
				select_fltr_workshop_type : select_fltr_workshop_type,
				select_fltr_workshop_category : select_fltr_workshop_category,
				select_fltr_workshop_status : select_fltr_workshop_status,
				table_workshops_list : table_workshops_list,
				hidden_manage_workshop_url : hidden_manage_workshop_url,
				hidden_workshop_url : hidden_workshop_url,
				select_workshop_filter : select_workshop_filter,
				fltr_workshop : fltr_workshop,
			};
		};
		
		mainObj.init = function() {
			settingsObj = mainObj.settings();
			mainObj.processSelect2();			
			mainObj.bindUIActions();
			mainObj.loadWorkshopListDT();
		};
		
		mainObj.bindUIActions = function() {
			settingsObj.select_workshop_filter.on('change', function(){
				var selectedControl = $(this),
					dataValue = selectedControl.val(),
					dataColumn = selectedControl.data('column');				
				mainObj.workshopListDTFilterColumn(dataColumn, dataValue);
			});
			
			 settingsObj.fltr_workshop.onEnter(function(){
		    	var selectedControl = $(this),
					dataValue = selectedControl.val(),
					dataColumn = selectedControl.data('column');				
				mainObj.workshopListDTFilterColumn(dataColumn, dataValue);
		    });
			
			settingsObj.select_fltr_workshop_city.on('change', function(){
				var citySelect = $(this),
					cityID = citySelect.val();
				mainObj.getLocalitiesOfCity(cityID);
			});
			
			settingsObj.table_workshops_list.on('click', '.hyperlink_btn', function(event){
				event.preventDefault();
    			window.open($(this).prop('href')); 
			});
		};
		
		mainObj.processSelect2 = function() {
			
			settingsObj.select_control_with_title.each(function(index){
				var selectControl = $(this),				
				title = selectControl.prop('title');
						
				selectControl.select2({
					placeholder: title,
					allowClear: true
				});				
			});
			
			settingsObj.select_control_with_title_multiple.each(function(index){
				var selectControl = $(this), 
				title = selectControl.prop('title');
						
				selectControl.select2({
					placeholder: title,
					closeOnSelect: false,
				});
			});			
		};
		
		mainObj.getLocalitiesOfCity = function(cityID) {
			var cityLocalitites = '<option></option>';
			
			if(!cityID) {
				cityID = '';
			} else {
				if($.isNumeric(cityID)) {
					cityLocalitites += $localities_data[cityID];
				}
			}
			settingsObj.select_fltr_workshop_locality.html(cityLocalitites).select2({		
				closeOnSelect: true,
				placeholder : "Please select a locality",
				allowClear: true
			});
		};
		
		mainObj.loadWorkshopListDT = function() {
			settingsObj.workshopListDT = settingsObj.table_workshops_list.DataTable( {
				"scrollX": true,
		        "pageLength": 10,
		        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
		        "processing": true,
		        "serverSide": true,
		        "lengthChange" : true,
		        "pagingType" : 'simple',
		        "ajax": {
		            "url": $ajax_url,
		            "data": function ( d ) {
		                d.action = $pedagoge_visitor_ajax_handler;
		                d.pedagoge_callback_function = 'workshop_list_datatable';
		                d.pedagoge_callback_class = 'ControlPanelWorkshop';
		            }
		        },
		        "columnDefs": [{
		                "targets": [ 1, 6, 8, 10, 15, 16],
		                "visible": false,                
		           },
		        ],
		        "rowCallback": function( row, data, index ) {
		        	var workshopID = data[0],
						userID = data[1],
						trainerName = data[3],
						workshopManageURL = settingsObj.hidden_manage_workshop_url,
						workshopURL = settingsObj.hidden_workshop_url+'/?workshop_id='+workshopID,
						trainerEditURL = workshopManageURL+'/?edit&user_id='+userID,
						workshopEditURL = trainerEditURL+'&workshop_id='+workshopID, 
						trainerEditLink = '<a title="View Trainer profile" href="'+trainerEditURL+'" target="_blank">'+trainerName+'</a>',
						workshopButtons= '<div style="width:100px!important;" class="btn-toolbar">'+
												'<div class="btn-group" data-toggle="buttons">'+
													'<a class="btn btn-info hyperlink_btn" title="View Workshop" href="'+workshopURL+'" target="_blank"><i class="fa fa-eye"></i></a>'+
													'<a class="btn btn-warning hyperlink_btn" title="Edit Workshop" href="'+workshopEditURL+'" target="_blank"><i class="fa fa-pencil-square-o"></i></a>'+
												'</div>'+
											'</div>';
					$('td:last-child', row).html( workshopButtons );
					
					$('td:eq(2)', row).html( trainerEditLink );
				}
		    } );
		};
		
		mainObj.workshopListDTFilterColumn = function( columnNo, value ) {
		    settingsObj.table_workshops_list.DataTable().column( columnNo ).search( value ).draw();
		};
		
	}, workshopObj;
	workshopObj = new PdgWorkshop;
	workshopObj.init();
});
