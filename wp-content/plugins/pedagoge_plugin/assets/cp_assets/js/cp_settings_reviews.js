(function(pedagogeJquery){
	pedagogeJquery(window.jQuery, window, document);
})(function($, window, document){
	var PdgReviewsSettings = function(){
		var mainObj = this, settingsObj;
		
		mainObj.settings = function() {
			var button_fix_reviews_average = $("#cmd_fix_reviews_average"),
			button_fetch_duplicate_reviews = $("#cmd_fetch_duplicate_reviews"),
			duplicate_reviews_result_area = $("#div_duplicate_reviews_result_area");
			
			return {
				button_fix_reviews_average : button_fix_reviews_average,
				button_fetch_duplicate_reviews : button_fetch_duplicate_reviews,
				duplicate_reviews_result_area : duplicate_reviews_result_area,
			};
		};
		
		mainObj.pdg_ajax_handler = function( data, identifier, ajaxData ) {
			var dynamicData = {};
			dynamicData[identifier] = data;
			dynamicData['nonce'] = $ajaxnonce;
			dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
			dynamicData['pedagoge_callback_class'] = ajaxData['pedagoge_callback_class'];
			dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
			return $.ajax( {
				url: $ajax_url,
				type: "post",
				data: dynamicData
			} );
		};		
		
		mainObj.init = function() {
			settingsObj = mainObj.settings();
			mainObj.bindUIActions();
		};
		
		mainObj.bindUIActions = function() {
			settingsObj.button_fix_reviews_average.on('click', function(){
				settingsObj.button_fix_reviews_average.button('loading');
				mainObj.updateDuplicateReviewsTable();
				mainObj.fixReviewsAverage();
			});
			settingsObj.button_fetch_duplicate_reviews.on('click', function(){
				settingsObj.button_fetch_duplicate_reviews.button('loading');
				mainObj.updateDuplicateReviewsTable();
				mainObj.fetchDuplicateReviews();	
			});
			settingsObj.duplicate_reviews_result_area.on('click', '.cmd_delete_duplicate_review', function(){
				var deleteButton = $(this);
				mainObj.removeDuplicateReview(deleteButton);				
			});
		};
		
		mainObj.updateDuplicateReviewsTable = function($html) {
			var $html_content = '';
			if($html) {
				$html_content = $html;	
			}
			settingsObj.duplicate_reviews_result_area.html($html_content);
		};
		
		mainObj.fixReviewsAverage = function() {
			var ajaxData = {
				'pedagoge_callback_function' : 'fix_review_average',
				'pedagoge_callback_class' : 'ControlPanelSettings',
			};
			mainObj.pdg_ajax_handler({}, 'fix_review_average', ajaxData).done(function(data){
				try {
					var $response_data = $.parseJSON( data ),
						$error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message;
					if ( ! $error ) {
						$.notify($message, 'success');
					}
				} catch ( err ) {
				} finally {
					settingsObj.button_fix_reviews_average.button('reset');
				}
			});
		};
		
		mainObj.fetchDuplicateReviews = function() {
			var ajaxData = {
				'pedagoge_callback_function' : 'fetch_duplicate_reviews',
				'pedagoge_callback_class' : 'ControlPanelSettings',
			};
			mainObj.pdg_ajax_handler({}, 'fetch_duplicate_reviews', ajaxData).done(function(data){
				try {
					var $response_data = $.parseJSON( data ),
						$error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message,
						$html_data = $response_data.html;
					if ( ! $error ) {
						$.notify($message, 'success');
						mainObj.updateDuplicateReviewsTable($html_data);
					}
				} catch ( err ) {
				} finally {
					settingsObj.button_fetch_duplicate_reviews.button('reset');
				}
			});
		};
		mainObj.removeDuplicateReview = function(deleteButton) {
			var reviewID = deleteButton.data('review_id');
			var parentRow = deleteButton.closest('tr');
			
			bootbox.confirm("Are you sure, you want to delete this review?", function(result){
				if(result === true) {
					deleteButton.button('loading');
					var ajaxData = {
						'pedagoge_callback_function' : 'remove_duplicate_review',
						'pedagoge_callback_class' : 'ControlPanelSettings',
						
					};
					var dynamicData = {
						'reviewID':reviewID
					};
					mainObj.pdg_ajax_handler(dynamicData, 'duplicate_review', ajaxData).done(function(data){
						try {
							var $response_data = $.parseJSON( data ),
								$error = $response_data.error,
								$error_type = $response_data.error_type,
								$message = $response_data.message;
							if ( ! $error ) {
								$.notify($message, 'success');
								parentRow.remove();
							}
						} catch ( err ) {
							deleteButton.button('reset');
						} finally {
						}
					});	
				}
			});
		};
		
		
	}, reviewSettingsObj;
	reviewSettingsObj = new PdgReviewsSettings;
	reviewSettingsObj.init();
});
