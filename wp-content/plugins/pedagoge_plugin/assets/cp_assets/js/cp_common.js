$(document).ready(function() {
	$(".not_clickable").click(function(event){
		event.preventDefault();
	});
	$("input:checkbox, input:radio").uniform();
	setInterval(showTime,500);
});

/**
 * Digital Clock 
 */
function showTime()
{
	var a_p = "";
	var d = new Date();
	var curr_hour = d.getHours();
	if (curr_hour < 12)
	{
		a_p = "AM";
	}
	else
	{
		a_p = "PM";
	}
	if (curr_hour == 0)
	{
		curr_hour = 12;
	}		
	if (curr_hour > 12)
	{
		curr_hour = curr_hour - 12;
	}
	if(curr_hour<10)
	{
		curr_hour="0" + curr_hour;			
	}
			
	var curr_min = d.getMinutes();
		
	curr_min = curr_min + "";
		
	if (curr_min<10)
	{
		curr_min = "0" + curr_min;
	}
	var curr_sec = d.getSeconds();
	if (curr_sec<10)
	{
		curr_sec = "0" + curr_sec;
	}
    jQuery('#timeclock').html(curr_hour + ":" + curr_min + ":" + curr_sec+" "+a_p);    
}