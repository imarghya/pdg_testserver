$(document).ready(function(){
   //Show follow up date on double click----------------------------
    $(document).on('click', '.approv_disapp_ref', function(){
    $reffer_id=$(this).attr("ref-id");
    $is_approve=$(this).attr("is_approve");
    $reff_by=$(this).attr("ref-by-id");
    var result = confirm("Are you sure?");
    if ($is_approve==0) {
     if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'approve_refeeral',
        pedagoge_callback_class: 'ControllerDashboard',
        reffer_id : $reffer_id,
        reff_by   : $reff_by,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#ref_btn"+$reffer_id).attr("is_approve",1);
                   $("#ref_btn"+$reffer_id).text("Unapprove");
                   alertify.success("Success: Referral Approved."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               alert(error);
            });
         //--------------------------------------------------------
      }
    }
    else{
      if (result) {
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'unapprove_refeeral',
        pedagoge_callback_class: 'ControllerDashboard',
        reffer_id : $reffer_id,
        reff_by   : $reff_by,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                if (response=='success') {
                   $("#ref_btn"+$reffer_id).attr("is_approve",0);
                   $("#ref_btn"+$reffer_id).text("Approve");
                   alertify.success("Success: Referral UnApproved."); 
                }
            })
            .fail(function(jqxhr, status, error){ 
               alert(error);
            });
         //--------------------------------------------------------
      } 
    }
    });
});