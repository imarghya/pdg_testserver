$(document).ready(function(){
	//$("input:checkbox, input:radio").uniform();
	
});

function fn_element_notification_message($element, $message, $bootbox) {
	
	if($bootbox) {
		bootbox.alert($message, function() {			
			$element[0].scrollIntoView( true );
			$element.notify($message, 'error');	
		});
	} else {
		$element[0].scrollIntoView( true );
		$element.notify($message, 'error');
	}
}

/**
 * System Settings Tab 
 */
$(document).ready(function(){
	$("#cmd_clear_data_cache").click(function(){
		var $button = $(this);
		fn_clear_cache($button, 'data');
	});
	$("#cmd_clear_scripts_cache").click(function(){
		var $button = $(this);
		fn_clear_cache($button, 'scripts');
	});
	
	$("#cmd_update_default_sender_email").click(function(){
		var $default_sender = $("#txt_default_email_sender").val();
		var $button = $(this);
		var $email_queue = null;
		var $email_async = null;
		var $email_log = null;
		
		if($("#chk_enable_email_queue").is(':checked')) {
			$email_queue = 'yes';
		} else {
			$email_queue = 'no';
		}
		
		if($("#chk_enable_asynchronous_emails").is(':checked')) {
			$email_async = 'yes';
		} else {
			$email_async = 'no';
		}
		
		if($("#chk_log_emails").is(':checked')) {
			$email_log = 'yes';
		} else {
			$email_log = 'no';
		}
		
		var $data = {};
		$data.email_queue = $email_queue;
		$data.email_async = $email_async;
		$data.email_log = $email_log;
		$data.default_sender_email = $default_sender;
		
		fn_update_email_system_settings($button, $data);
	});
	
	$("#chk_enable_minification").change(function(){
		var $minification_enabled = 'no';
		if(this.checked) {
			$minification_enabled = 'yes';
		} 
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_minification_enabled_ajax',
			pedagoge_callback_class : 'ControlPanelSettings',
			minification_enabled : $minification_enabled
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			}
	    }).complete(function() {
	    });
	});
	
});


function fn_clear_cache($button, $cache_type) {
	$button.button('loading');
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_clear_cache_ajax',
		pedagoge_callback_class : 'ControlPanelSettings',
		cache_type : $cache_type
	};
	
	$.post($ajax_url, submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
	
			var $error = $response_data.error,									 	
			 	$message = $response_data.message;
			 	
		 	if($error) {
				$.notify($message, 'error');
			} else {
				$.notify($message, 'success');
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		}
    }).complete(function() {	        	
    	$button.button('reset');
    });
}

function fn_update_email_system_settings($button, $data) {
	$button.button('loading');
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_update_email_system_settings',
		pedagoge_callback_class : 'ControlPanelSettings',
		email_queue : $data.email_queue,
		email_async : $data.email_async,
		email_log : $data.email_log,
		default_sender_email : $data.default_sender_email
	};
	
	$.post($ajax_url, submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
	
			var $error = $response_data.error,									 	
			 	$message = $response_data.message;
			 	
		 	if($error) {
				$.notify($message, 'error');
			} else {
				$.notify($message, 'success');
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		}
    }).complete(function() {	        	
    	$button.button('reset');
    });
}

//////////////////////////////////////////////////////////////// Email Settings/////////////////////////////////////////////

$(document).ready(function(){
	
	$("#cmd_create_mail_type").click(function(){
		var $button = $(this);
		var $message = '';
		var $txt_create_mail_type_label = $("#txt_create_mail_type_label").val();
		if($txt_create_mail_type_label.length <= 0) {
			$message = "Please Input Mail type";
			fn_element_notification_message($("#txt_create_mail_type_label"), $message);
			return;
		}
		
		$button.button('loading');
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_create_email_type',
			pedagoge_callback_class : 'ControlPanelSettings',
			email_type : $txt_create_mail_type_label
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');
					var $str_html = '<option value="'+$data+'">'+$txt_create_mail_type_label+'</option>';
					$("#txt_create_mail_type_label").val('');
					$("#select_mail_type").append($str_html);
					$("#select_mail_type").val($data).change();
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			}
	    }).complete(function() {	        	
	    	$button.button('reset');
	    });
	});
	
	$("#select_mail_type").change(function() {
		var $mail_settings_id = $("#select_mail_type option:selected").val();
		var $mail_type_label = $("#select_mail_type option:selected").text();
		
		var $email_type_loader = $("#img_mail_type_loader");
		
		fn_reset_email_settings_form();
		
		if($mail_settings_id.length > 0) {
			$email_type_loader.show();
			
			var submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_load_email_type_info',
				pedagoge_callback_class : 'ControlPanelSettings',
				mail_settings_id : $mail_settings_id
			};
			
			$.post($ajax_url, submit_data, function(response) {
				try {
					var $response_data = $.parseJSON(response);
			
					var $error = $response_data.error,									 	
					 	$message = $response_data.message,
					 	$email_data = $response_data.data;
					 	
				 	if($error) {
						$.notify($message, 'error');
					} else {
						$.notify($message, 'success');
						fn_load_settings_data_on_form($email_data);
					}
				} catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					console.log($message);
				}
		    }).complete(function() {	        	
		    	$email_type_loader.hide();
		    });
		}
	});
	
	$("#cmd_add_subscriber_email").click(function(){
		var $button = $(this);
		var $email_text_box_val = $("#txt_subscriber_email_address").val();
		var $message = '';
		var $mail_settings_id = $("#select_mail_type option:selected").val();
		
		if(!$.isNumeric($mail_settings_id)) {
			$message = "Please select an email settings and try again.";
			fn_element_notification_message($("#select_mail_type"), $message);
			return;
		}
		
		if($email_text_box_val.length<=0) {
			$message = "Please input an email address and try again.";
			fn_element_notification_message($("#txt_subscriber_email_address"), $message);
			return;
		}
		$button.button('loading');
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_add_email_subscriber',
			pedagoge_callback_class : 'ControlPanelSettings',
			mail_settings_id : $mail_settings_id,
			email_address : $email_text_box_val
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$email_data = $response_data.data;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');
					$("#txt_subscriber_email_address").val('');
					fn_reload_subscribers_list();
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			}
	    }).complete(function() {	        	
	    	$button.button('reset');
	    });
	});
	
	$("#div_list_subscribers").on('click', ".cmd_remove_subscriber", function(){
		var $button = $(this);
		var $parent_row = $button.parent().parent();
		var $subscriber_id = $button.data('subscriber_id');
		bootbox.confirm({
		    message: "Are you sure, you want to remove this subscriber?",
		    buttons: {
		        confirm: {
		            label: 'Yes',
		            className: 'btn-success'
		        },
		        cancel: {
		            label: 'No',
		            className: 'btn-danger'
		        }
		    },
		    callback: function (result) {
		        if(result === true) {		        	
		        	
		        	$button.button('loading');
					var submit_data = {
						nonce : $ajaxnonce,
						action : $pedagoge_visitor_ajax_handler,
						pedagoge_callback_function : 'fn_remove_email_subscriber',
						pedagoge_callback_class : 'ControlPanelSettings',
						subscriber_id : $subscriber_id						
					};
		        	$.post($ajax_url, submit_data, function(response) {
						try {
							var $response_data = $.parseJSON(response);
					
							var $error = $response_data.error,									 	
							 	$message = $response_data.message;
							 	
						 	if($error) {
								$.notify($message, 'error');
							} else {
								$.notify($message, 'success');
								$parent_row.remove();
							}
						} catch( err ) {
							var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
							console.log($message);
						}
				    }).complete(function() {	        	
				    	$button.button('reset');
				    });
		        }
		    }
		});
	});
	
	$("#cmd_update_email_settings").click(function(){
		var $button = $(this);
		var $mail_settings_id = $("#select_mail_type option:selected").val();
		var $txt_mail_type_label = $("#txt_mail_type_label").val();
		var $txt_mail_type_sender_email = $("#txt_mail_type_sender_email").val();
		var $txt_email_subject = $("#txt_email_subject").val();
		var $send_to_subscriber = 'no';
		var $send_to_user = 'no';
		var $select_email_sending_priority = $("#select_email_sending_priority option:selected").val();
		var $message = '';
		
		if($("#chk_send_to_subscriber").prop('checked')) {
			$send_to_subscriber = 'yes';
		}
		if($("#chk_send_to_user").prop('checked')) {
			$send_to_user = 'yes';
		}
		
		if($mail_settings_id.length <= 0) {
			$message = "Please select an email settings and try again.";
			fn_element_notification_message($("#select_mail_type"), $message);
			return;
		}
		
		if($txt_email_subject.length <= 0 ) {
			$message = "Please input Email Subject and try again.";
			fn_element_notification_message($("#txt_email_subject"), $message);
			return;
		}
		if($txt_mail_type_label.length <= 0 ) {
			$message = "Please input Mail Settings Label and try again.";
			fn_element_notification_message($("#txt_mail_type_label"), $message);
			return;
		}
		
		$button.button('loading');
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_update_email_settings',
			pedagoge_callback_class : 'ControlPanelSettings',
			mail_settings_id : $mail_settings_id,
			txt_mail_type_label : $txt_mail_type_label,
			txt_email_subject : $txt_email_subject,
			txt_mail_type_sender_email : $txt_mail_type_sender_email,
			send_to_subscriber : $send_to_subscriber,
			send_to_user : $send_to_user,
			select_email_sending_priority : $select_email_sending_priority
		};
    	$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');
					$("#select_mail_type option:selected").text($txt_mail_type_label);
					$("#txt_mail_type_name").val($data);
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			}
	    }).complete(function() {	        	
	    	$button.button('reset');
	    });
		
	});
	
});

function fn_load_settings_data_on_form($email_data) {
	$("#txt_mail_type_name").val($email_data.email_type);
	$("#txt_mail_type_label").val($email_data.email_label);
	$("#txt_mail_type_sender_email").val($email_data.sender_email);
	$("#txt_email_subject").val($email_data.email_subject);
	$("#div_list_subscribers").html($email_data.email_subscribers_list);
	
	var $send_to_subscriber = $email_data.send_to_subscriber,
	$send_to_user = $email_data.send_to_user,
	$email_priority = $email_data.email_priority;
	
	$("#select_email_sending_priority").val($email_priority).change();
	
	$('#chk_send_to_subscriber, #chk_send_to_user').prop('checked', false);
	
	if($send_to_subscriber == 'yes') {
		$('#chk_send_to_subscriber').prop('checked', true);
	}
	if($send_to_user == 'yes') {
		$('#chk_send_to_user').prop('checked', true);
	}
	$.uniform.update();
}

function fn_reset_email_settings_form() {
	$("#txt_mail_type_name, #txt_mail_type_label, #txt_mail_type_sender_email, #txt_subscriber_email_address, #txt_email_subject").val('');
	
	$('#chk_send_to_subscriber, #chk_send_to_user').prop('checked', false);
	$.uniform.update();
		
	$("#div_list_subscribers").html('Please input Subscriber email address');	
	$("#select_email_sending_priority").val('immediate').change();
}

function fn_reload_mail_types() {
	
}

function fn_reload_subscribers_list() {
	var $button = $("#cmd_add_subscriber_email");
	var $mail_settings_id = $("#select_mail_type option:selected").val();
	if(!$.isNumeric($mail_settings_id)) {
		return;
	}
	$button.button('loading');
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_email_subscribers_list',
		pedagoge_callback_class : 'ControlPanelSettings',
		mail_settings_id : $mail_settings_id
	};
	
	$.post($ajax_url, submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
	
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$email_data = $response_data.data;
			 	
		 	if($error) {
				$.notify($message, 'error');
			} else {
				$.notify($message, 'success');
				$("#div_list_subscribers").html($email_data);
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		}
    }).complete(function() {	        	
    	$button.button('reset');
    });
}


/////////////////////////////////documents manager///////////////////////////////////////
var $documents_manager_datatable = null;

$(document).ready(function(){
	var $documents_manager_root_url = $("#hidden_document_manager_path").val();
	
	$documents_manager_datatable = $('#table_documents_list').DataTable( {
		"dom" : "<'row'<'col-sm-12'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "processing": true,
        "serverSide": true,
        "lengthChange" : false,
        "pagingType" : 'simple',
        "ajax": {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_documents_manager_datatable';
                d.pedagoge_callback_class = 'ControlPanelSettings';
            }
        },
        "columnDefs": [{
                "targets": [ 1, 3, 6],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {
        	var $new_file_name = data[6];
        	var $user_id = data[1];
		    $html = '<a href="'+$documents_manager_root_url+$user_id+'/'+$new_file_name+'" target="_blank">Download</a>';
			$('td:last-child', row).html( $html );
		}
    } );
    $("#select_fltr_document_role_name").change(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	document_manager_table_filter_column($data_column, $selected_value);
    });
    
    $(".txt_fltr_documents").onEnter(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	document_manager_table_filter_column($data_column, $selected_value);
    });
});

function document_manager_table_filter_column ( i, value ) {
    $('#table_documents_list').DataTable().column( i ).search( value ).draw();
}

////////////////////////////////Email Editor//////////////////////////////////////
var editor = ace.edit("editor");
$(document).ready(function(){
    editor.setTheme("ace/theme/tomorrow");
    editor.getSession().setMode("ace/mode/php");
	
	$("#cmd_email_template_editor").click(function(){
		var $template_name = $("#txt_mail_type_name").val();
		var $template_label = $("#txt_mail_type_label").val();
		if($template_name.length<=0) {
			$.notify("Please select an Email Type first!", 'error');
			return false;
		}
		var $email_data = {};
		$email_data.template_name = $template_name;
		$email_data.template_label = $template_label;
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_load_email_template',
			pedagoge_callback_class : 'ControlPanelSettings',
			template_name : $template_name
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');
					$email_data.email = $data;
					fn_email_editor_modal($email_data);
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
				console.log(err);
			}
	    }).complete(function() {
	    }); 
	});
	
	$("#cmd_update_email_template").click(function(){
		var $template_name = $("#txt_mail_type_name").val();
		if($template_name.length<=0) {
			$.notify("Please select an Email Type first!", 'error');
			return false;
		}
		var $template_content = editor.getValue();
		if($template_content.length<=0) {
			$.notify("You must input template content into the Editor!", 'error');
			return false;
		}
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_save_email_template',
			pedagoge_callback_class : 'ControlPanelSettings',
			template_name : $template_name,
			template_content : $template_content
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');
					editor.setValue('');
					$('#modal_update_email_template').modal('hide');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
				console.log(err);
			}
	    }).complete(function() {
	    }); 
	});
	
	$("#cmd_delete_email_template").click(function(){
		var $button = $(this);
		var $template_name = $("#txt_mail_type_name").val();
		if($template_name.length<=0) {
			$.notify("Please select an Email Type first!", 'error');
			return false;
		}
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_delete_email_template',
			pedagoge_callback_class : 'ControlPanelSettings',
			template_name : $template_name,
		};
		$button.button('loading');
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
		
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				 	
			 	if($error) {
					$.notify($message, 'error');
				} else {
					$.notify($message, 'success');					
					$('#modal_update_email_template').modal('hide');
					$button.button('reset');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
				console.log(err);
				$button.button('reset');
			}
	    }).complete(function() {
	    	$button.button('reset');
	    }); 
	});
});

function fn_email_editor_modal($data) {
	editor.setValue('');
	var $template_name = $data.template_name;
	var $template_label = $data.template_label;
	var $email = $data.email;
	$("#span_email_template_name").html($template_label);
	editor.setValue($email);	
	$('#modal_update_email_template').modal('show');
	editor.resize();
}
