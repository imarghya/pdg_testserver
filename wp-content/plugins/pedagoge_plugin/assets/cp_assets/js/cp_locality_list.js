(function(pedagogeJquery) {
	
	pedagogeJquery(window.jQuery, window, document);
	
})(function($, window, document){
	
	var PdgLocalityList = function(){
		
		var mainObj = this, settingsObj;
		
		mainObj.settings = function() {
						
			var cmd_create_locality = $("#cmd_create_locality"),
				cmd_refresh_locality_list = $("#cmd_refresh_locality_list"),
				img_locality_list_loader = $("#img_locality_list_loader"),				
				select_fltr_city_name = $("#select_fltr_city_name"),
				fltr_locality = $(".fltr_locality"),
				txt_fltr_locality_name = $("#txt_fltr_locality_name"),
				table_locality_list = $("#table_locality_list"),
				localityListDT = '',
				div_locality_save_result_area = $("#div_locality_save_result_area"),
				modal_locality_updates = $("#modal_locality_updates"),
				select_locality_modal_city_name = $("#select_locality_modal_city_name"),
				txt_locality_modal_locality_name = $("#txt_locality_modal_locality_name"),
				hidden_locality_modal_locality_id = $("#hidden_locality_modal_locality_id"),
				cmd_save_locality = $("#cmd_save_locality");
				
			return {				
				cmd_create_locality : cmd_create_locality,
				cmd_refresh_locality_list : cmd_refresh_locality_list,
				img_locality_list_loader : img_locality_list_loader,
				select_fltr_city_name : select_fltr_city_name,
				fltr_locality : fltr_locality,
				txt_fltr_locality_name : txt_fltr_locality_name,
				table_locality_list : table_locality_list,
				localityListDT : localityListDT,
				modal_locality_updates : modal_locality_updates,
				select_locality_modal_city_name : select_locality_modal_city_name,
				txt_locality_modal_locality_name : txt_locality_modal_locality_name,
				div_locality_save_result_area : div_locality_save_result_area,
				hidden_locality_modal_locality_id : hidden_locality_modal_locality_id,
				cmd_save_locality : cmd_save_locality
			};			
		};
		
		mainObj.pdg_ajax_handler = function( data, identifier, ajaxData ) {
			
			var dynamicData = {};
			
			dynamicData[identifier] = data;
			dynamicData['nonce'] = $ajaxnonce;
			dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
			dynamicData['pedagoge_callback_class'] = ajaxData['pedagoge_callback_class'];
			dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
			
			return $.ajax( {
				url: $ajax_url,
				type: "post",
				data: dynamicData
			} );
		};		
		
		mainObj.init = function() {
			
			$("input:checkbox").uniform();
			
			settingsObj = mainObj.settings();			
			
			// setup select2
			mainObj.processSelect2();
			
			mainObj.bindUIActions();
			
			// load datatables
			mainObj.loadLocalityListDT();
			//create datatable if the html table has .datatable class			
			$('.datatable').DataTable({		
				"scrollY":        "200px",
		        "scrollCollapse": true,
		        "paging":         false
			});
		};
		
		mainObj.processSelect2 = function() {
			$(".select_control_with_title").each(function(index){
				var selectControl = $(this),
				title = selectControl.prop('title');		
				selectControl.select2({
					placeholder: title,
				});
			});
			
			$(".select_control_with_title_multiple fltr_locality").each(function(index){
				var selectControl = $(this), 
				title = selectControl.prop('title');		
				selectControl.select2({
					placeholder: $title,
					closeOnSelect: false
				});
			});			
		};
		
		mainObj.loadLocalityListDT = function() {
			settingsObj.localityListDT = settingsObj.table_locality_list.DataTable( {
				"dom" : "<'row'<'col-sm-12'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		        "processing": true,
		        "serverSide": true,
		        "lengthChange" : false,
		        "pagingType" : 'simple',
		        "ajax": {
		            "url": $ajax_url,
		            "data": function ( d ) {
		                d.action = $pedagoge_visitor_ajax_handler;
		                d.pedagoge_callback_function = 'fn_locality_list_datatable';
		                d.pedagoge_callback_class = 'ControlPanelLocality';
		            }
		        },
		        "columnDefs": [{
		                "targets": [ 1, 3, 5],
		                "visible": false,                
		           },
		        ],
		        "rowCallback": function( row, data, index ) {		    
				    $html = '<button class="btn btn-default col-md-12 cmd_view_locality" title="View Locality Info" ><i class="fa fa-eye" aria-hidden="true"></i> View</button>'	;
					$('td:last-child', row).html( $html );
				}
		    } );
		};
		
		mainObj.localityListDTFilterColumn = function( columnNo, value ) {
		    settingsObj.table_locality_list.DataTable().column( columnNo ).search( value ).draw();
		};
		
		mainObj.bindUIActions = function() {			
			settingsObj.select_fltr_city_name.change(function(){
				var selectedCity = $(this),
				cityID = selectedCity.val(),
				dtColumnNo = selectedCity.data('column');
				mainObj.localityListDTFilterColumn(dtColumnNo, cityID);
			});
			settingsObj.txt_fltr_locality_name.onEnter(function(){
				var localityFilterInputBox = $(this),
				localityName = localityFilterInputBox.val(),
				dtColumnNo = localityFilterInputBox.data('column');
				mainObj.localityListDTFilterColumn(dtColumnNo, localityName);
			});
			settingsObj.modal_locality_updates.on('hidden.bs.modal', function(e){
				mainObj.updateLocalityModal();
			});
			settingsObj.table_locality_list.on('click', '.cmd_view_locality', function(){
				var cmd_view_locality = $(this),
				rowData = settingsObj.localityListDT.row( cmd_view_locality.closest('tr')[0] ).data(),
				cityID = rowData[5],
				localityID = rowData[0],
				localityName = rowData[7];
				mainObj.updateLocalityModal(cityID,localityName, localityID);
				settingsObj.modal_locality_updates.modal('show');
			});
			
			settingsObj.cmd_save_locality.click(mainObj.saveLocalityData);
		};
		
		mainObj.updateLocalityModal = function(cityID, localityName, localityID) {
			var modalCityID = '',
				modalLocalityName = '',
				modalLocalityID = '';
			if(cityID) {
				modalCityID = cityID;
			}
			if(localityName) {
				modalLocalityName = localityName;
			}
			if(localityID) {
				modalLocalityID = localityID;
			}
			
			settingsObj.txt_locality_modal_locality_name.val(modalLocalityName);
			settingsObj.select_locality_modal_city_name.val(modalCityID).change();
			settingsObj.hidden_locality_modal_locality_id.val(modalLocalityID);
		};
		
		mainObj.saveLocalityData = function() {
			var button = settingsObj.cmd_save_locality,
				localityID = settingsObj.hidden_locality_modal_locality_id.val(),
				localityName = settingsObj.txt_locality_modal_locality_name.val(),
				cityID = settingsObj.select_locality_modal_city_name.val(),
				resultArea = settingsObj.div_locality_save_result_area;
				
			if(!$.isNumeric(cityID)) {
				$.notify(settingsObj.select_locality_modal_city_name, 'Please select a city', 'error');
				return false;	
			}
			if(localityName.length<=0) {
				$.notify(settingsObj.txt_locality_modal_locality_name, 'Please input locality name', 'error');
				return false;
			}
			
			button.button('loading');
			
			var ajaxData = {
				'pedagoge_callback_function' : 'save_locality',
				'pedagoge_callback_class' : 'ControlPanelLocality',
			};
			var dynamicData = {
				'localityID' : localityID,
				'localityName' : localityName,
				'cityID' : cityID
			};
			mainObj.pdg_ajax_handler(dynamicData, 'dynamicData', ajaxData).done(function(data){
				try {
					var $response_data = $.parseJSON( data ),
						$error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message;
					if ( ! $error ) {
						$.notify($message, 'success');
						settingsObj.modal_locality_updates.modal('hide');
						settingsObj.localityListDT.ajax.reload();
					} else {
						$.notify($message, 'error');
					}
				} catch ( err ) {
				} finally {
					button.button('reset');
				}
			});
		};
		
	}, localityListObj;
	
	localityListObj = new PdgLocalityList;
	localityListObj.init();
});


