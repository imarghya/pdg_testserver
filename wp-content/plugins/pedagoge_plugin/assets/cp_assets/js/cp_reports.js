$(document).ready(function() {
	fn_process_select_with_title();
	$("#select_pdg_report_type").change(function() {
		$(".cmd_generate_excfiles").prop('href', '#');
		var $report_val = $("#select_pdg_report_type option:selected").val();
		var $report_title = $("#select_pdg_report_type option:selected").text();
		
		var $report_title_area = $(".report_title_area");
		
		if($report_val.length<=0) {
			$report_title_area.html('Report Title');	
		} else {
			$report_title_area.html($report_title);
		}
		
		switch($report_val) {
			case 'locality_and_subject':
			case 'subjects_not_in_locality':
			case 'localities_not_having_subject':
				$("#div_locality_and_subject_list").show();
				fn_process_select_with_title();
				break;
			default:
				$("#div_locality_and_subject_list").hide();
				break;
		}		
	});
	
	$(".cmd_generate_reports").click(function(){
		var $button = $(this);
		var $report_type = $("#select_pdg_report_type option:selected").val();		
		var $report_result_area = $("#div_report_result_area");
		var $result_loader = $(".img_reports_loader");
		
		if( $report_type.length <= 0) {
			fn_alert_message($report_result_area, 'Error! Please select a report to generate...', 'error');
			return false;
		}
		
		var $subject_name_id = $("#select_subject_for_report").val();
		var $locality_name_id = $("#select_locality_for_report").val();
				
		$result_loader.show();
		$report_result_area.html('');
		$button.button('loading');
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,			
			pedagoge_callback_class : 'ControlPanelPedagogeReports',
			pedagoge_callback_function : 'fn_cp_reports_generator_ajax',
			report_type : $report_type,
			locality_name_id : $locality_name_id,
			subject_name_id : $subject_name_id
		};
		try {
			$.post($ajax_url, submit_data, function(response) {
				//$report_result_area.html(response);
				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $message = $response_data.message;
				var $data = $response_data.data;
				
				if($error) {
					fn_alert_message($report_result_area, $message, 'error');				
					
				} else {				
					$report_result_area.html($data);
					if ($response_data.hasOwnProperty('excel')) {
					    var $excel_url = $response_data.excel.url;
						$(".cmd_generate_excfiles").prop('href', $excel_url);
					}
					
				}				 
	        }).complete(function() {	        	
	        	$result_loader.hide();
	        	$button.button('reset');
	        });
	    } catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		    $.notify('Error Occured while trying to prepare the report.', 'error');
		    $button.button('reset');
		} finally{
			$button.button('reset');
		}
		
		
	});
	
	$("#div_report_result_area").on('click', '.cmd_generate_dated_reports', function(){
		var $button = $(this);
		var $report_type = $("#select_pdg_report_type option:selected").val();		
		var $report_display_area = $("#div_report_result_area");
		var $report_result_area = $report_display_area.find("#div_dated_report_result_area");
		var $from_date = $report_display_area.find("#txt_report_from_date").val();
		var $to_date = $report_display_area.find("#txt_report_to_date").val();
		
		
		if( $report_type.length <= 0) {
			fn_alert_message($report_result_area, 'Error! Please select a report to generate...', 'error');
			return false;
		}
		console.log($from_date.length);
		console.log($to_date.length);
		if($from_date.length <= 0 && $to_date.length <= 0) {
			console.log('test');
		} else {
			if( $from_date.length <= 0 || $to_date.length<=0) {
				fn_alert_message($report_result_area, 'Error! Please input date range to generate report...', 'error');
				return false;
			}
				
			// check if from date is valid or not
			if(!moment($from_date,"YYYY-MM-DD").isValid()) {
				fn_alert_message($report_result_area, 'Error! Please input from date in proper format (YYYY-MM-DD)...', 'error');
				return false;
			}
			
			
			// check if to date is valid or not
			if(!moment($to_date,"YYYY-MM-DD").isValid()) {
				fn_alert_message($report_result_area, 'Error! Please input to date in proper format (YYYY-MM-DD)...', 'error');
				return false;
			}
		}
				
		$report_result_area.html('');
		$button.button('loading');
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,			
			pedagoge_callback_class : 'ControlPanelPedagogeReports',
			pedagoge_callback_function : 'fn_cp_dated_reports_generator_ajax',
			report_type : $report_type,
			from_date : $from_date,
			to_date : $to_date
		};
		
		$.post($ajax_url, submit_data, function(response) {
			var $response_data = $.parseJSON(response);
			var $error = $response_data.error;
			var $message = $response_data.message;
			var $data = $response_data.data;
			
			if($error) {
				fn_alert_message($report_result_area, $message, 'error');				
				
			} else {				
				$report_display_area.html($data);
				if ($response_data.hasOwnProperty('excel')) {
				    var $excel_url = $response_data.excel.url;
					$(".cmd_generate_excfiles").prop('href', $excel_url);
				}
			}				 
        }).complete(function() {
        	$button.button('reset');
        }); 
	});
	
	
});

function fn_process_select_with_title() {
	$(".select_control_with_title").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
			allowClear: true,
		});
	});
	
	$(".select_control_with_title_multiple").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
			closeOnSelect: false
		});
	});
	
	$(".fltr_course, .select_course_update, .fltr_course_merge1, .fltr_course_merge2").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
			allowClear: true,
		});
	});	
	
}