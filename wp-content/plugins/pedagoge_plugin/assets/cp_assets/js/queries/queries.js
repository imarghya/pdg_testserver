$(document).ready(function(){

//console.log("ABC");
var timezone_offset_minutes = new Date().getTimezoneOffset();
timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
//console.log("TimeZone : "+timezone_offset_minutes);

var submit_data = {
nonce: $ajaxnonce,
action: $pedagoge_visitor_ajax_handler,
pedagoge_callback_function: 'assign_timezone',
pedagoge_callback_class: 'ControllerQueries',
timezone_offset_minutes: timezone_offset_minutes
};

$.post($ajax_url, submit_data)
  .done(function(response, status, jqxhr){ 
    //console.log(response);
  })
  .fail(function(jqxhr, status, error){ 
      // this is the ""error"" callback
  });

/*$.ajax({
    type: 'POST',
    url: '<?php echo get_site_url();?>/wp-content/plugins/pedagoge_plugin/time.php',
    data: { timezone_offset_minutes: timezone_offset_minutes },
    cache: false,
    dataType: 'JSON',
    success: function(response) { 
        //$("#current-timezone").text('Your browser\'s timezone is ' + response.timezone_name);
        //$("#get-timezone-name").remove();
    }
});*/

  if($("#header_query_ststus").val() != 'undefined')
  {
    var header_query_status=$("#header_query_ststus").val();
    var st_query_date=$("#query_st_date").val();
    var end_query_date=$("#query_end_date").val();
    var st_start_date=$("#follow_up_st_date").val();
    var end_start_date=$("#follow_up_end_date").val();
    var status=[];
    $.each($("input[name='query_status[]']:checked"), function(){            
            status.push($(this).val());
    });
    var flag_status=[];
    $.each($("input[name='flag_option[]']:checked"), function(){            
            flag_status.push($(this).val());
    });
    get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status);
  }
  
  setInterval(function(){
    get_admin_notification();
    get_admin_ajax_notification_count();
    get_finance_ajax_notification_count();
    get_sales_ajax_notification_count();
  },30000);

   $('#exp_date_payment').datepicker({showOn: 'button',buttonText:"<i class='fa fa-calendar'></i>",dateFormat: 'yy-mm-dd',minDate:0}).next(".ui-datepicker-trigger").addClass("exp_payment_date");
   //for show date picker-------------------------------------------
   //query date--
   $(document).on('focus', '#query_st_date', function(){
     $('#query_st_date').datepicker({ dateFormat: 'yy-mm-dd' }); 
   });
   $(document).on('focus', '#query_end_date', function(){
     $('#query_end_date').datepicker({ dateFormat: 'yy-mm-dd' }); 
   });
   
   $(document).on('focus', '.new_query_start_date', function(){
     $('.new_query_start_date').datepicker({ dateFormat: 'yy-mm-dd',minDate:0});
     $(this).prop('readonly', true); 
   });
   $(document).on('focusout', '.new_query_start_date', function(){
     //$('.new_query_start_date').datepicker({ dateFormat: 'yy-mm-dd',minDate:0});
     $(this).prop('readonly', false); 
   });
   //follow up date--
   $(document).on('focus', '#follow_up_st_date', function(){
     $('#follow_up_st_date').datepicker({ dateFormat: 'yy-mm-dd' }); 
   });
   $(document).on('focus', '#follow_up_end_date', function(){
     $('#follow_up_end_date').datepicker({ dateFormat: 'yy-mm-dd' }); 
   });
   
   $(document).on('focus', '#clone_query_date', function(){
     $('#clone_query_date').datepicker({ dateFormat: 'yy-mm-dd' }); 
   });
   
   $(document).on('focus', '#edit_query_date', function(){
     $('#edit_query_date').datepicker({ dateFormat: 'yy-mm-dd' }); 
   });
   
   /*$("#exp_date_payment_but").click(function() {
     $('#exp_date_payment').datepicker({ dateFormat: 'yy-mm-dd',minDate :0 });
     $("#exp_date_payment").datepicker("show");
   });
   $("#exp_date_payment").change(function(){
    $('#exp_date_payment').show();    
   })*/
   //$("html .date").datepicker({ dateFormat: 'yy-mm-dd' });
   //---------------------------------------------------------------
   //Show follow up date on double click----------------------------
    $(document).on('focus', '.follow_up_date', function(){
        $("#table_data .follow_up_date").datepicker({ dateFormat: 'yy-mm-dd' }).unbind('focus').dblclick(function() {
          $(this).datepicker('show');
         });
    });
  //------------------------------------------------------------
  //Show datable on qyery list----------------------------------
  //$('#table_query').dataTable( {
  //"ordering": false
  //});

  $(document).on("click",".show_notification",function(){
    var data_query_id = $(this).attr("data_id");
    var a_query_ids = data_query_id.split(',');

    $(".query_tr").each(function(index){
      $(".query_tr").removeClass("active_query");
    });
    $(".query_id").each(function(index){
      $(".query_id").prop("checked",false);
    });

    $.each(a_query_ids,function(index,value){
      //console.log(index+" => "+value);
      $("#query_tr_"+value).addClass("active_query");
      $("#query"+value).prop('checked',true);
    })

    //Query table top button eneable or disable function
      var co=$('input[name="query_id[]"]:checked').length;
      if (co > 0) {
       $("#query_approve_btn").removeClass("disabled");
       $("#query_disapprove_btn").removeClass("disabled");   
      }
      else if (co > 1) {
         $("#query_edit_btn").addClass("disabled");
         $("#query_clone_btn").addClass("disabled");
      }
      
      if(co == 1){
        $("#query_edit_btn").removeClass("disabled");
        $("#query_clone_btn").removeClass("disabled");
      }
      else{
       $("#query_edit_btn").addClass("disabled");
       $("#query_clone_btn").addClass("disabled");
      }
      if (co == 0) {
       $("#query_approve_btn").addClass("disabled");
       $("#query_disapprove_btn").addClass("disabled");    //code
      }
  })

  $(document).on("click",".receive_payment",function(){
    var data_query_id = $(this).attr("data_id");
    var a_query_ids = data_query_id.split(',');

    $(".query_tr").each(function(index){
      $(".query_tr").removeClass("active_query");
    });
    $(".query_id").each(function(index){
      $(".query_id").prop("checked",false);
    });
    
    $.each(a_query_ids,function(index,value){
      //console.log(index+" => "+value);
      $("#query_tr_"+value).addClass("active_query");
      $("#query"+value).prop('checked',true);
    })

    //Query table top button eneable or disable function
    var co=$('input[name="query_id[]"]:checked').length;
    if (co > 0) {
       $("#query_approve_btn").removeClass("disabled");
       $("#query_disapprove_btn").removeClass("disabled");   
    }
    else if (co > 1) {
         $("#query_edit_btn").addClass("disabled");
         $("#query_clone_btn").addClass("disabled");
    }
      
    if(co == 1){
        $("#query_edit_btn").removeClass("disabled");
        $("#query_clone_btn").removeClass("disabled");
    }
    else{
       $("#query_edit_btn").addClass("disabled");
       $("#query_clone_btn").addClass("disabled");
    }
    if (co == 0) {
       $("#query_approve_btn").addClass("disabled");
       $("#query_disapprove_btn").addClass("disabled");    //code
    }
  })

  $(document).on("click",".transfer_payment",function(){
    var data_query_id = $(this).attr("data_id");
    var a_query_ids = data_query_id.split(',');

    $(".query_tr").each(function(index){
      $(".query_tr").removeClass("active_query");
    });
    $(".query_id").each(function(index){
      $(".query_id").prop("checked",false);
    });

    $.each(a_query_ids,function(index,value){
      $("#query_tr_"+value).addClass("active_query");
      $("#query"+value).prop('checked',true);
    })

    //Query table top button eneable or disable function
      var co=$('input[name="query_id[]"]:checked').length;
      if (co > 0) {
       $("#query_approve_btn").removeClass("disabled");
       $("#query_disapprove_btn").removeClass("disabled");   
      }
      else if (co > 1) {
         $("#query_edit_btn").addClass("disabled");
         $("#query_clone_btn").addClass("disabled");
      }
      
      if(co == 1){
        $("#query_edit_btn").removeClass("disabled");
        $("#query_clone_btn").removeClass("disabled");
      }
      else{
       $("#query_edit_btn").addClass("disabled");
       $("#query_clone_btn").addClass("disabled");
      }
      if (co == 0) {
       $("#query_approve_btn").addClass("disabled");
       $("#query_disapprove_btn").addClass("disabled");    //code
      }
  })


  //------------------------------------------------------------
  //Checkbox function on query list table-----------------------
  $(document).on('click', '.query_id', function(){
        var selected = [];
        var status = $(this).attr("data-status");
        var h_status = $(this).attr("data-hstatus");
            if($(this).prop("checked") == true){
             $(this).closest('.query_tr').addClass("active_query");
             //selected.push($(this).val());
            }
            else if($(this).prop("checked") == false){
                $(this).closest('.query_tr').removeClass("active_query");
                $("#query_tr_"+$(this).val()).attr("data-wenk",'');
            }
            //Selected query ids------------------------------------
                $.each($("input[name='query_id[]']:checked"), function(){            
                selected.push($(this).val());
                
                 var text=$("#query_tr_"+$(this).val()).attr("data-ww");
                $("#query_tr_"+$(this).val()).attr("data-wenk",text);
                
                });
                $("#query_selected_ids").val(selected);
            //------------------------------------------------------
            //Query table top button eneable or disable function
            var co=$('input[name="query_id[]"]:checked').length;
            if (co > 0) {
              if(h_status == 1 || h_status == 2){
                  $("#query_approve_btn").addClass("disabled");
                  $("#query_disapprove_btn").removeClass("disabled");
                }
                else{
                  $("#query_approve_btn").removeClass("disabled");
                  $("#query_disapprove_btn").removeClass("disabled");
                }
             //$("#query_approve_btn").removeClass("disabled");
             //$("#query_disapprove_btn").removeClass("disabled");   
            }
            else if (co > 1) {
               $("#query_edit_btn").addClass("disabled");
               $("#query_clone_btn").addClass("disabled");
            }
            
            if(co == 1){
              $("#query_edit_btn").removeClass("disabled");
              $("#query_clone_btn").removeClass("disabled");

              if(status == "CLOSED"){
                $("#query_clone_btn").removeClass("disabled");
                $("#query_edit_btn").addClass("disabled");
                $("#query_approve_btn").addClass("disabled");
                $("#query_disapprove_btn").addClass("disabled");
              }
            }
            else{
             $("#query_edit_btn").addClass("disabled");
             $("#query_clone_btn").addClass("disabled");
            }
            if (co == 0) {
             $("#query_approve_btn").addClass("disabled");
             $("#query_disapprove_btn").addClass("disabled");    //code
            }
  });
  //------------------------------------------------------------
  //For close get id--------------------
   $(document).on('click', '.roc_assign', function(){
     var ids=$("#query_selected_ids").val();
     var selected = ids.split(",");
     var query_id=$(this).attr("q-id");
     var txt=$(this).val();
     $("#reasonCloser").val(txt);
     selected.push(query_id);
     $("#query_selected_ids").val(selected);
   });
  //For query assign to sales rep-------------------------------
  $(document).on('change', '#sales_rep', function(){
    header_query_status=$("#header_query_ststus").val();
    var sales_rep_name=$("#sales_rep option:selected").text();
    var sales_rep_id=$(this).val();
    var query_ids=$("#query_selected_ids").val();
    if (query_ids==0) {
        $('#sales_rep').val(''); 
        alertify.error("Error: Please select at list one query!");
    }
    else if (header_query_status==0) {
     alertify.error("Error: This query is not approved!!");  
    }
    else{
      var result = confirm("Are you sure?");
      if (result) {
        var ids=query_ids.split(",");
        var count=ids.length;
         $.each(ids, function( index, query_id ) {
            // var text=count+' Querries assigned to '+sales_rep_name;
             //var text=$("#query_tr_"+query_id).attr("data-ww");
             //$("#query_tr_"+query_id).attr("data-wenk",text);
             $("#query_tr_"+query_id+" .follow_up_date").attr("sales-rep",sales_rep_id);
             $("#sales_rep_name_"+query_id).text(sales_rep_name);
             $("#query_tr_"+query_id+" .query_ststus").val("HOT");
         });
         //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'assign_sales_rep',
        pedagoge_callback_class: 'ControllerQueries',
        query_ids: query_ids,
        sales_rep_id: sales_rep_id,
        
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){ 
               alertify.success("Success: Sales Rep successfully assigned.");
               $('#sales_rep').prop('selectedIndex',0);
               get_filter_queries('','','','','','',2);
               $("#header_query_ststus").val(2);
            })
            .fail(function(jqxhr, status, error){ 
                // this is the ""error"" callback
            });
         //--------------------------------------------------------
         
        }
        else{
         $('#sales_rep').val('');       
        }
     }
  });
  //---------------------------------------------------------------
  
  //Change Query Status-----------------------

  $(document).on('change', '.query_ststus', function(){
      var query_id=$(this).attr("query-id");
      var current_status=$(this).val();
      var result = confirm("Are you sure?");
      if (result) {
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'change_query_status',
        pedagoge_callback_class: 'ControllerQueries',
        query_id: query_id,
        status: current_status,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){ 
               alertify.success("Success: Status Changed Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error)
            });
         //--------------------------------------------------------
      }
      else{
        /*if (current_ststus==1) {
         $(this).val(0);       
        }
        else{
          $(this).val(1);       
        }*/
      }
   });


  //Change FastPass Status-----------------------------------------
  $(document).on('change', '.fast_pass', function(){
      var query_id=$(this).attr("query-id");
      var current_ststus=$(this).val();
      var result = confirm("Are you sure?");
      if (result) {
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'change_fast_pass_status',
        pedagoge_callback_class: 'ControllerQueries',
        query_id: query_id,
        status: current_ststus,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){ 
               alertify.success("Success: Status Changed Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error)
            });
         //--------------------------------------------------------
      }
      else{
        if (current_ststus==1) {
         $(this).val(0);       
        }
        else{
          $(this).val(1);       
        }
      }
   });
  //---------------------------------------------------------------
  //Change follow up date------------------------------------------
   $(document).on('change', '.follow_up_date', function(){
    var query_id=$(this).attr("query-id");
    var sales_rep_id=$(this).attr("sales-rep");
    var current_date=$(this).attr("current-date");
    var follow_up_date=$(this).val();
    if (sales_rep_id!=0) {
      //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'change_follow_up_date',
        pedagoge_callback_class: 'ControllerQueries',
        query_id: query_id,
        follow_up_date: follow_up_date,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){ 
               alertify.success("Success: Follow Up Date Changed Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error)
            });
         //--------------------------------------------------------   
    }
    else{
      $(this).val(current_date);
      alertify.error("Error: Sales Rep Not Assigned!.");  
    }
   });
  //---------------------------------------------------------------
  //Change Review Status-------------------------------------------
  $(document).on('change', '.review', function(){
      var query_id=$(this).attr("query-id");
      var current_ststus=$(this).val();
      var result = confirm("Are you sure?");
      if (result) {
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'change_review_status',
        pedagoge_callback_class: 'ControllerQueries',
        query_id: query_id,
        status: current_ststus,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){ 
               alertify.success("Success: Review Changed Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error)
            });
         //--------------------------------------------------------
      }
      else{
        if (current_ststus==1) {
         $(this).val(0);       
        }
        else{
          $(this).val(1);       
        }
      }
   });
  //---------------------------------------------------------------
  //Approve Query--------------------------------------------------
  $(document).on('click', '#query_approve', function(){
    var query_ids=$("#query_selected_ids").val();
    if (query_ids==0) {
        $('#sales_rep').val(''); 
        alertify.error("Error: Please select at list one query!");
    }
    else{
      var result = confirm("Are you sure?");
      if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_approve',
        pedagoge_callback_class: 'ControllerQueries',
        query_ids: query_ids,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
               alertify.success("Success: Query Approved.");
               var header_query_status=$("#header_query_ststus").val();
                get_filter_queries('','','','','','',header_query_status);
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //-------------------------------------------------------- 
      }
      else{
        // $('#sales_rep').val('');       
       }
    }
  });
  //---------------------------------------------------------------
  //Disapprove Query-----------------------------------------------
  $(document).on('submit', '#form_query_disapprove', function(event){
  //$("#form_query_disapprove").submit(function(event){
   event.preventDefault();
    var query_ids=$("#query_selected_ids").val();
    var result = confirm("Are you sure?");
      if (result) {
        $message=$("#query_disapprove_message").val();
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_disapprove',
        pedagoge_callback_class: 'ControllerQueries',
        query_ids: query_ids,
        message: $message,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                $("#modal_query_disapprove").modal('hide');
                $("#query_disapprove_message").val('');
                var header_query_status=$("#header_query_ststus").val();
                get_filter_queries('','','','','','',header_query_status);
                alertify.success("Success: Query Disapproved.");
            })
            .fail(function(jqxhr, status, error){ 
                // this is the ""error"" callback
            });
         //--------------------------------------------------------
      }
  });
  //---------------------------------------------------------------
  //Close Query-----------------------------------------------
  $(document).on('submit', '#form_roc', function(event){
  //$("#form_query_disapprove").submit(function(event){
   event.preventDefault();
    var query_ids=$("#query_selected_ids").val();
    var result = confirm("Are you sure?");
      if (result) {
        $message=$("#reasonCloser").val();
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_close',
        pedagoge_callback_class: 'ControllerQueries',
        query_ids: query_ids,
        message: $message,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                $("#roc").modal('hide');
                var header_query_status=$("#header_query_ststus").val();
                get_filter_queries('','','','','','',header_query_status);
                alertify.success("Success: Query Closed.");
                 $("#reasonCloser").val('');
            })
            .fail(function(jqxhr, status, error){ 
                // this is the ""error"" callback
            });
         //--------------------------------------------------------
      }
  });
  //---------------------------------------------------------------
  //click on cancel button-----------------------------------------
  $(document).on('click', '.fl_cancel', function(){
    $("html").trigger("click");   //for close date filter popup 
  });
  //---------------------------------------------------------------
  //Query filter by query date-------------------------------------
  $(document).on('click', '#filter_query_date_btn', function(){
     var st_query_date=$("#query_st_date").val();
     var end_query_date=$("#query_end_date").val();
     var st_start_date=$("#follow_up_st_date").val();
     var end_start_date=$("#follow_up_end_date").val();
     var status=[];
        $.each($("input[name='query_status[]']:checked"), function(){            
                      status.push($(this).val());
        });
        var flag_status=[];
        $.each($("input[name='flag_option[]']:checked"), function(){            
                flag_status.push($(this).val());
        });
     var header_query_status=$('#header_query_ststus').val();
     get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status);
  });
  //---------------------------------------------------------------
  //Query filter by follow up date-------------------------------------
  $(document).on('click', '#filter_followup_date_btn', function(){
     var st_query_date=$("#query_st_date").val();
     var end_query_date=$("#query_end_date").val();
     var st_start_date=$("#follow_up_st_date").val();
     var end_start_date=$("#follow_up_end_date").val();
     var status=[];
        $.each($("input[name='query_status[]']:checked"), function(){            
                      status.push($(this).val());
        });
        var flag_status=[];
        $.each($("input[name='flag_option[]']:checked"), function(){            
                flag_status.push($(this).val());
        });
     var header_query_status=$('#header_query_ststus').val();
     get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status);
  });
  //---------------------------------------------------------------
  //Query Filter on select status----------------------------------
  $(document).on('click', '.fl_query_status', function(){
     var st_query_date=$("#query_st_date").val();
     var end_query_date=$("#query_end_date").val();
     var st_start_date=$("#follow_up_st_date").val();
     var end_start_date=$("#follow_up_end_date").val();
        var status=[];
        $.each($("input[name='query_status[]']:checked"), function(){            
                status.push($(this).val());
        });
        var flag_status=[];
        $.each($("input[name='flag_option[]']:checked"), function(){            
                flag_status.push($(this).val());
        });
       // alert(status);
       var header_query_status=$('#header_query_ststus').val();
        get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status);
  });
  //---------------------------------------------------------------
  //Query Filter on select flag status----------------------------------
  $(document).on('click', '.fl_flag_status', function(){
     var st_query_date=$("#query_st_date").val();
     var end_query_date=$("#query_end_date").val();
     var st_start_date=$("#follow_up_st_date").val();
     var end_start_date=$("#follow_up_end_date").val();
        var status=[];
        $.each($("input[name='query_status[]']:checked"), function(){            
                status.push($(this).val());
        });
        var flag_status=[];
        $.each($("input[name='flag_option[]']:checked"), function(){            
                flag_status.push($(this).val());
        });
       var header_query_status=$('#header_query_ststus').val();
        get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status);
  });
  //---------------------------------------------------------------

  //Query Filter on select Lead Source----------------------------------
  $(document).on('click', '.fl_query_lead', function(){
    var st_query_date=$("#query_st_date").val();
    var end_query_date=$("#query_end_date").val();
    var st_start_date=$("#follow_up_st_date").val();
    var end_start_date=$("#follow_up_end_date").val();

    var lead_source=[];
    $.each($("input[name='query_lead[]']:checked"), function(){            
      lead_source.push($(this).val());
    });
    //console.log(lead_source);
    var status=[];
    $.each($("input[name='query_status[]']:checked"), function(){            
      status.push($(this).val());
    });
    var flag_status=[];
    $.each($("input[name='flag_option[]']:checked"), function(){            
      flag_status.push($(this).val());
    });
    var header_query_status=$('#header_query_ststus').val();
    get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status,lead_source);
  });

 //Header Query Status Dropdown filter---------------------------------------------------
 $(document).on('change', '#header_query_ststus', function(){
     var header_query_status=$(this).val();
     var st_query_date=$("#query_st_date").val();
     var end_query_date=$("#query_end_date").val();
     var st_start_date=$("#follow_up_st_date").val();
     var end_start_date=$("#follow_up_end_date").val();
        var status=[];
        $.each($("input[name='query_status[]']:checked"), function(){            
                status.push($(this).val());
        });
        var flag_status=[];
        $.each($("input[name='flag_option[]']:checked"), function(){            
                flag_status.push($(this).val());
        });
      get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,status,flag_status,header_query_status);
 });
 //----------------------------------------------------------------
 //Query Clone function--------------------------------------------
 //get query details---------------
 $("#query_clone").click(function(){
    var query_id=$("#query_selected_ids").val();
    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_query_status_byid',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                if ($.trim(response)=='CLOSED') {
                 //Get query details ajax---------------
                       var submit_data2 = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'get_clone_query_details',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id: query_id,
                        };
                         
                          $.post($ajax_url, submit_data2)
                            .done(function(query_response, status, jqxhr){
                               // alert(query_response);
                              $("#clone_query_data").html(query_response);
                              $("#clone_query_modal").modal("show");  
                            })
                            .fail(function(jqxhr, status, error){ 
                                // this is the ""error"" callback
                            });
                 //-------------------------------------
                 
                }
                else{
                 //alert("At first close the query or edit the query!");
                 alertify.log("Alert: At first close the query or edit the query!");
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
            //-------------------------------------------
            //Submit Clone Query-------------------------
            $(document).on('submit', '#clone_query_form', function(event){
                event.preventDefault();
                var query_id=$("#active_query_id").val();
                if($("#edit_my_home").prop("checked") == true){
                var myhome=1;
                }
                else{
                  var myhome=0     
                }
                
               if($("#edit_t_home").prop("checked") == true){ 
                var tutorhome=1;
               }
               else{
               var tutorhome=0;
               }
               
               if($("#edit_institute").prop("checked") == true){ 
                var institute=1;
               }
               else{
                var institute=0;       
               }
                //call ajax-----------------------------------------------
                        var submit_data = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'submit_clone_query',
                        pedagoge_callback_class: 'ControllerQueries',
                        clone_student_id: $("#clone_student_id").val(),
                        clone_subject : $("#clone_subject").val(),
                        clone_school : $("#clone_school").val(),
                        clone_school_type : $("#clone_school option:selected").attr("data-type"),
                        clone_board : $("#clone_board").val(),
                        clone_board_type : $("#clone_board option:selected").attr("data-type"),
                        clone_standard : $("#clone_standard").val(),
                        age : $("#clone_age").val(),
                        clone_my_home : myhome,
                        clone_t_home : tutorhome,
                        clone_institute : institute,
                        clone_locality : $("#clone_locality").val(),
                        clone_query_date : $("#clone_query_date").val(),
                        clone_requirement : $("#clone_requirement").val(),
                        clone_student_name : $("#clone_student_name").val(),
                        clone_phone_number : $("#clone_phone_number").val(),
                        clone_user_email : $("#clone_user_email").val(),
                        };
                        
                          $.post($ajax_url, submit_data)
                            .done(function(response, status, jqxhr){
                                $("#clone_query_modal").modal("hide");
                                get_filter_queries('','','','','','',0);
                                alertify.success("Success: Query Clone Success");
                            })
                            .fail(function(jqxhr, status, error){ 
                               //alert(error);
                            });
                //--------------------------------------------------------
            });
            //-------------------------------------------
         //--------------------------------------------------------
 });
 //----------------------------------------------------------------
 //Get Standard Year on change clone subject-----------------------
 $(document).on('change', '#clone_subject', function(){
 var subject_id=$(this).val();
 //alert(subject_id);
 var q_from_id='clone_standard';
 get_subject_type(subject_id,q_from_id);
 });
 //----------------------------------------------------------------
 
 //Query Edit function---------------------------------------------
 //get query details---------------
 $("#query_edit").click(function(){
    var query_id=$("#query_selected_ids").val();
    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_query_status_byid',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                if ($.trim(response)!='CLOSED') {
                 //Get query details ajax---------------
                       var submit_data2 = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'get_edit_query_details',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id: query_id,
                        };
                         
                          $.post($ajax_url, submit_data2)
                            .done(function(query_response, status, jqxhr){
                               // alert(query_response);
                              $("#edit_query_data").html(query_response);
                              $("#edit_query_modal").modal("show");  
                            })
                            .fail(function(jqxhr, status, error){ 
                                // this is the ""error"" callback
                            });
                 //-------------------------------------
                 
                }
                else{
                 alertify.log("Alert: Query is not active!");
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
            //-------------------------------------------
     });
 //----------------------------------------------------------------
          //Submit Edit Query-------------------------
            $(document).on('submit', '#edit_query_form', function(event){
                event.preventDefault();
                 var query_id=$("#query_selected_ids").val();
                  if($("#edit_my_home").prop("checked") == true){
                var myhome=1;
                }
                else{
                  var myhome=0     
                }
                
               if($("#edit_t_home").prop("checked") == true){ 
                var tutorhome=1;
               }
               else{
               var tutorhome=0;
               }
               
               if($("#edit_institute").prop("checked") == true){ 
                var institute=1;
               }
               else{
                var institute=0;       
               }
                //call ajax-----------------------------------------------
                        var submit_data = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'submit_edit_query',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id : query_id,
                        clone_student_id: $("#edit_student_id").val(),
                        clone_subject : $("#edit_subject").val(),
                        clone_school : $("#edit_school").val(),
                        clone_school_type : $("#edit_school option:selected").attr("data-type"),
                        clone_board : $("#edit_board").val(),
                        clone_board_type : $("#edit_board option:selected").attr("data-type"),
                        clone_standard : $("#edit_standard").val(),
                        age : $("#edit_age").val(),
                        clone_my_home : myhome,
                        clone_t_home : tutorhome,
                        clone_institute : institute,
                        clone_locality : $("#edit_locality").val(),
                        clone_query_date : $("#edit_query_date").val(),
                        clone_requirement : $("#edit_requirement").val(),
                        clone_student_name : $("#edit_student_name").val(),
                        clone_phone_number : $("#edit_phone_number").val(),
                        clone_user_email : $("#edit_user_email").val(),
                        };
                        
                          $.post($ajax_url, submit_data)
                            .done(function(response, status, jqxhr){
                                $("#edit_query_modal").modal("hide");
                                var header_query_status=$("#header_query_ststus").val();
                                get_filter_queries('','','','','','',header_query_status);
                                alertify.success("Success: Query Edit Success");
                            })
                            .fail(function(jqxhr, status, error){ 
                               //alert(error);
                            });
                //--------------------------------------------------------
            });
            //------------------------------------------------------------
  //Get Standard Year on change edit subject-----------------------
 $(document).on('change', '#edit_subject', function(){
 var subject_id=$(this).val();
 //alert(subject_id);
 var q_from_id='edit_standard';
 get_subject_type(subject_id,q_from_id);
 });
 //----------------------------------------------------------------
 
 //Function for new query------------------------------------------
 $("#subject_frm_button").click(function(){
    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_subject_from',
        pedagoge_callback_class: 'ControllerQueries',
        subjects: $("#mySingleField").val(),
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             $("#new_query_modal_item").html(response);
             $("#subjectSelect1").modal("show");
             $(".admin_mlc").fSelect();
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------    
 });
 //for submit each from--------
 //$(document).on('click', '.qdata_submit', function(e){
 //e.preventDefault();
 //var x=$(this).attr("idd");
 //var subject=$(this).attr("subject");
 $(document).on("change",".admin_mlc",function(){
  var loc_id = $(this).attr('id');
  $("#hid_"+loc_id).val( $(this).val());
 })
$(document).on("change",".prefer_location",function(){
  var prefer_id = $(this).attr('data-tt');
  if($(this).is(':checked')){
    $("#prefer_location_"+prefer_id).val( $(this).val());
  }
  else
  {
    $("#prefer_location_"+prefer_id).val('');
  }
});
$(document).on('submit','.subject_form',function(e){
e.preventDefault();
totalx=$(this).attr("totalx");
var x=$(this).attr("idd");
var subject=$(this).attr("subject");
 var age=$("#age_"+x).val();
 var school=$("#school_"+x).val();
 var school_type = $("#school_"+x+" option:selected").attr("data-type");
 var board=$("#board_"+x).val();
 var board_type = $("#board_"+x+" option:selected").attr("data-type");
 var standard=$("#standard_"+x).val();
 
 if($("#myhome_"+x).prop("checked") == true){
 var myhome=1;
 }
 else{
   var myhome=0     
 }
 
if($("#tutorhome_"+x).prop("checked") == true){ 
 var tutorhome=1;
}
else{
var tutorhome=0;
}

if($("#institute_"+x).prop("checked") == true){ 
 var institute=1;
}
else{
 var institute=0;       
}
 var localities=$("#localities_"+x).val();
 var start_date=$("#start_date_"+x).val();
 var requirement=$("#requirement_"+x).val();
 //alert(localities);
 //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'submit_q_subject_data',
        pedagoge_callback_class: 'ControllerQueries',
        x : x,
        subject : subject,
        age : age,
        school : school,
        school_type : school_type,
        board : board,
        board_type : board_type,
        standard : standard,
        myhome : myhome,
        tutorhome : tutorhome,
        institute : institute,
        localities : localities,
        start_date : start_date,
        requirement : requirement,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             var xx=(parseInt(x) +1);
             $("#subjectSelect"+x).modal("hide");
             $("#subjectSelect"+xx).modal("show");
             alertify.success("Submited...");
             
                if (totalx==x) {
                 $("#subjectSelect"+x).modal("hide");
                 $("#nameaddress").modal("show");
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------
       
  });
 //});
 //---------------------------------------------------------------
 
 //For Same as Preview--Data
 $(document).on('click', '.same_as_prv', function(e){
 var xx =$(this).attr("idd");
 var x=(parseInt(xx) +1);
 //alert(xx);
 if($(this).prop("checked") == true){
        //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_q_subject_data',
        pedagoge_callback_class: 'ControllerQueries',
        x : xx,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
            var str=response.split("|");
             //alert(str);
                $("age_"+x).val(str[0]);
                $("#school_"+x).val(str[1]);
                $("#board_"+x).val(str[2]);
                $("#standard_"+x).val(str[3]);
                if($.trim(str[4])==1) {
                 $('#myhome_'+x).prop('checked', true);
                 $("#prefer_location_"+x).val($.trim(str[4]));
                }
                if($.trim(str[5])==1) {
                 $('#tutorhome_'+x).prop('checked', true);
                 $("#prefer_location_"+x).val($.trim(str[5]));
                }
                
                if($.trim(str[6])==1) {
                 $('#institute_'+x).prop('checked', true);
                 $("#prefer_location_"+x).val($.trim(str[6]));
                }
                
                //var loc=str[7].split(",");
                //$("#localities_"+x).val(loc);

                /********************** Location Select ***********************/
              var loc=str[7].split(",");
              //console.log(loc);
              //$("#localities_"+x).val(loc);
                var selectedvalues = [];
              $.each(str[7].split(","), function(i,e){
                 $("#localities_"+x+" option[value='" + e + "']").prop("selected", true);

                 selectedvalues.push($("#localities_"+x+" option[value='"+e+"']").text());
              });
              //console.log(selectedvalues);
              var selectedvalue = selectedvalues.join(",");
              $("#hid_localities_"+x).val(selectedvalue);
              //$("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("Select Localities");
              //$("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("");
              $("#localities_"+x).parent('.fs-wrap').find(".fs-option").each(function(i,e) {
                $.each(str[7].split(","), function(i1,e){
                if($("#localities_"+x).parent('.fs-wrap').find(".fs-option").eq(i).attr("data-value") == e)
                {
                  $("#localities_"+x).parent('.fs-wrap').find(".fs-option").eq(i).addClass("selected");
                }
                });
              });

              $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("");
              //console.log(selectedvalue);
              $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text(selectedvalue);
                /********************** Location Select ***********************/

                
                $("#start_date_"+x).val(str[8]);
                $("#requirement_"+x).val(str[9]);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------
    }
    //else if($(this).prop("checked") == false){
      else{
                $("age_"+x).val('');
                $("#school_"+x).val('');
                $("#board_"+x).val('');
                $("#standard_"+x).val('');
                $('#myhome_'+x).prop('checked', false);
                $('#tutorhome_'+x).prop('checked', false);
                $('#institute_'+x).prop('checked', false);
                //$("#localities_"+x).val('');
                $("#start_date_"+x).val('');
                $("#requirement_"+x).val('');

                $("#localities_"+x+" option:selected").removeAttr("selected");
                $("#localities_"+x).parent('.fs-wrap').find(".fs-option").removeClass("selected");
                $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("");
                $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("Select Localities");

                $("#prefer_location_"+x).val('');
                $("#hid_localities_"+x).val('');
    }
 });
 //--------------------------------------------------------------
 //For Submit New Query------------------------------------------
 $(document).on('submit','#frmnameemail',function(e){
 //$(document).on('click', '#submit_all_new_query', function(e){
  e.preventDefault();
  var x=$("#x").val();
  var name=$("#studname").val();
  var email=$("#emailid").val();
  var phone=$("#phonenum").val();
  //alert(x);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'submit_new_query_data',
        pedagoge_callback_class: 'ControllerQueries',
        x : x,
        name : name,
        email : email,
        phone : phone,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             if(response == "fail"){
              alert("Please enter a register user details...");
             }
             else{
              $("#nameaddress").modal("hide");
              alertify.success("Success: Query Added Successfully.");
              get_filter_queries('','','','','','',0);
              }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------
 });
 //----------------------------------------------------------------
 //----------------------------------------------------------------
 //Get Teacher Responses in query view page------------------------
 $(document).on('click','.fees_modal',function(e){
  e.preventDefault();
  var intid=$(this).attr("intid");
  var invid=$(this).attr("invt");
  var ti_id=$(this).attr("ti-id");
  var type=$(this).attr("type");
  var role=$(this).attr("role");
  //alert(x);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_teacher_responses',
        pedagoge_callback_class: 'ControllerQueries',
        intid : intid,
        invid : invid,
        ti_id : ti_id,
        type : type,
        role : role,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             $("#fees_content").html(response);
             $("#fees").modal("show");
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------
 });
 //----------------------------------------------------------------
 //Responses Approve-----------------------------------------------
 $(document).on('click','#respons_approve',function(e){
     var query_id = $("#queryid").val();
    //console.log(query_id);
    //return false;
    //alert(query_id);
    var id=$(this).attr("idd");
    var type=$(this).attr("ttype");
    var query_id = $(this).attr("q_id");
    var email = $("#fees_email_id").val();
    var teacher_id = $(this).attr("teacher_id");
    var student_id = $(this).attr("student_id");
    //$(this).val("Approved");
    //$(this).attr("disabled",true);
    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'respons_approve',
        pedagoge_callback_class: 'ControllerQueries',
        id : id,
        type : type,
        email : email,
        query_id : query_id,
        teacher_id : teacher_id,
        student_id : student_id
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
            // alert(response);
            alertify.success("Success: Response Approved.");
             //$("#respid_"+id).remove();
             $("#fees").modal("hide");
             
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------
 });
 
 
 //------------- Remove Unapprove Teacher Query Response --------------------//
 function remove_unapprove_teacher(query_id,role){

  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'respons_disapprove_teacher',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        role : role
        };
        
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
            $(".query_response_teacher").html(response);
            //alert(response);
          })
          .fail(function(jqxhr, status, error){ 
             //alert(error);
          });

 }
 
 //------------- Remove Unapprove Match Teacher --------------------//
 function remove_unapprove_match(query_id,student_id){

  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'respons_disapprove_match_teacher',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        student_id : student_id
        };
        
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
            //alert(response);
            //$(".match_response").html("");
            $(".match_response").html(response);
            
          })
          .fail(function(jqxhr, status, error){ 
             //alert(error);
          });
 }
 
 //----------------------------------------------------------------
 //Respnses Disapprove---------------------------------------------
  $(document).on('click','#respons_disapprove',function(e){
    var id=$(this).attr("idd");
    var type=$(this).attr("ttype");
    var email = $("#fees_email_id").val();
    var teacher_id = $(this).attr("teacher_id");
    var student_id = $(this).attr("student_id");
    var role = $(this).attr('role');
    var query_id = $("#queryid").val();
    $("#idd").val(id);
    $("#ttype").val(type);
    $("#res_email").val(email);
    $("#teacher_id").val(teacher_id);
    $("#student_id").val(student_id);
    $("#role").val(role);
    //$("#query_id").val(query_id);
    $("#fees").modal("hide");
    $("#response_disapprove_modal").modal("show");
    
   });
 //----------------------------------------------------------------
 //Submit Disapprove Responses-------------------------------------
 $(document).on('click','#subject_frm_rsp_disapprove',function(e){
    var id=$("#idd").val();
    var type=$("#ttype").val();
    var email = $("#res_email").val();
    var message = $("#res_disapprove_message").val();
    var query_id = $("#query_id").val();
    var teacher_id = $("#teacher_id").val();
    var student_id = $("#student_id").val();
    var role = $("#role").val();
    var result = confirm("Are you sure?");
    if(result){
      //alert($ajax_url);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'respons_disapprove',
        pedagoge_callback_class: 'ControllerQueries',
        id : id,
        type : type,
        email : email,
        message : message,
        query_id :query_id,
        teacher_id : teacher_id,
        student_id : student_id
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
            // alert(response);
            remove_unapprove_match(query_id,student_id);
            remove_unapprove_teacher(query_id,role);
            alertify.success("Success: Response Disapproved.");
             //$("#respid_"+id).remove();
             $("#response_disapprove_modal").modal("hide");
             
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
     }
   //--------------------------------------------------------      
 });
 //----------------------------------------------------------------
 //Show Rating popup for recomended and matched teacher------------
 $(document).on('click','.get_rating',function(e){
   e.preventDefault();
   var ti_id=$(this).attr("ti-id");
   var type=$(this).attr("type");
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_rating_popup',
        pedagoge_callback_class: 'ControllerQueries',
        ti_id : ti_id,
        type : type,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             $("#tech_rating").html(response);
             $("#get_ratings_popup").modal("show"); 
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------      
 });
 //----------------------------------------------------------------
 //Finalized Teacher-----------------------------------------------
 $(document).on('click','#final_teacher',function(e){
  e.preventDefault();
  var query_id=$("#view_query_id").val();
  var teacher_id=$("#f_teacher_id").val();
  var student_id=$("#f_student_id").val();
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'finalazie_teacher',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        teacher_id : teacher_id,
        student_id : student_id,
        };
        //alert(query_id+"  "+teacher_id+"  "+student_id);
        //return false;
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             if (response=='NotExist') {
                alertify.error("Error: Teacher or Institute Not Exist!");
             }
             else{
                $(".hide_profile").hide();
                $(".show_profile").show();
                $("#finalized_teacher_content").html(response);
                $("#inb_teacher_id").val(teacher_id);
               alertify.success("Success: Teacher or Institute Finalized."); 
             }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------  
 });
 //----------------------------------------------------------------
 //Change Finalize Teacher-----------------------------------------
 $(document).on('click','#change_final_teacher',function(e){
  e.preventDefault();
  $(".hide_profile").show();
  $(".show_profile").hide();
 });
 //----------------------------------------------------------------
 //Start Inbound Payment function----------------------------------
 
 $('.inbound_payment_type').click(function () {
  var $this = $(this);
   $('.inbound_payment_type').removeClass('active');
   if ($this.hasClass('active')) {
      $this.removeClass('active').addClass('inactive')
   } else {
      $this.removeClass('inactive').addClass('active');
      $("#inbound_payment_type").val($this.val());
  }
  });
 //-----------
 $(".inbound_fees").click(function(){
   var $this = $(this);
   $("#inbound_payment_fee").val($this.val());     
 });
 //submit inbound form--------
  $(document).on('submit','#inbound_form',function(e){
  e.preventDefault();
  var query_id=$("#view_query_id").val();
  var teacher_id=$("#inb_teacher_id").val();
  var student_id=$("#inb_student_id").val();
  //alert(query_id+"   "+teacher_id+"    "+student_id);
  //return false;
  if (teacher_id=='') {
      alertify.error("Error: At first finalize the teacher!");
  }
  else{
     var cash_trans=0;
     var inbound_payment_type=$("#inbound_payment_type").val();
     var inbound_payment_fee=$("#inbound_payment_fee").val();
     var exp_date_payment=$("#exp_date_payment").val();
     var fees=$("#payment_fees").val();
     var inbound_payment_cycle=$("#inbound_payment_cycle").val();
     var vendor_invoice_number=$("#vendor_invoice_number").val();
     var payment_type_details='';
     if (inbound_payment_type==1) {
      payment_type_details=$("#inbound_cash").val();
     }
     else if (inbound_payment_type==2) {
       payment_type_details=$("#inbound_pytm").val();
     }
     else if (inbound_payment_type==3) {
       payment_type_details=$("#inbound_bank").val();
     }
     else{
      payment_type_details=$("#inbound_card").val();
     }
     
     if($('#cash_trans').prop("checked") == true){
      cash_trans=1;
     }
     else if($('#cash_trans').prop("checked") == false){
      cash_trans=0;
     }
      
     if(!payment_type_details.trim())
     {
      alertify.error("Error: Please type the payment details !!!");
      return false;
     }

    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_inbound_payment',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        teacher_id : teacher_id,
        student_id : student_id,
        inbound_payment_type : inbound_payment_type,
        payment_type_details : payment_type_details,
        inbound_payment_fee : inbound_payment_fee,
        exp_date_payment : exp_date_payment,
        fees : fees,
        inbound_payment_cycle : inbound_payment_cycle,
        vendor_invoice_number : vendor_invoice_number,
        cash_trans : cash_trans,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             $("#show_update_on_date").text(response);
             alertify.success("Success: Inbound Payment Updated."); 
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------       
  }
  });
 //----------------------------------------------------------------
 //----------------------------------------------------------------
 //Outbound Payment------------------------------------------------
 $('.outbound_payment_type').click(function () {
  var $this = $(this);
   $('.outbound_payment_type').removeClass('active');
   if ($this.hasClass('active')) {
      $this.removeClass('active').addClass('inactive')
   } else {
      $this.removeClass('inactive').addClass('active');
      $("#outbound_payment_type").val($this.val());
  }
  });
 //Add bank dynamic----------------------------------------------
 $("#add_bank").click(function(){
  var count =+$("#bank_count").val();
  count=count+1;
  var html='<div class="border_radius_12 blue_color blue_border distributor_name" id="bul'+count+'"><ul>'+
                '<li><label>Bank Name:</label> <input type="text" name="bank_name'+count+'" id="bank_name"></li>'+
                '<li><label>Account holders  Name:</label> <input type="text" name="acc_holder_name'+count+'" id="acc_holder_name"></li>'+
                '<li><label>Account Number:</label> <input type="text" name="acc_number'+count+'" id="acc_number"></li>'+
                '<li><label>IFSC Code:</label> <input type="text" name="ifsc_code'+count+'" id="ifsc_code"></li>'+
                '<li><button idd="'+count+'" type="button" class="admin_blue_background border_none border_radius_30 color_white bank_delete">Delete</button></li>'+
            '</ul></div>';
    $("#bank").append(html);
    $("#bank_count").val(count);     
    //alert(count);
 });
 //---------------------------------
 //delete bank----------
 $(document).on('click','.bank_delete',function(event){
 event.preventDefault();
 var idd=$(this).attr("idd");
 $('#bul'+idd).remove();
 });
 //--------------------
 //update paytm mobile number-------------------------------
 $("#paytm_no").keypress(function(e){
    if(e.which == 13) {
        e.preventDefault();
        var num=$(this).val();
        var htm='<p class="margin_bottom_10">'+num+'<button type="button" class="border_none blue_color"><i id="paytm_delete" class="fa fa-trash" aria-hidden="true"></i></button></p>';
        $("#mob_span").html(htm);
        $("#paytm_mob_no").val(num);
        $(this).val('');
    }     
 });
 //---------------------------------------------------------
 //Delete Paytm no------------------------------------------
 $(document).on('click','#paytm_delete',function(e){
 $("#mob_span").html('');
 $("#paytm_mob_no").val('');
 });
 //---------------------------------------------------------
 //submit outbound form-------------------------------------
 $('#outbound_form2').submit(function(event){
  event.preventDefault();
  //var bank_data=$("#bank input").serialize();
  var bank_data = "";
  var query_id=$("#view_query_id").val();
  var teacher_id=$("#inb_teacher_id").val();
  var student_id=$("#inb_student_id").val();
  //alert(teacher_id);

  if (teacher_id=='') {
      alertify.error("Error: At first finalize the teacher!");
      return false;
  }

  
  //var distri_name=$("#distri_name").val();
  var distri_name = "";
  //var paytm_mob_no=$("#paytm_mob_no").val();
  var paytm_mob_no = "";
  //var bank_count=$("#bank_count").val();
  var bank_count="";
  var cash_receive=0;
  var customer_invoice_number=$("#customer_invoice_numberate").val();
  var outbound_payment_type=$("#outbound_payment_type").val();
  var our_share=$("#our_share").val();
  var teacher_share=$("#teacher_share").val();
  var gst=$("#gst").val();
  var payment_type_details='';
  if (outbound_payment_type==1) {
      payment_type_details=$("#outbound_cash").val();
     }
     else if (outbound_payment_type==2) {
       payment_type_details=$("#outbound_pytm").val();
     }
     else if (outbound_payment_type==3) {
       payment_type_details=$("#outbound_bank").val();
     }
     else{
      payment_type_details=$("#outbound_card").val();
     }
     
     if($('#cash_receive').prop("checked") == true){
         cash_receive=1;
     }
     else if($('#cash_receive').prop("checked") == false){
         cash_receive=0;
     }

     if(!payment_type_details.trim())
     {
      alertify.error("Error: Please type the payment details !!!");
      return false;
     }
     if(!our_share.trim())
     {
      alertify.error("Error: Please type the Our Share !!!");
      return false;
     }
     if(!gst.trim())
     {
      alertify.error("Error: Please type the GST !!!");
      return false;
     }
     if(!teacher_share.trim())
     {
      alertify.error("Error: Please type Teacher Share !!!");
      return false;
     }

  //alert(query_id+"  "+teacher_id+"   "+student_id+"    "+payment_type_details);
  //return false;
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_outbound_payment',
        pedagoge_callback_class: 'ControllerQueries',
        bank_data : bank_data,
        query_id : query_id,
        teacher_id : teacher_id,
        student_id : student_id,
        distri_name : distri_name,
        paytm_mob_no : paytm_mob_no,
        bank_count : bank_count,
        cash_receive : cash_receive,
        customer_invoice_number : customer_invoice_number,
        outbound_payment_type: outbound_payment_type,
        payment_type_details : payment_type_details,
        our_share : our_share,
        teacher_share : teacher_share,
        gst : gst,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             console.log(response);
             $("#outbound_update_on").text(response);
             alertify.success("Success: Outbound Payment Updated."); 
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------  
});
 //----------------------------------------------------------------
 //Update query plan from admin dashboard--------------------------
 $(document).on("change","#query_plan",function(){
   var query_plan=$(this).val();
   var query_id=$("#view_query_id").val();
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_query_plan',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        query_plan : query_plan,
        
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               if (response=='success') {
                  alertify.success("Success: Query plan changed."); 
               }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
  });
 //----------------------------------------------------------------
 //Update Query Fast Pass------------------------------------------
      $('#add_plan').click(function(){
         var fast_pass=0;
         var query_id=$("#view_query_id").val();
            if($(this).prop("checked") == true){
                fast_pass=1;
            }
            else if($(this).prop("checked") == false){
                fast_pass=0;
            }
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_query_fast_pass',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        fast_pass : fast_pass,
        
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               if (response=='success') {
                  alertify.success("Success: Query fast pass status changed."); 
               }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //-------------------------------------------------------- 
      });
 //----------------------------------------------------------------
 //Update teacher plan---------------------------------------------
 $(document).on("change",".teacher_plan",function(){
   var t_type=$(this).attr("t-type");
   var ti_id=$(this).attr("ti-id");
   var teacher_plan=$(this).val();
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_teacher_ins_plan',
        pedagoge_callback_class: 'ControllerQueries',
        t_type : t_type,
        ti_id : ti_id,
        teacher_plan: teacher_plan,
        
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               if (response=='success') {
                  alertify.success("Success: Teacher plan changed."); 
               }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //-------------------------------------------------------- 
 });
 //----------------------------------------------------------------
 //**--------------------------------------------------------------
 /* Hml Editor */
var editor = new Jodit('#editor');
var editor2 = new Jodit('#editorJ');
/* Html Editor */
//End Document Ready function-------------------------------------------------------

});
 
//For get filter queries-------------------------------------------
function get_filter_queries(st_query_date,end_query_date,st_start_date,end_start_date,qstatus,flag_status,header_query_status,lead_source='') {
   //call ajax-----------------------------------------------
        var return_value = false;
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_filter_queries',
        pedagoge_callback_class: 'ControllerQueries',
        st_query_date : st_query_date,
        end_query_date : end_query_date,
        st_start_date : st_start_date,
        end_start_date : end_start_date,
        qstatus : qstatus,
        flag_status : flag_status,
        header_query_status : header_query_status,
        lead_source : lead_source
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                $("#query_clone_btn").addClass("disabled");
                $("#query_approve_btn").addClass("disabled");
                $("#query_disapprove_btn").addClass("disabled"); 
                $("#query_edit_btn").addClass("disabled");

                $("#table_data").html('');
                $("#table_data").html(response);
                //return_value = true;
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
               //return_value = false;
            });
            //return return_value;
         //--------------------------------------------------------

}

//Get Subject Type/Standrad----------------------------------------
function get_subject_type(subject_id,q_from_id) {
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_standard_list',
        pedagoge_callback_class: 'ControllerQueries',
        subject_id : subject_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                $("#"+q_from_id).html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------   
}
//-----------------------------------------------------------------




////////////////////code by sudipta/////////////////////////////


function fetch_select(val)
{


var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'emailtempfetch',
        pedagoge_callback_class: 'ControllerDashboard',
         val : val,
       
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);

               $('.jodit_editor').html(response);
               //$("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               alert(error);
            });
 

}

//-----------------------------------------------------------------



function checkingfile()
{
var filename = $('input[type=file]').val().replace(/.*(\/|\\)/, '');
//alert(filename);

var file_data = $("#imguploads").prop("files")[0]; // Getting the properties of file from file field
 // var form_data = new FormData(); // Creating object of FormData class
  //form_data.append("file", file_data) // Appending parameter named file with properties of file_field to form_data
 

            var form = new FormData();
            form.append('image', file_data); 
            alert (form);


var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'fileupload',
        pedagoge_callback_class: 'ControllerDashboard',


       data: form,
         
       
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
              // alert(response);

              $('#imageli').show();
              $('#imagediv').text(response);
               //$("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               alert(error);
            });

}


function email_submit()
{
var data;

var email_subject = $('#email_subject').val(); 
var to_email      = $("#ename").val();
var cc            = $("#cc1").val();
var bcc           = $("#bcc1").val();
var attach_file   = $('input[name=attachement_file]')[0].files[0];
var user_message  = $(".jodit_editor").text();

var post_data = new FormData();    
post_data.append( 'email_subject', email_subject );
post_data.append( 'to_email', to_email );
post_data.append( 'cc', cc );
post_data.append( 'bcc',bcc);
post_data.append( 'attach_file', attach_file );
post_data.append( 'user_message', user_message );



/*data = new FormData();
data.append('attachment_file', $('#attachement_file')[0].files[0]);
data.append('email_subject', $('#email_subject').val());
data.append('message', $(".jodit_editor").text());
data.append('cc', $("#cc1").val());
data.append('bcc', $("#bcc1").val());
data.append('toemail', $("#ename").val());*/

//console.log(post_data);
//return false;

var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'emailsubmit',
        pedagoge_callback_class: 'ControllerDashboard',
        data:post_data
    };

/*var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'emailsubmit',
        pedagoge_callback_class: 'ControllerDashboard',
        email_subject: $("#email_subject").val(),
        jodit_editor: $(".jodit_editor").text(),
        cc: $("#cc1").val(),
        bcc: $("#bcc1").val(),
        templates: $("#templates").val(),
        ename: $("#ename").val(),
        file: $("#imguploads").val(),
    };*/
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);

             $('#hid').text(response);
              $('#successdiv').show();

               //$("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               alert(error);
            });
 

}


//-----------------------------------------------------------------



function checkingfile()
{
var filename = $('input[type=file]').val().replace(/.*(\/|\\)/, '');

$('#imageli').show();

$('#imagediv').text(filename);

}


//////////////////////code by sudipta//////////////////////////////


/**************** SMS Code By Arghya *************************/

$(document).on("change",".sms_template",function(){
  //alert("Hi");
  var t_id = $(".sms_template option:selected").val();
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_sms_template_text',
        pedagoge_callback_class: 'ControllerQueries',
        t_id: t_id,
        };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
      //alert(response);
      $(".sms_text").val(response);
    })
    .fail(function(jqxhr, status, error){ 
        // this is the ""error"" callback
    });
  //
});

$(document).on("click",".sms_send",function(){
  var member_phone = $(".sms_member option:selected").val();
  var sms_text = $(".sms_text").val();
  var member_id = $(".sms_member option:selected").attr('data-member')
  var query_id = $("#query_id").val();
  var member_type = $(".sms_member option:selected").attr('data-type');
  var log = "A SMS has been sent to the "+member_type;
  if(!member_phone.trim() || !sms_text.trim())
  {
    alertify.error("Error: Please fill up all the fields");
    return false;
  }
  else
  {
    var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_sms_to_member',
        pedagoge_callback_class: 'ControllerQueries',
        member_phone: member_phone,
        message:sms_text,
        query_id:query_id,
        log:log,
        member_id:member_id
        };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
      //alert(response);
      if(response == 'success')
      {
        alertify.success("Success: SMS sent successfully");
        $(".sms_text").val("");
      }
    })
    .fail(function(jqxhr, status, error){ 
        
    });
  }
});

function urlify(text) {
    var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    //var urlRegex = /((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '">' + url + '</a>';
    })
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

$(document).on("click",".show_log",function(){
//alert($(this).attr("data-id"));

var note_id = $(this).attr('data-id');
var btn_type = $(this).attr('data-type');
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_activity_content_by_id',
        pedagoge_callback_class: 'ControllerQueries',
        id:note_id,
     };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
      //alert(response);
      $("#view_log_modal").modal('show');
      //$("#view_log").html("");
      //$("#view_log").html(response);
      $(".view_log").html("");
      if(btn_type == 'sms')
      {
        $(".view_log").html("<div class=sms_body>"+urlify(response)+"</div>");
      }
      else
      {
        $(".view_log").html(response);  
      }
      
    })
    .fail(function(jqxhr, status, error){ 
    });
});

$(document).on("click",".add_note_submit",function(){
  var note = $("#reasonCloser").val();
  var query_id = $("#query_id").val();
  if(!note.trim())
  {
    alertify.error("Error: Please fill up notes !!!");
    return false;
  }
  else
  {
    var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'add_activity_note',
        pedagoge_callback_class: 'ControllerQueries',
        note:note,
        query_id:query_id,
        };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
      //alert(response);
      if(response == 'success')
      {
        $("#admin_add_notes").modal('hide');
        alertify.success("Success: Note add successfully");
      }
    })
    .fail(function(jqxhr, status, error){ 
    });
  }
});

$(document).on("click",".add_note_update",function(){
  var note = $("#reasonCloser").val();
  var query_id = $("#query_id").val();
  var note_id = $("#note_id").val();
  if(!note.trim())
  {
    alertify.error("Error: Please fill up notes !!!");
    return false;
  }
  else
  {
    var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_activity_note',
        pedagoge_callback_class: 'ControllerQueries',
        note:note,
        query_id:query_id,
        note_id:note_id
        };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
      //alert(response);
      if(response == 'success')
      {
        $("#admin_add_notes").modal('hide');
        alertify.success("Success: Note updated successfully");
      }
    })
    .fail(function(jqxhr, status, error){ 
    });
  }
});

$(document).on("click",".admin_add_note",function(){
  $("#admin_add_notes").modal('show');
  $("#note_id").val("");
  $("#reasonCloser").val("");
  $(".add_note_submit").css('display','block');
  $(".add_note_update").css('display','none');
})

$(document).on("click",".view_note",function(){
  
  //alert($(this).attr('data-id'));
  var note_id = $(this).attr('data-id');
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_activity_logs_by_id',
        pedagoge_callback_class: 'ControllerQueries',
        id:note_id,
     };
  var dataToPost='';
  $.post($ajax_url, submit_data)
    .done(function(response, status, jqxhr){
      //alert(response);
      $(".add_note_submit").css('display','none');
      $(".add_note_update").css('display','block');
      $("#note_id").val(note_id);
      $("#admin_add_notes").modal('show');
      $("#reasonCloser").val("");
      $("#reasonCloser").val(response);
      /*if(response == 'success')
      {
        $("#admin_add_notes").modal('hide');
        alertify.success("Success: Note add successfully");
      }*/
    })
    .fail(function(jqxhr, status, error){ 
    });
});

function get_admin_notification(){
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_admin_ajax_notification',
        pedagoge_callback_class: 'ControllerQueries',
        };
        var dataToPost='';
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
             //alert(response);
             $("#head_notifacition").html(response);
          })
          .fail(function(jqxhr, status, error){ 
            // alert(error);
          });
}

function get_admin_ajax_notification_count(){
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_admin_ajax_notification_count',
        pedagoge_callback_class: 'ControllerQueries',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //alert(response);
               $("#count_noti_admin").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
}

function get_finance_ajax_notification_count(){
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_finance_ajax_notification_count',
        pedagoge_callback_class: 'ControllerQueries',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //console.log("Res Finance : "+response);
               $("#count_noti_finance").html(response);
               $("#payment_update_noti").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
}

function get_sales_ajax_notification_count(){
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_sales_ajax_notification_count',
        pedagoge_callback_class: 'ControllerQueries',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               //console.log("RES : "+response);
               var a_response = response.split(':');
               //console.log("RES : "+a_response[0]+"  "+a_response[1]+"  "+a_response[2]);
               $("#count_noti_sales").html(a_response[0]);
               $("#payment_receive_noti").html(a_response[1]);
               $("#payment_transfer_noti").html(a_response[2]);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
}

//Read notification-----------------------------------------------
$(document).on('click', '.receive_payment', function(e){
     //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'read_receive_notification_by_sales',
        pedagoge_callback_class: 'ControllerQueries',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
              // alertify.success("Success: Notification successfully deleted."); 
              // $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
});

//Read notification-----------------------------------------------
$(document).on('click', '.transfer_payment', function(e){
     //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'read_transfer_notification_by_sales',
        pedagoge_callback_class: 'ControllerQueries',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
              // alertify.success("Success: Notification successfully deleted."); 
              // $("#result_query").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
});

$(document).on('click', '.show_notification', function(e){
	var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'read_update_notification_by_finance',
        pedagoge_callback_class: 'ControllerQueries',
        };
        var dataToPost='';
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
              // alertify.success("Success: Notification successfully deleted."); 
              // $("#result_query").html(response);
        })
        .fail(function(jqxhr, status, error){
        	//alert(error);
        });
});

$(document).on("click","#admin_notification",function(){
	var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'read_notification_by_admin',
        pedagoge_callback_class: 'ControllerQueries',
        };
        var dataToPost='';
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
              // alertify.success("Success: Notification successfully deleted."); 
              // $("#result_query").html(response);
        })
        .fail(function(jqxhr, status, error){
        	//alert(error);
        });
});

$(document).on("change",".lead_source",function(){
//alert($(this).attr("query-id"));

  var query_id = $(this).attr("query-id");
  var lead = $(this).val();
  var result = confirm("Are you sure?");
  if(result) {
         //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'change_lead_source',
        pedagoge_callback_class: 'ControllerQueries',
        query_id: query_id,
        lead: lead,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){ 
               alertify.success("Success: Lead Source Changed Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error)
            });
         //--------------------------------------------------------
      }
      else{
        /*if (current_ststus==1) {
         $(this).val(0);       
        }
        else{
          $(this).val(1);       
        }*/
      }
});

