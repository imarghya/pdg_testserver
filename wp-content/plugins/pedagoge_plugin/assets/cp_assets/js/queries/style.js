
/* For menu Active */
$(document).ready(function () {
    $('.notification_admin li a').click(function(e) {

        $('.notification_admin li a').removeClass('active');

        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active');
        }
        //e.preventDefault();
    });
});
/* For menu active */


/* For Dropdown Menu */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Dropdown Menu */


/* For Filter Menu1 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction() {
    document.getElementById("filterFunctionDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter')) {

    var dropdownfilter = document.getElementsByClassName("dropdown-content-filter");
    var i;
    for (i = 0; i < dropdownfilter.length; i++) {
      var openDropdown = dropdownfilter[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu1 */

/* For Filter Menu2 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction1() {
    document.getElementById("filterFunctionDropdown1").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter1')) {

    var dropdownfilter1 = document.getElementsByClassName("dropdown-content-filter1");
    var i;
    for (i = 0; i < dropdownfilter1.length; i++) {
      var openDropdown = dropdownfilter1[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu2 */

/* For Filter Menu3 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction2() {
    document.getElementById("filterFunctionDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter2')) {

    var dropdownfilter2 = document.getElementsByClassName("dropdown-content-filter2");
    var i;
    for (i = 0; i < dropdownfilter2.length; i++) {
      var openDropdown = dropdownfilter2[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu3 */

/* For Filter Menu4 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction3() {
    document.getElementById("filterFunctionDropdown3").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter3')) {

    var dropdownfilter3 = document.getElementsByClassName("dropdown-content-filter3");
    var i;
    for (i = 0; i < dropdownfilter3.length; i++) {
      var openDropdown = dropdownfilter3[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu4 */

/* For Filter Menu5 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction4() {
    document.getElementById("filterFunctionDropdown4").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter4')) {

    var dropdownfilter4 = document.getElementsByClassName("dropdown-content-filter4");
    var i;
    for (i = 0; i < dropdownfilter4.length; i++) {
      var openDropdown = dropdownfilter4[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu5 */

/* For Slim Scroll */
		
		$(function(){
					$('.author_details').slimScroll({
						height: '1056px',
						size: '10px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						allowPageScroll: true,
						opacity: 1
					});
				});
				
/* For Teacher Dashbboard Preferences */				
				$(function(){
					$('.preference p').slimScroll({
						height: '150px',
						width: '100%',
						size: '5px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						allowPageScroll: true,
						opacity: 1
					});
				});
				
/* For Teacher Dashbboard Preferences */

/* For Admin Dashbboard Preferences */	


	if(screen.width > 1366){
			$(function(){
					$('.table_content_settings').slimScroll({
						height: '100%',
						width: '100%',
						size: '7px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						opacity: 1,
						allowPageScroll: true,
						wheelStep: 50,
						axis: 'both'
					});
				});
		}
		else if(screen.width < 1300){
				$(function(){
					$('.table_content_settings').slimScroll({
						height: '100%',
						width: '100%',
						size: '7px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						opacity: 1,
						allowPageScroll: true,
						wheelStep: 50,
						axis: 'both'
					});
				});
			}
		else
		{
			$(function(){
					$('.table_content_settings').slimScroll({
						height: '100%',
						width: '100%',
						size: '7px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						opacity: 1,
						allowPageScroll: true,
						wheelStep: 50,
						axis: 'both'
					});
				});
		}
		
		$(function(){
					$('.query_respons').slimScroll({
						height: '165px',
						width: '97%',
						size: '7px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						opacity: 1,
						allowPageScroll: true,
						wheelStep: 50,
						railVisible: true,
    					railColor: '#222',
    					railOpacity: 0.3,
						axis: 'y'
					});
				});



								
/* For Admin Dashbboard Preferences */





