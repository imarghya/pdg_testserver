var $table_teachers_list = null;
$(document).ready(function(){
	var $hidden_teachers_url = $("#hidden_teachers_url").val();
	var $hidden_teacher_edit_url = $("#hidden_teacher_edit_url").val();
	$table_teachers_list = $('#table_teachers_list').DataTable( {
		"dom" : "<'row'<'col-sm-12'>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_teachers_datatable';
                d.pedagoge_callback_class = 'ControlPanelTeachers';
            }
        },        
        "columnDefs": [{
                "targets": [ 1, 2, 3, 4, 9, 10],
                "visible": false,                
           },
        ],
        "rowCallback": function( row, data, index ) {
			var $slug = data[1];
			var $teacher_id = data[0];
		    var $html = '<div style="width:170px!important;" class="btn-toolbar">'+
				'<div class="btn-group" data-toggle="buttons">'+
					'<a class="btn btn-info cmd_view_teacher_info" title="View Teacher profile" href="'+$hidden_teachers_url+'/'+$slug+'" target="_blank"><i class="fa fa-eye"></i></a>'+
					'<a class="btn btn-success cmd_edit_teacher_info" title="Edit Teacher Profile" href="'+$hidden_teacher_edit_url+'/?role=teacher&id='+$teacher_id+'"><i class="fa fa-pencil-square-o"></i></a>'+
					'<button class="btn btn-warning cmd_edit_teacher_user_info" title="Edit Teacher User Info"><i class="fa fa-user"></i></button>'+
					'<button class="btn btn-danger cmd_delete_teacher" title="Delete Teacher"><i class="fa fa-trash-o"></i></button>'+
				'</div>'+
		     '</div>';	            
			$('td:eq(10)', row).html( $html );
		}
    } );
    
    $(".select_flt_teacher").change(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	teachers_table_filter_column($data_column, $selected_value);
    });
    
    $(".txt_fltr_teacher").onEnter(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	teachers_table_filter_column($data_column, $selected_value);
    });
    
    $("#cmd_reset_teachers_filters").click(function(){
    	teacher_table_reset_filters();
    });
    
    $('#table_teachers_list').on('click', '.cmd_delete_teacher', function(){
    	var $row_data = $table_teachers_list.row( $(this).closest('tr')[0] ).data();
    	var $teacher_id = $row_data[0];
    	
    	bootbox.confirm("Are you sure, you want to delete this Teacher?", function(result) {
    		if(result) {
    			if(!$.isNumeric($teacher_id) || $teacher_id <= 0) {
					var $str_msg = "Error! Teacher ID is not available";
		    		//fn_alert_message($result_area, $str_msg, "error");
		    		$.notify($str_msg, 'error');
		    		return false;
				}			
				
				var $submit_data = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'fn_delete_teacher_ajax',
					pedagoge_callback_class : 'ControlPanelTeachers',
					teacher_id : $teacher_id,
				};
				try {
					$.post($ajax_url, $submit_data, function(response) {
						
							var $response_data = $.parseJSON(response);
						
							var $error = $response_data.error,									 	
							 	$message = $response_data.message,
							 	$data = $response_data.data;				 	
							
							if($error) {
								//fn_alert_message($result_area, $message, "error");
								$.notify($message, 'error');
							} else {
								$table_teachers_list.ajax.reload();
								$.notify('Teacher was deleted successfully!', 'success');
							}
						
				    });
			    } catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					console.log($message);
				    $.notify('Error Occured while trying to delete the teacher.', 'error');
				}
    		}    		
		});
    });
    
    //cmd_view_teacher_info
    $('#table_teachers_list').on('click', '.cmd_view_teacher_info', function(){
    	var $href = $(this).prop('href');
    	window.open($href); 
    });
    
    $('#table_teachers_list').on('click', '.cmd_edit_teacher_info', function(){
    	var $href = $(this).prop('href');
    	window.open($href); 
    });
    
    //update user info of the teacher
    $('#table_teachers_list').on('click', '.cmd_edit_teacher_user_info', function(){
    	var $row_data = $table_teachers_list.row( $(this).closest('tr')[0] ).data();
		var $hidden_teacher_id = $row_data[0];
		var $hidden_teacher_user_id = $row_data[2];
		var $hidden_teacher_profile_id = $row_data[3];
		var $txt_teacher_user_name = $row_data[4];
		var $txt_teacher_email_address = $row_data[5];
		var $txt_teacher_first_name = $row_data[6];
		var $txt_teacher_last_name = $row_data[7];
		var $txt_teacher_mobile_no = $row_data[8];
		var $select_teacher_gender = $row_data[11];		
		var $select_approve_teacher = $row_data[13];
		var $select_active_teacher = $row_data[14];
		var $select_verify_teacher = $row_data[15];
		var $select_deleted_teacher = $row_data[16];
		
		var $data_array = {};
		$data_array.txt_teacher_first_name = $txt_teacher_first_name;
		$data_array.txt_teacher_last_name = $txt_teacher_last_name;
		$data_array.txt_teacher_mobile_no = $txt_teacher_mobile_no;
		$data_array.select_teacher_gender = $select_teacher_gender;
		$data_array.txt_teacher_user_name = $txt_teacher_user_name;
		$data_array.txt_teacher_email_address = $txt_teacher_email_address;
		$data_array.select_approve_teacher = $select_approve_teacher;
		$data_array.select_active_teacher = $select_active_teacher;
		$data_array.select_verify_teacher = $select_verify_teacher;
		$data_array.select_deleted_teacher = $select_deleted_teacher;
		$data_array.hidden_teacher_id = $hidden_teacher_id;
		$data_array.hidden_teacher_user_id = $hidden_teacher_user_id;
		$data_array.hidden_teacher_profile_id = $hidden_teacher_profile_id;
		fn_show_teacher_info_modal($data_array);
    });
    
    $('#modal_update_teacher_user_info').on('hidden.bs.modal', function (e) {
		//Reset modal
		fn_reset_teacher_info_modal();
    });
    
    $(".cmd_save_updated_teacher_info").click(function(){
    	var $button = $(this);
    	
    	var $result_area = $("#div_update_teacher_result_area");
    	
    	var $hidden_teacher_id = $("#hidden_teacher_id").val();
		var $hidden_teacher_user_id = $("#hidden_teacher_user_id").val();
		var $hidden_teacher_profile_id = $("#hidden_teacher_profile_id").val();
		var $txt_teacher_user_name = $("#txt_teacher_user_name").val();
		var $txt_teacher_email_address = $("#txt_teacher_email_address").val();
		var $txt_teacher_first_name = $("#txt_teacher_first_name").val();
		var $txt_teacher_last_name = $("#txt_teacher_last_name").val();
		var $txt_teacher_mobile_no = $("#txt_teacher_mobile_no").val();
		var $select_teacher_gender = $("#select_teacher_gender option:selected").val();
		var $select_approve_teacher = $("#select_approve_teacher option:selected").val();
		var $select_active_teacher = $("#select_active_teacher option:selected").val();
		var $select_verify_teacher = $("#select_verify_teacher option:selected").val();
		var $select_deleted_teacher = $("#select_deleted_teacher option:selected").val();
		var $txt_teacher_password = $("#txt_teacher_password").val();
		
		var $message = '';
		
		if(!$.isNumeric($hidden_teacher_id) || $hidden_teacher_id<=0) {
			//error
			$message = "Error! Teacher ID is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if(!$.isNumeric($hidden_teacher_user_id) || $hidden_teacher_user_id<=0) {
			//error
			$message = "Error! Teacher User ID is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if(!$.isNumeric($hidden_teacher_profile_id) || $hidden_teacher_profile_id<=0) {
			//error
			$message = "Error! Teacher Profile ID is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if($txt_teacher_first_name.length <= 0) {
			$message = "Error! Teacher First Name is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if($txt_teacher_user_name.length <= 0) {
			$message = "Error! Teacher User Name is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if($txt_teacher_email_address.length <= 0) {
			$message = "Error! Teacher Email address is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		/*if($txt_teacher_mobile_no.length != 10) {
			$message = "Error! Teacher mobile number must be 10 digit long.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}*/
		
		/*if($select_teacher_gender.length <= 0) {
			$message = "Error! Teacher Gender is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}*/
		if($select_approve_teacher.length <= 0) {
			$message = "Error! Teacher Approval status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		if($select_active_teacher.length <= 0) {
			$message = "Error! Teacher Activation status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		if($select_verify_teacher.length <= 0) {
			$message = "Error! Teacher Verification Status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		if($select_deleted_teacher.length <= 0) {
			$message = "Error! Teacher Deletion Status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		$button.button('loading');
		var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_update_teacher_data',
			pedagoge_callback_class : 'ControlPanelTeachers',
			hidden_teacher_id : $hidden_teacher_id,
			hidden_teacher_user_id : $hidden_teacher_user_id,
			hidden_teacher_profile_id : $hidden_teacher_profile_id,
			txt_teacher_user_name : $txt_teacher_user_name,
			txt_teacher_email_address : $txt_teacher_email_address,
			txt_teacher_first_name : $txt_teacher_first_name,
			txt_teacher_last_name : $txt_teacher_last_name,
			txt_teacher_mobile_no : $txt_teacher_mobile_no,
			select_teacher_gender : $select_teacher_gender,
			select_approve_teacher : $select_approve_teacher,
			select_active_teacher : $select_active_teacher,
			select_verify_teacher : $select_verify_teacher,
			select_deleted_teacher : $select_deleted_teacher,
			txt_teacher_password : $txt_teacher_password,
					
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {					
    				fn_alert_message($result_area, $message, "error");
				} else {
					$table_teachers_list.ajax.reload();
					$('#modal_update_teacher_user_info').modal('hide');
					$.notify($message, 'success');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {
			}			
	    }).complete(function() {
	    	//console.log('subject name reloading complete');
	    	$button.button('reset');
	    });
    });
    
});
function teachers_table_filter_column ( i, value ) {
    $('#table_teachers_list').DataTable().column( i ).search( value ).draw();
}
function teacher_table_reset_filters() {
	$(".txt_fltr_teacher, .select_flt_teacher").val('');
	$table_teachers_list
	 .search( '' )
	 .columns().search( '' )
	 .draw();
}


function fn_reset_teacher_info_modal() {	
	$("#div_update_teacher_result_area").html('');		
	$("#txt_teacher_first_name, #txt_teacher_last_name, #txt_teacher_mobile_no,  #select_approve_teacher").val('');
	$("#select_teacher_gender, #txt_teacher_user_name, #txt_teacher_password, #txt_teacher_email_address").val('');
	$("#select_active_teacher, #select_verify_teacher, #select_deleted_teacher, #hidden_teacher_id").val('');
	$("#hidden_teacher_user_id, #hidden_teacher_profile_id").val('');
}

function fn_show_teacher_info_modal($data_array) {
	
	$("#txt_teacher_first_name").val($data_array.txt_teacher_first_name);
	$("#txt_teacher_last_name").val($data_array.txt_teacher_last_name);
	$("#txt_teacher_mobile_no").val($data_array.txt_teacher_mobile_no);
	$("#select_teacher_gender").val($data_array.select_teacher_gender);
	$("#txt_teacher_user_name").val($data_array.txt_teacher_user_name);
	
	$("#txt_teacher_email_address").val($data_array.txt_teacher_email_address);
	$("#select_approve_teacher").val($data_array.select_approve_teacher);
	$("#select_active_teacher").val($data_array.select_active_teacher);
	$("#select_verify_teacher").val($data_array.select_verify_teacher);
	$("#select_deleted_teacher").val($data_array.select_deleted_teacher);
	
	$("#hidden_teacher_id").val($data_array.hidden_teacher_id);
	$("#hidden_teacher_user_id").val($data_array.hidden_teacher_user_id);
	$("#hidden_teacher_profile_id").val($data_array.hidden_teacher_profile_id);
	
	$('#modal_update_teacher_user_info').modal('show');
}