var $teacher_datatable = $student_datatable = null;

$(document).ready(function() {
	$("input:checkbox, input:radio").uniform();
	
	var $review_datatable = $('#reviews_list').DataTable( {
		
        "scrollX": true,
        "pageLength": 10,
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
        "processing": true,
        "serverSide": true,
        "searchDelay": 800,
        "ajax": {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_reviews_list_datatables';
                d.pedagoge_callback_class = 'ControllerReviews';
            }
        },
        "columnDefs": [{
                "targets": [ 2, 5, 7, 8, 9, 14, 19 ],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_view_review" title="View Review" data-toggle="modal" data-target="#modal_add_review"><i class="fa fa-eye" aria-hidden="true"></i> View</button>'	;
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $('.dataTables_filter input').attr('placeholder', 'Search');
    
    $(".cmd_refresh_reviews").click(function(){
    	$review_datatable.ajax.reload();
    });
    
    
    $("#reviews_list").on('click', '.cmd_view_review', function(){
    	var $row_data = $review_datatable.row( $(this).closest('tr')[0] ).data();
    	var $review_id = $row_data[0];
    	var $txt_modal_teacher_name = $row_data[6];
    	var $hidden_modal_teacher_user_id = $row_data[9];
    	var $txt_modal_teacher_email = $row_data[4];
    	var $txt_modal_student_name = $row_data[3];
    	var $hidden_modal_student_user_id = $row_data[8];
    	var $txt_modal_student_email = $row_data[1];
    	var $txt_modal_review_text = $row_data[7];
    	var $txt_modal_star1 = $row_data[10];
    	var $txt_modal_star2 = $row_data[11];
    	var $txt_modal_star3 = $row_data[12];
    	var $txt_modal_star4 = $row_data[13];
    	var $txt_modal_overall_star = $row_data[15];
    	
    	var $chk_modal_anonymous = $row_data[16];
		var $chk_modal_approved = $row_data[17];
		var $chk_modal_rejected = $row_data[18];
    	
    	$("#txt_modal_teacher_name").val($txt_modal_teacher_name);
    	$("#hidden_modal_teacher_user_id").val($hidden_modal_teacher_user_id);
    	$("#txt_modal_teacher_email").val($txt_modal_teacher_email);
    	$("#txt_modal_student_name").val($txt_modal_student_name);
    	$("#hidden_modal_student_user_id").val($hidden_modal_student_user_id);
    	$("#txt_modal_student_email").val($txt_modal_student_email);
    	$("#txt_modal_review_text").val($txt_modal_review_text);
    	$("#txt_modal_star1").val($txt_modal_star1);
    	$("#txt_modal_star2").val($txt_modal_star2);
    	$("#txt_modal_star3").val($txt_modal_star3);
    	$("#txt_modal_star4").val($txt_modal_star4);
    	$("#txt_modal_overall_star").val($txt_modal_overall_star);
    	$("#hidden_review_id").val($review_id);
    	
    	if($chk_modal_anonymous == 'yes') {
    		$('#chk_modal_anonymous').prop('checked',true);
    	} else {
    		$('#chk_modal_anonymous').prop('checked',false);
    	}
    	
    	if($chk_modal_approved == 'yes') {
    		$('#chk_modal_approved').prop('checked',true);
    	} else {
    		$('#chk_modal_approved').prop('checked',false);
    	}
    	
    	if($chk_modal_rejected == 'yes') {
    		$('#chk_modal_rejected').prop('checked',true);
    	} else {
    		$('#chk_modal_rejected').prop('checked',false);
    	}
		
		$.uniform.update('#chk_modal_rejected, #chk_modal_anonymous, #chk_modal_approved');
		    	
    });
    
    $('input.fltr_reviews').on( 'keypress', function (e) {
    	if (e.which == 13) {
		    reviews_table_filter_column( $(this).attr('data-column'), $(this).val() );  
		}
    } );
    
    $('.select_review_filter').on('change', function (e) {	    
	    reviews_table_filter_column( $(this).attr('data-column'), $("option:selected", this).val() );
	});
    
    $teacher_datatable = $('#tbl_teachers_list').DataTable( {
		
        "scrollX": true,
        "pageLength": 10,
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
        "processing": true,
        "serverSide": true,
        "searchDelay": 800,
        "ajax": $.fn.dataTable.pipeline( {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_teachers_institutes_list_datatables';
                d.pedagoge_callback_class = 'ControllerReviews';
            },
            pages: 5 // number of pages to cache
        } ),
        
        "columnDefs": [{
                "targets": [ 4, 5 ],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_select_teacher_institute" title="Select Teacher/Institute"><i class="fa fa-check-square-o" aria-hidden="true"></i> Select</button>'	;
			$('td:last-child', row).html( $html );			
		}
    } );
    
    
    $('#modal_find_teacher').on('shown.bs.modal', function (e) {
    	fn_update_coulmns_width();        		
    });
    
    $('#modal_find_teacher').on('hidden.bs.modal', function (e) {    		
        
		//Reset filter
		fn_reset_teachers_datatable_all_filters();
		
		//Reset selected inputs
		fn_reset_selected_teachers_filter();
    });
    
    
    $('input.fltr_teacher').on( 'keyup', function (e) {        
        if (e.which == 13) {
		    teacher_table_filter_column( $(this).attr('data-column'), $(this).val() );  
		}
    } );
    
    
    $("#tbl_teachers_list").on('click', '.cmd_select_teacher_institute', function() {    	
    	var $row_data = $teacher_datatable.row( $(this).closest('tr')[0] ).data();
    	var $selected_user_id = $row_data[0];
    	var $selected_mobile = $row_data[3];
    	var $selected_role = $row_data[6];
    	var $selected_email = $row_data[2];
    	var $teacher_name = $row_data[1];
    	
    	$("#hidden_fltr_selected_teacher_user_id").val($selected_user_id);
    	$("#txt_fltr_selected_teacher_mobile").val($selected_mobile);
    	$("#txt_fltr_selected_teacher_role").val($selected_role);
    	$("#txt_fltr_selected_teacher_email").val($selected_email);
    	$("#txt_fltr_selected_teacher_name").val($teacher_name);		
    });
    
    $(".cmd_select_filtered_teacher").click(function(){
    	var $selected_hidden_teacher_user_id = $("#hidden_fltr_selected_teacher_user_id").val();
    	
    	if($.isNumeric($selected_hidden_teacher_user_id)) {
    		$("#hidden_modal_teacher_user_id").val($("#hidden_fltr_selected_teacher_user_id").val());
    		$("#txt_modal_teacher_name").val($("#txt_fltr_selected_teacher_name").val());
    		$("#txt_modal_teacher_email").val($("#txt_fltr_selected_teacher_email").val());
    		$('#modal_find_teacher').modal('hide');
    	} else {    		
    		var $str_msg = "Please select a teacher!";
    		fn_alert_message(".div_selected_teacher_result_area",$str_msg,"error");
    		return false;
    	}
    });
    
    
    $student_datatable = $('#tbl_students_list').DataTable( {
		
        "scrollX": true,
        "pageLength": 10,
        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, 'All']],
        "processing": true,
        "serverSide": true,
        "searchDelay": 400,
        "ajax": $.fn.dataTable.pipeline( {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_student_guardians_list_datatables';
                d.pedagoge_callback_class = 'ControllerReviews';
            },
            pages: 5 // number of pages to cache
        } ),
        
        "columnDefs": [{
                "targets": [ 4, 5 ],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_select_student_guardian" title="Select Teacher/Guardian"><i class="fa fa-check-square-o" aria-hidden="true"></i> Select</button>'	;
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $('#modal_find_student').on('shown.bs.modal', function (e) {
    	fn_update_coulmns_width();        		
    });
    
    $('#modal_find_student').on('hidden.bs.modal', function (e) {    		
        
		//Reset filter
		fn_reset_students_datatable_all_filters();
		
		//Reset selected inputs
		fn_reset_selected_students_filter();
    });    
    
    $('input.fltr_student').on( 'keyup', function (e) {
    	if (e.which == 13) {
		    student_table_filter_column( $(this).attr('data-column'), $(this).val() );  
		}        
    } );
    
    $("#tbl_students_list").on('click', '.cmd_select_student_guardian', function() {    	
    	var $row_data = $student_datatable.row( $(this).closest('tr')[0] ).data();
    	var $selected_user_id = $row_data[0];
    	var $selected_mobile = $row_data[3];
    	var $selected_role = $row_data[6];
    	var $selected_email = $row_data[2];
    	var $teacher_name = $row_data[1];
    	
    	$("#hidden_fltr_selected_student_user_id").val($selected_user_id);
    	$("#txt_fltr_selected_student_mobile").val($selected_mobile);
    	$("#txt_fltr_selected_student_role").val($selected_role);
    	$("#txt_fltr_selected_student_email").val($selected_email);
    	$("#txt_fltr_selected_student_name").val($teacher_name);		
    });
    
    $(".cmd_select_filtered_student").click(function(){
    	var $selected_hidden_student_user_id = $("#hidden_fltr_selected_student_user_id").val();
    	
    	if($.isNumeric($selected_hidden_student_user_id)) {
    		$("#hidden_modal_student_user_id").val($("#hidden_fltr_selected_student_user_id").val());
    		$("#txt_modal_student_name").val($("#txt_fltr_selected_student_name").val());
    		$("#txt_modal_student_email").val($("#txt_fltr_selected_student_email").val());
    		$('#modal_find_student').modal('hide');
    	} else {    		
    		var $str_msg = "Please select a Student!";
    		fn_alert_message(".div_selected_student_result_area",$str_msg,"error");
    		return false;
    	}
    });
    
    
    $(".cmd_create_student").click(function(){
    	var $button = $(this);
    	var $result_area = $(".div_create_student_result_area");
    	var $select_student_create_role = $("#select_student_create_role option:selected").val();
    	var $select_student_create_gender = $("#select_student_create_gender option:selected").val();
		var $txt_student_create_age = $("#txt_student_create_age").val();
		var $txt_student_create_mobile = $("#txt_student_create_mobile").val();
		var $txt_student_create_email = $("#txt_student_create_email").val();
		var $txt_student_create_last_name = $("#txt_student_create_last_name").val();
		var $txt_student_create_first_name = $("#txt_student_create_first_name").val();
		
		var $str_message = '';
		
		if($txt_student_create_first_name.length<=0) {
			$str_message = "Please input first name...";
			fn_alert_message($result_area,$str_message,"error");
			$("#txt_student_create_first_name").focus();
			return;
		}
		
		if($txt_student_create_email.length<=0) {
			$str_message = "Please input email address...";
			fn_alert_message($result_area,$str_message,"error");
			$("#txt_student_create_email").focus();
			return;
		}
		
		if($txt_student_create_mobile.length != 10 ) {
			$str_message = "Please input 10 digit mobile no...";
			fn_alert_message($result_area,$str_message,"error");
			$("#txt_student_create_mobile").focus().select();
			return;
		}
		
		if($txt_student_create_age.length<=0 || $txt_student_create_age < 5) {
			$str_message = "Please input age...";
			fn_alert_message($result_area,$str_message,"error");
			$("#txt_student_create_age").focus().select();
			return;
		}
		
		$button.button('loading');
		$result_area.html('');
		
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_create_student_guardian',
			pedagoge_callback_class : 'ControllerSignup',
			select_student_create_role: $select_student_create_role,
			select_student_create_gender:$select_student_create_gender,
			txt_student_create_age:$txt_student_create_age,
			txt_student_create_mobile:$txt_student_create_mobile,
			txt_student_create_email:$txt_student_create_email,
			txt_student_create_last_name:$txt_student_create_last_name,
			txt_student_create_first_name:$txt_student_create_first_name
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, 'error');
				} else {					
					fn_alert_message($result_area, $message, 'success');
					$("#txt_filter_student_by_email").val($txt_student_create_email).keyup();
					//hide model
					$("#modal_create_student").modal('hide');
					//trigger filter
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'error');
			} finally {
				$button.button('reset');
			}			
        }).complete(function() {        	
        	$button.button('reset');
        });
		
    });
    
    $('#modal_add_review').on('hidden.bs.modal', function (e) {    		
        
		//Reset filter
		fn_reset_review_form();		
    });
    
    $(".cmd_save_modal_review").click(function() {
    	var $button = $(this);
    	var $result_area = $("#div_modal_result_area");
    	
    	var $teacher_user_id = $("#hidden_modal_teacher_user_id").val();
    	var $student_user_id = $("#hidden_modal_student_user_id").val();
    	var $rating_id = $("#hidden_review_id").val();
    	var $txt_modal_review_text = $("#txt_modal_review_text").val();
    	var $rejected = 'no';
    	var $approved = 'yes';
    	var $anonymous = 'no';
    	var $star1 = $("#txt_modal_star1").val();
    	var $star2 = $("#txt_modal_star2").val();
    	var $star3 = $("#txt_modal_star3").val();
    	var $star4 = $("#txt_modal_star4").val();
    	var $overall_stars = $("#txt_modal_overall_star").val();
    	
    	var $message = '';
    	
    	if($("#chk_modal_rejected").is(':checked')) {
    		$rejected = 'yes';
    	} else {
    		$rejected = 'no';
    	}
    	
    	if($("#chk_modal_approved").is(':checked')) {
    		$approved = 'yes';
    	} else {
    		$approved = 'no';
    	}
    	
    	if($("#chk_modal_anonymous").is(':checked')) {
    		$anonymous = 'yes';
    	} else {
    		$anonymous = 'no';
    	}
    	
    	if($teacher_user_id.length<=0 || $teacher_user_id<=0) {
    		$message = 'Please select Teacher/Institute';    		
			fn_alert_message($result_area, $message, 'error');
    		return;
    	}
    	
    	if($student_user_id.length<=0 || $student_user_id<=0) {
    		$message = 'Please select Student/Guardian';    		
			fn_alert_message($result_area, $message, 'error');
    		return;
    	}
    	
    	if($txt_modal_review_text.length<=5) {
    		$message = 'Please input review text';
    		$("#txt_modal_review_text").focus().select();
			fn_alert_message($result_area, $message, 'error');
    		return;
    	}
    	
    	if($star1.length <= 0 || $star2.length <= 0 || $star3.length <= 0 || $star4.length <= 0 || $overall_stars.length <= 0) {
    		$message = 'Please input review stars correctly!';
			fn_alert_message($result_area, $message, 'error');
    		return;
    	}
    	
    	$button.button('loading');
		$result_area.html('');
		
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'save_rating_and_review_ajax',
			pedagoge_callback_class : 'ControllerReviews',
			rating_id : $rating_id,
			teacher_userid : $teacher_user_id,
			student_userid : $student_user_id,
			star1 : $star1,
			star2 : $star2,
			star3 : $star3,
			star4 : $star4,
			review_text : $txt_modal_review_text,
			overall_rating : $overall_stars,
			is_anonymous : $anonymous,
			is_approved : $approved,
			is_rejected : $rejected
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, 'error');
				} else {					
					fn_alert_message($result_area, $message, 'success');
					//hide model
					$("#modal_add_review").modal('hide');
					//trigger filter
					$(".cmd_refresh_reviews").click();
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'error');
			} finally {
				$button.button('reset');
			}			
        }).complete(function() {        	
        	$button.button('reset');
        });
		    
    });
    
    $('.star_count').on('keyup', function() {
		fn_calculate_star_average();
	});
	
	$(".cmd_delete_modal_review").click(function(){
		var $button = $(this);
		var $save_button = $(".cmd_save_modal_review");
    	var $result_area = $("#div_modal_result_area");    	
    	var $rating_id = $("#hidden_review_id").val();
    	
    	if($.isNumeric($rating_id) && $rating_id>0) {
			bootbox.confirm("Are you sure, you want to delete this review?", function(result) {
				if(result) {
					
			  		$button.button('loading');
					$result_area.html('');
					$save_button.prop('disabled', true);
					
				  	var $submit_data = {
						nonce : $ajaxnonce,
						action : $pedagoge_visitor_ajax_handler,
						pedagoge_callback_function : 'delete_rating_and_review_ajax',
						pedagoge_callback_class : 'ControllerReviews',
						rating_id : $rating_id
					};
					
					$.post($ajax_url, $submit_data, function(response) {
						try {
							var $response_data = $.parseJSON(response);
						
							var $error = $response_data.error,									 	
							 	$message = $response_data.message,
							 	$data = $response_data.data;				 	
							
							if($error) {
								fn_alert_message($result_area, $message, 'error');
							} else {					
								fn_alert_message($result_area, $message, 'success');
								//hide model
								$("#modal_add_review").modal('hide');
								//trigger filter
								$(".cmd_refresh_reviews").click();
							}
						} catch( err ) {
							var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
						    fn_alert_message($result_area, $message, 'error');
						} finally {
							$button.button('reset');
							$save_button.prop('disabled', false);
						}			
			        }).complete(function() {        	
			        	$button.button('reset');
			        	$save_button.prop('disabled', false);
			        });
				}
			}); 
    	}
    	
	});		 
    
});

function reviews_table_filter_column ( i, value ) {
    $('#reviews_list').DataTable().column( i ).search( value ).draw();
}

function fn_calculate_star_average() {
	var $total_value = 0, $counter = 0, $average_val = 0;
	
	$('.star_count').each(function(index){
		var $value = parseFloat($(this).val());
		if($.isNumeric($value)) {			
			$total_value += parseFloat($(this).val());	
		}
				
		$counter++;
	});
	
	if($total_value > 0) {
		$average_val = $total_value/$counter;
	}
	$("#txt_modal_overall_star").val($average_val);
}

function fn_reset_review_form() {
	$("#hidden_modal_student_user_id, #txt_modal_student_email, #txt_modal_student_name, #txt_modal_teacher_email, #hidden_modal_teacher_user_id, #txt_modal_teacher_name").val('');
	$("#hidden_review_id, #txt_modal_overall_star, #txt_modal_star4, #txt_modal_star3, #txt_modal_star2, #txt_modal_star1, #txt_modal_review_text").val('');
	$("#div_modal_result_area").html('');
	
	$('#chk_modal_rejected, #chk_modal_anonymous').prop('checked',false);
	$('#chk_modal_approved').prop('checked',true);
	$.uniform.update('#chk_modal_rejected, #chk_modal_anonymous, #chk_modal_approved');
	
}

function fn_reset_teachers_datatable_all_filters() {
	$("#txt_filter_teacher_by_name, #txt_filter_teacher_by_email, #txt_filter_teacher_by_mobile").val('');
	$teacher_datatable.search( '' ).columns().search( '' ).draw();
	$(".div_selected_teacher_result_area").html('');
}

function fn_reset_selected_teachers_filter() {
	$("#hidden_fltr_selected_teacher_user_id, #txt_fltr_selected_teacher_mobile, #txt_fltr_selected_teacher_role, #txt_fltr_selected_teacher_email, #txt_fltr_selected_teacher_name ").val('');
}

function teacher_table_filter_column ( i, value ) {
    $('#tbl_teachers_list').DataTable().column( i ).search( value ).draw();
}



function fn_reset_students_datatable_all_filters() {
	$("#txt_filter_student_by_name, #txt_filter_student_by_email, #txt_filter_student_by_mobile").val('');
	$student_datatable.search( '' ).columns().search( '' ).draw();
	$(".div_selected_student_result_area").html('');
}

function fn_reset_selected_students_filter() {
	$("#hidden_fltr_selected_student_user_id, #txt_fltr_selected_student_mobile, #txt_fltr_selected_student_role, #txt_fltr_selected_student_email, #txt_fltr_selected_student_name ").val('');
}


function student_table_filter_column ( i, value ) {
    $('#tbl_students_list').DataTable().column( i ).search( value ).draw();
}

function fn_update_coulmns_width() {
	$($.fn.dataTable.tables(true)).DataTable().columns.adjust();
}

//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'GET' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }
 
            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if ( requestLength >= -1 ) {
                        json.data.splice( requestLength, json.data.length );
                    }
                     
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );
 
            drawCallback(json);
        }
    };
};
 
// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );

/***********************************************************Reviews Importing Code ******************************************/
$(document).ready(function() {
	
	$("#cmd_upload_review_file").click(function(event){		
    	event.preventDefault();
    	    	
    	var $file_input = $("#fileinput_review_import");
    	
    	var $button = $(this),
    		$result_area = $(".div_excel_upload_result_area"),
    		$str_message = '';    	
    	
    		
		if($file_input.get(0).files.length === 0) {
			$str_message = "Please select an excel file for uploading...";
			fn_alert_message($result_area, $str_message, 'error');
			return;
		} else {
			
			var $upload_data = new FormData();
			$upload_data.append('review_file', $file_input.get(0).files[0]);
			$upload_data.append("nonce", $ajaxnonce);
			$upload_data.append("action", $pedagoge_visitor_ajax_handler);
			$upload_data.append("pedagoge_callback_function", 'fn_import_review_file');
			$upload_data.append("pedagoge_callback_class", 'ControllerReviews');		
			
			$result_area.html('<div class="alert alert-info">Uploading review file...</div>');
			$button.button('loading');
			
			$.ajax({
				url : $ajax_url,
			    data : $upload_data,
			    processData : false, 
			    contentType : false,
			    type : 'POST',			    
			    success : function(response) {
			    	try {
					
						var $response_data = $.parseJSON(response);
						
						var $error = $response_data.error, 
							$error_type = $response_data.error_type,				 	
						 	$message = $response_data.message,
						 	$data = $response_data.data;
						 	
						if($error) {
							fn_alert_message($result_area, $message, 'error');
						} else {
							fn_alert_message($result_area, $message, 'success');
							resetFormElement($file_input);
						}
						
					} catch(err) {
						$button.button('reset');
						console.log('error occured '+err);
					}
			    },
			    complete:function(){
			    	$button.button('reset');
			    }
			});
		}    	
    });
    
    $("#cmd_process_reviews").click(function(){
		var $button = $(this);		
    	var $result_area = $(".div_review_process_result_area");    	
    	
    	$button.button('loading');
		$result_area.html('');
		
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_process_imported_reviews',
			pedagoge_callback_class : 'ControllerReviews',
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				fn_alert_message($result_area, response, 'info');
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'error');
			} finally {
				$button.button('reset');
			}			
        }).complete(function() {        	
        	$button.button('reset');
        });
    	
	});
});