$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(document).ready(function() {
	$("input:checkbox").uniform();
	
	/**
	 * Following code is required because datatable is within a tab. 
	 * When a datatable is within a tab its columns size has to be adjusted on tabbing event. 
	 */
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {		
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
    });
	
	$('.datatable').DataTable({		
		"scrollY":        "200px",
        "scrollCollapse": true,
        "paging":         false
	});
	
	fn_process_select_with_title();
});


function fn_process_select_with_title() {
	$(".select_control_with_title").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
		});
	});
	
	$(".select_control_with_title_multiple").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
			closeOnSelect: false
		});
	});
	
	$(".fltr_course, .select_course_update, .fltr_course_merge1, .fltr_course_merge2").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
			allowClear: true,
		});
	});	
	
}
/**************************************Merge Subjects Management ******************************************/
var $merge_subject_list1 = null;
var $merge_subject_list2 = null;
$(document).ready(function() {	
		
	$merge_subject_list1 = $('#subject_list1').DataTable( {
		"dom" : "<'row'<'col-sm-12'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "processing": true,
        "serverSide": true,
        "lengthChange" : false,
        "pagingType" : 'simple',
        "ajax": {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_subjects_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_select_merge_subject_name1" title="Select Subject" ><i class="fa fa-check-square-o" aria-hidden="true"></i> Select</button>'	;
			$('td:last-child', row).html( $html );
		}
    } );
    
    $merge_subject_list2 = $('#subject_list2').DataTable( {
    	"dom" : "<'row'<'col-sm-12'f>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "processing" : true,
        "serverSide" : true,
        "lengthChange" : false,
        "pagingType" : 'simple',
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_subjects_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_select_merge_subject_name2" title="Select Subject" >'+
		    '<i class="fa fa-check-square-o" aria-hidden="true"></i> Select</button>';
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $(".cmd_reset_selected_subjects").click(function(){
    	$("#txt_selected_subject_1, #txt_selected_subject_2, #hidden_merge_selected_subject_name1_id, #hidden_merge_selected_subject_name2_id").val('');
    	$("#subject_merge_result_area").html('');
    });
    
    $("#subject_list1").on('click', '.cmd_select_merge_subject_name1', function(){
    	var $row_data = $merge_subject_list1.row( $(this).closest('tr')[0] ).data();
    	var $record_id = $row_data[0];
    	var $record_value = $row_data[1];
    	$("#txt_selected_subject_1").val($record_value);
    	$("#hidden_merge_selected_subject_name1_id").val($record_id);
    });
    
    $("#subject_list2").on('click', '.cmd_select_merge_subject_name2', function(){
    	var $row_data = $merge_subject_list2.row( $(this).closest('tr')[0] ).data();
    	var $record_id = $row_data[0];
    	var $record_value = $row_data[1];
    	$("#txt_selected_subject_2").val($record_value);
    	$("#hidden_merge_selected_subject_name2_id").val($record_id);
    });
    
    $(".cmd_merge_subjects").click(function(){
    	var $button = $(this);
    	var $subject_name_id1 = $("#hidden_merge_selected_subject_name1_id").val();
    	var $subject_name_id2 = $("#hidden_merge_selected_subject_name2_id").val();
    	var $result_area = $("#subject_merge_result_area");
    	var $message = '';
    	if($subject_name_id1.length<=0) {
    		$message = "Please select a subject name from List1";
    		fn_alert_message($result_area, $message, "error");
    		return;
    	}
    	
    	if($subject_name_id2.length<=0) {
    		$message = "Please select a subject name from List2";
    		fn_alert_message($result_area, $message, "error");
    		return;
    	}
    	
    	if($subject_name_id1 == $subject_name_id2) {
    		$message = "Both selected subjects are the same. Same Subjects cannot be merged. Please try again.";
    		fn_alert_message($result_area, $message, "error");
    		return;
    	}
    	
    	bootbox.confirm("Are you sure, you want to merge these subjects?", function(result) {
    		$button.button('loading');
	    	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_merge_subject_names_ajax',
				pedagoge_callback_class : 'ControlPanelCourse',
				subject_name_id1 : $subject_name_id1,
				subject_name_id2 : $subject_name_id2,
			};
			
			$.post($ajax_url, $submit_data, function(response) {
				try {
					var $response_data = $.parseJSON(response);
				
					var $error = $response_data.error,									 	
					 	$message = $response_data.message,
					 	$data = $response_data.data;				 	
					
					if($error) {
						fn_alert_message($result_area, $message, "error");
					} else {
						/**
						 * reset merge form
						 * reload tables 
						 */
						$(".cmd_reset_selected_subjects").click();
						fn_refresh_subject_name_datatable();
					}
				} catch( err ) {
					$button.button('reset');
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					console.log($message);
				    
				} finally {
					
				}			
		    }).complete(function() {
		    	$button.button('reset');
		    });
    	});   	
    });
    
    $(".cmd_refresh_subject_list1").click(function(){
    	$merge_subject_list1.ajax.reload();
    });
    $(".cmd_refresh_subject_list2").click(function(){
    	$merge_subject_list2.ajax.reload();
    });
    
	
});



/**************************************Subject Names Management ******************************************/
var $table_subject_names = null;
$(document).ready(function() {
	$table_subject_names = $('#table_subject_names').DataTable( {    	
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_subjects_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_view_subject" title="View Subject" ><i class="fa fa-eye" aria-hidden="true"></i> View</button>'	;
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $("#table_subject_names").on('click', '.cmd_view_subject', function(){
    	var $row_data = $table_subject_names.row( $(this).closest('tr')[0] ).data();
    	var $record_id = $row_data[0];
    	var $record_value = $row_data[1];
    	
    	var $data_array = {};
		$data_array.title = 'Update Subject Name';
		$data_array.field_value = $record_value;
		$data_array.record_id = $record_id;
		fn_load_subject_name_modal($data_array);
    });
    
    $(".cmd_add_subject_names").click(function(){
		var $data_array = {};
		$data_array.title = 'Add a Subject Name';
		$data_array.field_value = '';
		$data_array.record_id = '';
		fn_load_subject_name_modal($data_array);
	});
	
	$(".cmd_refresh_subject_names").click(function(){
		$table_subject_names.ajax.reload();
	});
});



function fn_refresh_subject_name_datatable() {
	$table_subject_names.ajax.reload();
	/**
	 * Refresh Subject names everywhere
	 */
	fn_reload_subject_names_options();
	
	$merge_subject_list1.ajax.reload();
	$merge_subject_list2.ajax.reload();
	
}

function fn_load_subject_name_modal($data) {	
	$data.field_name = 'Subject Name';	
	$data.save_action = 'fn_save_subject_name';
	$data.delete_action = 'fn_delete_subject_name';
	$data.save_controller = 'ControlPanelCourse';
	$data.delete_controller = 'ControlPanelCourse';
	$data.save_callback = 'fn_refresh_subject_name_datatable';
	$data.delete_callback = 'fn_refresh_subject_name_datatable';
	fn_show_single_table_modal($data);
}

function fn_reload_subject_names_options() {
	console.log('loading subject Names');
	$.notify('Reloading subject names...', 'info');
	
	var $select_controls = '#select_fltr_subject_name, #select_create_course_subject_name, #select_merge_course_fltr_subject_name1, #select_merge_course_fltr_subject_name2';
	
	$($select_controls).html('');
	$($select_controls).select2({
		placeholder : "Loading..."
	});
	
  	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_subject_names_ajax',
		pedagoge_callback_class : 'ControlPanelCourse',		
	};
	
	$.post($ajax_url, $submit_data, function(response) {
		console.log('subject name reloading');
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				$.notify('Error loading subject names...', 'error');
			} else {
				console.log('kdsfgdsjgf');
				$($select_controls).html($data);
				$($select_controls).select2({		
					closeOnSelect: false,
					placeholder : "Subject Name"
				});
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		    
		} finally {
		}			
    }).complete(function() {
    	console.log('subject name reloading complete');
    });
}

/**************************************Subject Types Management ******************************************/
var $table_subject_type = null;
$(document).ready(function() {
	$table_subject_type = $('#table_subject_types').DataTable( {    	
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_subject_types_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_view_subject_type" title="View Subject Type" ><i class="fa fa-eye" aria-hidden="true"></i> View</button>'	;
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $("#table_subject_types").on('click', '.cmd_view_subject_type', function(){
    	var $row_data = $table_subject_type.row( $(this).closest('tr')[0] ).data();
    	var $record_id = $row_data[0];
    	var $record_value = $row_data[1];
    	var $data_array = {};
		$data_array.title = 'Update Subject Type';
		$data_array.field_value = $record_value;
		$data_array.record_id = $record_id;
		fn_load_subject_type_modal($data_array);
    });
    
    $(".cmd_add_subject_type").click(function(){
		var $data_array = {};
		$data_array.title = 'Add a Subject Type';
		$data_array.field_value = '';
		$data_array.record_id = '';
		fn_load_subject_type_modal($data_array);
	});

    $(".cmd_refresh_subject_type").click(function(){
		$table_subject_type.ajax.reload();
	});
});




function fn_refresh_course_type_datatable() {
	$table_subject_type.ajax.reload();
	fn_reload_subject_types_options();
}

function fn_load_subject_type_modal($data) {	
	$data.field_name = 'Subject Type';	
	$data.save_action = 'fn_save_subject_type';
	$data.delete_action = 'fn_delete_subject_type';
	$data.save_controller = 'ControlPanelCourse';
	$data.delete_controller = 'ControlPanelCourse';
	$data.save_callback = 'fn_refresh_course_type_datatable';
	$data.delete_callback = 'fn_refresh_course_type_datatable';
	fn_show_single_table_modal($data);
}

function fn_reload_subject_types_options() {
	$.notify('Reloading subject types...', 'info');
	var $str_select_control = '#select_fltr_subject_type, #select_create_course_subject_type, #select_merge_course_fltr_subject_type1, #select_merge_course_fltr_subject_type2';
	$($str_select_control).html('');
	$($str_select_control).select2({
		placeholder : "Loading..."
	});
	
  	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_subject_types_ajax',
		pedagoge_callback_class : 'ControlPanelCourse',		
	};
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				$.notify('Error loading subject names...', 'error');
			} else {
				$($str_select_control).html($data);
				$($str_select_control).select2({		
					closeOnSelect: false,
					placeholder : "Subject Type"
				});
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		    
		} finally {
		}			
    }).complete(function() {
    });
}

/**************************************Course Management ******************************************/
var $table_course_list = null;
$(document).ready(function() {
	$table_course_list = $('#table_course_list').DataTable( {
		"dom" : "<'row'<'col-sm-12'>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_course_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },        
        "columnDefs": [{
                "targets": [ 1, 3, 5, 7, 9],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_view_course" title="View Course Info" ><i class="fa fa-eye" aria-hidden="true"></i> View</button>'	;
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $('#modal_create_course').on('hidden.bs.modal', function (e) {
    	$("#select_create_course_subject_name, #select_create_course_subject_type, #select_create_course_academic_board, #select_create_course_age").select2("val", "");
    	$("#div_create_course_result_area").html('');
    });
    
        
    $(".cmd_save_new_course").click(function(){
    	var $button = $(this);
    	var $select_create_course_subject_name = $("#select_create_course_subject_name").val();
		var $select_create_course_subject_type = $("#select_create_course_subject_type").val();
		var $select_create_course_academic_board = $("#select_create_course_academic_board").val();
		var $select_create_course_age = $("#select_create_course_age").val();
		var $result_area = $("#div_create_course_result_area");
				
		if($select_create_course_subject_name.length<=0) {
			var $str_msg = "Please select a Subject Name!";
    		fn_alert_message($result_area, $str_msg, "error");
    		return false;
		}
		
		if($select_create_course_subject_type.length<=0) {
			var $str_msg = "Please select a Subject Type!";
    		fn_alert_message($result_area, $str_msg, "error");
    		return false;
		}
		
		$button.button('loading');
		var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_create_course_ajax',
			pedagoge_callback_class : 'ControlPanelCourse',
			subject_name : $select_create_course_subject_name,
			subject_type : $select_create_course_subject_type,
			academic_board : $select_create_course_academic_board,
			course_age : $select_create_course_age,
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, "error");
				} else {
					fn_reload_course_list();
					$("#modal_create_course").modal('hide');
					$.notify('Course was created successfully!', 'success');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {
				$button.button('reset');
			}
	    }).complete(function() {
	    	$button.button('reset');
	    });
    });
    
    $(".fltr_course").change(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	course_table_filter_column($data_column, $selected_value);
    	
    });
    
     $('#modal_update_course').on('hidden.bs.modal', function (e) {
    	$("#select_update_course_subject_name, #select_update_course_subject_type, #select_update_course_academic_board, #select_update_course_age").select2("val", "");
    	$("#hidden_update_course_id").val('');
    	$("#div_update_course_result_area").html('');
    });
    
    $('#table_course_list').on('click', '.cmd_view_course', function(){
    	var $row_data = $table_course_list.row( $(this).closest('tr')[0] ).data();
    	var $course_id = $row_data[0];
    	var $subject_name_id = $row_data[1];
    	var $subject_type_id = $row_data[3];
    	var $course_type = $row_data[5];
    	var $academic_board_id = $row_data[7];
    	var $course_age_id = $row_data[9];
    	
    	$("#select_update_course_name").val($subject_name_id).change();
    	$("#select_update_course_subject_type").val($subject_type_id).change();
    	$("#select_update_course_type").val($course_type).change();
    	$("#select_update_course_academic_board").val($academic_board_id).change();
    	$("#select_update_course_age").val($course_age_id).change();
    	$("#hidden_update_course_id").val($course_id);
    	$("#modal_update_course").modal('show');
    });
    
    $("#cmd_delete_course").click(function(){
    	var $course_id = $("#hidden_update_course_id").val();
    	var $button = $(this);
    	
		var $result_area = $("#div_update_course_result_area");
		
    	bootbox.confirm("Are you sure, you want to delete this course?", function(result) {
    		if(result) {
    			if(!$.isNumeric($course_id) || $course_id<=0) {
					var $str_msg = "Error! Course ID not available";
		    		fn_alert_message($result_area, $str_msg, "error");
		    		return false;
				}			
				$button.button('loading');
				var $submit_data = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'fn_delete_course_ajax',
					pedagoge_callback_class : 'ControlPanelCourse',
					course_id : $course_id,
				};
				
				$.post($ajax_url, $submit_data, function(response) {
					try {
						var $response_data = $.parseJSON(response);
					
						var $error = $response_data.error,									 	
						 	$message = $response_data.message,
						 	$data = $response_data.data;				 	
						
						if($error) {
							fn_alert_message($result_area, $message, "error");
						} else {
							fn_reload_course_list();
							fn_reload_merge_courses_list();
							$("#modal_update_course").modal('hide');
							$.notify('Course was deleted successfully!', 'success');
						}
					} catch( err ) {
						var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
						console.log($message);
					    
					} finally {
						$button.button('reset');
					}
			    }).complete(function() {
			    	$button.button('reset');
			    });
    		}    		
		});
    });
    
    $(".cmd_save_updated_course").click(function(){
    	console.log('updated');
    	var $course_id = $("#hidden_update_course_id").val();
    	var $subject_name_id = $("#select_update_course_name").val();
    	var $subject_type_id = $("#select_update_course_subject_type").val();
    	var $course_type_id = $("#select_update_course_type").val();
    	var $course_age_category = $("#select_update_course_age").val();
    	var $academic_board_id = $("#select_update_course_academic_board").val();
    	
    	var $button = $(this);
    	
		var $result_area = $("#div_update_course_result_area");
				
		if(!$.isNumeric($course_id) || $course_id<=0) {
			var $str_msg = "Error! Course ID not available";
    		fn_alert_message($result_area, $str_msg, "error");
    		return false;
		}
		
		/**
		 * Stop if subject name id and subject type id is empty 
		 */
		
		if(!$.isNumeric($subject_name_id) || $subject_name_id<=0) {
			var $str_msg = "Error! Please select Subject Name";
    		fn_alert_message($result_area, $str_msg, "error");
    		return false;
		}
		if(!$.isNumeric($subject_type_id) || $subject_type_id<=0) {
			var $str_msg = "Error! Please select Subject Type";
    		fn_alert_message($result_area, $str_msg, "error");
    		return false;
		}
			
		$button.button('loading');
		var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_update_course_ajax',
			pedagoge_callback_class : 'ControlPanelCourse',
			course_id : $course_id,
			subject_name_id : $subject_name_id,
    		subject_type_id : $subject_type_id,
	    	course_type_id : $course_type_id,
	    	course_age_category : $course_age_category,
	    	academic_board_id : $academic_board_id
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, "error");
				} else {
					fn_reload_course_list();
					fn_reload_merge_courses_list();
					$("#modal_update_course").modal('hide');
					$.notify('Course was updated successfully!', 'success');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {
				$button.button('reset');
			}
	    }).complete(function() {
	    	$button.button('reset');
	    });
    });
    
    $(".cmd_refresh_course_list").click(function(){
    	$table_course_list.ajax.reload();
    });
    
});

function course_table_filter_column ( i, value ) {
    $('#table_course_list').DataTable().column( i ).search( value ).draw();
}

function fn_reload_course_list() {
	$table_course_list.ajax.reload();
}
/**************************************Merging Course ******************************************/
var $table_course_list1 = null;
var $table_course_list2 = null;
$(document).ready(function() {
	$table_course_list1 = $('#tbl_course_list1').DataTable( {
		"dom" : "<'row'<'col-sm-12'>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12'p>>",
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_course_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },        
        "columnDefs": [{
                "targets": [ 1, 3, 5, 7, 9],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_select_merge_course_name1" title="Select Course" ><i class="fa fa-check-square-o" aria-hidden="true"></i> Select</button>';
			$('td:last-child', row).html( $html );
		}
    } );
    
    $table_course_list2 = $('#tbl_course_list2').DataTable( {
		"dom" : "<'row'<'col-sm-12'>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-12'p>>",
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_course_datatable';
                d.pedagoge_callback_class = 'ControlPanelCourse';
            }
        },        
        "columnDefs": [{
                "targets": [ 1, 3, 5, 7, 9],
                "visible": false,                
            }
        ],
        "rowCallback": function( row, data, index ) {		    
		    $html = '<button class="btn btn-default col-md-12 cmd_select_merge_course_name2" title="Select Course" ><i class="fa fa-check-square-o" aria-hidden="true"></i> Select</button>';
			$('td:last-child', row).html( $html );			
		}
    } );
    
    $(".fltr_course_merge1").change(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	course1_table_filter_column($data_column, $selected_value);
    	
    });
    
    $(".fltr_course_merge2").change(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	course2_table_filter_column($data_column, $selected_value);
    	
    });
    
    $(".cmd_refresh_course_list2").click(function(){
    	$table_course_list2.ajax.reload();
    });
    
    $(".cmd_refresh_course_list1").click(function(){
    	$table_course_list1.ajax.reload();
    });
    
    $('#tbl_course_list1').on('click', '.cmd_select_merge_course_name1', function(){
    	var $row_data = $table_course_list1.row( $(this).closest('tr')[0] ).data();
    	var $course_id = $row_data[0];
    	var $subject_name = $row_data[2];
    	var $subject_type = $row_data[4];
    	var $course_type = $row_data[6];
    	var $academic_board = $row_data[8];
    	var $course_age = $row_data[10];
		
		if(!$subject_type) {
			$subject_type = '';
		}
		
		if(!$course_type) {
			$course_type = '';
		}
		
		if(!$academic_board) {
			$academic_board = '';
		}
		
		if(!$course_age) {
			$course_age = '';
		}
		
    	var $tr_data = ''+
    		'<tr>'+
				'<td>'+$course_id+'</td>'+
				'<td>'+$subject_name+'</td>'+
				'<td>'+$subject_type+'</td>'+
				'<td>'+$course_type+'</td>'+
				'<td>'+$academic_board+'</td>'+
				'<td>'+$course_age+'</td>'+
			'</tr>';
		$("#tbl_selected_course1 tbody").html($tr_data);
		$("#hidden_merge_selected_course1").val($course_id);
    });
    
    $('#tbl_course_list2').on('click', '.cmd_select_merge_course_name2', function(){
    	var $row_data = $table_course_list2.row( $(this).closest('tr')[0] ).data();
    	var $course_id = $row_data[0];
    	var $subject_name = $row_data[2];
    	var $subject_type = $row_data[4];
    	var $course_type = $row_data[6];
    	var $academic_board = $row_data[8];
    	var $course_age = $row_data[10];
		
		if(!$subject_type) {
			$subject_type = '';
		}
		
		if(!$course_type) {
			$course_type = '';
		}
		
		if(!$academic_board) {
			$academic_board = '';
		}
		
		if(!$course_age) {
			$course_age = '';
		}
		
    	var $tr_data = ''+
    		'<tr>'+
				'<td class="course_id_to_merge">'+$course_id+'</td>'+
				'<td>'+$subject_name+'</td>'+
				'<td>'+$subject_type+'</td>'+
				'<td>'+$course_type+'</td>'+
				'<td>'+$academic_board+'</td>'+
				'<td>'+$course_age+'</td>'+
			'</tr>';
		$("#tbl_selected_course2 tbody").append($tr_data);
		$("#hidden_merge_selected_course2").val($course_id);
    });
    
    $("#cmd_reset_merge_course").click(function(){
    	$("#tbl_selected_course1 tbody").html('');
		$("#hidden_merge_selected_course1").val('');
    	$("#tbl_selected_course2 tbody").html('');
		$("#hidden_merge_selected_course2").val('');
    });
    
    $("#cmd_merge_course").click(function(){
    	var $course1 = $("#hidden_merge_selected_course1").val();
    	var $course2 = $("#hidden_merge_selected_course2").val();
    	var $result_area = $("#course_merge_result_area");
    	var $message = '';
    	var $button = $(this);
    	
    	if($course1.length<=0) {
    		$message = 'Please select a course from list1 to which you want to merge other courses.';
    		fn_alert_message($result_area, $message, "error");
    		return;
    	}
    	
    	if($course2.length<=0) {
    		$message = 'Please select a course from list2.';
    		fn_alert_message($result_area, $message, "error");
    		return;
    	}
    	
    	if($course1 == $course2) {
    		$message = 'Both courses are same. Please select different courses to merge.';
    		fn_alert_message($result_area, $message, "error");
    		return;
    	}
    	bootbox.confirm('Are you sure you want to merge these courses? This action is permanent and cannot be reversed.', function(result){
    		if(result) {
    			$button.button('loading');
				var $submit_data = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'fn_merge_course_ajax',
					pedagoge_callback_class : 'ControlPanelCourse',
					course1 : $course1,
					course2 : $course2
				};
				
				$.post($ajax_url, $submit_data, function(response) {
					try {
						var $response_data = $.parseJSON(response);
					
						var $error = $response_data.error,									 	
						 	$message = $response_data.message,
						 	$data = $response_data.data;				 	
						
						if($error) {
							fn_alert_message($result_area, $message, "error");
						} else {
							$("#cmd_reset_merge_course").click();
							fn_reload_merge_courses_list();
							fn_reload_course_list();
							fn_alert_message($result_area, $message, "success");
						}
					} catch( err ) {
						var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
						console.log($message);
					    
					} finally {
						$button.button('reset');
					}
			    }).complete(function() {
			    	$button.button('reset');
			    });
    		}
    	});
    });
    
});

function fn_reload_merge_courses_list() {
	$table_course_list1.ajax.reload();
	$table_course_list2.ajax.reload();
}

function course1_table_filter_column ( i, value ) {
    $('#tbl_course_list1').DataTable().column( i ).search( value ).draw();
}

function course2_table_filter_column ( i, value ) {
    $('#tbl_course_list2').DataTable().column( i ).search( value ).draw();
}