$(document).ready(function() {    
    $('#modal_find_teacher').on('hidden.bs.modal', function (e) {
		//Reset modal
		fn_reset_single_table_modal();
    });
    
    $("#cmd_single_table_modal_save").click(function(){
    	fn_save_single_table_modal_data();
    });
    
    $("#cmd_single_table_modal_delete").click(function(){
    	bootbox.confirm("Are you sure you want to delete this record?", function(result) {
    		if(result){
    			fn_delete_single_table_modal_data();
    		}
		});
    });
});

function fn_reset_single_table_modal() {	
	$("#modal_single_table_cud_title").html('Modal Title');
	$("#lbl_single_table_modal_textfield_label").html('Field Name');
	$("#div_single_table_modal_result_area").html('');	
	$("#txt_single_table_modal_text_field, #hidden_single_table_modal_record_id, #hidden_single_table_modal_save_action, #hidden_single_table_modal_delete_action, #hidden_single_table_modal_save_controller, #hidden_single_table_modal_delete_controller").val('');
	$("#hidden_single_table_modal_delete_callback, #hidden_single_table_modal_save_callback").val('');
}

function fn_show_single_table_modal($data_array) {
	$("#modal_single_table_cud_title").html($data_array.title);
	$("#lbl_single_table_modal_textfield_label").html($data_array.field_name);
		
	$("#txt_single_table_modal_text_field").val($data_array.field_value);
	
	$("#hidden_single_table_modal_record_id").val($data_array.record_id);
	$("#hidden_single_table_modal_save_action").val($data_array.save_action);
	$("#hidden_single_table_modal_delete_action").val($data_array.delete_action);
	$("#hidden_single_table_modal_save_controller").val($data_array.save_controller);
	$("#hidden_single_table_modal_delete_controller").val($data_array.delete_controller);
	$("#hidden_single_table_modal_delete_callback").val($data_array.delete_callback);
	$("#hidden_single_table_modal_save_callback").val($data_array.save_callback);
	$('#modal_single_table_cud').modal('show');
}

function fn_save_single_table_modal_data() {
	var $record_id = $("#hidden_single_table_modal_record_id").val(),
		$record_value = $("#txt_single_table_modal_text_field").val(),
		$save_controller = $("#hidden_single_table_modal_save_controller").val(),
		$save_action = $("#hidden_single_table_modal_save_action").val(),
		$save_callback = $("#hidden_single_table_modal_save_callback").val(),	
		$button = $("#cmd_single_table_modal_save"),
		$result_area = $("#div_single_table_modal_result_area"),
		$delete_button = $("#cmd_single_table_modal_delete");
	
	if($record_value.length<=0) {
		var $str_msg = "Please input record value!";
		fn_alert_message($result_area, $str_msg, "error");
		$("#txt_single_table_modal_text_field").select().focus();
		return false;
	}
	
	$button.button('loading');
	$result_area.html('');
	$delete_button.prop('disabled', true);
	
  	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : $save_action,
		pedagoge_callback_class : $save_controller,
		record_id : $record_id,
		record_value : $record_value
	};
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				fn_alert_message($result_area, $message, 'error');
			} else {					
				fn_alert_message($result_area, $message, 'success');				
				$("#modal_single_table_cud").modal('hide');
				if($save_callback.length>0) {
					var $save_callback_function = window[$save_callback];
					$save_callback_function();
				}								
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    fn_alert_message($result_area, $message, 'error');
		} finally {
			$button.button('reset');
		}			
    }).complete(function() {        	
    	$button.button('reset');
    	$delete_button.prop('disabled', false);
    });
	
}

function fn_delete_single_table_modal_data() {	// make sure bootbox is available for calling	
	/**
	 * If record id is not available then do not do anything.
	 */
	var $record_id = $("#hidden_single_table_modal_record_id").val(),
		$delete_controller = $("#hidden_single_table_modal_delete_controller").val(),
		$delete_action = $("#hidden_single_table_modal_delete_action").val(),
		$delete_callback = $("#hidden_single_table_modal_delete_callback").val(),		
		$button = $("#cmd_single_table_modal_delete"),
		$result_area = $("#div_single_table_modal_result_area"),
		$save_button = $("#cmd_single_table_modal_save");
	
	if($record_id.length<=0 || !$.isNumeric($record_id)) {
		var $str_msg = "Please select a record before trying to delete.";
		fn_alert_message($result_area, $str_msg, "error");
		return false;
	}
	
	$button.button('loading');
	$result_area.html('');
	$save_button.prop('disabled', true);
	
  	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : $delete_action,
		pedagoge_callback_class : $delete_controller,
		record_id : $record_id
	};
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				fn_alert_message($result_area, $message, 'error');
			} else {					
				fn_alert_message($result_area, $message, 'success');				
				$("#modal_single_table_cud").modal('hide');
				if($delete_callback.length>0) {
					var $delete_callback_function = window[$delete_callback];
					$delete_callback_function();	
				}
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    fn_alert_message($result_area, $message, 'error');
		} finally {
			$button.button('reset');
		}			
    }).complete(function() {        	
    	$button.button('reset');
    	$save_button.prop('disabled', false);
    });
}
