$(document).ready(function(){
	//fn_process_select_with_title();
	
	$("#select_master_locality").change(function(){
		var $master_locality = $(this).val();
		fn_load_clubbed_localities($master_locality);
	});
	
	$("#cmd_club_localities").click(function(){
		var $button = $(this);
		var $master_locality = $("#select_master_locality").val();
		var $children_localities = $("#select_children_localities").val();
		
		if(!$.isNumeric($master_locality)) {
			$("#select_master_locality").notify("Please select a locality!", 'warn');
			return false;
		}
		if($children_localities === null) {
			$("#select_children_localities").notify("Please select a locality!", 'warn');
			return false;
		}
		$button.button('loading');
		var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_save_clubbed_locality',
			pedagoge_callback_class : 'ControlPanelLocality',
			master_locality_id : $master_locality,
			children_localities : $children_localities
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					//fn_alert_message($result_area, $message, "error");
					$button.notify($message, 'error');
				} else {
					$.notify($message, 'success');
					$("#select_children_localities").val(null).trigger('change');
					fn_load_clubbed_localities($master_locality);
				}
			} catch( err ) {
				$button.button('reset');
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {
				$button.button('reset');
			}			
	    }).complete(function() {
	    	$button.button('reset');
	    });
	});
	
	$("#div_clubbed_localities_list_area").on('click', '.cmd_delete_clubbed_locality', function(){
		var $button = $(this);
		var $club_id = $button.data('club_id');
		if(!$.isNumeric($club_id)) {
			$.notify('Data is not available! Cannot delete!', 'warn');
			return false;
		}
		var $master_locality = $("#select_master_locality").val();
		bootbox.confirm("Are you sure, you want to delete this clubbed locality?", function(result) {
    		$button.button('loading');
	    	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_delete_clubbed_locality_ajax',
				pedagoge_callback_class : 'ControlPanelLocality',
				club_id : $club_id,			
			};
			
			$.post($ajax_url, $submit_data, function(response) {
				try {
					var $response_data = $.parseJSON(response);
				
					var $error = $response_data.error,									 	
					 	$message = $response_data.message,
					 	$data = $response_data.data;				 	
					
					if($error) {
						$.notify($message, 'error');
					} else {
						$.notify($message, 'success');
						fn_load_clubbed_localities($master_locality);
					}
				} catch( err ) {
					$button.button('reset');
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					console.log($message);
				    
				} finally {
					
				}			
		    }).complete(function() {
		    	$button.button('reset');
		    });
    	});
	});
	
});

function fn_load_clubbed_localities($locality_id) {
	var $result_area = $("#div_clubbed_localities_list_area");
	$result_area.html('<div class="alert alert-info"><h3>Loading clubbed localities list...</h3></div>');
	
	if(!$.isNumeric($locality_id)) {
		
		$.notify('Please select a locality from the master locality list.', 'warn');
		return false;
	}
	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_clubbed_locality',
		pedagoge_callback_class : 'ControlPanelLocality',
		master_locality_id : $locality_id,		
	};
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				//fn_alert_message($result_area, $message, "error");
				$.notify($message, 'danger');
			} else {
				$result_area.html($data);
			}
		} catch( err ) {
			
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			console.log($message);
		    
		} finally {
			
		}			
    }).complete(function() {
    });
}
