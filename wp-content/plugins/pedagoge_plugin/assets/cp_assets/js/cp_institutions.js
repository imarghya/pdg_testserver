var $table_institutes_list = null;
$(document).ready(function(){
	var $hidden_institutes_url = $("#hidden_institutes_url").val();
	var $hidden_institute_edit_url = $("#hidden_institute_edit_url").val();
	$table_institutes_list = $('#table_institutes_list').DataTable( {
		"dom" : "<'row'<'col-sm-12'>>" +
			"<'row'<'col-sm-12'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "processing" : true,
        "serverSide" : true,        
        "ajax" : {
            "url": $ajax_url,
            "data": function ( d ) {
                d.action = $pedagoge_visitor_ajax_handler;
                d.pedagoge_callback_function = 'fn_institutes_datatable';
                d.pedagoge_callback_class = 'ControlPanelInstitute';
            }
        },        
        "columnDefs": [{
                "targets": [ 2, 3, 4, 5],
                "visible": false,                
           },
        ],
        "rowCallback": function( row, data, index ) {
			var $slug = data[2];
			var $institute_id = data[0];
		    var $html = '<div style="width:170px!important;" class="btn-toolbar">'+
				'<div class="btn-group" data-toggle="buttons">'+
					'<a class="btn btn-info cmd_view_institute_info" title="View Institute profile" href="'+$hidden_institutes_url+'/'+$slug+'" target="_blank"><i class="fa fa-eye"></i></a>'+
					'<a class="btn btn-success cmd_edit_institute_info" title="Edit Institute Profile" href="'+$hidden_institute_edit_url+'/?role=institute&id='+$institute_id+'"  target="_blank"><i class="fa fa-pencil-square-o"></i></a>'+
					'<button class="btn btn-warning cmd_edit_institute_user_info" title="Edit Institute User Info"><i class="fa fa-user"></i></button>'+
					'<button class="btn btn-danger cmd_delete_institute" title="Delete Institute"><i class="fa fa-trash-o"></i></button>'+
				'</div>'+
		     '</div>';
			$('td:eq(10)', row).html( $html );
		}
    } );
    
    $(".select_fltr_institute").change(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	institutes_table_filter_column($data_column, $selected_value);
    });
    
    $(".txt_fltr_institute").onEnter(function(){
    	var $selected_value = $(this).val();
    	var $data_column = $(this).data('column');
    	institutes_table_filter_column($data_column, $selected_value);
    });
    
    $("#cmd_reset_institute_filters").click(function(){
    	institutes_table_reset_filters();
    });
    
    $('#table_institutes_list').on('click', '.cmd_delete_institute', function(){
    	var $row_data = $table_institutes_list.row( $(this).closest('tr')[0] ).data();
    	var $institute_id = $row_data[0];
    	
    	bootbox.confirm("Are you sure, you want to delete this Institute?", function(result) {
    		if(result) {
    			if(!$.isNumeric($institute_id) || $institute_id <= 0) {
					var $str_msg = "Error! Institite ID is not available";
		    		$.notify($str_msg, 'error');
		    		return false;
				}			
				
				var $submit_data = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'fn_delete_institute_ajax',
					pedagoge_callback_class : 'ControlPanelInstitute',
					institute_id : $institute_id,
				};
				try {
					$.post($ajax_url, $submit_data, function(response) {
						
						var $response_data = $.parseJSON(response);
					
						var $error = $response_data.error,									 	
						 	$message = $response_data.message,
						 	$data = $response_data.data;				 	
						
						if($error) {
							$.notify($message, 'error');
						} else {
							$table_institutes_list.ajax.reload();
							$.notify('Institute was deleted successfully!', 'success');
						}
						
				    });
			    } catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					console.log($message);
				    $.notify('Error Occured while trying to delete the Institute.', 'error');
				}
    		}    		
		});
    });
    
    //cmd_view_institute_info
    $('#table_institutes_list').on('click', '.cmd_view_institute_info', function(){
    	var $href = $(this).prop('href');
    	window.open($href); 
    });
    
    $('#table_institutes_list').on('click', '.cmd_edit_institute_info', function(){
    	var $href = $(this).prop('href');
    	window.open($href); 
    });
    
    //update user info of the institute
    $('#table_institutes_list').on('click', '.cmd_edit_institute_user_info', function(){
    	var $row_data = $table_institutes_list.row( $(this).closest('tr')[0] ).data();
		var txt_institute_name = $row_data[1];
		var txt_institute_mobile_no = $row_data[7];
		var txt_institute_contact_person_name = $row_data[8];
		var txt_institute_primary_contact_no = $row_data[9];
		var txt_institute_user_name = $row_data[5];
		var txt_institute_email_address = $row_data[6];
		var select_approve_institute = $row_data[10];
		var select_active_institute = $row_data[11];
		var select_verify_institute = $row_data[12];		
		var select_deleted_institute = $row_data[13];
		var hidden_institute_id = $row_data[0];
		var hidden_institute_user_id = $row_data[3];
		var hidden_institute_profile_id = $row_data[4];
		
		var $data_array = {};
		$data_array.txt_institute_name = txt_institute_name;
		$data_array.txt_institute_mobile_no = txt_institute_mobile_no;
		$data_array.txt_institute_contact_person_name = txt_institute_contact_person_name;
		$data_array.txt_institute_primary_contact_no = txt_institute_primary_contact_no;
		$data_array.txt_institute_user_name = txt_institute_user_name;
		$data_array.txt_institute_email_address = txt_institute_email_address;
		$data_array.select_approve_institute = select_approve_institute;
		$data_array.select_active_institute = select_active_institute;
		$data_array.select_verify_institute = select_verify_institute;
		$data_array.select_deleted_institute = select_deleted_institute;
		$data_array.hidden_institute_id = hidden_institute_id;
		$data_array.hidden_institute_user_id = hidden_institute_user_id;
		$data_array.hidden_institute_profile_id = hidden_institute_profile_id;
		fn_show_institute_info_modal($data_array);
    });
    
    $('#modal_update_institute_user_info').on('hidden.bs.modal', function (e) {
		//Reset modal
		fn_reset_institute_info_modal();
    });
    
    $(".cmd_save_updated_institute_info").click(function(){
    	var $button = $(this);
    	
    	var $result_area = $("#div_update_institute_result_area");
    	
    	var txt_institute_name = $("#txt_institute_name").val();
		var txt_institute_mobile_no = $("#txt_institute_mobile_no").val();
		var txt_institute_contact_person_name = $("#txt_institute_contact_person_name").val();
		var txt_institute_primary_contact_no = $("#txt_institute_primary_contact_no").val();
		var txt_institute_user_name = $("#txt_institute_user_name").val();
		var txt_institute_email_address = $("#txt_institute_email_address").val();
		var hidden_institute_id = $("#hidden_institute_id").val();
		var hidden_institute_user_id = $("#hidden_institute_user_id").val();
		var hidden_institute_profile_id = $("#hidden_institute_profile_id").val();
		
		var select_approve_institute = $("#select_approve_institute option:selected").val();
		var select_active_institute = $("#select_active_institute option:selected").val();
		var select_verify_institute = $("#select_verify_institute option:selected").val();
		var select_deleted_institute = $("#select_deleted_institute option:selected").val();
		var txt_institute_password = $("#txt_institute_password").val();
		
		var $message = '';
		
		if(!$.isNumeric(hidden_institute_id) || hidden_institute_id<=0) {
			//error
			$message = "Error! Institute ID is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if(!$.isNumeric(hidden_institute_user_id) || hidden_institute_user_id<=0) {
			//error
			$message = "Error! Institute User ID is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if(!$.isNumeric(hidden_institute_profile_id) || hidden_institute_profile_id<=0) {
			//error
			$message = "Error! Institute Profile ID is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		/*if(txt_institute_name.length <= 0) {
			$message = "Error! Institute Name is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}*/
		
		if(txt_institute_user_name.length <= 0) {
			$message = "Error! Institute User Name is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		if(txt_institute_email_address.length <= 0) {
			$message = "Error! Institute Email address is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		
		/*if(txt_institute_mobile_no.length != 10) {
			$message = "Error! Institute mobile number must be 10 digit long.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}*/
		
		if(select_approve_institute.length <= 0) {
			$message = "Error! Institute Approval status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		if(select_active_institute.length <= 0) {
			$message = "Error! Institute Activation status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		if(select_verify_institute.length <= 0) {
			$message = "Error! Institute Verification Status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		if(select_deleted_institute.length <= 0) {
			$message = "Error! Institute Deletion Status is not set.";
    		fn_alert_message($result_area, $message, "error");
    		return;
		}
		$button.button('loading');
		var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_update_institute_data',
			pedagoge_callback_class : 'ControlPanelInstitute',
			txt_institute_name : txt_institute_name,
			txt_institute_mobile_no : txt_institute_mobile_no,
			txt_institute_contact_person_name : txt_institute_contact_person_name,
			txt_institute_primary_contact_no : txt_institute_primary_contact_no,
			txt_institute_user_name : txt_institute_user_name,
			txt_institute_password : txt_institute_password,
			txt_institute_email_address : txt_institute_email_address,
			select_approve_institute : select_approve_institute,
			select_active_institute : select_active_institute,
			select_verify_institute : select_verify_institute,
			select_deleted_institute : select_deleted_institute,
			hidden_institute_id : hidden_institute_id,
			hidden_institute_user_id : hidden_institute_user_id,
			hidden_institute_profile_id : hidden_institute_profile_id,					
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {					
    				fn_alert_message($result_area, $message, "error");
				} else {
					$table_institutes_list.ajax.reload();
					$('#modal_update_institute_user_info').modal('hide');
					$.notify($message, 'success');
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {
			}			
	    }).complete(function() {
	    	//console.log('subject name reloading complete');
	    	$button.button('reset');
	    });
		
    });
});

function institutes_table_filter_column ( i, value ) {
    $('#table_institutes_list').DataTable().column( i ).search( value ).draw();
}
function institutes_table_reset_filters() {
	$(".select_fltr_institute, .txt_fltr_institute").val('');
	$table_institutes_list
	 .search( '' )
	 .columns().search( '' )
	 .draw();
}

function fn_reset_institute_info_modal() {	
	$("#div_update_institute_result_area").html('');		
	$("#txt_institute_name, #txt_institute_mobile_no, #txt_institute_contact_person_name,  #txt_institute_primary_contact_no").val('');
	$("#txt_institute_user_name, #txt_institute_password, #txt_institute_email_address, #select_approve_institute").val('');
	$("#select_active_institute, #select_verify_institute, #select_deleted_institute, #hidden_institute_id").val('');
	$("#hidden_institute_user_id, #hidden_institute_profile_id").val('');
}

function fn_show_institute_info_modal($data_array) {
	
	$("#txt_institute_name").val($data_array.txt_institute_name);
	$("#txt_institute_mobile_no").val($data_array.txt_institute_mobile_no);
	$("#txt_institute_contact_person_name").val($data_array.txt_institute_contact_person_name);
	$("#txt_institute_primary_contact_no").val($data_array.txt_institute_primary_contact_no);
	$("#txt_institute_user_name").val($data_array.txt_institute_user_name);
	
	$("#txt_institute_email_address").val($data_array.txt_institute_email_address);
	$("#select_approve_institute").val($data_array.select_approve_institute);
	$("#select_active_institute").val($data_array.select_active_institute);
	$("#select_verify_institute").val($data_array.select_verify_institute);
	$("#select_deleted_institute").val($data_array.select_deleted_institute);
	
	$("#hidden_institute_id").val($data_array.hidden_institute_id);
	$("#hidden_institute_user_id").val($data_array.hidden_institute_user_id);
	$("#hidden_institute_profile_id").val($data_array.hidden_institute_profile_id);
	
	$('#modal_update_institute_user_info').modal('show');
}