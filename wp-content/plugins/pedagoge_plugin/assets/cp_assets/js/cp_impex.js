$(document).ready(function() {
	//var isTableGenerator =false;
	$("#select_pdg_impex_type").change(function() {
		$(".cmd_export_data").prop('href', '#');
		var $impex_val = $("#select_pdg_impex_type option:selected").val();
		var $impex_title = $("#select_pdg_impex_type option:selected").text();
		var $impex_title_area = $(".impex_title_area");
		if ($impex_val.length <= 0) {
			$impex_title_area.html('Report title');
		} else {
			$impex_title_area.html($impex_title);
		}
	});
	$(".cmd_generate_impex").click(function() {
		//isTableGenerator = true;
		var $button = $(this);
		var $impex_type = $("#select_pdg_impex_type option:selected").val();
		var $impex_result_area = $("#div_impex_dump_area");
		var $result_loader = $(".img_impex_loader");

		if ($impex_type.length <= 0) {
			fn_alert_message($impex_result_area, 'Error! Please select a report to generate...', 'error');
			return false;
		}
		$result_loader.show();
		$impex_result_area.html('');
		$button.button('loading');
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_class : $pedagoge_callback_class,
			pedagoge_callback_function : 'fn_cp_impex_generator_ajax',
			impex_type : $impex_type
		};

		$.post($ajax_url, submit_data, function(response) {

			var $response_data = $.parseJSON(response);
			var $error = $response_data.error;
			var $message = $response_data.message;
			var $data = $response_data.data;

			if ($error) {
				fn_alert_message($impex_result_area, $message, 'error');

			} else {
				var $excel_url = $response_data.excel;
				$impex_result_area.html($data);
				//$(".cmd_export_data").prop('href', $excel_url);
			}
		}).complete(function() {
			$result_loader.hide();
			$button.button('reset');

		});
	});

});