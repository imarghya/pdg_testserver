$(document).ready(function(){
	
	$("#cmd_save_seo_options").click(function(){
		var $button = $(this);
		var $result_area = $("#div_seo_update_result_area");
		
		var $teacher_title = $("#txt_teacher_title_tag").val();
		var $teacher_meta = $("#txt_teacher_meta_tag").val();
		var $institute_title = $("#txt_institute_title_tag").val();
		var $institute_meta = $("#txt_institute_meta_tag").val();
		
		var $message = '';
		
		var $city_template_string = '{{{city}}}';
		var $subject_template_string = '{{{subject}}}';
		
		if($teacher_title.length <= 0 || $teacher_meta.length <= 0 || $institute_title.length <= 0 || $institute_meta.length <= 0) {
			$message = "Please fill the input fields properly and include "+$subject_template_string+" and "+$city_template_string+" in your templates.";
			fn_alert_message($result_area, $message, 'error');
			return false;
		}
		
		if($teacher_title.indexOf($subject_template_string) <= 0 || $teacher_title.indexOf($city_template_string) <= 0) {
			$message = "Please fill the teacher title input fields properly and include "+$subject_template_string+" and "+$city_template_string+" in your templates.";
			fn_alert_message($result_area, $message, 'error');
			return false;
		}
		
		if($institute_title.indexOf($subject_template_string) <= 0 || $institute_title.indexOf($city_template_string) <= 0) {
			$message = "Please fill the institute title input fields properly and include "+$subject_template_string+" and "+$city_template_string+" in your templates.";
			fn_alert_message($result_area, $message, 'error');
			return false;
		}
		
		if($teacher_meta.indexOf($subject_template_string) <= 0 || $teacher_meta.indexOf($city_template_string) <= 0) {
			$message = "Please fill the teacher meta input fields properly and include "+$subject_template_string+" and "+$city_template_string+" in your templates.";
			fn_alert_message($result_area, $message, 'error');
			return false;
		}
		
		if($institute_meta.indexOf($subject_template_string) <= 0 || $institute_meta.indexOf($city_template_string) <= 0) {
			$message = "Please fill the institute meta input fields properly and include "+$subject_template_string+" and "+$city_template_string+" in your templates.";
			fn_alert_message($result_area, $message, 'error');
			return false;
		}
		var $teacher_title = $("#txt_teacher_title_tag").val();
		var $teacher_meta = $("#txt_teacher_meta_tag").val();
		var $institute_title = $("#txt_institute_title_tag").val();
		var $institute_meta = $("#txt_institute_meta_tag").val();
		
		$button.button('loading');
    	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_save_seo_options',
			pedagoge_callback_class : 'ControlPanelSeo',
			teacher_title : $teacher_title,
			teacher_meta : $teacher_meta,
			institute_title : $institute_title,
			institute_meta : $institute_meta,
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, "error");
				} else {
					/**
					 * reset merge form
					 * reload tables 
					 */					
					fn_alert_message($result_area, $message, "success");
				}
			} catch( err ) {
				$button.button('reset');
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {
				
			}			
	    }).complete(function() {
	    	$button.button('reset');
	    });
	});
	
	
	$("#cmd_generate_seo_combination").click(function(){
		var $button = $(this);
		var $result_area = $(".div_seo_generate_url_result_area");
		var $message = '';		
		$button.button('loading');
    	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_create_seo_combination',
			pedagoge_callback_class : 'ControlPanelSeo',
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, "error");
				} else {
					fn_alert_message($result_area, $message, "success");
				}
			} catch( err ) {
				$button.button('reset');
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {}			
	    }).complete(function() {
	    	$button.button('reset');
	    });
	});
	
	$("#cmd_update_subject_slugs").click(function(){
		var $button = $(this);
		var $result_area = $(".div_seo_generate_url_result_area");
		var $message = '';		
		$button.button('loading');
    	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_update_subjects_slug',
			pedagoge_callback_class : 'ControlPanelCourse',
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, "error");
				} else {
					fn_alert_message($result_area, $message, "success");
				}
			} catch( err ) {
				$button.button('reset');
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {}			
	    }).complete(function() {
	    	$button.button('reset');
	    });
	});
	
	$("#cmd_delete_seo_urls_cache").click(function(){
		var $button = $(this);
		var $result_area = $(".div_seo_generate_url_result_area");
		var $message = '';		
		$button.button('loading');
    	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_clear_seo_urls_cache',
			pedagoge_callback_class : 'ControlPanelSeo',
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, "error");
				} else {
					fn_alert_message($result_area, $message, "success");
				}
			} catch( err ) {
				$button.button('reset');
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				console.log($message);
			    
			} finally {}			
	    }).complete(function() {
	    	$button.button('reset');
	    });
	});	
});
