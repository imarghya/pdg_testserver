$(document).ready(function() {
	var $modal_student_img_cropper = null,	
	$student_image_cropped = false,
	$student_image_loaded = false;
	
	$("#frm_students_profile").submit(function(e){
	    return false;
	});
	
	
	$("#frm_students_profile").validationEngine({scroll: false});
	
	$('#txt_dob').datetimepicker({
		format: 'YYYY-MM-DD',		
		showClear: true,
		showClose: true,
		minDate: moment().year(1947),
		maxDate: moment().year(moment().year()-1),
		useCurrent: false
	});
	
	$('.select_control').select2();
	
	$(".cmd_load_student_image").click(function(event){
    	event.preventDefault();
    	$("#fileinput_student_profile_image").click();
    });
	
	$("#fileinput_student_profile_image").change(function(event) {    	
    	//event.preventDefault();
    	var $student_profile_image_uploaded_files = event.target.files;
    	var $file_lengh = $student_profile_image_uploaded_files.length;
    	    	
    	var $str_files_status = '',
    	$file_name = '',
    	$file_size = 0,
    	$file_extension = '',    	
    	$is_image_error = false;
    	
    	if($student_profile_image_uploaded_files.length > 0) {
    		$file_name = $student_profile_image_uploaded_files[0].name;
	    	$file_size = $student_profile_image_uploaded_files[0].size;
	    	$file_extension = $file_name.substring($file_name.lastIndexOf('.') + 1).toLowerCase();
	    	
	    	var $str_size_kb_mb = 'KB',
	    	iSize = 0;
	    	
	    	if($file_size > 0) {
				$file_size = $file_size/1024;
				iSize = (Math.round($file_size * 100) / 100);
				
				if( iSize > 500 ) {
					$is_image_error = true;
					$str_files_status += 'Error! Profile picture size should not be more than 500 KB.<br/>';
				}
			}
			
			if($file_extension === 'gif' || $file_extension === 'png' || $file_extension === 'jpeg' || $file_extension === 'jpg') {
				
			} else {
				$is_image_error = true;
				$str_files_status += 'Error! Only Image Files having any of the following extensions (.jpg, .jpeg, .png, .gif) should be loaded!';
			}
			
			if(!$is_image_error) {				
				
				var reader = new FileReader();
				
	            reader.onload = function (e) {				
					$('#img_student_cropping').attr('src', e.target.result);	                
					var $modal_student_img_cropper_content = $(".modal_student_img_cropping").clone(true);
					var $image = $modal_student_img_cropper_content.find('#img_student_cropping');
					var cropBoxData;
					var canvasData;
					
					$image.cropper({
						aspectRatio: 4.5 / 4.5,
						autocrop: true,
						modal: true,
						autoCropArea: 0.8,
						cropBoxResizable: false,
						dragMode: 'move',
						width: 160,
						height: 90,
						minContainerWidth: 600,
						minContainerHeight: 400,
						crop: function(e) {
						    // Output the result data for cropping image.
						}
			        });
		
					$modal_student_img_cropper = bootbox.dialog({
			            title: "Crop the Image",                
			                message:$modal_student_img_cropper_content,
			                buttons: {
			                	cancel: {
			                    label: "Cancel",
			                    className: "btn-primary",
			                    callback: function () {
			                    }
			                },
			                success: {
			                    label: "Save",
			                    className: "btn-success btn_save_emp_cat",
			                    callback: function () {
			                    	fn_get_cropped_images($image);
			                        return false;
			                    }
			                }
			            }                
			        });
	            };
	            reader.readAsDataURL($student_profile_image_uploaded_files[0]);
			}
    	}
    	
    	if($str_files_status.length>0) {
    		$str_files_status = '<div class="alert alert-primary">'+$str_files_status+'</div>';
    	}
    	
    	$("#div_student_profile_img_status").html($str_files_status);
    });
    
    $("#cmd_save_profile").click(function(event){
    	event.preventDefault();
    	
    	var $btn = $(this),
    		$hidden_user_id = $("#hidden_user_id").val(),
    		$hidden_profile_info_id = $("#hidden_profile_info_id").val(),
    		$txt_first_name = $("#txt_first_name").val(),
			$txt_last_name = $("#txt_last_name").val(),
			$txt_mobile = $("#txt_mobile").val(),
			$txt_alternative_contact_no = $("#txt_alternative_contact_no").val(),
			dob_year = $("#dob_year").val(),
			dob_month = $("#dob_month").val(),
			dob_day = $("#dob_day").val(),
			$select_gender = $("#select_gender").val(),
			$img_student_picture = $("#img_student_picture").val(),
			$txt_address = $("#txt_address").val(),
			// $select_country = $("#select_country").val(),
			// $select_state = $("#select_state").val(),
			$select_city = $("#select_city").val(),
			$img_password_reset_loader = $("#img_password_reset_loader"),			
			$div_save_result = $("#div_save_result");

			$txt_dob = dob_year + '-' + dob_month + '-' + dob_day;
			//alert($txt_dob);
		$data_to_save = {};
		$data_to_save.hidden_user_id = $hidden_user_id;
		$data_to_save.hidden_profile_info_id = $hidden_profile_info_id;
		$data_to_save.txt_first_name = $txt_first_name;
		$data_to_save.txt_last_name = $txt_last_name;
		$data_to_save.txt_mobile = $txt_mobile;
		$data_to_save.txt_alternative_contact_no = $txt_alternative_contact_no;
		$data_to_save.txt_dob = $txt_dob;
		$data_to_save.select_gender = $select_gender;
		$data_to_save.txt_address = $txt_address;		
		$data_to_save.select_city = $select_city;
			
    	
    	if( $("#frm_students_profile").validationEngine('validate', {scroll: false}) ) {
    		var $upload_data = new FormData();
			$upload_data.append('student_data', JSON.stringify($data_to_save));
			
			var $profile_image = $("#img_student_picture").prop('src');
			$profile_image = fn_dataURItoBlob($profile_image);
			$upload_data.append("student_profile_image", $profile_image);
			
			$btn.hide();
			$img_password_reset_loader.show();
			$div_save_result.html('<h2>Please wait while we are storing your data in the database.</h2>');
			
			$upload_data.append("nonce", $ajaxnonce);
			$upload_data.append("action", $pedagoge_visitor_ajax_handler);
			$upload_data.append("pedagoge_callback_function", 'fn_save_student_profile_ajax');
			$upload_data.append("pedagoge_callback_class", 'ControllerProfileStudent');
		    
		    $.ajax({
				url : $ajax_url,
			    data : $upload_data,
			    processData : false, 
			    contentType : false,
			    type : 'POST',			    
			    success : function(response) {
			    	try {
					
						var $response_data = $.parseJSON(response);				
						var $error = $response_data.error, 
							//$error_type = $response_data.error_type,
						 	$message = $response_data.message;
						 	//$data = $response_data.data;
						if($error) {
							fn_alert_message($div_save_result, $message, 'error');
						} else {
							fn_alert_message($div_save_result, $message, 'success');
							setTimeout(function() {
								location.reload(true); // do this later.
								//window.location = $loading_url;
							},300);
						}
					} catch(err) {
						$btn.show();
						console.log('error occured '+err);
					}
			    },
			    complete:function(){
			    	$btn.show();
					$img_password_reset_loader.hide();
			    }
			});
    	}
    });
    
    function fn_get_cropped_images($image) {
		var $result = $image.cropper('getCroppedCanvas');
		var $image_data = $result.toDataURL("image/jpeg", 0.7);
		$("#img_student_picture").attr('src', $image_data);
		$modal_student_img_cropper.modal('hide');
		$modal_student_img_cropper = null;
		$student_image_cropped = true;
		//reset fileinput_student_profile_image
		resetFormElement($("#fileinput_student_profile_image"));
	}
	
});
