$(function() {
	/**Numeric Only **/
	$(".numeric").numeric();
	$(".integer").numeric(false, function() { console.log("Integers only"); this.value = ""; this.focus(); });
	$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
	$(".positive-integer").numeric({ decimal: false, negative: false }, function() { console.log("Positive integers only"); this.value = ""; this.focus(); });
    $(".decimal-2-places").numeric({ decimalPlaces: 2 });
    
    $('[data-toggle="tooltip"]').tooltip();
    
    $("input:checkbox, input:radio").uniform();
    fn_load_secret();
});
		
function fn_load_secret() {
	var submit_data = {					
		action : 'pedagoge_visitor_ajax_handler',
		pedagoge_callback_function : 'fn_load_secret_key',
		pedagoge_callback_class : 'PDGContactForm',
	};
	
	$.post($ajax_url, submit_data, function(response) {
		$ajaxnonce = response;				 
    }).complete(function(){
    	setTimeout(function()
		{
			fn_load_secret();
		}, 180000);
    });
}

/**
 * This function changes page's url structure without reloading the page. 
 */
function ChangeUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: title, Url: url };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        console.log("Browser does not support HTML5.");
    }
}


/**
 * Function to reset datatables.
 * @param $data_table - an instance of datatable
 * @param $cache - pipeline cache of datatable 
 */
function fn_reset_datatables($data_table, $cache) {
	$data_table = (typeof $data_table === "undefined") ? '' : $data_table;
	$cache = (typeof $cache === "undefined") ? '' : $cache;
	
	if((typeof $data_table === "string") || (typeof $cache === "string")) {
		
	} else {
		var oSettings = $data_table.fnSettings();
	    for(iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) 
	    {
	        oSettings.aoPreSearchCols[ iCol ].sSearch = '';
	    }
	    $cache.iCacheLower=-1;
	    $data_table.fnDraw();
	}
}
/*********Datatable Pipeline Setkey GetKey functions *******************/
function fnSetKey( aoData, sKey, mValue )
{
    for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
    {
        if ( aoData[i].name == sKey )
        {
            aoData[i].value = mValue;
        }
    }
}
 
function fnGetKey( aoData, sKey )
{
    for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
    {
        if ( aoData[i].name == sKey )
        {
            return aoData[i].value;
        }
    }
    return null;
}
/*********Datatable Pipeline Setkey GetKey functions ends *******************/

/**
 * This Function returns an element's Label
 */
function fn_get_elements_label($input_element) {
	var $element = null;	
	if( typeof $input_element === "string") {
		$element = $("#" + $input_element);	
	} else {
		$element = $input_element;
	}
	var $label = $("label[for='"+$element.prop('id')+"']");
	if ($label.length == 0) {
		$label = $element.closest('label');
	}	
	return $label;
}
/**
 * This function converts base64/URLEncoded data component to raw binary data held in a string
 */
function fn_dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

function resetFormElement(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}

var $alert_time_out_var=null;

/**
 * Function to create various types of messages such as Info/Error/Success/Warning
 * @param (string) (Required) Area where message will be displayed
 * @param (string) (Required) The content of the alert message.
 * @param (string) (Required) Types of alert (error/info/success/warning)  
 * @param (true/false) (Required) whether the message will be removed after 6 seconds or not
 */
function fn_alert_message($div_id, $message, $alert_type, $timer)
{
	clearTimeout($alert_time_out_var);
	
	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000 : $timer;
	
	var $result_area = null;
	if( typeof $div_id === "string") {
		$result_area = jQuery( "#" + $div_id );	
	} else {
		$result_area = $div_id;
	}
		
	var $str_message = '';
	
	
	$result_area.html('');
	
	switch($alert_type)
	{
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'info':
			$str_message ='<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'success':
			$str_message ='<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'warning':
			$str_message ='<div class="alert alert-block alert-warning fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'blank_success':
			$str_message ='<span class="label label-success fade in blink_me">'+$message+'</span>';
			break;
		case 'blank_error':
			$str_message ='<span class="label label-warning fade in blink_me">'+$message+'</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html( $str_message );
	
	if($timer)
	{
		$alert_time_out_var = setTimeout(function()
		{
			$result_area.html('');
		}, $timer);		
	}
}

//On Enter
jQuery.fn.onEnter = function(callback) {
	this.keyup(function(e) {
		if(e.keyCode == 13) {
			e.preventDefault();
			if (typeof callback == 'function') {
				callback.apply(this);
			}
		}
	} );
    return this;
};

/**
 * Reset Input Fields where field names (ID) are seperated by '|'
 * 
 * @param fields list 
 */
function fn_reset_input_fields( $fields_list ) {
	var $fields_array = $fields_list.split( '|' );
	
	for(var i = 0; i < $fields_array.length; i++) {
		var $field_id = trim( $fields_array[ i ] );
		$( "#" + $field_id ).val('');
	}
}

/**
 * Reset HTML Fields where field names(ID) are seperated by '|' 
 */
function fn_reset_html_fields( $fields_list ) {
	var $fields_array = $fields_list.split( '|' );
	
	for(var i = 0; i < $fields_array.length; i++) {
		var $field_id = trim( $fields_array[ i ] );
		$( "#" + $field_id ).html('');
	}
}

/**
 * trim function
 */
function trim(str)
{
	return str.replace(/^\s+|\s+$/g,"");
}

//ref http://stackoverflow.com/questions/2855865/jquery-regex-validation-of-e-mail-address
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

/**
 * @desc ucfirst javascript function
 */
function ucfirst(str) {
  //  discuss at: http://phpjs.org/functions/ucfirst/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  //   example 1: ucfirst('kevin van zonneveld');
  //   returns 1: 'Kevin van zonneveld'
  str += '';
  var f = str.charAt(0)
    .toUpperCase();
  return f + str.substr(1);
}


/*
 *
 * Copyright (c) 2006-2014 Sam Collett (http://www.texotela.co.uk)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version 1.4.1
 * Demo: http://www.texotela.co.uk/code/jquery/numeric/
 *
 */
(function(factory){if(typeof define === 'function' && define.amd){define(['jquery'], factory);}else{factory(window.jQuery);}}(function($){$.fn.numeric=function(config,callback){if(typeof config==="boolean"){config={decimal:config,negative:true,decimalPlaces:-1}}config=config||{};if(typeof config.negative=="undefined"){config.negative=true}var decimal=config.decimal===false?"":config.decimal||".";var negative=config.negative===true?true:false;var decimalPlaces=typeof config.decimalPlaces=="undefined"?-1:config.decimalPlaces;callback=typeof callback=="function"?callback:function(){};return this.data("numeric.decimal",decimal).data("numeric.negative",negative).data("numeric.callback",callback).data("numeric.decimalPlaces",decimalPlaces).keypress($.fn.numeric.keypress).keyup($.fn.numeric.keyup).blur($.fn.numeric.blur)};$.fn.numeric.keypress=function(e){var decimal=$.data(this,"numeric.decimal");var negative=$.data(this,"numeric.negative");var decimalPlaces=$.data(this,"numeric.decimalPlaces");var key=e.charCode?e.charCode:e.keyCode?e.keyCode:0;if(key==13&&this.nodeName.toLowerCase()=="input"){return true}else if(key==13){return false}var allow=false;if(e.ctrlKey&&key==97||e.ctrlKey&&key==65){return true}if(e.ctrlKey&&key==120||e.ctrlKey&&key==88){return true}if(e.ctrlKey&&key==99||e.ctrlKey&&key==67){return true}if(e.ctrlKey&&key==122||e.ctrlKey&&key==90){return true}if(e.ctrlKey&&key==118||e.ctrlKey&&key==86||e.shiftKey&&key==45){return true}if(key<48||key>57){var value=$(this).val();if($.inArray("-",value.split(""))!==0&&negative&&key==45&&(value.length===0||parseInt($.fn.getSelectionStart(this),10)===0)){return true}if(decimal&&key==decimal.charCodeAt(0)&&$.inArray(decimal,value.split(""))!=-1){allow=false}if(key!=8&&key!=9&&key!=13&&key!=35&&key!=36&&key!=37&&key!=39&&key!=46){allow=false}else{if(typeof e.charCode!="undefined"){if(e.keyCode==e.which&&e.which!==0){allow=true;if(e.which==46){allow=false}}else if(e.keyCode!==0&&e.charCode===0&&e.which===0){allow=true}}}if(decimal&&key==decimal.charCodeAt(0)){if($.inArray(decimal,value.split(""))==-1){allow=true}else{allow=false}}}else{allow=true;if(decimal&&decimalPlaces>0){var dot=$.inArray(decimal,$(this).val().split(""));if(dot>=0&&$(this).val().length>dot+decimalPlaces){allow=false}}}return allow};$.fn.numeric.keyup=function(e){var val=$(this).val();if(val&&val.length>0){var carat=$.fn.getSelectionStart(this);var selectionEnd=$.fn.getSelectionEnd(this);var decimal=$.data(this,"numeric.decimal");var negative=$.data(this,"numeric.negative");var decimalPlaces=$.data(this,"numeric.decimalPlaces");if(decimal!==""&&decimal!==null){var dot=$.inArray(decimal,val.split(""));if(dot===0){this.value="0"+val;carat++;selectionEnd++}if(dot==1&&val.charAt(0)=="-"){this.value="-0"+val.substring(1);carat++;selectionEnd++}val=this.value}var validChars=[0,1,2,3,4,5,6,7,8,9,"-",decimal];var length=val.length;for(var i=length-1;i>=0;i--){var ch=val.charAt(i);if(i!==0&&ch=="-"){val=val.substring(0,i)+val.substring(i+1)}else if(i===0&&!negative&&ch=="-"){val=val.substring(1)}var validChar=false;for(var j=0;j<validChars.length;j++){if(ch==validChars[j]){validChar=true;break}}if(!validChar||ch==" "){val=val.substring(0,i)+val.substring(i+1)}}var firstDecimal=$.inArray(decimal,val.split(""));if(firstDecimal>0){for(var k=length-1;k>firstDecimal;k--){var chch=val.charAt(k);if(chch==decimal){val=val.substring(0,k)+val.substring(k+1)}}}if(decimal&&decimalPlaces>0){var dot=$.inArray(decimal,val.split(""));if(dot>=0){val=val.substring(0,dot+decimalPlaces+1);selectionEnd=Math.min(val.length,selectionEnd)}}this.value=val;$.fn.setSelection(this,[carat,selectionEnd])}};$.fn.numeric.blur=function(){var decimal=$.data(this,"numeric.decimal");var callback=$.data(this,"numeric.callback");var negative=$.data(this,"numeric.negative");var val=this.value;if(val!==""){var re=new RegExp("^" + (negative?"-?":"") + "\\d+$|^" + (negative?"-?":"") + "\\d*" + decimal + "\\d+$");if(!re.exec(val)){callback.apply(this)}}};$.fn.removeNumeric=function(){return this.data("numeric.decimal",null).data("numeric.negative",null).data("numeric.callback",null).data("numeric.decimalPlaces",null).unbind("keypress",$.fn.numeric.keypress).unbind("keyup",$.fn.numeric.keyup).unbind("blur",$.fn.numeric.blur)};$.fn.getSelectionStart=function(o){if(o.type==="number"){return undefined}else if(o.createTextRange&&document.selection){var r=document.selection.createRange().duplicate();r.moveEnd("character",o.value.length);if(r.text=="")return o.value.length;return Math.max(0,o.value.lastIndexOf(r.text))}else{try{return o.selectionStart}catch(e){return 0}}};$.fn.getSelectionEnd=function(o){if(o.type==="number"){return undefined}else if(o.createTextRange&&document.selection){var r=document.selection.createRange().duplicate();r.moveStart("character",-o.value.length);return r.text.length}else return o.selectionEnd};$.fn.setSelection=function(o,p){if(typeof p=="number"){p=[p,p]}if(p&&p.constructor==Array&&p.length==2){if(o.type==="number"){o.focus()}else if(o.createTextRange){var r=o.createTextRange();r.collapse(true);r.moveStart("character",p[0]);r.moveEnd("character",p[1]-p[0]);r.select()}else{o.focus();try{if(o.setSelectionRange){o.setSelectionRange(p[0],p[1])}}catch(e){}}}}}));
