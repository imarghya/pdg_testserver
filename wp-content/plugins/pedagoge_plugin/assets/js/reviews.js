$(document).ready(function() {
	$(".cmd_send_rejection_mail").click(function(event){
		var $button = $(this);
		var $review_text_area = $("#txt_review_rejection_reason");
		var $review_id = $("#hidden_review_id").val();
		var $div_rejection_result = $(".div_rejection_result");
		var $div_rejection_successful = $(".div_rejection_successful");
		var $review_text = $review_text_area.val();
		
		if(!$.isNumeric($review_id)) {
			$str_return = '<div class="alert alert-warning">Error! Review ID is not set. Please try again.</div>';
			fn_alert_message($div_rejection_result, $str_return, 'error');
			return;	
		}
		
		if($review_text.length<=10) {
			$str_return = '<div class="alert alert-warning">You must write more about why the review got rejected.</div>';
			fn_alert_message($div_rejection_result, $str_return, 'error');
			return;	
		}
		
		$button.button('loading');
		var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_reject_review_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
			review_id : $review_id,
			review_text : $review_text			
		};

		$.post($ajax_url, $submit_data, function(response) {
			var $response_data = $.parseJSON(response);
			var $error = $response_data.error;
			var $result = $response_data.message;
			var $data = $response_data.data;
			if($error) {
				fn_alert_message($div_rejection_result, $result, 'error');
			} else {
				fn_alert_message($div_rejection_successful, $result, 'success');
			}		

        }).complete(function() {
        	$button.button('reset');
        });
	});
});