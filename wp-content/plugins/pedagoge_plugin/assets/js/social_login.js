$( document ).ready( function () {
	
	/*
	 * Check if the G Auth is loaded or not.
	 * If selector not found then return false
	 * If yes then return true
	 * else keep checking
	 * */
	function waitForGauth ( $selector, callback_fn ) {
		if ( $selector.length < 1 ) {
			//console.log('gmail not available');
			return false;
		}
		else if ( $selector.html().length > 0 ) {
			//console.log('gmail available');
			callback_fn();
			return true;
		}
		else {
			//console.log('gmail not available waiting');
			setTimeout( function () {
				waitForGauth( $selector, callback_fn );
			}, 500 );
		}
	}
	
	//If g auth is loaded then enable the #cmd_login_google btn
	waitForGauth( $( '.g-signin2' ), function () {
		$( '#cmd_login_google' ).prop( 'disabled', false ).removeClass( 'disabled' );
	} );
	
	$( "#cmd_login_google" ).click( function () {
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signIn().then( function ( googleUser ) {
			onSignIn( googleUser );
		} );
		
	} );
	
	$( "#div_login_signup" ).on( 'click', '#cmd_continue_user', function () {
		//#div_continue_user
		var $result_area = $( "#div_continue_user" ),
			$button = $( this ),
			$selected_user_role = $( "#select_social_user_role option:selected" ).val(),
			$str_return = '',
			$social_login_user_secred_key = $( "#social_login_user_secred_key" ).val(),
			$social_login_user_id = $( "#social_login_user_id" ).val(),
			$mobile_number = $( "#txt_social_login_mobile_number" ).val();
			
		if ( $mobile_number.length != 10 ) {
			$str_return = 'Please input your 10 digit mobile number...';
			pdg_show_alerts( $result_area, $str_return, 'error' );
			return;
		}
		
		if ( $selected_user_role.length <= 0 ) {
			$str_return = 'Please select User Roles...';
			pdg_show_alerts( $result_area, $str_return, 'error' );
			return;
		}
		$button.button( 'loading' );
		var $submit_data = {
			nonce: $ajaxnonce,
			action: $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function: 'fn_social_signup',
			pedagoge_callback_class: 'ControllerLogin',
			social_user_role: $selected_user_role,
			social_login_user_secred_key: $social_login_user_secred_key,
			social_login_user_id: $social_login_user_id,
			mobile_number : $mobile_number
		};
		
		$.post( $ajax_url, $submit_data, function ( response ) {
			var $response_data = $.parseJSON( response );
			var $error = $response_data.error,
				$error_type = $response_data.error_type,
				$message = $response_data.message,
				$next_url = $response_data.url;
			
			if ( $error ) {
				pdg_show_alerts( $result_area, $message, 'error' );
			} else {
				pdg_show_alerts( $result_area, $message, 'success' );
				window.location.href = $next_url;
			}
			
		} ).complete( function () {
			$button.button( 'reset' );
		} );
		
	} );
} );

/**
 * Google SignIn Function 
 */
function onSignIn ( googleUser ) {
	// Useful data for your client-side scripts:
	var profile = googleUser.getBasicProfile(),
		$google_user_id = profile.getId(),
		$google_user_first_name = profile.getGivenName(),
		$google_user_last_name = profile.getFamilyName(),
		$google_user_full_name = profile.getName(),
		$google_user_email = profile.getEmail(),
		$login_signup_block = $( "#div_login_signup" ),
		$result_area = $( "#div_login_result" ),
		$str_return = '';
	
	/**
	 * 1 blockui login section
	 * 2 login and redirect
	 */
	if ( $google_user_email.length > 0 && $google_user_id.length > 0 && $google_user_first_name.length > 0 && $google_user_last_name.length > 0 ) {
		$str_return = 'Please wait.. Signing in...';
		pdg_show_alerts( $result_area, $str_return, 'success' );
		$( '#login_form' ).find( 'input, textarea, button, select' ).attr( 'disabled', 'disabled' );
		$( '#login_form' ).hide();
		if ( typeof block == 'function' ) {
			$login_signup_block.block( { message: $str_return } );
		} else {
			//console.log( $str_return );
		}
		
		
		var $submit_data = {
			nonce: $ajaxnonce,
			action: $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function: 'fn_social_login',
			pedagoge_callback_class: 'ControllerLogin',
			social_user_id: $google_user_id,
			social_user_first_name: $google_user_first_name,
			social_user_last_name: $google_user_last_name,
			social_user_full_name: $google_user_full_name,
			social_user_email: $google_user_email,
			social_login_location: 'google'
		};
		
		$.post( $ajax_url, $submit_data, function ( response ) {
			var $response_data = $.parseJSON( response );
			var $error = $response_data.error,
				$error_type = $response_data.error_type,
				$loggedin = $response_data.loggedin,
				$message = $response_data.message,
				$next_url = $response_data.url;
			if ( $error ) {
				pdg_show_alerts( $result_area, $message, 'error' );
				$login_signup_block.unblock();
			} else {
				if ( $loggedin == true ) {
					$( ".pdg_social_login_icons" ).hide();
					pdg_show_alerts( $result_area, $message, 'success' );
					window.location.href = $next_url;
				} else {
					$str_return = 'Please fill up the details below to continue...';
					pdg_show_alerts( $result_area, $str_return, 'success' );
					$( ".pdg_social_login_icons" ).hide();
					var $signup_form = $response_data.signup_form;
					$login_signup_block.html( $signup_form );
					$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
				}
			}
			
		});
		
	} else {
		$str_return = 'Google Login failed. Please try again...';
		pdg_show_alerts( $result_area, $str_return, 'error' );
	}
};

//////////////////////////////////////////////////Alert Message Code//////////////////////////////

var $alert_time_out_var = null;

/**
 * Function to create various types of messages such as Info/Error/Success/Warning
 * @param (string) (Required) Area where message will be displayed
 * @param (string) (Required) The content of the alert message.
 * @param (string) (Required) Types of alert (error/info/success/warning)
 * @param (true/false) (Required) whether the message will be removed after 6 seconds or not
 */
function fn_alert_message ( $div_id, $message, $alert_type, $timer ) {
	clearTimeout( $alert_time_out_var );
	
	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000 : $timer;
	
	var $result_area = null;
	if ( typeof $div_id === "string" ) {
		$result_area = jQuery( "#" + $div_id );
	} else {
		$result_area = $div_id;
	}
	
	var $str_message = '';
	
	
	$result_area.html( '' );
	
	switch ( $alert_type ) {
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'info':
			$str_message = '<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'success':
			$str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'warning':
			$str_message = '<div class="alert alert-block alert-warning fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'blank_success':
			$str_message = '<span class="label label-success fade in blink_me">' + $message + '</span>';
			break;
		case 'blank_error':
			$str_message = '<span class="label label-warning fade in blink_me">' + $message + '</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html( $str_message );
	
	if ( $timer ) {
		$alert_time_out_var = setTimeout( function () {
			$result_area.html( '' );
		}, $timer );
	}
}