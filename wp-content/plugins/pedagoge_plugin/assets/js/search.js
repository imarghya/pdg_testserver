var $ajaxnonce = $ajaxnonce;
$(document).ready(function() {	
	$('.select_control').select2();	
	$('.multiple_select_control').select2({ 
        closeOnSelect: false,
        placeholder: "Select an Option"
    });
	
	fn_load_secret();
	
	$("#frm_search_sidebar").submit(function(e){
	    return false;
	});

	
	$("#search_result_area").on('mouseenter', '.store-item-image', function(event){
		var $teacher_float = $(this).find('.float_teacher');
		var $institute_float = $(this).find('.float_institute');
		
		var $float_div = '';
		if($teacher_float.length>0) {
			$float_div = $teacher_float;
			$float_div.text("Teacher");
		}
		if($institute_float.length>0) {
			$float_div = $institute_float;
			$float_div.text("Institute");
		}
		$float_div.css("font-weight", "bold");
		$float_div.css("font-size", "15px");
		$float_div.css("width", "100%");
		/*$float_div.css("padding-right", "5%");*/
		$float_div.css("color", "#333");
		$float_div.css("height", "20px");
		$float_div.css("text-align", "center");
	});
	
	$("#search_result_area").on('mouseleave', '.store-item-image', function(event){
		var $teacher_float = $(this).find('.float_teacher');
		var $institute_float = $(this).find('.float_institute');
		var $float_div = '';
		if($teacher_float.length>0) {
			$float_div = $teacher_float;
		}
		if($institute_float.length>0) {
			$float_div = $institute_float;
		}
		$float_div.text("");
	});
	

	$('#search_result_pagination').on('click', '#li_pagination_first', function(){
		var $search_result_pagination = $('#search_result_pagination');
		var $span_pagination = $search_result_pagination.find('.active');

		$span_pagination.removeClass('active');
		fn_pedagoge_search(1);
		window.scrollTo(0,0);
		$('.span_pagination[data-page_no="1"]').addClass('active');
	});

	$('#search_result_pagination').on('click', '#li_pagination_left', function(){		
		var $search_result_pagination = $('#search_result_pagination');
		var $span_pagination = $search_result_pagination.find('.active');
		var $page_no = $span_pagination.data('page_no');
		if($page_no == 1){
			/*Do Nothing*/
		}
		else{			
			$span_pagination.removeClass('active');
			$page_no -= 1;			
			fn_pedagoge_search($page_no);
			$('.span_pagination[data-page_no="'+$page_no+'"]').addClass('active');
			if($page_no == 1){
				$('#li_pagination_left').addClass('disabled');
			}	
		}
		window.scrollTo(0,0);
	});

	$('#search_result_pagination').on('click', '#li_pagination_right', function(){		
		var $search_result_pagination = $('#search_result_pagination');
		var $span_pagination = $search_result_pagination.find('.active');
		var $page_no = $span_pagination.data('page_no');
		var $last_li = $('#search_result_pagination li:nth-last-child(3)');
		var $last_page_no = $last_li.data('page_no');
		
					
		$span_pagination.removeClass('active');
		$page_no += 1;			
		fn_pedagoge_search($page_no);
		window.scrollTo(0,0);
		$('.span_pagination[data-page_no="'+$page_no+'"]').addClass('active');
		
	});
	
	fn_load_subjects_localities();
	
		/***show interst start**/

	/*$(".cmd_show_interest").click(function(){
			
	  	var $message = '';
		var $result_area = $("#show_interest_error");
		var $submit_button = $(this);
	  	var $loader = $(".img_locality_loader"),
		$loader_result_area = $(".span_loader_result");


		
	  	//event.preventDefault();
	  	var $button = $(this),
		  	$txt_modal_user_name = $("#txt_modal_user_name"),
		 	//$txt_modal_user_id = $("#txt_modal_user_id"),
		 	$txt_modal_phone = $("#txt_modal_phone"),
		 	$txt_modal_locality = $("#txt_modal_locality"),
		 	$txt_modal_request = $("#txt_modal_request");


		 	var $txt_phone_value = $txt_modal_phone.val();
			var $txt_locality_value = $txt_modal_locality.val();
			var $txt_request_value = $txt_modal_request.val();
			var $txt_user_name_value = $txt_modal_user_name.val();

			// alert($txt_phone_value);
			if($txt_user_name_value.length <= 2) {				
			$message = "You need to input your name!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}

		if($txt_phone_value.length <= 9) {				
			$message = "You need to input your phone number!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}
		
		if($txt_locality_value.length <= 3) {				
			$message = "You need to input your locality!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}
		
		$loader.show();
		$result_area.html('');
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'show_interest_ajax',
			pedagoge_callback_class : 'ControllerSearch',
			modal_user_name : $txt_modal_user_name.val(),
			//modal_user_id : $txt_modal_user_id.val(),
			
			modal_phone : $txt_modal_phone.val(),
		 	modal_locality : $txt_modal_locality.val(),
		 	modal_request : $txt_modal_request.val()
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message;				 	
				console.log($error);
				console.log($message);
				if($error) {
					fn_alert_message($result_area, $message, 'error');
				} else {
					//fn_alert_message($result_area, $message, 'success');
					var $str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
					$("#show_interest_modal_body").html($str_message);
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'error');
			} finally {
				//$submit_button.button('reset');
				$loader.hide();
			}
			
        }).complete(function() {
        	$(".cmd_show_interest").hide();
        	$('#myModalLabel').html('Thanks for Contacting Us');
        	$('#show_interest_modal_body').html('Your response has been received.We will contact you soon.&#9787;');
        	
        	$loader.hide();
        	
        });
        
  	});*/
		/**end of show interest**/
	//cmd_recommend
	$("#search_result_area").on('click', '.cmd_recommend', function(event){
		event.preventDefault();
		var $recommend_btn = $(this);
		var $current_logged_in_user = parseInt($recommend_btn.data('current_user'));
		var $teacher_user_id = parseInt($recommend_btn.data('teacher_user_id'));
		var $has_recommended = $recommend_btn.data('recommended');
		var $recommendation_count = $recommend_btn.find('.recommendation_count');
		var $recommend_image = $recommend_btn.find('.img-recommend');		
		var $recommendation_counter = parseInt($recommendation_count.html());
		
		if($current_logged_in_user > 0 && $teacher_user_id > 0) {
			var submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'search_recommend_teacher_ajax',
				pedagoge_callback_class : 'ControllerSearch',
				recommended_by : $current_logged_in_user,
				recommended_to : $teacher_user_id,
				has_recommended : $has_recommended
			};
			
			$.post($ajax_url, submit_data, function(response) {
				try {
					var $response_data = $.parseJSON(response);
					
					var $error = $response_data.error, 
						$error_type = $response_data.error_type,				 	
					 	$message = $response_data.message;
					
					if($error) {
					} else {
						if($has_recommended == 'yes') {							
							/**
							 * 1. change icon to orange
							 * 2. less counter
							 * 3. change has recommended to no 
							 */
							$recommend_image.attr('src', $unrecommended_icon);
							$recommendation_counter--;
							$recommendation_count.html($recommendation_counter);
							$recommend_btn.attr('data-recommended', 'no');
							$recommend_btn.data('recommended', 'no');
							
						} else {
							/**
							 * 1. change icon to green
							 * 2. increase counter
							 * 3. change has recommended to yes 
							 */
							$recommend_image.attr('src', $recommended_icon);
							$recommendation_counter++;
							$recommendation_count.html($recommendation_counter);
							$recommend_btn.attr('data-recommended', 'yes');
							$recommend_btn.data('recommended', 'yes');
						}
					}
				} catch(err) {					
				} finally {					
				}
				 
	        }).complete(function() {	        	
	        });
		}		
	});
		
	$("#cmd_search_by_subject_and_locality, #cmd_search_institute_teacher, #cmd_search_by_locality").click(function(event){
		fn_pedagoge_search();
	});
		
	$("#cmd_reset_search").click(function(event){
		fn_reset_search_forms();
		fn_pedagoge_search();
	});
	
	$('#chk_filter_institution, #chk_filter_teacher, .chk_teaching_location_type, .chk_filter_teaching_xp, '+
	'.chk_class_type_filter, .chk_filter_academic_board, .chk_class_timing_filter, .chk_fees_filter').change(function() {
		fn_pedagoge_search();
    });
    
    $("#chk_all_day").change(function(){
    	if($(this).is(':checked')) {
    		$(".chk_days_taught").prop('checked', true);
    	} else {
    		$(".chk_days_taught").prop('checked', false);
    	}
    	$.uniform.update();
    	fn_pedagoge_search();
    });
    
    $(".chk_days_taught").change(function() {
    	var $is_all_checked = true;
    	var $chk_days_taught = $('.chk_days_taught');
    	
    	$chk_days_taught.each(function(){
    		if($(this).is(':checked')) {
    			
    		} else {
    			$is_all_checked = false;
    		}
    	});
    	if($is_all_checked) {
    		$("#chk_all_day").prop('checked', true);
    	} else {
    		$("#chk_all_day").prop('checked', false);
    	}
    	$.uniform.update();
    	fn_pedagoge_search();
    });
    
	$("#search_result_pagination").on('click', '.cmd_pagination_btn', function(event){
		event.preventDefault();
		var $cmd_pagination_btn = $(this);
		var $span_pagination = $cmd_pagination_btn.parent();
		var $page_no = $span_pagination.data('page_no');
		var $is_active = $span_pagination.hasClass('active');
		
		if($.isNumeric($page_no) && $page_no>0 && !$is_active) {
			fn_pedagoge_search($page_no);
			window.scrollTo(0,0);
			$("#search_result_area").find(".span_pagination").removeClass('active');
			$span_pagination.addClass('active');
		}
	});
    
});

function fn_reset_search_forms() {
	$("#txt_search_subject, #txt_tutor_insti_search_name").val('');
	$("#frm_search_sidebar select").select2("val", "");
	$("#chk_filter_institution, #chk_filter_teacher, .chk_teaching_location_type, .chk_filter_teaching_xp, "+
	".chk_class_type_filter, .chk_filter_academic_board, .chk_class_timing_filter, .chk_fees_filter, #chk_all_day, .chk_days_taught").prop('checked', false);
	$.uniform.update();
}

function fn_pedagoge_search($pagination_no) {
	if ($pagination_no === undefined || $pagination_no === null) {
	     $pagination_no = 1;
	}
	
	var $subject_name = $("#txt_search_subject").val(),
	$insti_teacher_name = $("#txt_tutor_insti_search_name").val(),
	$locality_name = $("#select_search_locality").val(),
	$location_type_array = [],	
	$search_type = 'filter',
	$filter_institute_teacher = '',
	$teaching_xp_array = [],
	$class_type_array = [],
	$academic_board_array = [],
	$class_timing_array = [],
	$fees_range_array = [],
	$days_taught_array = [];
	//$found_locality_name = $("#txt_search_locality").val();
	
	var $chk_teaching_location_type = $(".chk_teaching_location_type");
	$chk_teaching_location_type.each(function(index){
		var $location_type = $(this);
		var $location_id = $location_type.val();
		if($location_type.is(':checked')) {
			$location_type_array.push($location_id);
		}
	});
	
	var $chk_teaching_xp = $(".chk_filter_teaching_xp");
	$chk_teaching_xp.each(function(index){
		var $teaching_xp = $(this);
		var $teaching_xp_id = $teaching_xp.val();
		if($teaching_xp.is(':checked')) {
			$teaching_xp_array.push($teaching_xp_id);
		}
	});
	
	var $chk_class_type = $(".chk_class_type_filter");
	$chk_class_type.each(function(index){
		var $class_type = $(this);
		var $class_type_id = $class_type.val();
		if($class_type.is(':checked')) {
			$class_type_array.push($class_type_id);
		}
	});
	
	var $chk_academic_board = $(".chk_filter_academic_board");
	$chk_academic_board.each(function(index){
		var $academic_board = $(this);
		var $academic_board_id = $academic_board.val();
		if($academic_board.is(':checked')) {
			$academic_board_array.push($academic_board_id);
		}
	});
	
	var $chk_fees_range = $(".chk_fees_filter");
	$chk_fees_range.each(function(index){
		var $fees_range = $(this);
		var $fees_range_id = $fees_range.val();
		if($fees_range.is(':checked')) {
			$fees_range_array.push($fees_range_id);
		}
	});
	
	var $chk_days_taught = $(".chk_days_taught");
	$chk_days_taught.each(function(index){
		var $day_taught = $(this);
		var $day_taught_name = $day_taught.val();
		if($day_taught.is(':checked')) {
			$days_taught_array.push($day_taught_name);
		}
	});
	
	if($subject_name.length <= 0 
		&& $insti_teacher_name.length <= 0 
		&& $locality_name == null 
		&& $location_type_array.length <= 0 
		&& $teaching_xp_array.length <= 0 
		&& $class_type_array.length <= 0 
		&& $academic_board_array.length <= 0 
		&& $class_timing_array.length <= 0 
		&& $fees_range_array.length <= 0 
		&& $days_taught_array.length <= 0 
		//&& $found_locality_name.length <= 0
	) {
		$search_type = 'reset';
	}
	
	var $teacher_filter_checked = $("#chk_filter_teacher").is(':checked');
	var $institute_filter_checked = $("#chk_filter_institution").is(':checked');
	
	if($teacher_filter_checked && $institute_filter_checked) {
		$filter_institute_teacher = '';
	} else if(!$teacher_filter_checked && !$institute_filter_checked) {
		$filter_institute_teacher = '';
	} else if($teacher_filter_checked) {
		$filter_institute_teacher = '';
		$filter_institute_teacher = 'teacher';
	} else if($institute_filter_checked) {
		$filter_institute_teacher = 'institution';
	}
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_search_ajax',
		pedagoge_callback_class : 'ControllerSearch',
		current_user_id : $current_user_id,
		current_user_role : $current_user_role,		
		subject_name : $subject_name,
		insti_teacher_name: $insti_teacher_name,
		locality_name : $locality_name,
		location_type_array : $location_type_array,
		filter_institute_teacher : $filter_institute_teacher,
		teaching_xp_array : $teaching_xp_array,
		class_type_array : $class_type_array,
		academic_board_array : $academic_board_array,
		class_timing_array : $class_timing_array,
		fees_range_array : $fees_range_array,
		days_taught_array : $days_taught_array,
		//found_locality_name : $found_locality_name,
		page_no : $pagination_no,
		search_type : $search_type
	};
	var $str_message = '<div class="col-md-12 alert alert-info"><h2>Searching for data...</h2></div>';
	$("#search_result_area").html($str_message);
	$.post($ajax_url, submit_data, function(response) {
		try {
			
			var $response_data = $.parseJSON(response);				
			var $error = $response_data.error, 
				$error_type = $response_data.error_type,				 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;
			 				 	
			if($error) {
				
				switch($error_type) {
					case 'security':
						var $str_message = '<div class="col-md-12 alert alert-warning"><h2>'+$message+'</h2></div>';
						$("#search_result_area").html($str_message);
						break;
					case 'nothing_found':
						$("#search_result_area").html($data.result);
						$("#search_result_pagination").html($data.pagination);
						break;
					default:
						break;
				}
			} else {
				
				$("#search_result_area").html($data.result);

				var $str_raw_pagination = $data.pagination;
				var $str_pagination = '';
				var $page_count = $str_raw_pagination.length;
				if($pagination_no == 1){
					$str_pagination = $str_pagination + '<li class="disabled" id="li_pagination_first"><a href="javascript:void(0)"><i class="fa fa-angle-double-left"></i></a></li>';
					$str_pagination = $str_pagination + '<li class="disabled" id="li_pagination_left"><a href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>';
				}
				else{
					$str_pagination = $str_pagination + '<li id="li_pagination_first"><a href="javascript:void(0)"><i class="fa fa-angle-double-left"></i></a></li>';
					$str_pagination = $str_pagination + '<li id="li_pagination_left"><a href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>';
				}

				if($page_count>10){
					if($pagination_no == 1){
						for(var i=0;i<10;i++){
							$str_pagination = $str_pagination + $str_raw_pagination[i];
						}
					}
					else if($pagination_no >= 1 && $pagination_no < $page_count){
						if($page_count < $pagination_no + 6){
							for(var i=$pagination_no - 4;i<$page_count;i++){
								$str_pagination = $str_pagination + $str_raw_pagination[i];	
							}	
						}
						else{
							for(var i=$pagination_no - 4;i<$pagination_no + 6;i++){
								$str_pagination = $str_pagination + $str_raw_pagination[i];	
							}
						}
					}
					else{
						for(var i=$pagination_no - 10;i<$page_count;i++){
							$str_pagination = $str_pagination + $str_raw_pagination[i];	
						}
					}
				}
				else{
					for(var i=0;i<$page_count;i++){
							$str_pagination = $str_pagination + $str_raw_pagination[i];
					}
				}

				$str_pagination = $str_pagination + '<li id="li_pagination_right"><a href="javascript:void(0)"><i class="fa fa-angle-right"></i></a></li>';
				/*$str_pagination = $str_pagination + '<li id="li_pagination_last"><a href="javascript:void(0)"><i class="fa fa-angle-double-right"></i></a></li>';*/

				$("#search_result_pagination").html($str_pagination);
			}
			
		} catch(err) {			
			console.log('error occured '+err);
		}				 
    }).complete(function() {
    	$('[data-toggle="tooltip"]').tooltip();
    });	
}

function fn_load_subjects_localities() {
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_subjects_localities',
		pedagoge_callback_class : 'ControllerSearch',
	};
	//console.log($ajaxnonce);
	$.post($ajax_url, submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
			
			var $error = $response_data.error, 
				$error_type = $response_data.error_type,				 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;
			
			if($error) {
			} else {
				var $subjects_list_array = $data.subjects;
				$( "#txt_search_subject" ).autocomplete({
			      source: function(request, response) {
						var results = $.ui.autocomplete.filter($subjects_list_array, request.term);
				        response(results.slice(0, 10));
					}
			    });
			    var $teacher_name_array = $data.teacher_names;
			    $( "#txt_tutor_insti_search_name" ).autocomplete({
			      source: function(request, response) {
						var results = $.ui.autocomplete.filter($teacher_name_array, request.term);
				        response(results.slice(0, 10));
					}
			    });
			    
			    var $locality_array = $data.locality;
			    $("#select_search_locality").select2({
				  data: $locality_array
				});
				
				var $searched_locality = getParameterByName('pdg_locality');				
				if($searched_locality.length>0) {
					var selectedItems =[];
					selectedItems.push($searched_locality);					
					$('#select_search_locality').val($searched_locality).trigger("change");;
					
				}
			}
		} catch(err) {					
		} finally {
		}
		 
    }).complete(function() {	        	
    });
}

function fn_load_secret() {
	var submit_data = {					
		action : 'pedagoge_visitor_ajax_handler',
		pedagoge_callback_function : 'fn_load_secret_key',
		pedagoge_callback_class : 'PDGContactForm',
	};
	
	$.post($ajax_url, submit_data, function(response) {
		$ajaxnonce = response;				 
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/*************************************************Ratings***********************************************/
$(document).ready(function() {
	$(".pdg_star_rating_teacher, .pdg_star_rating_institute").rating();
		
	$('.pdg_star_rating_teacher').on('rating.change', function(event, value, caption) {	    
		var $star_value = value;
		$(this).data('starvalue', $star_value);
		fn_calculate_star_average_teacher();
	});
	
	$('.pdg_star_rating_institute').on('rating.change', function(event, value, caption) {	    
		var $star_value = value;
		$(this).data('starvalue', $star_value);
		fn_calculate_star_average_institute();
	});
		
	$('.pdg_overall_rating_teacher').on('rating.change', function(event, value, caption) {	    
		var $star_value = value;
		$(this).data('starvalue', $star_value);		
	});
	
	$('.pdg_overall_rating_institute').on('rating.change', function(event, value, caption) {	    
		var $star_value = value;
		$(this).data('starvalue', $star_value);		
	});
		
	$("#search_result_area").on('click', '.cmd_rate_teacher', function(event){
		fn_reset_teacher_rating();
		var $cmd_rate_teacher = $(this);
		var $current_user_id = $cmd_rate_teacher.data('current_user'),
		$teacher_user_id = $cmd_rate_teacher.data('teacher_user_id');
		$("#hidden_teacher_modal_current_user_id").val($current_user_id);
		$("#hidden_teacher_modal_teacher_user_id").val($teacher_user_id);
	});
	
	$("#search_result_area").on('click', '.cmd_rate_institute', function(event){
		fn_reset_institute_rating();
		var $cmd_rate_institute = $(this);
		var $current_user_id = $cmd_rate_institute.data('current_user'),
		$teacher_user_id = $cmd_rate_institute.data('teacher_user_id');
		$("#hidden_institute_modal_current_user_id").val($current_user_id);
		$("#hidden_institute_modal_institute_user_id").val($teacher_user_id);
	});
	
	$("#cmd_save_review_teacher").click(function(){
		var $review_text = $("#txt_review_field_teacher").val(),
		$review_result_area = $("#div_rating_result_area_teacher"),
		$current_user_id = $("#hidden_teacher_modal_current_user_id").val(),
		$teacher_user_id = $("#hidden_teacher_modal_teacher_user_id").val(),		
		$is_rating_below_average = false,
		$pdg_star_rating = $(".pdg_star_rating_teacher"),
		$rating_data = [];
		var $rating_button = $("#search_result_area").find(".cmd_rate_teacher[data-teacher_user_id='" + $teacher_user_id + "']");
		
		var $overall_rating = $(".pdg_overall_rating_teacher").data('starvalue');
		
		var $is_anonymous = 'no';
		if($("#chk_anonymous_review_teacher").is(':checked')){
			$is_anonymous = 'yes';
		}		

		$review_result_area.html('');
		
		if($current_user_id <= 0 || $teacher_user_id <= 0) {
			var $str_return = 'Error! User is not logged in!';
			fn_alert_message($review_result_area, $str_return, 'error');
			
			setTimeout(function(){ 
				$('#rating_modal_teacher').modal('hide'); 
			}, 4500);
			return;
		}
		
		$pdg_star_rating.each(function(){
			var $star_value = $(this).data('starvalue'),
			$star_name = $(this).prop('name');
			
			if(!$.isNumeric($star_value)) {
				$star_value = 0;
			} else {
				$star_value = parseFloat($star_value);
			}
			if($star_value <= 3.4) {
				$is_rating_below_average = true;
			}
			var $star_data = {
				'name' : $star_name,
				'value' : $star_value
			};
			$rating_data.push($star_data);			
		});
		
		if($.isNumeric($overall_rating) && $overall_rating > 3.5) {
			$is_rating_below_average = false;
		} else {
			$is_rating_below_average = true;
		}
		
		if($review_text.length < 10) {			
			var $str_return = 'Please write your review...';			
			fn_alert_message($review_result_area, $str_return, 'error');
			return;
		}
		
		var $rating_loader = $("#div_rating_loader_teacher");
		var $rating_submit_btn = $(this);
		
		$rating_submit_btn.button('loading');
		$rating_loader.show();
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'search_rating_and_review_ajax',
			pedagoge_callback_class : 'ControllerSearch',
			rated_by : $current_user_id,
			rated_to : $teacher_user_id,
			rating_data : $rating_data,
			review_text : $review_text,
			overall_rating : $overall_rating,
			is_anonymous : $is_anonymous
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message;
				
				if($error) {
					fn_alert_message($review_result_area, $message, 'error');
					$rating_submit_btn.button('reset');
				} else {
					fn_alert_message($review_result_area, $message, 'success');					 
					$('#rating_modal_teacher').modal('hide');
					$.notify($message, "success");
					/**
					 *remove hyperlink
					 * remove button's modal target 
					 */
					$rating_button.data('target', '');					
					$rating_button.removeAttr('data-target');
				}
			} catch(err) {
				$rating_submit_btn.button('reset');
			} finally {
				$rating_loader.hide();
			}
			 
        }).complete(function() {
        	$rating_loader.hide();
        });
	});
	
	$("#cmd_save_review_institute").click(function(){
		var $review_text = $("#txt_review_field_institute").val(),
		$review_result_area = $("#div_rating_result_area_institute"),
		$current_user_id = $("#hidden_institute_modal_current_user_id").val(),
		$institute_user_id = $("#hidden_institute_modal_institute_user_id").val(),		
		$is_rating_below_average = false,
		$pdg_star_rating = $(".pdg_star_rating_institute"),
		$rating_data = [];
		var $rating_button = $("#search_result_area").find(".cmd_rate_institute[data-teacher_user_id='" + $institute_user_id + "']");
		
		var $overall_rating = $(".pdg_overall_rating_institute").data('starvalue');
		
		var $is_anonymous = 'no';
		if($("#chk_anonymous_review_institute").is(':checked')){
			$is_anonymous = 'yes';
		}		

		$review_result_area.html('');
		
		if($current_user_id <= 0 || $institute_user_id <= 0) {
			var $str_return = 'Error! User is not logged in!';
			fn_alert_message($review_result_area, $str_return, 'error');
			
			setTimeout(function(){ 
				$('#rating_modal_institute').modal('hide'); 
			}, 4500);
			return;
		}
		
		$pdg_star_rating.each(function(){
			var $star_value = $(this).data('starvalue'),
			$star_name = $(this).prop('name');
			
			if(!$.isNumeric($star_value)) {
				$star_value = 0;
			} else {
				$star_value = parseFloat($star_value);
			}
			if($star_value <= 3.4) {
				$is_rating_below_average = true;
			}
			var $star_data = {
				'name' : $star_name,
				'value' : $star_value
			};
			$rating_data.push($star_data);			
		});
		
		if($.isNumeric($overall_rating) && $overall_rating > 3.5) {
			$is_rating_below_average = false;
		} else {
			$is_rating_below_average = true;
		}
		
		if($review_text.length < 10) {			
			var $str_return = 'Please write your review...';			
			fn_alert_message($review_result_area, $str_return, 'error');
			return;
		}
		
		var $rating_loader = $("#div_rating_loader_institute");
		var $rating_submit_btn = $(this);
		
		$rating_submit_btn.button('loading');
		$rating_loader.show();
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'search_rating_and_review_ajax',
			pedagoge_callback_class : 'ControllerSearch',
			rated_by : $current_user_id,
			rated_to : $institute_user_id,
			rating_data : $rating_data,
			review_text : $review_text,
			overall_rating : $overall_rating,
			is_anonymous : $is_anonymous
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message;
				
				if($error) {
					fn_alert_message($review_result_area, $message, 'error');
					$rating_submit_btn.button('reset');
				} else {
					fn_alert_message($review_result_area, $message, 'success');					 
					$('#rating_modal_institute').modal('hide');
					$.notify($message, "success");
					/**
					 *remove hyperlink
					 * remove button's modal target 
					 */
					$rating_button.data('target', '');					
					$rating_button.removeAttr('data-target');
				}
			} catch(err) {
				$rating_submit_btn.button('reset');
			} finally {
				$rating_loader.hide();
			}
			 
        }).complete(function() {
        	$rating_loader.hide();
        });
	});
	
});

function fn_reset_teacher_rating() {
	$("#div_rating_result_area_teacher").html('');
	$("#txt_review_field_teacher, #hidden_teacher_modal_current_user_id, #hidden_teacher_modal_teacher_user_id").val('');
	$("#div_rating_loader_teacher").hide();
	$('.pdg_star_rating_teacher, .pdg_overall_rating_teacher').rating('reset');
	$('#chk_anonymous_review_teacher').prop('checked', false);
	$.uniform.update('#chk_anonymous_review_teacher');
}

function fn_reset_institute_rating() {
	$("#div_rating_result_area_institute").html('');
	$("#txt_review_field_institute, #hidden_institute_modal_current_user_id, #hidden_institute_modal_institute_user_id").val('');
	$("#div_rating_loader_institute").hide();
	$('.pdg_star_rating_institute, .pdg_overall_rating_institute').rating('reset');
	$('#chk_anonymous_review_institute').prop('checked', false);
	$.uniform.update('#chk_anonymous_review_institute');
}

function fn_calculate_star_average_teacher() {
	var $total_value = 0, $counter = 0, $average_val = 0;
	
	$('.pdg_star_rating_teacher').each(function(index){
		$total_value += parseFloat($(this).data('starvalue'));		
		$counter++;
	});
	if($total_value > 0) {
		$average_val = $total_value/$counter;
	}
	$(".pdg_overall_rating_teacher").rating('update', $average_val);
}

function fn_calculate_star_average_institute() {
	var $total_value = 0, $counter = 0, $average_val = 0;
	
	$('.pdg_star_rating_institute').each(function(index){
		$total_value += parseFloat($(this).data('starvalue'));		
		$counter++;
	});
	if($total_value > 0) {
		$average_val = $total_value/$counter;
	}
	$(".pdg_overall_rating_institute").rating('update', $average_val);
}