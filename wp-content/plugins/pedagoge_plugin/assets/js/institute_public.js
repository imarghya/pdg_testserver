$(document).ready(function(){
  	$('.carousel').carousel();
	
	$(".btn-signup").click(function(event){
		event.preventDefault();
	});
	
  	/**code by pallav**/

  	$("#cmd_phone").click(function(){
		window.location.href = "callto://+919883331570";
	});

	$("#cmd_whatsapp").click(function(){
		window.location.href = "https://web.whatsapp.com/";
	});

  	$(".cmd_show_interest").click(function(){

  		var $message = '';
		var $result_area = $("#show_interest_error_connect");
		var $submit_button = $(this);
	  	var $loader = $(".img_locality_loader"),
		$loader_result_area = $(".span_loader_result"); 


		$loader.show();
		$loader_result_area.html('');
	  	
	  	//event.preventDefault();
	  	var $button = $(this),
	  		$txt_modal_user_name = $("#txt_modal_user_name"),
	  		$txt_modal_user_id = $("#txt_modal_user_id"),
		 	$txt_modal_phone = $("#txt_modal_phone"),
		 	$txt_modal_locality = $("#txt_modal_locality");
		 	$txt_modal_request = $("#txt_modal_request");
		 	console.log($txt_modal_request);

		 	var $txt_phone_value = $txt_modal_phone.val();
			var $txt_locality_value = $txt_modal_locality.val();
			var $txt_request_value = $txt_modal_request.val();
			var $txt_user_name_value = $txt_modal_user_name.val();

			// alert($txt_phone_value);
			if($txt_user_name_value.length <= 2) {				
			$message = "You need to input your name!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}

			
		if($txt_phone_value.length <= 9) {				
			$message = "You need to input your phone number!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}

		
		
		if($txt_locality_value.length <= 3) {				
			$message = "You need to input your locality!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}
		
		/*if($txt_request_value.length <= 3) {				
			$message = "You need to input your message!";
			fn_alert_message($result_area, $message, 'error');
			$loader.hide();
			return;
		}*/
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'show_interest_ajax',
			pedagoge_callback_class : 'ControllerInstitute',
			modal_user_name : $txt_modal_user_name.val(),
			modal_user_id : $txt_modal_user_id.val(),
			modal_phone : $txt_modal_phone.val(),
		 	modal_locality : $txt_modal_locality.val(),
		 	modal_request : $txt_modal_request.val()
		};

		console.log($submit_data);
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, 'error');
				} else {
					//fn_alert_message($result_area, $message, 'success');
					var $str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
					$("#show_interest_modal_body").html($str_message);
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'error');
			} finally {
				//$submit_button.button('reset');
				$loader.hide();
			}
			
        }).complete(function() {
        	
        	$(".cmd_show_interest").hide();
        	$('#myModalLabel').html('Thanks for Showing Interest in this profile');
        	$("#show_interest_modal_body").html("Your response is received.We will contact you soon.&#9787;");

        	$loader.hide();
        });
  	});
	/**End of code by pallav**/
	

	$(".div_recommendation_area").on('click', '.cmd_recommend', function(event){
		event.preventDefault();
		var $recommend_btn = $(".cmd_recommend");
		var $current_logged_in_user = parseInt($recommend_btn.data('current_user'));
		var $teacher_user_id = parseInt($recommend_btn.data('teacher_user_id'));
		var $has_recommended = $recommend_btn.data('recommended');
		var $recommendation_count = $recommend_btn.find('.recommendation_count');
		var $recommend_image = $recommend_btn.find('.img-recommend');		
		var $recommendation_counter = parseInt($recommendation_count.html());
		
		var $cmd_recommend_button = $(".cmd_recommend_button");
		
		if($current_logged_in_user > 0 && $teacher_user_id > 0) {
			var submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'search_recommend_teacher_ajax',
				pedagoge_callback_class : 'ControllerSearch',
				recommended_by : $current_logged_in_user,
				recommended_to : $teacher_user_id,
				has_recommended : $has_recommended
			};
			
			$.post($ajax_url, submit_data, function(response) {
				try {
					var $response_data = $.parseJSON(response);
					
					var $error = $response_data.error, 
						$error_type = $response_data.error_type,				 	
					 	$message = $response_data.message;
					
					if($error) {
					} else {
						if($has_recommended == 'yes') {							
							/**
							 * 1. change icon to orange
							 * 2. less counter
							 * 3. change has recommended to no 
							 */
							$recommend_image.attr('src', $unrecommended_icon);							
							$recommendation_counter--;							
							$recommendation_count.html($recommendation_counter);
							$recommend_btn.attr('data-recommended', 'no');
							$recommend_btn.data('recommended', 'no');
							$cmd_recommend_button.html('Recommend');							
						} else {
							/**
							 * 1. change icon to green
							 * 2. increase counter
							 * 3. change has recommended to yes 
							 */

							$recommend_image.attr('src', $recommended_icon);							
							$recommendation_counter++;							
							$recommendation_count.html($recommendation_counter);
							$recommend_btn.attr('data-recommended', 'yes');
							$recommend_btn.data('recommended', 'yes');	
							$cmd_recommend_button.html('Unrecommend');						
						}
					}
				} catch(err) {					
				} finally {					
				}
				 
	        }).complete(function() {	        	
	        });
		}		
	});
	
	$('#rating_modal').on('shown.bs.modal', function() {		
        $("#div_rating_result_area").html('');
	});
	
	$(".pdg_star_rating, .pdg_overall_rating").rating();
	
	$(".rated_by_user").rating({displayOnly: true, step: 0.5});
		
	$('.pdg_star_rating').on('rating.change', function(event, value, caption) {	    
		var $star_value = value;
		$(this).data('starvalue', $star_value);
		fn_calculate_star_average();
	});
	
	$('.pdg_overall_rating').on('rating.change', function(event, value, caption) {	    
		var $star_value = value;
		$(this).data('starvalue', $star_value);		
	});
	
	/**************
	 *Save review data 
	 */
	$("#cmd_save_review").click(function(){
		var $review_text = $("#txt_review_field").val(),
		$review_result_area = $("#div_rating_result_area"),				
		$cmd_rating_done = $(".cmd_rating_done"),
		$current_user_id = $cmd_rating_done.data('current_user'),
		$teacher_user_id = $cmd_rating_done.data('teacher_user_id'),		
		$is_rating_below_average = false,
		$pdg_star_rating = $(".pdg_star_rating"),
		$rating_data = [];
		
		var $overall_rating = $(".pdg_overall_rating").data('starvalue');
		
		var $is_anonymous = 'no';
		if($("#chk_anonymous_review").is(':checked')){
			$is_anonymous = 'yes';
		}		

		$review_result_area.html('');
		
		if($current_user_id <= 0 || $teacher_user_id <= 0) {
			var $str_return = '<div class="alert alert-warning">Error! User is not logged in!</div>';
			$review_result_area.html($str_return);
			
			setTimeout(function(){ 
				$('#rating_modal').modal('hide'); 
			}, 4500);
			return;
		}
		
		$pdg_star_rating.each(function(){
			var $star_value = $(this).data('starvalue'),
			$star_name = $(this).prop('name');
			
			if(!$.isNumeric($star_value)) {
				$star_value = 0;
			} else {
				$star_value = parseFloat($star_value);
			}
			if($star_value <= 3.4) {
				$is_rating_below_average = true;
				//return false;
			}
			var $star_data = {
				'name' : $star_name,
				'value' : $star_value
			};
			$rating_data.push($star_data);			
		});
		
		if($.isNumeric($overall_rating) && $overall_rating > 3.5) {
			$is_rating_below_average = false;
		} else {
			$is_rating_below_average = true;
		}
		
		if($review_text.length < 10) {			
			var $str_return = 'Please write your review...';
			fn_alert_message($review_result_area, $str_return, 'error');
			return;
		}
		
		var $rating_loader = $("#div_rating_loader");
		var $rating_submit_btn = $("#cmd_save_review");
		
		$rating_submit_btn.button('loading');
		$rating_loader.show();
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'search_rating_and_review_ajax',
			pedagoge_callback_class : 'ControllerSearch',
			rated_by : $current_user_id,
			rated_to : $teacher_user_id,
			rating_data : $rating_data,
			review_text : $review_text,
			overall_rating : $overall_rating,
			is_anonymous : $is_anonymous
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message;
				
				if($error) {
					fn_alert_message($review_result_area, $message, 'error');
					$rating_submit_btn.button('reset');
				} else {
					fn_alert_message($review_result_area, $message, 'success');					 
					$('#rating_modal').modal('hide');
					$.notify($message, "success");
					/**
					 *remove hyperlink
					 * remove button's modal target 
					 */
					
					$cmd_rating_done.data('target', '');
					$cmd_rating_done.removeAttr('data-target');
				}
			} catch(err) {
				$rating_submit_btn.button('reset');
			} finally {
				$rating_loader.hide();
			}
			 
        }).complete(function() {
        	$rating_loader.hide();
        	//$('#cmd_save_review').hide();
        	//$("#review_modal_body").html('Thank You for posting your review.The review shall be posted once our moderators have accepted it.');
        });
	});
});

function fn_calculate_star_average() {
	var $total_value = 0, $counter = 0, $average_val = 0;
	
	$('.pdg_star_rating').each(function(index){
		$total_value += parseFloat($(this).data('starvalue'));		
		$counter++;
	});
	if($total_value > 0) {
		$average_val = $total_value/$counter;
	}
	$(".pdg_overall_rating").rating('update', $average_val);
}