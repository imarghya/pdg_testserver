
$( document ).ready( function () {	
	
	$( "#submit_academic1" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		$numberofsubjects=$("#subjectnum").val();
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject1').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic1').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			//if ( $("#frm_submit_academic1").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
						
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==1){
				
					var mystr=response.split("|");			
					
					$("#subjectSelect1").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect1").modal("hide");
					$("#subjectSelect2").modal("show");
					
				}
				
				alert("Data Submitted");
				
				
				
				
			
			} );
		
			//}
			
			
			
	} );
	
	
	$( "#submit_academic2" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject2').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic2').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			
			if ( $("#frm_submit_academic2").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==2){
				
					var mystr=response.split("|");			
					
					$("#subjectSelect2").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect2").modal("hide");
					$("#subjectSelect3").modal("show");
					
				}
				
				
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	
	$( "#submit_academic3" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject3').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic3').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic3").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==3){
				
					var mystr=response.split("|");			
					
					$("#subjectSelect3").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect3").modal("hide");
					$("#subjectSelect4").modal("show");
					
				}
				
				
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
		$( "#submit_academic4" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject4').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic4').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic4").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==4){
				
					var mystr=response.split("|");	

					$("#subjectSelect4").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect4").modal("hide");
					$("#subjectSelect5").modal("show");
					
				}
				
				
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic5" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject5').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic5').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic5").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==5){
				
					var mystr=response.split("|");

					$("#subjectSelect5").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect5").modal("hide");
					$("#subjectSelect6").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic6" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject6').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic6').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic6").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==6){
				
					var mystr=response.split("|");			
					
					$("#subjectSelect6").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect6").modal("hide");
					$("#subjectSelect7").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic7" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject7').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic7').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic7").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==7){
				
					var mystr=response.split("|");	

					$("#subjectSelect7").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect7").modal("hide");
					$("#subjectSelect8").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic8" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject8').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic8').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic8").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==8){
				
					var mystr=response.split("|");

					$("#subjectSelect8").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect8").modal("hide");
					$("#subjectSelect9").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );

$( "#submit_academic9" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject9').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic9').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic9").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==9){
				
					var mystr=response.split("|");

					$("#subjectSelect9").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect9").modal("hide");
					$("#subjectSelect10").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic10" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject10').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic10').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic10").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==10){
				
					var mystr=response.split("|");	

					$("#subjectSelect10").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect10").modal("hide");
					$("#subjectSelect11").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic11" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject11').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic11').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic11").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==11){
				
					var mystr=response.split("|");	

					$("#subjectSelect11").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect11").modal("hide");
					$("#subjectSelect12").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic12" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject12').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic12').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic12").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==12){
				
					var mystr=response.split("|");	

					$("#subjectSelect12").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect12").modal("hide");
					$("#subjectSelect13").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic13" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject13').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic13').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic13").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==13){
				
					var mystr=response.split("|");	

					$("#subjectSelect13").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect13").modal("hide");
					$("#subjectSelect14").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic14" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject14').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic14').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic14").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==14){
				
					var mystr=response.split("|");	

					$("#subjectSelect14").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect14").modal("hide");
					$("#subjectSelect15").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic15" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject15').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic15').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic15").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==15){
				
					var mystr=response.split("|");			
					$("#subjectSelect15").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect15").modal("hide");
					$("#subjectSelect16").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic16" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject16').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic16').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic16").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==16){
				
					var mystr=response.split("|");			
					$("#subjectSelect16").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect16").modal("hide");
					$("#subjectSelect17").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic17" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject17').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic17').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic17").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==17){
				
					var mystr=response.split("|");			
					$("#subjectSelect17").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect17").modal("hide");
					$("#subjectSelect18").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic18" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject18').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic18').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic18").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==18){
				
					var mystr=response.split("|");			
					$("#subjectSelect18").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect18").modal("hide");
					$("#subjectSelect19").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic19" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject19').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic19').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic19").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==19){
				
					var mystr=response.split("|");			
					$("#subjectSelect19").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect19").modal("hide");
					$("#subjectSelect20").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );
	
	$( "#submit_academic20" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject20').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic20').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academic20").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				if($numberofsubjects==20){
				
					var mystr=response.split("|");			
					$("#subjectSelect20").modal("hide");
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelect20").modal("hide");
					$("#subjectSelect21").modal("show");
					
				}
				alert("Data Submitted");
			
			
			} );
			}
	
	} );

	
} );
