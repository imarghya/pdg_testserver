$(document).ready(function() {
  var $validator = $("#wizardForm").validate({
    rules: {


learningsoughtfor: {
required: true
},
relationgardian: {
required: true
},
gendergardian: {
required: true
},
contactgardian: {
required: true
},
agegroupgardian: {
required: true
},
learningsoughtgardian: {
required: true
},


dynamicagegrp:{
required: true
},
dynamicagegrp1:{
required: true
},
dynamicagegrp2:{
required: true
},
dynamicagegrp3:{
required: true
},
dynamicagegrp4:{
required: true
},

dynamicname:{
required: true
},
dynamicname1: {
required: true
},
dynamicname2: {
required: true
},
dynamicname3: {
required: true
},
dynamicname4: {
required: true
},
learningsoughtgardian: {
required: true
},
dynamiclearningsoughtgardian1: {
required: true
},
dynamiclearningsoughtgardian2: {
required: true
},
dynamiclearningsoughtgardian3: {
required: true
},
dynamiclearningsoughtgardian4: {
required: true
},
dynamicgender1: {
required: true
},
dynamicgender2: {
required: true
},
dynamicgender3: {
required: true
},
dynamicgender4: {
required: true
},






      aboutcoaching: {
        required: true
      },
      distance1: {
        required: true
      },
      locality1: {
        required: true
      },
      numberclassesinst1: {
        required: true
      },
      classofteaching: {
        required: true
      },
      teacherzipcode: {
        required: true
      },
      categoryofteaching: {
        required: true
      },
      noofcourse: {
        required: true
      },
      institutionzipcode: {
        required: true
      },
      institutiontime1: {
        required: true
      },
      institutiontime2: {
        required: true
      },
      institutiontime3: {
        required: true
      },
      institutiontime4: {
        required: true
      },
      institutiontime5: {
        required: true
      },

      institutiondays1: {
        required: true
      },
      institutiondays2: {
        required: true
      },
      institutiondays3: {
        required: true
      },
      institutiondays4: {
        required: true
      },
      institutiondays5: {
        required: true
      },
      locationsforlearning: {
        required: true
      },
      educationsubcateryinstitution: {
        required: true
      },
      educationcateryinstitution: {
        required: true
      },
      learninglocality1: {
        required: true
      },
      learninglocality2: {
        required: true
      },
      learninglocality3: {
        required: true
      },
      learninglocality4: {
        required: true
      },
      learninglocality5: {
        required: true
      },


      learningaddress1: {
        required: true
      },

      learningaddress2: {
        required: true
      },

      learningaddress3: {
        required: true
      },

      learningaddress4: {
        required: true
      },

      learningaddress5: {
        required: true
      },

      coursename1: {
        required: true
      },
      coursename2: {
        required: true
      },
      coursename3: {
        required: true
      },
      coursename4: {
        required: true
      },
      coursename5: {
        required: true
      },
      coursefeesinstitution1: {
        required: true
      },
      coursefeesinstitution2: {
        required: true
      },
      coursefeesinstitution3: {
        required: true
      },
      coursefeesinstitution4: {
        required: true
      },
      coursefeesinstitution5: {
        required: true
      },

      frequencyoffeescollectioninstitution1: {
        required: true
      },
      frequencyoffeescollectioninstitution2: {
        required: true
      },
      frequencyoffeescollectioninstitution3: {
        required: true
      },
      frequencyoffeescollectioninstitution4: {
        required: true
      },
      frequencyoffeescollectioninstitution5: {
        required: true
      },
      maxstudentsinstitution1: {
        required: true
      },
      maxstudentsinstitution2: {
        required: true
      },
      maxstudentsinstitution3: {
        required: true
      },
      maxstudentsinstitution4: {
        required: true
      },
      maxstudentsinstitution5: {
        required: true
      },

      courselength1: {
        required: true
      },

      courselength2: {
        required: true
      },

      courselength3: {
        required: true
      },

      courselength4: {
        required: true
      },

      courselength5: {
        required: true
      },


      courselength21: {
        required: true
      },

      courselength22: {
        required: true
      },

      courselength23: {
        required: true
      },

      courselength24: {
        required: true
      },

      courselength25: {
        required: true
      },
      demoinstitution1: {
        required: true
      },
      demoinstitution2: {
        required: true
      },
      demoinstitution3: {
        required: true
      },
      demoinstitution4: {
        required: true
      },
      demoinstitution5: {
        required: true
      },
      lengthofclassinstitution1: {
        required: true
      },
      lengthofclassinstitution2: {
        required: true
      },
      lengthofclassinstitution3: {
        required: true
      },
      lengthofclassinstitution4: {
        required: true
      },
      lengthofclassinstitution5: {
        required: true
      },




      institutiosizeoffaculty: {
        required: true

      },
      institutionlearninglocatiom: {
        required: true
      },
      nooflocations: {
        required: true
      },


      nooflocations: {
        required: true
      },
      institutioncity: {
        required: true
      },
      institutionstate: {
        required: true
      },
      institutionhourto: {
        required: true
      },
      institutionhourfrom: {
        required: true
      },
      addresscorrespondence: {
        required: true
      },
      country: {
        required: true
      },
      state: {
        required: true
      },
      youcanteachin: {
        required: true
      },
      city: {
        required: true
      },
      contact: {
        required: true
      },
      hometutor: {
        required: true
      },
      address1: {
        required: true
      },
      daysdemo: {
required: true
      },
batchtimingsddemo: {
required: true
},
subjecttaught:{
required: true
},
      address2: {
        required: true
      },
      youcanteachin: {
        required: true
      },
      locality1: {
        required: true
      },
      categoryofteaching: {
        required: true
      },
      classofteaching: {
        required: true
      },
      subjecttaught: {
        required: true
      },
      daystaught: {
        required: true
      },
      batchtimings: {
        required: true
      },
      batchstrength: {
        required: true
      },
      feeslower: {
        required: true
      },
      feesupper: {
        required: true
      }
    }
  });

  $('#rootwizard').bootstrapWizard({
    'tabClass': 'nav nav-tabs',
    onTabShow: function(tab, navigation, index) {
      var $total = navigation.find('li').length;
      var $current = index + 1;
      var $percent = ($current / $total) * 100;
      $('#rootwizard').find('.progress-bar').css({
        width: $percent + '%'
      });
    },
    'onNext': function(tab, navigation, index) {
      var $valid = $("#wizardForm").valid();
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      }
    },
    'onTabClick': function(tab, navigation, index) {
      var $valid = $("#wizardForm").valid();
      if (!$valid) {
        $validator.focusInvalid();
        return false;
      }
    },
  });

  $('.date-picker').datepicker({
    orientation: "top auto",
    autoclose: true
  });
});
