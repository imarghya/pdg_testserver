(function(pedagogeJquery){
	pedagogeJquery(window.jQuery, window, document);
})(function($, window, document){
	var PdgWorkshop = function(){
		var mainObj = this, settingsObj;
		
		mainObj.settings = function() {
			var select_control_with_title_multiple = $(".select_control_with_title_multiple"),
			select_control_with_title = $(".select_control_with_title"), 
			select_fltr_workshop_locality = $("#select_fltr_workshop_locality"),
			select_fltr_workshop_city = $("#select_fltr_workshop_city"),
			select_fltr_workshop_type = $("#select_fltr_workshop_type"),
			select_fltr_workshop_status = $("#select_fltr_workshop_status"),
			select_fltr_workshop_category = $("#select_fltr_workshop_category"),
			cmd_reset_filter = $("#cmd_reset_filter"),
			frm_workshop_ticket_booking = $("#frm_workshop_ticket_booking"),
			cmd_modal_reserve_workshop_seats = $("#cmd_modal_reserve_workshop_seats"),
			modal_workshop_booking = $("#modal_workshop_booking"),
			hidden_workshop_id = $("#hidden_workshop_id"),
			modal_hidden_workshop_id = $("#modal_hidden_workshop_id"),
			public_workshop = $(".public_workshop"),
			div_workshops_list = $("#div_workshops_list"),
			chk_workshop_admin = $(".chk_workshop_admin"),
			btnWorkshopPagination = $("#btnWorkshopPagination"),
			hiddenWorkshopPageNo = $("#hiddenWorkshopPageNo");
			
			return {
				select_control_with_title_multiple : select_control_with_title_multiple,
				select_control_with_title : select_control_with_title,
				select_fltr_workshop_locality : select_fltr_workshop_locality,
				select_fltr_workshop_city : select_fltr_workshop_city,
				select_fltr_workshop_type : select_fltr_workshop_type,
				select_fltr_workshop_category : select_fltr_workshop_category,
				select_fltr_workshop_status : select_fltr_workshop_status,
				cmd_reset_filter : cmd_reset_filter,
				frm_workshop_ticket_booking : frm_workshop_ticket_booking,
				cmd_modal_reserve_workshop_seats : cmd_modal_reserve_workshop_seats,
				modal_workshop_booking : modal_workshop_booking,
				hidden_workshop_id : hidden_workshop_id,
				modal_hidden_workshop_id : modal_hidden_workshop_id,
				public_workshop : public_workshop,
				div_workshops_list : div_workshops_list,
				chk_workshop_admin : chk_workshop_admin,
				btnWorkshopPagination : btnWorkshopPagination,
				hiddenWorkshopPageNo : hiddenWorkshopPageNo,
			};
		};
		
		mainObj.init = function() {
			settingsObj = mainObj.settings();
			mainObj.processSelect2();
			mainObj.processWorkshopStatusSelect2();
			mainObj.bindUIActions();
		};
		
		mainObj.bindUIActions = function() {
			
			settingsObj.select_fltr_workshop_status.on('change', function(){
				mainObj.resetPagination();				
				mainObj.filterWorkshop();
			});
			
			settingsObj.select_fltr_workshop_category.on('change', function(){
				mainObj.resetPagination();				
				mainObj.filterWorkshop();
			});
			
			settingsObj.select_fltr_workshop_type.on('change', function(){
				mainObj.resetPagination();				
				mainObj.filterWorkshop();
			});
			
			settingsObj.select_fltr_workshop_city.on('change', function(){
				mainObj.resetPagination();
				var cityID = $(this).val();
				mainObj.getLocalitiesOfCity(cityID);
				mainObj.filterWorkshop();
			});
			
			settingsObj.select_fltr_workshop_locality.on('change', function(){
				mainObj.resetPagination();
				mainObj.filterWorkshop();
			});
			
			settingsObj.cmd_reset_filter.on('click', function(){
				settingsObj.select_fltr_workshop_status.val('active').change();
				settingsObj.select_fltr_workshop_category.val('').change();
				settingsObj.select_fltr_workshop_type.val('').change();
				settingsObj.select_fltr_workshop_city.val('').change();
				//$(".workshop_filter").change();
			});
			
			settingsObj.modal_workshop_booking.on('hidden.bs.modal', function(event){
				mainObj.resetWorkshopBookingForm();
			});
			
			settingsObj.public_workshop.on('click', '.cmd_book_workshop', function(event){
				event.preventDefault();				
				var workshopID = $(this).data('workshop_id');
				mainObj.showWorkshopBookingForm(workshopID);
			});
			
			settingsObj.cmd_modal_reserve_workshop_seats.on('click', function(event){
				event.preventDefault();
				var seatReservationForm = settingsObj.frm_workshop_ticket_booking,
					reservationButton = $(this);
				if(!seatReservationForm.validationEngine('validate')) {
					return false;
				}
				var dataToSubmit = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'reserve_workshop_seats_ajax',
					pedagoge_callback_class : 'ControllerWorkshop',
					workshop_reservation_data : seatReservationForm.serialize(),
				};
				
				reservationButton.button('loading');
				
				$.post($ajax_url, dataToSubmit, function(response) {
					try {
						var parsedResponseData = $.parseJSON(response);
					
						var responseError = parsedResponseData.error,									 	
						 	responseMessage = parsedResponseData.message,
						 	responseData = parsedResponseData.data;				 	
						
						if(responseError) {
							$.notify(responseMessage, 'error');
						} else {
							
							settingsObj.modal_workshop_booking.modal('hide');
							$.notify(responseMessage, 'success');
						}
					} catch( err ) {
						var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					    console.log($message);
					    reservationButton.button('reset');
					}
			    }).complete(function(){
			    	reservationButton.button('reset');
			    });
			});
			
			settingsObj.chk_workshop_admin.change(function(){				
				var checkElement = this,
					checkedStatus = 'no',
					dataType = $(checkElement).data('type'),
					workshopID = settingsObj.hidden_workshop_id.val();
					
				if(checkElement.checked) {
					checkedStatus = 'yes';
				}
				
				var dataToSubmit = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'workshop_approve_ajax',
					pedagoge_callback_class : 'ControllerManageworkshop',
					action_type : dataType,
					action_status : checkedStatus,
					workshopID : workshopID
					
				};
				
				$.post($ajax_url, dataToSubmit, function(response) {
					try {
						var parsedResponseData = $.parseJSON(response);
					
						var responseError = parsedResponseData.error,									 	
						 	responseMessage = parsedResponseData.message,
						 	responseData = parsedResponseData.data;				 	
						
						if(responseError) {
							$.notify(responseMessage, 'error');
						} else {							
							settingsObj.modal_workshop_booking.modal('hide');
							$.notify(responseMessage, 'success');
							settingsObj.div_workshops_list.html(responseData);
							
						}
					} catch( err ) {
						var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
					    console.log($message);
					}
			    });
				
			});
			
			settingsObj.btnWorkshopPagination.on('click', function(event){
				mainObj.filterWorkshop('paginate');
			});
		};
		
		mainObj.processSelect2 = function() {
			
			settingsObj.select_control_with_title.each(function(index){
				var selectControl = $(this),				
				title = selectControl.prop('title');
						
				selectControl.select2({
					placeholder: title,
					allowClear: true
				});				
			});
			
			settingsObj.select_control_with_title_multiple.each(function(index){
				var selectControl = $(this), 
				title = selectControl.prop('title');
						
				selectControl.select2({
					placeholder: title,
					closeOnSelect: false,
				});
			});			
		};
		mainObj.processWorkshopStatusSelect2 = function() {
			var select_fltr_workshop_status_title = settingsObj.select_fltr_workshop_status.prop('title');						
			settingsObj.select_fltr_workshop_status.select2({
				placeholder: select_fltr_workshop_status_title,
			});	
		};
		
		mainObj.getLocalitiesOfCity = function(cityID) {
			var cityLocalitites = '<option></option>';
			
			if(!cityID) {
				cityID = '';
			} else {
				if($.isNumeric(cityID)) {
					cityLocalitites += $localities_data[cityID];
				}
			}
			//settingsObj.select_fltr_workshop_locality.html(cityLocalitites).change();
			settingsObj.select_fltr_workshop_locality.html(cityLocalitites).select2({		
				closeOnSelect: true,
				placeholder : "Please select a locality",
				allowClear: true
			});
		};
		
		mainObj.filterWorkshop = function(paginateAction) {
			
			var workshopCategory = settingsObj.select_fltr_workshop_category.val(),
				workshopType = settingsObj.select_fltr_workshop_type.val(),
				workshopCity = settingsObj.select_fltr_workshop_city.val(),
				workshopLocality = settingsObj.select_fltr_workshop_locality.val(),
				workshopStatus = settingsObj.select_fltr_workshop_status.val(),
				pageNo = settingsObj.hiddenWorkshopPageNo.val(),
				paginate = 'no';
				
				if (typeof paginateAction != 'undefined') {
					paginate = 'yes';
				}
							
			var dataToSubmit = {
					nonce : $ajaxnonce,
					action : $pedagoge_visitor_ajax_handler,
					pedagoge_callback_function : 'workshop_filter_ajax',
					pedagoge_callback_class : 'ControllerWorkshop',
					workshopCategory : workshopCategory,
					workshopType : workshopType,
					workshopCity : workshopCity,
					workshopLocality : workshopLocality,
					workshopStatus : workshopStatus,
					pageNo : pageNo,
					paginate : paginate
			};
				
			$.post($ajax_url, dataToSubmit, function(response) {
				try {
					var parsedResponseData = $.parseJSON(response);
					
					var responseError = parsedResponseData.error,									 	
					 	responseMessage = parsedResponseData.message,
					 	responseData = parsedResponseData.data;				 	
					
					if(responseError) {
						$.notify(responseMessage, 'error');
					} else {
						$.notify(responseMessage, 'success');
						var returnedPageNo = parsedResponseData.pageNo;
						settingsObj.hiddenWorkshopPageNo.val(returnedPageNo);
						
						settingsObj.modal_workshop_booking.modal('hide');// dont know why it is here.
						if (paginate === 'yes') {
							settingsObj.div_workshops_list.append(responseData);
						} else {
							settingsObj.div_workshops_list.html(responseData);
						}
					}
				} catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				    console.log($message);					    
				}
		    });
		};
		mainObj.resetPagination = function() {
			settingsObj.hiddenWorkshopPageNo.val('0');
		};
		
		mainObj.resetWorkshopBookingForm = function() {
			settingsObj.frm_workshop_ticket_booking[0].reset();
		};
		
		mainObj.showWorkshopBookingForm = function(workshopID) {
			if($.isNumeric(workshopID)) {
				settingsObj.modal_hidden_workshop_id.val(workshopID);
				settingsObj.modal_workshop_booking.modal('show');
			}
		};
		
	}, workshopObj;
	workshopObj = new PdgWorkshop;
	workshopObj.init();
});

$(document).ready(function(){
	$('.bxslider').bxSlider({
		responsive : true,
		auto : false,
		slideWidth: 325,
		slideMargin : 0,
		minSlides: 1,
		maxSlides: 4,
		
	});
});
