$( document ).ready( function () {	
	
	
		$( "#submitnameemail" ).click( function ( event ) {
		 
		event.preventDefault();
		
		myval=$("#myuserid").val();
		 
			var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxaftersubmitmodal',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frmnameemail').serialize()+'&'+$.param({ 'myuserid': myval }),
				
			};
			 //if ( $("#frmnameemail").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				alert("Updated");
				window.location.href = base_url + '/memberdashboard';
			
			} );
		 
			 //}
	  });
	  
	  $( "#cmdeditsubjectselect" ).click( function ( event ) {
		 
		event.preventDefault();
		
		
			subid=$("#subid").val();
		 
			var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'update_data_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frmeditsubject').serialize()+'&'+$.param({ 'mysubid': subid }),
				
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				//alert(response);
				alert("Updated");
				window.location.href = base_url + '/memberdashboard?id=' + subid;
			
			} );
		 
			
			
			
	  });
	  
	  $( "#btnok" ).click( function ( event ) {
		  
		 subid=$("#subid").val();
		 firstquery=$("#firstquery").val();
		 
		  var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'update_archive_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $.param({ 'mysubid': subid }),
				
			};
			//if ( $("#frmnameemail").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				
				//$("#alertarchivesuccess").modal("show");
				
				window.location.href = base_url + '/memberdashboard?id=' + firstquery;
			
			} );
		 
			 //}
		  
	  });
	  
	  $( "#activequeryarchive" ).click( function ( event ) {
		  event.preventDefault();
		  
		  $("#alertarchive").modal("show");
		  
			/*var isGood=confirm('Are you sure you want to archive?');
			if (isGood) {
			 
			} else {
			  return false;
			}*/
			
			
		  
		  /*
		 subid=$("#subid").val();
		  
		  var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'update_archive_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $.param({ 'mysubid': subid }),
				
			};
			if ( $("#frmnameemail").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				
				alert("Archived");
				window.location.href = base_url + '/memberdashboard';
			
			} );
		 
			 }*/
		  
	  });
	  
	   
	  $( "#deletequery" ).click( function ( event ) {
		  event.preventDefault();
		
		 var isGood=confirm('Are you sure you want to delete?');
			if (isGood) {
			 
			} else {
			  return false;
			}
		  
		  
		 subid=$("#subid").val();
		  
		  var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'delete_unarchive_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $.param({ 'mysubid': subid }),
				
			};
			if ( $("#frmnameemail").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				
				alert("Deleted");
				window.location.href = base_url + '/archived';
			
			} );
		 
			 }
			 
			
		  
	  });
	 
	 $( ".unarchive" ).click( function ( event ) {
		  event.preventDefault();
			
		$("#temp").val($(this).attr("record"));
		
		
		
		  $("#deleteoractivate").modal("show");
		  
	  });
	  
	  $( "#deletearchive" ).click( function ( event ) {
		  
		   var isGood=confirm('Are you sure you want to delete?');
			if (isGood) {
			 
			} else {
			  return false;
			}
		  
		  
		  var subid=$("#temp").val();
		 
		  var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'delete_unarchive_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $.param({ 'mysubid': subid }),
				
			};
			if ( $("#frmnameemail").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				
				alert("Deleted");
				window.location.href = base_url + '/archived';
			
			} );
		 
			 }
			
		  
	  });
	  
	$( "#editactivate" ).click( function ( event ) {	 
		 
		 var subid=$("#temp").val();
		 
		  var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'modal_editsubjectselect_ajax',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $.param({ 'mysubid': subid }),
				
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				
				//$("#modal-contentx").html(response);
				$("#modal-contentx").html(response);
				$("#editsubjectselectarchive").modal("show");
				
			
			} );
		 
	  });
	  
	  //$( "#localitiesx" ).click( function ( event ) {
		/*	
		$( "#localitiesx" ).on('click', function (e) {
			alert("ok1");
			$('#localities').val($('#localitiesx').val());
			
		});*/
	  
	 
	  
});	 