/* For menu Active */
$(document).ready(function () {
    $('.menu li a').click(function(e) {

        $('.menu li a').removeClass('menu_active');

        var $this = $(this);
        if (!$this.hasClass('menu_active')) {
            $this.addClass('menu_active');
        }
        //e.preventDefault();
    });
});
/* For menu active */


/* For Dropdown Menu */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Dropdown Menu */

/* For Filter Menu1 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction() {
    document.getElementById("filterFunctionDropdown").classList.toggle("show");
}

$(document).click(function(e) 
{
    var container = $(".dropdownfilter");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
      if($("#filterFunctionDropdown").hasClass("show"))
        {
          $("#filterFunctionDropdown").removeClass("show");
        }
    }
    else{
    }
});

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter')) {
	  
    var dropdownfilter = document.getElementsByClassName("dropdown-content-filter");
    var i;
    for (i = 0; i < dropdownfilter.length; i++) {
      var openDropdown = dropdownfilter[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu1 */

/* For Filter Menu2 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction1() {
    document.getElementById("filterFunctionDropdown1").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter1')) {

    var dropdownfilter1 = document.getElementsByClassName("dropdown-content-filter1");
    var i;
    for (i = 0; i < dropdownfilter1.length; i++) {
      var openDropdown = dropdownfilter1[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu2 */

/* For Filter Menu3 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction2() {
    document.getElementById("filterFunctionDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter2')) {

    var dropdownfilter2 = document.getElementsByClassName("dropdown-content-filter2");
    var i;
    for (i = 0; i < dropdownfilter2.length; i++) {
      var openDropdown = dropdownfilter2[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu3 */

/* For Filter Menu4 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction3() {
    document.getElementById("filterFunctionDropdown3").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter3')) {

    var dropdownfilter3 = document.getElementsByClassName("dropdown-content-filter3");
    var i;
    for (i = 0; i < dropdownfilter3.length; i++) {
      var openDropdown = dropdownfilter3[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu4 */

/* For Filter Menu5 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction4() {
    document.getElementById("filterFunctionDropdown4").classList.toggle("show");
    //event.stopPropagation();
}

// Close the dropdown if the user clicks outside of it
//window.onclick = function(event) {
//  if (!event.target.matches('.dropdownfilter4')) {
//
//    var dropdownfilter4 = document.getElementsByClassName("dropdown-content-filter4");
//    var i;
//    for (i = 0; i < dropdownfilter4.length; i++) {
//      var openDropdown = dropdownfilter4[i];
//      if (openDropdown.classList.contains('show')) {
//        openDropdown.classList.remove('show');
//      }
//    }
//  }
//}

/* For Filter Menu5 */

/* For Slim Scroll */
	
		/*$(function(){
					$('.author_details').slimScroll({
						height: '1056px',
						size: '10px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						allowPageScroll: true,
						opacity: 1
					});
				});*/
				
				
				
				/*If want Slim Scroll then comment out the of function which are given over*/
				
				
				//$(function(){
				//	$('.preference p').slimScroll({
				//		height: '140px',
				//		size: '5px',
				//		alwaysVisible: true,
				//		distance: '0px',
				//		color: '#2e3847',
				//		allowPageScroll: true,
				//		opacity: 1
				//	});
				//});
				
				
/* For Slim Scroll */


/* For Pagination */
/*
$('#pagination-demo').twbsPagination({

	totalPages: 50,

	visiblePages: 3

	});*/
	
/* For Pagination */