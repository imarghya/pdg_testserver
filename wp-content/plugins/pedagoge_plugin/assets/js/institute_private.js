/**
 * General Coding 
 */
$(document).ready(function(){
	document.getElementById('top').scrollIntoView(true);

	if($('#chk_allow_demo_classes').is(':checked')){
		$('#price_per_demo_class').show();
		$('#txt_price_per_demo_class').show();	
	}
	else{
		$('#price_per_demo_class').hide();
		$('#txt_price_per_demo_class').hide();			
	}	

	$('#chk_allow_demo_classes').change(function(){
		if(this.checked){
			$('#txt_price_per_demo_class').show();
			$('#price_per_demo_class').show();
		}
		else{
			$('#txt_price_per_demo_class').hide();	
			$('#price_per_demo_class').hide();
		}
	});
	
	//Do not submit Forms by default 
	$("#frm_institute_intro, #frm_institute_class_details, #frm_institute_form_finished").submit(function(e){
	    return false;
	});
	
	$("#frm_institute_intro").validationEngine({scroll: false});
	
	$(".non_clickable").click(function(event){event.preventDefault();});
	
	$(".character_count").keyup(function(){
		
		$(".div_character_count").html('');
		var $current_count = $(this).val().length;
		if($current_count<=0) {
			return;
		}
		var $max_length = $(this).prop('maxlength');
		
		if(!$.isNumeric($max_length)) {
			$max_length = 0;
		}
		var $remaining_characters = $max_length - $current_count;
		var $message = '<div class="alert alert-info">'+$remaining_characters+' characters remained...</div>';
		$(this).parent().find('.div_character_count').html($message);
	});
	
	$(".character_count").blur(function(){
		$(".div_character_count").html('');
	});
	
});


/**
 * Tabs Management 
 */
$(document).ready(function(){
		
	$(".insti_tabs").click(function(event){
		event.preventDefault();
	});
	
	$(".cmd_show_class_details").click(function(event) {
		event.preventDefault();
		fn_save_profile_introduction();
	});
	$(".cmd_show_institute_intro").click(function(event){
		event.preventDefault();
		var $leave_class = fn_leave_class_details();
		if($leave_class) {
			fn_show_intro_form_tab();	
		}
	});
	$(".cmd_show_institute_form_finished").click(function(event){
		event.preventDefault();
		var $leave_class = fn_leave_class_details();
		if($leave_class) {
			fn_show_form_finish();	
		}
	});
	$(".cmd_show_class_details2").click(function(event) {
		event.preventDefault();
		fn_show_class_details_tab();
	});	
});

function fn_leave_class_details() {
	var $batches_tr = $("#tbl_batches_list tbody").find('tr');
	if($batches_tr.length>0) {
		return true;
	} else {
		bootbox.alert('<h3>You need to create atleast one batch to continue.</h3>');
		return false;
	}
}

function fn_remove_active_tab() {
	$(".nav-tabs li").removeClass('active');
	$(".tab-pane").removeClass('active');
}

function fn_show_class_details_tab() {
	fn_remove_active_tab();
	$("#li_institute_class_details").addClass('active');
	$("#institute_class_details").addClass('active');
}

function fn_show_intro_form_tab() {
	fn_remove_active_tab();
	$("#li_institute_intro").addClass('active');
	$("#institute_intro").addClass('active');
}

function fn_show_form_finish() {
	fn_remove_active_tab();
	$("#li_institute_form_finished").addClass('active');
	$("#institute_form_finished").addClass('active');
}

/**
 *   /////////////////////////////////////////////////Profile Introduction coding/////////////////////////////////////////////// 
 */
$(document).ready(function(){
	$('#from_hours_of_operation, #to_hours_of_operation').datetimepicker({
        format: 'LT',
        showClear: true,
		showClose: true,
        useCurrent: false,
        stepping: 5
    });   

    $('#from_hours_of_operation, #to_hours_of_operation').keydown(function() {		
	  return false;
	});
    
});

function fn_save_profile_introduction() {
	var $save_result_area = $(".div_institute_save_result_area"),
	$message = '',
	$save_button = $('.cmd_show_class_details'),
	$save_loader = $(".img_institute_save_loader");
	
	$save_result_area.html('');
	
	var $txt_institution_name = $("#txt_institution_name").val(),	
	$txt_contact_person = $("#txt_contact_person").val(),
	$txt_mobile_no = $("#txt_mobile_no").val(),
	$txt_alternate_contact_no = $("#txt_alternate_contact_no").val(),	
	$select_preffered_mode_of_contact = $("#select_preffered_mode_of_contact").val(),	
	$txt_address_of_correspondance = $("#txt_address_of_correspondance").val(),
	$select_institute_city = $("#select_institute_city").val(),
	$select_size_of_faculty = $("#select_size_of_faculty").val(),	
	$from_hours_of_operation = $("#from_hours_of_operation").val(),	
	$to_hours_of_operation = $("#to_hours_of_operation").val(),
	$select_average_teaching_experience = $("#select_average_teaching_experience").val(),	
	$txt_heading_of_profile = $("#txt_heading_of_profile").val(),
	$txt_registration_reference = $("#txt_registration_reference").val(),
	$txt_about_the_coaching = $("#txt_about_the_coaching").val();
	
	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
	$hidden_user_id = $("#hidden_dynamic_user_var").val(),
	$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
	$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
		
	if(!$("#frm_institute_intro").validationEngine('validate')) {
		return;
	}
	
	if(
		!$.isNumeric($hidden_institute_id) || 
		$hidden_institute_id <= 0 || 
		!$.isNumeric($hidden_user_id) || 
		$hidden_user_id <= 0 ||
		$hidden_user_email.length <= 0 ||
		!$.isNumeric($user_personal_info_id) || 
		$user_personal_info_id <= 0
	) {
		//bootbox.alert('<div class="alert alert-danger">Error! Profile Information is invalid. Please reload the page and try again.</div>');
		$message = 'Error! Profile Information is invalid. Please reload the page and try again.';
		fn_alert_message($save_result_area, $message, 'error');
		return;
	}	
	
	var $data_to_save = {};
	$data_to_save.hidden_dynamic_insti_var = $hidden_institute_id;
	$data_to_save.hidden_dynamic_user_var = $hidden_user_id;
	$data_to_save.hidden_dynamic_profile_edit_secret_key = $hidden_user_email;
	$data_to_save.hidden_dynamic_user_info_var = $user_personal_info_id;
	
	$data_to_save.txt_institution_name = $txt_institution_name;
	$data_to_save.txt_contact_person = $txt_contact_person;
	$data_to_save.txt_mobile_no = $txt_mobile_no;
	$data_to_save.txt_alternate_contact_no = $txt_alternate_contact_no;
	$data_to_save.select_preffered_mode_of_contact = $select_preffered_mode_of_contact;
	$data_to_save.txt_address_of_correspondance = $txt_address_of_correspondance;
	$data_to_save.select_institute_city = $select_institute_city;
	$data_to_save.select_size_of_faculty = $select_size_of_faculty;
	$data_to_save.from_hours_of_operation = $from_hours_of_operation;
	$data_to_save.to_hours_of_operation = $to_hours_of_operation;
	$data_to_save.select_average_teaching_experience = $select_average_teaching_experience;
	$data_to_save.txt_heading_of_profile = $txt_heading_of_profile;
	$data_to_save.txt_about_the_coaching = $txt_about_the_coaching;
	$data_to_save.txt_registration_reference = $txt_registration_reference;
	
	var $upload_data = new FormData();
	$upload_data.append('institute_intro_data', JSON.stringify($data_to_save));
	
	$save_button.button('loading');
	$save_loader.show();
	$save_result_area.html('<div class="alert alert-info">Please wait while we are storing your data in the database.</div>');
	
	$upload_data.append("nonce", $ajaxnonce);
	$upload_data.append("action", $pedagoge_visitor_ajax_handler);
	$upload_data.append("pedagoge_callback_function", 'fn_save_institute_intro_ajax');
	$upload_data.append("pedagoge_callback_class", $pedagoge_callback_class);	
    
    $.ajax({
		url : $ajax_url,
	    data : $upload_data,
	    processData : false, 
	    contentType : false,
	    type : 'POST',			    
	    success : function(response) {
	    	try {
				var $response_data = $.parseJSON(response);				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				if($error) {
					fn_alert_message($save_result_area, $message, 'error');
				} else {
					fn_alert_message($save_result_area, $message, 'success');
					fn_show_class_details_tab();
				}
			} catch(err) {
				$save_button.button('reset');
				console.log('error occured '+err);
			}
	    },
	    complete:function(){
	    	$save_button.button('reset');
			$save_loader.hide();
	    }
	});
}

/**
 *  /////////////////////////////////////////////////Class Management Code/////////////////////////////////////////////// 
 */
$(document).ready(function(){
	fn_process_select_with_title();
	
	$("#select_subject_name").select2({
		placeholder: "Select a Subject",
	});
	
	fn_load_subject_type();
	
    $("#select_fees_type").change(function(event){
    	var $str_fees_type_text_fields = '',    	
    	$fees_collections_fields = $(".div_fees_colletion_fields").find('.txt_fees_type_amount'),
    	$fees_type_id_array = [],
    	$selected_fees_type_array = [];
    	
    	$fees_collections_fields.each(function() {
    		var $fees_type_id = $(this).data('feestypeid');
    		if($.isNumeric($fees_type_id)) {
    			$fees_type_id_array.push($fees_type_id);
    		}
    	});
    	
    	$('#select_fees_type :selected').each(function(i, selected){
			var $selected_val = parseInt($(selected).val());
			var $selected_text = $(selected).text();
			
			var $found_index = $fees_type_id_array.indexOf($selected_val);
			$selected_fees_type_array.push($selected_val);			
			if($found_index < 0) {
				var $str_input = '<div class="div_dynamic_fees_collected_field"><label>'+$selected_text+' (In Rs.)</label><input type="text" class="form-control positive txt_fees_type_amount" data-feestypeid="'+$selected_val+'" placeholder="'+$selected_text+'" maxlength="7"><br/></div>';
				$str_fees_type_text_fields += $str_input;				
			}
		});
		
		$fees_collections_fields.each(function() {
    		var $new_fees_type_id = $(this).data('feestypeid');
    		if($.isNumeric($new_fees_type_id)) {
    			var $new_found_index = $selected_fees_type_array.indexOf(parseInt($new_fees_type_id));
    			if($new_found_index < 0) {
    				$(this).closest('.div_dynamic_fees_collected_field').remove();
    			}
    		}
    	});
		
		$(".div_fees_colletion_fields").append($str_fees_type_text_fields);
		$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
    });
	
    $('.select_max_students_per_class').hide();
    $("#select_class_type").change(function() {
    	if ( $(this).val() == "multiple" || $(this).val() == "both") {
    		$('.select_max_students_per_class').show();
    	}
    	else {
    		$('.select_max_students_per_class').hide();
    	}
	});
	
	$("#select_subject_name").change(function(event){
		fn_load_subject_type();
	});
	
	$("#select_subject_category").change(function(event){
		fn_load_subject_board_age();
	});
	
	$("#cmd_add_more_subjects").click(function(event){
		fn_add_more_subjects();
	});
	
	$(".cmd_add_more_batch").click(function(even){
		fn_add_batch();
	});
	
	$("#tbl_batches_list").on('click','.cmd_edit_batch', function(){
		var $batch_id = $(this).data('batchid');
		if($.isNumeric($batch_id)) {
			fn_load_batch_details($batch_id);
		}
	});
	
	$("#tbl_batches_list").on('click','.cmd_copy_batch', function(){
		var $batch_id = $(this).data('batchid');
		if($.isNumeric($batch_id)) {
			fn_load_batch_details($batch_id, false);
		}
	});
	$("#tbl_batches_list").on('click','.cmd_delete_batch', function(){
		var $batch_id = $(this).data('batchid');
		if($.isNumeric($batch_id)) {
			fn_delete_batch($batch_id);
		}
	});
	
	$("#cmd_reset_batch_details_form").click(function(){
		fn_reset_batch_form();
	});
	
	$("#select_location_own").change(function(event){
		var $str_own_location_fees_text_fields = '', 
		$own_location_fees_fields = $(".div_own_location_fees").find('.txt_own_location_fees_amount'),
    	$location_id_array = [],
    	$selected_own_locations_array = [];
    	
    	$own_location_fees_fields.each(function() {
    		var $own_location_id = $(this).data('own_location_id');
    		if($.isNumeric($own_location_id)) {
    			$location_id_array.push($own_location_id);
    		}
    	});
    	
    	$('#select_location_own :selected').each(function(i, selected){
			var $selected_val = parseInt($(selected).val());
			var $selected_text = $(selected).text();
			
			var $found_index = $location_id_array.indexOf($selected_val);
			$selected_own_locations_array.push($selected_val);			
			if($found_index < 0) {
				var $str_input = '<div class="div_own_location_fees_field"><label>Monthly Fees for '+$selected_text+' (In Rs.)</label><input type="text" class="form-control positive txt_own_location_fees_amount" data-own_location_id="'+$selected_val+'" placeholder="Monthly Fees for '+$selected_text+'" maxlength="7"><br/></div>';
				$str_own_location_fees_text_fields += $str_input;				
			}
		});
		
		$own_location_fees_fields.each(function() {
    		var $new_own_location_id = $(this).data('own_location_id');
    		if($.isNumeric($new_own_location_id)) {
    			var $new_found_index = $selected_own_locations_array.indexOf(parseInt($new_own_location_id));
    			if($new_found_index < 0) {
    				$(this).closest('.div_own_location_fees_field').remove();
    			}
    		}
    	});
    	
    	$(".div_own_location_fees").append($str_own_location_fees_text_fields);
		$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
    	
	});
	
	$("#select_location_student").change(function(event){
		var $str_student_location_fees_text_fields = '', 
		$student_location_fees_fields = $(".div_student_location_fees").find('.txt_student_location_fees_amount'),
    	$location_id_array = [],
    	$selected_student_locations_array = [];
    	
    	$student_location_fees_fields.each(function() {
    		var $student_location_id = $(this).data('student_location_id');
    		if($.isNumeric($student_location_id)) {
    			$location_id_array.push($student_location_id);
    		}
    	});
    	
    	$('#select_location_student :selected').each(function(i, selected){
			var $selected_val = parseInt($(selected).val());
			var $selected_text = $(selected).text();
			
			var $found_index = $location_id_array.indexOf($selected_val);
			$selected_student_locations_array.push($selected_val);			
			if($found_index < 0) {
				var $str_input = '<div class="div_student_location_fees_field"><label>Monthly Fees for '+$selected_text+' (In Rs.)</label><input type="text" class="form-control positive txt_student_location_fees_amount" data-student_location_id="'+$selected_val+'" placeholder="Monthly Fees for '+$selected_text+'" maxlength="7"><br/></div>';
				$str_student_location_fees_text_fields += $str_input;				
			}
		});
		
		$student_location_fees_fields.each(function() {
    		var $new_student_location_id = $(this).data('student_location_id');
    		if($.isNumeric($new_student_location_id)) {
    			var $new_found_index = $selected_student_locations_array.indexOf(parseInt($new_student_location_id));
    			if($new_found_index < 0) {
    				$(this).closest('.div_student_location_fees_field').remove();
    			}
    		}
    	});
    	
    	$(".div_student_location_fees").append($str_student_location_fees_text_fields);
		$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
    	
	});
	
	$("#select_location_institute").change(function(event){
		var $str_institute_location_fees_text_fields = '', 
		$institute_location_fees_fields = $(".div_institute_location_fees").find('.txt_institute_location_fees_amount'),
    	$location_id_array = [],
    	$selected_institute_locations_array = [];
    	
    	$institute_location_fees_fields.each(function() {
    		var $institute_location_id = $(this).data('institute_location_id');
    		if($.isNumeric($institute_location_id)) {
    			$location_id_array.push($institute_location_id);
    		}
    	});
    	
    	$('#select_location_institute :selected').each(function(i, selected){
			var $selected_val = parseInt($(selected).val());
			var $selected_text = $(selected).text();
			
			var $found_index = $location_id_array.indexOf($selected_val);
			$selected_institute_locations_array.push($selected_val);			
			if($found_index < 0) {
				var $str_input = '<div class="div_institute_location_fees_field"><label>Monthly Fees for '+$selected_text+' (In Rs.)</label><input type="text" class="form-control positive txt_institute_location_fees_amount" data-institute_location_id="'+$selected_val+'" placeholder="Monthly Fees for '+$selected_text+'" maxlength="7"><br/></div>';
				$str_institute_location_fees_text_fields += $str_input;				
			}
		});
		
		$institute_location_fees_fields.each(function() {
    		var $new_institute_location_id = $(this).data('institute_location_id');
    		if($.isNumeric($new_institute_location_id)) {
    			var $new_found_index = $selected_institute_locations_array.indexOf(parseInt($new_institute_location_id));
    			if($new_found_index < 0) {
    				$(this).closest('.div_institute_location_fees_field').remove();
    			}
    		}
    	});
    	
    	$(".div_institute_location_fees").append($str_institute_location_fees_text_fields);
		$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
    	
	});
});

function fn_process_select_with_title() {
	$(".select_control_with_title").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
		});
	});
	
	$(".select_control_with_title_multiple").each(function(index){
		var $title = $(this).prop('title');		
		$(this).select2({
			placeholder: $title,
			closeOnSelect: false
		});
	});	
}

function fn_load_subject_type() {
	var $subject_name_id = $("#select_subject_name").val();
	
	var $loader = $(".img_subjects_loader");
	var $result_area = $(".span_subjects_loader_area");
	
	$("#select_subject_category").html('');
	$("#select_subject_category").select2({
		placeholder : "Subject Category not available"
	});
	
	$("#select_subject_age_category").html('');
	$("#select_subject_age_category").select2({
		placeholder : "Age Category not available"
	});
	
	$("#select_subject_academic_board").html('');
	$("#select_subject_academic_board").select2({
		placeholder : "Academic Board Not available"
	});
	
	if($.isNumeric($subject_name_id)) {
		$loader.show();
		$result_area.html('');
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_load_subject_type_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
			subject_name_id : $subject_name_id
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, 'blank_error');
				} else {
					/**
					 *Load Subject type
					 * Load age
					 * load board 
					 */
					fn_alert_message($result_area, $message, 'blank_success');
					var $course_type = $data.course_type;
					
					$("#select_subject_category").html($course_type);
					$("#select_subject_category").select2({		
						closeOnSelect: false,
						placeholder : "Please select Subject Category"
					});
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'blank_error');
			} finally {
				//$submit_button.button('reset');
				$loader.hide();
			}			
        }).complete(function() {        	
        	$loader.hide();
        });
	}
}

function fn_load_subject_board_age() {
	var $subject_name_id = $("#select_subject_name").val();	
	var $subject_type_id = $("#select_subject_category").val();	
	
	var $loader = $(".img_subjects_loader");
	var $result_area = $(".span_subjects_loader_area");
	
	$("#select_subject_age_category").html('');
	$("#select_subject_age_category").select2({
		placeholder : "Age Category not available"
	});
	
	$("#select_subject_academic_board").html('');
	$("#select_subject_academic_board").select2({
		placeholder : "Academic Board Not available"
	});
	
	if($subject_type_id == null) {
		return false;
	}
		
	if($.isNumeric($subject_name_id)) {
		$loader.show();
		$result_area.html('');
	  	var $submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_load_subject_age_board_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
			subject_name_id : $subject_name_id,
			subject_type_id : $subject_type_id
		};
		
		$.post($ajax_url, $submit_data, function(response) {
			try {
				var $response_data = $.parseJSON(response);
			
				var $error = $response_data.error,									 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;				 	
				
				if($error) {
					fn_alert_message($result_area, $message, 'blank_error');
				} else {
					/**
					 * Load age
					 * load board 
					 */
					fn_alert_message($result_area, $message, 'blank_success');
					var $course_age_category = $data.course_age;
					var $course_academic_board = $data.academic_board;
					
					var $subject_age_placeholder = 'Age Category not available';
					var $academic_board_placeholder = 'Academic Board not available';
					if($course_age_category.length>0) {
						$subject_age_placeholder = 'Select Age Category';
					}
					if($course_academic_board.length>0) {
						$academic_board_placeholder = 'Select Academic Board';
					}
					
					$("#select_subject_age_category").html($course_age_category);
					$("#select_subject_age_category").select2({		
						closeOnSelect: false,
						placeholder : $subject_age_placeholder
					});
					$("#select_subject_academic_board").html($course_academic_board);
					$("#select_subject_academic_board").select2({		
						closeOnSelect: false,
						placeholder:$academic_board_placeholder
					});
				}
			} catch( err ) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message($result_area, $message, 'blank_error');
			} finally {
				//$submit_button.button('reset');
				$loader.hide();
			}
        }).complete(function() {        	
        	$loader.hide();
        });
	}
}

function fn_add_more_subjects() {
	/**
	 * 1. Collect all the information
	 * 2. Validate Subjects
	 * 3. Fetch Data from Database
	 * 4. Populate Subjects Table
	 * 5. clear all selections 
	 */
	var $message = '';
	var $btn = $("#cmd_add_more_subjects");
	var $result_area = $(".div_add_subject_result");
	
	var $subject_category_lengh = $("#select_subject_category option").length;
	var $subject_age_category_length = $("#select_subject_age_category option").length;
	var $subject_academic_board_length = $("#select_subject_academic_board option").length;
	
	var $select_subject_name = $("#select_subject_name").val();
	var $select_subject_category = $("#select_subject_category").val();
	var $select_subject_age_category = $("#select_subject_age_category").val();
	var $select_subject_academic_board = $("#select_subject_academic_board").val();
	
	if($select_subject_name == 0) {
		$message = 'Please select Subject Name';
		fn_alert_message($result_area, $message, 'warning', 3000);
		return false;
	}
	
	if($subject_category_lengh > 0 && $select_subject_category == null) {
		$message = 'Please select Subject Category';
		fn_alert_message($result_area, $message, 'warning', 3000);
		return false;
	}
	
	if($subject_age_category_length > 0 && $select_subject_age_category == null) {
		$message = 'Please select Age Category';
		fn_alert_message($result_area, $message, 'warning', 3000);
		return false;
	}
	
	if($subject_academic_board_length > 0 && $select_subject_academic_board == null) {
		$message = 'Please select Academic Board';
		fn_alert_message($result_area, $message, 'warning', 3000);
		return false;
	}
	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_course_table_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		select_subject_name : $select_subject_name,
		select_subject_category : $select_subject_category,
		select_subject_age_category : $select_subject_age_category,
		select_subject_academic_board : $select_subject_academic_board,
	};	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				fn_alert_message($result_area, $message, 'blank_error');
			} else {				
				fn_alert_message($result_area, $message, 'blank_success');
				/**
				 * Update Table
				 *  
				 */
				var $str_table_content = '';
				var $course_list_length = Object.keys($data).length;
				if($course_list_length>0) {
					
					$.each($data, function($course_id, $course_data){
						var $found_course_id = $("#tbl_course_list tbody").find('[data-courseid="'+$course_id+'"]');
						
						var $returned_subject_name = $course_data.subject_name;
						var $returned_subject_category = $course_data.subject_category;
						var $returned_age_category = $course_data.course_age == null ? 'N/A' : $course_data.course_age;
						var $returned_board_category = $course_data.acadamic_board == null ? 'N/A' : $course_data.acadamic_board;
						
						if($found_course_id.length<=0) {
							$str_table_content += '<tr data-courseid="'+$course_id+'">'+
							'<td class="td_course_id hidden_item">'+$course_id+'</td>'+
							'<td>'+$returned_subject_name+'</td>'+
							'<td>'+$returned_subject_category+'</td>'+
							'<td>'+$returned_age_category+'</td>'+
							'<td>'+$returned_board_category+'</td>'+
							'<td><input type="checkbox" class="chk_selected_courses" checked value="'+$course_id+'"></td></tr>';	
						}
					});
					
					$("#tbl_course_list tbody").append($str_table_content);
					$("#select_subject_name").select2("val", "");
					
					$("input:checkbox").uniform();
				}
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    fn_alert_message($result_area, $message, 'blank_error');
		    console.log(err);
		} finally {
			//$submit_button.button('reset');
			//$loader.hide();
		}
    }).complete(function() {        	
    	//$loader.hide();
    });
}

function fn_add_batch() {
	var $courses_list = $("#tbl_course_list tbody").find('.chk_selected_courses');
	var $courses_id_array = [];
	var $is_course_added = false;
	
	$courses_list.each(function(){
		if($(this).is(':checked')) {
			$is_course_added = true;
			var $course_id = $(this).val();
			$courses_id_array.push($course_id);
		}
	});
	var $element = '';
	var $message = '';
	if(!$is_course_added) {
		$element = $('#select_subject_name');
		$message = "Select and add subjects";			
		fn_notify_course_error($element, $message, true);	
		return;
	}
	
	var $select_course_days = $("#select_course_days").val();
	if($select_course_days==null || $select_course_days.length<=0) {		
		$element = $('#select_course_days');
		$message = "Select days of your classes";			
		fn_notify_course_error($element, $message, true);	
		return;
	}
	
	var $select_no_of_days = $("#select_no_of_days").val();	
	if($select_no_of_days==null || $select_no_of_days.length<=0) {		
		$element = $('#select_no_of_days');
		$message = "Select No.of Classes per batch";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $select_no_of_hours = $("#select_no_of_hours").val();	
	if($select_no_of_hours==null || $select_no_of_hours.length<=0) {
		$element = $('#select_no_of_hours');
		$message = "Select  No.of hours per class";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $select_class_timing_slot = $("#select_class_timing_slot").val();	
	if($select_class_timing_slot==null || $select_class_timing_slot.length<=0) {
		$element = $('#select_class_timing_slot');
		$message = "Select timings of your classes";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $select_class_type = $("#select_class_type").val();	
	if($select_class_type==null || $select_class_type.length<=0) {
		$element = $('#select_class_type');
		$message = "Select class/batch type";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $select_max_students_per_class = $("#select_max_students_per_class").val();
	var $txt_length_of_course = $("#txt_length_of_course").val();
	var $select_length_of_course = $("#select_length_of_course").val();
	var $select_school_students = $("#select_school_students").val();
	var $txt_about_the_course = $("#txt_about_the_course").val();
	if($txt_about_the_course.length<=10) {
		$element = $('#txt_about_the_course');
		$message = "Please write something about the course/batch";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $select_fees_type = $("#select_fees_type").val();
	if($select_fees_type==null || $select_fees_type.length<=0) {
		$element = $('#select_fees_type');
		$message = "Please select types of fees you charge";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $fees_collections_fields = $(".div_fees_colletion_fields").find('.txt_fees_type_amount');
	
	var $is_fees_amount_empty = false;
	var $fees_collected_array = [];
	
	$fees_collections_fields.each(function(index){		
		var $fees_collection_amount = $(this).val();
		if($.isNumeric($fees_collection_amount) && $fees_collection_amount > 0) {
			var $fees_collection_type = {};
			$fees_collection_type.amount = $fees_collection_amount;
			$fees_collection_type.fees_type_id = $(this).data('feestypeid');
			$fees_collected_array.push($fees_collection_type);
		} else {
			$is_fees_amount_empty = true;
		}
	});
	if($is_fees_amount_empty) {
		$element = $('.div_fees_colletion_fields');
		$message = "Fees amount can not be empty or 0. Please intput fees amount.";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	var $is_locations_empty = true;
	var $select_location_own = $("#select_location_own").val();
	var $select_location_student = $("#select_location_student").val();
	var $select_location_institute = $("#select_location_institute").val();
	var $teaching_locations_array = [];
	
	var $own_location_fees_array = [];
	var $student_location_fees_array = [];
	var $institute_location_fees_array = [];
	
	if($select_location_own!= null) {
		$is_locations_empty = false;
		var $teaching_location_own = {};
		$teaching_location_own.type = 'own';
		$teaching_location_own.data = $select_location_own;
		$teaching_locations_array.push($teaching_location_own);
		
		var $own_location_fees_fields = $(".div_own_location_fees").find('.txt_own_location_fees_amount');
		$own_location_fees_fields.each(function() {
			var $own_location_id = $(this).data('own_location_id');
			var $own_location_fees_amount = $(this).val();
			if(!$.isNumeric($own_location_fees_amount)) {
				$own_location_fees_amount = 0;
			}
			if($.isNumeric($own_location_id)) {
				var $own_location_fees_data = {};
				$own_location_fees_data.location = $own_location_id;
				$own_location_fees_data.amount = $own_location_fees_amount;
				$own_location_fees_array.push($own_location_fees_data);
			}
		});
		
	} 
	if($select_location_student!=null) {
		$is_locations_empty = false;
		var $teaching_location_student = {};
		$teaching_location_student.type = 'student';
		$teaching_location_student.data = $select_location_student;
		$teaching_locations_array.push($teaching_location_student);
		
		var $student_location_fees_fields = $(".div_student_location_fees").find('.txt_student_location_fees_amount');
		$student_location_fees_fields.each(function() {
			var $student_location_id = $(this).data('student_location_id');
			var $student_location_fees_amount = $(this).val();
			if(!$.isNumeric($student_location_fees_amount)) {
				$student_location_fees_amount = 0;
			}
			if($.isNumeric($student_location_id)) {
				var $student_location_fees_data = {};
				$student_location_fees_data.location = $student_location_id;
				$student_location_fees_data.amount = $student_location_fees_amount;
				$student_location_fees_array.push($student_location_fees_data);
			}
		});
	}
	if($select_location_institute!=null) {
		$is_locations_empty = false;
		var $teaching_location_institute = {};
		$teaching_location_institute.type = 'institute';
		$teaching_location_institute.data = $select_location_institute;
		$teaching_locations_array.push($teaching_location_institute);
		
		var $institute_location_fees_fields = $(".div_institute_location_fees").find('.txt_institute_location_fees_amount');
		
		$institute_location_fees_fields.each(function() {
			var $institute_location_id = $(this).data('institute_location_id');
			var $institute_location_fees_amount = $(this).val();
			if(!$.isNumeric($institute_location_fees_amount)) {
				$institute_location_fees_amount = 0;
			}
			if($.isNumeric($institute_location_id)) {
				var $institute_location_fees_data = {};
				$institute_location_fees_data.location = $institute_location_id;
				$institute_location_fees_data.amount = $institute_location_fees_amount;
				$institute_location_fees_array.push($institute_location_fees_data);
			}
		});
	}
	
	if($is_locations_empty) {
		$element = $('.div_locations_area');
		$message = "Please input atleast one location ";			
		fn_notify_course_error($element, $message, true);
		return;
	}
	
	/**
	 * 1. save batch
	 * 2. rest batch data
	 * 3. reload batches list
	 * 4. check if tab movement is involved. If tab movement requested then move tabs  
	 */
	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
	$hidden_user_id = $("#hidden_dynamic_user_var").val(),
	$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
	$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
	
	var $hidden_batch_id = $("#hidden_batch_id").val();
	var $message = '';
	var $btn = $(".cmd_add_more_batch");
	var $result_area = $(".div_add_batch_result_area");
	var $loader = $(".img_batches_loader");
	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_save_institute_batch_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		hidden_dynamic_insti_var : $hidden_institute_id,
		hidden_dynamic_user_var : $hidden_user_id,
		hidden_dynamic_profile_edit_secret_key : $hidden_user_email,
		hidden_dynamic_user_info_var : $user_personal_info_id,
		
		courses_id_array : $courses_id_array,
		select_course_days : $select_course_days,
		select_no_of_days : $select_no_of_days,
		select_no_of_hours : $select_no_of_hours,
		select_class_timing_slot : $select_class_timing_slot,
		select_class_type : $select_class_type,
		select_max_students_per_class : $select_max_students_per_class,
		txt_length_of_course : $txt_length_of_course,
		select_length_of_course : $select_length_of_course,
		select_school_students : $select_school_students,
		txt_about_the_course : $txt_about_the_course,
		fees_collected_array : $fees_collected_array,
		teaching_locations_array : $teaching_locations_array,
		hidden_batch_id : $hidden_batch_id,
		own_location_fees_array : $own_location_fees_array,
		student_location_fees_array : $student_location_fees_array,
		institute_location_fees_array : $institute_location_fees_array
	};
		
	$btn.button('loading');
	$result_area.html('<div class="alert alert-info">Saving Batch details...</div>');
	$loader.show();
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				fn_alert_message($result_area, $message, 'error');
			} else {				
				fn_alert_message($result_area, $message, 'success');
				/**
				 *	reset 
				 *  reload batch details
				 */
				fn_reset_batch_form();
				fn_reload_batches_list();
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    fn_alert_message($result_area, $message, 'blank_error');
		} finally {
			$btn.button('reset');
			$loader.hide();
		}
    }).complete(function() {        	
    	$btn.button('reset');
		$loader.hide();
    });
	
}

function fn_reload_batches_list() {
	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
	$hidden_user_id = $("#hidden_dynamic_user_var").val(),
	$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
	$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
		
	var $result_area = $(".div_add_batch_result_area");
	var $loader = $(".img_batches_loader");
	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_batches_list_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		hidden_dynamic_insti_var : $hidden_institute_id,
		hidden_dynamic_user_var : $hidden_user_id,
		hidden_dynamic_profile_edit_secret_key : $hidden_user_email,
		hidden_dynamic_user_info_var : $user_personal_info_id	
	};
		
	//$btn.button('loading');
	$result_area.html('<div class="alert alert-info">Saving Batch details...</div>');
	$loader.show();
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				fn_alert_message($result_area, $message, 'error');
			} else {				
				fn_alert_message($result_area, $message, 'success');
				/**
				 *	reset 
				 *  reload batches list
				 */
				$("#tbl_batches_list tbody").html($data);
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    fn_alert_message($result_area, $message, 'blank_error');
		} finally {			
			$loader.hide();
		}
    }).complete(function() {
		$loader.hide();
    });
}

function fn_notify_course_error($element, $message, $bootbox) {
	
	if($bootbox) {
		bootbox.alert($message, function() {			
			$element[0].scrollIntoView( true );
			$element.notify($message, 'error');	
		});
	} else {
		$element[0].scrollIntoView( true );
		$element.notify($message, 'error');
	}
}

function fn_reset_batch_form() {
	console.log('batch resetted');
	$("#tbl_course_list tbody").html('');	
		
	$("#select_subject_name, #select_course_days, #select_no_of_days, #select_no_of_hours, #select_class_timing_slot").val('').trigger('change');
	
	$("#select_class_type, #select_max_students_per_class, #select_length_of_course, #select_school_students, #select_fees_type").val('').trigger('change');
		
	$("#txt_length_of_course, #txt_about_the_course").val('');
	$("#select_location_own, #select_location_student, #select_location_institute").val('').trigger('change');
	$("#div_fees_colletion_fields").html('');
	$("#hidden_batch_id").val('');
}

function fn_load_batch_details($batch_id, $clone) {
	fn_reset_batch_form();
	
	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
	$hidden_user_id = $("#hidden_dynamic_user_var").val(),
	$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
	$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
	
	var $result_area = $(".div_add_batch_result_area");
	var $loader = $(".img_batches_loader");
	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_load_batch_details_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		hidden_dynamic_insti_var : $hidden_institute_id,
		hidden_dynamic_user_var : $hidden_user_id,
		hidden_dynamic_profile_edit_secret_key : $hidden_user_email,
		hidden_dynamic_user_info_var : $user_personal_info_id,
		batch_id : $batch_id	
	};
	
	$result_area.html('<div class="alert alert-info">Loading Batch details...</div>');
	$loader.show();
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;				 	
			
			if($error) {
				fn_alert_message($result_area, $message, 'error');
			} else {				
				fn_alert_message($result_area, $message, 'success');
				/*console.log($data);
				console.log($data.batch_data[0].tuition_batch_id);*/
				var $course_table = $data.str_course_table;
				$("#tbl_course_list tbody").append($course_table);
				
				if($clone === undefined) {
					var $tuition_batch_id = $data.batch_data[0].tuition_batch_id;
					$("#hidden_batch_id").val($tuition_batch_id);
				}
				
				var $course_days = $data.batch_data[0].course_days;
				if($course_days.length>0) {
					$("#select_course_days").val($course_days.split(',')).trigger("change");	
				}				
				
				var $no_of_days = $data.batch_data[0].no_of_days;
				var $days_per_week = $data.batch_data[0].days_per_week;
				$("#select_no_of_days").val($days_per_week).trigger("change");
				
				var $class_duration = $data.batch_data[0].class_duration;
				$("#select_no_of_hours").val($class_duration).trigger("change");
				
				var $class_type = $data.batch_data[0].class_type;				
				$("#select_class_type").val($class_type).trigger("change");
				
				var $class_capacity_id = $data.batch_data[0].class_capacity_id;
				$("#select_max_students_per_class").val($class_capacity_id).trigger("change");
				
				var $course_length = $data.batch_data[0].course_length;
				$("#txt_length_of_course").val($course_length);
				
				var $course_length_type = $data.batch_data[0].course_length_type;
				$("#select_length_of_course").val($course_length_type).trigger("change");
				
				var $about_course = $data.batch_data[0].about_course;
				$("#txt_about_the_course").val($about_course);
				
				var $school_array = $data.school_array;
				$("#select_school_students").val($school_array).trigger("change");
				
				var $class_timing_array = $data.class_timing_array;
				$("#select_class_timing_slot").val($class_timing_array).trigger("change");
				
				var $own_location_array = $data.own_location_array;
				$("#select_location_own").val($own_location_array).trigger("change");
				
				var $student_location_array = $data.student_location_array;
				$("#select_location_student").val($student_location_array).trigger("change");
				
				var $institute_location_array = $data.institute_location_array;
				$("#select_location_institute").val($institute_location_array).trigger("change");
				
				var $fees_type_array = $data.fees_type_array;
				$("#select_fees_type").val($fees_type_array).trigger("change");
				
				var $fees_html = $data.fees_html; 
				$(".div_fees_colletion_fields").html($fees_html);
				$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
				
				$("input:checkbox").uniform();
				//process locality fees
				var $locality_fees_data = {};
				$locality_fees_data.own_locality = $data.own_location_fees_array;
				$locality_fees_data.student_locality = $data.student_location_fees_array;
				$locality_fees_data.institute_locality = $data.institute_location_fees_array; 
				fn_populate_location_fees($locality_fees_data);
			}
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    fn_alert_message($result_area, $message, 'blank_error');
		} finally {			
			$loader.hide();
		}
    }).complete(function() {
		$loader.hide();
    });
}

function fn_populate_location_fees($locality_fees) {
	var $own_locality_fees = $locality_fees.own_locality;
	var $student_locality_fees = $locality_fees.student_locality;
	var $institute_locality_fees = $locality_fees.institute_locality;
	$.each($own_locality_fees, function($locality_id, $fees_amount){
		$(".div_own_location_fees").find('[data-own_location_id="'+$locality_id+'"]').val($fees_amount);
	});
	$.each($student_locality_fees, function($locality_id, $fees_amount){
		$(".div_student_location_fees").find('[data-student_location_id="'+$locality_id+'"]').val($fees_amount);
	});
	$.each($institute_locality_fees, function($locality_id, $fees_amount){
		$(".div_institute_location_fees").find('[data-institute_location_id="'+$locality_id+'"]').val($fees_amount);
	});
}

function fn_delete_batch($batch_id) {
	fn_reset_batch_form();
	
	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
	$hidden_user_id = $("#hidden_dynamic_user_var").val(),
	$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
	$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
	
	var $result_area = $(".div_add_batch_result_area");
	var $loader = $(".img_batches_loader");
	
	bootbox.dialog({
		message: "Are you sure, you want to delete this batch?",
		title: "Batch deletion confirmation...",
		buttons: {			
			danger: {
				label: "Confirm Delete!",
				className: "btn-danger",
				callback: function() {
					
					var $submit_data = {
						nonce : $ajaxnonce,
						action : $pedagoge_visitor_ajax_handler,
						pedagoge_callback_function : 'fn_delete_batch_ajax',
						pedagoge_callback_class : $pedagoge_callback_class,
						hidden_dynamic_insti_var : $hidden_institute_id,
						hidden_dynamic_user_var : $hidden_user_id,
						hidden_dynamic_profile_edit_secret_key : $hidden_user_email,
						hidden_dynamic_user_info_var : $user_personal_info_id,
						batch_id : $batch_id	
					};
					
					$result_area.html('<div class="alert alert-info">Loading Batch details...</div>');
					$loader.show();
					
					$.post($ajax_url, $submit_data, function(response) {
						try {
							var $response_data = $.parseJSON(response);
						
							var $error = $response_data.error,									 	
							 	$message = $response_data.message,
							 	$data = $response_data.data;				 	
							
							if($error) {
								fn_alert_message($result_area, $message, 'error');
							} else {				
								fn_alert_message($result_area, $message, 'success');
								fn_reload_batches_list();
							}
						} catch( err ) {
							var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
						    fn_alert_message($result_area, $message, 'blank_error');
						} finally {			
							$loader.hide();
						}
				    }).complete(function() {
						$loader.hide();
				    });
				}
			},
			main: {
				label: "Cancel",
				className: "btn-primary",
			}
		}
	});
	
	
}

/**
 *  /////////////////////////////////////////////////Form Finishing Code///////////////////////////////////////////////
 */
$(document).ready(function(){
    
    /***code by pallav js***/
    
	$("#chk_allow_demo_classes").click(function() {
	    if($(this).is(":checked")) {
	    	
	        $(".select_no_of_demo_classes").show();
	        $(".txt_price_per_demo_class").show();
	    } else {
	    	
	        $(".select_no_of_demo_classes").hide();
    		$(".txt_price_per_demo_class").hide();
	    }
	});
    $("#div_achievements").on('keyup', '.txt_class_achievements', function(e){
    	var $text_achievements = $(this).val();
		if($text_achievements.length > 3 && $text_achievements.length <= 6) {
			fn_add_class_achievements();			
		}
	});
	
	$("#div_class_images_list").on('click', '.thumbnail', function(event){
		event.preventDefault();
	});
	
	$("#div_class_images_list").on('click', '.cmd_remove_gallery_file', function(event){
		event.preventDefault();		
		var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
		$hidden_user_id = $("#hidden_dynamic_user_var").val(),
		$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
		$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
		
		var $image_file_name = $(this).data('file_name');
		var $container_div = $(this).closest('.col-sm-6');
				
		bootbox.dialog({
			message: "Are you sure, you want to delete this image?",
			title: "Image deletion confirmation...",
			buttons: {			
				danger: {
					label: "Confirm Delete!",
					className: "btn-danger",
					callback: function() {
						
						var $submit_data = {
							nonce : $ajaxnonce,
							action : $pedagoge_visitor_ajax_handler,
							pedagoge_callback_function : 'fn_delete_class_image_ajax',
							pedagoge_callback_class : $pedagoge_callback_class,
							hidden_dynamic_insti_var : $hidden_institute_id,
							hidden_dynamic_user_var : $hidden_user_id,
							hidden_dynamic_profile_edit_secret_key : $hidden_user_email,
							hidden_dynamic_user_info_var : $user_personal_info_id,
							image_file_name : $image_file_name
						};
						
						$("#div_class_images_list").block({ 
			                message: '<h3 alert alert-warning>Deleting class image...</h3>',
			                css: { border: '3px solid #a00' }
			            }); 
						
						$.post($ajax_url, $submit_data, function(response) {
							try {
								var $response_data = $.parseJSON(response);
							
								var $error = $response_data.error,									 	
								 	$message = $response_data.message,
								 	$data = $response_data.data;				 	
								
								if($error) {
									bootbox.alert($message);
								} else {
									$container_div.remove();
								}
							} catch( err ) {
								var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
							    
							    console.log($message);
							} finally {
								$("#div_class_images_list").unblock();
							}
					    }).complete(function() {
							$("#div_class_images_list").unblock();
					    });
					}
				},
				main: {
					label: "Cancel",
					className: "btn-primary",
				}
			}
		});
	});
	
	$("#cmd_upload_class_image").click(function(event){
		
    	event.preventDefault();
    	var $selected_file = $("#txt_institute_class_image_file_name").val();
    	var $file_input = $("#fileinput_institute_class_image");
    	
    	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
		$hidden_user_id = $("#hidden_dynamic_user_var").val(),
		$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
		$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
    	
    	var $button = $(this),
    		$result_area = $("#div_class_image_result_area");
    	
    	if($selected_file.length<=0) {
    		$file_input.click();	
    	} else {
    		//console.log('upload');
    		//var $upload_data = new FormData();
    		
    		if($file_input.get(0).files.length === 0) {
				//console.log('file not available');
			} else {
				
				var $upload_data = new FormData();
				$upload_data.append('gallery_image', $file_input.get(0).files[0]);
				$upload_data.append("nonce", $ajaxnonce);
				$upload_data.append("action", $pedagoge_visitor_ajax_handler);
				$upload_data.append("pedagoge_callback_function", 'fn_save_institute_class_image_ajax');
				$upload_data.append("pedagoge_callback_class", $pedagoge_callback_class);
				
				$upload_data.append("hidden_dynamic_insti_var", $hidden_institute_id);
				$upload_data.append("hidden_dynamic_user_var", $hidden_user_id);
				$upload_data.append("hidden_dynamic_profile_edit_secret_key", $hidden_user_email);
				$upload_data.append("hidden_dynamic_user_info_var", $user_personal_info_id);
				
				$result_area.html('<div class="alert alert-info">Uploading image...</div>');
				$button.button('loading');
				
				$.ajax({
					url : $ajax_url,
				    data : $upload_data,
				    processData : false, 
				    contentType : false,
				    type : 'POST',			    
				    success : function(response) {
				    	try {
						
							var $response_data = $.parseJSON(response);				
							var $error = $response_data.error, 
								$error_type = $response_data.error_type,				 	
							 	$message = $response_data.message,
							 	$data = $response_data.data;
							if($error) {
								fn_alert_message($result_area, $message, 'error');
							} else {
								fn_alert_message($result_area, $message, 'success');
								var $image_url = $data.image_url;
								var $image_file_name = $data.image_file_name;
								$("#txt_institute_class_image_file_name").val('');
								resetFormElement($file_input);
								
								var $upload_html = '<div class="col-sm-6 col-md-4">'+
														'<div class="thumbnail">'+
															'<img src="'+$image_url+'" class="img-responsive thumbbox" />'+
															'<hr />'+
															'<div class="caption">'+
																'<p>'+
																	'<button class="btn btn-danger col-xs-12 col-md-12 cmd_remove_gallery_file" data-file_name="'+$image_file_name+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>'+
																'</p>'+
															'</div>'+
														'</div>'+
													'</div>';
								$("#div_class_images_list").append($upload_html);
							}
							
						} catch(err) {
							$button.button('reset');
							console.log('error occured '+err);
						}
				    },
				    complete:function(){
				    	$button.button('reset');
				    }
				});
			}
    	}
    });
    $("#cmd_reset_class_image_selection").click(function(event){
    	event.preventDefault();
    	$("#div_class_image_result_area").html('');
		$("#span_select_image").html('Select image');
		$("#txt_institute_class_image_file_name").val('');
		resetFormElement($("#fileinput_institute_class_image"));
    });
    
    $("#fileinput_institute_class_image").change(function(event) {    	
    	//event.preventDefault();
    	var $institute_class_image_uploaded_files = event.target.files;
    	var $file_length = $institute_class_image_uploaded_files.length;
		
		$("#div_class_image_result_area").html('');
		$("#span_select_image").html('Select image');
		$("#txt_institute_class_image_file_name").val('');
		
    	var $str_files_status = '',
    	$file_name = '',
    	$file_size = 0,
    	$file_extension = '',    	
    	$is_image_error = false;
    	
    	if($institute_class_image_uploaded_files.length > 0) {
    		$file_name = $institute_class_image_uploaded_files[0].name;
	    	$file_size = $institute_class_image_uploaded_files[0].size;
	    	$file_extension = $file_name.substring($file_name.lastIndexOf('.') + 1).toLowerCase();
	    	
	    	var $str_size_kb_mb = 'KB',
	    	iSize = 0;
	    	
	    	if($file_size > 0) {
				$file_size = $file_size/1024;
				iSize = (Math.round($file_size * 100) / 100);
				
				if( iSize > 2100 ) {
					$is_image_error = true;
					$str_files_status += 'Error! Profile picture size should not be more than 2 MB.<br/>';
				}
			}
			
			if($file_extension === 'gif' || $file_extension === 'png' || $file_extension === 'jpeg' || $file_extension === 'jpg') {
				
			} else {
				$is_image_error = true;
				$str_files_status += 'Error! Only Image Files having any of the following extensions (.jpg, .jpeg, .png, .gif) would be uploaded!';
			}
    	}
    	
    	if($str_files_status.length>0) {
    		$str_files_status = '<div class="alert alert-primary">'+$str_files_status+'</div>';
    	}
    	if(!$is_image_error) {
    		$("#span_select_image").html('Upload image');
    		$("#txt_institute_class_image_file_name").val($file_name);
    	}
    	$("#div_class_image_result_area").html($str_files_status);
    });
    
    $(".cmd_submit_institute_details").click(function(event){
    	
    	var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),
		$hidden_user_id = $("#hidden_dynamic_user_var").val(),
		$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
		$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
    	
    	var $button = $(this),
    		$result_area = $(".div_institute_save_result_area"),
    		$loader = $(".img_institute_save_loader");
    		
    	var $txt_fees_structure_description = $("#txt_fees_structure_description").val();
		var $chk_payment_installments = $("#chk_payment_installments").prop('checked') ? 'yes' : 'no';	
		var $chk_allow_demo_classes = $("#chk_allow_demo_classes").prop('checked') ? 'yes' : 'no';
		var $price_per_demo_class = $('#txt_price_per_demo_class').val();
		
		var $txt_class_achievements = $("#div_achievements").find('.txt_class_achievements');
		var $achievements_array = [];
		
		$txt_class_achievements.each(function(index){		
			var $achievement_val = $(this).val();
			if($achievement_val.length > 0) {
				$achievements_array.push($achievement_val);
			}
		});
		
		var $data_to_save = {};
		$data_to_save.txt_fees_structure_description = $txt_fees_structure_description;
		$data_to_save.chk_payment_installments = $chk_payment_installments;
		$data_to_save.chk_allow_demo_classes = $chk_allow_demo_classes;
		$data_to_save.price_per_demo_class = $price_per_demo_class;
		$data_to_save.achievements_array = $achievements_array;
		
		var $upload_data = new FormData();
		$upload_data.append('institute_data', JSON.stringify($data_to_save));
		$upload_data.append("nonce", $ajaxnonce);
		$upload_data.append("action", $pedagoge_visitor_ajax_handler);
		$upload_data.append("pedagoge_callback_function", 'fn_save_institute_final_details_ajax');
		$upload_data.append("pedagoge_callback_class", $pedagoge_callback_class);
		$upload_data.append("hidden_dynamic_insti_var", $hidden_institute_id);
		$upload_data.append("hidden_dynamic_user_var", $hidden_user_id);
		$upload_data.append("hidden_dynamic_profile_edit_secret_key", $hidden_user_email);
		$upload_data.append("hidden_dynamic_user_info_var", $user_personal_info_id);
		
		$button.button('loading');
		$loader.show();
		$result_area.html('<h2>Please wait...</h2>');
		
		$.ajax({
		url : $ajax_url,
	    data : $upload_data,
	    processData : false, 
	    contentType : false,
	    type : 'POST',			    
	    success : function(response) {
	    	try {
			
				var $response_data = $.parseJSON(response);				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message,
				 	$data = $response_data.data;
				if($error) {
					fn_alert_message($result_area, $message, 'error');
				} else {
					fn_alert_message($result_area, $message, 'success');
					$('#modal_profile_finished').modal('show');
				}
			} catch(err) {
				$button.button('reset');
				console.log('error occured '+err);
			}
	    },
	    complete:function(){
	    	$button.button('reset');
			$loader.hide();
	    }
	});
		
    });
    
});

function fn_add_class_achievements() {
		
	var $empty_counter = 0;
	
	var $txt_class_achievements = $("#div_achievements").find('.txt_class_achievements');
	
	$txt_class_achievements.each(function(){
		var $text_achievements = $(this).val();
		if($text_achievements.length<=0) {
			$empty_counter++;
		}
	});
	
	if($empty_counter >= 1) {
		$txt_class_achievements.each(function(){
			var $text_achievements = $(this).val();
			if($text_achievements.length<=0) {
				$(this).closest('.row').remove();
				$empty_counter--;
			}
		});
	}
	
	if($empty_counter<=0) {
		var $str_text_field = '<div class="row"><input type="text" class="form-control txt_class_achievements" placeholder="Achievements" /></div>';
		$("#div_achievements").append($str_text_field);
	}
}


/****************************************
 * Profile Image cropper 
 */

var $cropper_active = false;
	
$(document).ready(function(){
	$(".avatar-view").tooltip({
        placement: 'bottom'
	});
		
	$(".avatar-view").click(function(event){
		fn_avatar_preview();
		$("#avatar-modal").modal('show');	
	});
	
	$("#avatarInput").change(function(){
		var $files = $(this).prop('files');
		if($files.length > 0) {
			var $file = $files[0];
		}
		if(isImageFile($file)) {
			var $url = URL.createObjectURL($file);
			var $image_html = '<img src="' + $url + '">';
			$(".avatar-preview").html($image_html);
			$(".avatar-wrapper").empty().html($image_html);
			$(".avatar-wrapper").find('img').cropper({
	          aspectRatio: 1,
	          preview: '.avatar-preview',
	          crop: function (e) {
	            var json = [
	                  '{"x":' + e.x,
	                  '"y":' + e.y,
	                  '"height":' + e.height,
	                  '"width":' + e.width,
	                  '"rotate":' + e.rotate + '}'
	                ].join();
	
	            //_this.$avatarData.val(json);
	            //console.log(json);
	            $(".avatar-data").val(json);
	            $cropper_active = true;
	          }
	        });			
		}
	});
	
	$("#avatar-modal").on("hidden.bs.modal", function () {
	    // put your default event here
	    fn_remove_cropper();
	});
	
	$(".avatar-save").click(function(event){
		if(!$cropper_active) {
			return false;
		}
		/*.avatar-data
		#avatarInput*/
		var $avatar_data = $(".avatar-data").val();
		
		var $hidden_institute_id = $("#hidden_dynamic_insti_var").val(),		
		$hidden_user_id = $("#hidden_dynamic_user_var").val(),
		$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
		$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();
    	
    	var $button = $(this),
    		$result_area = $(".crop_result_area");
    		
		var $upload_data = new FormData();		
		$upload_data.append("nonce", $ajaxnonce);
		$upload_data.append("action", $pedagoge_visitor_ajax_handler);
		$upload_data.append("pedagoge_callback_function", 'fn_save_institute_profile_image_ajax');
		$upload_data.append("pedagoge_callback_class", $pedagoge_callback_class);
		
		$upload_data.append("hidden_dynamic_insti_var", $hidden_institute_id);
		$upload_data.append("hidden_dynamic_user_var", $hidden_user_id);
		$upload_data.append("hidden_dynamic_profile_edit_secret_key", $hidden_user_email);
		$upload_data.append("hidden_dynamic_user_info_var", $user_personal_info_id);
		$upload_data.append('profile_image', $("#avatarInput").get(0).files[0]);
		$upload_data.append("avatar_data", $avatar_data);
		
		$result_area.html('<div class="alert alert-info">Uploading image...</div>');
		$button.button('loading');
		
		$.ajax({
			url : $ajax_url,
		    data : $upload_data,
		    processData : false, 
		    contentType : false,
		    type : 'POST',			    
		    success : function(response) {
		    	try {
				
					var $response_data = $.parseJSON(response);				
					var $error = $response_data.error, 
						$error_type = $response_data.error_type,				 	
					 	$message = $response_data.message,
					 	$data = $response_data.data;
					if($error) {
						fn_alert_message($result_area, $message, 'error');
					} else {
						fn_alert_message($result_area, $message, 'success');
						$("#avatar-modal").modal('hide');
						$(".avatar-view").find('img').prop('src', $data);
					}
					
				} catch(err) {
					$button.button('reset');
					console.log('error occured '+err);
				}
		    },
		    complete:function(){
		    	$button.button('reset');
		    }
		});	
	});
	
});

function isImageFile($file) {
	if ($file.type) {
		return /^image\/\w+$/.test($file.type);
	} else {
		return /\.(jpg|jpeg|png|gif)$/.test($file);
	}
}


function fn_avatar_preview() {
	var $avatar = $(".avatar-view").find('img');
	var $image_url = $avatar.attr('src');

	$(".avatar-preview").html('<img src="' + $image_url + '">');
	$(".avatar-data").val('');
	$(".crop_result_area").html('');
}

function fn_remove_cropper() {
	var $img =$(".avatar-wrapper").find('img'); 
	$img.cropper('destroy');
	$img.remove();
	resetFormElement($("#avatarInput"));
	$cropper_active = false;
	$(".crop_result_area").html('');
}
