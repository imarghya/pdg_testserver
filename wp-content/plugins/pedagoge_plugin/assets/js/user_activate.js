///////////////////////////////////////Login Form Processing Starts////////////////////////////////////////////////
/**
 *Login Form Processing 
 */
$(document).ready(function() 
{	
	$('#diff_email_addr').hide();
	$('#cmd_alt_profile_activation_link').hide();
	$("#cmd_profile_activation_link").click( function(event) {
		event.preventDefault(); //incase button becomes submit form.
						
		var $button = $(this),		
		 	$hidden_email_address = $("#hidden_email_address").val(),
		 	$hidden_account_activation_key = $("#hidden_account_activation_key").val();
	    
		$button.button('loading');
		$("#img_profile_activate_loader").show();
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'send_activation_email',
			pedagoge_callback_class: 'ControllerActivate',
			email_address : $hidden_email_address,
			account_activation_key : $hidden_account_activation_key			
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message;
				
				if($error) {					
					fn_alert_message('div_profile_activate_result', $message, 'error');					
				} else {						
					fn_alert_message('div_profile_activate_result', $message, 'success');
				}
			} catch(err) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message('div_profile_activate_result', $message, 'error');
			    					
			} finally {
				$button.button('reset');
				$("#img_profile_activate_loader").hide();
			}
			 
        }).complete(function() {
        	$button.button('reset');
        	$("#img_profile_activate_loader").hide();
        });
	    	
	});

	$("#cmd_alt_profile_activation_link").click( function(event) {
		event.preventDefault(); //incase button becomes submit form.
						
		var $button = $(this),		
		 	$alternate_email_address = $("#diff_email_addr").val(),
		 	$hidden_account_activation_key = $("#hidden_account_activation_key").val();
	    
		$button.button('loading');
		$("#img_profile_activate_loader").show();
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'send_activation_email',
			pedagoge_callback_class: 'ControllerActivate',
			email_address : $alternate_email_address,
			account_activation_key : $hidden_account_activation_key			
		};
		
		$.post($ajax_url, submit_data, function(response) {
			try {
				
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message;
				
				if($error) {					
					fn_alert_message('div_profile_activate_result', $message, 'error');					
				} else {						
					fn_alert_message('div_profile_activate_result', $message, 'success');
				}
			} catch(err) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
			    fn_alert_message('div_profile_activate_result', $message, 'error');
			    					
			} finally {
				$button.button('reset');
				$("#img_profile_activate_loader").hide();
			}
			 
        }).complete(function() {
        	$button.button('reset');
        	$("#img_profile_activate_loader").hide();
        });
	    	
	});

	$('#diff_email').click(function(){
		$('#diff_email_addr').show();
		$('#cmd_profile_activation_link').hide();
		$('#cmd_alt_profile_activation_link').show();
	});


});
///////////////////////////////////////Login Form Processing Ends////////////////////////////////////////////////

