/**
 *Password reset Processing
 */
$( document ).ready( function () {
	$( "#frm_username_send_password_reset_link, #frm_send_password_reset_link" ).validationEngine( {
		scroll: false,
		autoHidePrompt: true,
		autoHideDelay: 3500,
		fadeDuration: 0.3,
		promptPosition: "topLeft:0"
	} );
	
	$( "#frm_username_send_password_reset_link, #frm_send_password_reset_link" ).submit( function ( event ) {
		event.preventDefault();
	} );
	
	$( "#cmd_password_reset" ).click( function ( event ) {
		event.preventDefault();
		var $button = $( this ),
			$txt_pass_reset_email = $( "#txt_pass_reset_email" ),
			$img_password_reset_loader = $( "#img_password_reset_loader" ),
			$div_pass_reset_result = $( "#div_pass_reset_result" ),
			$frm_send_password_reset_link = $( "#frm_send_password_reset_link" );
		if ( $frm_send_password_reset_link.validationEngine( 'validate', {
				scroll: false,
				autoHidePrompt: true,
				autoHideDelay: 3500,
				fadeDuration: 0.3,
				promptPosition: "topLeft:0"
			} ) ) {
			
			$button.button( 'loading' );
			
			$img_password_reset_loader.show();
			
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'send_password_reset_link_ajax',
				pedagoge_callback_class: 'ControllerReset',
				email_address: $txt_pass_reset_email.val(),
				
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				try {
					var $response_data = $.parseJSON( response );
					
					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message;
					
					if ( $error ) {
						pdg_show_alerts( $div_pass_reset_result, $message, 'error' );
						$txt_pass_reset_email.select().focus();
						
					} else {
						pdg_show_alerts( $div_pass_reset_result, $message, 'success' );
						fn_reset_password_reset_forms();
					}
				} catch ( err ) {
					$txt_pass_reset_email.select().focus();
					
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
					pdg_show_alerts( $div_pass_reset_result, $message, 'error' );
				} finally {
					$button.button( 'reset' );
				}
				
			} ).complete( function () {
				$button.button( 'reset' );
				$img_password_reset_loader.hide();
			} );
		}
		
	} );
	
	
	$( "#frm_change_password" ).validationEngine( {
		scroll: false,
		autoHidePrompt: true,
		autoHideDelay: 3500,
		fadeDuration: 0.3,
		promptPosition: "topLeft:0"
	} );
	
	$( "#frm_change_password" ).submit( function ( event ) {
		event.preventDefault();
	} );
	
	$( '#chk_register_show_password' ).change( function () {
		if ( $( this ).is( ":checked" ) ) {
			$( ".class_password" ).prop( 'type', 'text' );
		} else {
			$( ".class_password" ).prop( 'type', 'password' );
		}
	} );
	
	
	$( "#cmd_change_password" ).click( function ( event ) {
		event.preventDefault();
		var $button = $( this ),
			$txt_password = $( "#txt_password" ),
			$img_password_reset_loader = $( "#img_password_reset_loader" ),
			$div_pass_reset_result = $( "#div_pass_reset_result" ),
			$frm_change_password = $( "#frm_change_password" ),
			$hidden_user_id = $( "#hidden_user_id" ),
			$hidden_user_email_encrypted = $( "#hidden_user_email_encrypted" );
		
		if ( $frm_change_password.validationEngine( 'validate', {
				scroll: false,
				autoHidePrompt: true,
				autoHideDelay: 3500,
				fadeDuration: 0.3,
				promptPosition: "topLeft:0"
			} ) ) {
			
			$button.button( 'loading' );
			
			$img_password_reset_loader.show();
			
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'change_password_ajax',
				pedagoge_callback_class: 'ControllerReset',
				password: $txt_password.val(),
				user_id: $hidden_user_id.val(),
				encrypted_email: $hidden_user_email_encrypted.val(),
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				try {
					var $response_data = $.parseJSON( response );
					
					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message,
						$redirect_url = $response_data.redirect_url;
					
					if ( $error ) {
						pdg_show_alerts( $div_pass_reset_result, $message, 'error' );
					} else {
						pdg_show_alerts( $div_pass_reset_result, $message, 'success' );
						setTimeout( function () {
							window.location = $redirect_url;
						}, 500 );
					}
				} catch ( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
					pdg_show_alerts( $div_pass_reset_result, $message, 'error' );
				} finally {
					$button.button( 'reset' );
					$img_password_reset_loader.hide();
				}
				
			} ).complete( function () {
				$button.button( 'reset' );
				$img_password_reset_loader.hide();
			} );
		}
		
	} );
	
	$( "#cmd_show_email_form" ).click( function ( event ) {
		event.preventDefault();
		fn_reset_password_reset_forms();
		$( "#div_reset_username" ).hide();
		$( "#div_reset_email" ).show().removeClass( 'hide' );
		
	} );
	
	$( "#cmd_show_user_form" ).click( function ( event ) {
		event.preventDefault();
		fn_reset_password_reset_forms();
		$( "#div_reset_username" ).show().removeClass( 'hide' );
		$( "#div_reset_email" ).hide();
	} );
	
	
	$( "#frm_username_send_password_reset_link" ).validationEngine( {
		scroll: false,
		autoHidePrompt: true,
		autoHideDelay: 3500,
		fadeDuration: 0.3,
		promptPosition: "topLeft:0"
	} );
	
	$( "#frm_username_send_password_reset_link" ).submit( function ( event ) {
		event.preventDefault();
	} );
	
	$( "#cmd_username_password_reset" ).click( function ( event ) {
		event.preventDefault();
		var $button = $( this ),
			$txt_pass_reset_username = $( "#txt_pass_reset_username" ),
			$img_password_reset_loader = $( "#img_password_reset_loader" ),
			$div_pass_reset_result = $( "#div_pass_reset_result" ),
			$frm_username_send_password_reset_link = $( "#frm_username_send_password_reset_link" );
		
		if ( $frm_username_send_password_reset_link.validationEngine( 'validate', {
				scroll: false,
				autoHidePrompt: true,
				autoHideDelay: 3500,
				fadeDuration: 0.3,
				promptPosition: "topLeft:0"
			} ) ) {
			
			$button.button( 'loading' );
			
			$img_password_reset_loader.show();
			
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'send_password_reset_link_for_username_ajax',
				pedagoge_callback_class: 'ControllerReset',
				username: $txt_pass_reset_username.val(),
				
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				try {
					var $response_data = $.parseJSON( response );
					
					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message;
					
					if ( $error ) {
						pdg_show_alerts( $div_pass_reset_result, $message, 'error' );
						$txt_pass_reset_username.select().focus();
						
					} else {
						pdg_show_alerts( $div_pass_reset_result, $message, 'success' );
						fn_reset_password_reset_forms();
					}
				} catch ( err ) {
					$txt_pass_reset_username.select().focus();
					
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
					pdg_show_alerts( $div_pass_reset_result, $message, 'error' );
				} finally {
					$button.button( 'reset' );
				}
				
			} ).complete( function () {
				$button.button( 'reset' );
				$img_password_reset_loader.hide();
			} );
		}
		
	} );
	
} );

function fn_reset_password_reset_forms () {
	$( "#txt_pass_reset_email" ).val( '' );
	$( "#txt_pass_reset_username" ).val( '' );
}
