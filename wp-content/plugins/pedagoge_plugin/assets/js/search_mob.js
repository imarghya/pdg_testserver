var $ajaxnonce = $ajaxnonce;
$(document).ready(function () {
	$("#search_result_area").on('click', '.cmd_recommend', function (event) {
		event.preventDefault();
		var $recommend_btn = $(this);
		var $current_logged_in_user = parseInt($recommend_btn.data('current_user'));
		var $teacher_user_id = parseInt($recommend_btn.data('teacher_user_id'));
	});

	$("#search_result_pagination").on('click', '.cmd_pagination_btn', function (event) {
		event.preventDefault();
		var $cmd_pagination_btn = $(this);
		var $span_pagination = $cmd_pagination_btn.parent();
		var $page_no = $span_pagination.data('page_no');
		var $is_active = $span_pagination.hasClass('active');

		if ($.isNumeric($page_no) && $page_no > 0 && !$is_active) {
			fn_pedagoge_search($page_no);
			window.scrollTo(0, 0);
			$("#search_result_area").find(".span_pagination").removeClass('active');
			$span_pagination.addClass('active');
		}
	});
});
var search_result_area = $('#search_result_area');
$('.mob-filter-menu-toggle').on('click', function () {
	$(window).scrollTop(search_result_area.offset().top - search_result_area.height());
});

function fn_reset_search_forms_mob() {
	$("#txt_search_subject_mob, #txt_tutor_insti_search_name_mob").val('');
	$("#frm_search_sidebar_mob select").select2("val", "");
	$("#chk_filter_institution_mob, #chk_filter_teacher_mob, .chk_teaching_location_type_mob, .chk_filter_teaching_xp, " +
		".chk_class_type_filter, .chk_filter_academic_board, .chk_class_timing_filter, .chk_fees_filter_mob, #chk_all_day_mob, .chk_days_taught_mob").prop('checked', false);
	$.uniform.update();
}

$(".cmd_sumbit_reset_mob").click(function (event) {
	event.preventDefault();
	fn_reset_search_forms_mob();
	fn_pedagoge_search_mob();
});
$(".cmd_sumbit_search_mob").click(function (event) {
	event.preventDefault();
	$('.mob-filter-menu-toggle').trigger('click');
	fn_pedagoge_search_mob();
});
$(".mob-filter-close-btn").click(function (event) {
	event.preventDefault();
	$('.mob-filter-menu-toggle').trigger('click');
});
fn_load_subjects_localities_mob();
function fn_load_subjects_localities_mob() {
	var submit_data = {
		nonce: $ajaxnonce,
		action: $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function: 'fn_load_subjects_localities',
		pedagoge_callback_class: 'ControllerSearch'
	};

	//console.log($ajaxnonce);
	$.post($ajax_url, submit_data, function (response) {
		try {
			var $response_data = $.parseJSON(response);
			var $error = $response_data.error,
				$error_type = $response_data.error_type,
				$message = $response_data.message,
				$data = $response_data.data;

			if ($error) {
			} else {
				var $subjects_list_array = $data.subjects;
				$("#txt_search_subject_mob").autocomplete({
					source: function (request, response) {
						var results = $.ui.autocomplete.filter($subjects_list_array, request.term);
						response(results.slice(0, 10));
					}
				});
				var $teacher_name_array = $data.teacher_names;
				$("#txt_tutor_insti_search_name_mob").autocomplete({
					source: function (request, response) {
						var results = $.ui.autocomplete.filter($teacher_name_array, request.term);
						response(results.slice(0, 10));
					}
				});

				var $locality_array = $data.locality;
				$("#select_search_locality_mob").select2({
					data: $locality_array
				});

				var $searched_locality = getParameterByName('pdg_locality');
				if ($searched_locality.length > 0) {
					var selectedItems = [];
					selectedItems.push($searched_locality);
					$('#select_search_locality_mob').val($searched_locality).trigger("change");

				}
			}
		} catch (err) {
		} finally {
		}

	}).complete(function () {
	});
}
function fn_pedagoge_search_mob($pagination_no) {
	if ($pagination_no === undefined || $pagination_no === null) {
		$pagination_no = 1;
	}

	var $subject_name = $("#txt_search_subject_mob").val(),
		$insti_teacher_name = $("#txt_tutor_insti_search_name_mob").val(),
		$locality_name = $("#select_search_locality_mob").val(),
		$location_type_array = [],
		$search_type = 'filter',
		$filter_institute_teacher = '',
		$teaching_xp_array = [],
		$class_type_array = [],
		$academic_board_array = [],
		$class_timing_array = [],
		$fees_range_array = [],
		$days_taught_array = [];
	//$found_locality_name = $("#txt_search_locality_mob").val();

	var $chk_teaching_location_type = $(".chk_teaching_location_type_mob");
	$chk_teaching_location_type.each(function (index) {
		var $location_type = $(this);
		var $location_id = $location_type.val();
		if ($location_type.is(':checked')) {
			$location_type_array.push($location_id);
		}
	});

	var $chk_teaching_xp = $(".chk_filter_teaching_xp_mob");
	$chk_teaching_xp.each(function (index) {
		var $teaching_xp = $(this);
		var $teaching_xp_id = $teaching_xp.val();
		if ($teaching_xp.is(':checked')) {
			$teaching_xp_array.push($teaching_xp_id);
		}
	});

	var $chk_class_type = $(".chk_class_type_filter_mob");
	$chk_class_type.each(function (index) {
		var $class_type = $(this);
		var $class_type_id = $class_type.val();
		if ($class_type.is(':checked')) {
			$class_type_array.push($class_type_id);
		}
	});

	var $chk_academic_board = $(".chk_filter_academic_board_mob");
	$chk_academic_board.each(function (index) {
		var $academic_board = $(this);
		var $academic_board_id = $academic_board.val();
		if ($academic_board.is(':checked')) {
			$academic_board_array.push($academic_board_id);
		}
	});

	var $chk_fees_range = $(".chk_fees_filter_mob");
	$chk_fees_range.each(function (index) {
		var $fees_range = $(this);
		var $fees_range_id = $fees_range.val();
		if ($fees_range.is(':checked')) {
			$fees_range_array.push($fees_range_id);
		}
	});

	var $chk_days_taught = $(".chk_days_taught_mob");
	$chk_days_taught.each(function (index) {
		var $day_taught = $(this);
		var $day_taught_name = $day_taught.val();
		if ($day_taught.is(':checked')) {
			$days_taught_array.push($day_taught_name);
		}
	});

	if ($subject_name.length <= 0
		&& $insti_teacher_name.length <= 0
		&& $locality_name == null
		&& $location_type_array.length <= 0
		&& $teaching_xp_array.length <= 0
		&& $class_type_array.length <= 0
		&& $academic_board_array.length <= 0
		&& $class_timing_array.length <= 0
		&& $fees_range_array.length <= 0
		&& $days_taught_array.length <= 0
	//&& $found_locality_name.length <= 0
	) {
		$search_type = 'reset';
	}

	var $teacher_filter_checked = $("#chk_filter_teacher_mob").is(':checked');
	var $institute_filter_checked = $("#chk_filter_institution_mob").is(':checked');

	if ($teacher_filter_checked && $institute_filter_checked) {
		$filter_institute_teacher = '';
	} else if (!$teacher_filter_checked && !$institute_filter_checked) {
		$filter_institute_teacher = '';
	} else if ($teacher_filter_checked) {
		$filter_institute_teacher = '';
		$filter_institute_teacher = 'teacher';
	} else if ($institute_filter_checked) {
		$filter_institute_teacher = 'institution';
	}

	var submit_data = {
		nonce: $ajaxnonce,
		action: $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function: 'fn_search_ajax',
		pedagoge_callback_class: 'ControllerSearch',
		current_user_id: $current_user_id,
		current_user_role: $current_user_role,
		subject_name: $subject_name,
		insti_teacher_name: $insti_teacher_name,
		locality_name: $locality_name,
		location_type_array: $location_type_array,
		filter_institute_teacher: $filter_institute_teacher,
		teaching_xp_array: $teaching_xp_array,
		class_type_array: $class_type_array,
		academic_board_array: $academic_board_array,
		class_timing_array: $class_timing_array,
		fees_range_array: $fees_range_array,
		days_taught_array: $days_taught_array,
		//found_locality_name : $found_locality_name,
		page_no: $pagination_no,
		search_type: $search_type
	};
	var $str_message = '<div class="col-md-12 alert alert-info"><h2>Searching for data...</h2></div>';
	$("#search_result_area").html($str_message);
	$.post($ajax_url, submit_data, function (response) {
		try {
			var $response_data = $.parseJSON(response);
			var $error = $response_data.error,
				$error_type = $response_data.error_type,
				$message = $response_data.message,
				$data = $response_data.data;

			if ($error) {

				switch ($error_type) {
					case 'security':
						var $str_message = '<div class="col-md-12 alert alert-warning"><h2>' + $message + '</h2></div>';
						$("#search_result_area").html($str_message);
						break;
					case 'nothing_found':
						$("#search_result_area").html($data.result);
						$("#search_result_pagination").html($data.pagination);
						break;
					default:
						break;
				}
			} else {

				$("#search_result_area").html($data.result);

				var $str_raw_pagination = $data.pagination;
				var $str_pagination = '';
				var $page_count = $str_raw_pagination.length;
				if ($pagination_no == 1) {
					$str_pagination = $str_pagination + '<li class="disabled" id="li_pagination_first"><a href="javascript:void(0)"><i class="fa fa-angle-double-left"></i></a></li>';
					$str_pagination = $str_pagination + '<li class="disabled" id="li_pagination_left"><a href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>';
				}
				else {
					$str_pagination = $str_pagination + '<li id="li_pagination_first"><a href="javascript:void(0)"><i class="fa fa-angle-double-left"></i></a></li>';
					$str_pagination = $str_pagination + '<li id="li_pagination_left"><a href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>';
				}

				if ($page_count > 10) {
					if ($pagination_no == 1) {
						for (var i = 0; i < 10; i++) {
							$str_pagination = $str_pagination + $str_raw_pagination[i];
						}
					}
					else if ($pagination_no == $page_count) {
						for (var i = $pagination_no - 10; i < $pagination_no; i++) {
							$str_pagination = $str_pagination + $str_raw_pagination[i];
						}
					}
					else if ($pagination_no == 2) {
						for (var i = $pagination_no - 2; i < $pagination_no + 8; i++) {
							$str_pagination = $str_pagination + $str_raw_pagination[i];
						}
					}
					else if ($pagination_no == 3) {
						for (var i = $pagination_no - 3; i < $pagination_no + 7; i++) {
							$str_pagination = $str_pagination + $str_raw_pagination[i];
						}
					}
					else {
						for (var i = $pagination_no - 4; i < $pagination_no + 6; i++) {
							$str_pagination = $str_pagination + $str_raw_pagination[i];
						}
					}
				}
				else {
					for (var i = 0; i < $page_count; i++) {
						$str_pagination = $str_pagination + $str_raw_pagination[i];
					}
				}

				$str_pagination = $str_pagination + '<li id="li_pagination_right"><a href="javascript:void(0)"><i class="fa fa-angle-right"></i></a></li>';
				/*$str_pagination = $str_pagination + '<li id="li_pagination_last"><a href="javascript:void(0)"><i class="fa fa-angle-double-right"></i></a></li>';*/

				$("#search_result_pagination").html($str_pagination);
			}

		} catch (err) {
			console.log('error occured ' + err);
		}
	}).complete(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
}
(function () {
	window.$clamp = function (c, d) {
		function s(a, b) {
			n.getComputedStyle || (n.getComputedStyle = function (a, b) {
				this.el = a;
				this.getPropertyValue = function (b) {
					var c = /(\-([a-z]){1})/g;
					"float" == b && (b = "styleFloat");
					c.test(b) && (b = b.replace(c, function (a, b, c) {
						return c.toUpperCase()
					}));
					return a.currentStyle && a.currentStyle[b] ? a.currentStyle[b] : null
				};
				return this
			});
			return n.getComputedStyle(a, null).getPropertyValue(b)
		}

		function t(a) {
			a = a || c.clientHeight;
			var b = u(c);
			return Math.max(Math.floor(a / b), 0)
		}

		function x(a) {
			return u(c) *
				a
		}

		function u(a) {
			var b = s(a, "line-height");
			"normal" == b && (b = 1.2 * parseInt(s(a, "font-size")));
			return parseInt(b)
		}

		function l(a) {
			if (a.lastChild.children && 0 < a.lastChild.children.length)return l(Array.prototype.slice.call(a.children).pop());
			if (a.lastChild && a.lastChild.nodeValue && "" != a.lastChild.nodeValue && a.lastChild.nodeValue != b.truncationChar)return a.lastChild;
			a.lastChild.parentNode.removeChild(a.lastChild);
			return l(c)
		}

		function p(a, d) {
			if (d) {
				var e = a.nodeValue.replace(b.truncationChar, "");
				f || (h = 0 < k.length ?
					k.shift() : "", f = e.split(h));
				1 < f.length ? (q = f.pop(), r(a, f.join(h))) : f = null;
				m && (a.nodeValue = a.nodeValue.replace(b.truncationChar, ""), c.innerHTML = a.nodeValue + " " + m.innerHTML + b.truncationChar);
				if (f) {
					if (c.clientHeight <= d)if (0 <= k.length && "" != h)r(a, f.join(h) + h + q), f = null; else return c.innerHTML
				} else"" == h && (r(a, ""), a = l(c), k = b.splitOnChars.slice(0), h = k[0], q = f = null);
				if (b.animate)setTimeout(function () {
					p(a, d)
				}, !0 === b.animate ? 10 : b.animate); else return p(a, d)
			}
		}

		function r(a, c) {
			a.nodeValue = c + b.truncationChar
		}

		d = d || {};
		var n = window, b = {
			clamp: d.clamp || 2,
			useNativeClamp: "undefined" != typeof d.useNativeClamp ? d.useNativeClamp : !0,
			splitOnChars: d.splitOnChars || [".", "-", "\u2013", "\u2014", " "],
			animate: d.animate || !1,
			truncationChar: d.truncationChar || "\u2026",
			truncationHTML: d.truncationHTML
		}, e = c.style, y = c.innerHTML, z = "undefined" != typeof c.style.webkitLineClamp, g = b.clamp, v = g.indexOf && (-1 < g.indexOf("px") || -1 < g.indexOf("em")), m;
		b.truncationHTML && (m = document.createElement("span"), m.innerHTML = b.truncationHTML);
		var k = b.splitOnChars.slice(0),
			h = k[0], f, q;
		"auto" == g ? g = t() : v && (g = t(parseInt(g)));
		var w;
		z && b.useNativeClamp ? (e.overflow = "hidden", e.textOverflow = "ellipsis", e.webkitBoxOrient = "vertical", e.display = "-webkit-box", e.webkitLineClamp = g, v && (e.height = b.clamp + "px")) : (e = x(g), e <= c.clientHeight && (w = p(l(c), e)));
		return {original: y, clamped: w}
	}
})();