(function(pedagogeJquery){
	
	pedagogeJquery(window.jQuery, window, document);
	
})(function($, window, document){
	
	var PdgWorkshopMgmt = function(){
		
		var mainObj = this, settingsObj;
		
		mainObj.settings = function() {
			
			var workshopListDataTable = null,
			frm_workshop_trainer_info = $("#frm_workshop_trainer_info"),
			select_control_with_title_multiple = $(".select_control_with_title_multiple"),
			select_control_with_title = $(".select_control_with_title"),			
			tabPane = $(".tab-pane"),			
			li_workshop_details = $("#li_workshop_details"),
			tab_workshop_details = $("#tab_workshop_details"),
			//workshop trainer info related field
			cmd_add_workshop = $(".cmd_add_workshop"), 
			txt_trainer_name = $("#txt_trainer_name"),
			txt_trainer_contact_no = $("#txt_trainer_contact_no"),
			txt_trainer_email_address = $("#txt_trainer_email_address"),
			select_average_teaching_experience = $("#select_average_teaching_experience"),
			select_workshop_trainer_gender = $("#select_workshop_trainer_gender"),
			select_graduation = $("#select_graduation"),
			select_postgraduation = $("#select_postgraduation"),
			select_professional_qualification = $("#select_professional_qualification"),
			txt_about_trainer = $("#txt_about_trainer"),
			txt_present_place_of_work = $("#txt_present_place_of_work"),
			txt_past_place_of_work = $("#txt_past_place_of_work"),
			div_trainer_info_result_area = $("#div_trainer_info_result_area"),
			cmd_save_personal_info = $("#cmd_save_personal_info"),			
			
			div_workshops_list = $("#div_workshops_list"),
			cmd_reload_workshop_list = $("#cmd_reload_workshop_list"),
			
			//workshop details related fields
			frm_workshop_info = $("#frm_workshop_info"),
			txt_workshop_name = $("#txt_workshop_name"),			
			
			txt_workshop_start_date_dd = $("#txt_workshop_start_date_dd"),
			txt_workshop_start_date_mm = $("#txt_workshop_start_date_mm"),
			txt_workshop_start_date_yyyy = $("#txt_workshop_start_date_yyyy"),			
			
			txt_workshop_end_date_dd = $("#txt_workshop_end_date_dd"),
			txt_workshop_end_date_mm = $("#txt_workshop_end_date_mm"),
			txt_workshop_end_date_yyyy = $("#txt_workshop_end_date_yyyy"),
			
			select_workshop_category = $("#select_workshop_category"),
			txt_about_workshop = $("#txt_about_workshop"),
			txt_workshop_prerequisits = $("#txt_workshop_prerequisits"),
			txt_workshop_tags = $("#txt_workshop_tags"),
			txt_workshop_address = $("#txt_workshop_address"),
			select_workshop_city = $("#select_workshop_city"),
			txt_workshop_location_pincode = $("#txt_workshop_location_pincode"),
			select_workshop_locality = $("#select_workshop_locality"),
			txt_workshop_landmark = $("#txt_workshop_landmark"),
			txt_workshop_seats = $("#txt_workshop_seats"),
			select_workshop_type = $("#select_workshop_type"),
			span_paid_workshop = $(".span_paid_workshop"),
			txt_workshop_price = $("#txt_workshop_price"),
			chk_workshop_pay_online = $("#chk_workshop_pay_online"),
			chk_workshop_pay_at_venue = $("#chk_workshop_pay_at_venue"),
			chk_workshop_request_featured = $("#chk_workshop_request_featured"),
			cmd_upload_workshop_image = $("#cmd_upload_workshop_image"),
			fileinput_workshop_image = $("#fileinput_workshop_image"),			
			div_image_result_area = $("#div_image_result_area"),
			div_workshop_images_list = $("#div_workshop_images_list"),			
			hidden_workshop_id = $("#hidden_workshop_id"),
			cmd_update_workshop_details = $("#cmd_update_workshop_details"),
			div_panel_workshop_images = $("#div_panel_workshop_images"),
			
			hidden_dynamic_user_var = $("#hidden_dynamic_user_var"),
			hidden_dynamic_profile_edit_secret_key = $("#hidden_dynamic_profile_edit_secret_key"),
			
			img_workshop_display_pic = $("#img_workshop_display_pic"),
			fileinput_workshop_display_image = $("#fileinput_workshop_display_image"),
			cmd_upload_workshop_display_image = $("#cmd_upload_workshop_display_image"),
			hidden_blank_profile_url = $("#hidden_blank_profile_url").val();
			
			return {				
				frm_workshop_trainer_info : frm_workshop_trainer_info,
				workshopListDataTable : workshopListDataTable,
				select_control_with_title_multiple : select_control_with_title_multiple,
				select_control_with_title : select_control_with_title,			
				tabPane : tabPane,
				li_workshop_details : li_workshop_details,		
				tab_workshop_details : tab_workshop_details,
				
				div_workshops_list : div_workshops_list,
				cmd_reload_workshop_list : cmd_reload_workshop_list,
				
				//workshop trainer info related field
				cmd_add_workshop : cmd_add_workshop,
				txt_trainer_name : txt_trainer_name,
				txt_trainer_contact_no : txt_trainer_contact_no,
				txt_trainer_email_address : txt_trainer_email_address,
				select_average_teaching_experience : select_average_teaching_experience,
				select_workshop_trainer_gender : select_workshop_trainer_gender,
				select_graduation : select_graduation,
				select_postgraduation : select_postgraduation,
				select_professional_qualification : select_professional_qualification,
				txt_about_trainer : txt_about_trainer,
				txt_present_place_of_work : txt_present_place_of_work,
				txt_past_place_of_work : txt_past_place_of_work,
				div_trainer_info_result_area : div_trainer_info_result_area,
				cmd_save_personal_info : cmd_save_personal_info,
				
				//workshop details related fields
				frm_workshop_info : frm_workshop_info,
				txt_workshop_name : txt_workshop_name,
				
				txt_workshop_start_date_dd : txt_workshop_start_date_dd, 
				txt_workshop_start_date_mm : txt_workshop_start_date_mm,
				txt_workshop_start_date_yyyy : txt_workshop_start_date_yyyy,
				txt_workshop_end_date_dd : txt_workshop_end_date_dd,
				txt_workshop_end_date_mm : txt_workshop_end_date_mm,
				txt_workshop_end_date_yyyy : txt_workshop_end_date_yyyy,
				select_workshop_category : select_workshop_category,
				txt_about_workshop : txt_about_workshop,
				txt_workshop_prerequisits : txt_workshop_prerequisits,
				txt_workshop_tags : txt_workshop_tags,
				txt_workshop_address : txt_workshop_address,
				select_workshop_city : select_workshop_city,
				txt_workshop_location_pincode : txt_workshop_location_pincode,
				select_workshop_locality : select_workshop_locality,
				txt_workshop_landmark : txt_workshop_landmark,
				txt_workshop_seats : txt_workshop_seats,
				select_workshop_type : select_workshop_type,
				span_paid_workshop : span_paid_workshop,
				txt_workshop_price : txt_workshop_price,
				chk_workshop_pay_online : chk_workshop_pay_online,
				chk_workshop_pay_at_venue : chk_workshop_pay_at_venue,
				chk_workshop_request_featured : chk_workshop_request_featured,
				cmd_upload_workshop_image : cmd_upload_workshop_image,
				fileinput_workshop_image : fileinput_workshop_image,
				div_image_result_area : div_image_result_area,
				div_workshop_images_list : div_workshop_images_list,
				hidden_workshop_id : hidden_workshop_id,
				cmd_update_workshop_details : cmd_update_workshop_details,
				div_panel_workshop_images : div_panel_workshop_images,
				hidden_dynamic_user_var : hidden_dynamic_user_var,
				hidden_dynamic_profile_edit_secret_key : hidden_dynamic_profile_edit_secret_key,
				
				img_workshop_display_pic : img_workshop_display_pic,
				fileinput_workshop_display_image : fileinput_workshop_display_image,
				cmd_upload_workshop_display_image : cmd_upload_workshop_display_image,
				hidden_blank_profile_url : hidden_blank_profile_url,
			};
		};
		
		mainObj.pdg_ajax_handler = function( data, identifier, ajaxData ) {
			
			var dynamicData = {};
			
			dynamicData[identifier] = data;
			dynamicData['nonce'] = $ajaxnonce;
			dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
			dynamicData['pedagoge_callback_class'] = ajaxData['pedagoge_callback_class'];
			dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
			
			return $.ajax( {
				url: $ajax_url,
				type: "post",
				data: dynamicData,
			} );
		};		
		
		mainObj.init = function() {
			
			settingsObj = mainObj.settings();
						
			mainObj.workshopDataTable();
			
			mainObj.bindUIActions();
			
			mainObj.paidWorkshopAction();
			
			mainObj.getLocalitiesOfCity();
			
			mainObj.resetWorkshopForm();

		};
		
		mainObj.bindUIActions = function() {
			
			mainObj.processSelect2();
			
			settingsObj.cmd_save_personal_info.on('click',function(event){
				event.preventDefault();
				mainObj.saveTrainerInfo();
			});
			
			settingsObj.cmd_reload_workshop_list.on('click', function(){
				mainObj.loadWorkshopList();
			});
			
			settingsObj.cmd_add_workshop.on('click', function(){
				mainObj.resetWorkshopForm();
				mainObj.selectWorkshopDetailsTab();
			});
			
			settingsObj.select_workshop_type.on('change', function(){				
				var workshopType = $(this).val();
				mainObj.paidWorkshopAction({type : workshopType});
								
			});
			
			settingsObj.select_workshop_city.on('change', function(){
				var cityID = $(this).val();
				mainObj.getLocalitiesOfCity(cityID);
			});
			
			settingsObj.cmd_update_workshop_details.on('click', function(event){
				event.preventDefault();
				mainObj.saveWorkshop();
			});
			
			settingsObj.cmd_upload_workshop_image.on('click', function(event){
				event.preventDefault();
				if($.isNumeric(settingsObj.hidden_workshop_id.val())) {
					settingsObj.fileinput_workshop_image.click();	
				}
			});
			
			settingsObj.cmd_upload_workshop_display_image.on('click', function(event){
				event.preventDefault();
				if($.isNumeric(settingsObj.hidden_workshop_id.val())) {
					settingsObj.fileinput_workshop_display_image.click();	
				}
			});
			
			settingsObj.hidden_workshop_id.on('change', function(){
				var workshopID = $(this).val();
				mainObj.workshopGalleryToggle(workshopID);				
			});
			
			settingsObj.fileinput_workshop_image.on('change', function(){
				
				var workshopImageFileInput = $(this),
					workshopID = settingsObj.hidden_workshop_id.val(),
					workshopImageFormData = new FormData(),
					workshopImageUploadResultArea = settingsObj.div_image_result_area,
					imageFile;
				
				// do not upload any image if workshopID is not available
				if(!$.isNumeric(workshopID)) {
					var errorMsg = "Please load a workshop first to be able to upload workshop images.";
					workshopImageUploadResultArea.html('<div class="alert alert-danger">'+errorMsg+'</div>');
					$.notify(errorMsg, 'error');
					return false;
				}
				
				if(workshopImageFileInput.get(0).files.length > 0) {
					
					/**
					 * check if the file size is greater than 2 mb
					 * check if it is an image or not.
					 * Check for dimension
					 */
					imageFile = workshopImageFileInput.get(0).files[0];
					
					if(!mainObj.isImageFile(imageFile)) {
						workshopImageUploadResultArea.html('<div class="alert alert-danger">Only .jpg, and .png images are acceptable!</div>');
						return false;
					}
					
					var iSize = 0,
						fileSize = imageFile.size;
						
					if( fileSize>0 ) {
						fileSize = fileSize/1024;
						iSize = ( Math.round( fileSize * 100 ) / 100 );
					}
					
					if( iSize <= 0 || iSize > 3000 ) {						
						workshopImageUploadResultArea.html('<div class="alert alert-danger">Upload file should not exceed 3 MB in size.!</div>');
						return false;
					}
					
					//check for dimensions (This can be implemented in the future)
					/*var imgForUpload = new Image();

			        imgForUpload.src = window.URL.createObjectURL( imageFile );
			
			        imgForUpload.onload = function() {
						var width = imgForUpload.naturalWidth,
			                height = imgForUpload.naturalHeight;
			
			            window.URL.revokeObjectURL( imgForUpload.src );
						
			            if( (width >= 550 || width <= 660) && (height >= 350  && height <= 450)) {
			            }
			            else {
			                //fail
			                //Please upload photo of 409px * 600px
			                workshopImageUploadResultArea.html('<div class="alert alert-danger">Please upload photo of 400px(h) * 600px(w)!</div>');
							return false;
			            }
					};*/
								
					workshopImageFormData.append('workshop_image', imageFile);
					workshopImageFormData.append("workshop_id", workshopID);
					workshopImageFormData.append("nonce", $ajaxnonce);
					workshopImageFormData.append("action", $pedagoge_visitor_ajax_handler);
					workshopImageFormData.append("pedagoge_callback_function", 'upload_workshop_images_ajax');
					workshopImageFormData.append("pedagoge_callback_class", 'ControllerManageworkshop');
	
					workshopImageUploadResultArea.html('<div class="alert alert-info">Uploading image...</div>');
					settingsObj.cmd_upload_workshop_image.button('loading');
					
					$.ajax({
						url : $ajax_url,
					    data : workshopImageFormData,
					    processData : false, 
					    contentType : false,
					    type : 'POST',			    
					    success : function(response) {
					    	try {
							
								var parcedResponseData = $.parseJSON(response),			
									responseError = parcedResponseData.error, 
									responseErrorType = parcedResponseData.error_type,				 	
								 	responseMessage = parcedResponseData.message,
								 	responseData = parcedResponseData.data;
								 	
								if(responseError) {
									fn_alert_message(workshopImageUploadResultArea, responseMessage, 'error');
									//$.notify(responseMessage, 'error');
								} else {
									fn_alert_message(workshopImageUploadResultArea, responseMessage, 'success');
									$.notify(responseMessage, 'success');
									
									var imageURL = responseData.image_url;
									var imageFileName = responseData.image_file_name;
									
									mainObj.resetFormElement(workshopImageFileInput);									
									var imageCard = mainObj.createWorkshopImageCard(imageURL, imageFileName);
									settingsObj.div_workshop_images_list.append(imageCard);
								}
								
							} catch(err) {
								settingsObj.cmd_upload_workshop_image.button('reset');
								console.log('error occured '+err);
							}
					    },
					    complete:function(){
					    	settingsObj.cmd_upload_workshop_image.button('reset');
					    }
					});
				}
			});
			
			settingsObj.fileinput_workshop_display_image.on('change', function(){
				
				var workshopImageFileInput = $(this),
					workshopID = settingsObj.hidden_workshop_id.val(),
					workshopImageFormData = new FormData(),
					imageFile;
				
				// do not upload any image if workshopID is not available
				if(!$.isNumeric(workshopID)) {
					var errorMsg = "Please load a workshop first to be able to upload workshop images.";
					$.notify(errorMsg, 'error');
					return false;
				}
				
				if(workshopImageFileInput.get(0).files.length > 0) {
					imageFile = workshopImageFileInput.get(0).files[0];
					
					if(!mainObj.isImageFile(imageFile)) {
						var errorMsg = 'Only .jpg, and .png images are acceptable!';
						$.notify(errorMsg, 'error');
						return false;
					}
					
					var iSize = 0,
						fileSize = imageFile.size;
						
					if( fileSize>0 ) {
						fileSize = fileSize/1024;
						iSize = ( Math.round( fileSize * 100 ) / 100 );
					}
					
					if( iSize <= 0 || iSize > 3000 ) {
						var errorMsg = 'Upload file should not exceed 3 MB in size.!';
						$.notify(errorMsg, 'error');
						return false;
					}
							
					workshopImageFormData.append('workshop_image', imageFile);
					workshopImageFormData.append("workshop_id", workshopID);
					workshopImageFormData.append("nonce", $ajaxnonce);
					workshopImageFormData.append("action", $pedagoge_visitor_ajax_handler);
					workshopImageFormData.append("pedagoge_callback_function", 'upload_workshop_profile_image_ajax');
					workshopImageFormData.append("pedagoge_callback_class", 'ControllerManageworkshop');

					var errorMsg = 'Uploading image...';
					$.notify(errorMsg, 'info');
					settingsObj.cmd_upload_workshop_display_image.button('loading');
					
					$.ajax({
						url : $ajax_url,
					    data : workshopImageFormData,
					    processData : false, 
					    contentType : false,
					    type : 'POST',			    
					    success : function(response) {
					    	try {
							
								var parcedResponseData = $.parseJSON(response),			
									responseError = parcedResponseData.error, 
									responseErrorType = parcedResponseData.error_type,				 	
								 	responseMessage = parcedResponseData.message,
								 	responseData = parcedResponseData.data;
								 	
								if(responseError) {
									$.notify(responseMessage, 'error');
								} else {
									$.notify(responseMessage, 'success');									
									var imageURL = responseData.image_url;
																		
									mainObj.resetFormElement(workshopImageFileInput);
									settingsObj.img_workshop_display_pic.attr("src", imageURL);
								}
								
							} catch(err) {
								settingsObj.cmd_upload_workshop_display_image.button('reset');
								console.log('error occured '+err);
							}
					    },
					    complete:function(){
					    	settingsObj.cmd_upload_workshop_display_image.button('reset');
					    }
					});
				}
			});
			
			settingsObj.div_workshop_images_list.on('click', '.cmd_remove_workshop_image', function(event){
				event.preventDefault();
						
				var imageFileName = $(this).data('file_name'),
					imageContainer = $(this).closest('.col-sm-6');
						
				bootbox.dialog({
					message: "Are you sure, you want to delete this image?",
					title: "Image deletion confirmation...",
					buttons: {			
						danger: {
							label: "Confirm Delete!",
							className: "btn-danger",
							callback: function() {
								
								var $submit_data = {
									nonce : $ajaxnonce,
									action : $pedagoge_visitor_ajax_handler,
									pedagoge_callback_function : 'fn_delete_workshop_image_ajax',
									pedagoge_callback_class : $pedagoge_callback_class,							
									image_file_name : imageFileName
								};
								
								settingsObj.div_workshop_images_list.block({ 
					                message: '<h3 alert alert-warning>Deleting workshop image...</h3>',
					                css: { border: '3px solid #a00' }
					            }); 
								
								$.post($ajax_url, $submit_data, function(response) {
									try {
										var parsedResponseData = $.parseJSON(response);
									
										var responseError = parsedResponseData.error,									 	
										 	responseMessage = parsedResponseData.message,
										 	responseData = parsedResponseData.data;				 	
										
										if(responseError) {
											//bootbox.alert($message);
											$.notify(responseMessage, 'error');
										} else {
											imageContainer.remove();
										}
									} catch( err ) {
										var errMsg = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
										$.notify(errMsg, 'error');
									    
									} finally {
										settingsObj.div_workshop_images_list.unblock();
									}
							    }).complete(function() {
									settingsObj.div_workshop_images_list.unblock();
							    });
							}
						},
						main: {
							label: "Cancel",
							className: "btn-primary",
						}
					}
				});
			});
			
			settingsObj.div_workshops_list.on('click', '.cmd_edit_workshop', function(event){
				event.preventDefault();
				var rowData = settingsObj.workshopListDataTable.row( $(this).closest('tr')[0] ).data(),
					workshopData = {};
					
					workshopData.workshop_id = rowData[0];
					workshopData.workshop_name = rowData[1];
					workshopData.start_date = rowData[2];
					workshopData.end_date = rowData[3];
					workshopData.category_id = rowData[4];
					workshopData.workshop_address = rowData[6];
					workshopData.workshop_city = rowData[7];
					workshopData.workshop_locality = rowData[9];
					workshopData.workshop_location_landmark = rowData[11];
					workshopData.workshop_pincode = rowData[12];
					workshopData.about_workshop = rowData[13];
					workshopData.workshop_instructions = rowData[14];
					workshopData.workshop_type = rowData[15];
					workshopData.workshop_seats = rowData[16];
					workshopData.workshop_price = rowData[17];
					workshopData.pay_online = rowData[18];
					workshopData.pay_at_venue = rowData[19];
					workshopData.tags = rowData[20];
					workshopData.request_featured = rowData[23];
					mainObj.updateWorkshopForm(workshopData);
			});
			
			settingsObj.div_workshops_list.on('click', '.cmd_view_workshop', function(event){
				event.preventDefault();
				var workshopURL = $(this).prop('href');
    			window.open(workshopURL);
			});
			
			settingsObj.div_workshops_list.on('click', '.cmd_delete_workshop', function(event){
				event.preventDefault();
				var rowData = settingsObj.workshopListDataTable.row( $(this).closest('tr')[0] ).data(),
					workshopID = rowData[0];
				if(!$.isNumeric(workshopID) || workshopID <= 0 ) {
					return false;
				}
				bootbox.confirm("Are you sure, you want to delete this Workshop? You cannot recover this information.", function(result) {
		    		if(result) {
						var $submit_data = {
							nonce : $ajaxnonce,
							action : $pedagoge_visitor_ajax_handler,
							pedagoge_callback_function : 'delete_workshop_ajax',
							pedagoge_callback_class : 'ControllerManageworkshop',
							workshop_id : workshopID,
							hidden_dynamic_user_var : settingsObj.hidden_dynamic_user_var.val(),
							hidden_dynamic_profile_edit_secret_key : settingsObj.hidden_dynamic_profile_edit_secret_key.val(),
						};
						try {
							$.post($ajax_url, $submit_data, function(response) {
								
								var parsedResponseData = $.parseJSON(response);
				
								var responseError = parsedResponseData.error,									 	
								 	responseMessage = parsedResponseData.message,
								 	responseData = parsedResponseData.data;				 	
								
								if(responseError) {
									$.notify(responseMessage, 'error');
								} else {
									//reload workshop list
									mainObj.loadWorkshopList();
									$.notify(responseMessage, 'success');
								}
						    });
					    } catch( err ) {
							var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
							console.log($message);
						    $.notify('Error Occured while trying to delete the Institute.', 'error');
						}
		    		}    		
				});
			});
			
		}; //bindUIActions functions ends here
		
		mainObj.saveTrainerInfo = function() {
			
			var trainerInfoForm = settingsObj.frm_workshop_trainer_info;
				
			if(!trainerInfoForm.validationEngine('validate')) {
				return false;
			}			
			
			var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'save_workshop_trainer_info_ajax',
				pedagoge_callback_class : $pedagoge_callback_class,
				trainer_data : trainerInfoForm.serialize(),
				professional_qualifications : settingsObj.select_professional_qualification.val(),
				hidden_dynamic_user_var : settingsObj.hidden_dynamic_user_var.val(),
				hidden_dynamic_profile_edit_secret_key : settingsObj.hidden_dynamic_profile_edit_secret_key.val(),
				
			};
			
			settingsObj.cmd_save_personal_info.button('loading');
			
			$.post($ajax_url, $submit_data, function(response) {
				try {
					var parsedResponseData = $.parseJSON(response);
				
					var responseError = parsedResponseData.error,									 	
					 	responseMessage = parsedResponseData.message,
					 	responseData = parsedResponseData.data;				 	
					
					if(responseError) {
						$.notify(responseMessage, 'error');
					} else {
						$.notify(responseMessage, 'success');
					}
				} catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				    console.log($message);
				    settingsObj.cmd_save_personal_info.button('reset');
				}
		    }).complete(function(){
		    	settingsObj.cmd_save_personal_info.button('reset');
		    });
		};
		
		mainObj.createWorkshopImageCard = function(imageURL, imageFileName) {
			var imageCard = '<div class="col-sm-6 col-md-4">'+
									'<div class="thumbnail">'+
										'<img src="'+imageURL+'" class="img-responsive thumbbox" height="235px" width="314px" />'+
										'<br />'+
										'<div class="caption">'+
											'<p>'+
												'<button class="btn btn-danger col-xs-12 col-md-12 cmd_remove_workshop_image" data-file_name="'+imageFileName+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>'+
											'</p> <br/>'+
										'</div>'+
									'</div>'+
								'</div>';
			return imageCard;
		};
		
		mainObj.selectWorkshopDetailsTab = function() {			
			$(".nav-tabs li").removeClass('active');
			settingsObj.tabPane.removeClass('active');
			settingsObj.li_workshop_details.addClass('active');
			settingsObj.tab_workshop_details.addClass('active');
		};
		
		mainObj.processSelect2 = function() {
			
			settingsObj.select_control_with_title.each(function(index){
				var selectControl = $(this),				
				title = selectControl.prop('title');
						
				selectControl.select2({
					placeholder: title,
				});				
			});
			
			settingsObj.select_control_with_title_multiple.each(function(index){
				var selectControl = $(this), 
				title = selectControl.prop('title');
						
				selectControl.select2({
					placeholder: title,
					closeOnSelect: false
				});
			});			
		};
		
		mainObj.paidWorkshopAction = function(workshopTypeData) {
			/**
			 * { type : free/paid, price : 0, payOnline : yes/no, payAtVenue : yes/no } 
			 */
			var workshopType = '',
				workshopPrice = 0,
				payOnline = false,
				payAtVenue = false,
				workshopPriceComponent = settingsObj.span_paid_workshop;
				
			if(workshopTypeData) {
				if(workshopTypeData.hasOwnProperty('type')) {
					workshopType = workshopTypeData.type;
				}
				
				if(workshopTypeData.hasOwnProperty('price')) {				
					workshopPrice = workshopTypeData.price; 
				}
				
				if(workshopTypeData.hasOwnProperty('payOnline')) {				
					payOnline = workshopTypeData.payOnline === 'yes' ?  true : false;
				}
				
				if(workshopTypeData.hasOwnProperty('payAtVenue')) {				
					payAtVenue = workshopTypeData.payAtVenue === 'yes' ?  true : false;
				}
			}
						
			workshopType === 'paid' ? settingsObj.span_paid_workshop.show() : settingsObj.span_paid_workshop.hide();
			settingsObj.txt_workshop_price.val(workshopPrice);			
			settingsObj.chk_workshop_pay_at_venue.prop('checked', payAtVenue);
			settingsObj.chk_workshop_pay_online.prop('checked', payOnline);			
		};
		
		mainObj.getLocalitiesOfCity = function(cityID) {
			var cityLocalitites = '<option></option>';
			 
			if(!cityID) {
				cityID = '';
			} else {
				if($.isNumeric(cityID)) {
					cityLocalitites += $localities_data[cityID];
				}
			}
			settingsObj.select_workshop_locality.html(cityLocalitites).select2({		
				closeOnSelect: true,
				placeholder : "Please select a locality"
			});
		};
		
		mainObj.resetWorkshopForm = function() {
			document.getElementById('frm_workshop_info').reset();
			settingsObj.select_workshop_category.val('').trigger('change');
			settingsObj.select_workshop_city.val('').trigger('change');
			settingsObj.select_workshop_locality.val('').trigger('change');
			settingsObj.select_workshop_type.val('').trigger('change');
			settingsObj.chk_workshop_pay_at_venue.prop('checked', false);
			settingsObj.chk_workshop_pay_online.prop('checked', false);
			settingsObj.chk_workshop_request_featured.prop('checked', false);
			settingsObj.hidden_workshop_id.val('');
			settingsObj.div_workshop_images_list.html('');
			$.uniform.update();
			mainObj.workshopIDChangeTrigger();
		
			settingsObj.img_workshop_display_pic.attr("src", settingsObj.hidden_blank_profile_url);
		};
		
		mainObj.isImageFile = function($file) {
			if ($file.type) {
				return /^image\/\w+$/.test($file.type);
			} else {
				return /\.(jpg|jpeg|png)$/.test($file);
			}
		};
		
		mainObj.resetFormElement = function(e) {
			e.wrap('<form>').closest('form').get(0).reset();
		  	e.unwrap();
		};
		
		mainObj.workshopIDChangeTrigger = function(){
			settingsObj.hidden_workshop_id.trigger('change');
		};
		
		mainObj.workshopGalleryToggle = function(workshopID) {
			var disableButton = true,
				div_workshop_images_area = $(".div_workshop_images_area");
			if($.isNumeric(workshopID)) {				
				disableButton = false;
			}
			settingsObj.cmd_upload_workshop_image.prop('disabled', disableButton);
			
			blockingMsg = "<h4>Load a Workshop or Create a workshop first.</h4>";
			
			if(disableButton) {
				div_workshop_images_area.block({ 
					message : blockingMsg,
					css: { 
						border: '3px solid #a00',
						textAlign: 'center',
						width : '100%',
						marginTop : '45px' 
					} 
				});
			} else {
				div_workshop_images_area.unblock();
			}
		};
		
		mainObj.loadWorkshopGallery = function(workshopID) {
			if(!$.isNumeric(workshopID)) {
				return false;
			}
			var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_load_workshop_gallery_ajax',
				pedagoge_callback_class : $pedagoge_callback_class,
				workshop_id : workshopID
			};
			
			$.post($ajax_url, $submit_data, function(response) {
				try {
					var parsedResponseData = $.parseJSON(response);
				
					var responseError = parsedResponseData.error,									 	
					 	responseMessage = parsedResponseData.message,
					 	responseData = parsedResponseData.data;				 	
					
					if(responseError) {
						$.notify(responseMessage, 'error');
					} else {
						var galleryHtml = responseData.gallery;
						settingsObj.div_workshop_images_list.html(galleryHtml);
						var profileImage = responseData.profile;
						if(profileImage.length > 2) {
							settingsObj.img_workshop_display_pic.attr("src", profileImage);
						}
					}
				} catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				    
				}
		    });			
		};
		
		mainObj.saveWorkshop = function() {
			var workshopForm = settingsObj.frm_workshop_info;
			if(!workshopForm.validationEngine('validate')) {
				return false;
			}
			//cmd_update_workshop_details
			settingsObj.cmd_update_workshop_details.button('loading');
			var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'save_workshop_details_ajax',
				pedagoge_callback_class : $pedagoge_callback_class,
				workshop_data : workshopForm.serialize(),
				hidden_dynamic_user_var : settingsObj.hidden_dynamic_user_var.val(),
				hidden_dynamic_profile_edit_secret_key : settingsObj.hidden_dynamic_profile_edit_secret_key.val(),
			};
			
			$.post($ajax_url, $submit_data, function(response) {
				try {
					var parsedResponseData = $.parseJSON(response);
				
					var responseError = parsedResponseData.error,									 	
					 	responseMessage = parsedResponseData.message,
					 	responseData = parsedResponseData.data;				 	
					
					if(responseError) {
						$.notify(responseMessage, 'error');
					} else {					
						var savedWorkshopID = responseData.workshop_id;
						settingsObj.hidden_workshop_id.val(savedWorkshopID);
						mainObj.workshopIDChangeTrigger();
						//reload workshop list
						mainObj.loadWorkshopList();
						$.notify(responseMessage, 'success');
					}
				} catch( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
				    console.log($message);
				    settingsObj.cmd_update_workshop_details.button('reset');
				}
		    }).complete(function(){
		    	settingsObj.cmd_update_workshop_details.button('reset');
		    });	
			
		};
		
		mainObj.workshopDataTable = function() {
			settingsObj.workshopListDataTable = settingsObj.div_workshops_list.find("#table_workshops_list").DataTable( {
				"dom" : "<'row'<'col-sm-12'f>>" +
					"<'row'<'col-sm-12'tr>>" +
					"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		        "processing": true,
		        "lengthChange" : false,
		        "pagingType" : 'simple',
		        "columnDefs": [{
		                "targets": [ 4, 6, 7, 9, 11, 12, 13, 14, 18, 19, 20, 23],		                
		                "visible": false,                
		           },
		        ],
		    } );
		};
		
		mainObj.loadWorkshopList = function() {
			settingsObj.div_workshops_list.html('');
			settingsObj.workshopListDataTable =  null;
			
			//$.notify("Please wait! Updating workshops list!", 'info');
			
			settingsObj.cmd_reload_workshop_list.button('loading');
			
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'reload_workshops_ajax',
				pedagoge_callback_class : $pedagoge_callback_class,
				hidden_dynamic_user_var : settingsObj.hidden_dynamic_user_var.val(),
				hidden_dynamic_profile_edit_secret_key : settingsObj.hidden_dynamic_profile_edit_secret_key.val(),
			};
			
			$.post($ajax_url, $submit_data, function(response) {
				try {
					var parsedResponse = $.parseJSON(response);
				
					var responseError = parsedResponse.error,									 	
					 	responseMessage = parsedResponse.message,
					 	responseData = parsedResponse.data;				 	
					
					if(responseError) {
						$.notify(responseMessage, 'error');					
					} else {
						//$.notify(responseMessage, 'success');
						settingsObj.div_workshops_list.html(responseData.html);
						mainObj.workshopDataTable();
					}
				} catch( err ) {
					console.log('<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err);
				} finally {
					settingsObj.cmd_reload_workshop_list.button('reset');
				}
		    }).complete(function() {
		    	settingsObj.cmd_reload_workshop_list.button('reset');
		    });
		};
		
		mainObj.updateWorkshopForm = function(workshopData) {
			/**
			 * 1. Reset Form
			 * 2. Update Form
			 * 3. Reload workshop images
			 * 4. shift tab
			 * 5. Trigger related events
			 */
			mainObj.resetWorkshopForm();
			if(workshopData) {
				if($.isNumeric(workshopData.workshop_id)) {
					
					if(!$.isNumeric(workshopData.workshop_price)) {
						workshopData.workshop_price = 0;
					}
					var workshopPayOnline = workshopData.pay_online == 'yes' ? true :  false,
						workshopPayAtVenue = workshopData.pay_at_venue == 'yes' ? true :  false,
						requestFeatured = workshopData.request_featured == 'yes' ? true :  false;
						
					//fix date issue
					if(workshopData.start_date.length == 10) {
						var startDateArr = workshopData.start_date.split('-');
						settingsObj.txt_workshop_start_date_dd.val(startDateArr[2]);
						settingsObj.txt_workshop_start_date_mm.val(startDateArr[1]);
						settingsObj.txt_workshop_start_date_yyyy.val(startDateArr[0]);
					}
					if(workshopData.end_date.length == 10) {
						var endDateArr = workshopData.end_date.split('-');
						settingsObj.txt_workshop_end_date_dd.val(endDateArr[2]);
						settingsObj.txt_workshop_end_date_mm.val(endDateArr[1]);
						settingsObj.txt_workshop_end_date_yyyy.val(endDateArr[0]);
					}
					
					settingsObj.txt_workshop_name.val(workshopData.workshop_name);					
					settingsObj.select_workshop_category.val(workshopData.category_id).change();
					settingsObj.txt_about_workshop.val(workshopData.about_workshop);
					settingsObj.txt_workshop_prerequisits.val(workshopData.workshop_instructions);
					settingsObj.txt_workshop_tags.val(workshopData.tags);
					settingsObj.txt_workshop_address.val(workshopData.workshop_address);
					settingsObj.select_workshop_city.val(workshopData.workshop_city).change();
					settingsObj.txt_workshop_location_pincode.val(workshopData.workshop_pincode);
					settingsObj.select_workshop_locality.val(workshopData.workshop_locality).change();
					settingsObj.txt_workshop_landmark.val(workshopData.workshop_location_landmark);
					settingsObj.txt_workshop_seats.val(workshopData.workshop_seats);
					settingsObj.select_workshop_type.val(workshopData.workshop_type).change();
					settingsObj.txt_workshop_price.val(workshopData.workshop_price);					
					settingsObj.chk_workshop_pay_online.prop('checked', workshopPayOnline);
					settingsObj.chk_workshop_pay_at_venue.prop('checked', workshopPayAtVenue);
					settingsObj.chk_workshop_request_featured.prop('checked', requestFeatured);
					settingsObj.hidden_workshop_id.val(workshopData.workshop_id);
					
					$.uniform.update();
					
					//load workshop images
					mainObj.loadWorkshopGallery(workshopData.workshop_id);
					mainObj.workshopIDChangeTrigger();
					mainObj.selectWorkshopDetailsTab();
				}
			}
			
		};
		
	}, workshopObj;
	workshopObj = new PdgWorkshopMgmt;
	workshopObj.init();
});

function keepLB (str) { 
  var reg=new RegExp("(%0A)", "g");
  return str.replace(reg,"%0D$1");
}
