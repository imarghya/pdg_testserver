$(document).ready(function(){
   //for show date picker-------------------------------------------
   //query date--
   $(document).on('focus', '.new_query_start_date', function(){
     $('.new_query_start_date').datepicker({ dateFormat: 'yy-mm-dd',minDate: 0 });
     $(this).prop('readonly', true); 
   });
   
   $(document).on('focusout', '.new_query_start_date', function(){
     //$('.new_query_start_date').datepicker({ dateFormat: 'yy-mm-dd',minDate: 0 });
     $(this).prop('readonly', false); 
   });

   $(document).on('focus', '#edit_query_date', function(){
     $('#edit_query_date').datepicker({ dateFormat: 'yy-mm-dd',minDate: 0 }); 
   });
   
  //Get Standard Year on change edit subject-----------------------
 $(document).on('change', '#edit_subject', function(){
 var subject_id=$(this).val();
 //alert(subject_id);
 var q_from_id='edit_standard';
 get_subject_type(subject_id,q_from_id);
 });
 //----------------------------------------------------------------
 
 //Function for new query------------------------------------------
 $("#subject_frm_button").click(function(){
    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_subject_from',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        subjects: $("#mySingleField").val(),
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             $("#new_query_modal_item").html(response);
             $("#subjectSelect1").modal("show");
             $('.mlc').fSelect();
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------    
 });
 //for submit each from--------
 //$(document).on('click', '.qdata_submit', function(e){
 //e.preventDefault();
 //var x=$(this).attr("idd");
 //var subject=$(this).attr("subject");
 
$(document).on("change",".sub_locality",function(){
  var loc_id = $(this).attr('id');
  $("#hid_"+loc_id).val( $(this).val());
 });
$(document).on("change",".prefer_location",function(){
var prefer_id = $(this).attr('data-tt');
if($(this).is(':checked')){
$("#prefer_location_"+prefer_id).val( $(this).val());
}
else
{
$("#prefer_location_"+prefer_id).val('');
}
});

$(document).on('submit','.subject_form',function(e){
//$('.new_query_start_date').prop('readonly',false);
e.preventDefault();

totalx=$(this).attr("totalx");
var x=$(this).attr("idd");

//$("#start_date_"+x).prop('readonly',false);
//$("#start_date_"+x).prop('required',true);

var subject=$(this).attr("subject");
var age=$("#age_"+x).val();
var school=$("#school_"+x).val();
var school_type = $("#school_"+x+" option:selected").attr("data-type");
var board=$("#board_"+x).val();
var board_type = $("#board_"+x+" option:selected").attr("data-type");
var standard=$("#standard_"+x).val();
 
 if($("#myhome_"+x).prop("checked") == true){
 var myhome=1;
 }
 else{
   var myhome=0     
 }
 
if($("#tutorhome_"+x).prop("checked") == true){ 
 var tutorhome=1;
}
else{
var tutorhome=0;
}

if($("#institute_"+x).prop("checked") == true){ 
 var institute=1;
}
else{
 var institute=0;       
}
 var localities=$("#localities_"+x).val();
 var start_date=$("#start_date_"+x).val();
 var requirement=$("#requirement_"+x).val();
 var query_id = $(this).find("#query_id").val();
 //console.log(query_id);
 //alert(localities);
 //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'submit_stu_subject_data',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        x : x,
        subject : subject,
        age : age,
        school : school,
        school_type : school_type,
        board : board,
        board_type : board_type,
        standard : standard,
        myhome : myhome,
        tutorhome : tutorhome,
        institute : institute,
        localities : localities,
        start_date : start_date,
        requirement : requirement,
        query_id : query_id
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             alertify.success("Submited...");
             var xx=(parseInt(x) +1);
             if(response != "")
             {
              $("#frm_submit_new"+x+" #query_id").val(response);
             }
              $("#subjectSelect"+x).modal("hide");
              $("#subjectSelect"+xx).modal("show");
                if (totalx==x) {
                 $("#subjectSelect"+x).modal("hide");
                 $("#nameaddress").modal("show");
                }
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------
       
  });
 //});
 //---------------------------------------------------------------
 
 //For Same as Preview--Data
 $(document).on('click', '.same_as_prv', function(e){
 var xx =$(this).attr("idd");
 var x=(parseInt(xx) +1);
 //alert(xx);
 if($(this).prop("checked") == true){
        //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_q_subject_data',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        x : xx,
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
            var str=response.split("|");
             //alert(str);
                $("#age_"+x).val(str[0]);
                $("#school_"+x).val(str[1]);
                $("#board_"+x).val(str[2]);
                $("#standard_"+x).val(str[3]);
                if ($.trim(str[4])==1) {
                 $('#myhome_'+x).prop('checked', true);
                 $("#prefer_location_"+x).val($.trim(str[4]));
                }
                if ($.trim(str[5])==1) {
                 $('#tutorhome_'+x).prop('checked', true);
                 $("#prefer_location_"+x).val($.trim(str[5]));
                }
                
                if ($.trim(str[6])==1) {
                 $('#institute_'+x).prop('checked', true);
                 $("#prefer_location_"+x).val($.trim(str[6]));
                }
                
                //var loc=str[7].split(",");
                //$("#localities_"+x).val(loc);
                
              /********************** Location Select ***********************/
              var loc=str[7].split(",");
              //console.log(loc);
              //$("#localities_"+x).val(loc);
                var selectedvalues = [];
              $.each(str[7].split(","), function(i,e){
                 $("#localities_"+x+" option[value='" + e + "']").prop("selected", true);

                 selectedvalues.push($("#localities_"+x+" option[value='"+e+"']").text());
              });
              //console.log(selectedvalues);
              var selectedvalue = selectedvalues.join(",");
              $("#hid_localities_"+x).val(selectedvalue);
              //$("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("Select Localities");
              //$("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("");
              $("#localities_"+x).parent('.fs-wrap').find(".fs-option").each(function(i,e) {
                $.each(str[7].split(","), function(i1,e){
                if($("#localities_"+x).parent('.fs-wrap').find(".fs-option").eq(i).attr("data-value") == e)
                {
                  $("#localities_"+x).parent('.fs-wrap').find(".fs-option").eq(i).addClass("selected");
                }
                });
              });

              $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("");
              //console.log(selectedvalue);
              $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text(selectedvalue);
                /********************** Location Select ***********************/
                
                $("#start_date_"+x).val(str[8]);
                $("#requirement_"+x).val(str[9]);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
        //--------------------------------------------------------
    }
    //else if($(this).prop("checked") == false){
      else{
                $("#age_"+x).val('');
                $("#school_"+x).val('');
                $("#board_"+x).val('');
                $("#standard_"+x).val('');
                $('#myhome_'+x).prop('checked', false);
                $('#tutorhome_'+x).prop('checked', false);
                $('#institute_'+x).prop('checked', false);
                //$("#localities_"+x).val('');
                $("#start_date_"+x).val('');
                $("#requirement_"+x).val('');
                
                $("#localities_"+x+" option:selected").removeAttr("selected");
                $("#localities_"+x).parent('.fs-wrap').find(".fs-option").removeClass("selected");
                $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("");
                $("#localities_"+x).parent('.fs-wrap').find(".fs-label").text("Select Localities");

                $("#prefer_location_"+x).val('');
                $("#hid_localities_"+x).val('');
    }
 });
 //--------------------------------------------------------------
 //For Submit New Query------------------------------------------
 $(document).on('submit','#frmnameemail',function(e){
 //$(document).on('click', '#submit_all_new_query', function(e){
  e.preventDefault();
  var x=$("#x").val();
  var name=$("#studname").val();
  var email=$("#emailid").val();
  var phone=$("#phonenum").val();
  var query_ids = '';
  for(var inc = 1; inc <= x; inc++){
    if(inc == 1)
    {
      query_ids += $("#frm_submit_new"+inc+" #query_id").val();
    }
    else
    {
     query_ids += ','+$("#frm_submit_new"+inc+" #query_id").val(); 
    }
    
  }
  //alert(x);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'update_student_data',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        x : x,
        name : name,
        email : email,
        phone : phone,
        query_ids : query_ids
        };
        
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
             //alert(response);
             $("#nameaddress").modal("hide");
             alertify.success("Success: Query Added Successfully.");
            // get_filter_queries('','','','','','',0);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
   //--------------------------------------------------------
 });


//Change Mobile Number OTP Verification

$(document).on("click","#stu_verify_phone",function(){
  var phone = $("#stu_phone_number").val();

  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_otp_code',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        phone_no : phone,
        };
        
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
           //alert(response);
           //$("#nameaddress").modal("hide");
           alertify.success("An OTP is sent.");
          // get_filter_queries('','','','','','',0);
        })
        .fail(function(jqxhr, status, error){ 
            //alert(error);
        });
});

$(document).on("click","#stu_check_otp",function(){
  var otp_code = $("#otp_code").val();

  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'check_otp_code',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        otp_code : otp_code,
        };
        
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
           //alert(response);
           //$("#nameaddress").modal("hide");
           if(response == "success"){
            $("#frmnameemail #phonenum").val($("#stu_phone_number").val());
            $("#stu_phone_verify_modal").modal("hide");
           }
           else{
            alertify.error("OTP code mismatch");
           }
           //alertify.success("SMS has been sent.");
          // get_filter_queries('','','','','','',0);
        })
        .fail(function(jqxhr, status, error){ 
            //alert(error);
        });
});


//Change Email Address Verification

$(document).on("click","#stu_verify_email",function(){
  var email = $("#stu_email").val();

  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'send_email_code',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        email : email,
        };
        
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
           //alert(response);
           //$("#nameaddress").modal("hide");
           alertify.success("Email has been sent.");
          // get_filter_queries('','','','','','',0);
        })
        .fail(function(jqxhr, status, error){ 
            //alert(error);
        });
});

$(document).on("click","#stu_check_email",function(){
  var email_code = $("#email_code").val();

  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'check_email_code',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        email_code : email_code,
        };
        
        $.post($ajax_url, submit_data)
          .done(function(response, status, jqxhr){
           //alert(response);
           //$("#nameaddress").modal("hide");
           if(response == "success"){
            $("#frmnameemail #emailid").val($("#stu_email").val());
            $("#stu_email_verify_modal").modal("hide");
           }
           else{
            alertify.error("Email code mismatch");
           }
           //alertify.success("SMS has been sent.");
          // get_filter_queries('','','','','','',0);
        })
        .fail(function(jqxhr, status, error){ 
            //alert(error);
        });
});


 //----------------------------------------------------------------
 rec_filter_data=[];
 //Query Recomendation Filter function-----------------------------
 $(document).on('change','#rec_check1',function(e){ //For Teacher
  if($(this).prop("checked") == true){
    rec_filter_data.push(1);
    $("#chk_li1").show();
    filterFunction();
  }
  else{
   $("#chk_li1").hide();
   rec_filter_data = rec_filter_data.filter(function(item) { 
    return item !== 1
   });
   filterFunction();
  }
  $("#rec_filter_data").val(rec_filter_data);
  get_rec_filter_data($("#rec_filter_data").val());
 });
 
 $(document).on('change','#rec_check2',function(e){ //For Institute
  if($(this).prop("checked") == true){
    rec_filter_data.push(2);
     $("#chk_li2").show();
     filterFunction();
  }
  else{
   rec_filter_data = rec_filter_data.filter(function(item) { 
    return item !== 2
   });
    $("#chk_li2").hide();
    filterFunction();
  }
  $("#rec_filter_data").val(rec_filter_data);
  get_rec_filter_data($("#rec_filter_data").val());
 });
 
 $(document).on('change','#rec_check3',function(e){ //For Invite
  if($(this).prop("checked") == true){
    rec_filter_data.push(3);
     $("#chk_li3").show();
     filterFunction();
  }
  else{
   rec_filter_data = rec_filter_data.filter(function(item) { 
    return item !== 3
   });
    $("#chk_li3").hide();
    filterFunction();
  }
  $("#rec_filter_data").val(rec_filter_data);
  get_rec_filter_data($("#rec_filter_data").val());
 });
 
 //-----------------------------
 $(document).on('click','#chk_li1 span i',function(e){
   $("#chk_li1").hide();
   $("#rec_check1").trigger("click");
   filterFunction();
 });
 $(document).on('click','#chk_li2 span i',function(e){
   $("#chk_li2").hide();
   $("#rec_check2").trigger("click");
   filterFunction();
 });
 $(document).on('click','#chk_li3 span i',function(e){
   $("#chk_li3").hide();
   $("#rec_check3").trigger("click");
   filterFunction();
 });
 //-----------------------------
 
 //------------------------------------------------------------------------
 //Get Student Recomended list---------------------------------------------
 $(document).on('click','#recomended_tab',function(e){
  var query_id=$("#active_query_id").val();
  get_student_query_details(query_id);
  //$.LoadingOverlay("hide");
 });
 //Get Student Shortlist---------------------------------------------------
 $(document).on('click','#short_list_tab',function(e){
  var query_id=$("#active_query_id").val();
  $("#rec_filter_data").val('');
  $.LoadingOverlay("show");
  //alert(query_id);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_student_shortlist',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                $("#tab1default").html('');
                $("#tab2default").html('');
                $("#tab2default").html(response);
                $.LoadingOverlay("hide");
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
      //-------------------------------------------------------- 
 });
 //----------------------------------------------------------------
 
 //Get Student Interest In You List---------------------------------------------------
 $(document).on('click','#interest_in_tab',function(e){
  var query_id=$("#active_query_id").val();
  $("#rec_filter_data").val('');
  $.LoadingOverlay("show");
  //alert(query_id);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_student_interestinyou',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               // alert(response);
                $("#tab1default").html('');
                $("#tab2default").html('');
                $("#tab3default").html(response);
                $.LoadingOverlay("hide");
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
      //-------------------------------------------------------- 
 });
 //----------------------------------------------------------------
 //Get Student Matched List---------------------------------------------------
 $(document).on('click','#matched_tab',function(e){
  var query_id=$("#active_query_id").val();
  $("#rec_filter_data").val('');
  //alert(query_id);
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_student_matched',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               // alert(response);
                $("#tab1default").html('');
                $("#tab2default").html('');
                $("#tab3default").html('');
                $("#tab4default").html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
      //-------------------------------------------------------- 
 });
 //----------------------------------------------------------------
  //Query Edit function---------------------------------------------
 //get query details---------------
 $(document).on('click','#query_edit',function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
    //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_query_status_byid',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //if ($.trim(response)!='CLOSED') {
                 //Get query details ajax---------------
                       var submit_data2 = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'get_edit_query_details',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id: query_id,
                        };
                         
                          $.post($ajax_url, submit_data2)
                            .done(function(query_response, status, jqxhr){
                               // alert(query_response);
                              $("#edit_query_data").html(query_response);
                              $("#edit_query_modal").modal("show");  
                            })
                            .fail(function(jqxhr, status, error){ 
                                // this is the ""error"" callback
                            });
                 //-------------------------------------
                 
                //}
                //else{
                // alertify.log("Alert: Query is not active!");
                //}
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
            //-------------------------------------------
      });
       //----------------------------------------------------------------
             //Submit Edit Query-------------------------
            $(document).on('submit', '#edit_query_form', function(event){
                event.preventDefault();
                var query_id=$("#active_query_id").val();
                if($("#edit_my_home").prop("checked") == true){
                var myhome=1;
                }
                else{
                  var myhome=0     
                }
                
               if($("#edit_t_home").prop("checked") == true){ 
                var tutorhome=1;
               }
               else{
               var tutorhome=0;
               }
               
               if($("#edit_institute").prop("checked") == true){ 
                var institute=1;
               }
               else{
                var institute=0;       
               }
                //call ajax-----------------------------------------------
                        var submit_data = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'submit_edit_query',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id : query_id,
                        clone_student_id: $("#edit_student_id").val(),
                        clone_subject : $("#edit_subject").val(),
                        clone_school : $("#edit_school").val(),
                        clone_school_type : $("#edit_school option:selected").attr("data-type"),
                        clone_board : $("#edit_board").val(),
                        clone_board_type : $("#edit_board option:selected").attr("data-type"),
                        clone_standard : $("#edit_standard").val(),
                        age : $("#edit_age").val(),
                        clone_my_home : myhome,
                        clone_t_home : tutorhome,
                        clone_institute : institute,
                        clone_locality : $("#edit_locality").val(),
                        clone_query_date : $("#edit_query_date").val(),
                        clone_requirement : $("#edit_requirement").val(),
                        clone_student_name : $("#edit_student_name").val(),
                        clone_phone_number : $("#edit_phone_number").val(),
                        clone_user_email : $("#edit_user_email").val(),
                        };
                        
                          $.post($ajax_url, submit_data)
                            .done(function(response, status, jqxhr){
                                $("#edit_query_modal").modal("hide");
                                get_student_query_details(query_id);
                                alertify.success("Success: Query Edit Success");
                            })
                            .fail(function(jqxhr, status, error){ 
                               //alert(error);
                            });
                //--------------------------------------------------------
            });
            //-------------------------------------------
  //Get Standard Year on change edit subject-----------------------
 $(document).on('change', '#edit_subject', function(){
 var subject_id=$(this).val();
 //alert(subject_id);
 var q_from_id='edit_standard';
 get_subject_type(subject_id,q_from_id);
 });
 //----------------------------------------------------------------
 //Create Archive Query---------------------------------------------------
 $(document).on('click', '#archive', function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
   var result = confirm("Are you sure you want to archive?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_archive',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //get_student_pagination();
                alertify.success("Success: Query archive success.");
                location.reload();
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 
 //Create Shortlist Query---------------------------------------------------
 $(document).on('click', '.short-list', function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
   var tiid=$(this).attr("ti-id");
  // alert(tiid);
   var result = confirm("Are you sure you want to shortlist?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'create_query_shortlist',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                get_student_query_details(query_id);
                alertify.success("Success: Teacher Shortlisted Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 //Create Shortlist Query---------------------------------------------------
 $(document).on('click', '.remove-shortlist', function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
   var tiid=$(this).attr("ti-id");
  // alert(tiid);
   var result = confirm("Are you sure you want to remove from shortlist?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'remove_query_shortlist',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //get_student_query_details(query_id);
                $("#short_list_tab")[0].click();
                var ss=+$("#short-list-count").text();
                $("#short-list-count").text(ss-1);
                alertify.success("Success: Query successfully removed from shortlisted.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 
  //Create Invite Query---------------------------------------------------
$(document).on('click', '.invite', function(e){
  e.preventDefault();
  var selected_tab = $(this).closest('.tab-content').find('.active').attr('id');
  if($(".nav-tabs > li.active").find('a').attr('href') == "#"+selected_tab){
    var selected_tab_id = $(".nav-tabs > li.active").find('a').attr('id');
  }

  var query_id=$("#active_query_id").val();
  var tiid=$(this).attr("ti-id");
  var type=$(this).attr("type");
  // alert(tiid);
   var result = confirm("Are you sure you want to invite?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'create_query_invite',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        type : type,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //get_student_pagination();
                //get_student_query_details(query_id);
                $("#"+selected_tab_id)[0].click();
                alertify.success("Success: Teacher invited Successfully.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
  //Un Invite Query---------------------------------------------------
 $(document).on('click', '.uninvite', function(e){
  e.preventDefault();
  var selected_tab = $(this).closest('.tab-content').find('.active').attr('id');
  if($(".nav-tabs > li.active").find('a').attr('href') == "#"+selected_tab){
    var selected_tab_id = $(".nav-tabs > li.active").find('a').attr('id');
  }
  var query_id=$("#active_query_id").val();
  var tiid=$(this).attr("ti-id");
  var type=$(this).attr("type");
  // alert(tiid);
   var result = confirm("Are you sure you want to uninvite?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'create_query_uninvite',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        type : type,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //get_student_query_details(query_id);
                $("#"+selected_tab_id)[0].click();
                alertify.success("Success: Query successfully invited.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 
  //Student Ignore Interest---------------------------------------------------
 $(document).on('click', '.stu_ignore', function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
   var tiid=$(this).attr("ti-id");
   var type=$(this).attr("type");
  // alert(tiid);
   var result = confirm("Are you sure you want to ignore?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'stu_ignore_interest',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        type : type,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               // get_student_query_details(query_id);
                 $("#interest_in_tab")[0].click();
                 var ss=+$("#int-list-count").text();
                 $("#int-list-count").text(ss-1);
                alertify.success("Success: Query successfully ignored.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 
   //Student Get Intro---------------------------------------------------
 $(document).on('click', '.stu_get_intro', function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
   var tiid=$(this).attr("ti-id");
   var type=$(this).attr("type");
  // alert(tiid);
   var result = confirm("Thank you for showing interest. Our Representative will call you within 72 hours and get you connected with the Teacher/Institution.");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'stu_get_intro',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        type : type,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //get_student_query_details(query_id);
                $("#interest_in_tab")[0].click();
                alertify.success("Success: Get intro submitted.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 
 //Student Unmatch---------------------------------------------------
 $(document).on('click', '.unmatch', function(e){
   e.preventDefault();
   var query_id=$("#active_query_id").val();
   var tiid=$(this).attr("ti-id");
   var type=$(this).attr("type");
  // alert(tiid);
   var result = confirm("Are you sure?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'stu_unmatch',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        tiid : tiid,
        type : type,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
              $("#matched_tab")[0].click();
                //get_student_query_details(query_id);
                alertify.success("Success: Successfully Unmatched.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 //Get Pagination on click page link-------------------------------
 $(document).on('click', '.page-link', function(e){
   e.preventDefault();
   var query_id=$(this).attr("data-id");
   $(this).parent().addClass('active').siblings().removeClass('active');
   get_student_query_details(query_id);
 });
 //----------------------------------------------------------------
 get_student_pagination();
 
 //------------------------For Past Query Page--------------------------------------
 //Expand Collapse Function--------
  $(document).on('click', '.expand', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   //$("#q_show_"+query_id).hide(1000);
   //$("#q_hide_"+query_id).show(1000);
   $("#q_show_"+query_id).hide();
   $("#q_hide_"+query_id).fadeIn(1000);
  });
  
  $(document).on('click', '.collapse', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   //$("#q_show_"+query_id).show(1000);
   //$("#q_hide_"+query_id).hide(1000);
   
   $("#q_show_"+query_id).fadeIn(1000);
   $("#q_hide_"+query_id).hide();
  });
  
   //Create Archive Past Query---------------------------------------------------
 $(document).on('click', '.archive', function(e){
   e.preventDefault();
  var query_id=$(this).attr("q_id");
   var result = confirm("Are you sure you want to archive?");
   if (result) {
        //call ajax-----------------------------------------------
         var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'query_archive',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id: query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                $("#qspan_id_"+query_id).remove();
                alertify.success("Success: Query archive success.");
            })
            .fail(function(jqxhr, status, error){ 
                //alert(error);
            });
         //--------------------------------------------------------
      }
  });
 //----------------------------------------------------------------
 //Edit Past query-------------------------------------------------
  //get query details---------------
 $(document).on('click','.edit',function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
    //Get query details ajax---------------
                       var submit_data2 = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'get_edit_past_query_details',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id: query_id,
                        };
                         
                          $.post($ajax_url, submit_data2)
                            .done(function(query_response, status, jqxhr){
                               // alert(query_response);
                              $("#edit_Past_query_data").html(query_response);
                              $("#edit_past_query_modal").modal("show");  
                            })
                            .fail(function(jqxhr, status, error){ 
                                // this is the ""error"" callback
                            });
                 //------------------------------------- 
                
      });
   //Submit Edit Query-------------------------
   $(document).on('submit', '#edit_past_query_form', function(event){
       event.preventDefault();
       var query_id=$("#query_id").val();
       if($("#edit_my_home").prop("checked") == true){
       var myhome=1;
       }
       else{
         var myhome=0     
       }
       
      if($("#edit_t_home").prop("checked") == true){ 
       var tutorhome=1;
      }
      else{
      var tutorhome=0;
      }
      
      if($("#edit_institute").prop("checked") == true){ 
       var institute=1;
      }
      else{
       var institute=0;       
      }
       //call ajax-----------------------------------------------
               var submit_data = {
               nonce: $ajaxnonce,
               action: $pedagoge_visitor_ajax_handler,
               pedagoge_callback_function: 'submit_edit_query',
               pedagoge_callback_class: 'ControllerMemberdashboard',
               query_id : query_id,
               clone_student_id: $("#edit_student_id").val(),
               clone_subject : $("#edit_subject").val(),
               clone_school : $("#edit_school").val(),
               clone_school_type : $("#edit_school option:selected").attr("data-type"),
               clone_board : $("#edit_board").val(),
               clone_board_type : $("#edit_board option:selected").attr("data-type"),
               clone_standard : $("#edit_standard").val(),
               age : $("#edit_age").val(),
               clone_my_home : myhome,
               clone_t_home : tutorhome,
               clone_institute : institute,
               clone_locality : $("#edit_locality").val(),
               clone_query_date : $("#edit_query_date").val(),
               clone_requirement : $("#edit_requirement").val(),
               clone_student_name : $("#edit_student_name").val(),
               clone_phone_number : $("#edit_phone_number").val(),
               clone_user_email : $("#edit_user_email").val(),
               };
               
                 $.post($ajax_url, submit_data)
                   .done(function(response, status, jqxhr){
                       $("#edit_past_query_modal").modal("hide");
                       //$("#qspan_id_"+query_id).remove();
                       alertify.success("Success: Query Edit Success");
                   })
                   .fail(function(jqxhr, status, error){ 
                      //alert(error);
                   });
       //--------------------------------------------------------
   });
 //----------------------------------------------------------------
 //---------------------------------------------------------------------------------
 //Archived Query-------------------------------------------------------------------
   //Create Archive Past Query---------------------------------------------------
 $(document).on('click', '.uarchive', function(e){
   e.preventDefault();
   var query_id=$(this).attr("q_id");
   $("#temp_q_id").val(query_id);
   $("#deleteoractivate").modal("show");
  });
 //----------------------------------------------------------------
 //Delete Query---------------------------------------------------
  $(document).on('click','#deletearchive',function(e){
      e.preventDefault();
      var result = confirm("Are you sure you want to delete?");
      if (result) {
     //call ajax-----------------------------------------------
     var query_id=$("#temp_q_id").val();
                 //Get query details ajax---------------
                       var submit_data2 = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'delete_query',
                        pedagoge_callback_class: 'ControllerMemberdashboard',
                        query_id: query_id,
                        };
                         
                          $.post($ajax_url, submit_data2)
                            .done(function(query_response, status, jqxhr){
                               $("#deleteoractivate").modal("hide");
                               $("#qspan_id_"+query_id).remove();
                               alertify.success("Success: Query Delete Success");
                              
                            })
                            .fail(function(jqxhr, status, error){ 
                                // this is the ""error"" callback
                            });
                 //-------------------------------------
      }
   });
   //---------------------------------------------------------------
 //get details----------------------------------------------------
 
   $(document).on('click','#editactivate',function(e){
     //call ajax-----------------------------------------------
     var query_id=$("#temp_q_id").val();
                 //Get query details ajax---------------
                       var submit_data2 = {
                        nonce: $ajaxnonce,
                        action: $pedagoge_visitor_ajax_handler,
                        pedagoge_callback_function: 'get_edit_past_query_details',
                        pedagoge_callback_class: 'ControllerQueries',
                        query_id: query_id,
                        };
                         
                          $.post($ajax_url, submit_data2)
                            .done(function(query_response, status, jqxhr){
                               // alert(query_response);
                              $("#edit_Past_query_data").html(query_response);
                              $("#edit_past_query_modal").modal("show");  
                            })
                            .fail(function(jqxhr, status, error){ 
                                // this is the ""error"" callback
                            });
                 //------------------------------------- 
   });
   //---------------------------------------------------------------

//End Document Ready function-------------------------------------------------------
});

//For get rsult for rec list on change filter data-----------------
function get_rec_filter_data(data) {
  var myarray=data.split(",");
  
  if($.inArray('1', myarray) != -1) { //teacher
    $("#rec_teacher").show();
    $("#rec_teacher .author_box").each(function(){
        if($(this).attr('data-usertype') == 'teacher') {
            $(this).show();
        }
    });
   } else {
      //$("#rec_teacher").hide();
      $("#rec_teacher .author_box").each(function(){
        if($(this).attr('data-usertype') == 'teacher') {
            $(this).hide();
        }
      });
   }
   
   if($.inArray('2', myarray) != -1) { //institute
    $("#rec_institute").show();
    $("#rec_teacher .author_box").each(function(){
        if($(this).attr('data-usertype') == 'institution') {
            $(this).show();
        }
    });
   } else {
      //$("#rec_institute").hide();
      $("#rec_teacher .author_box").each(function(){
        if($(this).attr('data-usertype') == 'institution') {
            $(this).hide();
        }
      });
   }
   
   if($.inArray('3', myarray) != -1) { //invite
    $("#rec_invite").show();
   } else {
      $("#rec_invite").hide();
   }
   
   if (data=='' || data==0) {
      $("#rec_teacher .author_box").each(function(){
            $(this).show();
      });
      //$("#rec_teacher").show();
      //$("#rec_institute").show();
      $("#rec_invite").show();
   }
   
}
//------------------------------------------------------------------
//For get Pagination--------------------------------------------------
function get_student_pagination() {
      //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_student_pagination',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
                //alert(response);
                $("#pagination").html('');
                $("#pagination").html(response);
                var first_query_id=$("#first_query_id").val();
                get_student_query_details(first_query_id);
                
                //for pagination slider-----------
                $(".center").slick({
                  infinite: true,
                  draggable : false,
                  centerMode: false,
                  slidesToShow: 5,
                  slidesToScroll: 1
                });
                //--------------------------------
                
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
      //-------------------------------------------------------- 
}
//------------------------------------------------------------------
//Get Student Query Details-----------------------------------------
function get_student_query_details(query_id) {
   $.LoadingOverlay("show");
   $("#active_query_id").val(query_id);
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_student_query_details',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id : query_id,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
            //alert(response);
            $("#student_query_details").html('');
            $("#student_query_details").html(response);
            $.LoadingOverlay("hide");
        })
        .fail(function(jqxhr, status, error){ 
           //alert(error);
        });
   //--------------------------------------------------------
}
//------------------------------------------------------------------

/******************** LOAD MORE RECOMMEND PROFILE *************************/

$(document).on("click",".loadmore_recommend",function(){
  $.LoadingOverlay("show");
  var page = $(this).attr('data-page');
  var query_id = $("#active_query_id").val();
  //alert($("#active_query_id").val());
  var resultperpage = 30;
  var total_rows = $("#rec_teacher").attr('data-all');

  if(((parseInt(page)) * resultperpage) > total_rows)
  {
    $(this).hide();
  }

  $(this).attr('data-page',parseInt(page)+1);
  var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        //pedagoge_callback_function: 'get_student_query_details',
        pedagoge_callback_function: 'get_loadmore_recommended_profile',
        pedagoge_callback_class: 'ControllerMemberdashboard',
        query_id : query_id,
        page : page,
        };
   
      $.post($ajax_url, submit_data)
        .done(function(response, status, jqxhr){
          
            //console.log(response.length);
            $.LoadingOverlay("hide");
            //$("#student_query_details").html('');
            //$("#student_query_details").html(response);
            $("#rec_teacher").append(response);
            
        })
        .fail(function(jqxhr, status, error){ 
           //alert(error);
        });
});

/******************** END OF LOAD MORE RECOMMEND PROFILE *************************/


//Get Subject Type/Standrad----------------------------------------
function get_subject_type(subject_id,q_from_id) {
  //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_standard_list',
        pedagoge_callback_class: 'ControllerQueries',
        subject_id : subject_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               // alert(response);
                $("#"+q_from_id).html(response);
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------   
}
//-----------------------------------------------------------------