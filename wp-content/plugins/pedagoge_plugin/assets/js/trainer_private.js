/**
 * General Coding
 */
$(document).ready(function () {
	$("#select_other_technical_domain,#select_other_nontechnical_domain").select2({
		tags: true,
		maximumSelectionLength: 3,
		tokenSeparators: [','],
		minimumInputLength: 2,
		maximumInputLength: 40
	});
	$("#select_keywords").select2({
		tags: true,
		maximumSelectionLength: 6,
		tokenSeparators: [','],
		minimumInputLength: 2,
		maximumInputLength: 20
	});
	$("#select_companies_served").select2({
		tags: true,
		maximumSelectionLength: 6,
		tokenSeparators: [','],
		minimumInputLength: 2,
		maximumInputLength: 40
	});
	$("#select_user_video").select2({
		tags: true,
		maximumSelectionLength: 3
	});
	$("#select_professional_qualification").select2({
		tags: false,
		maximumSelectionLength: 6
	});

	//Do not submit Forms by default
	$("#frm_trainer_intro").submit(function (e) {
		return false;
	}).validationEngine({scroll: false});

	$(".non_clickable").click(function (event) {
		event.preventDefault();
	});

	$(".character_count").keyup(function () {
		$(".div_character_count").html('');
		var $current_count = $(this).val().length;
		if ($current_count <= 0) {
			return;
		}
		var $max_length = $(this).prop('maxlength');

		if (!$.isNumeric($max_length)) {
			$max_length = 0;
		}
		var $remaining_characters = $max_length - $current_count;
		var $message = '<div class="alert alert-info">' + $remaining_characters + ' characters remained...</div>';
		$(this).parent().find('.div_character_count').html($message);
	}).blur(function () {
		$(".div_character_count").html('');
	});

	fn_process_select_with_title();

	$(".insti_tabs").click(function (event) {
		event.preventDefault();
	});

	var $cmd_show_class_details = $(".cmd_show_class_details");

	$cmd_show_class_details.click(function (event) {
		event.preventDefault();
		fn_save_profile_introduction();
	});

	var $file_upload_profile_picture = $('#file_upload_profile_picture'),
		$file_upload_resume = $('#file_upload_resume'),
		$file_upload_sample_course_material = $('#file_upload_sample_course_material'),
		$data_to_save_file = {},
		$hidden_trainer_id, $hidden_user_id,
		$hidden_user_email, $user_personal_info_id;

	$hidden_trainer_id = $("#hidden_dynamic_insti_var").val();
	$hidden_user_id = $("#hidden_dynamic_user_var").val();
	$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val();
	$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();

	$data_to_save_file.hidden_dynamic_insti_var = $hidden_trainer_id;
	$data_to_save_file.hidden_dynamic_user_var = $hidden_user_id;
	$data_to_save_file.hidden_dynamic_profile_edit_secret_key = $hidden_user_email;
	$data_to_save_file.hidden_dynamic_user_info_var = $user_personal_info_id;

	var $fileUploadFields = $('input[type="file"]');
	var $save_result_area = $("#div_trainer_save_result_area"),
		$message = '',
		$save_button = $cmd_show_class_details,
		$save_loader = $(".img_trainer_save_loader");
	$save_result_area.html('');
	var $upload_data = new FormData();

	/*Profile picture starts*/
	$file_upload_profile_picture.click(function () {
		$file_upload_profile_picture.val("");
	}).change(function () {
		var $file_upload_profile_picture_file = null;
		$file_upload_profile_picture_file = $('#file_upload_profile_picture')[0].files[0];
		$upload_data.append('trainer_intro_data', JSON.stringify($data_to_save_file));
		$upload_data.append('file_upload_profile_picture', $file_upload_profile_picture_file);
		$save_button.button('loading');
		$save_loader.show();
		$fileUploadFields.prop('disabled', true);
		pdg_show_alerts($save_result_area, "Processed profile picture.. Please wait..", 'info');
		$upload_data.append("nonce", $ajaxnonce);
		$upload_data.append("action", $pedagoge_visitor_ajax_handler);
		$upload_data.append("pedagoge_callback_function", 'fn_save_trainer_profile_picture_ajax');
		$upload_data.append("pedagoge_callback_class", 'ControllerPrivatetrainer');
		$.ajax({
			url: $ajax_url,
			data: $upload_data,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (response) {
				try {
					var $response_data = $.parseJSON(response);
					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message,
						$data = $response_data.data;
					if ($error) {
						pdg_show_alerts($save_result_area, $message, 'error');
					} else {
						var $file_url = $response_data.file_url;
						$('.trainer_profile_pic_block img').attr("src", $file_url + "?version=" + Math.random().toString(36).substring(7));
						pdg_show_alerts($save_result_area, $message, 'success');
					}
				} catch (err) {
					$save_button.button('reset');
				}
			},
			complete: function () {
				$save_button.button('reset');

				$save_loader.hide();
				$fileUploadFields.prop('disabled', false);

			}
		});
	});
	/*Profile picture ends*/

	/*Resume starts*/
	$file_upload_resume.click(function () {
		$file_upload_resume.val("");
	}).change(function () {
		var $file_upload_resume_file = null;
		$file_upload_resume_file = $('#file_upload_resume')[0].files[0];
		$upload_data.append('trainer_intro_data', JSON.stringify($data_to_save_file));
		$upload_data.append('file_upload_resume', $file_upload_resume_file);
		$save_button.button('loading');
		$save_loader.show();
		$fileUploadFields.prop('disabled', true);
		pdg_show_alerts($save_result_area, "Processed resume.. Please wait..", 'info');
		$upload_data.append("nonce", $ajaxnonce);
		$upload_data.append("action", $pedagoge_visitor_ajax_handler);
		$upload_data.append("pedagoge_callback_function", 'fn_save_trainer_resume_ajax');
		$upload_data.append("pedagoge_callback_class", 'ControllerPrivatetrainer');
		$.ajax({
			url: $ajax_url,
			data: $upload_data,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (response) {
				try {
					var $response_data = $.parseJSON(response);
					var $error = $response_data.error,
						$url = $response_data.url,
						$error_type = $response_data.error_type,
						$message = $response_data.message,
						$data = $response_data.data;
					if ($error) {
						pdg_show_alerts($save_result_area, $message, 'error');
					} else {
						var $file_url = $response_data.file_url;
						$('.trainer_resume_block a').attr("href", $file_url);
						pdg_show_alerts($save_result_area, $message, 'success');
					}
				} catch (err) {
					$save_button.button('reset');
				}
			},
			complete: function () {
				$save_button.button('reset');

				$save_loader.hide();
				$fileUploadFields.prop('disabled', false);
			}
		});
	});
	/*Resume ends*/


	/*Sample course material starts*/
	$file_upload_sample_course_material.click(function () {
		$file_upload_sample_course_material.val("");
	}).change(function () {
		var $file_upload_sample_course_material_file = null;
		$file_upload_sample_course_material_file = $('#file_upload_sample_course_material')[0].files[0];
		$upload_data.append('trainer_intro_data', JSON.stringify($data_to_save_file));
		$upload_data.append('file_upload_sample_course_material', $file_upload_sample_course_material_file);
		$save_button.button('loading');
		$save_loader.show();
		$fileUploadFields.prop('disabled', true);
		pdg_show_alerts($save_result_area, "Processed sample course material.. Please wait..", 'info');
		$upload_data.append("nonce", $ajaxnonce);
		$upload_data.append("action", $pedagoge_visitor_ajax_handler);
		$upload_data.append("pedagoge_callback_function", 'fn_save_trainer_sample_course_material_ajax');
		$upload_data.append("pedagoge_callback_class", 'ControllerPrivatetrainer');
		$.ajax({
			url: $ajax_url,
			data: $upload_data,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (response) {
				try {
					var $response_data = $.parseJSON(response);
					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message,
						$data = $response_data.data;
					if ($error) {
						pdg_show_alerts($save_result_area, $message, 'error');
					} else {
						var $file_url = $response_data.file_url;
						$('.trainer_sample_course_material_block a').attr("href", $file_url);
						pdg_show_alerts($save_result_area, $message, 'success');
					}
				} catch (err) {
					$save_button.button('reset');
				}
			},
			complete: function () {
				$save_button.button('reset');

				$save_loader.hide();
				$fileUploadFields.prop('disabled', false);
			}
		});
	});
	/*Sample course material ends*/
});


/**
 *   /////////////////////////////////////////////////Profile Introduction coding///////////////////////////////////////////////
 */

function fn_save_profile_introduction() {
	var $save_result_area = $("#div_trainer_save_result_area"),
		$message = '',
		$save_button = $('.cmd_show_class_details'),
		$save_loader = $(".img_trainer_save_loader");

	$save_result_area.html('');

	var $txt_trainer_first_name = $("#txt_trainer_first_name").val(),
		$txt_trainer_last_name = $("#txt_trainer_last_name").val(),
		$txt_mobile_no = $("#txt_mobile_no").val(),
		$txt_alternate_contact_no = $("#txt_alternate_contact_no").val(),
		$txt_email_id = $("#txt_email_id").val(),
		$txt_address_of_correspondance = $("#txt_address_of_correspondance").val(),
		$dob_year = $("#dob_year").val(),
		$dob_month = $("#dob_month").val(),
		$dob_day = $("#dob_day").val(),
		$select_gender = $("#select_gender").val(),
		$select_graduation = $('#select_graduation').val(),
		$select_postgraduation = $('#select_postgraduation').val(),
		$select_professional_qualification = $('#select_professional_qualification').val(),
		$txt_other_qualification = $('#txt_other_qualification').val(),
		$select_companies_served = $("#select_companies_served").val(),
		$txt_heading_of_profile = $("#txt_heading_of_profile").val(),
		$select_user_video = $("#select_user_video").val(),
		$select_technical_domain = $("#select_technical_domain").val(),
		$select_other_technical_domain = $("#select_other_technical_domain").val(),
		$select_nontechnical_domain = $("#select_nontechnical_domain").val(),
		$select_other_nontechnical_domain = $("#select_other_nontechnical_domain").val(),
		$select_average_training_experience = $("#select_average_training_experience").val(),
		$select_training_frequency = $("#select_training_frequency").val(),
		$select_training_level = $("#select_training_level").val(),
		$select_keywords = $("#select_keywords").val(),
		$txt_training_experiences_of_profile = $("#txt_training_experiences_of_profile").val(),
		$trainer_references_ClientReference_1_contact_no = $("#trainer_references_ClientReference_1_contact_no").val(),
		$trainer_references_ClientReference_1_contact_name = $("#trainer_references_ClientReference_1_contact_name").val(),
		$trainer_references_ClientReference_2_contact_no = $("#trainer_references_ClientReference_2_contact_no").val(),
		$trainer_references_ClientReference_2_contact_name = $("#trainer_references_ClientReference_2_contact_name").val(),
		$trainer_references_PeerColleagueReference_1_contact_no = $("#trainer_references_PeerColleagueReference_1_contact_no").val(),
		$trainer_references_PeerColleagueReference_1_contact_name = $("#trainer_references_PeerColleagueReference_1_contact_name").val(),
		$trainer_references_PeerColleagueReference_2_contact_no = $("#trainer_references_PeerColleagueReference_2_contact_no").val(),
		$trainer_references_PeerColleagueReference_2_contact_name = $("#trainer_references_PeerColleagueReference_2_contact_name").val(),
		$trainer_references_PersonalReference_1_contact_no = $("#trainer_references_PersonalReference_1_contact_no").val(),
		$trainer_references_PersonalReference_1_contact_name = $("#trainer_references_PersonalReference_1_contact_name").val(),
		$trainer_references_PersonalReference_2_contact_no = $("#trainer_references_PersonalReference_2_contact_no").val(),
		$trainer_references_PersonalReference_2_contact_name = $("#trainer_references_PersonalReference_2_contact_name").val(),

		$hidden_trainer_id = $("#hidden_dynamic_insti_var").val(),
		$hidden_user_id = $("#hidden_dynamic_user_var").val(),
		$hidden_user_email = $("#hidden_dynamic_profile_edit_secret_key").val(),
		$user_personal_info_id = $("#hidden_dynamic_user_info_var").val();

	if (!$("#frm_trainer_intro").validationEngine('validate')) {
		return;
	}

	if (
		!$.isNumeric($hidden_trainer_id) ||
		$hidden_trainer_id <= 0 || !$.isNumeric($hidden_user_id) ||
		$hidden_user_id <= 0 ||
		$hidden_user_email.length <= 0 || !$.isNumeric($user_personal_info_id) ||
		$user_personal_info_id <= 0
	) {
		$message = 'Profile Information is invalid. Please reload the page and try again.';
		pdg_show_alerts($save_result_area, $message, 'error');
		return;
	}

	var $date_of_birth = $dob_year + '-' + $dob_month + '-' + $dob_day;
	var $data_to_save = {};
	$data_to_save.hidden_dynamic_insti_var = $hidden_trainer_id;
	$data_to_save.hidden_dynamic_user_var = $hidden_user_id;
	$data_to_save.hidden_dynamic_profile_edit_secret_key = $hidden_user_email;
	$data_to_save.hidden_dynamic_user_info_var = $user_personal_info_id;

	$data_to_save.txt_trainer_first_name = $txt_trainer_first_name;
	$data_to_save.txt_trainer_last_name = $txt_trainer_last_name;
	$data_to_save.txt_email_id = $txt_email_id;
	$data_to_save.txt_mobile_no = $txt_mobile_no;
	$data_to_save.txt_alternate_contact_no = $txt_alternate_contact_no;
	$data_to_save.txt_address_of_correspondance = $txt_address_of_correspondance;
	$data_to_save.select_gender = $select_gender;
	$data_to_save.date_of_birth = $date_of_birth;
	$data_to_save.select_graduation = $select_graduation;
	$data_to_save.select_postgraduation = $select_postgraduation;
	$data_to_save.select_professional_qualification = $select_professional_qualification;
	$data_to_save.txt_other_qualification = $txt_other_qualification;
	$data_to_save.select_companies_served = $select_companies_served;
	$data_to_save.txt_heading_of_profile = $txt_heading_of_profile;
	$data_to_save.select_user_video = $select_user_video;
	$data_to_save.select_technical_domain = $select_technical_domain;
	$data_to_save.select_other_technical_domain = $select_other_technical_domain;
	$data_to_save.select_nontechnical_domain = $select_nontechnical_domain;
	$data_to_save.select_other_nontechnical_domain = $select_other_nontechnical_domain;
	$data_to_save.select_average_training_experience = $select_average_training_experience;
	$data_to_save.select_training_frequency = $select_training_frequency;
	$data_to_save.select_training_level = $select_training_level;
	$data_to_save.select_keywords = $select_keywords;
	$data_to_save.txt_training_experiences_of_profile = $txt_training_experiences_of_profile;
	$data_to_save.trainer_references_ClientReference_1_contact_no = $trainer_references_ClientReference_1_contact_no;
	$data_to_save.trainer_references_ClientReference_1_contact_name = $trainer_references_ClientReference_1_contact_name;
	$data_to_save.trainer_references_ClientReference_2_contact_no = $trainer_references_ClientReference_2_contact_no;
	$data_to_save.trainer_references_ClientReference_2_contact_name = $trainer_references_ClientReference_2_contact_name;
	$data_to_save.trainer_references_PeerColleagueReference_1_contact_no = $trainer_references_PeerColleagueReference_1_contact_no;
	$data_to_save.trainer_references_PeerColleagueReference_1_contact_name = $trainer_references_PeerColleagueReference_1_contact_name;
	$data_to_save.trainer_references_PeerColleagueReference_2_contact_no = $trainer_references_PeerColleagueReference_2_contact_no;
	$data_to_save.trainer_references_PeerColleagueReference_2_contact_name = $trainer_references_PeerColleagueReference_2_contact_name;
	$data_to_save.trainer_references_PersonalReference_1_contact_no = $trainer_references_PersonalReference_1_contact_no;
	$data_to_save.trainer_references_PersonalReference_1_contact_name = $trainer_references_PersonalReference_1_contact_name;
	$data_to_save.trainer_references_PersonalReference_2_contact_no = $trainer_references_PersonalReference_2_contact_no;
	$data_to_save.trainer_references_PersonalReference_2_contact_name = $trainer_references_PersonalReference_2_contact_name;

	var $upload_data = new FormData();
	$upload_data.append('trainer_intro_data', JSON.stringify($data_to_save));
	$save_button.button('loading');
	$save_loader.show();
	pdg_show_alerts($save_result_area, "Your request is getting processed.. Please wait..", 'info');
	$upload_data.append("nonce", $ajaxnonce);
	$upload_data.append("action", $pedagoge_visitor_ajax_handler);
	$upload_data.append("pedagoge_callback_function", 'fn_save_trainer_intro_ajax');
	$upload_data.append("pedagoge_callback_class", 'ControllerPrivatetrainer');
	$.ajax({
		url: $ajax_url,
		data: $upload_data,
		processData: false,
		contentType: false,
		type: 'POST',
		success: function (response) {
			try {
				var $response_data = $.parseJSON(response);
				var $error = $response_data.error,
					$error_type = $response_data.error_type,
					$message = $response_data.message,
					$data = $response_data.data;
				if ($error) {
					pdg_show_alerts($save_result_area, $message, 'error');
				} else {
					pdg_show_alerts($save_result_area, $message, 'success');
				}
				$('html, body').stop().animate({
					scrollTop: $($save_result_area).offset().top - 100
				}, 500);
			} catch (err) {
				$save_button.button('reset');
			}
		},
		complete: function () {
			$save_button.button('reset');

			$save_loader.hide();

		}
	});
}

function fn_process_select_with_title() {
	$(".select_control_with_title").each(function (index) {
		var $title = $(this).prop('title');
		$(this).select2({
			placeholder: $title
		});
	});

	$(".select_control_with_title_multiple").each(function (index) {
		var $title = $(this).prop('title');
		$(this).select2({
			placeholder: $title,
			closeOnSelect: false
		});
	});
}
