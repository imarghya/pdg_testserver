$(document).ready(function(){
	$('#modal_teacher_institute_documents_manager').on('hidden.bs.modal', function (e) {
		$("#div_teacher_institute_documents_list").html('<h3>Click Reload List button to load the documents list</h3>');
    });
	$("#cmd_teacher_institute_documents_manager").click(function(){
		var $teacher_institute_user_id = $(this).data('teacher_institute_userid');
		$("#hidden_input_teacher_institute_user_id").val($teacher_institute_user_id);
		$('#modal_teacher_institute_documents_manager').modal('show');
	});
	
	$('#modal_teacher_institute_documents_manager').on('shown.bs.modal', function () {
		fn_load_teacher_institute_documents_list();
	});
	
	$("#cmd_reset_document_selection").click(function(event){
    	event.preventDefault();
		$("#txt_teacher_institute_document_file_name, #txt_teacher_institute_document_title").val('');
		resetFormElement($("#fileinput_teacher_institute_document"));
    });
    
    $("#cmd_select_teacher_institute_document").click(function(event){
    	$("#fileinput_teacher_institute_document").click();
    });
    
    $("#fileinput_teacher_institute_document").change(function(event) {    	
    	//event.preventDefault();
    	var $teacher_institute_uploaded_files = event.target.files;
    	var $file_length = $teacher_institute_uploaded_files.length;
		
		$("#div_class_image_result_area").html('');
		$("#span_select_image").html('Select image');
		$("#txt_teacher_class_image_file_name").val('');
		
    	var $str_files_status = '',
    	$file_name = '',
    	$file_size = 0,
    	$file_extension = '',    	
    	$is_image_error = false;
    	
    	if($teacher_institute_uploaded_files.length > 0) {
    		$file_name = $teacher_institute_uploaded_files[0].name;
	    	$file_size = $teacher_institute_uploaded_files[0].size;
	    	$file_extension = $file_name.substring($file_name.lastIndexOf('.') + 1).toLowerCase();
	    	
	    	var $str_size_kb_mb = 'KB',
	    	iSize = 0;
	    	
	    	if($file_size > 0) {
				$file_size = $file_size/1024;
				iSize = (Math.round($file_size * 100) / 100);
				
				if( iSize > 5100 ) {
					$is_image_error = true;
					$str_files_status += 'Error! Document size should not be more than 5 MB.<br/>';
				}
			}
			
			if($file_extension === 'pdf' || $file_extension === 'doc' || $file_extension === 'docx' || $file_extension === 'ppt' || $file_extension === 'pptx' || $file_extension === 'xls' || $file_extension === 'xlsx') {
				
			} else {
				$is_image_error = true;
				$str_files_status += 'Error! Only Document Files having any of the following extensions (.pdf, .doc, .docx, .ppt, .pptx, .xls, .xlsx ) would be uploaded!';
			}
    	}
    	
    	if(!$is_image_error) {    		
    		$("#txt_teacher_institute_document_file_name, #txt_teacher_institute_document_title").val($file_name);
    	} else {
    		$.notify($str_files_status);
    	}
    });
    
    $("#cmd_upload_teacher_institute_document").click(function(){
    	var $teacher_institute_user_id = $("#hidden_input_teacher_institute_user_id").val();
    	var $file_title = $("#txt_teacher_institute_document_file_name").val();
    	var $file_name = $("#txt_teacher_institute_document_title").val();
    	var $file_input = $("#fileinput_teacher_institute_document");
    	var $button = $(this);
    	
    	if(!$.isNumeric($teacher_institute_user_id) || $teacher_institute_user_id <= 0) {
			$message = "User Information is not available! Cannot load the documents list!";
			$.notify($message, 'error');
			return;
		}
		
		if($file_name.length<=0) {
			$message = "Please select a file first for upload";
			$.notify($message, 'error');
			return;
		}
		
		if($file_title.length<=0) {
			$message = "Please input file title to describe the file you are uploading!";
			$.notify($message, 'error');
			return;
		}
		
		if($file_input.get(0).files.length === 0) {
			$message = "Please select a file first for upload";
			$.notify($message, 'error');
		} else {
			
			var $upload_data = new FormData();
			$upload_data.append('teacher_institute_document', $file_input.get(0).files[0]);
			$upload_data.append("nonce", $ajaxnonce);
			$upload_data.append("action", $pedagoge_visitor_ajax_handler);
			$upload_data.append("pedagoge_callback_function", 'fn_save_teacher_institute_document_ajax');
			$upload_data.append("pedagoge_callback_class", 'ControllerPrivateteacher');
			$upload_data.append("teacher_institute_user_id", $teacher_institute_user_id);
			$upload_data.append("file_title", $file_title);
			$upload_data.append("file_name", $file_name);
			
			$.notify("Uploading document!", 'info');
			$button.button('loading');
			
			$.ajax({
				url : $ajax_url,
			    data : $upload_data,
			    processData : false, 
			    contentType : false,
			    type : 'POST',			    
			    success : function(response) {
			    	try {
					
						var $response_data = $.parseJSON(response);				
						var $error = $response_data.error, 
							$error_type = $response_data.error_type,				 	
						 	$message = $response_data.message,
						 	$data = $response_data.data;
						if($error) {
							$.notify($message, 'error');
						} else {
							$("#txt_teacher_institute_document_file_name, #txt_teacher_institute_document_title").val('');
							resetFormElement($file_input);
							$.notify($message, 'success');
							fn_load_teacher_institute_documents_list();
						}
						
					} catch(err) {
						$button.button('reset');
						console.log('error occured '+err);
					}
			    },
			    complete:function(){
			    	$button.button('reset');
			    }
			});
		}    	
    });
    
    $("#cmd_reload_teacher_institute_documents_list").click(function(){
    	fn_load_teacher_institute_documents_list();
    });
	
});

function fn_load_teacher_institute_documents_list() {
	var $button = $("#cmd_reload_teacher_institute_documents_list");
	var $document_list_area = $("#div_teacher_institute_documents_list");
	var $teacher_institute_user_id = $("#hidden_input_teacher_institute_user_id").val();
	var $message = '';
	
	$document_list_area.html('<h3>List Loading...</h3>');
	
	if(!$.isNumeric($teacher_institute_user_id) || $teacher_institute_user_id <= 0) {
		$message = "User Information is not available! Cannot load the documents list!";
		fn_alert_message($document_list_area, $message, 'error');
		return;
	}
	$document_list_area.block({ 
        message: '<h3 alert alert-warning>Refreshing documents list...</h3>',
        css: { border: '3px solid #a00' }
    }); 
	$button.button('loading');
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_get_teacher_institute_files_ajax',
		pedagoge_callback_class : 'ControllerPrivateteacher',
		teacher_institute_user_id : $teacher_institute_user_id
	};
	
	$.post($ajax_url, $submit_data, function(response) {
		try {
			var $response_data = $.parseJSON(response);
		
			var $error = $response_data.error,									 	
			 	$message = $response_data.message,
			 	$data = $response_data.data;
			
			if($error) {
				fn_alert_message($("#div_teacher_institute_documents_list"), $message, 'error');
			} else {
				$("#div_teacher_institute_documents_list").html($data);
			}
			$document_list_area.unblock();
		} catch( err ) {
			var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> '+err;
		    console.log($message);
		    $document_list_area.unblock();
		} finally {
			$button.button('reset');
			$document_list_area.unblock();
		}
		
    }).complete(function() {
    	$button.button('reset');
    });
}
