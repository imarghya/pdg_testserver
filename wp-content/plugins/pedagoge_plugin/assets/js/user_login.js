///////////////////////////////////////Login Form Processing Starts////////////////////////////////////////////////
/**
 *Login Form Processing
 */
$( document ).ready( function () {
	
	$( "#login_form" ).validationEngine( {
		scroll: false,
		autoHidePrompt: true,
		autoHideDelay: 3500,
		fadeDuration: 0.3,
		promptPosition: "topLeft:0"
	} );
	
	$( '#chk_login_show_password' ).change( function () {
		if ( $( this ).is( ":checked" ) ) {
			$( "#txt_login_password" ).prop( 'type', 'text' );
		} else {
			$( "#txt_login_password" ).prop( 'type', 'password' );
		}
	} );
	
	$( "#txt_login_email_address" ).onEnter( function ( event ) {
		var $user_name = $( this ).val();
		
		if ( $user_name.length > 0 ) {
			$( "#txt_login_password" ).select().focus();
			return false;
		}
		
	} );
	
	$( "#txt_login_password" ).onEnter( function ( event ) {
		var $txt_user_name = $( "#txt_login_email_address" ),
			$password = $( this ).val();
		
		if ( $txt_user_name.val().length <= 0 ) {
			$txt_user_name.select().focus();
			return false;
		}
		
		if ( $password.length > 0 ) {
			$( "#cmd_login" ).click();
		}
	} );
	
	$( "#cmd_login" ).click( function ( event ) {
		event.preventDefault(); //incase button becomes submit form.
		
		var $button = $( this ),
			$txt_username = $( "#txt_login_email_address" ),
			$txt_password = $( "#txt_login_password" ),
			$remember_me = $( "#chk_login_remember_me" ).prop( 'checked' );
		var $nexturl = $( '#nexturl' ).val();
		
		if ( $( "#login_form" ).validationEngine( 'validate', {
				scroll: false,
				autoHidePrompt: true,
				autoHideDelay: 3500,
				fadeDuration: 0.3,
				promptPosition: "topLeft:0"
			} ) ) {
			
			$button.button( 'loading' );
			//hex_sha512(password.value);
			var $hidden_hashed_password = hex_sha512( $txt_password.val() );
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'user_login_ajax',
				pedagoge_callback_class: 'ControllerLogin',
				user_name: $txt_username.val(),
				password: $txt_password.val(),
				remember: $remember_me,
				nexturl: $nexturl,
				hidden_hashed_password: $hidden_hashed_password
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				try {
					
					var $response_data = $.parseJSON( response ),
						$error = $response_data.error,
						$error_type = $response_data.error_type,
						$loggedin = $response_data.loggedin,
						$message = $response_data.message,
						$next_url = $response_data.url;
					
					if ( $error ) {
						//handle login error
						$button.button( 'reset' );
						pdg_show_alerts( 'div_login_result', $message, 'error' );
						switch ( $error_type ) {
							case 'wrong_username':
								$txt_username.select().focus();
								break;
							case 'wrong_password':
								$txt_password.select().focus();
								break;
						}
						
					} else {
						if ( $loggedin == true ) {
							//var $loading_url = $response_data.url;
							pdg_show_alerts( 'div_login_result', $message, 'success' );
							window.location.href = $next_url;
							
							/*setTimeout(function() {
							 location.reload(true);
							 },500);	*/
						}
					}
				} catch ( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
					pdg_show_alerts( 'div_login_result', $message, 'error' );
					$button.button( 'reset' );
				} finally {
				
				}
				
			} ).complete( function () {
				//$button.button('reset');
			} );
		}
	} );
} );
///////////////////////////////////////Login Form Processing Ends////////////////////////////////////////////////

