$(document).ready(function(){
	$('.bxslider').bxSlider();
	$('#cmd_trp_submit_multiple').hide();

	$data_array_counter = 0;

	$('#cmd_trp_submit').click(function(){
		var $error_flag = 0;
		var $teacher_institute_name = $('#teacher_institute_name').val();
		if($teacher_institute_name == ''){
			$error_flag = 1;
			var $error_message = "Enter Teacher/Institute Name";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_phone = $('#teacher_institute_phone').val();
		if($teacher_institute_phone == ''){
			$error_flag = 1;
			var $error_message = "Enter Contact Number";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		if($teacher_institute_phone.length != 10){
			$error_flag = 1;
			var $error_message = "Invalid Contact Number";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_email = $('#teacher_institute_email').val();
		if($teacher_institute_email.length > 0){
			var $email_status = validateEmail($teacher_institute_email);
			if($email_status == false){
				$error_flag = 1;
				var $error_message = "Invalid Email ID";
				fn_display_error_message($error_message, "teacher_institute_data");	
				return;
			}
		}
		var $teacher_institute_subjects = $('#teacher_institute_subjects').val();
		if($teacher_institute_subjects == ''){
			$error_flag = 1;
			var $error_message = "Enter at least one Subject";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_locality = $('#locality').val();
		if($teacher_institute_locality == ''){
			$error_flag = 1;
			var $error_message = "Enter Locality";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}


		if($error_flag == 0){
			$('#trp_error').html("");
			$('#trp_modal_error').html("");
			$('#cmd_trp_submit').attr('data-toggle', 'modal');
			$('#cmd_trp_submit').attr('data-target', '#trp_user_modal');
		}

		$('#cmd_save_trp_user_modal').click(function(){
			if($('#user_name').val() == ''){
				$error_flag = 1;
				var $error_message = "Enter your Name";
				fn_display_error_message($error_message, "single_modal");
				return;
			}

			if(($('#user_email').val()).length > 0){
				var $email_status = validateEmail($('#user_email').val());
				if($email_status == false){
					$error_flag = 1;
					var $error_message = "Invalid Email ID";
					fn_display_error_message($error_message, "single_modal");	
					return;
				}
			}

			if($('#user_contact_number').val() == ''){
				$error_flag = 1;
				var $error_message = "Enter Contact Number";
				fn_display_error_message($error_message, "single_modal");
				return;
			}
			if(($('#user_contact_number').val()).length != 10){
				$error_flag = 1;
				var $error_message = "Invalid Contact Number";
				fn_display_error_message($error_message, "single_modal");
				return;
			}

			fn_get_post_trp_data();
		});
	});

	$('#cmd_trp_add').click(function(){
		var $error_flag = 0;
		var $teacher_institute_name = $('#teacher_institute_name').val();
		if($teacher_institute_name == ''){
			$error_flag = 1;
			var $error_message = "Enter Teacher/Institute Name";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_phone = $('#teacher_institute_phone').val();
		if($teacher_institute_phone == ''){
			$error_flag = 1;
			var $error_message = "Enter Contact Number";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		if($teacher_institute_phone.length != 10){
			$error_flag = 1;
			var $error_message = "Invalid Contact Number";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_email = $('#teacher_institute_email').val();
		if($teacher_institute_email.length > 0){
			var $email_status = validateEmail($teacher_institute_email);
			if($email_status == false){
				$error_flag = 1;
				var $error_message = "Invalid Email ID";
				fn_display_error_message($error_message, "teacher_institute_data");
				return;	
			}
		}
		var $teacher_institute_subjects = $('#teacher_institute_subjects').val();
		if($teacher_institute_subjects == ''){
			$error_flag = 1;
			var $error_message = "Enter at least one Subject";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_locality = $('#locality').val();
		if($teacher_institute_locality == ''){
			$error_flag = 1;
			var $error_message = "Enter Locality";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		
		$('#cmd_trp_submit').hide();
		$('#cmd_trp_submit_multiple').show();
		fn_get_post_multiple_trp_data();
		$('#teacher_institute_name').val("");
	    $('#teacher_institute_phone').val("");
		$('#teacher_institute_email').val("");
		$('#teacher_institute_subjects').val("");
		$('#locality').val("");

		var $message = $teacher_institute_name+"'s data has been recorded. Enter New Teacher Details"
		fn_display_error_message($message, "teacher_institute_data_info");

	});

	$('#cmd_trp_submit_multiple').click(function(){
		var $error_flag = 0;
		var $teacher_institute_name = $('#teacher_institute_name').val();
		if($('#teacher_institute_name').val() == ''){
			$error_flag = 1;
			var $error_message = "Enter Teacher/Institute Name";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_phone = $('#teacher_institute_phone').val();
		if($teacher_institute_phone == ''){
			$error_flag = 1;
			var $error_message = "Enter Contact Number";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		if($teacher_institute_phone.length != 10){
			$error_flag = 1;
			var $error_message = "Invalid Contact Number";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_email = $('#teacher_institute_email').val();
		if($teacher_institute_email.length > 0){
			var $email_status = validateEmail($teacher_institute_email);
			if($email_status == false){
				$error_flag = 1;
				var $error_message = "Invalid Email ID";
				fn_display_error_message($error_message, "teacher_institute_data");	
				return;
			}
		}
		var $teacher_institute_subjects = $('#teacher_institute_subjects').val();
		if($teacher_institute_subjects == ''){
			$error_flag = 1;
			var $error_message = "Enter at least one Subject";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}
		var $teacher_institute_locality = $('#locality').val();
		if($teacher_institute_locality == ''){
			$error_flag = 1;
			var $error_message = "Enter Locality";
			fn_display_error_message($error_message, "teacher_institute_data");
			return;
		}


		fn_get_post_multiple_trp_data();

		if($error_flag == 0){
			$('#trp_error').html("");
			$('#trp_modal_error').html("");
			$('#cmd_trp_submit_multiple').attr('data-toggle', 'modal');
			$('#cmd_trp_submit_multiple').attr('data-target', '#trp_user_modal');
		}

		$('#cmd_save_trp_user_modal').click(function(){
			var $error_flag = 0;
			if($('#user_name').val() == ''){
				$error_flag = 1;
				var $error_message = "Enter your Name";
				fn_display_error_message($error_message, "single_modal");
				return;
			}
			if(($('#user_email').val()).length > 0){
				var $email_status = validateEmail($('#user_email').val());
				if($email_status == false){
					$error_flag = 1;
					var $error_message = "Invalid Email ID";
					fn_display_error_message($error_message, "single_modal");	
					return;
				}
			}
			if($('#user_contact_number').val() == ''){
				$error_flag = 1;
				var $error_message = "Enter Contact Number";
				fn_display_error_message($error_message, "single_modal");
				return;
			}
			if(($('#user_contact_number').val()).length != 10){
				$error_flag = 1;
				var $error_message = "Invalid Contact Number";
				fn_display_error_message($error_message, "single_modal");
				return;
			}
			fn_submit_multiple_trp_data();
		});
	});

	
});

function validateEmail($email){
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test($email);
}


function fn_get_post_trp_data(){
	var $duplicate_teacher_flag = false;
	var $teacher_institute_data = [];

	var $teacher_institute_name = $('#teacher_institute_name').val();
	var $teacher_institute_phone = $('#teacher_institute_phone').val();
	var $teacher_institute_email = $('#teacher_institute_email').val();
	var $teacher_institute_subjects = $('#teacher_institute_subjects').val();
	var $teacher_institute_locality = $('#locality').val();

	var $user_name = $('#user_name').val();
	var $user_email = $('#user_email').val();
	var $user_contact_number = $('#user_contact_number').val();

	$teacher_institute_data.push($teacher_institute_name);
	$teacher_institute_data.push($teacher_institute_phone);
	$teacher_institute_data.push($teacher_institute_email);
	$teacher_institute_data.push($teacher_institute_subjects);
	$teacher_institute_data.push($teacher_institute_locality);

	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	$loader.show();
	$loader_result_area.html('');

	var $button = $(this);
			 	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_save_trp_ajax',
		pedagoge_callback_class : 'ControllerTrp',
		teacher_institute_data : $teacher_institute_data,
		user_name : $user_name,
		user_email : $user_email,
		user_contact_number : $user_contact_number
	};

	$.post($ajax_url, $submit_data, function(response) {
		try {
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$message = $response_data.message,
				 	$duplicate_teacher = $response_data.duplicate_teacher;
					
				$error_occured = false; 	
				if($error){				
				}
				if($duplicate_teacher){
					$('#trp_modal_body').html('<h3>'+$message+'</h3>');
					$duplicate_teacher_flag = true;
				} 						
			}
		catch(err){
				$button.button('reset');
		}
		finally{
				$loader.hide();
		}			

	}).complete(function() {
		if($duplicate_teacher_flag == false){
    		$('#trp_modal_body').html('<h3>Thank You for your referral. We will contact you soon. &#9787;</h3>');
		}
		$loader.hide();
	});
}

$teacher_institute_multiple_data = [];

function fn_get_post_multiple_trp_data(){
	$data_array_counter += 1;
	var $teacher_institute_name = $('#teacher_institute_name').val();
	var $teacher_institute_phone = $('#teacher_institute_phone').val();
	var $teacher_institute_email = $('#teacher_institute_email').val();
	var $teacher_institute_subjects = $('#teacher_institute_subjects').val();
	var $teacher_institute_locality = $('#locality').val();

	$teacher_institute_multiple_data.push($teacher_institute_name);
	$teacher_institute_multiple_data.push($teacher_institute_phone);
	$teacher_institute_multiple_data.push($teacher_institute_email);
	$teacher_institute_multiple_data.push($teacher_institute_subjects);
	$teacher_institute_multiple_data.push($teacher_institute_locality);
}

function fn_submit_multiple_trp_data(){
	var $duplicate_teacher_flag = false;
	var $user_name = $('#user_name').val();
	var $user_email = $('#user_email').val();
	var $user_contact_number = $('#user_contact_number').val();

	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	$loader.show();
	$loader_result_area.html('');

	var $button = $(this);
			 	
	var $submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'fn_insert_multiple_trp_ajax',
		pedagoge_callback_class : 'ControllerTrp',
		teacher_institute_data_array : $teacher_institute_multiple_data,
		user_name : $user_name,
		user_email : $user_email,
		user_contact_number : $user_contact_number,
		data_array_counter: $data_array_counter
	};

	$.post($ajax_url, $submit_data, function(response) {
		try {
				var $response_data = $.parseJSON(response);
				
				var $error = $response_data.error, 
					$error_type = $response_data.error_type,				 	
				 	$data = $response_data.data,
				 	$success_msg = $response_data.success_message,
				 	$duplicate_teacher = $response_data.duplicate_teacher;

					
				$error_occured = false;
					
				if($error){

				}
				if($duplicate_teacher){
					var $i = 0;				
					$('#trp_modal_body').html($success_msg+$data);
					$duplicate_teacher_flag = true;	
				}	
			}
			catch(err) {
				$button.button('reset');
			}
			finally {
				$loader.hide();
			}										
	}).complete(function() {
		if($duplicate_teacher_flag == false){
    		$('#trp_modal_body').html('<h3>Thank You for your referral. We will contact you soon. &#9787;</h3>');
    	}
		$loader.hide();		    
	});
}

function fn_display_error_message($error_message, $type){
	if($type == "teacher_institute_data_info"){
		$('#trp_error').html('<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4> '+$error_message+'</h4></div>');
	}

	if($type == "teacher_institute_data"){
		$('#trp_error').html('<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4> '+$error_message+'</h4></div>');
	}

	if($type = "single_modal"){
		$('#trp_modal_error').html('<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4> '+$error_message+'</h4></div>');
	}
}
