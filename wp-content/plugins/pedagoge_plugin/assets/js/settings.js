$(document).ready(function() {
	$("#cmd_test_ajax").click(function(event){
		var $loader = $("#img_loader"),
			$result_area = $("#div_settings_result_area");
		
		$loader.show();
		$result_area.html('');
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_settings_process_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
		};
		
		$.post($ajax_url, submit_data, function(response) {
			$result_area.html(response);				 
        }).complete(function() {	        	
        	$loader.hide();
        });
	});
	
	$(".cmd_migrate").click(function(event){
		var $cmd_migrate = $(this),
			$btn_type = $cmd_migrate.data('type');
		if($btn_type=='migrate_subject' || $btn_type == 'migrate_locality') {
			//confirm before processing
			var $str_msg = "";
			switch($btn_type) {
				case 'migrate_subject' :
					$str_msg = "Are you sure, you want to reset database?";
					break;
				case 'migrate_locality' :
					$str_msg = "Are you sure, you want to delete all users?";
					break;
			}
			bootbox.confirm($str_msg, function(result) {
				if(result) {
					fn_migrate_operation($btn_type);
				}
			});
		} else {
			fn_migrate_operation($btn_type);
		}
	});
});

function fn_migrate_operation($migration_type) {
	var $loader = $("#img_loader"),
		$result_area = $("#div_settings_result_area");
		
		$loader.show();
		$result_area.html('');
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_process_migration_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
			migration_type : $migration_type
		};
		
		$.post($ajax_url, submit_data, function(response) {
			$result_area.html(response);				 
        }).complete(function() {	        	
        	$loader.hide();
        });
}
