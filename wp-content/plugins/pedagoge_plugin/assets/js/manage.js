$(document).ready(function() {
	fakewaffle.responsiveTabs(['xs', 'sm']);
	$("#div_manage_review_name_result_area").on("click", ".cmd_show_review", function(event){
		$('#review_modal').show();
	});
	/***course category starts**/
	$('.select_control').select2();	
	$('.multiple_select_control').select2({		
		closeOnSelect: false
	});

	$(".select_board_type").hide();
	$(".select_course_age_type").hide();

	$("#select_course_type").change(function(){
		var $selected_course_type = $(this).val();
		if ($(this).val() == 2 ) {
			$(".select_board_type").hide();
			$(".select_course_age_type").show();
		} 
		else if($(this).val() == 1) {
			$(".select_board_type").show();
			$(".select_course_age_type").hide();

		}
		else {
			$(".select_board_type").hide();
			$(".select_course_age_type").hide();				
		}		
	});
	/***course category ends***/
	$("#select_country").change(function(){
		var $selected_country = $(this).val();
		if($selected_country.length<=0) {
			//reset state
			//reset city
			//reset result area
			fn_reset_city_state();
		} else {
			//load state via ajax and populate state select
			fn_load_state($selected_country);
			//console.log($selected_country);	
						
		}	
			
	});
	
	$("#select_state").change(function(){
		
		var $selected_state = $(this).val();
		if($selected_state.length<=0) {
			//reset state
			//reset city
			//reset result area
			fn_reset_city_state();
		} else {
			//load state via ajax and populate state select
			fn_load_city($selected_state);
		}		
	});
	
	$("#select_city").change(function(){
	});
	
	
	$("#div_manage_country_result_area").on("click", ".cmd_edit_state", function(event){
		
		
		var $current_obj = $(this);
		var $tr_state_data = $current_obj.closest('.tr_state_data');
		var $state_id = $tr_state_data.find(".td_state_id").html(), 
		$state_name = $tr_state_data.find(".td_state_name").html();
		//console.log($state_id+' : '+$state_name);
		$("#txt_state_name").val($state_name);
		$("#hidden_state_id").val($state_id);
		
	});
	
	$("#div_manage_country_result_area").on("click", ".cmd_edit_city", function(event){
		
		var $current_obj = $(this);
		var $tr_city_data = $current_obj.closest('.tr_city_data');
		var $city_id = $tr_city_data.find(".td_city_id").html(), 
		$city_name = $tr_city_data.find(".td_city_name").html();
		//console.log($state_id+' : '+$state_name);
		$("#txt_city_name").val($city_name);
		$("#hidden_city_id").val($city_id);
		
	});
		
		/**update taecher active field**/


		/**end of update teacher active field**/

		/**teacher management**/
		$("#select_teacher_name").click(function(){
			var $teacher_name_data = $(this).val();			
			fn_load_teacher_name($teacher_name_data);
			
		});

		$("#div_manage_teacher_name_result_area").on("click", ".cmd_edit_teacher_name", function(event){
			var $current_obj = $(this);
			var $tr_teacher_name_data = $current_obj.closest('.tr_teacher_name_data');
			var $teacher_id = $tr_teacher_name_data.find(".td_teacher_id").html(), 
			$teacher_name = $tr_teacher_name_data.find(".td_teacher_name").html();
			//console.log($state_id+' : '+$state_name);
			$("#txt_teacher_name").val($teacher_name);
			$("#hidden_teacher_id").val($teacher_id);
			fn_save_teacher_active_data($teacher_id);

		});
	
		/**end of teacher management**/


		/**Institute management**/

		$("#select_institute_name").click(function(){
			var $institute_name_data = $(this).val();			
			fn_load_institute_name($institute_name_data);
			
		});

		$("#div_manage_institute_name_result_area").on("click", ".cmd_edit_institute_name", function(event){
			var $current_obj = $(this);
			var $tr_institute_name_data = $current_obj.closest('.tr_institute_name_data');
			var $institute_id = $tr_institute_name_data.find(".td_institute_id").html(), 
			$institute_name = $tr_institute_name_data.find(".td_institute_name").html();
			//console.log($state_id+' : '+$state_name);
			$("#txt_institute_name").val($institute_name);
			$("#hidden_institute_id").val($institute_id);
			fn_save_institute_active_data($institute_id);
		});
	
		/**end of institute management**/

		
		
		/***Subject type Management***/
		$("#select_subject_type").click(function(){
			var $subject_type_data = $(this).val();
			fn_load_subject_type($subject_type_data);		
		});

		$("#div_manage_subject_result_area").on("click", ".cmd_edit_subject_type", function(event){
			var $current_obj = $(this);
			var $tr_subject_type_data = $current_obj.closest('.tr_subject_type_data');
			var $subject_type_id = $tr_subject_type_data.find(".td_subject_type_id").html(), 
			$subject_type = $tr_subject_type_data.find(".td_subject_type").html();
			//console.log($state_id+' : '+$state_name);
			$("#txt_subject_type").val($subject_type);
			$("#hidden_subject_type_id").val($subject_type_id);
		
		});

  		$("#div_manage_subject_result_area").on("click", ".cmd_save_subject_type", function(event){
  			event.preventDefault();
  	
			
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');

		  	var $button = $(this);
			 	$txt_subject_type = $("#txt_subject_type").val();
			 	$txt_subject_type_id = $("#hidden_subject_type_id").val();
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_save_subject_type_ajax',
				pedagoge_callback_class : 'ControllerManage',
				subject_type : $txt_subject_type,
				subject_type_id : $txt_subject_type_id
			};

			$.post($ajax_url, $submit_data, function(response) {
							

	        }).complete(function() {
	        	$loader.hide();
	        	location.reload(true);
	        });
  		});

  		$("#div_manage_subject_result_area").on("click", ".cmd_delete_subject_type", function(event){
			//event.preventDefault();
			var $current_obj = $(this);
			var $tr_subject_type_data = $current_obj.closest('.tr_subject_type_data');
			var $subject_type_id = $tr_subject_type_data.find(".td_subject_type_id").html(), 
			$subject_type = $tr_subject_type_data.find(".td_subject_type").html();
			//console.log($state_id+' : '+$state_name);
			 $("#txt_subject_type").val($subject_type);
			$("#hidden_subject_type_id").val($subject_type_id);
		
		
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');
		  	//event.preventDefault();
		  	var $button = $(this),
			 	$txt_subject_type = $("#txt_subject_type"),
			 	$txt_subject_type_id = $("#hidden_subject_type_id").val()
			 	;

			 //alert($txt_subject_name_id);

		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_delete_subject_type_ajax',
				pedagoge_callback_class : 'ControllerManage',
				subject_type : $txt_subject_type.val(),
				subject_type_id : $txt_subject_type_id
			};
			$.post($ajax_url, $submit_data, function(response) {
							

	        }).complete(function() {
	        	$loader.hide();
	        	location.reload(true);
	        });
  		});
	
		/***End of Subject type Management***/

		/***start of schoolmanagement****/

		$("#select_school").click(function(){
			var $school_data = $(this).val();
			fn_load_school($school_data);		
		});

		$("#div_manage_school_result_area").on("click", ".cmd_edit_school", function(event){
			
			var $current_obj = $(this);
			var $tr_school_data = $current_obj.closest('.tr_school_data');
			var $school_id = $tr_school_data.find(".td_school_id").html(), 
			$school = $tr_school_data.find(".td_school").html();
			$("#txt_school").val($school);
			$("#hidden_school_id").val($school_id);		
		});


		$("#div_manage_school_result_area").on("click", ".cmd_save_school", function(event){
  			
  			event.preventDefault();
  	
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");
			$loader.show();
			$loader_result_area.html('');

			var $select_school_city_value = $("#select_city_type");
		  	var $button = $(this);
			 	$txt_school = $("#txt_school").val();
			 	$txt_school_id = $("#hidden_school_id").val();
			 	$select_school_city_value = $("#select_city_type ").val();	 	
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_save_school_ajax',
				pedagoge_callback_class : 'ControllerManage',
				school_name : $txt_school,
				school_id : $txt_school_id,
				school_city : $select_school_city_value
			};

			$.post($ajax_url, $submit_data, function(response) {
				
				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $result = $response_data.result;
				var $data = $response_data.data;
				if($error) {
					fn_alert_message($loader_result_area, $result, 'blank_error');
					$loader.hide();
					
				} else {
					fn_alert_message($loader_result_area, $result, 'blank_success');
				}			

	        }).complete(function() {
	        	$loader.hide();
	        });
  		});


  		$("#div_manage_school_result_area").on("click", ".cmd_delete_school", function(event){
			var $current_obj = $(this);
			var $tr_school_data = $current_obj.closest('.tr_school_data');
			var $school_id = $tr_school_data.find(".td_school_id").html(), 
			$school = $tr_school_data.find(".td_school").html();
			$("#txt_school").val($school);
			$("#hidden_school_id").val($school_id);		
		
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");
			$loader.show();
			$loader_result_area.html('');

			var $select_school_city_value = $("#select_city_type");
		  	var $button = $(this);
			 	$txt_school = $("#txt_school").val();
			 	$txt_school_id = $("#hidden_school_id").val();
			 	$select_school_city_value = $("#select_city_type ").val();	 	
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_delete_school_ajax',
				pedagoge_callback_class : 'ControllerManage',
				school_name : $txt_school,
				school_id : $txt_school_id,
				school_city : $select_school_city_value
			};

			$.post($ajax_url, $submit_data, function(response) {
				
				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $result = $response_data.result;
				var $data = $response_data.data;
				if($error) {
					fn_alert_message($loader_result_area, $result, 'blank_error');
					$loader.hide();
					
				} else {
					fn_alert_message($loader_result_area, $result, 'blank_success');
				}			

	        }).complete(function() {
	        	$loader.hide();
	        });
  		});

		/***end of school management***/

		/***start of qualification management**/
		$("#select_qualification").click(function(){
			var $qualification_data = $(this).val();
			fn_load_qualification($qualification_data);		
		});

		$("#div_manage_qualification_result_area").on("click", ".cmd_edit_qualification", function(event){
			
			var $current_obj = $(this);
			var $tr_qualification_data = $current_obj.closest('.tr_qualification_data');
			var $qualification_id = $tr_qualification_data.find(".td_qualification_id").html(), 
			$qualification = $tr_qualification_data.find(".td_qualification").html();
			alert($qualification_id);
			//console.log($state_id+' : '+$state_name);
			$("#txt_qualification").val($qualification);
			$("#hidden_qualification_id").val($qualification_id);
		
		});

		$("#div_manage_qualification_result_area").on("click", ".cmd_save_qualification", function(event){
  			
  			event.preventDefault();
  	
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");
			$loader.show();
			$loader_result_area.html('');

			var $select_qualification_type_value = $("#select_qualification_type");
		  	var $button = $(this);
			 	$txt_qualification = $("#txt_qualification").val();
			 	$txt_qualification_id = $("#hidden_qualification_id").val();
			 	$select_qualification_type = $("#select_qualification_type ").val();	 	
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_save_qualification_ajax',
				pedagoge_callback_class : 'ControllerManage',
				qualification : $txt_qualification,
				qualification_id : $txt_qualification_id,
				qualification_type : $select_qualification_type
			};

			$.post($ajax_url, $submit_data, function(response) {
				
				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $result = $response_data.result;
				var $data = $response_data.data;
				
				if($error) {
					fn_alert_message($loader_result_area, $result, 'blank_error');
					$loader.hide();
					
				} else {
					alert('success');
					fn_alert_message($loader_result_area, $result, 'blank_success');
					//fn_populate_locality_form($data);
				}			

	        }).complete(function() {
	        	$loader.hide();
	        	//$('#manage').load($('li.active a[data-toggle=$("#pdg_manage_locality")]').attr('href'));
	        	//location.reload(true);
	        });
  		});

  		$("#div_manage_qualification_result_area").on("click", ".cmd_delete_qualification", function(event){
			//event.preventDefault();
			var $current_obj = $(this);
			var $tr_qualification_data = $current_obj.closest('.tr_qualification_data');
			var $qualification_id = $tr_qualification_data.find(".td_qualification_id").html(), 
			$qualification = $tr_qualification_data.find(".td_qualification").html();
			
			//console.log($state_id+' : '+$state_name);
			$("#txt_qualification").val($qualification);
			$("#hidden_qualification_id").val($qualification_id);
		
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');

			var $select_qualification_type_value = $("#select_qualification_type");
		  	var $button = $(this);
			 	$txt_qualification = $("#txt_qualification").val();
			 	$txt_qualification_id = $("#hidden_qualification_id").val();
			 	$select_qualification_type = $("#select_qualification_type ").val();

			 	
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_delete_qualification_ajax',
				pedagoge_callback_class : 'ControllerManage',
				qualification : $txt_qualification,
				qualification_id : $txt_qualification_id,
				qualification_type : $select_qualification_type
			};

			$.post($ajax_url, $submit_data, function(response) {

				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $result = $response_data.result;
				var $data = $response_data.data;
				if($error) {
					fn_alert_message($loader_result_area, $result, 'blank_error');
					$loader.hide();
					
				} else {
					alert('success');
					fn_alert_message($loader_result_area, $result, 'blank_success');
				}			

	        }).complete(function() {
	        	$loader.hide();
	        });
  		});

		/***end of qualification management**/

		/***start of locality management***/

		$("#select_locality").click(function(){
			var $locality_data = $(this).val();
			fn_load_locality($locality_data);		
		});

		$("#div_manage_locality_result_area").on("click", ".cmd_edit_locality", function(event){
			
			var $current_obj = $(this);
			var $tr_locality_data = $current_obj.closest('.tr_locality_data');
			var $locality_id = $tr_locality_data.find(".td_locality_id").html(), 
			$locality = $tr_locality_data.find(".td_locality").html();
			//console.log($state_id+' : '+$state_name);
			$("#txt_locality").val($locality);
			$("#hidden_locality_id").val($locality_id);
		
		});

  		$("#div_manage_locality_result_area").on("click", ".cmd_save_locality", function(event){
  			
  			event.preventDefault();
  	
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');

			var $txt_locality_value = $("#txt_locality");
		  	var $button = $(this);
			 	$txt_locality = $("#txt_locality").val();
			 	$txt_locality_id = $("#hidden_locality_id").val();
			 	$select_city_id = $("#select_city_type").val();
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_save_locality_ajax',
				pedagoge_callback_class : 'ControllerManage',
				locality : $txt_locality,
				locality_id : $txt_locality_id,
				city_id : $select_city_id
			};

			$.post($ajax_url, $submit_data, function(response) {
				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $result = $response_data.result;
				var $data = $response_data.data;
				if($error) {
					fn_alert_message($loader_result_area, $result, 'blank_error');
					$loader.hide();
					
				} else {
					fn_alert_message($loader_result_area, $result, 'blank_success');
					//fn_populate_locality_form($data);

				}			

	        }).complete(function() {
	        	$loader.hide();
	        	//$('#manage').load($('li.active a[data-toggle=$("#pdg_manage_locality")]').attr('href'));
	        	//location.reload(true);


	        });
  		});


  		$("#div_manage_locality_result_area").on("click", ".cmd_delete_locality", function(event){
			//event.preventDefault();
			var $current_obj = $(this);
			var $tr_locality_data = $current_obj.closest('.tr_locality_data');
			var $locality_id = $tr_locality_data.find(".td_locality_id").html(), 
			$locality = $tr_locality_data.find(".td_locality").html();
			//console.log($state_id+' : '+$state_name);
			$("#txt_locality").val($locality);
			$("#hidden_locality_id").val($locality_id);
		
		
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');
		  	//event.preventDefault();
		  	var $button = $(this);
			 	$txt_locality = $("#txt_locality").val();
			 	$txt_locality_id = $("#hidden_locality_id").val();
			 	$select_city_id = $("#select_city_type").val();

			 //alert($txt_subject_name_id);

		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_delete_locality_ajax',
				pedagoge_callback_class : 'ControllerManage',
				locality : $txt_locality,
				locality_id : $txt_locality_id,
				city_id : $select_city_id
			};

			$.post($ajax_url, $submit_data, function(response) {

				var $response_data = $.parseJSON(response);
				var $error = $response_data.error;
				var $result = $response_data.result;
				var $data = $response_data.data;
				if($error) {
					fn_alert_message($loader_result_area, $result, 'blank_error');
					$loader.hide();
					
				} else {
					fn_alert_message($loader_result_area, $result, 'blank_success');
				}
				
	        }).complete(function() {
	        	$loader.hide();	        	
	        	//location.reload(true);
	        });
  		});
		
		/***end of locality management***/

		/***Subject name Management***/
		$("#select_subject_name").click(function(){
			var $subject_name_data = $(this).val();		
			fn_load_subject_name($subject_name_data);
			
		});

		$("#div_manage_subject_name_result_area").on("click", ".cmd_edit_subject_name", function(event){
			var $current_obj = $(this);
			var $tr_subject_name_data = $current_obj.closest('.tr_subject_name_data');
			var $subject_name_id = $tr_subject_name_data.find(".td_subject_name_id").html(), 
			$subject_name = $tr_subject_name_data.find(".td_subject_name").html();
			//console.log($state_id+' : '+$state_name);
			$("#txt_subject_name").val($subject_name);
			$("#hidden_subject_name_id").val($subject_name_id);
		
		});

  		$("#div_manage_subject_name_result_area").on("click", ".cmd_save_subject_name", function(event){
  			event.preventDefault();
  	
			
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');

		  	var $button = $(this);
			 	$txt_subject_name = $("#txt_subject_name").val();
			 	$txt_subject_name_id = $("#hidden_subject_name_id").val();
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_save_subject_name_ajax',
				pedagoge_callback_class : 'ControllerManage',
				subject_name : $txt_subject_name,
				subject_name_id : $txt_subject_name_id
			};

			$.post($ajax_url, $submit_data, function(response) {
							

	        }).complete(function() {
	        	$loader.hide();
	        	location.reload(true);
	        	$('#manage a[href="#pdg_manage_course_category"]').tab('show');
	        	//location.reload(true);

	        });
  		});

  		$("#div_manage_subject_name_result_area").on("click", ".cmd_delete_subject_name", function(event){
			//event.preventDefault();
			var $current_obj = $(this);
			var $tr_subject_name_data = $current_obj.closest('.tr_subject_name_data');
			var $subject_name_id = $tr_subject_name_data.find(".td_subject_name_id").html(), 
			$subject_name = $tr_subject_name_data.find(".td_subject_name").html();

			$("#txt_subject_name").val($subject_name);
			$("#hidden_subject_name_id").val($subject_name_id);
			console.log('Success');
			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');
		  	//event.preventDefault();
		  	var $button = $(this),
			 	$txt_subject_name = $("#txt_subject_name"),
			 	$txt_subject_name_id = $("#hidden_subject_name_id").val()
			 	;

			 //alert($txt_subject_name_id);

		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_delete_subject_name_ajax',
				pedagoge_callback_class : 'ControllerManage',
				subject_name : $txt_subject_name.val(),
				subject_name_id : $txt_subject_name_id
			};
			$.post($ajax_url, $submit_data, function(response) {
							

	        }).complete(function() {
	        	$loader.hide();
	        	location.reload(true);
	        });
  		});

		/***End of Subject Management***/
		/***course management***/

		$("#cmd_save_course_age_type").click(function(event){

  			event.preventDefault();
  			
			 


			var $loader = $(".img_locality_loader"),
			$loader_result_area = $(".span_loader_result");


			$loader.show();
			$loader_result_area.html('');

		  	var $button = $(this);
			 	$select_course_type = $("#select_course_type").val();
				 $select_subject_type = $("#select_course_subject_type").val();
				 $select_subject_name = $("#select_course_subject_name").val();
				 $select_subject_board = $("#select_board_type").val();
				 $select_course_age_type = $("#select_course_age_type").val();
			 	
		  	var $submit_data = {
				nonce : $ajaxnonce,
				action : $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function : 'fn_save_course_type_category_ajax',
				pedagoge_callback_class : 'ControllerManage',
				subject_name_id : $select_subject_name,
				subject_type_id : $select_subject_type,
				course_type_id :  $select_course_type,
				academic_board_id : $select_subject_board,
				course_age_category_id : $select_course_age_type
			};

			$.post($ajax_url, $submit_data, function(response) {
							

	        }).complete(function() {
	        	$loader.hide();
	        	location.reload(true);
	        });
  		});
		/**end of course management***/
	
});


function fn_populate_select_state($state_data) {
	
	var $str_options=' ';
	
	Object.keys($state_data).forEach(function(key){
		//console.log(key,$state_data[key]);
		//console.log($state_data[key].state_name);
		$str_options += '<option value="'+$state_data[key].state_id+'">'+$state_data[key].state_name+'</option>' ;
	}); 
	
	$("#select_state").append($str_options);
}

function fn_populate_select_city($city_data) {
	
	var $str_options=' ';
	
	Object.keys($city_data).forEach(function(key){
		//console.log(key,$city_data[key]);
		//console.log($city_data[key].city_name);
		$str_options += '<option value="'+$city_data[key].city_id+'">'+$city_data[key].city_name+'</option>' ;
	}); 
	
	$("#select_city").append($str_options);
	
}

function fn_load_state($country_id) {
	
	fn_reset_state();
	
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_states_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		country_id : $country_id
	};
	
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_select_state($data);
			fn_populate_state_form($data);
			//$("#select_state").append($data);
		}
		
    }).complete(function() {	        	
    	$loader.hide();
    	$("#state_table").DataTable({
    		
    		
    	});
    });
}

function fn_load_city($state_id) {
	
	fn_reset_city();
	
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_city_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		state_id : $state_id
	};
	
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			//$("#select_city").append($data);
			fn_populate_select_city($data);
			fn_populate_city_form($data);
		}
		
    }).complete(function() {	        	
    	$loader.hide();
    	$("#city_table").DataTable({
    		
    		
    	});
    });
}

function fn_reset_city_state() {
	$("#div_manage_country_result_area").html('');
	fn_reset_city();
	fn_reset_state();	
}

function fn_reset_state() {
	var $option = '<option value="">Select State</option>';
	$("#select_state").html($option);
}

function fn_reset_city() {
	var $option = '<option value="">Select City</option>';
	$("#select_city").html($option);
}

function fn_populate_state_form($state_data) {
	
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_state">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_state_id" />'+
									'<label for="txt_state_name">State Name</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_state_name" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success col-md-12"><i class="fa fa-save"></i> Save</button>'+
									
								'</div>'+
							'</div>'+
						'</form><div id="div_sate_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div class="state_div">'+
								'<table id="state_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>State Id</th>'+
											'<th>State Name</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($state_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_state_data">'+
								'<td class="td_state_id">'+$state_data[key].state_id+'</td>'+
								'<td class="td_state_name">'+$state_data[key].state_name+'</td>'+
								'<td align="center"><button class="btn btn-success cmd_edit_state col-md-12"><i class="fa fa-save"></i> Edit</button></td>'+
							'</tr>';
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';							 	
	var $div_manage_country_result_area = $("#div_manage_country_result_area");
	$div_manage_country_result_area.html($str_form_fields);
}

function fn_populate_city_form($city_data) {
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_city">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_city_id" />'+
									'<label for="txt_state_name">City Name</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_city_name" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success col-md-12"><i class="fa fa-save"></i> Save</button>'+
								'</div>'+
							'</div>'+
						'</form><div id="div_sate_save_result"></div><hr/>';
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="city_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>City Id</th>'+
											'<th>City Name</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
		Object.keys($city_data).forEach(function(key){
		
		$str_form_fields += '<tr class="tr_city_data">'+
								'<td class="td_city_id">'+$city_data[key].city_id+'</td>'+
								'<td class="td_city_name">'+$city_data[key].city_name+'</td>'+
								'<td align="center"><button class="btn btn-success cmd_edit_city col-md-12"><i class="fa fa-save"></i> Edit</button></td>'+
							'</tr>';
		
		
	}); 							 	
	var $div_manage_country_result_area = $("#div_manage_country_result_area");
	$div_manage_country_result_area.html($str_form_fields);
	
}
/***Subject name Management***/

/**start of teacher's name**/

function fn_load_teacher_name($teacher_id) {
	
	
	//$subject_id = $subject_name_id.find("#hidden_subject_name_id").val();
    //$subject_name = $subject_name_id.find("#txt_subject_name").val();
	//console.log($subject_name);
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_teacher_name_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		teacher_id : $teacher_id
	};
		
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_teacher_name_form($data);
		}
		
    }).complete(function() {	        	
    	$loader.hide();   	
    	$("#teacher_name_table").DataTable({    		
    	});
    });
    
    
}


function fn_populate_teacher_name_form($teacher_name_data) {
	
	
	var $str_form_input = 
						'<form id>'+
							'<div class="row form-group div_form_manage_teacher_name">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_teacher_id" />'+
									'<label for="txt_teacher_name">Teacher Name</label>'+
								'</div>'+
								'<div class="col-md-12"><input type="text" class="form-control" id="txt_teacher_name" /></div>'+
								'<div class="col-md-4">'+
									'<!--<button class="btn btn-success cmd_save_teacher_name col-md-12"><i class="fa fa-save"></i> Save</button>-->'+
								'</div>'+
							'</div>'+
						'</form><div id="div_teacher_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="teacher_name_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>Teacher Id</th>'+
											'<th>Teacher Name</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($teacher_name_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_teacher_name_data">'+
								'<td class="td_teacher_id">'+$teacher_name_data[key].teacher_id+'</td>'+
								'<td class="td_teacher_name">'+$teacher_name_data[key].first_name+'</td>'+
								'<td align="center"><button class="btn btn-success cmd_edit_teacher_name col-md-12"><i class="fa fa-save"></i> Authenticate</button></td>'+
							'</tr>';
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_teacher_name_result_area = $("#div_manage_teacher_name_result_area");
	return $div_manage_teacher_name_result_area.html($str_form_fields);
	
}

/**end of teacher's name**/


/**start of institute name**/

function fn_load_institute_name($institute_id) {
	
	
	//$subject_id = $subject_name_id.find("#hidden_subject_name_id").val();
    //$subject_name = $subject_name_id.find("#txt_subject_name").val();
	//console.log($subject_name);
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_institute_name_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		institute_id : $institute_id
	};
		
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_institute_name_form($data);
		}
		
    }).complete(function() {	        	
    	$loader.hide();   	
    	$("#institute_name_table").DataTable({    		
    	});
    });
}


function fn_populate_institute_name_form($institute_name_data) {
	
	
	var $str_form_input = 
						'<form id>'+
							'<div class="row form-group div_form_manage_institute_name">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_institute_id" />'+
									'<label for="txt_institute_name">Institute Name</label>'+
								'</div>'+
								'<div class="col-md-12"><input type="text" class="form-control" id="txt_institute_name" /></div>'+
								'<div class="col-md-4">'+
									'<!--<button class="btn btn-success cmd_save_institute_name col-md-12"><i class="fa fa-save"></i> Save</button>-->'+
								'</div>'+
							'</div>'+
						'</form><div id="div_institute_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="institute_name_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>Institute Id</th>'+
											'<th>Institute Name</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($institute_name_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_institute_name_data">'+
								'<td class="td_institute_id">'+$institute_name_data[key].institute_id+'</td>'+
								'<td class="td_institute_name">'+$institute_name_data[key].institute_name+'</td>'+
								'<td align="center"><button class="btn btn-success cmd_edit_institute_name col-md-12"><i class="fa fa-save"></i> Authenticate</button></td>'+
							'</tr>';
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_institute_name_result_area = $("#div_manage_institute_name_result_area");
	return $div_manage_institute_name_result_area.html($str_form_fields);
	
}

/**end of institute name**/




/***Start of Manage Teacher JS***/

function fn_save_teacher_active_data($teacher_id) {
		
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");
	$loader.show();
	$loader_result_area.html('');

	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_teacher_status_update_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		teacher_id :$teacher_id
	};
	//alert(submit_data);
	jQuery.post($ajax_url, submit_data, function(response) {
    	var $response_data = jQuery.parseJSON(response);
		$error = $response_data.error;
		$message = $response_data.message;

		if($error == true) {
			$.notify($message, "error");
			
		} else {
			
		}
    }).complete(function() {
    	
    	$loader.hide();
    	location.reload(true);
    });
	
}


/**End of Manage teacher JS***/


/***Start of Manage Institute JS***/

function fn_save_institute_active_data($institute_id) {

		var $loader = $(".img_locality_loader"),
		$loader_result_area = $(".span_loader_result");
		$loader.show();
		$loader_result_area.html('');
		
		
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'get_institute_status_update_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
			institute_id :$institute_id
		};
		
		jQuery.post($ajax_url, submit_data, function(response) {
        	var $response_data = jQuery.parseJSON(response);
			$error = $response_data.error;
			$message = $response_data.message;
			if($error == true) {
				//fn_alert_message($result_area, $message, 'error');
				$.notify($message, "error");
				
			} else {
				
			}
        }).complete(function() {
        	
        	$loader.hide();
        	location.reload(true);
        });
	
}


/**End of Manage institute JS***/

/***start of qualification management****/

function fn_populate_qualification_form($qualification_data) {
	
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_qualification">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_qualification_id" />'+
									'<label for="txt_qualification">Qualification</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_qualification" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success cmd_save_qualification col-md-12"><i class="fa fa-save"></i> Save</button>'+
								'</div>'+
							'</div>'+
						'</form><div id="div_qualification_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="qualification_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>Qualification Id</th>'+
											'<th>Qualification</th>'+
											'<th>Qualification Type</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($qualification_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_qualification_data">'+
								'<td class="td_qualification_id">'+$qualification_data[key].qualification_id+'</td>'+
								'<td class="td_qualification">'+$qualification_data[key].qualification+'</td>'+
								'<td class="td_qualification_type">'+$qualification_data[key].qualification_type+'</td>'+
								
								'<td align="center"><button class="btn btn-success cmd_edit_qualification col-md-5" id="cmd_edit_qualification"><i class="fa fa-save"></i> Edit</button>'+'<div class=col-md-2>'+'</div>'+
								'<button class="btn btn-danger cmd_delete_qualification col-md-5"><i class="fa fa-save"></i> Delete</button></td>'+'</tr>';
							
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_qualification_result_area = $("#div_manage_qualification_result_area");
	return $div_manage_qualification_result_area.html($str_form_fields);
	
}

function fn_load_qualification($qualification_id) {
	
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_qualification_name_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		qualification_id : $qualification_id
	};
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_qualification_form($data);
		}
		
    }).complete(function() {
    	$loader.hide();   	
    	$('#qualification_table').DataTable({  		
    	});
    });  
}
/***end of qualification management***/

/****start of schoolmanagement***/


function fn_populate_school_form($school_data) {
	
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_school">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_school_id" />'+
									'<label for="txt_school">School Name</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_school" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success cmd_save_school col-md-12"><i class="fa fa-save"></i> Save</button>'+
								'</div>'+
							'</div>'+
						'</form><div id="div_school_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="school_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>School Id</th>'+
											'<th>School</th>'+
											
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($school_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_school_data">'+
								'<td class="td_school_id">'+$school_data[key].school_id+'</td>'+
								'<td class="td_school">'+$school_data[key].school_name+'</td>'+
								
								'<td align="center"><button class="btn btn-success cmd_edit_school col-md-5"><i class="fa fa-save"></i> Edit</button>'+'<div class=col-md-2>'+'</div>'+
								'<button class="btn btn-danger cmd_delete_school col-md-5"><i class="fa fa-save"></i> Delete</button></td>'+'</tr>';
							
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_school_result_area = $("#div_manage_school_result_area");
	return $div_manage_school_result_area.html($str_form_fields);
	
}

function fn_load_school($school_id) {
	
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_school_name_ajax',
		pedagoge_callback_class : 'ControllerManage',
		school_id : $school_id
	};
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_school_form($data);
		}
		
    }).complete(function() {
    	$loader.hide();   	
    	$('#school_table').DataTable({  		
    	});
    });  
}


/****end of schoolmanagement****/


/****start of locality js***/

function fn_populate_locality_form($locality_data) {
	
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_locality">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_locality_id" />'+
									'<label for="txt_locality">Locality</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_locality" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success cmd_save_locality col-md-12"><i class="fa fa-save"></i> Save</button>'+
								'</div>'+
							'</div>'+
						'</form><div id="div_locality_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="locality_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>Locality Id</th>'+
											'<th>Locality</th>'+
											
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($locality_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_locality_data">'+
								'<td class="td_locality_id">'+$locality_data[key].locality_id+'</td>'+
								'<td class="td_locality">'+$locality_data[key].locality+'</td>'+
								
								'<td align="center"><button class="btn btn-success cmd_edit_locality col-md-5" id="cmd_edit_locality"><i class="fa fa-save"></i> Edit</button>'+'<div class=col-md-2>'+'</div>'+
								'<button class="btn btn-danger cmd_delete_locality col-md-5"><i class="fa fa-save"></i> Delete</button></td>'+'</tr>';
							
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_locality_result_area = $("#div_manage_locality_result_area");
	return $div_manage_locality_result_area.html($str_form_fields);
	
}

function fn_load_locality($locality_id) {
	
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_locality_name_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		locality_id : $locality_id
	};
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_locality_form($data);
		}
		
    }).complete(function() {
    	$loader.hide();   	
    	$('#locality_table').DataTable({  		
    	});
    });  
}

/***end of locality js***/


/***Start of Subject Management***/

function fn_populate_subject_type_form($subject_type_data) {
	
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_subject_type">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_subject_type_id" />'+
									'<label for="txt_subject_type">Subject Type</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_subject_type" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success cmd_save_subject_type col-md-12"><i class="fa fa-save"></i> Save</button>'+
								'</div>'+
							'</div>'+
						'</form><div id="div_subject_type_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="subject_type_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>Subject Type Id</th>'+
											'<th>Subject Type</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($subject_type_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_subject_type_data">'+
								'<td class="td_subject_type_id">'+$subject_type_data[key].subject_type_id+'</td>'+
								'<td class="td_subject_type">'+$subject_type_data[key].subject_type+'</td>'+
								'<td align="center"><button class="btn btn-success cmd_edit_subject_type col-md-5"><i class="fa fa-save"></i> Edit</button>'+'<div class=col-md-2>'+'</div>'+
								'<button class="btn btn-danger cmd_delete_subject_type col-md-5"><i class="fa fa-save"></i> Delete</button></td>'+'</tr>';
							
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_subject_result_area = $("#div_manage_subject_result_area");
	return $div_manage_subject_result_area.html($str_form_fields);
	
}

function fn_load_subject_type($subject_type_id) {
	
	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_subject_type_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		subject_type_id : $subject_type_id
	};
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_subject_type_form($data);
		}
		
    }).complete(function() {
    	$loader.hide();   	
    	$('#subject_type_table').DataTable({  		
    	});
    });  
}


function fn_load_subject_name($subject_name_id) {

	var $loader = $(".img_locality_loader"),
	$loader_result_area = $(".span_loader_result");

	//$loader.show();
	$loader_result_area.html('');
	
	var submit_data = {
		nonce : $ajaxnonce,
		action : $pedagoge_visitor_ajax_handler,
		pedagoge_callback_function : 'get_subject_name_ajax',
		pedagoge_callback_class : $pedagoge_callback_class,
		subject_name_id : $subject_name_id
	};
	$.post($ajax_url, submit_data, function(response) {
		var $response_data = $.parseJSON(response);
		var $error = $response_data.error;
		var $result = $response_data.result;
		var $data = $response_data.data;
		if($error) {
			fn_alert_message($loader_result_area, $result, 'blank_error');
		} else {
			fn_alert_message($loader_result_area, $result, 'blank_success');
			fn_populate_subject_name_form($data);
		}
		
    }).complete(function() {
    	$loader.hide();   	
    	$("#subject_name_table").DataTable({    		
    	});
    });
    
}

function fn_populate_subject_name_form($subject_name_data) {
	
	
	var $str_form_input = 
						'<form>'+
							'<div class="row form-group div_form_manage_subject_name">'+
								'<div class="col-md-2">'+
									'<input type="hidden" id="hidden_subject_name_id" />'+
									'<label for="txt_subject_name">Subject Name</label>'+
								'</div>'+
								'<div class="col-md-6"><input type="text" class="form-control" id="txt_subject_name" /></div>'+
								'<div class="col-md-4">'+
									'<button class="btn btn-success cmd_save_subject_name col-md-12"><i class="fa fa-save"></i> Save</button>'+
								'</div>'+
							'</div>'+
						'</form><div id="div_subject_save_result"></div><hr/>';
	
	
	
	var $str_form_fields = $str_form_input+'<div>'+
								'<table id="subject_name_table" class="table table-striped table-bordered" cellspacing="0" width="100%">'+
									'<thead>'+
										'<tr>'+
											'<th>Subject Name Id</th>'+
											'<th>Subject Name</th>'+
											'<th>Action</th>'+
										'</tr>'+
									 '</thead>';
									 
					 
									 
	Object.keys($subject_name_data).forEach(function(key){
		
		$str_form_fields +=		
							'<tr class="tr_subject_name_data">'+
								'<td class="td_subject_name_id">'+$subject_name_data[key].subject_name_id+'</td>'+
								'<td class="td_subject_name">'+$subject_name_data[key].subject_name+'</td>'+
								'<td align="center"><button class="btn btn-success cmd_edit_subject_name col-md-5"><i class="fa fa-save"></i> Edit</button>'+'<div class=col-md-2>'+'</div>'+
								'<button class="btn btn-danger cmd_delete_subject_name col-md-5"><i class="fa fa-save"></i> Delete</button></td>'+'</tr>';
		
		
	}); 
	
	$str_form_fields += '</table>'+
							'</div>';	
													 	
	var $div_manage_subject_name_result_area = $("#div_manage_subject_name_result_area");
	return $div_manage_subject_name_result_area.html($str_form_fields);
	
}
/*End of subject management*/

function fn_alert_message($div_id, $message, $alert_type, $timer){
	
	clearTimeout($alert_time_out_var);
	
	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000 : $timer;
	
	var $result_area = null;
	if( typeof $div_id === "string") {
		$result_area = jQuery( "#" + $div_id );	
	} else {
		$result_area = $div_id;
	}
		
	var $str_message = '';
	
	
	$result_area.html('');
	
	switch($alert_type)
	{
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'info':
			$str_message ='<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'success':
			$str_message ='<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'warning':
			$str_message ='<div class="alert alert-block alert-primary fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> '+$message+'</div>';
			break;
		case 'blank_success':
			$str_message ='<span class="label label-success fade in blink_me">'+$message+'</span>';
			break;
		case 'blank_error':
			$str_message ='<span class="label label-primary fade in blink_me">'+$message+'</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html( $str_message );
	
	if($timer)
	{
		$alert_time_out_var = setTimeout(function()
		{
			$result_area.html('');
		}, $timer);		
	}
}

/******************************************* Reviews Management Datatable *********************/
$(document).ready(function() {
	var $reviews_datatable = null;
	var $academic_session_modal = null;
	var $academic_session_dialog_content = null;
	
	$("#select_review_name").click(function(){
		$reviews_datatable = null;
		var $loader = $(".img_reviews_loader"),
		$loader_result_area = $(".span_reviw_loader_result");
	
		$loader.show();
		$loader_result_area.html('');
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_load_reviews_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
		};
		$.post($ajax_url, submit_data, function(response) {
			var $response_data = $.parseJSON(response);
			var $error = $response_data.error;
			var $result = $response_data.result;
			var $data = $response_data.data;
						
			if($error) {
				fn_alert_message($loader_result_area, $result, 'blank_error');				
			} else {				
				fn_alert_message($loader_result_area, $result, 'blank_success');
				
				$("#div_manage_review_result_area").html($data);
				if($data.indexOf('review_table')>=0) {
					$reviews_datatable = $("#div_manage_review_result_area").find('#review_table').DataTable( {
				        "columnDefs": [
				            {
				                "targets": [ 1, 2, 3, 5, 6, 7, 8, 10, 12, 13, 14 ],
				                "visible": false,
				                
				            },
				            {
				                "targets": [ 0, 1, 2, 3, 5, 6, 7, 8, 9, 16],
				                "searchable": false
				            }				            
				        ]
				    });
				}
			}
			
	    }).complete(function() {
	    	$loader.hide();
	    });
	});
	
	$('#div_manage_review_result_area').on('click', '.cmd_show_review', function(){
		fn_reset_review_modal();		
		var $row = $reviews_datatable.row( $(this).closest('tr')[0] );
		var $row_data = $row.data();
		var $rowIndex = $row.index();
		
		var $review_id = $row_data[0];
		var $reviewer_name = $row_data[11];
		var $reviewer_email = $row_data[10];
		var $reviewee_name = $row_data[15];
		var $reviewee_email = $row_data[13];
		var $rating1 = $row_data[5];
		var $rating2 = $row_data[6];
		var $rating3 = $row_data[7];
		var $rating4 = $row_data[8];
		var $avg_rating = $row_data[9];
		var $is_approved = $row_data[4];
		var $review_text = $row_data[1];
				
		$(".div_modal_reviewer_name").html($reviewer_name);
		$(".div_modal_reviewer_email").html($reviewer_email);
		$(".div_modal_reviewee_name").html($reviewee_name);
		$(".div_modal_reviewee_email").html($reviewee_email);
		$(".div_modal_rating1").html($rating1);
		$(".div_modal_rating2").html($rating2);
		$(".div_modal_rating3").html($rating3);
		$(".div_modal_rating4").html($rating4);
		$(".div_modal_avg_rating").html($avg_rating);
		$(".div_modal_approved").html($is_approved);
		$(".div_modal_review_text").html($review_text);
		$("#hidden_review_id").val($review_id);
		$("#hidden_review_row_id").val($rowIndex);
		
		$academic_session_dialog_content = fn_academic_dialog_clone();
		
		$academic_session_modal = bootbox.dialog({
			size: 'large',
            title: "Review Details...",                
			message:$academic_session_dialog_content,
			buttons: {
				success: {
					label: "Approve!",
					className: "btn-success",
					callback: function() {
			        	fn_approve_review_data($academic_session_dialog_content);
                        return false;
					}
				},
				cancel: {
					label: "Cancel!",
					className: "btn-primary",					
			    },
			}          
        });
		
    });
	
	function fn_reset_review_modal() {
		
		$(".div_modal_reviewer_name").html('');
		$(".div_modal_reviewer_email").html('');
		$(".div_modal_reviewee_name").html('');
		$(".div_modal_reviewee_email").html('');
		$(".div_modal_rating1").html('');
		$(".div_modal_rating2").html('');
		$(".div_modal_rating3").html('');
		$(".div_modal_rating4").html('');
		$(".div_modal_avg_rating").html('');
		$(".div_modal_approved").html('');
		$(".div_modal_review_text").html('');
		$("#hidden_review_id, #hidden_review_row_id").val('');
		
	}
	
	function fn_academic_dialog_clone() {
		return $(".div_session_dialog").clone();
	}
	
	function fn_approve_review_data($review_modal_content, $btn) {
		var $hidden_review_id = $review_modal_content.find("#hidden_review_id").val();
		var $hidden_review_row_id = $review_modal_content.find("#hidden_review_row_id").val();
		
		var $review_approve_area = $review_modal_content.find(".div_review_approve_area");
		if(!$.isNumeric($hidden_review_id) || !$.isNumeric($hidden_review_row_id) || $hidden_review_id < 0 || $hidden_review_row_id < 0) {
			var $result = "Review Information is incorrect. Pleast try again";
			fn_alert_message($review_approve_area, $result, 'error');
			return;
		}
		$review_approve_area.html('');
		
		var submit_data = {
			nonce : $ajaxnonce,
			action : $pedagoge_visitor_ajax_handler,
			pedagoge_callback_function : 'fn_approve_review_ajax',
			pedagoge_callback_class : $pedagoge_callback_class,
			review_id : $hidden_review_id
		};
		$.post($ajax_url, submit_data, function(response) {
			var $response_data = $.parseJSON(response);
			var $error = $response_data.error;
			var $result = $response_data.result;
			var $data = $response_data.data;
						
			if($error) {
				fn_alert_message($review_approve_area, $result, 'error');				
			} else {				
				fn_alert_message($review_approve_area, $result, 'success');
				var $row_id = parseInt($hidden_review_row_id);
				$reviews_datatable.row( $row_id ).remove().draw();
				$academic_session_modal.modal('hide');
				$academic_session_dialog_content = null;
			}
			
	    }).complete(function() {
	    	
	    });
	}
	
	$(".accordion-toggle").click(function(){		
		var $id_value = $(this).data('id');
		if($id_value === undefined) {
			
		} else {
			$("#"+$id_value).click();
		}
	});
    
});
