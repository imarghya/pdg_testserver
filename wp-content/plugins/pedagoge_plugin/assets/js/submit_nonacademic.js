
$( document ).ready( function () {	
	
	$( "#submit_academicna1" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
		$numberofsubjects=$("#subjectnum").val();
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna1').val();
		
			var submit_data = {
				
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna1').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna1").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				
				if($numberofsubjects==1){
				
					var mystr=response.split("|");	

					$("#subjectSelectNext1").modal("hide");					
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext1").modal("hide");
					$("#subjectSelectNext2").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	
	$( "#submit_academicna2" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna2').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna2').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna2").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==2){
				
					var mystr=response.split("|");			
					
					
					$("#subjectSelectNext2").modal("hide");
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext2").modal("hide");
					$("#subjectSelectNext3").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	
	$( "#submit_academicna3" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna3').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna3').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna3").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==3){
				
					var mystr=response.split("|");	

					$("#subjectSelectNext3").modal("hide");					
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext3").modal("hide");
					$("#subjectSelectNext4").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
		$( "#submit_academicna4" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna4').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna4').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna4").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==4){
				
					var mystr=response.split("|");	
					$("#subjectSelectNext4").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext4").modal("hide");
					$("#subjectSelectNext5").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna5" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna5').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna5').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna5").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==5){
				
					var mystr=response.split("|");	

					$("#subjectSelectNext5").modal("hide");					
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext5").modal("hide");
					$("#subjectSelectNext6").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna6" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna6').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna6').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna6").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==6){
				
					var mystr=response.split("|");	

					$("#subjectSelectNext6").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext6").modal("hide");
					$("#subjectSelectNext7").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna7" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna7').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna7').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna7").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==7){
				
					var mystr=response.split("|");
					$("#subjectSelectNext7").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext7").modal("hide");
					$("#subjectSelectNext8").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna8" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna8').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna8').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna8").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==8){
				
					var mystr=response.split("|");	
					$("#subjectSelectNext8").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext8").modal("hide");
					$("#subjectSelectNext9").modal("show");
					
				}
			
			
			} );
			}
	
	} );

$( "#submit_academicna9" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna9').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna9').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna9").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==9){
				
					var mystr=response.split("|");

					$("#subjectSelectNext9").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext9").modal("hide");
					$("#subjectSelectNext10").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna10" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna10').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna10').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna10").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==10){
				
					var mystr=response.split("|");		

					$("#subjectSelectNext10").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext10").modal("hide");
					$("#subjectSelectNext11").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna11" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna11').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna11').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna11").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==11){
				
					var mystr=response.split("|");			
					$("#subjectSelectNext11").modal("hide");										
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext11").modal("hide");
					$("#subjectSelectNext12").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna12" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna12').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna12').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna12").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==12){
				
					var mystr=response.split("|");			
					$("#subjectSelectNext12").modal("hide");										
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext12").modal("hide");
					$("#subjectSelectNext13").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna13" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna13').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna13').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna13").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==13){
				
					var mystr=response.split("|");			
					$("#subjectSelectNext13").modal("hide");										
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext13").modal("hide");
					$("#subjectSelectNext14").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna14" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna14').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna14').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna14").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==14){
				
					var mystr=response.split("|");
					$("#subjectSelectNext14").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext14").modal("hide");
					$("#subjectSelectNext15").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna15" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subject15').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna15').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna15").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==15){
				
					var mystr=response.split("|");	

					$("#subjectSelectNext15").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext15").modal("hide");
					$("#subjectSelectNext16").modal("show");
					
				}
			
			
			} );
		
			}
	} );
	
	$( "#submit_academicna16" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna16').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna16').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna16").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				$numberofsubjects=$("#subjectnum").val();
				
				alert("Data Submitted");
				
				if($numberofsubjects==16){
				
					var mystr=response.split("|");	
					$("#subjectSelectNext16").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext16").modal("hide");
					$("#subjectSelectNext17").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna17" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna17').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academic17na').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna17").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==17){
				
					var mystr=response.split("|");
					$("#subjectSelectNext17").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext17").modal("hide");
					$("#subjectSelectNext18").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna18" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna18').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna18').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna18").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==18){
				
					var mystr=response.split("|");	
					$("#subjectSelectNext18").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext18").modal("hide");
					$("#subjectSelectNext19").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna19" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna19').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna19').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna19").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==19){
				
					var mystr=response.split("|");	
					$("#subjectSelectNext19").modal("hide");										
					
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext19").modal("hide");
					$("#subjectSelectNext20").modal("show");
					
				}
			
			
			} );
			}
	
	} );
	
	$( "#submit_academicna20" ).click( function ( event ) {
		
		event.preventDefault(); //incase button becomes submit form.
		
		
	
		//$selectedsub=$("#mySingleField").val();
		
		var subject=$('#subjectna20').val();
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'submit_data_ajaxna',
				pedagoge_callback_class: 'ControllerMemberdashboard',	
				data: $('#frm_submit_academicna20').serialize()+'&'+$.param({ 'mysubject': subject }),
				
			};
			if ( $("#frm_submit_academicna20").validationEngine('validate') ) {
			$.post( $ajax_url, submit_data, function ( response ) {
				
				$numberofsubjects=$("#subjectnum").val();
				alert("Data Submitted");
				
				if($numberofsubjects==20){
				
					var mystr=response.split("|");			
					$("#subjectSelectNext20").modal("hide");										
					$("#nameaddress").modal("show");	
					$("#studname").val(mystr[0]);
					$("#emailid").val(mystr[1]);
					$("#phonenum").val(mystr[2]);
					
					
					$("#myuserid").val(mystr[3]);
				
				}else{
					
					$("#subjectSelectNext20").modal("hide");
					$("#subjectSelectNext21").modal("show");
					
				}
			
			
			} );
			}
	
	} );

	
} );

		


