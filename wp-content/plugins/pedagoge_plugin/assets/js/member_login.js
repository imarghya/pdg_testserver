///////////////////////////////////////Login Form Processing Starts////////////////////////////////////////////////
/**
 *Login Form Processing
 */
 var base_url="http://navkiraninfotech.com/Clients/pdg";
$( document ).ready( function () {
	
	
	
	$( "#cmd_login" ).click( function ( event ) {
		
		
		event.preventDefault(); //incase button becomes submit form.
		
	
		
		var $button = $( this ),
			$txt_username = $( "#txt_username" ),
			$txt_password = $( "#txt_password" );
			
		var $nexturl = $( '#nexturl' ).val();
		
		
			var submit_data = {
				nonce: $ajaxnonce,
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'member_login_ajax',
				pedagoge_callback_class: 'ControllerMemberlogin',
				user_name: $txt_username.val(),
				pass_word: $txt_password.val(),
				
				nexturl: $nexturl,
				
			};
			
			$.post( $ajax_url, submit_data, function ( response ) {
				
				
				
				try {
					
					var $response_data = $.parseJSON( response ),
						$error = $response_data.error,
						$error_type = $response_data.error_type,
						$loggedin = $response_data.loggedin,
						$message = $response_data.message,
						$next_url = $response_data.url;
					
					//alert($response_data.userid);
					
					
					
					if ( $error ) {
						//handle login error
						$button.button( 'reset' );
						pdg_show_alerts( 'div_login_result', $message, 'error' );
						switch ( $error_type ) {
							case 'wrong_username':
								$txt_username.select().focus();
								break;
							case 'wrong_password':
								$txt_password.select().focus();
								break;
						}
						
					} else {
						if ( $loggedin == true ) {
							//var $loading_url = $response_data.url;
							if($response_data.userid=='2054')
							window.location.href = base_url + '/teacherdashboard';
							else if($response_data.userid=='1')
							window.location.href = base_url + '/adminpanel/';
							else 
							window.location.href = base_url + '/memberdashboard';
							
							/*setTimeout(function() {
							 location.reload(true);
							 },500);	*/
						}
					}
				} catch ( err ) {
					var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
					pdg_show_alerts( 'div_login_result', $message, 'error' );
					$button.button( 'reset' );
				} finally {
				
				}
				
			} ).complete( function () {
				//$button.button('reset');
			} );
		
	} );
} );
///////////////////////////////////////Login Form Processing Ends////////////////////////////////////////////////

