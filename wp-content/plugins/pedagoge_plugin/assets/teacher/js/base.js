
/* For menu Active */
$(document).ready(function () {
    $('.menu li a').click(function(e) {

        $('.menu li a').removeClass('menu_active');

        var $this = $(this);
        if (!$this.hasClass('menu_active')) {
            $this.addClass('menu_active');
        }
        //e.preventDefault();
    });
});
/* For menu active */


/* For Dropdown Menu */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Dropdown Menu */


/* For Filter Menu1 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction() {
    document.getElementById("filterFunctionDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter')) {

    var dropdownfilter = document.getElementsByClassName("dropdown-content-filter");
    var i;
    for (i = 0; i < dropdownfilter.length; i++) {
      var openDropdown = dropdownfilter[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu1 */

/* For Filter Menu2 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction1() {
    document.getElementById("filterFunctionDropdown1").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter1')) {

    var dropdownfilter1 = document.getElementsByClassName("dropdown-content-filter1");
    var i;
    for (i = 0; i < dropdownfilter1.length; i++) {
      var openDropdown = dropdownfilter1[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu2 */

/* For Filter Menu3 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction2() {
    document.getElementById("filterFunctionDropdown2").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter2')) {

    var dropdownfilter2 = document.getElementsByClassName("dropdown-content-filter2");
    var i;
    for (i = 0; i < dropdownfilter2.length; i++) {
      var openDropdown = dropdownfilter2[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu3 */

/* For Filter Menu4 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction3() {
    document.getElementById("filterFunctionDropdown3").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter3')) {

    var dropdownfilter3 = document.getElementsByClassName("dropdown-content-filter3");
    var i;
    for (i = 0; i < dropdownfilter3.length; i++) {
      var openDropdown = dropdownfilter3[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu4 */

/* For Filter Menu5 */

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function filterFunction4() {
    document.getElementById("filterFunctionDropdown4").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropdownfilter4')) {

    var dropdownfilter4 = document.getElementsByClassName("dropdown-content-filter4");
    var i;
    for (i = 0; i < dropdownfilter4.length; i++) {
      var openDropdown = dropdownfilter4[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/* For Filter Menu5 */

/* For Slim Scroll */

	/*if(screen.width < 767){
			$("#features-wrap p").slimScroll({destroy: true});
		}
		else
		{
			$(function(){
					$('#features-wrap p').slimScroll({
						height: '75px',
						size: '5px',
						wheelStep: 10
					});
				});
		}*/
		
		$(function(){
					$('.author_details').slimScroll({
						height: '1056px',
						size: '10px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						allowPageScroll: true,
						opacity: 1
					});
				});
				
/* For Teacher Dashbboard Preferences */				
				$(function(){
					$('.preference p').slimScroll({
						height: '140px',
						width: '100%',
						size: '5px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2e3847',
						allowPageScroll: true,
						opacity: 1
					});
				});
				
				$(function(){
					$('.interest p').slimScroll({
						height: '140px',
						width: '100%',
						size: '5px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2b7a74',
						allowPageScroll: true,
						opacity: 1
					});
				});
				
				$(function(){

					$('.head_notifacition').slimScroll({

						height: '300px',

						size: '5px',

						width:'100%',

						alwaysVisible: true,

						distance: '4px',

						color: '#2e3847',

						opacity: 1

					});

				});
				
				$(function(){

					$('.experience_scroll').slimScroll({
						height: '260px',
						size: '7px',
						width:'100%',
						alwaysVisible: true,
						distance: '0',
						color: '#33acdb',
						allowPageScroll: true,
						railVisible: true,
    					railColor: '#337087',
    					railOpacity: 1,
						wheelStep: 150,
						distance: '5px',
						opacity: 1
					});

				});
				
				$(function(){

					$('.fs-options').slimScroll({
						height: '260px',
						size: '7px',
						width:'100%',
						alwaysVisible: true,
						distance: '0',
						color: '#33acdb',
						allowPageScroll: true,
						railVisible: true,
    					railColor: '#337087',
    					railOpacity: 1,
						wheelStep: 150,
						distance: '5px',
						opacity: 1
					});

				});
				
				
/* For Teacher Dashbboard Preferences */
				
/* For Slim Scroll */

/* Custom Spin Button For Input type number */


    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fa fa-caret-up" aria-hidden="true"></i></div><div class="quantity-button quantity-down"><i class="fa fa-caret-down" aria-hidden="true"></i></div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });

/* Custom Spin Button For Input type number */


/* Child Model */

function myFunction() {
    var x = document.getElementById('myDIV');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

/* ChildModel */