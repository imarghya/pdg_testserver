  $( function() {
    var availableTags = [
      "Kolkata",
      "Mumbai",
      "Delhi",
      "Bangalore",
      "Hyderabad",
      "Ahmedabad ",
      "Chennai",
      "Surat",
      "Pune",
      "Lucknow",
      "Jaipur",
      "Nagpur",
      "Indore",
      "Thane",
      "Bhopal",
      "Visakhapatnam",
      "Pimpri & Chinchwad",
      "Patna",
      "Ludhiana",
      "Nashik",
      "Rajkot",
      "Dhanbad"
    ];
    $( "#tags" ).autocomplete({
      source: availableTags
    });
  } );
