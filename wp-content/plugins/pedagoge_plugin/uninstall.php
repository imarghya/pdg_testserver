<?php

/**
 * Fired when the plugin is uninstalled.
 * 
 * @link       http://www.pedagoge.com
 * @since      0.0.1
 *
 * @package    pedagoge_plugin
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
