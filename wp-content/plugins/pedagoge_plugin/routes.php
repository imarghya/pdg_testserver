<?php 
$routes = array (
	'/signup',//public profiles (does not require login)
	'/register',//public profiles (does not require login)
	'/login',//public profiles (does not require login)
	'/reset',
	'/activate',//public profiles (does not require login)
	'/profile',
	'/search',//public profiles (does not require login)
	'/teacher',    //public profiles (does not require login)
	'/institute',//public profiles (does not require login)
	'/trp',//Teacher Referral Programme
	'/reviews',//Reviews Accept and Reject
	'/dashboard',
	'/trainer',//public profiles (does not require login)
	'/seo',
	'/edit',
	'/manageworkshop',
	'/workshop', // for workshop
	'/settings', // for test
	'/memberlogin',//for student dashboard
	'/memberdashboard',//for member dashboard
	'/archived',//archived tab
	'/past',//past tab
	'/teacherdashboard',//for teacher dashboard
        '/emailverification',//for email verification
	'/saved',//for saved tab in teacher dashboard
	'/interested',//for interested tab in teacher dashboard
	'/referrals',//referral tab in teacher dashboard
	'/tarchived',//archived tab in teacher dashboard
	'/match',//match tab in teacher dashboard
	'/adminpanel',//admin panel
	'/viewquery',//view query
	'/activitylog',//activity log
	'/logout',//activity log
);
?>