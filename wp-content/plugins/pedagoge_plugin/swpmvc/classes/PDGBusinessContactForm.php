<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * This Controller class handles all the request callbacks/Contact Forms ajax actions.
 * 
 * @author Ganesh
 */
class PDGBusinessContactForm {

	public static function business_form_ajax() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";
		if ( ! isset( $_POST['formNumber'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}
		$formNumber = sanitize_text_field( $_POST['formNumber'] );

		if ( $formNumber == 1 ) {
			$contact_companyName_value = sanitize_text_field( $_POST['contact_companyName_value'] );
			$contact_name_value = sanitize_text_field( $_POST['contact_name_value'] );
			$contact_email_value = sanitize_text_field( $_POST['contact_email_value'] );
			$contact_number_value = sanitize_text_field( $_POST['contact_number_value'] );
			$contact_designation_value = sanitize_text_field( $_POST['contact_designation_value'] );

			$hnpt_value = '';
			if ( isset( $_POST['hnpt_val'] ) ) {
				$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
			}

			if ( strlen( $hnpt_value ) > 0 ) {
				$return_message['message'] = $formErrorMsg;
				echo json_encode( $return_message );
				die();
			}

			if ( empty( $contact_name_value ) || empty( $contact_email_value ) || empty( $contact_number_value ) || empty( $contact_companyName_value ) ) {
				$return_message['message'] = $formErrorMsg;
				echo json_encode( $return_message );
				die();
			}

			if ( ! filter_var( $contact_email_value, FILTER_VALIDATE_EMAIL ) === true ) {
				$return_message['message'] = 'Email Address is invalid';
				echo json_encode( $return_message );
				die();
			}

			if ( ! preg_match( "/^[0-9]+$/", $contact_number_value ) ) {
				$return_message['message'] = 'Phone no. is invalid';
				echo json_encode( $return_message );
				die();
			}

			$template_vars['contact_name'] = $contact_name_value;
			$template_vars['contact_email'] = $contact_email_value;
			$template_vars['contact_number'] = $contact_number_value;
			$template_vars['contact_designation'] = $contact_designation_value;
			$template_vars['contact_companyName'] = $contact_companyName_value;

			$templateForm = "business_contact_form";
			$emailSubject = "Business response";
		} elseif ( $formNumber == 2 ) {
			$contact_name_value = sanitize_text_field( $_POST['contact_name_value'] );
			$contact_email_value = sanitize_text_field( $_POST['contact_email_value'] );
			$contact_number_value = sanitize_text_field( $_POST['contact_number_value'] );

			$hnpt_value = '';
			if ( isset( $_POST['hnpt_val'] ) ) {
				$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
			}

			if ( strlen( $hnpt_value ) > 0 ) {
				$return_message['message'] = $formErrorMsg;
				echo json_encode( $return_message );
				die();
			}

			if ( empty( $contact_name_value ) || empty( $contact_email_value ) || empty( $contact_number_value ) ) {
				$return_message['message'] = $formErrorMsg;
				echo json_encode( $return_message );
				die();
			}

			if ( ! filter_var( $contact_email_value, FILTER_VALIDATE_EMAIL ) === true ) {
				$return_message['message'] = ' Email Address is invalid';
				echo json_encode( $return_message );
				die();
			}

			if ( ! preg_match( "/^[0-9]+$/", $contact_number_value ) ) {
				$return_message['message'] = ' Phone no. is invalid';
				echo json_encode( $return_message );
				die();
			}

			$template_vars['contact_name'] = $contact_name_value;
			$template_vars['contact_email'] = $contact_email_value;
			$template_vars['contact_number'] = $contact_number_value;
			$templateForm = "trainer_business_contact_form";
			$emailSubject = "Trainer response";
		} else {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$activation_email_header = PDGViewLoader::load_view( 'email_templates/email_header' );
		$activation_email_content = PDGViewLoader::load_view( 'email_templates/' . $templateForm, $template_vars );
		$activation_email_footer = PDGViewLoader::load_view( 'email_templates/email_footer' );

		$email_content = $activation_email_header . $activation_email_content . $activation_email_footer;

		$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
		$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

		add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html"; ' ) );
		wp_mail( 'business@pedagoge.com', $emailSubject, $email_content, $headers );
		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for your requirement, we shall get back to you soon!';

		echo json_encode( $return_message );
		die();
	}

	public static function landingjul09_ajax() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		$contact_name_value = sanitize_text_field( $_POST['contact_name_value'] );
		$contact_email_value = sanitize_text_field( $_POST['contact_email_value'] );
		$contact_number_value = sanitize_text_field( $_POST['contact_number_value'] );
		$contact_requirement_value = sanitize_text_field( $_POST['contact_requirement_value'] );

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $contact_name_value ) || empty( $contact_email_value ) || empty( $contact_number_value ) || empty( $contact_requirement_value ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( ! filter_var( $contact_email_value, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9]+$/", $contact_number_value ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['contact_name'] = $contact_name_value;
		$template_vars['contact_email'] = $contact_email_value;
		$template_vars['contact_number'] = $contact_number_value;
		$template_vars['contact_requirement'] = $contact_requirement_value;

		$emailSubject = "Business response";


		$activation_email_header = PDGViewLoader::load_view( 'email_templates/email_header' );
		$activation_email_content = PDGViewLoader::load_view( 'email_templates/landingjul09_contact_form', $template_vars );
		$activation_email_footer = PDGViewLoader::load_view( 'email_templates/email_footer' );

		$email_content = $activation_email_header . $activation_email_content . $activation_email_footer;

		$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
		$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

		add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html"; ' ) );
		wp_mail( 'hello@pedagoge.com', $emailSubject, $email_content, $headers );
		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for your requirement, we shall get back to you soon!';

		echo json_encode( $return_message );
		die();
	}

	public static function thankyoucallback_ajax() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		$reviewComment_value = sanitize_text_field( $_POST['reviewComment_value'] );
		$rating_value = sanitize_text_field( $_POST['rating_value'] );
		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $reviewComment_value ) || empty( $rating_value ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[1-5]+$/", $rating_value ) ) {
			$return_message['message'] = 'Rating is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['reviewComment'] = $reviewComment_value;
		$template_vars['rating'] = $rating_value;

		$emailSubject = "Payment callback page response";


		$activation_email_header = PDGViewLoader::load_view( 'email_templates/email_header' );
		$activation_email_content = PDGViewLoader::load_view( 'email_templates/paymentcallback_contact_form', $template_vars );
		$activation_email_footer = PDGViewLoader::load_view( 'email_templates/email_footer' );
		$email_content = $activation_email_header . $activation_email_content . $activation_email_footer;

		$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
		$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

		add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html"; ' ) );
		wp_mail( 'hello@pedagoge.com', $emailSubject, $email_content, $headers );
		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for your review!';

		echo json_encode( $return_message );
		die();
	}

	public static function home_callback() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		if ( ! isset( $_POST['searchData'] ) || ! is_array( $_POST['searchData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$search_data = $_POST['searchData'];

		$subject = isset( $search_data['subject'] ) && ! is_array( $search_data['subject'] ) ? sanitize_text_field( trim( $search_data['subject'] ) ) : '';
		$name = isset( $search_data['name'] ) && ! is_array( $search_data['name'] ) ? sanitize_text_field( trim( $search_data['name'] ) ) : '';
		$email = isset( $search_data['email'] ) && ! is_array( $search_data['email'] ) ? sanitize_text_field( trim( $search_data['email'] ) ) : '';
		$phone = isset( $search_data['phone'] ) && ! is_array( $search_data['phone'] ) ? sanitize_text_field( trim( $search_data['phone'] ) ) : '';
		$tuition_location = isset( $search_data['tuition_location'] ) && ! is_array( $search_data['tuition_location'] ) ? sanitize_text_field( trim( $search_data['tuition_location'] ) ) : '';
		$locality = isset( $search_data['locality'] ) && ! is_array( $search_data['locality'] ) ? sanitize_text_field( trim( $search_data['locality'] ) ) : '';

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $name ) || empty( $subject ) || empty( $phone ) || empty( $tuition_location ) || empty( $locality ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $email ) != "" && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['name'] = $name;
		$template_vars['email'] = $email;
		$template_vars['phone'] = $phone;
		$template_vars['tuition_location'] = $tuition_location;
		$template_vars['locality'] = $locality;
		$template_vars['subject'] = $subject;

		$mail_data = array (
			'to_address'    => 'showinterest@pedagoge.com',
			'mail_type'     => 'home_request_callback_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';
		$return_message['url'] = home_url('/thank-you/');

		echo json_encode( $return_message );
		die();
	}

	public static function home_contactus_form() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		if ( ! isset( $_POST['searchData'] ) || ! is_array( $_POST['searchData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$search_data = $_POST['searchData'];

		$subject = isset( $search_data['subject'] ) && ! is_array( $search_data['subject'] ) ? sanitize_text_field( trim( $search_data['subject'] ) ) : '';
		$name = isset( $search_data['name'] ) && ! is_array( $search_data['name'] ) ? sanitize_text_field( trim( $search_data['name'] ) ) : '';
		$phone = isset( $search_data['phone'] ) && ! is_array( $search_data['phone'] ) ? sanitize_text_field( trim( $search_data['phone'] ) ) : '';
		$locality = isset( $search_data['locality'] ) && ! is_array( $search_data['locality'] ) ? sanitize_text_field( trim( $search_data['locality'] ) ) : '';

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $name ) || empty( $subject ) || empty( $phone ) || empty( $locality ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['name'] = $name;
		$template_vars['phone'] = $phone;
		$template_vars['locality'] = $locality;
		$template_vars['subject'] = $subject;

		$mail_data = array (
			'to_address'    => 'hello@pedagoge.com',
			'mail_type'     => 'homepage_contact_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';

		echo json_encode( $return_message );
		die();
	}

	public static function search_callback() {

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		if ( ! isset( $_POST['searchData'] ) || ! is_array( $_POST['searchData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$search_data = $_POST['searchData'];

		$pageurl = isset( $search_data['pageurl'] ) && ! is_array( $search_data['pageurl'] ) ? sanitize_text_field( trim( $search_data['pageurl'] ) ) : '';
		$teachername = isset( $search_data['teachername'] ) && ! is_array( $search_data['teachername'] ) ? sanitize_text_field( trim( $search_data['teachername'] ) ) : '';
		$subject = isset( $search_data['subject'] ) && ! is_array( $search_data['subject'] ) ? sanitize_text_field( trim( $search_data['subject'] ) ) : '';
		$name = isset( $search_data['name'] ) && ! is_array( $search_data['name'] ) ? sanitize_text_field( trim( $search_data['name'] ) ) : '';
		$email = isset( $search_data['email'] ) && ! is_array( $search_data['email'] ) ? sanitize_text_field( trim( $search_data['email'] ) ) : '';
		$phone = isset( $search_data['phone'] ) && ! is_array( $search_data['phone'] ) ? sanitize_text_field( trim( $search_data['phone'] ) ) : '';
		$tuition_location = isset( $search_data['tuition_location'] ) && ! is_array( $search_data['tuition_location'] ) ? sanitize_text_field( trim( $search_data['tuition_location'] ) ) : '';
		$locality = isset( $search_data['locality'] ) && ! is_array( $search_data['locality'] ) ? sanitize_text_field( trim( $search_data['locality'] ) ) : '';
		$pref_mode_comm_array = isset( $search_data['pref_mode_comm_array'] ) && is_array( $search_data['pref_mode_comm_array'] ) ? $search_data['pref_mode_comm_array'] : array ();
		$pref_time_comm_array = isset( $search_data['pref_time_comm_array'] ) && is_array( $search_data['pref_time_comm_array'] ) ? $search_data['pref_time_comm_array'] : array ();

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $pageurl ) || empty( $teachername ) || empty( $name ) || empty( $subject ) || empty( $phone ) || empty( $tuition_location ) || empty( $locality ) || empty( $pref_mode_comm_array ) || empty( $pref_time_comm_array ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $email ) != "" && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$pref_communication_mode = implode( ", ", $pref_mode_comm_array );
		$pref_communication_time = '';

		foreach ( $pref_time_comm_array as $time ) {
			switch ( $time ) {
				case 1:
					$pref_communication_time .= '10 - 12 PM, ';
					break;
				case 2:
					$pref_communication_time .= '12 - 3 PM, ';
					break;
				case 3:
					$pref_communication_time .= '3 - 6 PM, ';
					break;
				case 4:
					$pref_communication_time .= '6 - 9 PM, ';
					break;
				default:
					break;
			}
		}

		$template_vars['pageurl'] = $pageurl;
		$template_vars['teachername'] = $teachername;
		$template_vars['name'] = $name;
		$template_vars['email'] = $email;
		$template_vars['phone'] = $phone;
		$template_vars['tuition_location'] = $tuition_location;
		$template_vars['locality'] = $locality;
		$template_vars['subject'] = $subject;
		$template_vars['pref_communication_mode'] = $pref_communication_mode;
		$template_vars['pref_communication_time'] = $pref_communication_time;

		$mail_data = array (
			'to_address'    => 'showinterest@pedagoge.com',
			'mail_type'     => 'search_callback_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';
		$return_message['url'] = home_url('/thank-you/');

		echo json_encode( $return_message );
		die();
	}

	public static function profile_callback_form() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		if ( ! isset( $_POST['searchData'] ) || ! is_array( $_POST['searchData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$search_data = $_POST['searchData'];

		$pageurl = isset( $search_data['pageurl'] ) && ! is_array( $search_data['pageurl'] ) ? sanitize_text_field( trim( $search_data['pageurl'] ) ) : '';
		$teachername = isset( $search_data['teachername'] ) && ! is_array( $search_data['teachername'] ) ? sanitize_text_field( trim( $search_data['teachername'] ) ) : '';
		$subject = isset( $search_data['subject'] ) && ! is_array( $search_data['subject'] ) ? sanitize_text_field( trim( $search_data['subject'] ) ) : '';
		$name = isset( $search_data['name'] ) && ! is_array( $search_data['name'] ) ? sanitize_text_field( trim( $search_data['name'] ) ) : '';
		$email = isset( $search_data['email'] ) && ! is_array( $search_data['email'] ) ? sanitize_text_field( trim( $search_data['email'] ) ) : '';
		$phone = isset( $search_data['phone'] ) && ! is_array( $search_data['phone'] ) ? sanitize_text_field( trim( $search_data['phone'] ) ) : '';
		$tuition_location = isset( $search_data['tuition_location'] ) && ! is_array( $search_data['tuition_location'] ) ? sanitize_text_field( trim( $search_data['tuition_location'] ) ) : '';
		$locality = isset( $search_data['locality'] ) && ! is_array( $search_data['locality'] ) ? sanitize_text_field( trim( $search_data['locality'] ) ) : '';
		$pref_mode_comm_array = isset( $search_data['pref_mode_comm_array'] ) && is_array( $search_data['pref_mode_comm_array'] ) ? $search_data['pref_mode_comm_array'] : array ();
		$pref_time_comm_array = isset( $search_data['pref_time_comm_array'] ) && is_array( $search_data['pref_time_comm_array'] ) ? $search_data['pref_time_comm_array'] : array ();

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $pageurl ) || empty( $teachername ) || empty( $name ) || empty( $subject ) || empty( $phone ) || empty( $tuition_location ) || empty( $locality ) || empty( $pref_mode_comm_array ) || empty( $pref_time_comm_array ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $email ) != "" && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$pref_communication_mode = implode( ", ", $pref_mode_comm_array );
		$pref_communication_time = '';

		foreach ( $pref_time_comm_array as $time ) {
			switch ( $time ) {
				case 1:
					$pref_communication_time .= '10 - 12 PM, ';
					break;
				case 2:
					$pref_communication_time .= '12 - 3 PM, ';
					break;
				case 3:
					$pref_communication_time .= '3 - 6 PM, ';
					break;
				case 4:
					$pref_communication_time .= '6 - 9 PM, ';
					break;
				default:
					break;
			}
		}

		$template_vars['pageurl'] = $pageurl;
		$template_vars['teachername'] = $teachername;
		$template_vars['name'] = $name;
		$template_vars['email'] = $email;
		$template_vars['phone'] = $phone;
		$template_vars['tuition_location'] = $tuition_location;
		$template_vars['locality'] = $locality;
		$template_vars['subject'] = $subject;
		$template_vars['pref_communication_mode'] = $pref_communication_mode;
		$template_vars['pref_communication_time'] = $pref_communication_time;

		$mail_data = array (
			'to_address'    => 'showinterest@pedagoge.com',
			'mail_type'     => 'profile_callback_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';

		echo json_encode( $return_message );
		die();
	}

	public static function profile_takeademo_callback() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";
		if ( ! isset( $_POST['searchData'] ) || ! is_array( $_POST['searchData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$search_data = $_POST['searchData'];

		$pageurl = isset( $search_data['pageurl'] ) && ! is_array( $search_data['pageurl'] ) ? sanitize_text_field( trim( $search_data['pageurl'] ) ) : '';
		$teachername = isset( $search_data['teachername'] ) && ! is_array( $search_data['teachername'] ) ? sanitize_text_field( trim( $search_data['teachername'] ) ) : '';
		$subject = isset( $search_data['subject'] ) && ! is_array( $search_data['subject'] ) ? sanitize_text_field( trim( $search_data['subject'] ) ) : '';
		$name = isset( $search_data['name'] ) && ! is_array( $search_data['name'] ) ? sanitize_text_field( trim( $search_data['name'] ) ) : '';
		$email = isset( $search_data['email'] ) && ! is_array( $search_data['email'] ) ? sanitize_text_field( trim( $search_data['email'] ) ) : '';
		$phone = isset( $search_data['phone'] ) && ! is_array( $search_data['phone'] ) ? sanitize_text_field( trim( $search_data['phone'] ) ) : '';
		$tuition_location = isset( $search_data['tuition_location'] ) && ! is_array( $search_data['tuition_location'] ) ? sanitize_text_field( trim( $search_data['tuition_location'] ) ) : '';
		$locality = isset( $search_data['locality'] ) && ! is_array( $search_data['locality'] ) ? sanitize_text_field( trim( $search_data['locality'] ) ) : '';

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $pageurl ) || empty( $teachername ) || empty( $name ) || empty( $subject ) || empty( $phone ) || empty( $tuition_location ) || empty( $locality ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $email ) != "" && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['pageurl'] = $pageurl;
		$template_vars['teachername'] = $teachername;
		$template_vars['name'] = $name;
		$template_vars['email'] = $email;
		$template_vars['phone'] = $phone;
		$template_vars['tuition_location'] = $tuition_location;
		$template_vars['locality'] = $locality;
		$template_vars['subject'] = $subject;

		$mail_data = array (
			'to_address'    => 'showinterest@pedagoge.com',
			'mail_type'     => 'profile_demo_callback_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';

		echo json_encode( $return_message );
		die();
	}

	public static function profile_takethiscourse_callback() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";
		if ( ! isset( $_POST['searchData'] ) || ! is_array( $_POST['searchData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$search_data = $_POST['searchData'];

		$pageurl = isset( $search_data['pageurl'] ) && ! is_array( $search_data['pageurl'] ) ? sanitize_text_field( trim( $search_data['pageurl'] ) ) : '';
		$teachername = isset( $search_data['teachername'] ) && ! is_array( $search_data['teachername'] ) ? sanitize_text_field( trim( $search_data['teachername'] ) ) : '';
		$subject = isset( $search_data['subject'] ) && ! is_array( $search_data['subject'] ) ? sanitize_text_field( trim( $search_data['subject'] ) ) : '';
		$fees = isset( $search_data['fees'] ) && ! is_array( $search_data['fees'] ) ? sanitize_text_field( trim( $search_data['fees'] ) ) : '';
		$batchsize = isset( $search_data['batchsize'] ) && ! is_array( $search_data['batchsize'] ) ? sanitize_text_field( trim( $search_data['batchsize'] ) ) : '';
		$length = isset( $search_data['length'] ) && ! is_array( $search_data['length'] ) ? sanitize_text_field( trim( $search_data['length'] ) ) : '';
		$time = isset( $search_data['time'] ) && ! is_array( $search_data['time'] ) ? sanitize_text_field( trim( $search_data['time'] ) ) : '';
		$name = isset( $search_data['name'] ) && ! is_array( $search_data['name'] ) ? sanitize_text_field( trim( $search_data['name'] ) ) : '';
		$email = isset( $search_data['email'] ) && ! is_array( $search_data['email'] ) ? sanitize_text_field( trim( $search_data['email'] ) ) : '';
		$phone = isset( $search_data['phone'] ) && ! is_array( $search_data['phone'] ) ? sanitize_text_field( trim( $search_data['phone'] ) ) : '';
		$tuition_location = isset( $search_data['tuition_location'] ) && ! is_array( $search_data['tuition_location'] ) ? sanitize_text_field( trim( $search_data['tuition_location'] ) ) : '';
		$locality = isset( $search_data['locality'] ) && ! is_array( $search_data['locality'] ) ? sanitize_text_field( trim( $search_data['locality'] ) ) : '';

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $name ) || empty( $phone ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $email ) != "" && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['pageurl'] = $pageurl;
		$template_vars['teachername'] = $teachername;
		$template_vars['name'] = $name;
		$template_vars['email'] = $email;
		$template_vars['phone'] = $phone;
		$template_vars['tuition_location'] = $tuition_location;
		$template_vars['locality'] = $locality;
		$template_vars['subject'] = $subject;
		$template_vars['fees'] = $fees;
		$template_vars['batchsize'] = $batchsize;
		$template_vars['length'] = $length;
		$template_vars['time'] = $time;

		$mail_data = array (
			'to_address'    => 'showinterest@pedagoge.com',
			'mail_type'     => 'profile_take_this_course_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';

		echo json_encode( $return_message );
		die();
	}

	public static function institution_callback_form() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		if ( ! isset( $_POST['contactData'] ) || ! is_array( $_POST['contactData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$contact_data = $_POST['contactData'];

		$fullname = isset( $contact_data['fullname'] ) && ! is_array( $contact_data['fullname'] ) ? sanitize_text_field( trim( $contact_data['fullname'] ) ) : '';
		$inst_school_name = isset( $contact_data['inst_school_name'] ) && ! is_array( $contact_data['inst_school_name'] ) ? sanitize_text_field( trim( $contact_data['inst_school_name'] ) ) : '';
		$phoneno = isset( $contact_data['phoneno'] ) && ! is_array( $contact_data['phoneno'] ) ? sanitize_text_field( trim( $contact_data['phoneno'] ) ) : '';
		$designation = isset( $contact_data['designation'] ) && ! is_array( $contact_data['designation'] ) ? sanitize_text_field( trim( $contact_data['designation'] ) ) : '';
		$emailid = isset( $contact_data['emailid'] ) && ! is_array( $contact_data['emailid'] ) ? sanitize_text_field( trim( $contact_data['emailid'] ) ) : '';

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $fullname ) || empty( $inst_school_name ) || empty( $phoneno ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $emailid ) != "" && ! filter_var( $emailid, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phoneno ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['fullname'] = $fullname;
		$template_vars['phoneno'] = $phoneno;
		$template_vars['designation'] = $designation;
		$template_vars['inst_school_name'] = $inst_school_name;
		$template_vars['emailid'] = $emailid;

		$mail_data = array (
			'to_address'    => 'Institution@pedagoge.com',
			'mail_type'     => 'institution_callback_form_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';

		echo json_encode( $return_message );
		die();
	}

	public static function register_as_trainer_modal_callback() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		unset( $template_vars );
		$template_vars = array ();
		$formErrorMsg = "Invalid request. Try again!";

		if ( ! isset( $_POST['contactData'] ) || ! is_array( $_POST['contactData'] ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		$contact_data = $_POST['contactData'];

		$name = isset( $contact_data['name'] ) && ! is_array( $contact_data['name'] ) ? sanitize_text_field( trim( $contact_data['name'] ) ) : '';
		$email = isset( $contact_data['email'] ) && ! is_array( $contact_data['email'] ) ? sanitize_text_field( trim( $contact_data['email'] ) ) : '';
		$phone = isset( $contact_data['phone'] ) && ! is_array( $contact_data['phone'] ) ? sanitize_text_field( trim( $contact_data['phone'] ) ) : '';

		$hnpt_value = '';
		if ( isset( $_POST['hnpt_val'] ) ) {
			$hnpt_value = sanitize_text_field( $_POST['hnpt_val'] );
		}

		if ( strlen( $hnpt_value ) > 0 ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( empty( $name ) || empty( $phone ) || empty( $email ) ) {
			$return_message['message'] = $formErrorMsg;
			echo json_encode( $return_message );
			die();
		}

		if ( trim( $email ) != "" && ! filter_var( $email, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Email Address is invalid';
			echo json_encode( $return_message );
			die();
		}

		if ( ! preg_match( "/^[0-9 -]+$/", $phone ) ) {
			$return_message['message'] = 'Phone no. is invalid';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['name'] = $name;
		$template_vars['email'] = $email;
		$template_vars['phone'] = $phone;

		$mail_data = array (
			'to_address'    => 'institution@pedagoge.com',
			'mail_type'     => 'institution_register_as_trainer_callback_form_to_admin',
			'template_vars' => $template_vars,
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for showing interest. Our team is working for you and will get in touch with you shortly!';

		echo json_encode( $return_message );
		die();
	}

	public static function fn_load_secret_key() {
		echo wp_create_nonce( "pedagoge" );
		die();
	}

}