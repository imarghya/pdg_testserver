<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PDGSearchContent {
	public function __construct() {

	}

	public static function fn_initial_content( $data_to_process ) {
		global $wpdb;

		$sorry_img = '<img src="' . PEDAGOGE_ASSETS_URL . '/images/error.png" width="10px" height="10px"/>';

		$loggedin_user_id = $data_to_process['loggedin_user_id'];
		$loggedin_user_role = $data_to_process['loggedin_user_role'];
		$start_from = $data_to_process['start_from'];
		$no_of_records_per_page = $data_to_process['no_of_records_per_page'];
		$pagination_no = isset( $data_to_process['pagination_no'] ) ? $data_to_process['pagination_no'] : 1;
		$sort_exp_array = isset( $data_to_process['sort_exp_array'] ) && is_array( $data_to_process['sort_exp_array'] ) ? $data_to_process['sort_exp_array'] : array ();
		$sort_reviews_array = isset( $data_to_process['sort_reviews_array'] ) && is_array( $data_to_process['sort_reviews_array'] ) ? $data_to_process['sort_reviews_array'] : array ();
		$sort_fee_array = isset( $data_to_process['sort_fee_array'] ) && is_array( $data_to_process['sort_fee_array'] ) ? $data_to_process['sort_fee_array'] : array ();

		$sort_exp = isset( $data_to_process['sort_exp_array'][0] ) ? $data_to_process['sort_exp_array'][0] : 9999;//9999 for no input recieved from checkboxes
		$sort_reviews = isset( $data_to_process['sort_reviews_array'][0] ) ? $data_to_process['sort_reviews_array'][0] : 9999;//9999 for no input recieved from checkboxes
		$sort_fee = isset( $data_to_process['sort_fee_array'][0] ) ? $data_to_process['sort_fee_array'][0] : 9999;//9999 for no input recieved from checkboxes

		$str_fees_amount_condition = "";
		$sort_exp_weight_condition = "";
		switch ( (int) $sort_exp ) {
			case 9999:
				$sort_exp = '';
				break;
			case 0:
				$sort_exp = 'asc';
				$sort_exp_weight_condition = "xp_weight $sort_exp,";
				break;
			case 1:
			default:
				$sort_exp = 'desc';
				$sort_exp_weight_condition = "xp_weight $sort_exp,";
				break;
		}

		switch ( (int) $sort_reviews ) {
			case 0:
				$sort_reviews = 'asc';
				break;
			case 9999:
			case 1:
			default:
				$sort_reviews = 'desc';
				break;
		}

		switch ( (int) $sort_fee ) {
			case 9999:
				$str_fees_amount_condition = "";
				break;
			case 0:
				$sort_fee = 'asc';
				$str_fees_amount_condition = "fees_amount $sort_fee,";
				break;
			case 1:
			default:
				$sort_fee = 'desc';
				$str_fees_amount_condition = "fees_amount $sort_fee,";
				break;
		}

		if ( ! is_numeric( $pagination_no ) || $pagination_no <= 0 ) {
			$pagination_no = 1;
		}
		$filter_institute_teacher = '';
		if ( isset( $data_to_process['filter_institute_teacher'] ) ) {
			$filter_institute_teacher = $data_to_process['filter_institute_teacher'];
		}
		$search_result = array ();

		$found_user_id = array ();
		$total_search_count = 0;

		if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'teacher' ) {
			$str_sql = "
				select count(teacher_id) from pdg_teacher where 
				approved = 'yes' and
				active = 'yes' and
				deleted = 'no'
			";
			$teacher_count = $wpdb->get_var( $str_sql );
			if ( ! empty( $teacher_count ) || is_numeric( $teacher_count ) ) {
				$total_search_count += $teacher_count;
			}
		}

		if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'institution' ) {
			$str_sql = "
				select count(institute_id) from pdg_institutes where 
				approved = 'yes' and
				active = 'yes' and
				deleted = 'no'
			";
			$institute_count = $wpdb->get_var( $str_sql );
			if ( ! empty( $institute_count ) || is_numeric( $institute_count ) ) {
				$total_search_count += $institute_count;
			}
		}

		$pagination_array = array (
			'total_found_records' => $total_search_count,
			'page_no'             => $pagination_no
		);
		$str_pagination = self::fn_return_pagination_html( $pagination_array );

		$str_search_sql = '';
		switch ( $filter_institute_teacher ) {
			case 'teacher':
				$str_search_sql = "					
					select 
					pdg_teacher.user_id
					from pdg_teacher 
					left join view_pdg_review_average on pdg_teacher.user_id = view_pdg_review_average.tutor_institute_user_id
					where
					pdg_teacher.approved = 'yes' and
					pdg_teacher.active = 'yes' and
					pdg_teacher.deleted = 'no' 
					order by view_pdg_review_average.avg_rating $sort_reviews, $str_fees_amount_condition $sort_exp_weight_condition view_pdg_review_average.total_count desc
					limit $start_from, $no_of_records_per_page
				";
				break;
			case 'institution':
				$str_search_sql = "
					select user_id from pdg_institutes 
					left join view_pdg_review_average on pdg_institutes.user_id = view_pdg_review_average.tutor_institute_user_id
					where
					approved = 'yes' and
					active = 'yes' and
					deleted = 'no' 
					order by view_pdg_review_average.avg_rating $sort_reviews, $str_fees_amount_condition $sort_exp_weight_condition view_pdg_review_average.total_count desc
					limit $start_from, $no_of_records_per_page
				";
				break;
			default:
				$str_search_sql = "
					select user_id from view_teacher_institute_combined where
					approved = 'yes' and
					active = 'yes' and
					profile_deleted = 'no' 
					order by avg_rating $sort_reviews, $str_fees_amount_condition $sort_exp_weight_condition total_count desc
					limit $start_from, $no_of_records_per_page
				";
				break;
		}

		$found_teachers_institutes = $wpdb->get_results( $str_search_sql );

		if ( ! empty( $found_teachers_institutes ) ) {
			foreach ( $found_teachers_institutes as $found_data ) {
				$user_id = $found_data->user_id;
				$found_user_id[] = $user_id;
			}
		}

		if ( ! empty( $found_user_id ) ) {

			$data_to_process = array (
				'sort_exp_array'         => $sort_exp_array,
				'sort_fee_array'         => $sort_fee_array,
				'sort_reviews_array'     => $sort_reviews_array,
				'found_users'            => $found_user_id,
				'start_from'             => 0,
				'loggedin_user_id'       => $loggedin_user_id,
				'loggedin_user_role'     => $loggedin_user_role,
				'no_of_records_per_page' => $no_of_records_per_page,
			);

			$search_result = self::fn_return_search_result_html( $data_to_process );
		}

		return $search_result;
	}

	public static function fn_return_search_result_html( $data_to_process ) {
		global $wpdb;
		$found_users_id_array = $data_to_process['found_users'];
		$start_from = $data_to_process['start_from'];
		$loggedin_user_id = $data_to_process['loggedin_user_id'];
		$loggedin_user_role = $data_to_process['loggedin_user_role'];
		$no_of_records_per_page = $data_to_process['no_of_records_per_page'];

		$sort_exp = isset( $data_to_process['sort_exp_array'][0] ) ? $data_to_process['sort_exp_array'][0] : 9999;//9999 for no input recieved from checkboxes
		$sort_reviews = isset( $data_to_process['sort_reviews_array'][0] ) ? $data_to_process['sort_reviews_array'][0] : 9999;//9999 for no input recieved from checkboxes
		$sort_fee = isset( $data_to_process['sort_fee_array'][0] ) ? $data_to_process['sort_fee_array'][0] : 9999;//9999 for no input recieved from checkboxes

		$str_fees_amount_condition = "";
		$sort_exp_weight_condition = "";
		switch ( (int) $sort_exp ) {
			case 9999:
				$sort_exp = '';
				break;
			case 0:
				$sort_exp = 'asc';
				$sort_exp_weight_condition = "xp_weight $sort_exp,";
				break;
			case 1:
			default:
				$sort_exp = 'desc';
				$sort_exp_weight_condition = "xp_weight $sort_exp,";
				break;
		}

		switch ( (int) $sort_reviews ) {
			case 0:
				$sort_reviews = 'asc';
				break;
			case 9999:
			case 1:
			default:
				$sort_reviews = 'desc';
				break;
		}

		switch ( (int) $sort_fee ) {
			case 9999:
				$str_fees_amount_condition = "";
				break;
			case 0:
				$sort_fee = 'asc';
				$str_fees_amount_condition = "fees_amount $sort_fee,";
				break;
			case 1:
			default:
				$sort_fee = 'desc';
				$str_fees_amount_condition = "fees_amount $sort_fee,";
				break;
		}

		$str_found_user_id_array = implode( ', ', $found_users_id_array );

		$return_array = array (
			'result'     => '',
			'pagination' => '',
		);

		$total_found_records = count( $found_users_id_array );

		$is_current_user_allowed_to_review = false;
		switch ( $loggedin_user_role ) {
			case 'student':
				$is_current_user_allowed_to_review = true;
				break;
			case 'guardian':
				$is_current_user_allowed_to_review = true;
				break;
		}

		$recommendations_sql = "select * from pdg_recommendations where recommended_to in ($str_found_user_id_array)";
		$recommend_data = $wpdb->get_results( $recommendations_sql );

		$ratings_sql = "select * from pdg_reviews where tutor_institute_user_id in ($str_found_user_id_array)";
		$rating_data = $wpdb->get_results( $ratings_sql );

		$str_search_sql = "
			select * from view_teacher_institute_combined where
			user_id in ($str_found_user_id_array) and
			approved = 'yes' and
			active = 'yes' and
			profile_deleted = 'no' 
			order by avg_rating $sort_reviews, $str_fees_amount_condition $sort_exp_weight_condition total_count desc 
			LIMIT $start_from, $no_of_records_per_page
		";

		$teacher_array = array ();
		$institute_array = array ();

		$found_teachers_institutes = $wpdb->get_results( $str_search_sql );

		foreach ( $found_teachers_institutes as $found_data ) {
			$teacher_insti_id = $found_data->teach_insti_id;
			$user_role = $found_data->user_role_name;

			switch ( $user_role ) {
				case 'teacher':
					$teacher_array[] = $teacher_insti_id;
					break;
				case 'institution':
					$institute_array[] = $teacher_insti_id;
					break;
			}
		}

		$locations_sql = "select locality, tutor_institute_user_id, tuition_location_type from view_pdg_tutor_location where `tutor_institute_user_id` in ($str_found_user_id_array)";
		$locations_sql1 = "select locality, tutor_institute_user_id, tuition_location_type from pdg_search_tutor_location where `tutor_institute_user_id` in ($str_found_user_id_array)";

		$subjects_sql = "select tutor_institute_user_id, subject_name from view_pdg_batch_subject where `tutor_institute_user_id` in ($str_found_user_id_array)";
		$subjects_sql1 = "select tutor_institute_user_id, subject_name from pdg_search_subject where `tutor_institute_user_id` in ($str_found_user_id_array)";

		$fees_amount = "SELECT
							MIN(fees_amount) AS fee,
							tutor_institute_user_id
						FROM
							view_pdg_batch_fees
						WHERE
							`tutor_institute_user_id` IN ($str_found_user_id_array)
						GROUP BY
							tutor_institute_user_id";
							
		$fees_amount1="SELECT feeslower AS fee FROM user_data_teacher WHERE new_user_id IN ($str_found_user_id_array)";					

		$locations_db = $wpdb->get_results( $locations_sql );
		if(empty($locations_db)){
		$locations_db = $wpdb->get_results( $locations_sql1 );	
		}
		$subjects_db = $wpdb->get_results( $subjects_sql );
		if(empty($subjects_db)){
		$subjects_db = $wpdb->get_results( $subjects_sql1 );	
		}
		
		$fees_db = $wpdb->get_results( $fees_amount, ARRAY_A );
		if(empty($fees_db)){
		$fees_db = $wpdb->get_results( $fees_amount1, ARRAY_A );	
		}
		
		$fees_array = array ();

		foreach ( $fees_db as $fee_val ) {
			$fees_array[ $fee_val['tutor_institute_user_id'] ] = $fee_val['fee'];
		}

		//todo: sql sort here:
		$teacher_sql = "select * from view_pdg_teachers where teacher_id in (" . implode( ',', $teacher_array ) . ")";
		$institute_sql = "select * from view_pdg_institutes where institute_id in (" . implode( ',', $institute_array ) . ")";


		$teacher_data = array ();
		$institute_data = array ();

		if ( count( $teacher_array ) > 0 ) {
			$teacher_db = $wpdb->get_results( $teacher_sql );
			foreach ( $teacher_db as $teacher ) {
				$teacher_id = $teacher->teacher_id;
				$teacher_user_id = $teacher->user_id;
				$teacher_data[ $teacher_id ]['id'] = $teacher_id;
				$teacher_data[ $teacher_id ]['name'] = $teacher->first_name . ' ' . $teacher->last_name;
				$teacher_data[ $teacher_id ]['teaching_xp'] = $teacher->teaching_xp;
				$teacher_data[ $teacher_id ]['gender'] = $teacher->gender;
				$teacher_data[ $teacher_id ]['fees'] = isset( $fees_array[ $teacher_user_id ] ) && $fees_array[ $teacher_user_id ] !== '' ? $fees_array[ $teacher_user_id ] : 'N/A';
				$profile_slug = $teacher->profile_slug;
				$teacher_data[ $teacher_id ]['profile_slug'] = $profile_slug;

				$subject_array = array ();
				foreach ( $subjects_db as $subject ) {
					$subject_user_id = $subject->tutor_institute_user_id;
					$subject_name = $subject->subject_name;
					if ( $subject_user_id == $teacher_user_id ) {
						if ( ! in_array( $subject_name, $subject_array ) ) {
							$subject_array[] = $subject_name;
						}
					}
				}
				if(empty($subject_array)){
				$sql="SELECT subjects FROM user_data_teacher WHERE new_user_id='".$teacher_user_id."'";
				$sub = $wpdb->get_results($sql);	
				$subject_array[]=$sub[0]->subjects;	
				}
				$teacher_data[ $teacher_id ]['subjects'] = implode( ', ', $subject_array );


				$locations_array = array ();
				$tuition_location_array = array ();
				foreach ( $locations_db as $location ) {
					$location_user_id = $location->tutor_institute_user_id;
					$location_name = $location->locality;
					if ( $location_user_id == $teacher_user_id ) {
						$tuition_location_type = $location->tuition_location_type;
						if ( ! in_array( $location_name, $locations_array ) ) {
							$locations_array[] = $location_name;
							$tuition_location_array [ $tuition_location_type ] = $tuition_location_type;
						}
					}
				}
                                if(empty($locations_array)){
				$sql="SELECT locality1,locality2,locality3 FROM user_data_teacher WHERE new_user_id='".$teacher_user_id."'";
				$sub = $wpdb->get_results($sql);	
				$locations_array[]=$sub[0]->locality1;
				$locations_array[]=$sub[0]->locality2;
				$locations_array[]=$sub[0]->locality3;
				}
				
				$teacher_data[ $teacher_id ]['locations'] = implode( ', ', $locations_array );
				$teacher_data[ $teacher_id ]['tuition_location_type'] = $tuition_location_array;
			}
		}

		if ( count( $institute_array ) > 0 ) {
			$institute_db = $wpdb->get_results( $institute_sql );
			foreach ( $institute_db as $institute ) {
				$institute_id = $institute->institute_id;
				$institute_data[ $institute_id ]['id'] = $institute_id;
				$institute_data[ $institute_id ]['name'] = $institute->institute_name;
				$institute_data[ $institute_id ]['teaching_xp'] = $institute->teaching_xp;
				$institute_user_id = $institute->user_id;

				$profile_slug = $institute->profile_slug;
				$institute_data[ $institute_id ]['profile_slug'] = $profile_slug;

				$subject_array = array ();
				$tuition_location_array = array ();
				foreach ( $subjects_db as $subject ) {
					$subject_user_id = $subject->tutor_institute_user_id;
					$subject_name = $subject->subject_name;
					if ( $subject_user_id == $institute_user_id ) {
						if ( ! in_array( $subject_name, $subject_array ) ) {
							$subject_array[] = $subject_name;
						}
					}
				}
				$institute_data[ $institute_id ]['subjects'] = implode( ', ', $subject_array );
				$institute_data[ $institute_id ]['fees'] = isset( $fees_array[ $institute_user_id ] ) && $fees_array[ $institute_user_id ] !== '' ? $fees_array[ $institute_user_id ] : 'N/A';
				$locations_array = array ();
				foreach ( $locations_db as $location ) {
					$location_user_id = $location->tutor_institute_user_id;
					$location_name = $location->locality;
					$tuition_location_type = $location->tuition_location_type;
					if ( $location_user_id == $institute_user_id ) {
						if ( ! in_array( $location_name, $locations_array ) ) {
							$locations_array[] = $location_name;
							$tuition_location_array [ $tuition_location_type ] = $tuition_location_type;
						}
					}
				}
				$showed_locations = array ();

				$institute_data[ $institute_id ]['locations'] = implode( ', ', $locations_array );
				$institute_data[ $institute_id ]['tuition_location_type'] = $tuition_location_array;
			}
		}

		$str_search_result = '';
		$return_data = array ();
		foreach ( $found_teachers_institutes as $search_found_data ) {
			$personal_info_id = $search_found_data->personal_info_id;
			$user_role_name = $search_found_data->user_role_name;
			$teach_insti_id = $search_found_data->teach_insti_id;
			$user_id = $search_found_data->user_id;

			if ( $user_role_name == 'teacher' ) {

				if ( array_key_exists( $teach_insti_id, $teacher_data ) ) {

					$str_subjects_name = '';

					if ( isset( $teacher_data[ $teach_insti_id ]['subjects'] ) ) {
						$str_subjects_name = $teacher_data[ $teach_insti_id ]['subjects'];
					}

					$recommendation_count = 0;
					$current_user_has_recommended = false;
					if ( ! empty( $recommend_data ) ) {
						foreach ( $recommend_data as $recommend ) {
							$recommend_to_user_id = $recommend->recommended_to;
							$recommended_by_user_id = $recommend->recommended_by;

							if ( $user_id == $recommend_to_user_id ) {
								if ( $recommended_by_user_id == $loggedin_user_id ) {
									$current_user_has_recommended = true;
								}
								$recommendation_count ++;
							}
						}
					}

					$ratings_total = 0;
					$rating_count = 0;
					$current_user_has_rated = false;
					$average_rating = 0;

					if ( ! empty( $rating_data ) ) {
						foreach ( $rating_data as $ratings ) {
							$rated_to_user_id = $ratings->tutor_institute_user_id;
							$rated_by_user_id = $ratings->reviewer_user_id;
							$star_rating = $ratings->overall_rating;
							$is_approved = $ratings->is_approved;

							if ( $user_id == $rated_to_user_id ) {
								if ( $rated_by_user_id == $loggedin_user_id ) {
									$current_user_has_rated = true;
								}
								if ( $is_approved == 'no' ) {
									continue;
								}
								$rating_count ++;
								$ratings_total += $star_rating;
							}
						}
					}
					$teacher_data[ $teach_insti_id ]['stars_total_rating_count'] = $rating_count;
					//todo: work here
					$rating_icon_name = $average_rating;
					if ( $rating_count > 0 && $ratings_total > 0 ) {
						$average_rating = $ratings_total / $rating_count;
						$average_rating = self::pdg_avg_rating( $average_rating );

						$rating_icon_name = str_replace( '.', '', $average_rating );
					}

					$teacher_data[ $teach_insti_id ]['recommendations_count'] = $recommendation_count;

					$str_recommendation_icon = '';
					$str_rating_icon = '';
					if ( is_numeric( $loggedin_user_id ) && $loggedin_user_id > 0 && $is_current_user_allowed_to_review ) {


					} else {

					}
					$str_teacher_gender = $teacher_data[ $teach_insti_id ]['gender'];
					$profile_image_path = 'storage/uploads/images/';
					$teacher_profile_image_url = '';
					$profile_data_img_orientation = 'square';
					$teacher_profile_image_path = PEDAGOGE_PLUGIN_DIR . $profile_image_path . $user_id . '/profile.jpg';
					if ( file_exists( $teacher_profile_image_path ) ) {
						$file_timestamp = date( 'U', filemtime( $teacher_profile_image_path ) );
						$profile_data_img_orientation = self::getImgOrientation( $teacher_profile_image_path );
						$teacher_profile_image_url = PEDAGOGE_PLUGIN_URL . '/' . $profile_image_path . $user_id . '/profile.jpg?' . $file_timestamp;
					} else {
						//$teacher_profile_image_url = PEDAGOGE_ASSETS_URL . '/images/sample.jpg';
						$teacher_profile_image_url = ControllerTeacher::get_empty_profile_picture('teacher', $str_teacher_gender);
					}
					$profile_slug = $teacher_data[ $teach_insti_id ]['profile_slug'];
					//$place_of_teaching=self::pdg_location_type( $teacher_data[ $teach_insti_id ]['tuition_location_type'] );
					$sql="SELECT hometutor FROM user_data_teacher WHERE new_user_id='".$user_id."' AND is_new=1";
					
					$st='';
					foreach($teacher_data[ $teach_insti_id ]['tuition_location_type'] as $key => $val){
					 $st= $val;
					}
					//exit;
					if($st==''){
					$locations_type_db = $wpdb->get_results( $sql );
					$st= $locations_type_db[0]->hometutor;
					}
					if($st=='student'){
					$stt="Teacher's,Student's";
					}
					elseif($st=='teacher'){
					$stt='Teacher';
					}
					elseif($st=='own'){
					$stt='Teacher';
					}
					elseif($st=='Own Location'){
					$stt='Teacher';
					}
					elseif($st=="Both own location and student's location"){
					$stt="Teacher's,Student's";
					}
					else{
					$stt='';
					}
					$return_data [] = array (
						'name'                         => $teacher_data[ $teach_insti_id ]['name'],
						'experience'                   => $teacher_data[ $teach_insti_id ]['teaching_xp'],
						'rating_stars'                 => self::pdg_avg_rating_to_stars( $average_rating ),
						'place_of_teaching'            => array('img_teaching'=>'&#xE84F;','img_teaching_title'=>$stt),
						'locations'                    => $teacher_data[ $teach_insti_id ]['locations'],
						'subjects'                     => $str_subjects_name,
						'fees'                         => $teacher_data[ $teach_insti_id ]['fees'],
						'stars_total_rating_count'     => $teacher_data[ $teach_insti_id ]['stars_total_rating_count'],
						'recommendations_count'        => $teacher_data[ $teach_insti_id ]['recommendations_count'],
						'url'                          => $profile_slug,
						'profile_data_img'             => $teacher_profile_image_url,
						'profile_data_img_orientation' => $profile_data_img_orientation,
						'profile_data_id'              => $user_id,
						'profile_type'                 => 'teacher',
					);

					//todo: work here
					$teacher_search_icon = PEDAGOGE_ASSETS_URL . '/images/teacher_search.png';
					$str_search_result .= '';
				}

			} else if ( $user_role_name == 'institution' ) {

				if ( array_key_exists( $teach_insti_id, $institute_data ) ) {

					$str_subjects_name = '';
					if ( isset( $institute_data[ $teach_insti_id ]['subjects'] ) ) {
						$str_subjects_name = $institute_data[ $teach_insti_id ]['subjects'];
					}


					$short_subjects = substr( $str_subjects_name, 0, 8 );
					$short_subjects .= '...';

					$recommendation_count = 0;
					$current_user_has_recommended = false;
					if ( ! empty( $recommend_data ) ) {
						foreach ( $recommend_data as $recommend ) {
							$recommend_to_user_id = $recommend->recommended_to;
							$recommended_by_user_id = $recommend->recommended_by;

							if ( $user_id == $recommend_to_user_id ) {
								if ( $recommended_by_user_id == $loggedin_user_id ) {
									$current_user_has_recommended = true;
								}
								$recommendation_count ++;
							}
						}
					}
					$ratings_total = 0;
					$rating_count = 0;
					$current_user_has_rated = false;
					$average_rating = 0;

					if ( ! empty( $rating_data ) ) {
						foreach ( $rating_data as $ratings ) {
							$rated_to_user_id = $ratings->tutor_institute_user_id;
							$rated_by_user_id = $ratings->reviewer_user_id;
							$star_rating = $ratings->overall_rating;
							$is_approved = $ratings->is_approved;

							if ( $user_id == $rated_to_user_id ) {
								if ( $rated_by_user_id == $loggedin_user_id ) {
									$current_user_has_rated = true;
								}
								if ( $is_approved == 'no' ) {
									continue;
								}
								$rating_count ++;
								$ratings_total += $star_rating;
							}
						}
					}
					$institute_data[ $teach_insti_id ]['stars_total_rating_count'] = $rating_count;
					//todo: work here
					$rating_icon_name = $average_rating;
					if ( $rating_count > 0 && $ratings_total > 0 ) {
						$average_rating = $ratings_total / $rating_count;
						$average_rating = self::pdg_avg_rating( $average_rating );

						$rating_icon_name = str_replace( '.', '', $average_rating );
					}
					$institute_data[ $teach_insti_id ]['recommendations_count'] = $recommendation_count;
					//todo: work here
					$str_recommendation_icon = '';
					$str_rating_icon = '';
					if ( is_numeric( $loggedin_user_id ) && $loggedin_user_id > 0 && $is_current_user_allowed_to_review ) {

					} else {

					}
					$profile_image_path = 'storage/uploads/images/';
					$teacher_profile_image_url = '';
					$profile_data_img_orientation = 'square';
					$teacher_profile_image_path = PEDAGOGE_PLUGIN_DIR . $profile_image_path . $user_id . '/profile.jpg';
					if ( file_exists( $teacher_profile_image_path ) ) {
						$file_timestamp = date( 'U', filemtime( $teacher_profile_image_path ) );
						$profile_data_img_orientation = self::getImgOrientation( $teacher_profile_image_path );
						$teacher_profile_image_url = PEDAGOGE_PLUGIN_URL . '/' . $profile_image_path . $user_id . '/profile.jpg?' . $file_timestamp;
					} else {
						$teacher_profile_image_url = ControllerTeacher::get_empty_profile_picture('institute');
					}

					$institute_search_icon = PEDAGOGE_ASSETS_URL . '/images/institute_search.png';
					//todo: work here
					$profile_slug = $institute_data[ $teach_insti_id ]['profile_slug'];
					$str_search_result .= '';
                                        $iplace_of_teaching=self::pdg_location_type( $institute_data[ $teach_insti_id ]['tuition_location_type'] );
					$return_data [] = array (
						'name'                         => $institute_data[ $teach_insti_id ]['name'],
						'experience'                   => $institute_data[ $teach_insti_id ]['teaching_xp'],
						'rating_stars'                 => self::pdg_avg_rating_to_stars( $average_rating ),
						'place_of_teaching'            => @array_unique($iplace_of_teaching),
						'locations'                    => $institute_data[ $teach_insti_id ]['locations'],
						'subjects'                     => $str_subjects_name,
						'fees'                         => $institute_data[ $teach_insti_id ]['fees'],
						'recommendations_count'        => $institute_data[ $teach_insti_id ]['recommendations_count'],
						'stars_total_rating_count'     => $institute_data[ $teach_insti_id ]['stars_total_rating_count'],
						'url'                          => $profile_slug,
						'profile_data_img'             => $teacher_profile_image_url,
						'profile_data_img_orientation' => $profile_data_img_orientation,
						'profile_data_id'              => $user_id,
						'profile_type'                 => 'institute',
					);
				}
			}
		}

		return $return_data;
	}

	public function whereInSanitize( $where, $whereQuery ) {
		if ( trim( $where ) == '' ) {
			return $whereQuery;
		}

		return '';
	}

	public static function getImgOrientation( $img_file ) {
		if ( ! file_exists( $img_file ) || ! is_file( $img_file ) || ! is_readable( $img_file ) ) {
			return false;
		}

		list( $width, $height ) = @getimagesize( $img_file );

		if ( $width == $height ) {
			$orientation = "square";
		} else if ( $width > $height ) {
			$orientation = "landscape";
		} else {
			$orientation = "portrait";
		}

		return $orientation;
	}

	public static function pdg_avg_rating( $avg_rating ) {
		$return_rating = 0;

		if ( $avg_rating > 4.5 ) {
			$return_rating = 5;
		} else if ( $avg_rating > 4 ) {
			$return_rating = 4.5;
		} else if ( $avg_rating > 3.5 ) {
			$return_rating = 4;
		} else if ( $avg_rating > 3 ) {
			$return_rating = 3.5;
		} else if ( $avg_rating > 2.5 ) {
			$return_rating = 3;
		} else if ( $avg_rating > 2 ) {
			$return_rating = 2.5;
		} else if ( $avg_rating > 1.5 ) {
			$return_rating = 2;
		} else if ( $avg_rating > 1 ) {
			$return_rating = 1.5;
		} else if ( $avg_rating > 0.5 ) {
			$return_rating = 1;
		} else if ( $avg_rating > 0 ) {
			$return_rating = 0.5;
		}

		return $return_rating;
	}

	public static function pdg_avg_rating_to_stars( $avg_rating ) {
		$avg_rating = self::pdg_avg_rating( $avg_rating );
		$return_array = array ();
		$avg_rating = round( $avg_rating * 2, 0 ) / 2;
		if ( $avg_rating == 5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
			);
		} elseif ( $avg_rating == 4.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
			);

		} elseif ( $avg_rating == 4 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 3.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 3 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 2.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 2 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);
		} elseif ( $avg_rating == 1.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 1 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 0.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 0 ) {
			$return_array = array (
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);
		}

		return $return_array;
	}

	public static function pdg_location_type( $param ) {
		$tuition_location_array = array ();
		foreach ( $param as $tuition_location ) {
			switch ( $tuition_location ) {
				case 'student':
					//$tuition_location_array[] = array (
					//	'img_teaching'       => '&#xE52F;',
					//	'img_teaching_title' => "Teacher's",
					//);
					$tuition_location_array[] = array (
						'img_teaching'       => '&#xE52Ft;',
						'img_teaching_title' => "Student's",
					);
					break;
				case 'own':
				//     $tuition_location_array[] = array (
				//		'img_teaching'       => '&#xE52F;',
				//		'img_teaching_title' => "Teacher's",
				//	);
				//	break;
				case 'teacher':
					$tuition_location_array[] = array (
						'img_teaching'       => '&#xE88At;',
						'img_teaching_title' => "Teacher's",
					);
					break;
				case 'institute':
					$tuition_location_array[] = array (
						'img_teaching'       => '&#xE84F;',
						'img_teaching_title' => "Institute",
					);
					break;
				default:
					break;
			}
		}

		return $tuition_location_array;
	}

	public static function fn_return_pagination_html( $pagination_array ) {
		$total_found_records = $pagination_array['total_found_records'];
		$pagination_no = isset( $pagination_array['page_no'] ) ? $pagination_array['page_no'] : 1;

		if ( ! is_numeric( $pagination_no ) || $pagination_no <= 0 ) {
			$pagination_no = 1;
		}

		$total_page_count = 1;
		$str_pagination = array ();

		if ( $total_found_records > 0 ) {
			$total_page_count = ceil( $total_found_records / 12 );
		}

		for ( $page_counter = 1; $page_counter <= $total_page_count; $page_counter ++ ) {
			if ( $pagination_no == $page_counter ) {
				$str_pagination[] = '<li class="active span_pagination" data-page_no="' . $page_counter . '"><a href="#" class="cmd_pagination_btn">' . $page_counter . '</a></li>';
			} else {
				$str_pagination[] = '<li class="span_pagination" data-page_no="' . $page_counter . '"><a href="#" class="cmd_pagination_btn">' . $page_counter . '</a></li>';
			}
		}

		return $str_pagination;
	}
}