<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PedagogeMailer  {
	
	private $from_address = '';
	private $to_address = '';
	private $cc_addresses = array();
	private $mail_header = '';
	private $mail_footer = '';
	protected $mail_subject = "Mail from Pedagoge Website";
	private $log_email = 'no';
	private $mail_type = '';
	private $email_subscribers = array();	
	
	private $send_to_subscriber = '';
	private $send_to_user = 'yes';	
	protected $mail_data = array();
	
	//future settings
	private $email_priority = '';
	protected $email_template = '';
	
	public function __construct($data) {
		
		if(isset($data['to_address'])) {
			$this->to_address = $data['to_address'];
		}
		if(isset($data['mail_type'])) {
			$this->mail_type = $data['mail_type'];
		}
		$this->mail_data = $data;
	}
	
	public function sendmail() {
		global $wpdb;
		$return_data = array(
			'error' => TRUE,
			'message' => ''
		);
		
		// If mail type is not set then fail silently
		if(empty($this->mail_type)) {			
			$return_data['message'] = "Error! Mail Type is not set.";
			return $return_data;
		}
		
		// If to email is not set then fail silently
		if(empty($this->to_address)) {			
			$return_data['message'] = "Error! Recipient is not set.";
			return $return_data;
		}
		
		// fetch email settings
		$email_settings_db = $wpdb->get_results("select * from pdg_email_settings where email_type = '$this->mail_type'");
		$email_settings_id = '';
		$sender_email_address = '';
		$email_subject = '';
		// if email type is not set in the database then fail.
		if(empty($email_settings_db)) {
			$return_data['message'] = "Error! Mail Type is not available in the database.";
			return $return_data;
		}
		
		// set email settings
		foreach($email_settings_db as $email_settings) {
			$email_settings_id = $email_settings->email_settings_id;
			$sender_email_address = $email_settings->sender_email;
			$this->send_to_subscriber = $email_settings->send_to_subscriber;
			$this->send_to_user = $email_settings->send_to_user;
			$this->email_priority = $email_settings->email_priority;
			$email_subject = $email_settings->email_subject;
		}
		
		// If email subject is available then set it to database version.
		if(!empty($email_subject)) {
			$this->mail_subject = $email_subject;
		}
		
		//set sender email address
		$this->fn_set_sender_email($sender_email_address);
				
		//fetch and set email subscribers list
		$email_subscribers_list_db = $wpdb->get_results("select * from pdg_email_subscribers where email_setting_id = $email_settings_id");
		foreach($email_subscribers_list_db as $email_subscribers) {
			$email_address = $email_subscribers->email_address;
			if(!array_key_exists($email_address, $this->email_subscribers)) {
				$this->email_subscribers[] = $email_address;
			}
		}
		
		// Set Email Log by fetching email logging setting.
		$log_email = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_email_log');
		
		$return_data = $this->fn_sendmail_now();
		return $return_data;
	}
	
	private function fn_set_sender_email($sender_email_address = '') {
		// if sender email is not set then fetch default system sender email
		if(empty($sender_email_address)) {
			$this->from_address = ControlPanelSettings::fn_email_settings_wp_options('fetch', 'pdg_default_email_sender');
		} else {
			$this->from_address = $sender_email_address;
		}
		
		// even if the default system sender email is not available then set it manually.
		if(empty($this->from_address)) {
			$this->from_address = 'noreply@pedagoge.com';
		}
	}
	
	private function fn_sendmail_now() {
		
		$return_data = array(
			'error' => TRUE,
			'message' => ''
		);
		$email_content = '';	
		/**
		 * find whether updated template is available or not.
		 * if available then load that else load original
		 */
		$pedagoge_updated_email_template_dir = PEDAGOGE_PLUGIN_DIR.'storage/updated_email_templates/';
		$updated_email_template_file_name = $pedagoge_updated_email_template_dir.$this->mail_type.'.html.php';
		if(file_exists($updated_email_template_file_name)) {
			$email_content = $this->load_email_view($updated_email_template_file_name);
		} else {
			$view_file_name = PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/'.$this->mail_type.'.html.php';
			$email_content = $this->load_email_view($view_file_name);
		}
		
		
		$to_address = $this->to_address;
		$subject = $this->mail_subject;
		
		$headers[] = 'From: Pedagoge <'.$this->from_address.'>';
		
		//send to subscribers
		if($this->send_to_subscriber=='yes') {
			foreach($this->email_subscribers as $subscriber) {
				$headers[] = 'Cc: '.$subscriber.'';
			}
		}
		
		$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';			
		
		add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
		$mail_process_result = wp_mail($to_address, $subject, $email_content, $headers);
		
		if($this->log_email == 'yes') {
			$this->fn_log_mails($email_content);
		}
		
		$return_data['error'] = FALSE;
		$return_data['message'] = "Mail was successfully sent!";
		$return_data['mail_result'] = $mail_process_result;		
		return $return_data;
	}	
	
	private function fn_sendmail_async() {
		//configure in future
	}
	
	private function fn_log_mails($email_content) {
		//configure in future
	}
	
	/**
	 * All views are relative to the folder swpmvc/views/
	 * just name the file and not its extensions.
	 */
	private function load_email_view($view, $template_vars = null) {
		$view_file_name = $view;

		$str_return = '';

		if(file_exists($view_file_name)) {

			extract($this->mail_data);

			if(isset($template_vars) && !empty($template_vars)) {
				extract($template_vars);
			}

			ob_start();
			include_once($view_file_name);
			$str_return = ob_get_clean();

		} else {
			$str_return = 'Error! Loading view '.$view_file_name;
		}

		return $str_return;
	}
	
	/**
	 * This function is for future reference and implementation.
	 * At present we are able to create a mail type in the database. 
	 * In future, mail types will be defined here, and only its related information
	 * will be stored in the database.
	 */
	private function fn_mail_types() {
		$mail_types = array(
			'password_reset_user' => 'Password Reset User',
			'password_reset_confirmation' => 'Password Reset Confirmation',
			'user_signup_and_account_activation' => 'User Signup and Account Activation',
			'user_signup_and_account_activation_notice' => 'User Signup and Account Activation Notice',
			'' => '',
			'' => '',
			'' => '',
			'' => '',
			'' => '',
			'' => '',
		);
	}
	
}