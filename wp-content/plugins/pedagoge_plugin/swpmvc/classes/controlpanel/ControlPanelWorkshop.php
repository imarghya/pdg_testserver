<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelWorkshop {	
	
	public static function workshop_list_datatable() {
		global $wpdb;
		$table = 'view_pdg_workshops';		
		$primaryKey = 'workshop_id';
		
		$columns = array(
			array( 'db' => 'workshop_id', 'dt' => 0, 'dt_type' => 'number' ),
			array( 'db' => 'user_id',   'dt' => 1, 'dt_type' => 'number' ),
			array( 'db' => 'workshop_name',   'dt' => 2 ),
			array( 'db' => 'trainer_name',   'dt' => 3 ),
			array( 'db' => 'start_date',   'dt' => 4 ),
			array( 'db' => 'end_date',   'dt' => 5 ),
			array( 'db' => 'category_id',   'dt' => 6, 'dt_type' => 'number' ),			
			array( 'db' => 'workshop_category',  'dt' => 7 ),
			array( 'db' => 'workshop_city',   'dt' => 8, 'dt_type' => 'number' ),
			array( 'db' => 'city_name',   'dt' => 9 ),
			array( 'db' => 'workshop_locality',   'dt' => 10, 'dt_type' => 'number' ),
			array( 'db' => 'locality',   'dt' => 11 ),
			array( 'db' => 'workshop_type',   'dt' => 12 ),
			array( 'db' => 'workshop_seats',   'dt' => 13 ),
			array( 'db' => 'workshop_price',   'dt' => 14 ),
			array( 'db' => 'pay_online',   'dt' => 15 ),
			array( 'db' => 'pay_at_venue',   'dt' => 16 ),
			array( 'db' => 'is_approved',   'dt' => 17 ),
			array( 'db' => 'is_active',   'dt' => 18 ),
			array( 'db' => 'is_featured',   'dt' => 19 ),
			array( 'db' => 'workshop_id',   'dt' => 20, 'dt_type' => 'number' ),
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
}