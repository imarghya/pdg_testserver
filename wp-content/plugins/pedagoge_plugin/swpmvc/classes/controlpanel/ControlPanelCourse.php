<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelCourse {
	
	public static function fn_return_subject_names_list() {
		global $wpdb;
		$return_message = array('error' => FALSE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');
		
		$return_message['data'] = PDGManageCache::fn_load_cache('pdg_subject_name');
		
		return $return_message;		
	}
	
	public static function fn_return_subjects_list_table($subjects_list, $class_name) {
		$str_subjects_tr = '';	
	
		$str_subject_list1 = '
			<table class="datatable table table-striped table-vcenter table-bordered table-condensed table-hover '.$class_name.'">
				<thead>
					<tr>
						<td>ID</td>
						<td>Subject Name</td>
						<td>Select</td>
					</tr>
				</thead>
				<tbody>				
		';
		
		foreach($subjects_list as $subject) {
			$str_subjects_tr.='
				<tr class="row_subject_name">
					<td>'.$subject->subject_name_id.'</td>
					<td>'.$subject->subject_name.'</td>
					<td><input type="checkbox" class="chk_subject_name" value="'.$subject->subject_name_id.'"></td>
				</tr>
			';
		}
		
		$str_subject_list1.=$str_subjects_tr;
		
		$str_subject_list1.='
				</tbody>
			</table>
		';
		return $str_subject_list1;
	}

	public static function fn_subjects_datatable() {
		global $wpdb;
		$table = 'pdg_subject_name';
		// Table's primary key
		$primaryKey = 'subject_name_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'subject_name_id', 'dt' => 0 ),
			array( 'db' => 'subject_name',  'dt' => 1 ),
			array( 'db' => 'subject_name_id',   'dt' => 2 ),						
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	public static function fn_subject_types_datatable() {
		global $wpdb;
		$table = 'pdg_subject_type';
		// Table's primary key
		$primaryKey = 'subject_type_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'subject_type_id', 'dt' => 0 ),
			array( 'db' => 'subject_type',  'dt' => 1 ),
			array( 'db' => 'subject_type_id',   'dt' => 2 ),						
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	public static function fn_course_datatable() {
		global $wpdb;
		$table = 'view_course_category';
		// Table's primary key
		$primaryKey = 'course_category_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'course_category_id', 'dt' => 0, 'dt_type' => 'number' ),
			array( 'db' => 'subject_name_id',  'dt' => 1, 'dt_type' => 'number' ),
			array( 'db' => 'subject_name',   'dt' => 2 ),
			array( 'db' => 'subject_type_id', 'dt' => 3, 'dt_type' => 'number' ),
			array( 'db' => 'subject_type',  'dt' => 4 ),
			array( 'db' => 'course_type_id',   'dt' => 5, 'dt_type' => 'number' ),
			array( 'db' => 'course_type', 'dt' => 6 ),
			array( 'db' => 'academic_board_id',  'dt' => 7, 'dt_type' => 'number' ),
			array( 'db' => 'academic_board',   'dt' => 8 ),
			array( 'db' => 'course_age_category_id', 'dt' => 9, 'dt_type' => 'number' ),
			array( 'db' => 'course_age',  'dt' => 10 ),
			array( 'db' => 'course_category_id',   'dt' => 11, 'dt_type' => 'number' ),
			
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	public static function fn_save_subject_name() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_name');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		
		$record_id = $_POST['record_id'];
		$record_value = sanitize_text_field(trim($_POST['record_value']));
		
		if(empty($record_value)) {
			$return_message['message'] = "Please input record value!";
			echo json_encode($return_message);
			die();
		}
		
		$subject_name_slug = sanitize_title($record_value);
		
		$data_array = array(
			'subject_name' => $record_value,
			'slug' => $subject_name_slug
		);
		$data_format = array('%s', '%s');
		
		$duplicate_data = $wpdb->get_results("select * from pdg_subject_name where subject_name like '$record_value'");
		if(empty($record_id) || !is_numeric($record_id)) {
			//insert
			if(!empty($duplicate_data)) {
				$return_message['message'] = "Duplicate record found. Please check your input and try again.";
				echo json_encode($return_message);
				die();
			}
			
			$wpdb->insert('pdg_subject_name', $data_array, $data_format);
			$new_record_id = $wpdb->insert_id;
			
			if(!is_numeric($new_record_id) || $new_record_id<=0) {
				$return_message['message'] = "Record was not saved. Please try again!";
				echo json_encode($return_message);
				die();
			}
			
			//update cache
			PDGManageCache::fn_update_cache('pdg_subject_name');
			
			$return_message['error'] = FALSE;
			$return_message['message'] = "Record was saved successfully!";
			echo json_encode($return_message);
			die();
			
		} else {
			//update
			$duplicate_record_id = '';
			if(!empty($duplicate_data)) {
				foreach($duplicate_data as $dup_data) {
					$duplicate_record_id = $dup_data->subject_name_id;
				}
			}
			if(!empty($duplicate_record_id) && $record_id!=$duplicate_record_id) {
				$return_message['message'] = "Duplicate record found. Please check your input and try again.";
				echo json_encode($return_message);
				die();
			}
			
			$where_data_array = array(
				'subject_name_id' => $record_id
			);
			$where_data_format = array('%d');
			$wpdb->update('pdg_subject_name', $data_array, $where_data_array, $data_format, $where_data_format);
			
			//update cache
			PDGManageCache::fn_update_cache('pdg_subject_name');
			PDGManageCache::fn_update_cache('registered_subjects');
			
			$return_message['error'] = FALSE;
			$return_message['message'] = "Record was updated successfully!";
			echo json_encode($return_message);
			die();
		}
	}
	
	public static function fn_delete_subject_name() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_name');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$record_id = $_POST['record_id'];		
		if(!is_numeric($record_id)) {
			$return_message['message'] = "Record ID is not available! Please select a record before deleting.";
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * Check if subject name is connected with any course or not. if connected then do not delete
		 */
		
		$course_connected = $wpdb->get_results("select * from pdg_course_category where subject_name_id = $record_id");
		if(!empty($course_connected)) {
			$return_message['message'] = "This subject name is already attached with a course. First delete the course then delete the subject name.";
			echo json_encode($return_message);
			die();
		}
		
		$wpdb->delete('pdg_subject_name', array('subject_name_id' => $record_id), array('%d'));
		
		//update cache
		PDGManageCache::fn_update_cache('pdg_subject_name');
		PDGManageCache::fn_update_cache('registered_subjects');
			
		$return_message['error'] = FALSE;
		$return_message['message'] = "Record was deleted successfully!";
		echo json_encode($return_message);
		die();
	}
	
	public static function fn_save_subject_type() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_type');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$record_id = $_POST['record_id'];
		$record_value = sanitize_text_field(trim($_POST['record_value']));
		
		if(empty($record_value)) {
			$return_message['message'] = "Please input record value!";
			echo json_encode($return_message);
			die();
		}
		$data_array = array(
			'subject_type' => $record_value
		);
		$data_format = array('%s');
		
		$duplicate_data = $wpdb->get_results("select * from pdg_subject_type where subject_type like '$record_value'");
		if(empty($record_id) || !is_numeric($record_id)) {
			//insert
			if(!empty($duplicate_data)) {
				$return_message['message'] = "Duplicate record found. Please check your input and try again.";
				echo json_encode($return_message);
				die();
			}
			
			$wpdb->insert('pdg_subject_type', $data_array, $data_format);
			$new_record_id = $wpdb->insert_id;
			
			if(!is_numeric($new_record_id) || $new_record_id<=0) {
				$return_message['message'] = "Record was not saved. Please try again!";
				echo json_encode($return_message);
				die();
			}
			
			//update cache
			PDGManageCache::fn_update_cache('pdg_subject_type');
			
			$return_message['error'] = FALSE;
			$return_message['message'] = "Record was saved successfully!";
			echo json_encode($return_message);
			die();
			
		} else {
			//update
			$duplicate_record_id = '';
			if(!empty($duplicate_data)) {
				foreach($duplicate_data as $dup_data) {
					$duplicate_record_id = $dup_data->subject_name_id;
				}
			}
			if(!empty($duplicate_record_id) && $record_id!=$duplicate_record_id) {
				$return_message['message'] = "Duplicate record found. Please check your input and try again.";
				echo json_encode($return_message);
				die();
			}
			
			$where_data_array = array(
				'subject_type_id' => $record_id
			);
			$where_data_format = array('%d');
			$wpdb->update('pdg_subject_type', $data_array, $where_data_array, $data_format, $where_data_format);
			
			//update cache
			PDGManageCache::fn_update_cache('pdg_subject_type');
			
			$return_message['error'] = FALSE;
			$return_message['message'] = "Record was updated successfully!";
			echo json_encode($return_message);
			die();
		}
		
	}
	
	public static function fn_delete_subject_type() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_type');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$record_id = $_POST['record_id'];		
		if(!is_numeric($record_id)) {
			$return_message['message'] = "Record ID is not available! Please select a record before deleting.";
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * Check if subject type is connected with any course or not. if connected then do not delete
		 */
		
		$course_connected = $wpdb->get_results("select * from pdg_course_category where course_type_id = $record_id");
		if(!empty($course_connected)) {
			$return_message['message'] = "This subject type is already attached with a course. First delete the course then delete the subject name.";
			echo json_encode($return_message);
			die();
		}
		
		$wpdb->delete('pdg_subject_type', array('subject_type_id' => $record_id), array('%d'));
		
		//update cache
		PDGManageCache::fn_update_cache('pdg_subject_type');
		
		$return_message['error'] = FALSE;
		$return_message['message'] = "Record was deleted successfully!";
		echo json_encode($return_message);
		die();
	}

	public static function fn_load_subject_names_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		
		$str_return = '<option></option>';
		
		$subject_names_db = PDGManageCache::fn_load_cache('pdg_subject_name');
		if(empty($subject_names_db)) {
			$str_return = '<option>Data not available</option>';
		} else {
			foreach($subject_names_db as $subject_names) {
				$str_return.= '<option value="'.$subject_names->subject_name_id.'">'.$subject_names->subject_name.'</option>';
			}
		}
		$return_message['error'] = FALSE;
		$return_message['data'] = $str_return;
		echo json_encode($return_message);
		die();
	}
	
	public static function fn_load_subject_types_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		
		$str_return = '<option></option>';
		
		$subject_types_db = PDGManageCache::fn_load_cache('pdg_subject_type');
		if(empty($subject_types_db)) {
			$str_return = '<option>Data not available</option>';
		} else {
			foreach($subject_types_db as $subject_types) {
				$str_return.= '<option value="'.$subject_types->subject_type_id.'">'.$subject_types->subject_type.'</option>';
			}
		}
		$return_message['error'] = FALSE;
		$return_message['data'] = $str_return;
		echo json_encode($return_message);
		die();
	}
	
	public static function fn_create_course_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		
		$subject_name_list = $_POST['subject_name'];
		$subject_type_list = $_POST['subject_type'];
		$academic_board_list = $_POST['academic_board'];
		$course_age_list = $_POST['course_age'];
		
		$subject_name_array = array();
		$subject_type_array = array();
		$academic_board_array = array();
		$course_age_array = array();
		
		if(is_array($subject_name_list)) {
			foreach($subject_name_list as $subject_name_id) {
				if(is_numeric($subject_name_id)) {
					$subject_name_array[] = $subject_name_id;					
				}
			}
			
		} else {
			if(is_numeric($subject_name_list)) {
				$subject_name_array[] = $subject_name_list;				
			}
		}
		
		
		if(is_array($subject_type_list)) {
			foreach($subject_type_list as $subject_type_id) {
				if(is_numeric($subject_type_id)) {
					$subject_type_array[] = $subject_type_id;
				}
			}
		} else {
			if(is_numeric($subject_type_list)) {
				$subject_type_array[] = $subject_type_list;
			}
		}
		
		if(is_array($academic_board_list)) {
			foreach($academic_board_list as $academic_board_id) {
				if(is_numeric($academic_board_id)) {
					$academic_board_array[] = $academic_board_id;
				}
			}
		} else {
			if(is_numeric($academic_board_list)) {
				$academic_board_array[] = $academic_board_list;
			}
		}
		
		if(is_array($course_age_list)) {
			foreach($course_age_list as $course_age_id) {
				if(is_numeric($course_age_id)) {
					$course_age_array[] = $course_age_id;
				}
			}
		} else {
			if(is_numeric($course_age_list)) {
				$course_age_array[] = $course_age_list;
			}
		}
		
		if(empty($subject_name_array)) {
			$return_message['message'] = 'Please select subject name';
			echo json_encode($return_message);
			die();
		}
		
		if(empty($subject_type_array)) {
			$return_message['message'] = 'Please select subject type';
			echo json_encode($return_message);
			die();
		}
		$str_subject_names_arr = implode(',', $subject_name_array);
		
		$str_sql = "select * from pdg_course_category where subject_name_id in ($str_subject_names_arr)";
		$found_courses_db = $wpdb->get_results($str_sql);
				
		foreach($subject_name_array as $subject_name_id) {
			
			foreach($subject_type_array as $subject_type_id) {
				
				if(!empty($academic_board_array)) {
					
					foreach($academic_board_array as $academic_board_id) {
						if(!empty($course_age_array)) {
							foreach($course_age_array as $course_age_id) {
								
								$insert_array = array(
									'subject_name_id' => $subject_name_id,
									'subject_type_id' => $subject_type_id,
									'academic_board_id' => $academic_board_id,
									'course_age_category_id' => $course_age_id
								);
								
								$found_duplicate = FALSE;
								if(!empty($found_courses_db)) {
									$found_duplicate = self::fn_find_duplicate_course($found_courses_db, $insert_array);
								}
								if(!$found_duplicate) {
									$insert_data_format = array('%d', '%d', '%d','%d');
									$wpdb->insert('pdg_course_category', $insert_array, $insert_data_format);	
								}
							}
						} else {
							$insert_array = array(
								'subject_name_id' => $subject_name_id,
								'subject_type_id' => $subject_type_id,
								'academic_board_id' => $academic_board_id,
							);
							$found_duplicate = FALSE;
							if(!empty($found_courses_db)) {
								$found_duplicate = self::fn_find_duplicate_course($found_courses_db, $insert_array);
							}
							if(!$found_duplicate) {
								$insert_data_format = array('%d', '%d', '%d');
								$wpdb->insert('pdg_course_category', $insert_array, $insert_data_format);	
							}
						}
					}
					
				} else {
					if(!empty($course_age_array)) {
						foreach($course_age_array as $course_age_id) {
							$insert_array = array(
								'subject_name_id' => $subject_name_id,
								'subject_type_id' => $subject_type_id,								
								'course_age_category_id' => $course_age_id
							);
							$found_duplicate = FALSE;
							if(!empty($found_courses_db)) {
								$found_duplicate = self::fn_find_duplicate_course($found_courses_db, $insert_array);
							}
							if(!$found_duplicate) {							
								$insert_data_format = array('%d', '%d', '%d');
								$wpdb->insert('pdg_course_category', $insert_array, $insert_data_format);	
							}
						}
					} else {
						$insert_array = array(
							'subject_name_id' => $subject_name_id,
							'subject_type_id' => $subject_type_id,
						);
						$found_duplicate = FALSE;
						if(!empty($found_courses_db)) {
							$found_duplicate = self::fn_find_duplicate_course($found_courses_db, $insert_array);
						}
						if(!$found_duplicate) {
							
							$insert_data_format = array('%d', '%d');
							$wpdb->insert('pdg_course_category', $insert_array, $insert_data_format);
							
							
						}
						
					}
				}				
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Courses created successfully!';
		echo json_encode($return_message);
		die();
		
	}

	public static function fn_find_duplicate_course($course_data, $to_search) {
		$return_data = FALSE;
		
		$subject_name_id = isset($to_search['subject_name_id']) ? $to_search['subject_name_id'] : ''; 
		$subject_type_id = isset($to_search['subject_type_id']) ? $to_search['subject_type_id'] : '';
		$academic_board_id = isset($to_search['academic_board_id']) ? $to_search['academic_board_id'] : '';
		$course_age_id = isset($to_search['course_age_category_id']) ? $to_search['course_age_category_id'] : '';
		
		foreach($course_data as $course_info) {
			
			$db_subject_name_id = $course_info->subject_name_id; 
			$db_subject_type_id = $course_info->subject_type_id;
			$db_academic_board_id = $course_info->academic_board_id;
			$db_course_age_id = $course_info->course_age_category_id;
			
			if($db_subject_name_id<=0) {
				$db_subject_name_id = '';
			}
			
			if($db_subject_type_id<=0) {
				$db_subject_type_id = '';
			}
			
			if($db_academic_board_id<=0) {
				$db_academic_board_id = '';
			}
			
			if($db_course_age_id<=0) {
				$db_course_age_id = '';
			}
			
			if($subject_name_id == $db_subject_name_id && $subject_type_id == $db_subject_type_id && $academic_board_id == $db_academic_board_id && $course_age_id == $db_course_age_id) {
				$return_data = TRUE;
				break;
			}
		}
		
		return $return_data;
	}

	public static function fn_delete_course_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$course_id = $_POST['course_id'];		
		if(!is_numeric($course_id) || $course_id <= 0) {
			$return_message['message'] = "Course ID is not available! Please select a course before deleting.";
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * Check if course is connected with any teacher or not. if connected then do not delete
		 */
		
		$course_connected = $wpdb->get_results("select batch_subject_id from pdg_batch_subject where course_category_id = $course_id");
		if(!empty($course_connected)) {
			$return_message['message'] = "This course is already attached with a teacher's batches. First delete the batch then delete the course.";
			echo json_encode($return_message);
			die();
		}
		
		$wpdb->delete('pdg_course_category', array('course_category_id' => $course_id), array('%d'));
				
		$return_message['error'] = FALSE;
		$return_message['message'] = "Course was deleted successfully!";
		echo json_encode($return_message);
		die();
	}

	public static function fn_update_course_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_update');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$course_id = $_POST['course_id'];
		$subject_name_id = $_POST['subject_name_id'];
		$subject_type_id = $_POST['subject_type_id'];
		$course_type_id = $_POST['course_type_id'];
		$course_age_category = $_POST['course_age_category'];
		$academic_board_id = $_POST['academic_board_id'];
		

		
		if(!is_numeric($course_id) || $course_id <= 0) {
			$return_message['message'] = "Course ID is not available! Please select a course before deleting.";
			echo json_encode($return_message);
			die();
		}
		
		if(!is_numeric($subject_name_id) || $course_id <= 0) {
			$return_message['message'] = "Please select Subject name...";
			echo json_encode($return_message);
			die();
		}

		if(!is_numeric($subject_type_id) || $course_id <= 0) {
			$return_message['message'] = "Please select Subject Type...";
			echo json_encode($return_message);
			die();
		}

		if(!is_numeric($course_type_id)) {
			$str_course_type_id = ' course_type_id IS NULL and ';
			$course_type_id = 'NULL';
		} else {
			$str_course_type_id = " course_type_id = $course_type_id and ";
		}
		
		if(!is_numeric($course_age_category)) {
			$str_course_age_category = " course_age_category_id IS NULL and ";
			$course_age_category = 'NULL';
		} else {
			$str_course_age_category = " course_age_category_id = $course_age_category and ";
		}
		
		if(!is_numeric($academic_board_id)) {
			$str_academic_board_id = " academic_board_id IS NULL and ";
			$academic_board_id = 'NULL';
		} else {
			$str_academic_board_id = " academic_board_id = $academic_board_id and ";
		}
		
		
		
		// find duplicates
		$str_sql = "
			select * from pdg_course_category where 
			subject_name_id = $subject_name_id and
			subject_type_id = $subject_type_id and
			$str_course_type_id
			$str_course_age_category
			$str_academic_board_id
			course_category_id != $course_id
		";
				
		$found_duplicate_db = $wpdb->get_results($str_sql);
		if(!empty($found_duplicate_db)) {
			
			$return_message['message'] = "Duplicate course found! Please check your input and try again...";
			echo json_encode($return_message);
			die();
		}
		
		if(!is_numeric($course_type_id)) {
			$course_type_id = 'NULL';
		}
		
		if(!is_numeric($course_age_category)) {
			$course_age_category = 'NULL';
		}
		
		if(!is_numeric($academic_board_id)) {
			$academic_board_id = 'NULL';
		}
		
		$str_sql = "
			update pdg_course_category set
			subject_name_id = $subject_name_id,
			subject_type_id = $subject_type_id,
			course_type_id = $course_type_id,
			academic_board_id = $academic_board_id,
			course_age_category_id = $course_age_category
			where course_category_id = $course_id
		";
		$wpdb->get_results($str_sql);
						
		$return_message['error'] = FALSE;
		$return_message['message'] = "Course was updated successfully!";
		echo json_encode($return_message);
		die();
	}

	public static function fn_merge_subject_names_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}		
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_subject_merge');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$subject_name_id1 = $_POST['subject_name_id1'];
		$subject_name_id2 = $_POST['subject_name_id2'];
		
		if(!is_numeric($subject_name_id1) || $subject_name_id1<=0) {
			$return_message['message'] = "Please select subject name from list1";
			echo json_encode($return_message);
			die();
		}
		
		if(!is_numeric($subject_name_id2) || $subject_name_id2<=0) {
			$return_message['message'] = "Please select subject name from list2";
			echo json_encode($return_message);
			die();
		}
		
		if($subject_name_id1 == $subject_name_id2) {
			$return_message['message'] = "Both subject names are same. Please select different subject names.";
			echo json_encode($return_message);
			die();
		}
		
		$str_sql = "update pdg_course_category set subject_name_id = $subject_name_id1 where subject_name_id = $subject_name_id2";
		$wpdb->get_results($str_sql);
		$wpdb->delete('pdg_subject_name', array('subject_name_id'=>$subject_name_id2),array('%d'));
		
		//update cache
		PDGManageCache::fn_update_cache('pdg_subject_name');
		PDGManageCache::fn_update_cache('registered_subjects');
		
		$return_message['error'] = FALSE;
		$return_message['message'] = "Subjects were merged successfully!";
		echo json_encode($return_message);
		die();
	}

	public static function fn_merge_course_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}		
		//check for permission
		$has_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_course_merge');
		if(!$has_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to perform this action!';
			echo json_encode($return_message );
			die();
		}
		$course1 = $_POST['course1'];
		$course2 = $_POST['course2'];
		
		if(!is_numeric($course1) || $course1 <= 0) {
			$return_message['message'] = "Please select a course from list1";
			echo json_encode($return_message);
			die();
		}
		
		if(!is_numeric($course2) || $course2 <= 0) {
			$return_message['message'] = "Please select a course from list2";
			echo json_encode($return_message);
			die();
		}
		
		if($course1 == $course2) {
			$return_message['message'] = "Both course are same. Please select different courses to merge.";
			echo json_encode($return_message);
			die();
		}
		
		$str_sql = "update pdg_batch_subject set course_category_id = $course1 where course_category_id = $course2;";
		$wpdb->get_results($str_sql);
		
		$str_delete_sql = "delete from pdg_course_category where course_category_id = $course2";
		$wpdb->get_results($str_delete_sql);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = "Courses were merged successfully!";
		echo json_encode($return_message);
		die();
	}

	public static function fn_update_subjects_slug() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$str_sql = "select * from pdg_subject_name where slug is NULL limit 100;";
		$subjects_list_db = $wpdb->get_results($str_sql);
		foreach($subjects_list_db as $subject_info) {
			$subject_name_id = $subject_info->subject_name_id;
			$subject_name = $subject_info->subject_name;
			$slug = sanitize_title($subject_name);
			$update_array = array(
				'slug' => $slug
			);
			$where_array = array('subject_name_id'=>$subject_name_id);
			$wpdb->update('pdg_subject_name', $update_array, $where_array, array('%s'), array('%d'));
		}
		
		$pending_subjects = $wpdb->get_var("select count(subject_name_id) from pdg_subject_name where slug is NULL");
		$pending_subjects_count = 0;
		if(!empty($pending_subjects)) {
			$pending_subjects_count = $pending_subjects;
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Subjects slugs has been modified. Pending subjects: '.$pending_subjects_count;
		
		echo json_encode($return_message );
		die();
	}
}
