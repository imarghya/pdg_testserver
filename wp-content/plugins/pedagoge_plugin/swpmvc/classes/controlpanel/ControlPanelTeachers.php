<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}
use Symfony\Component\Filesystem\Filesystem;
class ControlPanelTeachers {
	
	public static function fn_teachers_datatable() {
		global $wpdb;
		$table = 'view_pdg_teacher_extented';
		// Table's primary key
		$primaryKey = 'teacher_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'teacher_id', 'dt' => 0, 'dt_type' => 'number' ),
			array( 'db' => 'profile_slug',  'dt' => 1),
			array( 'db' => 'user_id',   'dt' => 2, 'dt_type' => 'number' ),
			array( 'db' => 'personal_info_id', 'dt' => 3, 'dt_type' => 'number' ),
			array( 'db' => 'user_name',  'dt' => 4 ),
			array( 'db' => 'user_email',   'dt' => 5),
			array( 'db' => 'first_name', 'dt' => 6 ),
			array( 'db' => 'last_name',  'dt' => 7),
			array( 'db' => 'mobile_no',   'dt' => 8 ),
			array( 'db' => 'alternative_contact_no', 'dt' => 9),
			array( 'db' => 'current_address_city',  'dt' => 10, 'dt_type' => 'number' ),
			array( 'db' => 'gender',   'dt' => 11, 'dt_type' => 'exact'),
			array( 'db' => 'plan',   'dt' => 12),
			array( 'db' => 'approved',   'dt' => 13),
			array( 'db' => 'active',   'dt' => 14),
			array( 'db' => 'verified',   'dt' => 15),
			array( 'db' => 'deleted',   'dt' => 16),
			array( 'db' => 'teacher_id',   'dt' => 17, 'dt_type' => 'number' ),
			array( 'db' => 'created',   'dt' => 18),
			array( 'db' => 'updated',   'dt' => 19),
			
			array( 'db' => 'localities',   'dt' => 20),
			array( 'db' => 'subjects',   'dt' => 21),
			array( 'db' => 'subject_type',   'dt' => 22),
			array( 'db' => 'academic_board',   'dt' => 23),
			array( 'db' => 'course_age',   'dt' => 24),
		);
		
		$sql_details = DataTablesController::$wp_db_details;
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}

	public static function fn_delete_teacher_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		//check for permission
		$has_delete_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_teacher_user_info_edit');
		if(!$has_delete_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to delete the teacher!';
			
			echo json_encode($return_message );
			die();
		}
		
		$teacher_id = $_POST['teacher_id'];
		if(!is_numeric($teacher_id) || $teacher_id <= 0) {
			$return_message['error_type'] = 'id_not_available';
			$return_message['message'] = 'Error! Teachers ID is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		$teachers_db = $wpdb->get_results("select * from view_pdg_teachers where teacher_id = $teacher_id");
		if(empty($teachers_db)) {
			$return_message['error_type'] = 'teacher_not_available';
			$return_message['message'] = 'Error! Teachers information is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		$teacher_user_id="";
		$teacher_personal_info_id = '';
		foreach($teachers_db as $teacher_info) {
			$teacher_user_id = $teacher_info->user_id;
			$personal_info_id = $teacher_info->personal_info_id;	
		}
		if(!is_numeric($teacher_user_id) || $teacher_user_id <= 0) {
			$return_message['error_type'] = 'teacher_not_available';
			$return_message['message'] = 'Error! Teachers user information is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		/**		 
		 * pdg_recommendations / userid
		 * pdg_reviews / userid
		 * pdg_teacher / userid/teacher_id
		 * pdg_teacher_qualification /teacherid
		 * pdg_teaching_achievements / userid		 
		 * pdg_user_contact_mode / userid
		 * pdg_user_info / userid 
		 * delete wordpress user
		 * delete images
		 * clear cache
		 */
		
		/**
		 * first find the batches and delete them
		 */
		$batches_db = $wpdb->get_results("select * from pdg_tuition_batch where tutor_institute_user_id = $teacher_user_id");
		foreach($batches_db as $batch_info) {
			$batch_id = $batch_info->tuition_batch_id;
			self::fn_delete_teacher_institute_batches($batch_id);
		}
		$wpdb->get_results("delete from pdg_recommendations where recommended_to = $teacher_user_id");
		$wpdb->get_results("delete from pdg_reviews where tutor_institute_user_id = $teacher_user_id");
		$wpdb->get_results("delete from pdg_teacher_qualification where teacher_id = $teacher_id");
		$wpdb->get_results("delete from pdg_teaching_achievements where user_id = $teacher_user_id");
		$wpdb->get_results("delete from pdg_user_contact_mode where user_id = $teacher_user_id");
		$wpdb->get_results("delete from pdg_user_info where user_id = $teacher_user_id");
		$wpdb->get_results("delete from pdg_teacher where user_id = $teacher_user_id");		
		
		wp_delete_user($teacher_user_id);
		
		self::fn_delete_users_images($teacher_user_id);
		
		PDGManageCache::fn_update_cache('teacher_names_data');
		PDGManageCache::fn_update_cache('registered_locality');
		PDGManageCache::fn_update_cache('registered_subjects');
		PDGManageCache::fn_update_cache('view_pdg_seo_combo');
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Teacher Information was deleted successfully!';
		
		echo json_encode($return_message );
		die();
	}

	static function fn_delete_teacher_institute_batches ($batch_id) {
		global $wpdb;
		/**
		 * pdg_batch_class_timing
		 * pdg_batch_fees
		 * pdg_batch_school
		 * pdg_batch_subject		 
		 * pdg_tuition_batch
		 * pdg_tutor_location 
		 */
		 
		 $deleted = TRUE;
		 if(!is_numeric($batch_id) || $batch_id <= 0) {
		 	$deleted = FALSE;
		 } else {
		 	$wpdb->get_results("delete from pdg_batch_class_timing where tuition_batch_id = $batch_id");
			$wpdb->get_results("delete from pdg_batch_fees where tuition_batch_id = $batch_id");
			$wpdb->get_results("delete from pdg_batch_school where pdg_batch_id = $batch_id");
			$wpdb->get_results("delete from pdg_batch_subject where tuition_batch_id = $batch_id");
			$wpdb->get_results("delete from pdg_tuition_batch where tuition_batch_id = $batch_id");
			$wpdb->get_results("delete from pdg_tutor_location where tuition_batch_id = $batch_id");	
		 }
		 return $deleted;
	}
	
	static function fn_delete_users_images($user_id) {
		//use Symfony\Component\Filesystem\Filesystem;
		$deleted = TRUE;
		 if(!is_numeric($user_id) || $user_id <= 0) {
		 	$deleted = FALSE;
		 } else {
		 	$profile_dir = PEDAGOGE_PLUGIN_DIR.'storage/uploads/images/'.$user_id;			
			$filesystem = new Symfony\Component\Filesystem\Filesystem();
		 	if ($filesystem->exists($profile_dir)) {
			    $filesystem->remove($profile_dir);
			}
		 }
		 return $deleted;
	}
	
	public static function fn_update_teacher_data() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		//check for permission
		$has_delete_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_teacher_user_info_edit');
		if(!$has_delete_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to delete the teacher!';
			
			echo json_encode($return_message );
			die();
		}
		
		$hidden_teacher_id = $_POST['hidden_teacher_id'];
		$hidden_teacher_user_id = $_POST['hidden_teacher_user_id'];
		$hidden_teacher_profile_id = $_POST['hidden_teacher_profile_id'];
		$txt_teacher_user_name = sanitize_text_field($_POST['txt_teacher_user_name']);
		$txt_teacher_email_address = sanitize_text_field($_POST['txt_teacher_email_address']);
		$txt_teacher_first_name = sanitize_text_field($_POST['txt_teacher_first_name']);
		$txt_teacher_last_name = sanitize_text_field($_POST['txt_teacher_last_name']);
		$txt_teacher_mobile_no = $_POST['txt_teacher_mobile_no'];
		$select_teacher_gender = sanitize_text_field($_POST['select_teacher_gender']);
		$select_approve_teacher = sanitize_text_field($_POST['select_approve_teacher']);
		$select_active_teacher = sanitize_text_field($_POST['select_active_teacher']);
		$select_verify_teacher = sanitize_text_field($_POST['select_verify_teacher']);
		$select_deleted_teacher = sanitize_text_field($_POST['select_deleted_teacher']);
		$txt_teacher_password = sanitize_text_field($_POST['txt_teacher_password']);
				
		if(!is_numeric($hidden_teacher_id) || !is_numeric($hidden_teacher_profile_id) || !is_numeric($hidden_teacher_user_id)) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'Teacher User/Profile ID is not available.';
			
			echo json_encode($return_message );
			die();
		}
		
		if($hidden_teacher_user_id <=1 ) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'Teacher User ID is not correct.';
			
			echo json_encode($return_message );
			die();
		}
		if(!empty($txt_teacher_mobile_no)) {
			if(strlen($txt_teacher_mobile_no)!=10 || !is_numeric($txt_teacher_mobile_no)) {
				$return_message['error_type'] = 'data_na';
				$return_message['message'] = 'Mobile no is required. It must be a unique 10 digit number.';
				
				echo json_encode($return_message );
				die();
			}
		}		
		
		if(empty($txt_teacher_user_name)) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'User Name is required. It must be unique.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(empty($txt_teacher_first_name)) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'First Name is required. It must be unique.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(!filter_var($txt_teacher_email_address, FILTER_VALIDATE_EMAIL)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please input proper email address.';
			echo json_encode($return_message);
			die();
		}
		
		if(empty($select_approve_teacher)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select approval status.';
			echo json_encode($return_message);
			die();
		}
		
		if(empty($select_active_teacher)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select activation status.';
			echo json_encode($return_message);
			die();
		}
		if(empty($select_verify_teacher)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select verification status.';
			echo json_encode($return_message);
			die();
		}
		if(empty($select_deleted_teacher)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select deletion status.';
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * Check for duplicate mobile no.
		 */
		if(!empty($txt_teacher_mobile_no)){
			$str_duplicate_mobile_sql = "select * from pdg_user_info where user_id!=$hidden_teacher_user_id and mobile_no = '$txt_teacher_mobile_no'";
			$duplicate_mobile_db = $wpdb->get_results($str_duplicate_mobile_sql);
			if(!empty($duplicate_mobile_db)) {
				$return_message['error_type'] = 'error';
				$return_message['message'] = 'Error! Duplicate Mobile no. Please change it and try again.';
				echo json_encode($return_message);
				die();
			}
		}		

		/**
		 * Check for duplicate email or user name
		 */
		$str_duplicate_user_info_sql = "
			select * from wp_users 
			where 
				ID != $hidden_teacher_user_id and 
				(user_login = '$txt_teacher_user_name' or 
				user_email = '$txt_teacher_email_address')";
		
		$duplicate_user_db = $wpdb->get_results($str_duplicate_user_info_sql);
		
		if(!empty($duplicate_user_db)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Duplicate User Name or Email. Please change it and try again.';
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * 1. Update Userdata
		 */
		$user_data = array(
			'ID' => $hidden_teacher_user_id,
			'user_login' => $txt_teacher_user_name,
			'user_email' => $txt_teacher_email_address,
			'first_name' => $txt_teacher_first_name,
			'last_name' => $txt_teacher_last_name,
		);
		
		if(!empty($txt_teacher_password)) {
			wp_set_password( $txt_teacher_password, $hidden_teacher_user_id );
		}		
		wp_insert_user($user_data);
		
		//update user name for wordpress user manually
		$wpdb->update($wpdb->users, array('user_login' => $txt_teacher_user_name), array('ID' => $hidden_teacher_user_id));
		
		/**
		 * Update user info table
		 */
		$update_user_info_array = array(
			'mobile_no' => $txt_teacher_mobile_no,
			'gender' => $select_teacher_gender,
			
		);
		$update_user_info_where_array = array(
			'user_id' => $hidden_teacher_user_id
		);
		$wpdb->update('pdg_user_info', $update_user_info_array, $update_user_info_where_array, array('%s','%s'), array('%d'));
		
		/**
		 * update teacher data
		 */
		 
		/**
		 * Fetch profile approved status for sending profile approval notification to user
		 */
		$teacher_insti_profile_data = $wpdb->get_results("select profile_slug, approved from pdg_teacher where user_id = $hidden_teacher_user_id");
		$profile_slug = '';
		$existing_approval_status = '';
		foreach($teacher_insti_profile_data as $profile_data) {
			$profile_slug = $profile_data->profile_slug;
			$existing_approval_status = $profile_data->approved;
		}
		
		
		$teacher_update_data_array = array(
			'approved' => $select_approve_teacher,
			'active' => $select_active_teacher,
			'verified' => $select_verify_teacher,
			'deleted' => $select_deleted_teacher
		);
		$teacher_update_data_where_array = array(
			'user_id' => $hidden_teacher_user_id
		);
		$wpdb->update('pdg_teacher', $teacher_update_data_array, $teacher_update_data_where_array, array('%s','%s','%s','%s'), array('%d'));
		
		PDGManageCache::fn_update_cache('teacher_names_data');
		
		/**
		 * Notify to user that profile is approved
		 */
		if($existing_approval_status == 'no') {
			if($select_approve_teacher == 'yes' && $select_active_teacher == 'yes') {
				$mail_data = array(
					'user_role' => 'teacher',
					'user_email' => $txt_teacher_email_address,
					'profile_slug' => $profile_slug
				);
				ControlPanelTeachers::fn_send_approval_mail_to_teacher_institute($mail_data);	
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Teacher\'s information was updated successfully!';
		echo json_encode($return_message);
		die();
	}

	public static function fn_send_approval_mail_to_teacher_institute($data) {
		//profile_approval_notification_to_user
		$user_role = $data['user_role'];
		$user_email = $data['user_email'];
		$profile_slug = $data['profile_slug'];
		 
		switch($user_role) {
			case 'teacher';				
				if(!empty($profile_slug)) {
					$profile_slug = home_url('/teacher/'.$profile_slug);
				}
				break;
			case 'institution';				
				if(!empty($profile_slug)) {
					$profile_slug = home_url('/institute/'.$profile_slug);
				}
				break;  
		 }
		 
		$mail_data = array(
			'to_address' => $user_email,
			'mail_type' => 'profile_approval_notification_to_user',
			'profile_link' => $profile_slug
		);
		$pedagoge_mailer = new PedagogeMailer($mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();	
	}
	
	/**
	 * Run it once every 7 days
	 */
	public static function fn_prep_incomplete_profiles_for_reminder() {
		global $wpdb;
		
		$incomplete_teacher_institute_array = array();
		
		$str_institute_sql = "
			select 
				view_pdg_institutes.*,
				CASE
					WHEN pdg_tuition_batch.tutor_institute_user_id IS NULL THEN 'No'
					ELSE 'Yes'
				END AS batch
			from view_pdg_institutes 
			left join pdg_tuition_batch on pdg_tuition_batch.tutor_institute_user_id = view_pdg_institutes.user_id
			where 
				view_pdg_institutes.approved = 'no' and
				
				view_pdg_institutes.deleted = 'no' and
				view_pdg_institutes.created <= DATE_SUB(NOW(), INTERVAL 7 DAY)
		";
		$institute_data = $wpdb->get_results($str_institute_sql);
		
		foreach($institute_data as $institute_info) {
			$profile_slug = $institute_info->profile_slug;
			$user_id = $institute_info->user_id;
			$mobile_no = $institute_info->mobile_no;
			$profile_heading = $institute_info->profile_heading;
			$batch = $institute_info->batch;
			$email_address = $institute_info->user_email;
			
			$institute_name = $institute_info->institute_name;			
			$correspondance_address = $institute_info->correspondance_address;			
			$about_coaching = $institute_info->about_coaching;			
			
			$is_empty = FALSE;
			
			if(empty($profile_slug)) {
				$is_empty = TRUE;	
			} else if(empty($institute_name)) {
				$is_empty = TRUE;
				$institute_name = 'User';
			} else if(empty($mobile_no)) {
				$is_empty = TRUE;
			} else if(empty($correspondance_address)) {
				$is_empty = TRUE;
			} else if(empty($profile_heading)) {
				$is_empty = TRUE;
			} else if(empty($about_coaching)) {
				$is_empty = TRUE;
			} else if($batch == 'no') {
				$is_empty = TRUE;
			}
			
			if($is_empty) {
				if(!array_key_exists($user_id, $incomplete_teacher_institute_array)) {
					$incomplete_teacher_institute_array[$user_id] = array(
						'name' => $institute_name,
						'email' => $email_address
					);
				}
			}
		}
		
		$str_teachers_sql = "
			select 
				view_pdg_teachers.*,
				CASE
					WHEN pdg_tuition_batch.tutor_institute_user_id IS NULL THEN 'No'
					ELSE 'Yes'
				END AS batch
			from view_pdg_teachers 
			left join pdg_tuition_batch on pdg_tuition_batch.tutor_institute_user_id = view_pdg_teachers.user_id
			where 
				view_pdg_teachers.approved = 'no' and
				
				view_pdg_teachers.deleted = 'no' and
				view_pdg_teachers.created <= DATE_SUB(NOW(), INTERVAL 7 DAY)
		";
		$teacher_data = $wpdb->get_results($str_teachers_sql);
		
		foreach($teacher_data as $teacher_info) {
			$profile_slug = $teacher_info->profile_slug;
			$user_id = $teacher_info->user_id;
			$mobile_no = $teacher_info->mobile_no;
			$profile_heading = $teacher_info->profile_heading;
			$batch = $teacher_info->batch;
			$email_address = $teacher_info->user_email;
			
			$teacher_name = trim($teacher_info->first_name.' '.$teacher_info->last_name);			
			$correspondance_address = $teacher_info->current_address;						
			
			$is_empty = FALSE;
			
			if(empty($profile_slug)) {
				$is_empty = TRUE;	
			} else if(empty($teacher_name)) {
				$is_empty = TRUE;
				$teacher_name = 'User';
			} else if(empty($mobile_no)) {
				$is_empty = TRUE;
			} else if(empty($correspondance_address)) {
				$is_empty = TRUE;
			} else if(empty($profile_heading)) {
				$is_empty = TRUE;
			} else if($batch == 'no') {
				$is_empty = TRUE;
			}
			
			if($is_empty) {
				if(!array_key_exists($user_id, $incomplete_teacher_institute_array)) {
					$incomplete_teacher_institute_array[$user_id] = array(
						'name' => $teacher_name,
						'email' => $email_address
					);
				}
			}
		}
		foreach($incomplete_teacher_institute_array as $user_id=>$user_info_array) {
			$name = $user_info_array['name'];
			$email = $user_info_array['email'];
			$insert_array = array(
				'email_address' => $email,
				'user_id' => $user_id,
				'teacher_institute_name' => $name
			);
			$wpdb->insert('pdg_incomplete_profile_for_reminder', $insert_array);
		}
	}

	public static function fn_send_incomplete_profile_reminder() {
		global $wpdb;
		
		$mail_data = $wpdb->get_results("select * from pdg_incomplete_profile_for_reminder limit 10");
		foreach($mail_data as $mail_info) {
			$name = $mail_info->teacher_institute_name;
			$reminder_id = $mail_info->reminder_id;
			$email_address = $mail_info->email_address;
			
			if (!filter_var($email_address, FILTER_VALIDATE_EMAIL) === false) {
				
				// send reminder email
				$mail_data = array(
					'to_address' => $email_address,
					'mail_type' => 'reminder_for_profile_edit',
					'user_name' => $name
				);
				$pedagoge_mailer = new PedagogeMailer($mail_data);
				$mail_process_completed = $pedagoge_mailer->sendmail();
				$pedagoge_mailer = null;
			}
			
			// delete reminder
			$wpdb->get_results("delete from pdg_incomplete_profile_for_reminder where $reminder_id = $reminder_id");
		}
	}
}
