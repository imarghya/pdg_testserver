<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelInstitute {
	
	public static function fn_institutes_datatable() {
		global $wpdb;
		$table = 'view_pdg_institutes_extented';
		// Table's primary key
		$primaryKey = 'institute_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'institute_id', 'dt' => 0, 'dt_type' => 'number' ),
			array( 'db' => 'institute_name',  'dt' => 1),
			array( 'db' => 'profile_slug',   'dt' => 2),
			array( 'db' => 'user_id', 'dt' => 3, 'dt_type' => 'number' ),
			array( 'db' => 'personal_info_id', 'dt' => 4, 'dt_type' => 'number' ),
			array( 'db' => 'user_name',  'dt' => 5 ),
			array( 'db' => 'user_email',   'dt' => 6),
			array( 'db' => 'mobile_no',   'dt' => 7 ),
			array( 'db' => 'contact_person_name',   'dt' => 8 ),
			array( 'db' => 'primary_contact_no',   'dt' => 9 ),
			array( 'db' => 'approved',   'dt' => 10),
			array( 'db' => 'active',   'dt' => 11),
			array( 'db' => 'verified',   'dt' => 12),
			array( 'db' => 'deleted',   'dt' => 13),
			array( 'db' => 'institute_id',   'dt' => 14, 'dt_type' => 'number' ),
			array( 'db' => 'created',   'dt' => 15),
			array( 'db' => 'updated',   'dt' => 16),	
			array( 'db' => 'localities',   'dt' => 17),
			array( 'db' => 'subjects',   'dt' => 18),
			array( 'db' => 'subject_type',   'dt' => 19),
			array( 'db' => 'academic_board',   'dt' => 20),
			array( 'db' => 'course_age',   'dt' => 21),	
			array( 'db' => 'city_name' , 'dt' => 22 , 'dt_type' => 'exact'),
		);
		
		$sql_details = DataTablesController::$wp_db_details;
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}

	public static function fn_delete_institute_ajax() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		//check for permission
		$has_delete_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_institute_delete');
		if(!$has_delete_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to delete the institute!';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_id = $_POST['institute_id'];
		if(!is_numeric($institute_id) || $institute_id <= 0) {
			$return_message['error_type'] = 'id_not_available';
			$return_message['message'] = 'Error! Institute ID is not available. Please try again.';			
			echo json_encode($return_message );
			die();
		}

		$institutes_db = $wpdb->get_results("select * from view_pdg_institutes where institute_id = $institute_id");
		if(empty($institutes_db)) {
			$return_message['error_type'] = 'institute_not_available';
			$return_message['message'] = 'Error! Institute information is not available. Please try again.';			
			echo json_encode($return_message );
			die();
		}

		$institute_user_id="";
		$institute_personal_info_id = '';
		foreach($institutes_db as $institute_info) {
			$institute_user_id = $institute_info->user_id;
			$institute_personal_info_id = $institute_info->personal_info_id;	
		}
		if(!is_numeric($institute_user_id) || $institute_user_id <= 0) {
			$return_message['error_type'] = 'institute_not_available';
			$return_message['message'] = 'Error! Institute user information is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		/**		 
		 * pdg_recommendations / userid
		 * pdg_reviews / userid
		 * pdg_teacher / userid/teacher_id
		 * pdg_teacher_qualification /teacherid
		 * pdg_teaching_achievements / userid		 
		 * pdg_user_contact_mode / userid
		 * pdg_user_info / userid 
		 * delete wordpress user
		 * delete images
		 * clear cache
		 */
		
		/**
		 * first find the batches and delete them
		 */
		$batches_db = $wpdb->get_results("select * from pdg_tuition_batch where tutor_institute_user_id = $institute_user_id");
		foreach($batches_db as $batch_info) {
			$batch_id = $batch_info->tuition_batch_id;
			ControlPanelTeachers::fn_delete_teacher_institute_batches($batch_id);
		}
		$wpdb->get_results("delete from pdg_recommendations where recommended_to = $institute_user_id");
		$wpdb->get_results("delete from pdg_reviews where tutor_institute_user_id = $institute_user_id");
		
		$wpdb->get_results("delete from pdg_teaching_achievements where user_id = $institute_user_id");
		$wpdb->get_results("delete from pdg_user_contact_mode where user_id = $institute_user_id");
		$wpdb->get_results("delete from pdg_user_info where user_id = $institute_user_id");
		$wpdb->get_results("delete from pdg_institutes where user_id = $institute_user_id");		
		
		wp_delete_user($institute_user_id);
		
		ControlPanelTeachers::fn_delete_users_images($institute_user_id);
		
		PDGManageCache::fn_update_cache('teacher_names_data');
		PDGManageCache::fn_update_cache('registered_locality');
		PDGManageCache::fn_update_cache('registered_subjects');
		PDGManageCache::fn_update_cache('view_pdg_seo_combo');
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Institute Information was deleted successfully!';
		
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_update_institute_data() {
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		//check for permission
		$has_delete_permission = current_user_can('manage_options') || current_user_can('pdg_cap_cp_institute_user_info_edit');
		if(!$has_delete_permission) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! You do not have the permission to update the information!';
			
			echo json_encode($return_message );
			die();
		}
		
		$hidden_institute_id = $_POST['hidden_institute_id'];
		$hidden_institute_user_id = $_POST['hidden_institute_user_id'];
		$hidden_institute_profile_id = $_POST['hidden_institute_profile_id'];
		
		$txt_institute_name = sanitize_text_field($_POST['txt_institute_name']);
		$txt_institute_mobile_no = $_POST['txt_institute_mobile_no'];
		$txt_institute_contact_person_name = sanitize_text_field($_POST['txt_institute_contact_person_name']);
		$txt_institute_primary_contact_no = sanitize_text_field($_POST['txt_institute_primary_contact_no']);
		$txt_institute_user_name = sanitize_text_field($_POST['txt_institute_user_name']);
		$txt_institute_email_address = sanitize_text_field($_POST['txt_institute_email_address']);
		$txt_institute_password = sanitize_text_field($_POST['txt_institute_password']);
		
		$select_approve_institute = sanitize_text_field($_POST['select_approve_institute']);
		$select_active_institute = sanitize_text_field($_POST['select_active_institute']);
		$select_verify_institute = sanitize_text_field($_POST['select_verify_institute']);
		$select_deleted_institute = sanitize_text_field($_POST['select_deleted_institute']);
		
				
		if(!is_numeric($hidden_institute_id) || !is_numeric($hidden_institute_user_id) || !is_numeric($hidden_institute_profile_id)) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'Institute User/Profile ID is not available.';
			
			echo json_encode($return_message );
			die();
		}
		
		if($hidden_institute_user_id <=1 ) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'Institute User ID is not correct.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(!empty($txt_institute_mobile_no)) {
			if(strlen($txt_institute_mobile_no)!=10 || !is_numeric($txt_institute_mobile_no)) {
				$return_message['error_type'] = 'data_na';
				$return_message['message'] = 'Mobile no is required. It must be a unique 10 digit number.';
				
				echo json_encode($return_message );
				die();	
			}
		}
		
		if(empty($txt_institute_user_name)) {
			$return_message['error_type'] = 'data_na';
			$return_message['message'] = 'User Name is required. It must be unique.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(!filter_var($txt_institute_email_address, FILTER_VALIDATE_EMAIL)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please input proper email address.';
			echo json_encode($return_message);
			die();
		}
		
		if(empty($select_approve_institute)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select approval status.';
			echo json_encode($return_message);
			die();
		}
		
		if(empty($select_active_institute)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select activation status.';
			echo json_encode($return_message);
			die();
		}
		if(empty($select_verify_institute)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select verification status.';
			echo json_encode($return_message);
			die();
		}
		if(empty($select_deleted_institute)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please select deletion status.';
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * Check for duplicate mobile no.
		 */
		if(!empty($txt_institute_mobile_no)) {
			$str_duplicate_mobile_sql = "select * from pdg_user_info where user_id!=$hidden_institute_user_id and mobile_no = '$txt_institute_mobile_no'";
			$duplicate_mobile_db = $wpdb->get_results($str_duplicate_mobile_sql);
			if(!empty($duplicate_mobile_db)) {
				$return_message['error_type'] = 'error';
				$return_message['message'] = 'Error! Duplicate Mobile no. Please change it and try again.';
				echo json_encode($return_message);
				die();
			}	
		}		

		/**
		 * Check for duplicate email or user name
		 */
		$str_duplicate_user_info_sql = "
			select * from wp_users 
			where 
				ID != $hidden_institute_user_id and 
				(user_login = '$txt_institute_user_name' or 
				user_email = '$txt_institute_email_address')";
		
		$duplicate_user_db = $wpdb->get_results($str_duplicate_user_info_sql); 
		if(!empty($duplicate_user_db)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Duplicate User Name or Email. Please change it and try again.';
			echo json_encode($return_message);
			die();
		}
		
		/**
		 * 1. Update Userdata
		 */
		$user_data = array(
			'ID' => $hidden_institute_user_id,
			'user_login' => $txt_institute_user_name,
			'user_email' => $txt_institute_email_address,
		);
		if(!empty($txt_institute_password)) {
			wp_set_password( $txt_institute_password, $hidden_institute_user_id );
		}
		
		wp_insert_user($user_data);
		//update user name for wordpress user manually
		$wpdb->update($wpdb->users, array('user_login' => $txt_institute_user_name), array('ID' => $hidden_institute_user_id));
		
		/**
		 * Update user info table
		 */
		$update_user_info_array = array(
			'mobile_no' => $txt_institute_mobile_no
		);
		$update_user_info_where_array = array(
			'user_id' => $hidden_institute_user_id
		);
		$wpdb->update('pdg_user_info', $update_user_info_array, $update_user_info_where_array, array('%s','%s'), array('%d'));
		
		/**
		 * update teacher data
		 */
		 		 
		/**
		 * Fetch profile approved status for sending profile approval notification to user
		 */
		$str_sql = "select profile_slug, approved from pdg_institutes where user_id = $hidden_institute_user_id";		
		$teacher_insti_profile_data = $wpdb->get_results($str_sql);	
		
		$profile_slug = '';
		$existing_approval_status = '';
		foreach($teacher_insti_profile_data as $profile_data) {
			$profile_slug = $profile_data->profile_slug;
			$existing_approval_status = $profile_data->approved;
		}
		 
		$teacher_update_data_array = array(
			'institute_name' => $txt_institute_name,
			'contact_person_name' => $txt_institute_contact_person_name,
			'primary_contact_no' => $txt_institute_primary_contact_no,
			'approved' => $select_approve_institute,
			'active' => $select_active_institute,
			'verified' => $select_verify_institute,
			'deleted' => $select_deleted_institute
		);
		$teacher_update_data_where_array = array(
			'user_id' => $hidden_institute_user_id
		);
		$wpdb->update('pdg_institutes', $teacher_update_data_array, $teacher_update_data_where_array, array('%s','%s','%s','%s','%s','%s','%s'), array('%d'));
		
		PDGManageCache::fn_update_cache('teacher_names_data');
		
		/**
		 * Notify to user that profile is approved
		 */
		if($existing_approval_status == 'no') {
			if($select_approve_teacher == 'yes' && $select_active_institute == 'yes') {
				$mail_data = array(
					'user_role' => 'institution',
					'user_email' => $txt_institute_email_address,
					'profile_slug' => $profile_slug
				);
				ControlPanelTeachers::fn_send_approval_mail_to_teacher_institute($mail_data);	
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Institute\'s information was updated successfully!';
		echo json_encode($return_message);
		die();
	}
}
