<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelLocality {
	public static function fn_load_clubbed_locality() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$locality_id = $_POST['master_locality_id'];
		if(!is_numeric($locality_id) || $locality_id <= 0) {
			$return_message['error_type'] = 'id_not_available';
			$return_message['message'] = 'Error! Locality ID is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		//view_pdg_clubbed_locality
		$str_sql = "select * from view_pdg_clubbed_locality where master_locality_id = $locality_id";
		$clubbed_locality_data = $wpdb->get_results($str_sql);
		if(empty($clubbed_locality_data)) {
			
			$return_message['message'] = 'Error! There is no locality grouping! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>							
							<th>Master Locality Name</th>							
							<th>Child Locality Name</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
		';
		foreach($clubbed_locality_data as $locality_data) {
			$club_id = $locality_data->club_id;
			$str_return .= '
				<tr>
					<td>'.$club_id.'</td>
					<td>'.$locality_data->master_locality_name.'</td>
					<td>'.$locality_data->child_locality_name.'</td>
					<td>
						<button class="btn btn-danger cmd_delete_clubbed_locality" data-club_id="'.$club_id.'" data-loading-text="Deleting...">Delete</button>
					</td>
				</tr>
			';
		}
		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Locality groups loaded successfully!';
		$return_message['data'] = $str_return;
		
		echo json_encode($return_message );
		die();
	}

	public static function fn_save_clubbed_locality() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$locality_id = $_POST['master_locality_id'];
		if(!is_numeric($locality_id) || $locality_id <= 0) {
			$return_message['error_type'] = 'id_not_available';
			$return_message['message'] = 'Error! Please select master locality and try again.';
			
			echo json_encode($return_message );
			die();
		}
		$children_localities = $_POST['children_localities'];
		if(!is_array($children_localities)) {
			$return_message['error_type'] = 'id_not_available';
			$return_message['message'] = 'Error! Please select child localities and try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$localities_in_db = $wpdb->get_results("select * from pdg_clubbed_locality where master_locality_id = $locality_id");
		$available_locality_array[] = $locality_id;
		foreach($localities_in_db as $locality_db) {
			$child_loc_id_in_db = $locality_db->child_locality_id;
			if(!in_array($child_loc_id_in_db, $available_locality_array)) {
				$available_locality_array[] = $child_loc_id_in_db;
			}
		}
		
		foreach($children_localities as $child_locality_id) {
			if(is_numeric($child_locality_id)) {
				if(!in_array($child_locality_id, $available_locality_array)) {
					$insert_array = array(
						'master_locality_id' => $locality_id,
						'child_locality_id' => $child_locality_id
					);
					$wpdb->insert('pdg_clubbed_locality', $insert_array);
					$available_locality_array[] = $child_locality_id;
				}	
			}
			
		}
				
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Localities were clubbed together successfully!';
		
		echo json_encode($return_message );
		die();
	}

	public static function fn_delete_clubbed_locality_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$club_id = $_POST['club_id'];
		if(!is_numeric($club_id) || $club_id <= 0) {
			$return_message['error_type'] = 'id_not_available';
			$return_message['message'] = 'Error! Locality club ID is not correct please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$wpdb->get_results("delete from pdg_clubbed_locality where club_id = $club_id");
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Clubbed Locality deleted successfully!';
		
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_locality_list_datatable() {
		global $wpdb;
		$table = 'view_pdg_locality';		
		$primaryKey = 'locality_id';
		
		$columns = array(
			array( 'db' => 'locality_id', 'dt' => 0 ),
			array( 'db' => 'country_id',   'dt' => 1 ),
			array( 'db' => 'country_name',   'dt' => 2 ),
			array( 'db' => 'state_id',   'dt' => 3 ),
			array( 'db' => 'state_name',   'dt' => 4 ),
			array( 'db' => 'city_id',   'dt' => 5 ),
			array( 'db' => 'city_name',   'dt' => 6 ),			
			array( 'db' => 'locality',  'dt' => 7 ),
			array( 'db' => 'locality_id',   'dt' => 8 ),
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	public static function save_locality() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}

		$city_id = $_POST['dynamicData']['cityID'];
		$locality_id = $_POST['dynamicData']['localityID'];
		pedagoge_applog($_POST['dynamicData']['localityName']);
		$locality_name = sanitize_text_field($_POST['dynamicData']['localityName']);
		//pedagoge_applog(print_r($_POST, TRUE));
		if(!is_numeric($city_id) || $city_id <= 0) {
			$return_message['message'] = 'Please select a city';
			echo json_encode($return_message);
			die();
		}
		if(strlen($locality_name) <= 0) {
			$return_message['message'] = 'Please input locality name';
			echo json_encode($return_message);
			die();
		}
		
		if(!is_numeric($locality_id)) {
			//insert
			//find duplicate
			$str_sql = "select * from pdg_locality where city_id=$city_id && locality = '$locality_name'";
			$query_result = $wpdb->get_results($str_sql);
			if(!empty($query_result)) {
				$return_message['message'] = 'Locality Name is already registered for the city! Please input another name.';
				echo json_encode($return_message);
				die();
			}

			$insert_array = array(
				'locality' => $locality_name,
				'city_id' => $city_id
			);
			
			$wpdb->insert('pdg_locality', $insert_array, array('%s', '%d'));
			
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Locality was created successfully!';
			echo json_encode($return_message);
			die();

		} else {
			//update
			//find duplicate
			$str_sql = "select * from pdg_locality where city_id=$city_id && locality = '$locality_name' && locality_id !=$locality_id";
			$query_result = $wpdb->get_results($str_sql);
			if(!empty($query_result)) {
				$return_message['message'] = 'Locality Name is already registered for the city! Please input another name.';
				echo json_encode($return_message);
				die();
			}
			$update_array = array(
				'locality' => $locality_name,
				'city_id' => $city_id
			);
			$where_array = array(
				'locality_id' => $locality_id
			);
			
			$wpdb->update('pdg_locality', $update_array, $where_array, array('%s', '%d'), array('%d'));
			
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Locality was updated successfully!';
			echo json_encode($return_message);
			die();
		}
	}	
}