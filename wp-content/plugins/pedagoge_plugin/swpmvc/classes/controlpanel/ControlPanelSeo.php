<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelSeo {
	
	public static function fn_save_seo_options() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$teacher_title = sanitize_text_field($_POST['teacher_title']);
		$teacher_meta = sanitize_text_field($_POST['teacher_meta']);
		$institute_title = sanitize_text_field($_POST['institute_title']);
		$institute_meta = sanitize_text_field($_POST['institute_meta']);
		
		$str_subject_template = '{{{subject}}}';
		$str_city_template = '{{{city}}}';
		
		if(empty($teacher_title) || strpos($teacher_title, $str_subject_template) == FALSE || strpos($teacher_title, $str_city_template) == FALSE) {			
			$return_message['message'] = "Please fill the teacher title input fields properly and include $str_subject_template and $str_city_template in your templates.";
			echo json_encode($return_message );
			die();
		}
		
		if(empty($teacher_meta) || strpos($teacher_meta, $str_subject_template) == FALSE || strpos($teacher_meta, $str_city_template) == FALSE) {			
			$return_message['message'] = "Please fill the teacher meta input fields properly and include $str_subject_template and $str_city_template in your templates.";
			echo json_encode($return_message );
			die();
		}
		
		if(empty($institute_title) || strpos($institute_title, $str_subject_template) == FALSE || strpos($institute_title, $str_city_template) == FALSE) {			
			$return_message['message'] = "Please fill the institute title input fields properly and include $str_subject_template and $str_city_template in your templates.";
			echo json_encode($return_message );
			die();
		}
		
		if(empty($institute_meta) || strpos($institute_meta, $str_subject_template) == FALSE || strpos($institute_meta, $str_city_template) == FALSE) {			
			$return_message['message'] = "Please fill the institute meta input fields properly and include $str_subject_template and $str_city_template in your templates.";
			echo json_encode($return_message );
			die();
		}
		
		
		$pdg_teacher_seo_title_option_name = 'pdg_teacher_seo_title_phrase';
		update_option( $pdg_teacher_seo_title_option_name, $teacher_title );
		
		$pdg_institute_seo_title_option_name = 'pdg_institute_seo_title_phrase';
		update_option( $pdg_institute_seo_title_option_name, $institute_title );
		
		$pdg_teacher_seo_meta_option_name = 'pdg_teacher_seo_meta_phrase';
		update_option( $pdg_teacher_seo_meta_option_name, $teacher_meta );
		
		$pdg_institute_seo_meta_option_name = 'pdg_institute_seo_meta_phrase';
		update_option( $pdg_institute_seo_meta_option_name, $institute_meta );
		
		$str_sql = "update pdg_seo_urls set updated = 'no'";
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'SEO Options saved. Now update the URLs.';
		echo json_encode($return_message);
		die();
		
	}
	

	public static function fn_create_seo_combination() {
				
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		
		global $wpdb;
		
		$subject_locality_array = array();
		
		$str_sql = "
			select	
				distinct
				view_course_category.subject_name_id,				
				view_pdg_locality.city_id,				
				pdg_user_info.user_role_id,
				pdg_user_role.user_role_name
			from pdg_batch_subject
			left join pdg_tutor_location on pdg_tutor_location.tuition_batch_id = pdg_batch_subject.tuition_batch_id
			left join view_pdg_locality on pdg_tutor_location.locality_id = view_pdg_locality.locality_id
			left join view_course_category on view_course_category.course_category_id = pdg_batch_subject.course_category_id
			left join pdg_tuition_batch on pdg_tuition_batch.tuition_batch_id = pdg_batch_subject.tuition_batch_id
			left join pdg_user_info on pdg_user_info.user_id = pdg_tuition_batch.tutor_institute_user_id
			left join pdg_user_role on pdg_user_role.user_role_id = pdg_user_info.user_role_id
		";
		
		$subjects_city_db = $wpdb->get_results($str_sql);
		foreach($subjects_city_db as $subject_city) {
			$city_id = $subject_city->city_id;
			$subject_id = $subject_city->subject_name_id;
			$role = $subject_city->user_role_name;
			
			if(!empty($city_id) && !empty($subject_id) && !empty($role)) {
				$hash = md5($city_id.'#'.$subject_id.'#'.$role);
				if(!array_key_exists($hash, $subject_locality_array)) {
					$subject_locality_array[$hash]['city'] = $city_id;
					$subject_locality_array[$hash]['subject'] = $subject_id;
					$subject_locality_array[$hash]['role'] = $role;
				}	
			}
		}		
			
		$seo_db = $wpdb->get_results("select hash from pdg_seo_combo");
		$seo_hash_array = array();
		foreach($seo_db as $seo_info) {
			$seo_hash_array[] = $seo_info->hash;
		}
		
		$new_array = array();
		foreach($subject_locality_array as $key=>$value) {			
			if(!in_array($key, $seo_hash_array)) {
					
				$subject_id = $value['subject'];
				$city_id = $value['city'];				
				$role = $value['role'];
				
				$insert_array = array(
					'subject_id' => $subject_id,
					'city_id' => $city_id,
					'hash' => $key,
					'role' => $role
				);
				$format_array = array('%d', '%d', '%s', '%s');
				$wpdb->insert('pdg_seo_combo', $insert_array);
				$seo_hash_array[] = $key;				
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'URLS combination generated...';
		echo json_encode($return_message);
		die();				
	}

	public static function fn_clear_seo_urls_cache() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		
		PDGManageCache::fn_update_cache('view_pdg_seo_combo');
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Great! SEO URLs cache has been primed for action! :)';
		echo json_encode($return_message);
		die();
	}
	
	public static function fn_return_seo_options($option_name) {
		$option_value = '';
		
		$pdg_teacher_seo_title_option_name = 'pdg_teacher_seo_title_phrase';
		$pdg_institute_seo_title_option_name = 'pdg_institute_seo_title_phrase';
		
		$pdg_teacher_seo_meta_option_name = 'pdg_teacher_seo_meta_phrase';
		$pdg_institute_seo_meta_option_name = 'pdg_institute_seo_meta_phrase';		
		
		$deprecated = null;
		$autoload = 'yes';
		
		switch($option_name) {
			case 'teacher_title':
				$option_value =  get_option( $pdg_teacher_seo_title_option_name );
				break;
			case 'teacher_meta':
				$pdg_teacher_seo_meta =  get_option( $pdg_teacher_seo_meta_option_name );
				break;
			case 'institute_title':
				$pdg_institute_seo_title = get_option( $pdg_institute_seo_title_option_name );
				break;
			case 'institute_meta':
				$pdg_institute_seo_meta = get_option( $pdg_institute_seo_meta_option_name );
				break;
		}
		if($option_value == FALSE) {
			switch($option_name) {
				case 'teacher_title':					
					$option_value = sanitize_text_field('Find {{{subject}}} tuition teachers in {{{city}}}');
	    			add_option( $pdg_teacher_seo_title_option_name, $option_value, $deprecated, $autoload );
					break;
				case 'teacher_meta':					
					$option_value = sanitize_text_field('Find {{{subject}}} tuition teachers in {{{city}}} at Pedagoge');
	    			add_option( $pdg_teacher_seo_meta_option_name, $option_value, $deprecated, $autoload );
					break;
				case 'institute_title':
					$option_value = sanitize_text_field('Find {{{subject}}} coaching classes in {{{city}}}');
	    			add_option( $pdg_institute_seo_title_option_name, $option_value, $deprecated, $autoload );
					break;
				case 'institute_meta':					
					$option_value = sanitize_text_field('Find {{{subject}}} coaching classes in {{{city}}} at Pedagoge');
	    			add_option( $pdg_institute_seo_meta_option_name, $option_value, $deprecated, $autoload );
					break;
			}
		}
		return $option_value;
	}
	
	public static function fn_return_seo_urls_list() {
		$str_return = '<h3>Sorry! URLs List is not available. Please create URL combinations first.</h3>';
				
		$seo_combos_db = PDGManageCache::fn_load_cache('view_pdg_seo_combo');		
		if(!empty($seo_combos_db)) {
			$str_return = '<h3>SEO URL List</h3><ul>';			
		}
		
		foreach($seo_combos_db as $seo_combos) {
			$subject_name = $seo_combos->subject_name;
			$subject_slug = $seo_combos->subject_slug;
			
			$city_name = $seo_combos->city_name;
			$city_slug = $seo_combos->city_slug;
			
			$user_role = $seo_combos->role;
			$url_title = $seo_combos->title;
			
			$seo_user_role_name = "";
			switch($user_role) {
				case 'teacher':
					$seo_user_role_name = sanitize_title("Tuition Teachers");
					break;
				case 'institution':
					$seo_user_role_name = sanitize_title("Coaching Classes");
					break;
			}
			
			$seo_url = home_url('/search/'.$city_slug.'/'.$subject_slug.'/'.$seo_user_role_name); 
			if(empty($url_title)) {
				$url_title = self::fn_return_seo_title($city_name, $subject_name, $user_role);
			}
			
			$str_return.='
				<li><a href="'.$seo_url.'" title="'.$url_title.'" target="_blank">'.$url_title.'</a></li>
			';
		}
		
		if(!empty($seo_combos_db)) {
			$str_return .= '</ul>';
		}
		
		return $str_return; 
	}

	public static function fn_return_seo_title($city_name, $subject_name, $role='') {
		$str_return = '';
		$pdg_teacher_seo_title =  ControlPanelSeo::fn_return_seo_options('teacher_title');
		$pdg_institute_seo_title = ControlPanelSeo::fn_return_seo_options('institute_title');
		switch($role) {
			case 'teacher':
				$str_return = str_replace('{{{subject}}}', $subject_name, $pdg_teacher_seo_title);
				$str_return = str_replace('{{{city}}}', $city_name, $str_return);
				break;
			case 'institution':
				$str_return = str_replace('{{{subject}}}', $subject_name, $pdg_institute_seo_title);
				$str_return = str_replace('{{{city}}}', $city_name, $str_return);
				break;
			default:
				$str_return = "Find $subject_name tuition teacher and coaching classes in $city_name";
				break;
		}
		return $str_return;
	}
	
	public static function fn_return_seo_meta($city_name, $subject_name, $role='') {
		$str_return = '';
		$pdg_teacher_seo_meta =  ControlPanelSeo::fn_return_seo_options('teacher_meta');
		$pdg_institute_seo_meta = ControlPanelSeo::fn_return_seo_options('institute_meta');
		switch($role) {
			case 'teacher':
				$str_return = str_replace('{{{subject}}}', $subject_name, $pdg_teacher_seo_meta);
				$str_return = str_replace('{{{city}}}', $city_name, $str_return);
				break;
			case 'institution':
				$str_return = str_replace('{{{subject}}}', $subject_name, $pdg_institute_seo_meta);
				$str_return = str_replace('{{{city}}}', $city_name, $str_return);
				break;
			default:
				$str_return = "Find $subject_name tuition teacher and coaching classes in $city_name at Pedagoge";
				break;
		}
		return $str_return;
	}
}