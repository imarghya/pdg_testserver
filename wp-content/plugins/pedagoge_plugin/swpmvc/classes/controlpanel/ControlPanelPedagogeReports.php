<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelPedagogeReports {
	
	public static function fn_cp_reports_generator_ajax() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$report_type = $_POST['report_type'];
		
		if(method_exists('ControlPanelPedagogeReports', $report_type)) {
			$return_data = ControlPanelPedagogeReports::$report_type();
			//$return_data = call_user_func(array('ControlPanelPedagogeReports', $report_type));
			echo json_encode($return_data);
			die();
		} else {
			$return_message['error_type'] = 'report_type_undefined';
			$return_message['message'] = 'Report type is undefined. Please select a report type!';			
			echo json_encode($return_message);
			die();
		}
	}

	public static function empty_localities() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_sql = "
			SELECT * FROM view_pdg_locality
			where locality_id not in(select distinct locality_id from pdg_tutor_location)
		";
		$empty_localities_db = $wpdb -> get_results($str_sql);
		if (empty($empty_localities_db)) {
			$return_message['error_type'] = 'no_result_found';
			$return_message['message'] = "Error! No data available.";
			return $return_message;
		}
		$locality_array = array();
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Locality</th>
							<th>City</th>
							<th>State</th>
							<th>Country</th>
						</tr>
					</thead>
					<tbody>
	    ';

		foreach ($empty_localities_db as $empty_locality) {
			$str_return .= '
				<tr>
					<td>' . $empty_locality -> locality_id . '</td>
					<td>' . $empty_locality -> locality . '</td>
					<td>' . $empty_locality -> city_name . '</td>
					<td>' . $empty_locality -> state_name . '</td>
					<td>' . $empty_locality -> country_name . '</td>
				</tr>
			';
			$locality_array[] = array(
				'ID' => $empty_locality -> locality_id,
				'Locality' => $empty_locality -> locality,
				'City' => $empty_locality -> city_name,
				'State' => $empty_locality -> state_name,
				'Country' => $empty_locality -> country_name,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		$excel_data_array = array(
			'report_title' => 'empty_locality',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'empty_locality',
			'data' => $locality_array
		);
		$excel_file = new PDGExcelExport($excel_data_array);
				
		$excel_file_location = $excel_file->ExcelExport();
		
		$return_message['excel'] = $excel_file_location;
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;
		return $return_message;
	}

	public static function empty_subjects() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_sql = "
			select 
				subject_name_id,
				subject_name
			from 
				pdg_subject_name 
			where 
				subject_name_id not in 
				(
					select 
						distinct(pdg_course_category.subject_name_id) 
					from 
						pdg_batch_subject 
					left join pdg_course_category on pdg_batch_subject.course_category_id = pdg_course_category.course_category_id
				)
		";

		$empty_subjects_db = $wpdb -> get_results($str_sql);
		if (empty($empty_subjects_db)) {
			$return_message['error_type'] = 'no_result_found';
			$return_message['message'] = "Error! No data available.";
			return $return_message;
		}

		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>NAME</th>
						</tr>
					</thead>
					<tbody>
						
					
	    ';
		$empty_subjects_array = array();
		foreach ($empty_subjects_db as $empty_subjects) {
			$str_return .= '
				<tr>
					<td>' . $empty_subjects -> subject_name_id. '</td>
					<td>' . $empty_subjects -> subject_name . '</td>
				</tr>
			';
			$empty_subjects_array[] = array(
				'ID' => $empty_subjects -> subject_name_id,
				'NAME' => $empty_subjects -> subject_name,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'empty_subjects',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'empty_subjects',
			'data' => $empty_subjects_array
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}
	
	public static function empty_course() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_sql = "
			select 
				* 
			from 
				view_course_category
			where 
				course_category_id not in( 
					select distinct(course_category_id) from pdg_batch_subject
				)
		";

		$empty_course_db = $wpdb -> get_results($str_sql);
		if (empty($empty_course_db)) {
			$return_message['error_type'] = 'no_result_found';
			$return_message['message'] = "Error! No data available.";
			return $return_message;
		}

		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Subject Name</th>
							<th>Subject Type</th>
							<th>Course Type</th>
							<th>Academic Board</th>
							<th>Age</th>
							<th>created</th>
						</tr>
					</thead>
					<tbody>
						
					
	    ';
		$array_empty_course = array();
		foreach ($empty_course_db as $empty_course) {
			$str_return .= '
				<tr>
					<td>' . $empty_course -> course_category_id. '</td>
					<td>' . $empty_course -> subject_name . '</td>
					<td>' . $empty_course -> subject_type. '</td>
					<td>' . $empty_course -> course_type . '</td>
					<td>' . $empty_course -> academic_board. '</td>
					<td>' . $empty_course -> course_age . '</td>
					<td>' . $empty_course -> created. '</td>
				</tr>
			';
			$array_empty_course[] = array(
				'ID' => $empty_course -> course_category_id,
				'Subject Name' => $empty_course -> subject_name,
				'Subject Type' => $empty_course -> subject_type,
				'Course Type' => $empty_course -> course_type,
				'Academic Board' => $empty_course -> academic_board,
				'Age' => $empty_course -> course_age,
				'created' => $empty_course -> created,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'empty_course',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'empty_course',
			'data' => $array_empty_course
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}	
	
	public static function teachers_in_subjects() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_sql = "
			select	
				distinct pdg_course_category.subject_name_id,
				pdg_subject_name.subject_name,
				count(distinct(pdg_tuition_batch.tutor_institute_user_id)) as total_teachers
			from pdg_batch_subject
			left join pdg_tuition_batch on pdg_tuition_batch.tuition_batch_id = pdg_batch_subject.tuition_batch_id
			left join pdg_course_category on pdg_batch_subject.course_category_id = pdg_course_category.course_category_id
			left join pdg_subject_name on pdg_subject_name.subject_name_id = pdg_course_category.subject_name_id
			group by pdg_course_category.subject_name_id
		";

		$teachers_subjects_db = $wpdb -> get_results($str_sql);
		if (empty($teachers_subjects_db)) {
			$return_message['error_type'] = 'no_result_found';
			$return_message['message'] = "Error! No data available.";
			return $return_message;
		}

		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Subject</th>
							<th>Total Teachers</th>
						</tr>
					</thead>
					<tbody>
	    ';
		$array_teacher_subjects = array();
		foreach ($teachers_subjects_db as $teachers_subject) {
			$str_return .= '
				<tr>
					<td>' . $teachers_subject -> subject_name_id. '</td>
					<td>' . $teachers_subject -> subject_name . '</td>
					<td>' . $teachers_subject -> total_teachers . '</td>
				</tr>
			';
			$array_teacher_subjects[] = array(
				'ID' => $teachers_subject -> subject_name_id,
				'Subject' => $teachers_subject -> subject_name,
				'Total Teachers' => $teachers_subject -> total_teachers,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'teacher_in_subjects',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'teacher_in_subjects',
			'data' => $array_teacher_subjects
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		
		$return_message['excel'] = $excel_file->ExcelExport();;
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function reviews_reports() {
		echo json_encode(self::fn_dated_reports_form());
		die();
	}
	
	public static function teacher_registration() {
		echo json_encode(self::fn_dated_reports_form());
		die();
	}
	
	public static function institute_registration() {
		echo json_encode(self::fn_dated_reports_form());
		die();
	}

	public static function master_report() {
		echo json_encode(self::fn_dated_reports_form());
		die();
	}

	public static function fn_dated_reports_form() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '
			<h3>Please select date for Datewise reports</h3>
			<div class="row">
				<div class="col-md-4">
					<label for="txt_report_from_date">From Date (YYYY-MM-DD)</label>
					<input type="text" class="form-control" id="txt_report_from_date"/>
				</div>
				<div class="col-md-4">
					<label for="txt_report_to_date">To Date (YYYY-MM-DD)</label>
					<input type="text" class="form-control" id="txt_report_to_date"/>
				</div>
				<div class="col-md-4">
					<br />
					<button class="btn btn-success col-md-12 cmd_generate_dated_reports" data-loading-text="Generating reports...">Generate Report</button>
				</div>
			</div>
			<div id="div_dated_report_result_area"></div>
			<hr />
		';

		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}
	
	public static function fn_cp_dated_reports_generator_ajax() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$from_date = sanitize_text_field($_POST['from_date']);
		$to_date = sanitize_text_field($_POST['to_date']);
		
		$report_dates = array();
		if(!empty($from_date) || !empty($to_date)) {
			$date_regex = '/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/';
			
			if (!preg_match($date_regex, $from_date)) {
			    $return_message['error_type'] = 'invalid_date';
				$return_message['message'] = 'From date is invalid. Please check your input.';
				
				echo json_encode($return_message );
				die();
			}
	
			if (!preg_match($date_regex, $to_date)) {
			    $return_message['error_type'] = 'invalid_date';
				$return_message['message'] = 'To date is invalid. Please check your input.';
				
				echo json_encode($return_message );
				die();
			}
			$report_dates['from_date'] = $from_date;
			$report_dates['to_date'] = $to_date;
		}
		
		$report_type = $_POST['report_type'];
		
		switch($report_type) {
			case 'reviews_reports':
				$return_data = self::fn_review_dated_reports($report_dates);
				echo json_encode($return_data);
				die();
				break;
			case 'teacher_registration':
				$return_data = self::fn_teacher_registration_reports($report_dates);
				echo json_encode($return_data);
				die();
				break;
			case 'institute_registration':
				$return_data = self::fn_institute_registration_reports($report_dates);
				echo json_encode($return_data);
				die();
				break;
			case 'master_report':
				$return_data = self::fn_master_reports($report_dates);
				echo json_encode($return_data);
				die();
				break;
			default:
				$return_message['error_type'] = 'report_type_undefined';
				$return_message['message'] = 'Report type is undefined. Please select a report type!';
				break; 
		}
		
		echo json_encode($return_message);
		die();
	}

	public static function fn_review_dated_reports($report_dates) {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = "";
		
		$str_sql = "select * from view_pdg_reviews ";
		if(!empty($report_dates)) {
			$from_date = $report_dates['from_date'];
			$to_date = $report_dates['to_date'];
			$str_sql .= " where (created between '$from_date 00:00:00' and '$to_date 23:59:59')";
		}
		
		$review_data = $wpdb->get_results($str_sql);
		
		if(empty($review_data)) {
			$str_return = '<h3>Sorry! Data is not available.</h3>';
		} else {
			// table
			$str_return = '
				<div class="table-responsive">
					<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Reviewer Name</th>
								<th>Reviewer Role</th>
								<th>Teacher Name</th>
								<th>Teacher Role</th>
								<th>Review Text</th>
								<th>Star1</th>
								<th>Star2</th>
								<th>Star3</th>
								<th>Star4</th>
								<th>Average</th>
								<th>Overall</th>
								<th>Anonymous</th>
								<th>Approved</th>
								<th>Rejected</th>
								<th>Created</th>
							</tr>
						</thead>
						<tbody>
		    ';
			$array_review = array();
			foreach ($review_data as $review_info) {
				$str_return .= '
					<tr>
						<td>'.$review_info->review_id.'</td>
						<td>'.$review_info->student_name.'</td>
						<td>'.$review_info->student_role.'</td>
						<td>'.$review_info->teacher_name.'</td>
						<td>'.$review_info->teacher_role.'</td>
						<td>'.$review_info->review_text.'</td>
						<td>'.$review_info->rating1.'</td>
						<td>'.$review_info->rating2.'</td>
						<td>'.$review_info->rating3.'</td>
						<td>'.$review_info->rating4.'</td>
						<td>'.$review_info->average_rating.'</td>
						<td>'.$review_info->overall_rating.'</td>
						<td>'.$review_info->is_anonymous.'</td>
						<td>'.$review_info->is_approved.'</td>
						<td>'.$review_info->is_rejected.'</td>
						<td>'.$review_info->created.'</td>
					</tr>
				';
				$array_review[] = array(
					'ID' => $review_info->review_id,
					'Reviewer Name' => $review_info->student_name,
					'Reviewer Role' => $review_info->student_role,
					'Teacher Name' => $review_info->teacher_name,
					'Teacher Role' => $review_info->teacher_role,
					'Review Text' => $review_info->review_text,
					'Star1' => $review_info->rating1,
					'Star2' => $review_info->rating2,
					'Star3' => $review_info->rating3,
					'Star4' => $review_info->rating4,
					'Average' => $review_info->average_rating,
					'Overall' => $review_info->overall_rating,
					'Anonymous' => $review_info->is_anonymous,
					'Approved' => $review_info->is_approved,
					'Rejected' => $review_info->is_rejected,
					'Created' => $review_info->created,
				);
			}
	
			$str_return .= '
						</tbody>
					</table>
				</div>
			';
		}
		$excel_data_array = array(
			'report_title' => 'Reviews',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'Reviews',
			'data' => $array_review
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	//fn_reviews_reports
	public static function locality_and_subject() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		$subject_name_id = $_POST['subject_name_id'];
		$locality_name_id = $_POST['locality_name_id'];
		
		if(!is_numeric($subject_name_id) && !is_numeric($locality_name_id)) {
			$return_message['error_type'] = 'incorrect_data';
			$return_message['message'] = 'Please select Subject or Locality for this report';			
			echo json_encode($return_message );
			die();
		}
		
		$arr_users_by_subjects = array ();
		if(is_numeric($subject_name_id)) {
			$str_sql = "
				select distinct(tutor_institute_user_id) as found_user_id from view_pdg_batch_subject where subject_name_id = $subject_name_id
			";
			
			$found_users_by_subjects = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_subjects ) ) {
				foreach ( $found_users_by_subjects as $found_user ) {
					$found_user_id = $found_user->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_subjects ) ) {
						$arr_users_by_subjects[] = $found_user_id;
					}
				}
			}	
		}
		
		$arr_users_by_locality = array ();
		if(is_numeric($locality_name_id)) {
			$str_sql = "
				select distinct(tutor_institute_user_id) as found_user_id from view_pdg_tutor_location where locality_id = $locality_name_id
			";
			
			$found_users_by_locality_name = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_locality_name ) ) {
				foreach ( $found_users_by_locality_name as $locations_search_data ) {
					$found_user_id = $locations_search_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_locality ) ) {
						$arr_users_by_locality[] = $found_user_id;
					}
				}
			}
		}
		$found_users_id_array = array();
		if(is_numeric($subject_name_id) && is_numeric($locality_name_id)) {
			$found_users_id_array = array_intersect( $arr_users_by_subjects, $arr_users_by_locality );	
		} else if(is_numeric($subject_name_id)) {
			$found_users_id_array = $arr_users_by_subjects;
		} else if(is_numeric($locality_name_id)) {
			$found_users_id_array = $arr_users_by_locality;
		}
		
		
		
		if(empty($found_users_id_array)) {
			$return_message['message'] = 'Sorry! No Teacher or Institute found!';			
			echo json_encode($return_message );
			die();
		} else {
			$str_users_id_list = implode( ',', $found_users_id_array );
			//SELECT * FROM `view_pdg_teachers_institutes`
			$str_sql = "select * from view_pdg_teachers_institutes where user_id in ($str_users_id_list)";
			$found_users_db = $wpdb->get_results($str_sql);
			if(empty($found_users_db)) {
				$return_message['message'] = 'Sorry! No Teacher or Institute found!';			
				echo json_encode($return_message );
				die();
			}
			$str_return = '
				<div class="table-responsive">
					<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>																
								<th>Teacher Name</th>
								<th>Teacher Role</th>
								<th>Contact Number</th>
								<th>Link</th>								
							</tr>
						</thead>
						<tbody>
		    ';
			$array_locality_report = array();
			foreach ($found_users_db as $found_user) {
				$str_user_role = $found_user->user_role_name;
				$str_profile_slug = $found_user->profile_slug;
				$str_profile_link = '';
				
				switch($str_user_role) {
					case 'teacher':
						$str_profile_link = home_url('/teacher/'.$str_profile_slug);
						break;
					case 'institution':
						$str_profile_link = home_url('/institute/'.$str_profile_slug);
						break;
				}
				$str_return .= '
					<tr>
						<td>'.$found_user->user_id.'</td>
						<td><a href="'.$str_profile_link.'" target="_blank">'.$found_user->teacher_name.'</a></td>
						<td>'.$found_user->user_role_display.'</td>
						<td>'.$found_user->mobile_no.'</td>
						<td><a href="'.$str_profile_link.'" target="_blank">Profile Link</a></td>
					</tr>
				';
				$array_locality_report[] = array(
					'ID' => $found_user->user_id,
					'Teacher Name' => $found_user->teacher_name,
					'Teacher Role' => $found_user->user_role_display,
					'Contact Number' => $found_user->mobile_no,
					'Link' => $str_profile_link,
				);
			}
	
			$str_return .= '
						</tbody>
					</table>
				</div>
			';
		}
		$excel_data_array = array(
			'report_title' => 'subject_locality',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'subject_locality',
			'data' => $array_locality_report
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function localities_not_having_subject() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		$subject_name_id = $_POST['subject_name_id'];
		
		
		if(!is_numeric($subject_name_id)) {
			$return_message['error_type'] = 'incorrect_data';
			$return_message['message'] = 'Please select Subject for this report';			
			echo json_encode($return_message );
			die();
		}
		
		$locality_array = array();		
		//collect all the batches having subject
		//compare the batches with locality if available then remove
		$str_subject_locality = "
			select locality from pdg_locality where locality_id not in (
				select distinct pdg_tutor_location.locality_id from pdg_tutor_location where pdg_tutor_location.tuition_batch_id in (
					select distinct view_pdg_batch_subject.tuition_batch_id from view_pdg_batch_subject where view_pdg_batch_subject.subject_name_id = $subject_name_id
				)
			)
		";
		
		$locality_names_db = $wpdb->get_results($str_subject_locality);
		if(empty($locality_names_db)) {
			$return_message['message'] = 'Data Not available';			
			echo json_encode($return_message );
			die();
		}
		
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>Locality Name</th>
						</tr>
					</thead>
					<tbody>
	    ';
		$array_locality = array();
		foreach ($locality_names_db as $locality_name) {
			$str_return .= '
				<tr>
					<td>'.$locality_name->locality.'</td>					
				</tr>
			';
			$array_locality[] = array(
				'Locality Name' => $locality_name->locality
			); 
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'locality_not_having_subject',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'locality_not_having_subject',
			'data' => $array_locality
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function subjects_not_in_locality() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		
		$locality_name_id = $_POST['locality_name_id'];
		
		if(!is_numeric($locality_name_id)) {
			$return_message['error_type'] = 'incorrect_data';
			$return_message['message'] = 'Please select Locality for this report';			
			echo json_encode($return_message );
			die();
		}
		$subjects_array = array();		
		
		$str_subject_locality = "
			select subject_name from pdg_subject_name where subject_name_id not in (
				select distinct view_pdg_batch_subject.subject_name_id from view_pdg_batch_subject where view_pdg_batch_subject.tuition_batch_id in (
					select distinct tuition_batch_id from pdg_tutor_location where locality_id = $locality_name_id
				)
			)
		";
		
		$subject_names_db = $wpdb->get_results($str_subject_locality);
		if(empty($subject_names_db)) {
			$return_message['message'] = 'Data Not available';			
			echo json_encode($return_message );
			die();
		}
		$array_subject = array();
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>Subject Name</th>
						</tr>
					</thead>
					<tbody>
	    ';

		foreach ($subject_names_db as $subject_name) {
			$str_return .= '
				<tr>
					<td>'.$subject_name->subject_name.'</td>					
				</tr>
			';
			$array_subject[] = array(
				'Subject Name' =>$subject_name->subject_name
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		
		$excel_data_array = array(
			'report_title' => 'subject_not_in_locality',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'subject_not_in_locality',
			'data' => $array_subject
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}
	
	public static function fn_teacher_registration_reports($report_dates) {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = "";
		
		$str_sql = "select * from view_pdg_teachers	";
		if(!empty($report_dates)) {
			$from_date = $report_dates['from_date'];
			$to_date = $report_dates['to_date'];
			$str_sql .= " where (created between '$from_date 00:00:00' and '$to_date 23:59:59')";
		}
		
		$db_data = $wpdb->get_results($str_sql);
		
		$array_teacher_registration = array();
		if(empty($db_data)) {
			$str_return = '<h3>Sorry! Data is not available.</h3>';
		} else {
			// table
			$str_return = '
				<div class="table-responsive">
					<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile_no</th>
								<th>Gender</th>
								<th>Approved</th>
								<th>Active</th>
								<th>Verified</th>								
								<th>Created</th>
							</tr>
						</thead>
						<tbody>
		    ';
			
			foreach ($db_data as $record) {
				$profile_slug = $record->profile_slug;
				$profile_slug = home_url('/teacher/'.$profile_slug);
				$str_return .= '
					<tr>
						<td>'.$record->teacher_id.'</td>
						<td><a href="'.$profile_slug.'" target="_blank">'.$record->display_name.'</a></td>
						<td>'.$record->user_email.'</td>
						<td>'.$record->mobile_no.'</td>
						<td>'.$record->gender.'</td>
						<td>'.$record->approved.'</td>
						<td>'.$record->active.'</td>
						<td>'.$record->verified.'</td>
						<td>'.$record->created.'</td>					
					</tr>
				';
				$array_teacher_registration[] = array(
					'ID' => $record->teacher_id,
					'Name' => $record->display_name,
					'Email' => $record->user_email,
					'Mobile_no' => $record->mobile_no,
					'Gender' => $record->gender,
					'Approved' => $record->approved,
					'Active' => $record->active,
					'Verified' => $record->verified,
					'Created' => $record->created,
				);
			}
	
			$str_return .= '
						</tbody>
					</table>
				</div>
			';
		}
		
		$excel_data_array = array(
			'report_title' => 'teacher_registration_report',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'teacher_registration_report',
			'data' => $array_teacher_registration
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function fn_institute_registration_reports($report_dates) {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = "";
		
		$str_sql = "select * from view_pdg_institutes ";
		if(!empty($report_dates)) {
			$from_date = $report_dates['from_date'];
			$to_date = $report_dates['to_date'];
			$str_sql .= " where (created between '$from_date 00:00:00' and '$to_date 23:59:59')";
		}
		
		$db_data = $wpdb->get_results($str_sql);
		$array_institution = array();
		if(empty($db_data)) {
			$str_return = '<h3>Sorry! Data is not available.</h3>';
		} else {
			// table
			$str_return = '
				<div class="table-responsive">
					<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile_no</th>
								<th>Approved</th>
								<th>Active</th>
								<th>Verified</th>								
								<th>Created</th>
							</tr>
						</thead>
						<tbody>
		    ';
			
			foreach ($db_data as $record) {
				$profile_slug = $record->profile_slug;
				$profile_slug = home_url('/institute/'.$profile_slug);
				$str_return .= '
					<tr>
						<td>'.$record->institute_id.'</td>
						<td><a href="'.$profile_slug.'" target="_blank">'.$record->institute_name.'</a></td>
						<td>'.$record->user_email.'</td>
						<td>'.$record->mobile_no.'</td>
						<td>'.$record->approved.'</td>
						<td>'.$record->active.'</td>
						<td>'.$record->verified.'</td>
						<td>'.$record->created.'</td>					
					</tr>
				';
				$array_institution[] = array(
					'ID' => $record->institute_id,
					'Name' => $record->institute_name,
					'Email' => $record->user_email,
					'Mobile_no' => $record->mobile_no,
					'Approved' => $record->approved,
					'Active' => $record->active,
					'Verified' => $record->verified,
					'Created' => $record->created,
				);
			}
	
			$str_return .= '
						</tbody>
					</table>
				</div>
			';
		}
		
		$excel_data_array = array(
			'report_title' => 'register_institute',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'register_institute',
			'data' => $array_institution
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}
	
	public static function social_signups() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		
		$signup_array = array();		
		
		$str_signup_sql = "
			select * from view_pdg_social_user_info
		";
		
		$signup_db = $wpdb->get_results($str_signup_sql);
		if(empty($signup_db)) {
			
			$return_message['message'] = 'Data Not available';			
			echo json_encode($return_message );
			die();
		}
		
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Username</th>
							<th>Nice Name</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Role</th>
							<th>Mobile</th>
							<th>Social</th>							
						</tr>
					</thead>
					<tbody>
	    ';

		foreach ($signup_db as $signup_info) {
			$str_return .= '
				<tr>
					<td>'.$signup_info->user_id.'</td>
					<td>'.$signup_info->user_name.'</td>
					<td>'.$signup_info->user_nicename.'</td>
					<td>'.$signup_info->user_email.'</td>
					<td>'.$signup_info->first_name.'</td>
					<td>'.$signup_info->last_name.'</td>
					<td>'.$signup_info->user_role_display.'</td>
					<td>'.$signup_info->mobile_no.'</td>
					<td>'.$signup_info->social_location.'</td>
				</tr>
			';
			$signup_array[] = array(
				'ID' =>$signup_info->user_id,
				'Username' =>$signup_info->user_name,
				'Nice Name' =>$signup_info->user_nicename,
				'Email' =>$signup_info->user_email,
				'First Name' =>$signup_info->first_name,
				'Last Name' =>$signup_info->last_name,
				'Role' =>$signup_info->user_role_display,
				'Mobile' =>$signup_info->mobile_no,
				'Social' =>$signup_info->social_location,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'social_signups',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'social_signups',
			'data' => $signup_array
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function unapproved_teachers() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		
		$records_array = array();		
		
		$str_sql = "
			select 
				view_pdg_teachers.*,
				view_pdg_social.social_location 
			from view_pdg_teachers
			left join view_pdg_social on view_pdg_social.user_id = view_pdg_teachers.user_id
			where view_pdg_teachers.approved = 'no'
		";
		
		$report_data_db = $wpdb->get_results($str_sql);
		if(empty($report_data_db)) {
			
			$return_message['message'] = 'Data Not available';			
			echo json_encode($return_message );
			die();
		}
		
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Username</th>
							<th>Email</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Role</th>
							<th>Mobile</th>
							<th>Social</th>
							<th>Approved</th>
							<th>Active</th>
							<th>Verified</th>
							<th>Deleted</th>
							<th>Created</th>
						</tr>
					</thead>
					<tbody>
	    ';

		foreach ($report_data_db as $record_info) {
			$str_return .= '
				<tr>
					<td>'.$record_info->user_id.'</td>
					<td>'.$record_info->user_name.'</td>					
					<td>'.$record_info->user_email.'</td>
					<td>'.$record_info->first_name.'</td>
					<td>'.$record_info->last_name.'</td>
					<td>'.$record_info->user_role_display.'</td>
					<td>'.$record_info->mobile_no.'</td>
					<td>'.$record_info->social_location.'</td>
					<td>'.$record_info->approved.'</td>
					<td>'.$record_info->active.'</td>
					<td>'.$record_info->verified.'</td>
					<td>'.$record_info->deleted.'</td>
					<td>'.$record_info->created.'</td>
				</tr>
			';
			$records_array[] = array(
				'ID' =>$record_info->user_id,
				'Username' =>$record_info->user_name,
				'Email' =>$record_info->user_email,
				'First Name' =>$record_info->first_name,
				'Last Name' =>$record_info->last_name,
				'Role' =>$record_info->user_role_display,
				'Mobile' =>$record_info->mobile_no,
				'Social' =>$record_info->social_location,
				'Approved' =>$record_info->approved,
				'Active' =>$record_info->active,
				'Verified' =>$record_info->verified,
				'Deleted' =>$record_info->deleted,
				'Created' =>$record_info->created,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'unapproved_teachers',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'unapproved_teachers',
			'data' => $records_array
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function unapproved_institutes() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		
		$records_array = array();		
		
		$str_sql = "
			select 
				view_pdg_institutes.*,
				view_pdg_social.social_location 
			from view_pdg_institutes
			left join view_pdg_social on view_pdg_social.user_id = view_pdg_institutes.user_id
			where view_pdg_institutes.approved = 'no'
		";
		
		$report_data_db = $wpdb->get_results($str_sql);
		if(empty($report_data_db)) {
			
			$return_message['message'] = 'Data Not available';			
			echo json_encode($return_message );
			die();
		}
		
		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Username</th>
							<th>Email</th>							
							<th>Name</th>
							<th>Role</th>
							<th>Mobile</th>
							<th>Social</th>
							<th>Approved</th>
							<th>Active</th>
							<th>Verified</th>
							<th>Deleted</th>
							<th>Created</th>
						</tr>
					</thead>
					<tbody>
	    ';

		foreach ($report_data_db as $record_info) {
			$str_return .= '
				<tr>
					<td>'.$record_info->user_id.'</td>
					<td>'.$record_info->user_name.'</td>					
					<td>'.$record_info->user_email.'</td>
					<td>'.$record_info->institute_name.'</td>
					<td>'.$record_info->user_role_display.'</td>
					<td>'.$record_info->mobile_no.'</td>
					<td>'.$record_info->social_location.'</td>
					<td>'.$record_info->approved.'</td>
					<td>'.$record_info->active.'</td>
					<td>'.$record_info->verified.'</td>
					<td>'.$record_info->deleted.'</td>
					<td>'.$record_info->created.'</td>
				</tr>
			';
			$records_array[] = array(
				'ID' =>$record_info->user_id,
				'Username' =>$record_info->user_name,
				'Email' =>$record_info->user_email,
				'Name' =>$record_info->institute_name,
				'Role' =>$record_info->user_role_display,
				'Mobile' =>$record_info->mobile_no,
				'Social' =>$record_info->social_location,
				'Approved' =>$record_info->approved,
				'Active' =>$record_info->active,
				'Verified' =>$record_info->verified,
				'Deleted' =>$record_info->deleted,
				'Created' =>$record_info->created,
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'unapproved_teachers',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'unapproved_teachers',
			'data' => $records_array
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}

	public static function fn_master_reports($report_dates) {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');

		$str_return = '';
		
		$records_array = array();		
		
		//$str_sql = "SELECT * FROM pdg_query WHERE querydate >= '2018-01-15' AND querydate < '2018-01-19'";
		$str_sql = "SELECT pq.id,pq.student_id,pq.standard,pq.myhome,pq.tutorhome,pq.institute,pq.localities,pq.start_date,pq.requirement,pq.querydate,pq.follow_up_date,pq.age,pq.approved,
			pq.disapprove_msg,pq.archived,pq.fast_pass,pq.lead_source,pq.is_review,pq.query_status,pq.roc,pq.query_plan, pq.contact_no, pq.contact_email,
			wu1.display_name AS teacher_name,wu.display_name AS sales_rep_name,
			pip.payment_by AS inbound_payment_by, pip.payment_by_details AS inbound_payment_by_details, pip.exp_date_payment AS inbound_exp_date_payment, pip.fees AS inbound_fees, 
			pip.is_transferd AS inbound_is_transferd,pip.invoice_no AS inbound_invoice_no,
			pop.our_share AS outbound_our_share,pop.teacher_share AS outbound_teacher_share,pop.gst AS outbound_gst,pop.customer_invoice_no AS outbound_invoice_no,
			pop.payment_type AS outbound_payment_type,pop.payment_by_details AS outbound_payment_by_details,
			psn.subject_name,ps.school_name,pab.academic_board,pc.college_name,pu.university_name,pct.course_type FROM pdg_query AS pq
			LEFT JOIN pdg_inbound_payment AS pip on pq.id = pip.fk_query_id
			LEFT JOIN pdg_outbound_payment AS pop on pq.id = pop.fk_query_id
			LEFT JOIN pdg_college AS pc ON pq.college = pc.college_id
			LEFT JOIN pdg_schools AS ps ON ps.school_id = pq.school
			LEFT JOIN pdg_academic_board AS pab ON pab.academic_board_id = pq.board
			LEFT JOIN pdg_university AS pu ON pu.id = pq.university
			LEFT JOIN wp_users AS wu ON wu.ID=pq.current_sales_rep_id
			LEFT JOIN wp_users AS wu1 ON wu1.ID=pip.teacherinst_id
			LEFT JOIN pdg_subject_name AS psn ON psn.subject_name_id = pq.subject
			LEFT JOIN pdg_course_category AS pcc ON psn.subject_name_id=pcc.subject_name_id
			LEFT JOIN pdg_course_type AS pct ON pcc.course_type_id=pct.course_type_id";

		if(!empty($report_dates)) {
			$from_date = $report_dates['from_date'];
			$to_date = $report_dates['to_date'];
			$str_sql .= " WHERE (pq.querydate BETWEEN '$from_date' and '$to_date')";
		}
		$str_sql .= "GROUP BY pq.id";
		$report_data_db = $wpdb->get_results($str_sql);
		//echo "<pre>";print_r($report_data_db);echo "</pre>";
		if(empty($report_data_db)) {
			
			$return_message['message'] = 'Data Not available';			
			echo json_encode($return_message );
			die();
		}

		$str_return = '
			<div class="table-responsive">
				<table class="table table-striped table-vcenter table-bordered table-condensed table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Student ID</th>
							<th>Subject Name</th>
							<th>Course Type</th>
							<th>School Name</th>
							<th>Academic Board</th>
							<th>College Name</th>
							<th>University Name</th>
							<th>Standard</th>							
							<th>My Home</th>
							<th>Tutor Home</th>
							<th>Institute</th>
							<th>Localities</th>
							<th>Flag</th>
							<th>Start Date</th>
							<th>Query Date</th>
							<th>Follow Up date</th>
							<th>Age</th>
							<th>Approve</th>
							<th>Disapprove Message</th>
							<th>Archive</th>
							<th>Lead Source</th>
							<th>Is review</th>
							<th>Query Status</th>
							<th>ROC</th>
							<th>Query Plan</th>
							<th>Contact No</th>
							<th>Contact Email</th>
							<th>Teacher Name</th>
							<th>Sales Rep Name</th>
							<th>Inbound Payment By</th>
							<th>Inbound Payment Details</th>
							<th>Inbound Expected Date Of Payment</th>
							<th>Inbound Fees</th>
							<th>Inbound Is Transferd</th>
							<th>Inbound Invoice No</th>
							<th>Outbound Our Share</th>
							<th>Outbound Teacher Share</th>
							<th>Outbound GST</th>
							<th>Outbound Invoice No</th>
							<th>Outbound Payment Type</th>
							<th>Outbound Payment Details</th>
						</tr>
					</thead>
					<tbody>';

		foreach ($report_data_db as $record_info) {
			$flag='';
			$system_date=time();
			$query_start_date=strtotime($record_info->start_date);
			$datediff = $query_start_date - $system_date;
			$days= floor($datediff / (60 * 60 * 24));
			if($days <=3 && $query_start_date > $system_date ){
				$flag = 'Very Urgent';  
			}
			elseif($days > 3 && $days <= 7 && $query_start_date > $system_date){
				$flag = 'Urgent';   
			}
			elseif($days > 7 && $query_start_date > $system_date){
				$flag = 'Normal';   
			}
			else{
				$flag = 'Expired';     
			}

			$localities = $record_info->localities;
			$arr_localities = array();
			$locality_sql = "SELECT locality FROM pdg_locality WHERE locality_id IN ($localities)";
			$locality_result = $wpdb->get_results($locality_sql);

			foreach ($locality_result as $key => $value) {
				$arr_localities[] = $value->locality;
			}

			$inbound_payment_by = '';
			if($record_info->inbound_payment_by == 1){
				$inbound_payment_by = 'Cash';
			}
			else if($record_info->inbound_payment_by == 2){
				$inbound_payment_by = 'Paytm';
			}
			else if($record_info->inbound_payment_by == 3){
				$inbound_payment_by = 'Bank';
			}
			else if($record_info->inbound_payment_by == 4){
				$inbound_payment_by = 'Card';
			}	

			$outbound_payment_by = '';
			if($record_info->outbound_payment_type == 1){
				$outbound_payment_by = 'Cash';
			}
			else if($record_info->outbound_payment_type == 2){
				$outbound_payment_by = 'Paytm';
			}
			else if($record_info->outbound_payment_type == 3){
				$outbound_payment_by = 'Bank';
			}
			else if($record_info->inbound_payment_by == 4){
				$outbound_payment_by = 'Card';
			}

			$str_return .= '
				<tr>
					<td>'.$record_info->id.'</td>
		            <td>'.$record_info->student_id.'</td>
		            <td>'.$record_info->subject_name.'</td>
		            <td>'.$record_info->course_type.'</td>
		            <td>'.$record_info->school_name.'</td>
		            <td>'.$record_info->academic_board.'</td>
		            <td>'.$record_info->college_name.'</td>
		            <td>'.$record_info->university_name.'</td>
		            <td>'.$record_info->standard.'</td>
		            <td>'.($record_info->myhome == 0?'No':'Yes').'</td>
		            <td>'.($record_info->tutorhome == 0?'No':'Yes').'</td>
		            <td>'.($record_info->institute == 0?'No':'Yes').'</td>
		            <td>'.implode(',', $arr_localities).'</td>
		            <td>'.$flag.'</td>
		            <td>'.$record_info->start_date.'</td>
		            <td>'.$record_info->querydate.'</td>
		            <td>'.$record_info->follow_up_date.'</td>
		            <td>'.$record_info->age.'</td>
		            <td>'.($record_info->approved == 0?'No':'Yes').'</td>
		            <td>'.$record_info->disapprove_msg.'</td>
		            <td>'.($record_info->archived == 0?'No':'Yes').'</td>
		            <td>'.$record_info->lead_source.'</td>
		            <td>'.($record_info->is_review == 0 ?'No':'Yes').'</td>
		            <td>'.$record_info->query_status.'</td>
		            <td>'.$record_info->roc.'</td>
		            <td>'.$record_info->query_plan.'</td>
		            <td>'.$record_info->contact_no.'</td>
		            <td>'.$record_info->contact_email.'</td>
		            <td>'.$record_info->teacher_name.'</td>
		            <td>'.$record_info->sales_rep_name.'</td>
		            <td>'.$inbound_payment_by.'</td>
		            <td>'.$record_info->inbound_payment_by_details.'</td>
		            <td>'.$record_info->inbound_exp_date_payment.'</td>
		            <td>'.$record_info->inbound_fees.'</td>
		            <td>'.$record_info->inbound_is_transferd.'</td>
		            <td>'.$record_info->inbound_invoice_no.'</td>
		            <td>'.$record_info->outbound_our_share.'</td>
		            <td>'.$record_info->outbound_teacher_share.'</td>
		            <td>'.$record_info->outbound_gst.'</td>
		            <td>'.$record_info->outbound_invoice_no.'</td>
		            <td>'.$outbound_payment_by.'</td>
		            <td>'.$record_info->outbound_payment_by_details.'</td>
				</tr>';


			$records_array[] = array(
				'ID' =>$record_info->id,
				'Student ID' =>$record_info->student_id,
				'Subject Name' =>$record_info->subject_name,
				'Course Type' => $record_info->course_type,
				'School Name' =>$record_info->school_name,
				'Academic Board' =>$record_info->academic_board,
				'College Name' =>$record_info->college_name,
				'University Name' =>$record_info->university_name,
				'Standard' =>$record_info->standard,
				'My Home' =>($record_info->myhome == 0?'No':'Yes'),
				'Tutor Home' =>($record_info->tutorhome == 0?'No':'Yes'),
				'Institute' =>($record_info->institute == 0?'No':'Yes'),
				'Localities' =>implode(',', $arr_localities),
				'Flag' =>$flag,
				'Start Date' =>$record_info->start_date,
				'Query Date' =>$record_info->querydate,
				'Follow Up date' =>$record_info->follow_up_date,
				'Age' =>$record_info->age,
				'Approve' =>($record_info->approved == 0?'No':'Yes'),
				'Disapprove Message' =>$record_info->disapprove_msg,
				'Archive' =>($record_info->archived == 0?'No':'Yes'),
				'Lead Source' =>$record_info->lead_source,
				'Is review' =>($record_info->is_review == 0?'No':'Yes'),
				'Query Status' =>$record_info->query_status,
				'ROC' =>$record_info->roc,
				'Query Plan' =>$record_info->query_plan,
				'Contact No' =>$record_info->contact_no,
				'Contact Email' =>$record_info->contact_email,
				'Teacher Name' =>$record_info->teacher_name,
				'Sales Rep Name' =>$record_info->sales_rep_name,
				'Inbound Payment By' =>$inbound_payment_by,
				'Inbound Payment Details' =>$record_info->inbound_payment_by_details,
				'Inbound Expected Date Of Payment' =>$record_info->inbound_exp_date_payment,
				'Inbound Fees' =>$record_info->inbound_fees,
				'Inbound Is Transferd' =>$record_info->inbound_is_transferd,
				'Inbound Invoice No' =>$record_info->inbound_invoice_no,
				'Outbound Our Share' =>$record_info->outbound_our_share,
				'Outbound Teacher Share' =>$record_info->outbound_teacher_share,
				'Outbound GST' =>$record_info->outbound_gst,
				'Outbound Invoice No' =>$record_info->outbound_invoice_no,
				'Outbound Payment Type' =>$outbound_payment_by,
				'Outbound Payment Details' =>$record_info->outbound_payment_by_details,
				
			);
		}

		$str_return .= '
					</tbody>
				</table>
			</div>
		';
		
		$excel_data_array = array(
			'report_title' => 'no_of_students_served',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'master_report',
			'data' => $records_array
		);		
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$return_message['excel'] = $excel_file->ExcelExport();
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = $str_return;

		return $return_message;
	}
}