<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}

class ControlPanelSettings {
	
	public static function fn_clear_cache_ajax() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$cache_type = $_POST['cache_type'];
		
		switch($cache_type) {
			case 'data':
				PDGManageCache::fn_update_cache();				
				$return_message['message'] = "Data cache was cleaned successfully!";
				break;
			case 'scripts':
				$cache_folder_path = PEDAGOGE_PLUGIN_DIR.'storage/cache/';
				//delete css
				foreach(glob($cache_folder_path.'*.css') as $file) {
					if(is_file($file)) {
						@unlink($file);	
					}
				}
			    //delete css
				foreach(glob($cache_folder_path.'*.js') as $file) {
					if(is_file($file)) {
						@unlink($file);	
					}
				}
				$return_message['message'] = "Scripts cache was cleaned successfully!";
				break;
			default:
				$return_message['message'] = "Error! Undefined cache type! Please try again!";
				echo json_encode($return_message );
				die();
				break;
		}
		$return_message['error'] = FALSE;
		echo json_encode($return_message );
		die();
	} 

	public static function fn_update_email_system_settings() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$email_queue = sanitize_text_field($_POST['email_queue']);		
		$email_async = sanitize_text_field($_POST['email_async']);
		$email_log = sanitize_text_field($_POST['email_log']);
		$default_sender_email = sanitize_text_field($_POST['default_sender_email']);
		
		if(empty($default_sender_email)) {
			$default_sender_email = 'noreply@pedagoge.com';
		}
		
		$result = filter_var( $default_sender_email, FILTER_VALIDATE_EMAIL );
		if($result == FALSE) {
			
			$return_message['message'] = 'Email address is incorrect! Please try again!';			
			echo json_encode($return_message );
			die();
		}
		switch($email_async) {
			case 'yes':
			case 'no':
				self::fn_email_settings_wp_options('save', 'pdg_email_async', $email_async);
				break;
		}
		switch($email_queue) {
			case 'yes':
			case 'no':
				self::fn_email_settings_wp_options('save', 'pdg_email_queue', $email_queue);
				break;
		}
		switch($email_log) {
			case 'yes':
			case 'no':
				self::fn_email_settings_wp_options('save', 'pdg_email_log', $email_log);
				break;
		}
		self::fn_email_settings_wp_options('save', 'pdg_default_email_sender', $default_sender_email);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Settings updated successfully!';
		
		echo json_encode($return_message );
		die();
	}
	
	
	/**
	 * operation - save/fetch
	 * options names available are
	 * - pdg_default_email_sender
	 * - pdg_email_async
	 * - pdg_email_queue
	 */
	public static function fn_email_settings_wp_options($operation, $option_name, $option_value = '') {
		
		$deprecated = null;
		$autoload = 'yes';
		
		$return_value = FALSE;
		
		if(empty($option_name)) {
			return $return_value;
		}
		
		$return_value =  get_option( $option_name );
		
		if($operation == 'save') {
			$return_value = $option_value;
			if($return_value == FALSE) {
				//add option
				add_option( $option_name, $option_value, $deprecated, $autoload );
			} else {
				// update option
				update_option($option_name, $option_value);				
			}
		}		
		return $return_value;		
	}

	public static function fn_minification_enabled_ajax() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$minification_enabled = sanitize_text_field($_POST['minification_enabled']);
		switch($minification_enabled) {
			case 'yes':
			case 'no':
				self::fn_email_settings_wp_options('save', 'pdg_minification_enabled', $minification_enabled);
				break;
		}
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Settings updated successfully!';
		
		echo json_encode($return_message );
		die();
	}

	public static function fn_create_email_type() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$email_label = sanitize_text_field($_POST['email_type']);
		if(empty($email_label)) {
			$return_message['message'] = 'Please input Email type and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$email_type = sanitize_title_with_dashes($email_label);
		$email_type = str_replace('-', '_', $email_type);
		
		/**
		 * Check for Email Type Duplicate
		 */
		$duplicate_email_type = $wpdb->get_results("select * from pdg_email_settings where email_type = '$email_type'");
		if(!empty($duplicate_email_type)) {
			$return_message['message'] = 'Duplicate Email Type. Please input another Email type and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$insert_array = array(
			'email_label' => $email_label,
			'email_type' => $email_type
		);
		$insert_array_format = array(
			'%s',
			'%s'
		);
		$wpdb->insert('pdg_email_settings', $insert_array, $insert_array_format);
		$email_settings_id = $wpdb->insert_id;
		
		$return_message['message'] = 'Email Type Saved successfully!';			
		$return_message['error'] = FALSE;
		$return_message['data'] = $email_settings_id;
		echo json_encode($return_message);
		die();
		
	}

	public static function fn_load_email_type_info() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$email_settings_id = $_POST['mail_settings_id'];
		if(!is_numeric($email_settings_id)) {
			$return_message['message'] = 'Please select Email type and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$email_settings_data = $wpdb->get_results("select * from pdg_email_settings where email_settings_id = $email_settings_id");
		if(!empty($email_settings_data)) {
			$email_label = '';
			$email_type = '';
			$sender_email = '';
			$send_to_subscriber = '';
			$send_to_user = '';
			$email_priority = '';
			$email_subject = '';
			foreach($email_settings_data as $email_setting) {
				$email_label = $email_setting->email_label;
				$email_type = $email_setting->email_type;
				$sender_email = $email_setting->sender_email;
				$send_to_subscriber = $email_setting->send_to_subscriber;
				$send_to_user = $email_setting->send_to_user;
				$email_priority = $email_setting->email_priority;
				$email_subject = $email_setting->email_subject;
			}
			$email_subscribers_list = self::fn_get_subscribers_email($email_settings_id);
			$return_data = array(
				'email_subscribers_list' => $email_subscribers_list,
				'email_label' => $email_label,
				'email_type' => $email_type,
				'sender_email' => $sender_email,
				'send_to_subscriber' => $send_to_subscriber,
				'send_to_user' => $send_to_user,
				'email_priority' => $email_priority,
				'email_subject' => $email_subject
			);
			
			$return_message['message'] = 'Settings Loaded successfully!';
			$return_message['error'] = FALSE;
			$return_message['data'] = $return_data;			
			echo json_encode($return_message );
			die();
			
		} else {
			$return_message['message'] = 'Error! Settings not available. Please try again!';			
			echo json_encode($return_message );
			die();
		}		
	}

	public static function fn_get_subscribers_email($settings_id) {
		global $wpdb;
		$str_return = 'Please input Subscriber email address';
		if(is_numeric($settings_id)) {
			$subscribers_email_data = $wpdb->get_results("select * from pdg_email_subscribers where email_setting_id = $settings_id");
			if(!empty($subscribers_email_data)) {
				$str_return = '
					<table class="table table-striped table-bordered " id="tbl_email_subscribers_list">
				';
				foreach($subscribers_email_data as $email_subscriber) {
					$email_sub_id = $email_subscriber->sub_id;
					$email_address = $email_subscriber->email_address;
					$str_return .= '
						<tr>
							<td>'.$email_address.'</td>
							<td><button class="btn btn-danger col-md-12 cmd_remove_subscriber" data-subscriber_id="'.$email_sub_id.'" data-loading-text="Removing...">Remove</button></td>
						</tr>
					';
				}
				$str_return .= '</table>';	
			}
			
		}
		return $str_return;
	}

	public static function fn_load_email_subscribers_list() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$email_settings_id = $_POST['mail_settings_id'];
		if(!is_numeric($email_settings_id)) {
			$return_message['message'] = 'Please select Email type and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$return_message['message'] = 'Subscribers List Loaded successfully!';
		$return_message['error'] = FALSE;
		$return_message['data'] = self::fn_get_subscribers_email($email_settings_id);			
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_add_email_subscriber() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$email_settings_id = $_POST['mail_settings_id'];
		if(!is_numeric($email_settings_id)) {
			$return_message['message'] = 'Please select Email type and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$email_address = sanitize_text_field($_POST['email_address']);
		if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
			$return_message['message'] = 'Invalid Email format. Please try again!';			
			echo json_encode($return_message );
			die();
		}
		/**
		 * check for duplicates. if not duplicate then add.
		 */
		$str_sql = "
			select * from pdg_email_subscribers 
			where email_setting_id = $email_settings_id and 
			email_address = '$email_address'";
		
		$is_duplicate_db = $wpdb->get_results($str_sql);
		if(empty($is_duplicate_db)) {
			$insert_array = array(
				'email_setting_id' => $email_settings_id,
				'email_address' => $email_address
			);
			$format_array = array('%d', '%s');
			$wpdb->insert('pdg_email_subscribers', $insert_array, $format_array);
		}
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Subscriber Email address added successfully!';			
		echo json_encode($return_message );
		die();
	}

	public static function fn_remove_email_subscriber() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$subscriber_id = $_POST['subscriber_id'];
		if(!is_numeric($subscriber_id)) {
			$return_message['message'] = 'Subscriber ID is not available. Please try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$wpdb->get_results("delete from pdg_email_subscribers where sub_id = $subscriber_id");
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Subscriber Email address was removed successfully!';			
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_update_email_settings() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$mail_settings_id = $_POST['mail_settings_id'];
		if(!is_numeric($mail_settings_id)) {
			$return_message['message'] = 'Mail Settings ID is not available. Please try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$txt_mail_type_label = sanitize_text_field($_POST['txt_mail_type_label']);
		$txt_email_subject = sanitize_text_field($_POST['txt_email_subject']);
		$txt_mail_type_sender_email = sanitize_text_field($_POST['txt_mail_type_sender_email']);
		$send_to_subscriber = sanitize_text_field($_POST['send_to_subscriber']);
		$send_to_user = sanitize_text_field($_POST['send_to_user']);
		$select_email_sending_priority = sanitize_text_field($_POST['select_email_sending_priority']);
		
		if(empty($txt_mail_type_label)) {
			$return_message['message'] = 'Please Input Mail Type Label!';			
			echo json_encode($return_message );
			die();
		}

		if(empty($txt_email_subject)) {
			$return_message['message'] = 'Please Input Email Subject!';			
			echo json_encode($return_message );
			die();
		}
		
		if(!empty($txt_mail_type_sender_email) && !filter_var($txt_mail_type_sender_email, FILTER_VALIDATE_EMAIL)) {
			$return_message['message'] = 'Please Input Sender Email in proper format and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$email_type = sanitize_title_with_dashes($txt_mail_type_label);
		$email_type = str_replace('-', '_', $email_type);
		
		/**
		 * Check for Email Type Duplicate
		 */
		$duplicate_email_type = $wpdb->get_results("select * from pdg_email_settings where email_type = '$email_type' and email_settings_id != $mail_settings_id");
		if(!empty($duplicate_email_type)) {
			$return_message['message'] = 'Duplicate Email Type. Please input another Email type and try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$update_array = array(
			'email_label' => $txt_mail_type_label,
			'email_subject' => $txt_email_subject,
			'sender_email' => $txt_mail_type_sender_email,
			'send_to_subscriber' => $send_to_subscriber,
			'send_to_user' => $send_to_user,
			'email_priority' => $select_email_sending_priority,
			'email_type' => $email_type
		);
		$data_format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s');
		$where_data = array(
			'email_settings_id' => $mail_settings_id
		);
		
		$wpdb->update('pdg_email_settings', $update_array, $where_data, $data_format, array('%d'));
		$return_message['error'] = FALSE;
		$return_message['data'] = $email_type;
		$return_message['message'] = 'Email setting was updated successfully!';			
		echo json_encode($return_message );
		die();
	}

	public static function fn_documents_manager_datatable() {
		global $wpdb;
		$table = 'view_pdg_documents_manager';
		
		$primaryKey = 'file_id';
		
		$columns = array(
			array( 'db' => 'file_id', 'dt' => 0 ),
			array( 'db' => 'user_id',  'dt' => 1 ),
			array( 'db' => 'teacher_name',   'dt' => 2 ),						
			array( 'db' => 'user_role_name',   'dt' => 3 ),
			array( 'db' => 'user_role_display',   'dt' => 4 ),
			array( 'db' => 'original_file_name',   'dt' => 5 ),
			array( 'db' => 'new_file_name',   'dt' => 6 ),
			array( 'db' => 'file_title',   'dt' => 7 ),
			array( 'db' => 'created',   'dt' => 8 ),
			array( 'db' => 'profile_slug',   'dt' => 9 ),
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	public static function fn_load_email_template() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$template_name = sanitize_text_field($_POST['template_name']);
		if(empty($template_name)) {
			$return_message['message'] = 'Template is empty! Please try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$pedagoge_email_template_dir = PEDAGOGE_PLUGIN_DIR.'views/pedagoge_email_templates/';
		$pedagoge_updated_email_template_dir = PEDAGOGE_PLUGIN_DIR.'storage/updated_email_templates/';
		
		if(!is_dir($pedagoge_updated_email_template_dir)) {
			mkdir($pedagoge_updated_email_template_dir);
		}
		
		$original_file_name = $pedagoge_email_template_dir.$template_name.'.html.php';
		$updated_file_name = $pedagoge_updated_email_template_dir.$template_name.'.html.php';
		
		$file_to_read = '';
		
		//check if updated exists or not
		if(!file_exists($updated_file_name)) {
			//check if original exists or not
			if(!file_exists($original_file_name)) {
				$return_message['message'] = 'Template file is not available! Please try again!';			
				echo json_encode($return_message );
				die();
			} else {
				$file_to_read = $original_file_name;
			}
		} else {
			$file_to_read = $updated_file_name;
		}
		
		$myfile = fopen($file_to_read, "r");
		$file_content = fread($myfile, filesize($file_to_read));
		fclose($myfile);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Template loaded successfully!';
		$return_message['data'] = utf8_encode(trim($file_content));
		
		echo json_encode($return_message );
		die();
	}

	public static function fn_save_email_template() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$template_name = sanitize_text_field($_POST['template_name']);
		if(empty($template_name)) {
			$return_message['message'] = 'Template name is empty! Please try again!';			
			echo json_encode($return_message );
			die();
		}
		$template_content = stripslashes($_POST['template_content']);
		if(empty($template_content)) {
			$return_message['message'] = 'Template content is empty! Please try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$pedagoge_updated_email_template_dir = PEDAGOGE_PLUGIN_DIR.'storage/updated_email_templates/';
		
		if(!is_dir($pedagoge_updated_email_template_dir)) {
			mkdir($pedagoge_updated_email_template_dir);
		}
		
		$updated_file_name = $pedagoge_updated_email_template_dir.$template_name.'.html.php';
		
		$myfile = fopen($updated_file_name, "w");		
		fwrite($myfile, $template_content);
		fclose($myfile);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Template was updated successfully!';			
		echo json_encode($return_message );
		die();
		
	}

	public static function fn_delete_email_template() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$template_name = sanitize_text_field($_POST['template_name']);
		if(empty($template_name)) {
			$return_message['message'] = 'Template name is empty! Please try again!';			
			echo json_encode($return_message );
			die();
		}
		
		$pedagoge_updated_email_template_dir = PEDAGOGE_PLUGIN_DIR.'storage/updated_email_templates/';
		
		if(!is_dir($pedagoge_updated_email_template_dir)) {
			mkdir($pedagoge_updated_email_template_dir);
		}
		
		$updated_file_name = $pedagoge_updated_email_template_dir.$template_name.'.html.php';
		
		if(!file_exists($updated_file_name)) {
			$return_message['message'] = 'Updated template is not available! Please try again!';			
			echo json_encode($return_message );
			die();
		}
		
		unlink($updated_file_name);
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Updated template was deleted successfully!';
		
		echo json_encode($return_message );
		die();
		
	}

	public static function fix_review_average() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		$str_sql = "
			select * from pdg_reviews 
			where 
			 (average_rating <=0 || average_rating is null) ||
			 (overall_rating <=0 || overall_rating is null)
		";
		
		$reviews_to_process = $wpdb->get_results($str_sql." limit 20");
		
		foreach($reviews_to_process as $review) {
			$review_id = $review->review_id;
			$rating1 = $review->rating1;
			$rating2 = $review->rating2;
			$rating3 = $review->rating3;
			$rating4 = $review->rating4;
			
			$total_ratings = $rating1 + $rating2 + $rating3 + $rating4;
			$average_rating = round(($total_ratings/4), 2);
			$update_array = array(
				'average_rating' => $average_rating,
				'overall_rating' => $average_rating
			);
			$update_array_format = array('%f', '%f');
			$where_data_array = array('review_id' => $review_id);
			$wpdb->update('pdg_reviews', $update_array, $where_data_array, $update_array_format, '%d');
		}
		
		$count_sql = "
			select count(*) from pdg_reviews 
			where 
			 (average_rating <=0 || average_rating is null) ||
			 (overall_rating <=0 || overall_rating is null)
		";
		
		$pending_reviews_fix_count = $wpdb->get_var( $count_sql);
			
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Reviews Average issue Fixed. Reviews to fix : '.$pending_reviews_fix_count;
			
		echo json_encode($return_message );
		die();
	}

	public static function fetch_duplicate_reviews() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$str_duplicate_reviews_table = '
			<table class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Reviewer</th>
						<th>Reviewee</th>
						<th>text</th>
						<th>Star1</th>
						<th>Star2</th>
						<th>Star3</th>
						<th>Star4</th>
						<th>Average</th>
						<th>Overall</th>
						<th>Approved</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					
		';
		$uniqe_reviews_array = array();
		$reviews_db = $wpdb->get_results("select * from pdg_reviews");
		$duplicate_reviews_count = 0;
		foreach($reviews_db as $review_info) {
			$review_id = $review_info->review_id;
			$reviewer_user_id = trim($review_info->reviewer_user_id);
			$reviewee_user_id = trim($review_info->tutor_institute_user_id);
			$unique_review_key = $reviewer_user_id.'#'.$reviewee_user_id;
			if(!array_key_exists($unique_review_key, $uniqe_reviews_array)) {
				$uniqe_reviews_array[$unique_review_key] = array(
					'id' => $review_id,
					'reviewer' => $reviewer_user_id,
					'reviewee' => $reviewee_user_id,
					'text' => $review_info->review_text,
					'star1' => $review_info->rating1,
					'star2' => $review_info->rating2,
					'star3' => $review_info->rating3,
					'star4' => $review_info->rating4,
					'average' => $review_info->average_rating,
					'overall' => $review_info->overall_rating,
					'approved' => $review_info->is_approved,					
				);
			} else {
				//original
				$str_duplicate_reviews_table .= '
					<tr>
						<td>'.$uniqe_reviews_array[$unique_review_key]['id'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['reviewer'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['reviewee'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['text'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['star1'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['star2'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['star3'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['star4'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['average'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['overall'].'</td>
						<td>'.$uniqe_reviews_array[$unique_review_key]['approved'].'</td>
						<td><button class="btn cmd_delete_duplicate_review btn-danger" data-review_id="'.$uniqe_reviews_array[$unique_review_key]['id'].'" data-loading-text="Deleting...">Delete</button></td>
					</tr>
				';
				//duplicates
				$str_duplicate_reviews_table .= '
					<tr>
						<td>'.$review_id.'</td>
						<td>'.$reviewer_user_id.'</td>
						<td>'.$reviewee_user_id.'</td>
						<td>'.$review_info->review_text.'</td>
						<td>'.$review_info->rating1.'</td>
						<td>'.$review_info->rating2.'</td>
						<td>'.$review_info->rating3.'</td>
						<td>'.$review_info->rating4.'</td>
						<td>'.$review_info->average_rating.'</td>
						<td>'.$review_info->overall_rating.'</td>
						<td>'.$review_info->is_approved.'</td>
						<td><button class="btn cmd_delete_duplicate_review btn-danger" data-review_id="'.$review_id.'" data-loading-text="Deleting...">Delete</button></td>
					</tr>
				';
				$duplicate_reviews_count++;
			}
		}
		$str_duplicate_reviews_table.= '			
				</tbody>
			</table>			
		';
		$str_duplicate_reviews_table = '<p>Total Duplicate reviews: '.$duplicate_reviews_count.'</p>'.$str_duplicate_reviews_table;
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Duplicate reviews fetched: '.$duplicate_reviews_count;
		$return_message['html'] = $str_duplicate_reviews_table;
			
		echo json_encode($return_message );
		die();
	}

	public static function remove_duplicate_review() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$duplicate_review_id = $_POST['duplicate_review']['reviewID'];
		
		if(!is_numeric($duplicate_review_id) || $duplicate_review_id <= 0) {
			$return_message['error_type'] = 'wrong_data';
			$return_message['message'] = 'Wrong Data! Please try again!';
			
			echo json_encode($return_message );
			die();
		}
		
		$wpdb->get_results("delete from pdg_reviews where review_id = $duplicate_review_id");
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Deleted successfully! Deleted Review ID'.$duplicate_review_id;
			
		echo json_encode($return_message );
		die();
	}
}