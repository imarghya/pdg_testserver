<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die ;
}
class ControlPanelPedagogeImpex {

	public static function fn_institute_dump() {
		global $wpdb;
		$return_message = array('error' => TRUE, 'error_type' => '', 'message' => 'Error message!', 'data' => '');
		$str_sql = "select * from view_pdg_institutes_extented";
		$institute_dump_db = $wpdb -> get_results($str_sql);
		if (empty($institute_dump_db)) {
			$return_message['error_type'] = 'no_result_found';
			$return_message['message'] = "Error! No data available.";
			return $return_message;
		}
				
        $excel_file_info = self::fn_return_institute_excel($institute_dump_db);
		
		$created_excel_url = $excel_file_info['url'];
		$created_excel_path = $excel_file_info['path'];
				
		$export_mail_data = array(
			'report_type' => 'Institute',
			'file_url' => $created_excel_url,
			'file_path' => $created_excel_path,
		);
		
		self::mail_data_dump($export_mail_data);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = '<h3>Report has been created and mailed successfully! Please check your Email to download the report.</h3>';
		$return_message['excel'] = $created_excel_url;

		return $return_message;
		
	}

	public static function fn_teacher_dump() {
		global $wpdb;
		$str_sql = "select * from view_pdg_teacher_extented";

		$teachers_data_db = $wpdb -> get_results($str_sql);

		if (empty($teachers_data_db)) {
			$return_message['error_type'] = 'no_result_found';
			$return_message['message'] = "Error! No data available.";
			return $return_message;
		}

		
		$excel_file = self::fn_return_teacher_excel($teachers_data_db);		
		
		$created_excel_url = $excel_file['url'];
		$created_excel_path = $excel_file['path'];
				
		$export_mail_data = array(
			'report_type' => 'Teacher',
			'file_url' => $created_excel_url,
			'file_path' => $created_excel_path,
		);
		
		self::mail_data_dump($export_mail_data);
				

		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data available';
		$return_message['data'] = '<h3>Report has been created successfully! Please click the download button to download the report.</h3>';
		return $return_message;
	}
	
	public static function fn_return_institute_excel($institute_dump_db) {
		$institute_excel_array = array();
		$teacher_notes_array = self::get_all_teacher_notes();
				
		foreach ($institute_dump_db as $institute_dump) {
			$user_id = $institute_dump->user_id;
			$teacher_note = '';
			if(array_key_exists($user_id, $teacher_notes_array)) {
				$teacher_note = $teacher_notes_array[$user_id];
			}
			
			$institute_excel_array[] = array(
				'Institute ID' => $institute_dump->institute_id,
				'Name' => $institute_dump -> institute_name,
				'User ID' => $institute_dump->user_id,				
				'User Name' => $institute_dump->user_name,
				'User Nicename' => $institute_dump->user_nicename,
				'User Email' => $institute_dump->user_email,
				'Display Name' => $institute_dump->display_name,
				'User Role Display' => $institute_dump->user_role_display,				
				'Mobile No' => $institute_dump->mobile_no,
				'Alternative Contact Number' => $institute_dump->alternative_contact_no,				
				'Contact Person Name' => $institute_dump->contact_person_name,
				'Primary Contact Number' => $institute_dump->primary_contact_no,
				'Alternate Contact Number' => $institute_dump->alternate_contact_no,
				'Other Communication Mode' => $institute_dump->other_communication_mode,
				'Address' => $institute_dump->correspondance_address,				
				'City Name' => $institute_dump->city_name,
				'Profile Heading' => $institute_dump->profile_heading,				
				'Size of Faculty' => $institute_dump->size_of_faculty,
				'Operation Hours From' => $institute_dump->operation_hours_from,
				'Operation Hours To' => $institute_dump->operation_hours_to,
				'About Coaching' => $institute_dump->about_coaching,				
				'Teaching Experience' => $institute_dump->teaching_xp,
				'Allow Installments' => $institute_dump->allow_installments,
				'Fees Stucture' => $institute_dump->about_fees_structure,
				'Demo Allowed' => $institute_dump->demo_allowed,
				'No of Demos Class' => $institute_dump->no_of_demo_class,
				'Price Per Demo' => $institute_dump->price_per_demo_class,
				'Approved' => $institute_dump->approved,
				'Active' => $institute_dump->active,
				'Verified' => $institute_dump->verified,
				'Deleted' => $institute_dump->deleted,
				'Created' => $institute_dump->created,
				'Updated' => $institute_dump->updated,
				'Localities' => $institute_dump->localities,
				'Subjects' => $institute_dump->subjects,
				'Class' => $institute_dump->subject_type,
				'Board' => $institute_dump->academic_board,
				'Age' => $institute_dump->course_age,
				'Notes' => $teacher_note
			);
		}
		
		$excel_data_array = array(
			'report_title' => 'institute_dump',
			'sheet_title' => date('Y-m-d'),
			'file_name' => 'institute_dump',
			'data' => $institute_excel_array
		);	
		
		$excel_file = new PDGExcelExport($excel_data_array);
		$excel_report_info = $excel_file->ExcelExport();		
		return  $excel_report_info;
	}

	public static function fn_return_teacher_excel($teachers_data_db) {
		$teacher_excel_array = array();
		
		$teacher_notes_array = self::get_all_teacher_notes();
		
		foreach ($teachers_data_db as $teacher_dump) {
			$user_id = $teacher_dump->user_id;
			$teacher_note = '';
			if(array_key_exists($user_id, $teacher_notes_array)) {
				$teacher_note = $teacher_notes_array[$user_id];
			}
			
			$teacher_excel_array[] = array(
				'Teacher ID' => $teacher_dump->teacher_id,				
				'User ID' => $user_id,				
				'User Name' => $teacher_dump->user_name,
				'User Nicename' => $teacher_dump->user_nicename,
				'User Email' => $teacher_dump->user_email,
				'Display Name' => $teacher_dump->display_name,
				'First Name' => $teacher_dump->first_name,
				'Last Name' => $teacher_dump->last_name,
				'User Role Display' => $teacher_dump->user_role_display,				
				'Mobile No' => $teacher_dump->mobile_no,
				'Alternative Contact Number' => $teacher_dump->alternative_contact_no,
				'Date Of Birth' => $teacher_dump->date_of_birth,
				'Gender' => $teacher_dump->gender,
				'Other Communication Mode' => $teacher_dump->other_communication_mode,
				'Other Qualification' => $teacher_dump->other_qualification,				
				'Operation Hours From' => $teacher_dump->operation_hours_from,
				'Operation Hours To' => $teacher_dump->operation_hours_to,
				'Profile Heading' => $teacher_dump->profile_heading,	
				'About Coaching' => $teacher_dump->about_coaching,				
				'Teaching Experience' => $teacher_dump->teaching_xp,
				'Student Knows By' => $teacher_dump->students_konws_by,
				'Allow Installments' => $teacher_dump->allow_installments,
				'Fees Stucture' => $teacher_dump->about_fees_structure,
				'Demo Allowed' => $teacher_dump->demo_allowed,
				'No of Demos Class' => $teacher_dump->no_of_demo_class,
				'Price Per Demo' => $teacher_dump->price_per_demo_class,
				'Approved' => $teacher_dump->approved,
				'Active' => $teacher_dump->active,
				'Verified' => $teacher_dump->verified,
				'Created' => $teacher_dump->created,
				'Updated' => $teacher_dump->updated,
				'Deleted' => $teacher_dump->deleted,
				'Localities' => $teacher_dump->localities,
				'Subjects' => $teacher_dump->subjects,
				'Class' => $teacher_dump->subject_type,
				'Board' => $teacher_dump->academic_board,
				'Age' => $teacher_dump->course_age,
				'Notes' => $teacher_note
			);
		}
		
		$excel_data_array = array(
			'report_title' => 'teacher_dump',
			'sheet_title' => 'teacher_dump_'.date('Y-m-d'),
			'file_name' => 'teacher_dump',
			'data' => $teacher_excel_array
		);
		
		$excel_file = new PDGExcelExport($excel_data_array);
		return $excel_file->ExcelExport(); 
	}

	public static function create_zip($files = array(), $destination = '',$overwrite = false) {
		
		//if the zip file already exists and overwrite is false, return false
		if(file_exists($destination) && !$overwrite) {
			 return false; 
		}
		//vars
		$valid_files = array();
		//if files were passed in...
		if(is_array($files)) {
			//cycle through each file
			foreach($files as $file) {
				//make sure the file exists
				if(file_exists($file)) {
					$valid_files[] = $file;
				}
			}
		}
		
		if(count($valid_files)) {
			//create the archive
			
			$zip = new ZipArchive();
			
			if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
				return false;
			}
			
			//add the files
			foreach($valid_files as $file) {
				$zip->addFile($file,basename($file));
			}
			
			//close the zip -- done!
			$zip->close();
			
			//check to make sure the file exists
			return file_exists($destination);			
		}
		else {
			return false;
		}
	}
	
	
	public static function mail_data_dump($data) {
		$report_type = $data['report_type']; // Teacher/Institute
		$excel_file_url = $data['file_url'];
		$excel_file_path = $data['file_path'];
		$date_time = date('YmdHis');		
		
		$zipped_report_file = PEDAGOGE_PLUGIN_DIR.'storage/temp/'.$date_time.$report_type.'_dump.zip';
		
		$zipped_report = self::create_zip(array($excel_file_path), $zipped_report_file);
		
		$email_message = '
			<h3>Email from Pedagoge Server!</h3>
			Hello Team! <br /><br />
			
			Please find Data Dump for '.$report_type.' in the attachment. In case it\'s not there then click on the link below to download the report.<br /><br />
			
			<strong><a href="'.$excel_file_url.'">Download '.$report_type.' Data Dump!</a></strong> (This download link may expire after few days!)<br /><br />
			
			Regards,<br />
			Niraj Kumar
		';
		$headers[] = 'From: Niraj Kumar <niraj.kumar@pedagoge.com>';		
		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		wp_mail( 'datadump@pedagoge.com', $report_type.' Data Dump - '.$date_time, $email_message, $headers, array($zipped_report_file) );
				
	}

	public static function get_all_teacher_notes() {
		global $wpdb;
		$notes_array = array();
		$notes_db = $wpdb->get_results("select * from pdg_teacher_notes");
		foreach($notes_db as $note_info) {
			$user_id = $note_info->user_id;
			$note = $note_info->note.' {{{'.$note_info->note_taker_name.'}}} ';
			if(array_key_exists($user_id, $notes_array)) {
				$note = $notes_array[$user_id].' => '.$note;
			}
			$notes_array[$user_id] = $note;
		}
		return $notes_array;
	}
	
}
