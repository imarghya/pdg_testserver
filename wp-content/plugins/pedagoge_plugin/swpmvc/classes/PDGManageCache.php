<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PDGManageCache {
	public function __construct() {
		
	}
	
	public static function fn_db_table_cache_array() {
		$cache_array = array(
			'pdg_course_type' => 'table',
			'pdg_subject_name' => 'table',					
			'pdg_locality' => 'table',
			'pdg_class_timing' => 'table',
			'pdg_subject_type' => 'table',
			'pdg_academic_board' => 'table',
			'pdg_teaching_xp' => 'table',
			'pdg_qualification' => 'table',
			'pdg_mode_of_contact' => 'table',
			'pdg_state' => 'table',
			'pdg_city' => 'table',
			'pdg_fees_type' => 'table',
			'pdg_schools' => 'table',
			'pdg_class_capacity' => 'table',
			'pdg_size_of_faculty' => 'table',
			'teacher_names_data' => 'custom',
			'pdg_course_age_category' => 'table',
			'registered_locality' => 'custom',
			'registered_subjects' => 'custom',
			'view_pdg_seo_combo' => 'table',
			'pdg_workshop_categories' => 'table'
		);
		return $cache_array;
	}
	
	public static function fn_create_cache($cache_name) {
		global $wpdb;
		
		$db_table_cache_array = self::fn_db_table_cache_array();
		
		$return_data = '';
		if(!empty($cache_name) && array_key_exists($cache_name, $db_table_cache_array)) {
			
			$cache_type = $db_table_cache_array[$cache_name];
			
			switch($cache_type) {
				case 'table':
					$return_data = $wpdb->get_results("select * from $cache_name");
					break;
					
				case 'custom':
					switch($cache_name) {
						case 'teacher_names_data':
							$return_data = ControllerSearch::fn_teacher_names_data();
							break;
						case 'registered_locality':
							$str_sql = "select * from pdg_locality where locality_id in (select distinct (locality_id) from pdg_tutor_location)";
							$return_data = $wpdb->get_results($str_sql);
							break;

						case 'registered_subjects':
							$str_subject_sql = "select distinct(subject_name) from view_pdg_batch_subject order by subject_name";
							$return_data = $wpdb->get_results($str_subject_sql);
							break;
					}
					break;
			}
			
			$file_path = PEDAGOGE_PLUGIN_DIR.'storage/cache/'.$cache_name.'.json';
			$str_json_data = json_encode($return_data);
			$handle = fopen($file_path, 'w');
			fwrite($handle, $str_json_data);
			fclose($handle);
		}
		return $return_data;
	}
	
	public static function fn_load_cache($cache_name, $assoc = false) {
		$return_data = '';
		if(!empty($cache_name)) {
			$file_path = PEDAGOGE_PLUGIN_DIR.'storage/cache/'.$cache_name.'.json';
			if(file_exists($file_path)) {
				// load and return
				$handle = fopen($file_path, 'r');
				$data = fread($handle,filesize($file_path));
				fclose($handle);
				
				$return_data = json_decode($data, $assoc);
			} else {
				$return_data = self::fn_create_cache($cache_name);
			}
		}
		return $return_data;
	}
	
	public static function fn_delete_cache($cache_name = '') {
		if(!empty($cache_name)) {
			$cache_file = PEDAGOGE_PLUGIN_DIR.'storage/cache/'.$cache_name.'.json';
			if(is_file($cache_file)) {
				unlink($cache_file);
			}
		} else {
			$storage_dir = PEDAGOGE_PLUGIN_DIR.'storage/cache/';
	        $scanned_directory = array_diff(scandir($storage_dir), array('..', '.'));
	        foreach($scanned_directory as $storage_file) {
	        	$extension = strtolower(pathinfo($storage_file, PATHINFO_EXTENSION));
				if($extension == 'json') {
					$file_path = $storage_dir.$storage_file;
					if(is_file($file_path)) {
						unlink($file_path);	
					}
				}
	        }
		}
	}
	
	public static function fn_update_cache($cache_name = '') {
		if(!empty($cache_name)) {
			self::fn_delete_cache($cache_name);
			self::fn_create_cache($cache_name);
		} else {
			self::fn_delete_cache();
			self::fn_create_all_cache();
		}
	}
	
	public static function fn_create_all_cache() {
		
		$db_table_cache_array = self::fn_db_table_cache_array();
		
		foreach($db_table_cache_array as $cache_name=>$cache_type) {
			self::fn_create_cache($cache_name);	
		}		
	}
	
}