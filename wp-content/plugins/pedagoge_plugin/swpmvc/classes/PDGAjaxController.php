<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Default Ajax handler for the Pedagoge Virtual Pages. All ajax calls route through this class to their proper handler (Class Methods). All ajax calls must send class name and method name via post/get for method invocation. Ajax Methods must be public and static.
 * $_REQUEST must contain the following variables 
 * 
 * 		pedagoge_callback_class - Contains the Class Name where the ajax method is defined.
 * 		pedagoge_callback_function - Contains the static Method Name which is to be executed.
 * 
 * Note - Virtual Pages are created by Controller Classes defined in pedagoge_plugin (Wordpress Plugin) 
 * 
 * @package pedagoge_plugin
 * @version 0.2
 * @author Niraj Kumar
 * @todo 
 * 		Check for nonces here. 
 * 		Include and check for registered user and visitor for all the ajax calls.
 * 		If there are any security related issues then return proper http code. 
 * 		Create and implement Laravel like middleware for nonce check, Form Validation, Authentication etc.
 */

class PDGAjaxController {
	
	/**
	 * Defines and adds hooks for ajax functions to wordpress. 
	 * It exposes the following handles to be used for ajax calls from clients.
	 *  
	 * 		wp_ajax_pedagoge_users_ajax_handler - Ajax handler for loggedin user.
	 * 		wp_ajax_pedagoge_visitor_ajax_handler - Ajax handler for visitors who have not logged in.
	 * 		wp_ajax_nopriv_pedagoge_visitor_ajax_handler - alias of wp_ajax_pedagoge_visitor_ajax_handler
	 * 
	 * @author Niraj Kumar
	 */
	public function __construct() {
		add_action( 'wp_ajax_pedagoge_users_ajax_handler', array( $this, 'pedagoge_users_ajax_handler' ) );
		add_action( 'wp_ajax_pedagoge_visitor_ajax_handler', array( $this, 'pedagoge_visitor_ajax_handler' ) );
		add_action( 'wp_ajax_nopriv_pedagoge_visitor_ajax_handler', array( $this, 'pedagoge_visitor_ajax_handler' ) );
	}
	
	/**
	 * Wordpress hooked method to handle Logged in user's ajax calls. This method  routes the ajax calls to pdg_ajax_handler private method.
	 * 
	 * @author Niraj Kumar
	 */
	public function pedagoge_users_ajax_handler() {
		//@todo Visitor needs to be logged-in to continue.
		$this->pdg_ajax_handler();
	}
	
	/**
	 * Wordpress hooked method to handle ajax calls of anonymous visitors. This method routes the ajax calls to pdg_ajax_handler private method.
	 * 
	 * @author Niraj Kumar
	 */
	public function pedagoge_visitor_ajax_handler() {
		$this->pdg_ajax_handler();
	}
	
	/**
	 * Private method which extracts the class and method name from the $_REQUEST variables and invokes the static function if found. 
	 * 
	 
	 * 
	 * @author Niraj Kumar
	 */
	private function pdg_ajax_handler() {
		//@todo implement and process requests middlewares
		if(isset($_REQUEST['pedagoge_callback_function']) && isset($_REQUEST['pedagoge_callback_class'])) {
			$callback_function = $_REQUEST['pedagoge_callback_function'];
			$callback_class = $_REQUEST['pedagoge_callback_class'];
			
			if(class_exists($callback_class)) {
				if(method_exists($callback_class, $callback_function)) {
					$callback_class::$callback_function();
				} else {
					wp_die('Class name was not found.');
				}
			} else {
				wp_die('Method name was not found.');
			}
			
		} else {			
			//@todo implement and send 403 Forbidden request
			wp_die('You must send Class and Function Name which will be invoked.');
		}
	}
}
