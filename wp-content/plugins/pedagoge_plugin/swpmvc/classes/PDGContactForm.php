<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Class contains ajax method to handle contact us page form submission. This class is over-ridden by ControllerBusinessContactForm. However, this class contains a utility function to refresh application nonce (secret) key for nonce check.
 * 
 * @author Niraj Kumar
 */
class PDGContactForm {
	
	public static function contact_form_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => ''
		);
		
		$contact_name_value = sanitize_text_field($_POST['contact_name_value']);
		$contact_email_value = sanitize_text_field($_POST['contact_email_value']);
		$contact_number_value = sanitize_text_field($_POST['contact_number_value']); 
		$contact_message_value = sanitize_text_field($_POST['contact_message_value']);
		
		$hnpt_value = '';
		if(isset($_POST['hnpt_val'])) {
			$hnpt_value = sanitize_text_field($_POST['hnpt_val']);	
		}
		
		if(strlen($hnpt_value)>0) {
			$return_message['message'] = '<h3>Invalid entry. Try again!</h3>';
			echo json_encode($return_message);
			die();
		}		
		
		if(empty($contact_email_value) || empty($contact_email_value) || empty($contact_number_value) || empty($contact_message_value)) {
			
			$return_message['message'] = '<h3>Invalid entry. Try again!</h3>';
			echo json_encode($return_message);
			die();
		}

		if (!filter_var($contact_email_value, FILTER_VALIDATE_EMAIL) === true) {
   			 //echo("$email is a valid email address");
   			 $return_message['message'] = '<h3>Email Address is invalid</h3>';
			echo json_encode($return_message);
			die();
		}

		if(!preg_match("/^[0-9]+$/", $contact_number_value)) {
  			
  			$return_message['message'] = '<h3>Phone no. is invalid</h3>';
			echo json_encode($return_message );
			die();
		}
		
		$ip_address = pedagoge_get_visitor_ip();
		if(empty($ip_address) || strlen($ip_address)<5) {
			$return_message['message'] = '<h3>IP Address information is not available.</h3>';
			echo json_encode($return_message );
			die();
		}
		
		$current_date = date("Y-m-d");
		$submission_count_sql = "
			select 
				count(id) 
			from pdg_contact_form_logger 
			where 
				ip_address='$ip_address' and 
				submission_date BETWEEN UNIX_TIMESTAMP('$current_date') AND UNIX_TIMESTAMP('$current_date 23:59:59')
		";
		
		$submission_count = $wpdb->get_var($submission_count_sql);
		
		if($submission_count>10) {
			$return_message['message'] = '<h3>You have already submitted too many enquiries today! Rest assured, Someone will get back to your soon. Please try again tomorrow.</h3>';
			echo json_encode($return_message );
			die();
		}
		
		$insert_array = array(
			'ip_address'=>$ip_address,
			'submission_date'=> $current_date,
			'name' => $contact_name_value,
			'email'=>$contact_email_value,
			'contact_number'=>$contact_number_value,
			'message'=>$contact_message_value,
		);
		$insert_array_format = array(
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
		);
		
		$wpdb->insert('pdg_contact_form_logger', $insert_array, $insert_array_format);
		
		$template_vars['contact_name'] = $contact_name_value;
		$template_vars['contact_email'] = $contact_email_value;
		$template_vars['contact_number'] = $contact_number_value;
		$template_vars['contact_message'] = $contact_message_value;
		
		//send to admin
		$mail_data = array(
			'to_address' => 'hello@pedagoge.com',
			'mail_type' => 'contact_us_form_submission_notification_to_admin',
			'contact_name' => $contact_name_value,
			'contact_email' => $contact_email_value,
			'contact_number' => $contact_number_value,
			'contact_message' => $contact_message_value,
		);
		$pedagoge_mailer = new PedagogeMailer($mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$pedagoge_mailer = null;
		
		//Send to user				
		$new_mail_data = array(
			'to_address' => $contact_email_value,
			'mail_type' => 'contact_us_form_submission_notification_to_user',
		);
		$pedagoge_mailer = new PedagogeMailer($new_mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$pedagoge_mailer = null;	
		
		$return_message['error'] = FALSE;
		$return_message['message'] = '<h3>Thank you for contacting us, we shall get back to you soon.</h3>';
		
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_load_secret_key() {
		echo wp_create_nonce( "pedagoge" );
		die();
	}
	
}