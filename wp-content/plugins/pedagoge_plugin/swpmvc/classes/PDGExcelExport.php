<?php

class PDGExcelExport {
	
	private $report_title;
	private $sheet_title;
	private $file_name;
	private $data_array;
	private $file_location;
	private $file_url;
	
	public function __construct($data) {
		if(isset($data['report_title'])) {
			$this->report_title = $data['report_title'];
		} else {
			$this->report_title = 'Pedagoge Excel Report';
		}
		
		if(isset($data['sheet_title'])) {
			$this->sheet_title = $data['sheet_title'];
		} else {
			$this->sheet_title = 'Pedagoge Report Excel Sheet';
		}
		
		$date_time = date('YmdHis');
		if(isset($data['file_name'])) {
			$this->file_name = $date_time.$data['file_name'].'.xls';
		} else {
			$this->file_name = $date_time.'pedagoge_report.xls';
		}
		
		if(isset($data['data']) && is_array($data['data'])) {
			$this->data_array = $data['data'];
		}
		
		$this->file_location = PEDAGOGE_PLUGIN_DIR.'storage/temp/';
		
		if(!is_dir($this->file_location)) {
			mkdir($this->file_location);
		}
		
		$this->file_url = PEDAGOGE_PLUGIN_URL.'/storage/temp/'.$this->file_name;
	}
	
	public function ExcelExport() {	
		
		// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setTitle($this->report_title);
		
		$objPHPExcel->getActiveSheet()->fromArray(array_keys(current($this->data_array)), null, 'A1');
		$cell_length_to = count(array_keys(current($this->data_array)));	
		
		// Fill worksheet from values in array
        $objPHPExcel->getActiveSheet()->fromArray($this->data_array, null, 'A2');
		// Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($this->sheet_title);
        
        // Save Excel 2007 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		
		$file_name = $this->file_location.$this->file_name;
        $objWriter->save($file_name);
		
		$return_data = array(
			'url' => $this->file_url,
			'path' => $file_name
		);
		
		return $return_data;
	}
}