<?php

class PDGViewLoader {
	
	/**
	 * All views are relative to the folder views/
	 * just name the file and not its extensions.
	 */
	public static function load_view($view, $template_vars = null) {
			
		$view_file_name = PEDAGOGE_PLUGIN_DIR.'views/'.$view.'.html.php';
		
		$str_return = '';
		
		if(file_exists($view_file_name)) {		
			
			if(isset($template_vars) && !empty($template_vars)) {
				extract($template_vars);
			}
			
			ob_start();
			include_once($view_file_name);			
			$str_return = ob_get_clean();
			
		} else {
			$str_return = 'Error! Loading view '.$view_file_name;	
		}
		
		return $str_return;
	}
}
