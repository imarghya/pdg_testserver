<?php
/**
 * Asset Loader class. Master Controller class uses this to register and load assets (css/JS)
 */
 
 
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
	
/**
 * Assets Loader for the Pedagoge Web Application
 */
class PDGAssetsLoader {
	
	private $loaded_styles = null;
	private $loaded_scripts_in_header = null;
	private $loaded_scripts_in_footer = null;
	
	
	private $registered_styles = null;
	private $registered_scripts = null;
	
	public function __construct() {
		$this->loaded_styles = array();
		$this->loaded_scripts_in_header = array();
		$this->loaded_scripts_in_footer = array();
		
		$this->registered_scripts = array();
		$this->registered_styles = array();
		$this->fn_register_scripts();
		$this->fn_register_styles();
	}

	private function fn_register_scripts() {
		
		$scripts_list = array();
		
		// Bower Components			
		
		$scripts_list[] = "jquery.min#".BOWER_ROOT_URL."/jquery/dist/jquery.min.js"; //https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js
		$scripts_list[] = "jquery#".BOWER_ROOT_URL."/jquery/dist/jquery.js"; //https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js
		
		$scripts_list[] = "jquery-ui#".BOWER_ROOT_URL."/jquery-ui/jquery-ui.min.js";
		
		//$scripts_list[] = "datatables#".BOWER_ROOT_URL."/datatables/media/js/jquery.dataTables.min.js";
		$scripts_list[] = "datatables#".BOWER_ROOT_URL."/datatables/media/js/jquery.dataTables.js";
		$scripts_list[] = "datatables-bootstrap#".BOWER_ROOT_URL."/datatables/media/js/dataTables.bootstrap.min.js";
		
		$scripts_list[] = "typehead.bundle#".BOWER_ROOT_URL."/typeahead.js/dist/typeahead.bundle.min.js";
		$scripts_list[] = "typehead.jquery#".BOWER_ROOT_URL."/typeahead.js/dist/typeahead.jquery.min.js";
		
		$scripts_list[] = "pace#".BOWER_ROOT_URL."/PACE/pace.min.js";
		$scripts_list[] = "jquery-blockui#".BOWER_ROOT_URL."/blockUI/jquery.blockUI.js";
		$scripts_list[] = "bootstrap#".BOWER_ROOT_URL."/bootstrap/dist/js/bootstrap.min.js";
		$scripts_list[] = "jquery-slimscroll#".BOWER_ROOT_URL."/jquery-slimscroll/jquery.slimscroll.min.js";
		$scripts_list[] = "switchery#".BOWER_ROOT_URL."/switchery/dist/switchery.min.js";
		$scripts_list[] = "uniform#".BOWER_ROOT_URL."/jquery.uniform/jquery.uniform.min.js";
		$scripts_list[] = "waves#".BOWER_ROOT_URL."/Waves/dist/waves.min.js";
		
		$scripts_list[] = "cropper#".BOWER_ROOT_URL."/cropper/dist/cropper.min.js";
		$scripts_list[] = "bootbox#".BOWER_ROOT_URL."/bootbox.js/bootbox.js";
		$scripts_list[] = "select2#".BOWER_ROOT_URL."/select2/dist/js/select2.full.min.js";
		$scripts_list[] = "bootstrapwizard#".BOWER_ROOT_URL."/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js";
		$scripts_list[] = "moment#".BOWER_ROOT_URL."/moment/min/moment.min.js";
		$scripts_list[] = "bsdatetimepicker#".BOWER_ROOT_URL."/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js";
		$scripts_list[] = "lodash#".BOWER_ROOT_URL."/lodash/dist/lodash.min.js";
		
		
		$scripts_list[] = "validationengine#".BOWER_ROOT_URL."/validationEngine/js/jquery.validationEngine.js";
		$scripts_list[] = "validationengine-en#".BOWER_ROOT_URL."/validationEngine/js/languages/jquery.validationEngine-en.js";
			
		// Application Assets		
		$scripts_list[] = "modernizr#".PEDAGOGE_ASSETS_URL."/plugins/3d-bold-navigation/js/modernizr.js";
		$scripts_list[] = "snap-svg#".PEDAGOGE_ASSETS_URL."/plugins/offcanvasmenueffects/js/snap.svg-min.js";
		$scripts_list[] = "offcanvasmenueffects#".PEDAGOGE_ASSETS_URL."/plugins/offcanvasmenueffects/js/classie.js";
				
		//Ankitesh's Script
		$scripts_list[] = "ankitesh-sha512#".PEDAGOGE_ASSETS_URL."/js/ankitesh/sha512.js";
		$scripts_list[] = "ankitesh-forms#".PEDAGOGE_ASSETS_URL."/js/ankitesh/forms.js";
		$scripts_list[] = "ankitesh-header#".PEDAGOGE_ASSETS_URL."/js/ankitesh/header.js";
		$scripts_list[] = "ankitesh-modern#".PEDAGOGE_ASSETS_URL."/js/ankitesh/modern.min.js";
		
		// Application JS Common File
		$scripts_list[] = "pedagoge_common#".PEDAGOGE_ASSETS_URL."/js/pedagoge_common.js";
		
		//insert styles into registered styles array
		foreach( $scripts_list as $scripts_string ) {
			$string_list_arr = explode( "#", $scripts_string );				
			$handle = $string_list_arr[0];
			$location = $string_list_arr[1];
			$this->registered_scripts[ $handle ] = $location;				
		}
	}
	
	private function fn_register_styles() {
		$styles_list = array();
		$styles_list[] = "google_opensans#http://fonts.googleapis.com/css?family=Open+Sans:400,300,600";
		
		// Bower Components
		$styles_list[] = "jqueryui#".BOWER_ROOT_URL."/jquery-ui/themes/base/jquery-ui.min.css";
		$styles_list[] = "pace#".BOWER_ROOT_URL."/PACE/themes/blue/pace-theme-flash.css";
		$styles_list[] = "uniform#".BOWER_ROOT_URL."/jquery.uniform/themes/default/css/uniform.default.min.css";
		$styles_list[] = "bootstrap#".BOWER_ROOT_URL."/bootstrap/dist/css/bootstrap.min.css";
		$styles_list[] = "fontawesome#".BOWER_ROOT_URL."/font-awesome/css/font-awesome.min.css";
		$styles_list[] = "simple-line-icons#".BOWER_ROOT_URL."/simple-line-icons/css/simple-line-icons.css";
		$styles_list[] = "waves#".BOWER_ROOT_URL."/Waves/dist/waves.min.css";
		$styles_list[] = "switchery#".BOWER_ROOT_URL."/switchery/dist/switchery.min.css";
		$styles_list[] = "validationEngine#".BOWER_ROOT_URL."/validationEngine/css/validationEngine.jquery.css";
		$styles_list[] = "cropper#".BOWER_ROOT_URL."/cropper/dist/cropper.min.css";
		$styles_list[] = "select2#".BOWER_ROOT_URL."/select2/dist/css/select2.min.css";
		$styles_list[] = "select2-bootstrap-theme#".BOWER_ROOT_URL."/select2-bootstrap-theme/dist/select2-bootstrap.min.css";
		$styles_list[] = "bsdatetimepicker#".BOWER_ROOT_URL."/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css";
		
		$styles_list[] = "datatables-bootstrap#".BOWER_ROOT_URL."/datatables/media/css/dataTables.bootstrap.min.css";
		
		
		//Application Assets
		$styles_list[] = "offcanvasmenueffects#".PEDAGOGE_ASSETS_URL."/plugins/offcanvasmenueffects/css/menu_cornerbox.css";
		$styles_list[] = "3d-bold-navigation#".PEDAGOGE_ASSETS_URL."/plugins/3d-bold-navigation/css/style.css";		
		$styles_list[] = "ankitesh-search#".PEDAGOGE_ASSETS_URL."/css/ankitesh/search.css";
		$styles_list[] = "ankitesh-modern#".PEDAGOGE_ASSETS_URL."/css/ankitesh/modern.css";
		$styles_list[] = "ankitesh-theme-green#".PEDAGOGE_ASSETS_URL."/css/ankitesh/themes/green.css";
		$styles_list[] = "ankitesh-custom#".PEDAGOGE_ASSETS_URL."/css/ankitesh/custom.css";
		$styles_list[] = "ankitesh-validationEngine#".PEDAGOGE_ASSETS_URL."/css/ankitesh/validationEngine.jquery.css";
		
		
		$styles_list[] = "pedagoge_common#".PEDAGOGE_ASSETS_URL."/css/pedagoge_common.css";
				
		//insert styles into registered styles array
		foreach( $styles_list as $style_string ) {
			$string_list_arr = explode( "#", $style_string );				
			$handle = $string_list_arr[0];
			$location = $string_list_arr[1];
			$this->registered_styles[ $handle ] = $location;				
		}
	}
	
	public function fn_load_scripts() {
		$assets_array = array();
		$assets_array[ 'css' ] = $this->registered_styles;
		$assets_array[ 'js' ] = $this->registered_scripts;			
		
		return $assets_array;				
	}		
} // Class ends here
