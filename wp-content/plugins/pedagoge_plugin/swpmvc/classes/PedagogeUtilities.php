<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PedagogeUtilities{
	
	/**
	 * This function checks if the secret key matches with user's email address
	 * 1. Locates wordpress user's email address via user id
	 * 2. Encrypts email address and tries to match with the secret key
	 * 3. Returns error and message on failure.
	 * 4. Returns message and wordpress user on success
	 * 
	 * @param integer $user_id (alternative - $hidden_dynamic_user_var)
	 * 
	 * @param string $secret_key (alternative - $hidden_dynamic_profile_edit_secret_key)
	 * 
	 * @return array 
	 */
	public static function user_secret_key_match($user_id, $secret_key) {
		global $wpdb;
		$return_message = self::return_message_format();
		
		if(!is_numeric($user_id) || $user_id <= 1) {
			$return_message['error_type'] = 'empty_user_info';
			$return_message['message'] = 'Error! User information not correct. Please try again.';
			return $return_message;
		}
		
		$wp_user = get_user_by('id', $user_id);
		$users_email = $wp_user->user_email;
		require_once( ABSPATH . 'wp-includes/class-phpass.php');
		$wp_hasher = new PasswordHash(8, TRUE);
		
		if(!$wp_hasher->CheckPassword($users_email, $secret_key)) {
		    $return_message['error_type'] = 'secret_key_unmatched';
			$return_message['message'] = 'Error! Users secret key do no match! Please try again.';
			return $return_message;
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Secrets matched!';
		$return_message['data'] = $wp_user;
		return $return_message;
	}
	
	/**
	 * Function checks for 
	 */
	public static function ajax_session_security_check() {
		$return_message = self::return_message_format();
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
	}
	
	/**
	 * Function returns default format to be used in all ajax calls for return json response.
	 * 
	 * @return array
	 */
	public static function return_message_format() {
		return array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => array()
		);
	}

	public static function is_production_env() {
		$production_server = 'www.pedagoge.com';
		$is_production_environment = false;
		
		if($_SERVER['SERVER_NAME'] == $production_server) {
			$is_production_environment = true;
		}
		
		return $is_production_environment;
	}	
}
