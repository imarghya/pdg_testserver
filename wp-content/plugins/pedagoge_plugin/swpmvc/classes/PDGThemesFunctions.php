<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class PDGThemesFunctions {
	public function __construct() {

	}

	public function landingJul09SQL( $idList ) {
		global $wpdb;
		$sql             = "SELECT
			substring_index(Group_concat(DISTINCT view_pdg_tutor_location.locality SEPARATOR ', '), ',', 2) AS locality,
			Group_concat(DISTINCT view_pdg_batch_subject.subject_name SEPARATOR ', ') AS `subject`,
			view_teacher_institute_combined.user_id,
			view_teacher_institute_combined.teach_insti_id,
			view_teacher_institute_combined.user_role_name,
			view_teacher_institute_combined.user_name,
			view_teacher_institute_combined.total_count,
			view_teacher_institute_combined.avg_rating,
			view_teacher_institute_combined.teacher_name,
			pdg_recommendations.recommended_to
			FROM
				view_teacher_institute_combined
			LEFT JOIN pdg_recommendations
			ON view_teacher_institute_combined.user_id = pdg_recommendations.recommended_to 
			JOIN view_pdg_tutor_location
			ON view_teacher_institute_combined.user_id = view_pdg_tutor_location.tutor_institute_user_id 
			JOIN view_pdg_batch_subject
			ON view_teacher_institute_combined.user_id = view_pdg_batch_subject.tutor_institute_user_id
			WHERE
				user_id in (" . $idList . ") and approved = 'yes' and active = 'yes' and profile_deleted = 'no'
			GROUP BY
				view_teacher_institute_combined.user_id
			ORDER BY FIELD(user_id, " . $idList . ")";
		$result          = $wpdb->get_results( $sql );
		$teacher_array   = array ();
		$institute_array = array ();
		foreach ( $result as $found_data ) {
			$teacher_insti_id = $found_data->teach_insti_id;
			$user_role        = $found_data->user_role_name;
			switch ( $user_role ) {
				case 'teacher':
					$teacher_array[] = $teacher_insti_id;
					break;
				case 'institution':
					$institute_array[] = $teacher_insti_id;
					break;
			}
		}

		$teacher_sql   = "select user_id, profile_slug from view_pdg_teachers where teacher_id in (" . implode( ',', $teacher_array ) . ")";
		$institute_sql = "select user_id, profile_slug from view_pdg_institutes where institute_id in (" . implode( ',', $institute_array ) . ")";
		$teacher_db    = $wpdb->get_results( $teacher_sql );
		$institute_db  = $wpdb->get_results( $institute_sql );

		$slugData = array ();

		if ( ! empty( $institute_db ) && is_array( $institute_db ) ) {
			foreach ( $institute_db as $idb ) {
				$slugData[ $idb->user_id ] = home_url( '/institute/' . $idb->profile_slug );
			}
		}

		if ( ! empty( $teacher_db ) && is_array( $teacher_db ) ) {
			foreach ( $teacher_db as $tdb ) {
				$slugData[ $tdb->user_id ] = home_url( '/teacher/' . $tdb->profile_slug );
			}
		}

		$profile_image_path        = 'storage/uploads/images/';
		$teacher_profile_image_url = '';

		return array (
			'profile_image_path'        => $profile_image_path,//storage/uploads/images
			'teacher_profile_image_url' => $teacher_profile_image_url,
			'result'                    => $result,
			'slugData'                  => $slugData,
		);
	}
}
