<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * This class creates view for website's signup page and Processes User and Social Signup.
 * 
 * @author nirajkvinit 
 */
class ControllerSignup extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Signup';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		
		$file_version = '0.4';

		parent::fn_themes_ver2_load_scripts();
		$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine_en'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_js['ver2_common_desktop'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/common-desktop.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['pedagoge_signup'] = PEDAGOGE_ASSETS_URL . "/js/user_signup.js?ver=" . $this->fixed_version . "." . $file_version;
		$this->app_js['social_login'] = PEDAGOGE_ASSETS_URL . "/js/social_login.js?ver=" . $this->fixed_version . "." . $file_version;

		//$this->app_css['ver2_common_desktop'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/common-desktop.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['ver2_login'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/login.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css?ver=' . $this->fixed_version . "." . $file_version;

	}

	public function fn_get_header() {
		return $this->load_view( 'ver2/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'ver2/footer' );
	}

	public function fn_get_content() {

		if ( is_user_logged_in() ) {
			$content = '
				<h1>You are already logged in!</h1>
				<p><strong>Please wait.. redirecting...</strong></p>
				<META http-equiv="refresh" content="3; URL=' . home_url( '/' ) . '">
			';
		} else {

			$user_roles_model = new ModelUserRole();
			$user_role = $user_roles_model->get_public_roles();
			$template_vars['user_role'] = $user_role;

			$content = $this->load_view( 'signup/ver2/index', $template_vars );
		}

		return $content;
	}


	/**
	 * Function to register common variables to global application variables registry
	 */
	private function fn_register_common_variables() {
	}


	public static function user_signup_ajax() {
		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		
		$first_name = $display_name = $full_name = trim( sanitize_text_field( $_POST['full_name'] ) );
		$last_name = '';
		
		$user_name = $email_address = trim( sanitize_email( $_POST['email_address'] ) );		
		$mobile_number = trim( sanitize_user( $_POST['mobile_number'] ) );
		$password = sanitize_text_field( $_POST['password'] );
		$user_role = trim( sanitize_text_field( $_POST['user_role'] ) );
		
		$parts = explode(" ", $full_name);
		if(count($parts) > 1) {
			$last_name = array_pop($parts);
			$first_name = implode(" ", $parts);	
		}

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( ! filter_var( $email_address, FILTER_VALIDATE_EMAIL ) ) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Please input proper email address.';
			echo json_encode( $return_message );
			die();
		}
		
		if(!is_numeric($mobile_number) && strlen($mobile_number)!=10) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = '10 Digit valid mobile number is required for signing up!';
			echo json_encode( $return_message );
			die();
		}
		
		if ( trim( $password ) == "" || trim( $user_role ) == "" || trim( $full_name ) == "" ) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Full name, password and user role are required while signing up.';
			echo json_encode( $return_message );
			die();
		}

		/**
		 * check for duplicate username
		 * check for duplicate email address
		 */

		$str_duplicate_email_username = "
			select user_login, user_email from wp_users where user_login like '$user_name' or user_email like '$email_address'
		";

		$duplicate_users = $wpdb->get_results( $str_duplicate_email_username );
		if ( ! empty( $duplicate_users ) ) {
			$duplicate_user_name = '';
			$duplicate_email = '';
			foreach ( $duplicate_users as $found_user ) {
				$duplicate_user_name = strtolower( trim( $found_user->user_login ) );
				$duplicate_email = strtolower( trim( $found_user->user_email ) );
			}
			if ( $duplicate_email == $email_address ) {
				//throw duplicate email error
				$return_message['error_type'] = 'duplicate_email';
				$return_message['message'] = 'Error! This email address is already registered! Please use another email address and try again.';

				echo json_encode( $return_message );
				die();
			}
			if ( $duplicate_user_name == $user_name ) {
				//throw duplicate username error
				$return_message['error_type'] = 'duplicate_username';
				$return_message['message'] = 'Error! User Name is already registered! Please use another user name and try again.';

				echo json_encode( $return_message );
				die();
			}

			//throw duplicate username error
			$return_message['error_type'] = 'duplicate_username';
			$return_message['message'] = 'Error! User Name or email address is already registered! Please try again.';

			echo json_encode( $return_message );
			die();
		}
		
		/**
		 * Check for duplicate phone number
		 */
		$str_duplicate_mobile_sql = "select user_id from pdg_user_info where mobile_no like '$mobile_number';";
		$duplicate_mobile_no = $wpdb->get_results($str_duplicate_mobile_sql);
		if(!empty($duplicate_mobile_no)) {
			$return_message['message'] = 'Error! This mobile number is already registered! Please input another mobile no and try again.';
			echo json_encode( $return_message );
			die();
		}
		
		switch ( $user_role ) {
			case 'teacher':
			case 'institution':
			case 'student':
			case 'guardian':
			case 'trainer':
				break;
			default:
				$return_message['error_type'] = 'user_role';
				$return_message['message'] = 'Error! You must select a user role. Please try again.';

				echo json_encode( $return_message );
				die();
				break;
		}

		$user_role_id = '';

		$user_role_columns_array = array (
			'user_role_name' => $user_role,
			'deleted'        => 'no'
		);

		$master_model = new ModelMaster( 'pdg_user_role' );
		$user_role_data = $master_model->get_records_by_table_name( 'pdg_user_role', $user_role_columns_array );

		if ( ! empty( $user_role_data ) ) {
			foreach ( $user_role_data as $user_role_info ) {
				$user_role_id = $user_role_info->user_role_id;
			}
		}

		$userdata = array (
			'first_name'   => $first_name,
			'last_name'    => $last_name,
			'display_name' => $display_name,
			'user_login'   => $user_name,
			'user_email'   => $email_address,
			'role'         => $user_role,
			'user_pass'    => $password
		);

		$saved_user_id = wp_insert_user( $userdata );

		if ( ! is_wp_error( $saved_user_id ) && $saved_user_id != 0 ) {

			/**
			 * if user registration is successful then
			 * 1. Update his meta information
			 * 2. log him in.
			 */
			
			$str_save_user_msg = 'Success! Your information was registered successfully! <br/> Please wait logging you in!';
			
			//Signup info/IP address save
			$social_login_data = array (
				'user_id'         => $saved_user_id,
				'social_location' => 'website'
			);
			ControllerSignup::social_login_save_info($social_login_data);
			
			/**
			 * 1. register into userinfo table
			 * 2. Generate account activation key
			 * 3. send account activation email
			 */

			$salt = wp_generate_password( 20 ); // 20 character "random" string
			$key = sha1( $salt . $email_address . uniqid( time(), true ) );

			$data_array = array (
				'user_id'        => $saved_user_id,
				'user_role_id'   => $user_role_id,
				'activation_key' => $key,
				'mobile_no' => $mobile_number
			);
			$data_format_array = array (
				'%d',
				'%d',
				'%s',
				'%s'
			);

			$result = $wpdb->insert( 'pdg_user_info', $data_array, $data_format_array );

			if ( false === $result ) {
				//do nothing
			} else {

				switch ( $user_role ) {
					case 'teacher':
						$insert_array = array (
							'user_id' => $saved_user_id,
						);
						$wpdb->insert( 'pdg_teacher', $insert_array, array ( '%d' ) );
						break;
					case 'institution':
						$insert_array = array (
							'user_id' => $saved_user_id,
							'city_id' => 1
						);
						$wpdb->insert( 'pdg_institutes', $insert_array, array ( '%d', '%d' ) );
						break;
					case 'student':
					case 'guardian':
						// Fix this later
						break;
					case 'trainer':
						$insert_array = array (
							'user_id' => $saved_user_id,
						);
						$wpdb->insert( 'pdg_trainer', $insert_array, array ( '%d' ) );
						break;
				}

				//Send Account Activation Email
				$account_activation_url = add_query_arg( 'activation_key', $key, home_url( '/activate' ) );
				$mail_data = array (
					'to_address'             => $email_address,
					'mail_type'              => 'user_signup_and_account_activation',
					'account_activation_url' => $account_activation_url
				);
				$pedagoge_mailer = new PedagogeMailer( $mail_data );
				$mail_process_completed = $pedagoge_mailer->sendmail();
				$pedagoge_mailer = null;

				//Send Signup Notification
				$new_mail_data = array (
					'to_address' => 'ask@pedagoge.com',
					'mail_type'  => 'user_signup_and_account_activation_notice',
					'user_name'  => $user_name,
					'user_role'  => $user_role,
					'user_email' => $email_address
				);
				$pedagoge_mailer = new PedagogeMailer( $new_mail_data );
				$mail_process_completed = $pedagoge_mailer->sendmail();
				$pedagoge_mailer = null;
			}


			$return_message['error'] = false;
			$return_message['message'] = $str_save_user_msg;
			$return_message['user_id'] = $saved_user_id;

			$user_info = array ();
			$user_info['user_login'] = $user_name;
			$user_info['user_password'] = $password;
			$user_info['remember'] = 'true';

			$user_signon = wp_signon( $user_info, false );

		} else {

			$error_msg = '';
			if ( is_object( $saved_user_id ) ) {
				$error_msg = $saved_user_id->get_error_message();
			}

			$str_save_user_msg = 'Error registering your details! Please try again!';

			$return_message['error_message'] = $str_save_user_msg . $error_msg;
		}
		echo json_encode( $return_message );
		die();
	}
	
	/**
	 * This ajaxified function is used to create student/guardian user account.
	 * This function is being called from Dashboard's Reviews Page to create a user manually. 
	 */
	public static function fn_create_student_guardian() {
		global $wpdb;
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'
		);


		$select_student_create_role = sanitize_text_field( $_POST['select_student_create_role'] );
		$select_student_create_gender = sanitize_text_field( $_POST['select_student_create_gender'] );
		$txt_student_create_age = sanitize_text_field( $_POST['txt_student_create_age'] );
		$txt_student_create_mobile = sanitize_text_field( $_POST['txt_student_create_mobile'] );
		$txt_student_create_email = sanitize_text_field( $_POST['txt_student_create_email'] );
		$txt_student_create_first_name = sanitize_text_field( $_POST['txt_student_create_first_name'] );
		$txt_student_create_last_name = sanitize_text_field( $_POST['txt_student_create_last_name'] );

		if ( ! is_numeric( $txt_student_create_age ) || $txt_student_create_age <= 5 ) {
			$return_message['message'] = 'Please input age';
			echo json_encode( $return_message );
			die();
		}

		if ( ! is_numeric( $txt_student_create_mobile ) || strlen( $txt_student_create_mobile ) != 10 ) {
			$return_message['message'] = 'Please input 10 digit mobile number.';
			echo json_encode( $return_message );
			die();
		}

		if ( ! filter_var( $txt_student_create_email, FILTER_VALIDATE_EMAIL ) ) {
			$return_message['message'] = 'Please input proper email address.';
			echo json_encode( $return_message );
			die();
		}

		$registered_emails = array ();
		$registered_user_names = array ();
		$wpuserdata = $wpdb->get_results( "select user_email, user_login from wp_users" );

		foreach ( $wpuserdata as $user_data ) {
			$user_name = $user_data->user_login;
			$email = $user_data->user_email;

			if ( ! in_array( $user_name, $registered_user_names ) ) {
				$registered_user_names[] = $user_name;
			}

			if ( ! in_array( $email, $registered_emails ) ) {
				$registered_emails[] = $email;
			}
		}

		// check for duplicate email address
		if ( in_array( $txt_student_create_email, $registered_emails ) ) {
			$return_message['message'] = 'This email is already registered. Please input another email address.';
			echo json_encode( $return_message );
			die();
		}

		//check for duplicate phone number		
		$duplicate_mobile = $wpdb->get_results( "select * from pdg_user_info where mobile_no like '$txt_student_create_mobile'" );
		if ( ! empty( $duplicate_mobile ) ) {
			$return_message['message'] = 'This Mobile no is already registered. Please input another mobile no.';
			echo json_encode( $return_message );
			die();
		}

		$user_roles_db = $wpdb->get_results( "select * from pdg_user_role" );
		$user_roles_array = array ();
		foreach ( $user_roles_db as $user_role ) {
			$user_role_id = $user_role->user_role_id;
			$user_role_name = $user_role->user_role_name;
			if ( ! array_key_exists( $user_role_name, $user_roles_array ) ) {
				$user_roles_array[ $user_role_name ] = $user_role_id;
			}
		}

		$student_user_name = sanitize_title( $txt_student_create_first_name . $txt_student_create_last_name );
		$student_user_name = str_replace( '-', '_', $student_user_name );
		if ( in_array( $student_user_name, $registered_user_names ) ) {
			/**
			 * randomize username
			 */
			$random_no = wp_generate_password( 3 );
			$student_user_name .= $random_no;
		}

		$password = wp_generate_password( 8 );

		$userdata = array (
			'user_login'   => $student_user_name,
			'user_email'   => $txt_student_create_email,
			'first_name'   => $txt_student_create_first_name,
			'last_name'    => $txt_student_create_last_name,
			'display_name' => $txt_student_create_first_name . ' ' . $txt_student_create_last_name,
			'role'         => $select_student_create_role,
			'user_pass'    => $password
		);

		$saved_user_id = wp_insert_user( $userdata );

		if ( ! is_numeric( $saved_user_id ) || $saved_user_id == 0 ) {
			$return_message['message'] = 'User could not be registered. Please try again.';
			echo json_encode( $return_message );
			die();
		} else {

			$salt = wp_generate_password( 20 ); // 20 character "random" string
			$activationkey = sha1( $salt . $txt_student_create_email . uniqid( time(), true ) );

			$user_role_id = $user_roles_array[ $select_student_create_role ];

			$data_array = array (
				'user_id'           => $saved_user_id,
				'user_role_id'      => $user_role_id,
				'activation_key'    => $activationkey,
				'mobile_no'         => $txt_student_create_mobile,
				'gender'            => $select_student_create_gender,
				'profile_activated' => 'yes'
			);
			$data_format_array = array (
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
			);

			$result = $wpdb->insert( 'pdg_user_info', $data_array, $data_format_array );
			if ( $select_student_create_role == 'student' ) {
				$student_data_array = array (
					'user_id'      => $saved_user_id,
					'profile_slug' => $student_user_name,
				);
				$student_data_format_array = array (
					'%d',
					'%s',
				);
				$result = $wpdb->insert( 'pdg_student', $student_data_array, $student_data_format_array );
			} else if ( $select_student_create_role == 'guardian' ) {
				$student_data_array = array (
					'user_id'      => $saved_user_id,
					'profile_slug' => $student_user_name,
				);
				$student_data_format_array = array (
					'%d',
					'%s',
				);
				$result = $wpdb->insert( 'pdg_guardian', $student_data_array, $student_data_format_array );
			}
			$return_message['error'] = false;
			$return_message['message'] = 'User was registered successfully!';
			echo json_encode( $return_message );
			die();
		}
	}

	public static function social_login_save_info($data) {
		global $wpdb;
		$ip_address = pedagoge_get_visitor_ip();
		$data['registration_ip_address'] = $ip_address;
		$wpdb->insert( 'pdg_social_login', $data );
	}
}
