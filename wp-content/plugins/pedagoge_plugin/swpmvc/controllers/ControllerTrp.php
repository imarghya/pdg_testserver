<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerTrp extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge Teacher Referral Program';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		//$this->fn_register_common_variables();
	}
	
	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$activate_version = '0.2';
		
		unset($this->css_assets['waves']);
		unset($this->css_assets['ankitesh-validationEngine']);
		unset($this->css_assets['offcanvasmenueffects']);
		unset($this->css_assets['3d-bold-navigation']);
		unset($this->css_assets['ankitesh-search']);
		unset($this->css_assets['ankitesh-modern']);
		unset($this->css_assets['ankitesh-theme-green']);
		unset($this->css_assets['ankitesh-custom']);		
		unset($this->header_js['ankitesh-forms']);
		unset($this->footer_js['validationengine']);
		unset($this->footer_js['validationengine-en']);
		unset($this->footer_js['ankitesh-modern']);
		unset($this->footer_js['offcanvasmenueffects']);
		
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		$this->css_assets['jquery.bxslider'] = PEDAGOGE_ASSETS_URL.'/plugins/jquery.bxslider/jquery.bxslider.css?ver='.$this->fixed_version;

		$this->css_assets['assets_custom'] = PEDAGOGE_ASSETS_URL.'/css/custom.css?ver='.$this->fixed_version;
		$this->css_assets['trp'] = PEDAGOGE_ASSETS_URL.'/css/trp.css?ver='.$this->fixed_version;
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
		//$this->css_assets['custom_backend'] = PEDAGOGE_PROUI_URL."/css/custom_backend.css?ver=".$this->fixed_version;
		
	    $this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		$this->footer_js['jquery.bxslider'] = PEDAGOGE_ASSETS_URL.'/plugins/jquery.bxslider/jquery.bxslider.min.js?ver='.$this->fixed_version;
		$this->app_js['trp'] = PEDAGOGE_ASSETS_URL.'/js/trp.js?ver='.$activate_version;
	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}

	public function fn_get_content(){
		$master_model = new ModelTRP();
		$trp_data = $master_model->all();

		$this->app_data['trp_data'] = $trp_data;

		return $this->load_view('trp/index');
	}

	public static function fn_save_trp_ajax(){
		global $wpdb;

		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => null,
			'duplicate_teacher' => false
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}

		$teacher_institute_data = $_POST['teacher_institute_data'];
		$teacher_institute_name = sanitize_text_field($teacher_institute_data[0]);
		$teacher_institute_phone = sanitize_text_field($teacher_institute_data[1]);
		$teacher_institute_email = sanitize_text_field($teacher_institute_data[2]);
		$teacher_institute_subjects = sanitize_text_field($teacher_institute_data[3]);
		$teacher_institute_locality = sanitize_text_field($teacher_institute_data[4]);

		$user_name = sanitize_text_field($_POST['user_name']);
		$user_email = sanitize_text_field($_POST['user_email']);
		$user_contact_number = sanitize_text_field($_POST['user_contact_number']);

		$query_pdg_teachers = "select `mobile_no` from `view_pdg_teachers` where mobile_no = '$teacher_institute_phone'";
		$query_pdg_institutes = "select `mobile_no` from `view_pdg_institutes` where mobile_no = '$teacher_institute_phone'";
		$query_pdg_trp = "select `trp_teacher_institute_phone` from `pdg_trp` where trp_teacher_institute_phone = '$teacher_institute_phone'";

		$query_result_teacher = $wpdb->get_row($query_pdg_teachers);
		$query_result_institute = $wpdb->get_row($query_pdg_institutes);
		$query_result_trp = $wpdb->get_row($query_pdg_trp);

		if(empty($query_result_teacher) && empty($query_result_institute) && empty($query_result_trp)){
			$insert_array = array(
							'trp_id'=>'',
							'trp_teacher_institute_name'=>$teacher_institute_name,
							'trp_teacher_institute_phone'=>$teacher_institute_phone,
							'trp_teacher_institute_email'=>$teacher_institute_email,	
							'trp_teacher_institute_subjects_taught'=>$teacher_institute_subjects,
							'trp_teacher_institute_locality'=>$teacher_institute_locality,
							'trp_user_name'=>$user_name,
							'trp_user_phone_no'=>$user_contact_number,
							'trp_user_email'=>$user_email,
							'deleted'=>'no'
						);
			$wpdb->insert('pdg_trp', $insert_array,array('%d', '%s','%s','%s','%s','%s','%s','%s','%s','%s'));

			$template_vars['teacher_institute_name'] = $teacher_institute_name;
			$template_vars['teacher_institute_phone'] = $teacher_institute_phone;
			$template_vars['teacher_institute_email'] = $teacher_institute_email;
			$template_vars['teacher_institute_subjects'] = $teacher_institute_subjects;
			$template_vars['teacher_institute_locality'] = $teacher_institute_locality;
			$template_vars['user_name'] = $user_name;
			$template_vars['user_phone_no'] = $user_contact_number;
			$template_vars['user_email'] = $user_email;

			$trp_mail_content = PDGViewLoader::load_view('email_templates/trp',$template_vars);

			$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
			$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

			add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
			wp_mail('trp@pedagoge.com', 'Pedagoge TRP Mail', $trp_mail_content , $headers);
		}
		else{
			$return_message['duplicate_teacher']= true;
			$error_msg = '<div class="alert alert-warning"><h4>Error! '.$teacher_institute_name.' is already registered with us!</br></br>Please reload the page and try again.</h4></div>';
			$return_message['message'] = $error_msg;
		
			echo json_encode( $return_message );
			die();
		}
	}


	function fn_insert_multiple_trp_ajax(){
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'error_message' => '',
			'success_message' => '',
			'data' => $error_array,
			'duplicate_teacher' => false
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}

		$teacher_institute_data_array = $_POST['teacher_institute_data_array'];
		$user_name = sanitize_text_field($_POST['user_name']);
		$user_phone = sanitize_text_field($_POST['user_contact_number']);
		$user_email = sanitize_text_field($_POST['user_email']);
		$counter = sanitize_text_field($_POST['data_array_counter']);

		$error_msg = '';
		$index = 0;

		for($i = 0; $i < $counter; $i += 1){
			$teacher_institute_name = sanitize_text_field($teacher_institute_data_array[$index]);
			$teacher_institute_phone = sanitize_text_field($teacher_institute_data_array[$index+1]);
			$teacher_institute_email = sanitize_text_field($teacher_institute_data_array[$index+2]);
			$teacher_institute_subjects = sanitize_text_field($teacher_institute_data_array[$index+3]);
			$teacher_institute_locality = sanitize_text_field($teacher_institute_data_array[$index+4]);

			$query_pdg_teachers = "select `mobile_no` from `view_pdg_teachers` where mobile_no = '$teacher_institute_phone'";
			$query_pdg_institutes = "select `mobile_no` from `view_pdg_institutes` where mobile_no = '$teacher_institute_phone'";
			$query_pdg_trp = "select `mobile_no` from `pdg_trp` where trp_teacher_institute_phone = '$teacher_institute_phone'";

			$query_result_teacher = $wpdb->get_row($query_pdg_teachers);
			$query_result_institute = $wpdb->get_row($query_pdg_institutes);
			$query_result_trp = $wpdb->get_row($query_pdg_trp);

			if(empty($query_result_teacher) && empty($query_result_institute) && empty($query_result_trp)){
				$insert_array = array(
							'trp_id'=>'',
							'trp_teacher_institute_name'=>$teacher_institute_name,
							'trp_teacher_institute_phone'=>$teacher_institute_phone,
							'trp_teacher_institute_email'=>$teacher_institute_email,	
							'trp_teacher_institute_subjects_taught'=>$teacher_institute_subjects,
							'trp_teacher_institute_locality'=>$teacher_institute_locality,
							'trp_user_name'=>$user_name,
							'trp_user_phone_no'=>$user_phone,
							'trp_user_email'=>$user_email,
							'deleted'=>'no'
						);
				$wpdb->insert('pdg_trp', $insert_array,array('%d', '%s','%s','%s','%s','%s','%s','%s','%s','%s'));

				$template_vars['teacher_institute_name'] = $teacher_institute_name;
				$template_vars['teacher_institute_phone'] = $teacher_institute_phone;
				$template_vars['teacher_institute_email'] = $teacher_institute_email;
				$template_vars['teacher_institute_subjects'] = $teacher_institute_subjects;
				$template_vars['teacher_institute_locality'] = $teacher_institute_locality;
				$template_vars['user_name'] = $user_name;
				$template_vars['user_phone_no'] = $user_contact_number;
				$template_vars['user_email'] = $user_email;

				$trp_mail_content = PDGViewLoader::load_view('email_templates/trp',$template_vars);

				$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
				$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

				add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
				wp_mail('trp@pedagoge.com', 'Pedagoge TRP Mail', $trp_mail_content , $headers);

				$success_msg .= '<div class="alert alert-success"><h4>'.$teacher_institute_name.' is succesfully registered!</h4></div>';
				
				$return_message['success_message'] = $success_msg;
			}
			else{
				$return_message['duplicate_teacher']= true;
				$error_msg .= '<div class="alert alert-warning"><h4>Error! '.$teacher_institute_name.' is already registered!</br></br>Please reload the page and try again</h4></div>';
				
				$return_message['data'] = $error_msg;	
			}
			$index += 5;	
		}
		echo json_encode( $return_message );
		die();
	}
}
