<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerPrivateinstitute extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge Institute Profile:';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}
	
	public function fn_load_scripts() {
		unset($this->css_assets['waves']);
		unset($this->css_assets['offcanvasmenueffects']);
		unset($this->css_assets['3d-bold-navigation']);
		unset($this->css_assets['ankitesh-search']);
		unset($this->css_assets['ankitesh-modern']);
		unset($this->css_assets['ankitesh-theme-green']);
		unset($this->css_assets['ankitesh-custom']);		
		
		unset($this->header_js['ankitesh-forms']);
		unset($this->header_js['modernizr']);
		unset($this->header_js['snap-svg']);
		unset($this->footer_js['jquery-slimscroll']);
		unset($this->footer_js['waves']);
		unset($this->footer_js['ankitesh-modern']);
		unset($this->footer_js['offcanvasmenueffects']);
		
		$profile_version = '1.2';
		/**
		 * CSS Loading
		 */
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;		
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;		
		$this->css_assets['bsdatetimepicker'] = $this->registered_css['bsdatetimepicker'].'?ver='.$this->fixed_version;
		
		$this->app_css['institute_registration'] = PEDAGOGE_ASSETS_URL."/css/institute_registration.css?ver=".$profile_version;
		
		/**
		 * JS Loading
		 */				
		$this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['moment'] = $this->registered_js['moment'].'?ver='.$this->fixed_version;
		$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
		$this->footer_js['bootbox'] = $this->registered_js['bootbox']."?ver=".$this->fixed_version;
		$this->footer_js['bsdatetimepicker'] = $this->registered_js['bsdatetimepicker'].'?ver='.$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
		$this->footer_js['blockui'] = BOWER_ROOT_URL."/blockUI/jquery.blockUI.js?ver=".$this->fixed_version;		
		
		/**
		 * Cropper
		 */		
		$this->css_assets['cropper'] =  $this->registered_css['cropper'].'?ver='.$this->fixed_version;
		$this->footer_js['cropper'] = $this->registered_js['cropper'].'?ver='.$this->fixed_version;
		$this->app_js['document_uploader'] = PEDAGOGE_ASSETS_URL."/js/teacher_institute_documents_manager.js?ver=".$profile_version;
		$this->app_js['pedagoge_profile'] = PEDAGOGE_ASSETS_URL."/js/institute_private.js?ver=".$profile_version;
	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}
	
	public function fn_get_content() {	

		$current_user_id = $this->app_data['pdg_current_user']->ID;
		
		if(is_user_logged_in()) {
			
			$institute_data = $this->app_data['institute_data'];

			if(empty($institute_data)) {
				return '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-error">
								<div class="panel-heading">Error! Profile data not available!</div>
								<div class="panel-body">
									<br />
									<h2>Error! Institute Data is not available.</h2>
								</div>
							</div>
						</div>
					</div>
				</section>
			';
			} else {
				return $this->load_view('profile/pvtinstitute/index');
			}
		} else {
			return '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-warning">
								<div class="panel-heading">User not logged in!</div>
								<div class="panel-body">
									<br />
									<h2 class="alert alert-warning">Error! User ID is not set. You need to log in!</h2>
								</div>
							</div>
						</div>
					</div>
				</section>
			';
		}
	}
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		global $wpdb;
		
		$current_user_id = '';
		$view_institute_data = '';
		$view_institute_model = new ModelMaster('view_pdg_institutes');
		$view_institute_model->set_primary_key('institute_id');
		
		if(isset($_GET['role']) && isset($_GET['id'])) {
			//only admin can access this via Edit URL
			$institute_id = $_GET['id'];
			if(is_numeric($institute_id)) {
				$view_institute_data = $view_institute_model->where(array('institute_id'=>$institute_id))->find();
			}
			
			if(!empty($view_institute_data)) {
				foreach($view_institute_data as $institute_data) {
					$current_user_id = $institute_data->user_id;
				}
				$this->app_data['pdg_current_user'] = get_user_by('id', $current_user_id);
			}
			
		} else {
			$current_user_id = $this->app_data['pdg_current_user']->ID;			
			$view_institute_data = $view_institute_model->where(array('user_id'=>$current_user_id))->find();
		}		
		
		$this->app_data['institute_data'] = $view_institute_data;
		
		if(!empty($view_institute_data)) {
			
			$this->app_data['preferred_mode_of_contact'] = PDGManageCache::fn_load_cache('pdg_mode_of_contact');
			$this->app_data['city_data'] = PDGManageCache::fn_load_cache('pdg_city');		
			$this->app_data['subject_names_data'] = PDGManageCache::fn_load_cache('pdg_subject_name');
			$this->app_data['class_timing_data'] = PDGManageCache::fn_load_cache('pdg_class_timing');
			$this->app_data['fees_type_data'] = PDGManageCache::fn_load_cache('pdg_fees_type');
			$this->app_data['schools_data'] = PDGManageCache::fn_load_cache('pdg_schools');
			$this->app_data['class_capacity_data'] = PDGManageCache::fn_load_cache('pdg_class_capacity');
			$this->app_data['teaching_xp_data'] = PDGManageCache::fn_load_cache('pdg_teaching_xp');
			$this->app_data['faculty_size_data'] = PDGManageCache::fn_load_cache('pdg_size_of_faculty');
			$this->app_data['found_localities'] = $wpdb->get_results("select * from view_pdg_locality");
			
			$user_info_model = new ModelUserInfo();
			$pdg_user_data = $user_info_model->where(array('user_id'=>$current_user_id))->find();
			$this->app_data['pdg_institute_user_info'] = $pdg_user_data;
			
			// Institute's contact modes
			$str_institute_contact_mode_sql = "
				select 
					pdg_user_contact_mode.*,
					pdg_mode_of_contact.contact_mode
				from pdg_user_contact_mode
				left join pdg_mode_of_contact on pdg_mode_of_contact.contact_mode_id = pdg_user_contact_mode.contact_mode_id
				where pdg_user_contact_mode.user_id = $current_user_id
			";
			$institute_contact_modes = $wpdb->get_results($str_institute_contact_mode_sql);
			if(!empty($institute_contact_modes)) {
				$this->app_data['institute_contact_modes'] = $institute_contact_modes;
			}
			
			$achievements_data = $wpdb->get_results("select * from pdg_teaching_achievements where user_id = $current_user_id");
			if(!empty($achievements_data)) {
				$this->app_data['achievements_data'] = $achievements_data;
			}
		}
	}

	public static function fn_save_institute_intro_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_data = json_decode( stripslashes( $_POST['institute_intro_data'] ), TRUE);
		
		$institute_user_id = $institute_data['hidden_dynamic_user_var'];
		$institute_user_info_id = $institute_data['hidden_dynamic_user_info_var'];
		$institute_id = $institute_data['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $institute_data['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;		
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {		
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		/**
		 * Create Profile Slugs
		 */
		 
		$raw_slug = $users_email.$institute_user_id;
		$refined_slug = md5($raw_slug);
		
		
		$txt_institution_name = sanitize_text_field($institute_data['txt_institution_name']);		
		$txt_contact_person = sanitize_text_field($institute_data['txt_contact_person']);		
		$txt_mobile_no = sanitize_text_field($institute_data['txt_mobile_no']);
		$txt_alternate_contact_no = sanitize_text_field($institute_data['txt_alternate_contact_no']);
		$select_preffered_mode_of_contact = $institute_data['select_preffered_mode_of_contact'];
		$txt_address_of_correspondance = sanitize_text_field($institute_data['txt_address_of_correspondance']);
		$select_institute_city = sanitize_text_field($institute_data['select_institute_city']);
		$select_size_of_faculty = sanitize_text_field($institute_data['select_size_of_faculty']);
		
		$from_hours_of_operation  = sanitize_text_field($institute_data['from_hours_of_operation']);
		$to_hours_of_operation = sanitize_text_field($institute_data['to_hours_of_operation']);
		$select_average_teaching_experience  = sanitize_text_field($institute_data['select_average_teaching_experience']);		
		$txt_heading_of_profile  = sanitize_text_field($institute_data['txt_heading_of_profile']);
		$txt_about_the_coaching = sanitize_text_field($institute_data['txt_about_the_coaching']);
		$txt_registration_reference = sanitize_text_field($teacher_data['txt_registration_reference']);
		
		/**
		 * Check to see if the provided phone no is duplicate or not.
		 */
		if(empty($txt_mobile_no)) {			
			$return_message['message'] = 'Error! Mobile no is required! Please input phone no.';
			echo json_encode($return_message );
			die();
		} else {
			$str_sql = "select mobile_no from pdg_user_info where mobile_no like '$txt_mobile_no' && user_id != $institute_user_id";
			
			$is_duplicate_phone = $wpdb->get_results($str_sql);
			
			if(!empty($is_duplicate_phone)) {
				$return_message['error_type'] = 'duplicate_phone';
				$return_message['message'] = 'Error! Phone no is duplicate! Please try again.';
	
				echo json_encode($return_message );
				die();
			}
		}
		
		/**
		 * Update user information
		 */
		$user_data = array(
			'mobile_no' =>$txt_mobile_no,
			'alternative_contact_no' =>$txt_alternate_contact_no,
			'registration_reference' => $txt_registration_reference
					
		);
		$user_data_format = array('%s', '%s','%s',);
		$where_data = array(
			'personal_info_id' => $institute_user_info_id
		);		
		$wpdb->update('pdg_user_info', $user_data, $where_data, $user_data_format, array('%d'));
		
		/**
		 * Update Institute Information
		 */
		$institute_data = array(						
			'institute_name' => $txt_institution_name,
			'contact_person_name' => $txt_contact_person,
			'primary_contact_no' => $txt_mobile_no,
			'alternate_contact_no' => $txt_alternate_contact_no,			
			'correspondance_address' => $txt_address_of_correspondance,
			'city_id' => $select_institute_city,			
			'profile_heading' => $txt_heading_of_profile,
			'size_of_faculty' => $select_size_of_faculty,			
			'operation_hours_from' => $from_hours_of_operation,
			'operation_hours_to' => $to_hours_of_operation,
			'about_coaching' => $txt_about_the_coaching,			
			'avg_teaching_xp_id' => $select_average_teaching_experience,
			'profile_slug'=>$refined_slug
		);
		
		$institute_data_format = array(
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s'
		);
		
		$institute_where = array(
			'institute_id' => $institute_id
		);
		
		$update_return = $wpdb->update('pdg_institutes', $institute_data, $institute_where, $institute_data_format,array('%d'));
		
		$wpdb->get_results("delete from pdg_user_contact_mode where user_id = $institute_user_id");
		foreach($select_preffered_mode_of_contact as $key=>$value) {
			if(is_numeric($value)) {
				$insert_array = array(
					'user_id'=>$institute_user_id,
					'contact_mode_id'=>$value
				);
				$wpdb->insert('pdg_user_contact_mode', $insert_array,array('%d', '%d'));
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Profile intro was updated Successfully!';
		echo json_encode($return_message );
		die();
		
	}

	public static function fn_load_subject_type_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$subject_name_id = $_POST['subject_name_id'];
		if(!is_numeric($subject_name_id) || $subject_name_id <= 0) {
			$return_message['error_type'] = 'empty_data';
			$return_message['message'] = 'Error! Please select Subject name and try again!';
			
			echo json_encode($return_message );
			die();
		}
		
		$str_sql = "SELECT * FROM view_course_category where subject_name_id = $subject_name_id";
		
		$subject_result = $wpdb->get_results($str_sql);
		
		$subject_type_array = array();
				
		$str_subject_type = '';
				
		foreach($subject_result as $result) {
			$subject_type_id = $result->subject_type_id;
			$subject_type_name = $result->subject_type;
			
			if(!empty($subject_type_name)) {
				if(!in_array($subject_type_id, $subject_type_array)) {
					$subject_type_array[] = $subject_type_id;
					$str_subject_type.= '<option value="'.$subject_type_id.'">'.$subject_type_name.'</option>';
				}
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Subject Categories loaded successfully!';
		$return_message['data'] = array(
			'course_type'=>$str_subject_type			
		);
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_load_subject_age_board_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$subject_name_id = $_POST['subject_name_id'];
		if(!is_numeric($subject_name_id) || $subject_name_id <= 0) {
			$return_message['error_type'] = 'empty_data';
			$return_message['message'] = 'Error! Please select Subject name and try again!';
			
			echo json_encode($return_message );
			die();
		}
		
		$subject_type_posted_data = $_POST['subject_type_id'];
		if(!array($subject_type_posted_data)) {
			$return_message['error_type'] = 'empty_data';
			$return_message['message'] = 'Error! Please select Subject Category and try again!';
			echo json_encode($return_message );
			die();
		}
		
		$is_empty_age = FALSE;
		$is_empty_board = FALSE;
		
		$course_age_category_array = array();
		$academic_board_array = array();
		
		$str_course_age = '';
		$str_academic_board = '';
		
		foreach($subject_type_posted_data as $subject_type_id) {
			if(!is_numeric($subject_type_id)) {
				$is_empty_age = TRUE;
				$is_empty_board = TRUE;
				break;
			}
			$str_sql = "SELECT * FROM view_course_category where subject_name_id = $subject_name_id and subject_type_id =$subject_type_id";
			$found_result = $wpdb->get_results($str_sql);
			foreach($found_result as $result) {
				$course_age_id = $result->course_age_category_id;
				$course_age_name = $result->course_age;
				$academic_board_id = $result->academic_board_id;
				$academic_board_name = $result->academic_board;
				
				if(empty($course_age_id)) {
					$is_empty_age = TRUE;
				} else {
					if(!empty($course_age_name)) {
						if(!in_array($course_age_id, $course_age_category_array)) {
							$course_age_category_array[] = $course_age_id;
							$str_course_age.= '<option value="'.$course_age_id.'">'.$course_age_name.'</option>';
						}	
					}
				}
				
				if(empty($academic_board_id)) {
					$is_empty_board = TRUE;
				} else {
					if(!empty($academic_board_name)) {
						if(!in_array($academic_board_id, $academic_board_array)) {
							$academic_board_array[] = $academic_board_id;
							$str_academic_board.= '<option value="'.$academic_board_id.'">'.$academic_board_name.'</option>';
						}
					}
				}
			}
		}
		if($is_empty_age) {
			$str_course_age = '';
		}
		if($is_empty_board) {
			$str_academic_board = '';
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Subject dependecies loaded successfully!';
		$return_message['data'] = array(
			
			'course_age'=>$str_course_age,
			'academic_board'=>$str_academic_board,
		);
		echo json_encode($return_message );
		die();
	}

	public static function fn_load_course_table_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$subject_name_id = $_POST['select_subject_name'];
		if(!is_numeric($subject_name_id) || $subject_name_id <= 0) {
			$return_message['error_type'] = 'empty_data';
			$return_message['message'] = 'Error! Please select Subject name and try again!';
			
			echo json_encode($return_message );
			die();
		}
		
		$subject_type_posted_data = $_POST['select_subject_category'];
		if(!array($subject_type_posted_data)) {
			$return_message['error_type'] = 'empty_data';
			$return_message['message'] = 'Error! Please select Subject Category and try again!';
			echo json_encode($return_message );
			die();
		}
		$str_subject_type_arr = implode(',', $subject_type_posted_data);
		
		$select_subject_age_category = $_POST['select_subject_age_category'];
		$select_subject_academic_board = $_POST['select_subject_academic_board'];
		
		$sql_age_category = '';
		if(!is_array($select_subject_age_category)) {
			$select_subject_age_category = '';
		} else {
			$select_subject_age_category = implode(',', $select_subject_age_category);
			$sql_age_category = " and course_age_category_id in ($select_subject_age_category)";
		}
		
		$sql_academic_board = '';
		if(!is_array($select_subject_academic_board)) {
			$select_subject_academic_board = '';
		} else {
			$select_subject_academic_board = implode(',', $select_subject_academic_board);
			$sql_academic_board = " and academic_board_id in ($select_subject_academic_board)";
		}
		
		$str_sql = "SELECT * FROM view_course_category 
					where subject_name_id = $subject_name_id and 
					subject_type_id in($str_subject_type_arr) $sql_age_category $sql_academic_board";
		$course_result = $wpdb->get_results($str_sql);
		$course_array = array();
		foreach($course_result as $course) {
			$course_id = $course->course_category_id;
			$subject_name = $course->subject_name;
			$subject_category = $course->subject_type;
			$acadamic_board = $course->academic_board;
			$course_age = $course->course_age;
			if(!array_key_exists($course_id, $course_array)) {
				$course_array[$course_id] = array(
					'id'=>$course_id,
					'subject_name'=>$subject_name,
					'subject_category'=>$subject_category,
					'acadamic_board'=>$acadamic_board,
					'course_age'=>$course_age
				);
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Course data loaded successfully!';
		$return_message['data'] = $course_array;
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_save_institute_batch_ajax() {
		global $wpdb;
			
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$hidden_batch_id = $_POST['hidden_batch_id'];
		$courses_id_array = $_POST['courses_id_array'];
		if(empty($courses_id_array) || !array($courses_id_array)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please select a subject.';			
			echo json_encode($return_message );
			die();
		}
		
		$select_course_days = $_POST['select_course_days'];
		if(empty($select_course_days) || !array($select_course_days)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please select which days you teach.';			
			echo json_encode($return_message );
			die();
		} else {
			$select_course_days = implode(',', $select_course_days);
		}
		$select_no_of_days = $_POST['select_no_of_days'];
		if(!is_numeric($select_no_of_days)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please select No of days you teach.';			
			echo json_encode($return_message );
			die();
		}
		$select_no_of_hours = $_POST['select_no_of_hours'];
		if(!is_numeric($select_no_of_days)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please select No of hours you teach per class.';			
			echo json_encode($return_message );
			die();
		}
		$select_class_timing_slot = $_POST['select_class_timing_slot'];
		if(empty($select_class_timing_slot) || !array($select_class_timing_slot)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please select No of hours you teach per class.';			
			echo json_encode($return_message );
			die();
		}
		$select_class_type = $_POST['select_class_type'];
		if($select_class_type=='both') {
			$select_class_type = 'multiple';
		}
		$select_max_students_per_class = $_POST['select_max_students_per_class'];
		if(!is_numeric($select_max_students_per_class)) {
			$select_max_students_per_class = '';
		}
		
		$txt_length_of_course = $_POST['txt_length_of_course'];
		$select_length_of_course = $_POST['select_length_of_course'];
		if(empty($select_length_of_course)) {
			$select_length_of_course = 'months';
		}
		$select_school_students = $_POST['select_school_students'];
		
		$txt_about_the_course = sanitize_text_field($_POST['txt_about_the_course']);
		$fees_collected_array = $_POST['fees_collected_array'];
		if(empty($fees_collected_array) || !array($fees_collected_array)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please input fees details of your class.';			
			echo json_encode($return_message );
			die();
		}
		$teaching_locations_array = $_POST['teaching_locations_array'];
		if(empty($teaching_locations_array) || !array($teaching_locations_array)) {
			$return_message['error_type'] = 'error';
			$return_message['message'] = 'Error! Please input teaching locations.';			
			echo json_encode($return_message );
			die();
		}		
		
		$own_location_fees_array = $_POST['own_location_fees_array'];
		$student_location_fees_array = $_POST['student_location_fees_array'];
		$institute_location_fees_array = $_POST['institute_location_fees_array'];
		
		if(!is_numeric($hidden_batch_id) || $hidden_batch_id <= 0) {
			//create batch for institute
			$insert_array = array(
				'tutor_institute_user_id' => $institute_user_id,
				'course_days' => $select_course_days,
				'no_of_days' => $select_no_of_days,
				'days_per_week' => $select_no_of_days,
				'class_duration' => $select_no_of_hours,
				'class_type' => $select_class_type,
				'class_capacity_id' => $select_max_students_per_class,
				'course_length' => $txt_length_of_course,
				'course_length_type' => $select_length_of_course,
				'about_course' => $txt_about_the_course,
			);
			
			$data_type = array(
				'%d',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s',
				'%d',
				'%f',
				'%s',
				'%s'
			);
			$wpdb->insert('pdg_tuition_batch', $insert_array, $data_type);
			$hidden_batch_id = $wpdb->insert_id;
			if(!is_numeric($hidden_batch_id) || $hidden_batch_id<=0) {
				$return_message['error_type'] = 'batch_save_error';
				$return_message['message'] = 'Error! Batch details could not be saved. Please try again';			
				echo json_encode($return_message );
				die();
			}

			/**
			 * 1. pdg_batch_subject
			 * 2. pdg_batch_school
			 * 3. pdg_batch_fees
			 * 4. pdg_batch_class_timing
			 * 5. pdg_tutor_location
			 */
			 
			//pdg_batch_subject
			foreach($courses_id_array as $courses_id) {					
				$course_category_insert_array = array(
					'tuition_batch_id'=>$hidden_batch_id,
					'course_category_id'=>$courses_id
				);
				$course_category_format_array = array(
					'%d',
					'%d'
				);
				$wpdb->insert('pdg_batch_subject', $course_category_insert_array, $course_category_format_array);
			}
			
			//pdg_batch_school
			foreach($select_school_students as $key=>$value) {
				if(is_numeric($value)) {
					$school_insert_array = array(
						'pdg_batch_id'=>$hidden_batch_id,
						'pdg_school_id'=>$value
					);
					$wpdb->insert('pdg_batch_school', $school_insert_array, array('%d', '%d'));
				}
			}
			//pdg_batch_fees
			foreach($fees_collected_array as $index_val=>$fees_data) {
				$fees_amount = $fees_data['amount'];
				$fees_type_id = $fees_data['fees_type_id'];
				if(is_numeric($fees_amount) && is_numeric($fees_amount)) {
					$fees_insert_array = array(
						'tuition_batch_id' => $hidden_batch_id,
						'fees_type_id' => $fees_type_id,
						'fees_amount' => $fees_amount
					);
					$wpdb->insert('pdg_batch_fees', $fees_insert_array, array('%d', '%d', '%f'));
				}
			}
			//pdg_batch_class_timing
			foreach($select_class_timing_slot as $class_timing) {
				if(is_numeric($class_timing)) {
					$class_timing_insert_array = array(
						'tuition_batch_id' => $hidden_batch_id,
						'class_timing_id' => $class_timing
					);
					$wpdb->insert('pdg_batch_class_timing', $class_timing_insert_array, array('%d','%d'));
				}
			}
			//pdg_tutor_location
			foreach($teaching_locations_array as $key=>$teaching_location) {
				$location_type = sanitize_text_field($teaching_location['type']);
				$location_localities= $teaching_location['data'];
				
				foreach($location_localities as $key=>$value) {
					$location_insert_array = array(
						'tuition_location_type' => $location_type,
						'tuition_batch_id' => $hidden_batch_id,
						'locality_id' => $value,
					);
					$location_insert_format_array = array('%s', '%d', '%d');
					switch($location_type) {
						case 'own':
							if(is_array($own_location_fees_array)) {
								foreach($own_location_fees_array as $location_fees_array){
									if($location_fees_array['location'] == $value) {
										$location_insert_array['locality_fees'] = $location_fees_array['amount'];
										$location_insert_format_array[] = '%f';
									}
								}
							}
							break;
						case 'student':
							if(is_array($student_location_fees_array)) {
								foreach($student_location_fees_array as $location_fees_array){
									if($location_fees_array['location'] == $value) {
										$location_insert_array['locality_fees'] = $location_fees_array['amount'];
										$location_insert_format_array[] = '%f';
									}
								}
							}
							break;
						case 'institute':
							if(is_array($institute_location_fees_array)) {
								foreach($institute_location_fees_array as $location_fees_array){
									if($location_fees_array['location'] == $value) {
										$location_insert_array['locality_fees'] = $location_fees_array['amount'];
										$location_insert_format_array[] = '%f';
									}
								}
							}
							break;
					}
					$wpdb->insert('pdg_tutor_location', $location_insert_array, $location_insert_format_array);	
				}				
			}			
		} else {
			$update_array = array(
				'tutor_institute_user_id' => $institute_user_id,
				'course_days' => $select_course_days,
				'no_of_days' => $select_no_of_days,
				'days_per_week' => $select_no_of_days,
				'class_duration' => $select_no_of_hours,
				'class_type' => $select_class_type,
				'class_capacity_id' => $select_max_students_per_class,
				'course_length' => $txt_length_of_course,
				'course_length_type' => $select_length_of_course,
				'about_course' => $txt_about_the_course,
			);
			
			$data_type = array(
				'%d',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s',
				'%d',
				'%f',
				'%s',
				'%s'
			);
			
			$wpdb->update('pdg_tuition_batch', $update_array, array('tuition_batch_id'=>$hidden_batch_id),$data_type,array('%d'));
			
			/**
			 * 1. pdg_batch_subject
			 * 2. pdg_batch_school
			 * 3. pdg_batch_fees
			 * 4. pdg_batch_class_timing
			 * 5. pdg_tutor_location
			 */
			if(is_numeric($hidden_batch_id) && $hidden_batch_id>0) {
				//pdg_batch_subject
				$wpdb->get_results("delete from pdg_batch_subject where tuition_batch_id = $hidden_batch_id");				
				foreach($courses_id_array as $courses_id) {					
					$course_category_insert_array = array(
						'tuition_batch_id'=>$hidden_batch_id,
						'course_category_id'=>$courses_id
					);
					$course_category_format_array = array(
						'%d',
						'%d'
					);
					$wpdb->insert('pdg_batch_subject', $course_category_insert_array, $course_category_format_array);
				}
				
				//pdg_batch_school
				$wpdb->get_results("delete from pdg_batch_school where pdg_batch_id = $hidden_batch_id");
				foreach($select_school_students as $key=>$value) {
					if(is_numeric($value)) {
						$school_insert_array = array(
							'pdg_batch_id'=>$hidden_batch_id,
							'pdg_school_id'=>$value
						);
						$wpdb->insert('pdg_batch_school', $school_insert_array, array('%d', '%d'));
					}
				}
				
				//pdg_batch_fees
				$wpdb->get_results("delete from pdg_batch_fees where tuition_batch_id = $hidden_batch_id");
				foreach($fees_collected_array as $index_val=>$fees_data) {
					$fees_amount = $fees_data['amount'];
					$fees_type_id = $fees_data['fees_type_id'];
					if(is_numeric($fees_amount) && is_numeric($fees_amount)) {
						$fees_insert_array = array(
							'tuition_batch_id' => $hidden_batch_id,
							'fees_type_id' => $fees_type_id,
							'fees_amount' => $fees_amount
						);
						$wpdb->insert('pdg_batch_fees', $fees_insert_array, array('%d', '%d', '%f'));
					}
				}
				//pdg_batch_class_timing
				$wpdb->get_results("delete from pdg_batch_class_timing where tuition_batch_id = $hidden_batch_id");
				foreach($select_class_timing_slot as $class_timing) {
					if(is_numeric($class_timing)) {
						$class_timing_insert_array = array(
							'tuition_batch_id' => $hidden_batch_id,
							'class_timing_id' => $class_timing
						);
						$wpdb->insert('pdg_batch_class_timing', $class_timing_insert_array, array('%d','%d'));
					}
				}
				//pdg_tutor_location
				$wpdb->get_results("delete from pdg_tutor_location where tuition_batch_id = $hidden_batch_id");
				foreach($teaching_locations_array as $key=>$teaching_location) {
					$location_type = sanitize_text_field($teaching_location['type']);
					$location_localities= $teaching_location['data'];
					foreach($location_localities as $key=>$value) {
						$location_insert_array = array(
							'tuition_location_type' => $location_type,
							'tuition_batch_id' => $hidden_batch_id,
							'locality_id' => $value,
						);
						$location_insert_format_array = array('%s', '%d', '%d');
						switch($location_type) {
							case 'own':
								if(is_array($own_location_fees_array)) {
									foreach($own_location_fees_array as $location_fees_array){
										if($location_fees_array['location'] == $value) {
											$location_insert_array['locality_fees'] = $location_fees_array['amount'];
											$location_insert_format_array[] = '%f';
										}
									}
								}
								break;
							case 'student':
								if(is_array($student_location_fees_array)) {
									foreach($student_location_fees_array as $location_fees_array){
										if($location_fees_array['location'] == $value) {
											$location_insert_array['locality_fees'] = $location_fees_array['amount'];
											$location_insert_format_array[] = '%f';
										}
									}
								}
								break;
							case 'institute':
								if(is_array($institute_location_fees_array)) {
									foreach($institute_location_fees_array as $location_fees_array){
										if($location_fees_array['location'] == $value) {
											$location_insert_array['locality_fees'] = $location_fees_array['amount'];
											$location_insert_format_array[] = '%f';
										}
									}
								}
								break;
						}
						$wpdb->insert('pdg_tutor_location', $location_insert_array, $location_insert_format_array);	
					}				
				}	
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Success! Class Data saved...';
		
		echo json_encode($return_message );
		die();
	}

	public static function fn_load_batches_list_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$return_message['error'] = false;
		$return_message['data'] = self::fn_return_batches_list($institute_user_id);
		$return_message['message'] = 'Batches List loaded successfully!';
		
		echo json_encode($return_message );
		die();
	}

	public static function fn_return_batches_list($user_id) {
		global $wpdb;
		
		$str_sql = "
			select 
				view_pdg_batch_subject.tuition_batch_id,
				group_concat(distinct(view_pdg_batch_subject.subject_name)) as subject_name,
				group_concat(distinct(view_pdg_batch_subject.subject_type)) as subject_type,
				group_concat(distinct(view_pdg_batch_subject.academic_board)) as academic_board,
				group_concat(distinct(view_pdg_batch_subject.course_age)) as course_age,
				group_concat(distinct(pdg_locality.locality)) as locality	
			from view_pdg_batch_subject  
			left join pdg_tutor_location on pdg_tutor_location.tuition_batch_id = view_pdg_batch_subject.tuition_batch_id
			left join pdg_locality on pdg_locality.locality_id = pdg_tutor_location.locality_id
			where view_pdg_batch_subject.tutor_institute_user_id = $user_id
			group by (view_pdg_batch_subject.tuition_batch_id)
		";		
		$batches_data = $wpdb->get_results($str_sql);		

		$str_return = '';
		foreach($batches_data as $batch_data) {			
			$str_return.='
				<tr>
					<td>'.$batch_data->tuition_batch_id.'</td>
					<td>'.$batch_data->subject_name.'</td>
					<td>'.$batch_data->subject_type.'</td>
					<td>'.$batch_data->academic_board.'</td>
					<td>'.$batch_data->course_age.'</td>
					<td>'.$batch_data->locality.'</td>
					<td>
						<button class="btn btn-danger col-md-12 cmd_delete_batch" data-batchid="'.$batch_data->tuition_batch_id.'"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
					</td>
					<td>
						<button class="btn btn-info col-md-12 cmd_copy_batch" data-batchid="'.$batch_data->tuition_batch_id.'"><i class="fa fa-clone" aria-hidden="true"></i> Copy</button>
					</td>
					<td>
						<button class="btn btn-warning col-md-12 cmd_edit_batch" data-batchid="'.$batch_data->tuition_batch_id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
					</td>
				</tr>
			';			
		}
		return $str_return;
	}
	
	public static function fn_load_batch_details_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		$batch_id = $_POST['batch_id'];
		if(!is_numeric($batch_id) || $batch_id<=0) {
			$return_message['error_type'] = 'incorrect_data';
			$return_message['message'] = 'Error! Batch ID is not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		$batch_data = $wpdb->get_results("SELECT * FROM pdg_tuition_batch where tuition_batch_id=$batch_id", ARRAY_A);
		/**
		 * 1. pdg_batch_subject
		 * 2. pdg_batch_school
		 * 3. pdg_batch_fees
		 * 4. pdg_batch_class_timing
		 * 5. pdg_tutor_location
		 */
		$course_data = $wpdb->get_results("select * from view_pdg_batch_subject where tuition_batch_id = $batch_id");
		$str_course_table = '';
		//pedagoge_applog($course_data);
		foreach($course_data as $course_details) {
			$str_course_table .= '
			<tr data-courseid="'.$course_details->course_category_id.'">
				<td class="td_course_id hidden_item">'.$course_details->course_category_id.'</td>
				<td>'.$course_details->subject_name.'</td>
				<td>'.$course_details->subject_type.'</td>
				<td>'.$course_details->course_age.'</td>
				<td>'.$course_details->academic_board.'</td>
				<td><input type="checkbox" class="chk_selected_courses" checked value="'.$course_details->course_category_id.'"></td>
			</tr>';
		}
		
		$batch_school_data = $wpdb->get_results("select pdg_school_id from pdg_batch_school where pdg_batch_id = $batch_id");
		$school_array = array();
		foreach($batch_school_data as $school) {
			$school_id = $school->pdg_school_id;
			if(!in_array($school_id, $school_array)) {
				$school_array[] = $school_id;
			}
		}
		
		$class_timing_data = $wpdb->get_results("select class_timing_id from pdg_batch_class_timing where tuition_batch_id = $batch_id");
		$class_timing_array = array();
		foreach($class_timing_data as $class_timing) {
			$class_timing_id = $class_timing->class_timing_id;
			if(!in_array($class_timing_id, $class_timing_array)) {
				$class_timing_array[] = $class_timing_id;
			}
		}
		
		$own_location_array = array();
		$student_location_array = array();
		$institute_location_array = array();
		
		$own_location_fees_array = array();
		$student_location_fees_array = array();
		$institute_location_fees_array = array();
		
		$locations_db = $wpdb->get_results("select tuition_location_type, locality_id, locality_fees from pdg_tutor_location where tuition_batch_id = $batch_id");
		foreach($locations_db as $locations) {
			$location_type = $locations->tuition_location_type;
			$location_id = $locations->locality_id;
			$locality_fees = $locations->locality_fees;
			if(!is_numeric($locality_fees)) {
				$locality_fees = 0;
			}
			switch($location_type) {
				case 'own':
					if(!in_array($location_id, $own_location_array)) {
						$own_location_array[] = $location_id;
					}
					$own_location_fees_array[$location_id] = $locality_fees;
					break;
				case 'student':
					if(!in_array($location_id, $student_location_array)) {
						$student_location_array[] = $location_id;
					}
					$student_location_fees_array[$location_id] = $locality_fees;
					break;
				case 'institute':
					if(!in_array($location_id, $institute_location_array)) {
						$institute_location_array[] = $location_id;
					}
					$institute_location_fees_array[$location_id] = $locality_fees;
					break;
			}
		}
		
		$fees_db = $wpdb->get_results("select * from view_pdg_batch_fees where tuition_batch_id = $batch_id");
		$fees_html = '';
		$fees_type_array = array();
		
		foreach($fees_db as $fees_info) {
			$fees_type_id = $fees_info->fees_type_id;
			$fees_amount = $fees_info->fees_amount;
			$fees_type_name = $fees_info->fees_type;
			$fees_html .= '
				<div class="div_dynamic_fees_collected_field">
					<label>'.$fees_type_name.' (In Rs.)</label>
					<input type="text" 
						class="form-control positive txt_fees_type_amount" 
						data-feestypeid="'.$fees_type_id.'" 
						placeholder="'.$fees_type_name.'" 
						maxlength="7" value="'.$fees_amount.'">
					<br />
				</div>
			';
			$fees_type_array[] = $fees_type_id;
		}
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Batch information loaded successfully!';
		$return_message['data'] = array(
			'batch_data' => $batch_data,
			'school_array' => $school_array,
			'str_course_table' => $str_course_table,
			'class_timing_array' => $class_timing_array,
			'own_location_array' => $own_location_array,
			'student_location_array' => $student_location_array,
			'institute_location_array' => $institute_location_array,
			'fees_html' => $fees_html,
			'fees_type_array' => $fees_type_array,
			'own_location_fees_array' => $own_location_fees_array, 
			'student_location_fees_array' => $student_location_fees_array,
			'institute_location_fees_array' => $institute_location_fees_array,
		);
				
		echo json_encode($return_message );
		die();
		
	}

	public static function fn_delete_batch_ajax() {
		global $wpdb;
			
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		$hidden_batch_id = $_POST['batch_id'];
		if(!is_numeric($hidden_batch_id) || $hidden_batch_id<=0) {
			$return_message['error_type'] = 'incomplete_data';
			$return_message['message'] = 'Error! Batch information not given. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		/**
		 * 1. pdg_batch_subject
		 * 2. pdg_batch_school
		 * 3. pdg_batch_fees
		 * 4. pdg_batch_class_timing
		 * 5. pdg_tutor_location
		 */
		
		//pdg_batch_subject
		$wpdb->get_results("delete from pdg_batch_subject where tuition_batch_id = $hidden_batch_id");				
						
		//pdg_batch_school
		$wpdb->get_results("delete from pdg_batch_school where pdg_batch_id = $hidden_batch_id");
						
		//pdg_batch_fees
		$wpdb->get_results("delete from pdg_batch_fees where tuition_batch_id = $hidden_batch_id");
		
		//pdg_batch_class_timing
		$wpdb->get_results("delete from pdg_batch_class_timing where tuition_batch_id = $hidden_batch_id");
		
		//pdg_tutor_location
		$wpdb->get_results("delete from pdg_tutor_location where tuition_batch_id = $hidden_batch_id");
		
		//delete batch now
		$wpdb->get_results("delete from pdg_tuition_batch where tuition_batch_id = $hidden_batch_id");
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Batch details were removed from database.';		
		echo json_encode($return_message );
		die();
	}

	public static function fn_institute_images_ajax() {
		
		//pedagoge_applog(print_r($_REQUEST, TRUE));
		
		echo 'success';
		die();
	}
	
	public static function fn_delete_class_image_ajax() {
		global $wpdb;
			
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$file_name = sanitize_text_field($_POST['image_file_name']);
		
		$profile_image_path = 'storage/uploads/images/'; 
		$gallary_dir = PEDAGOGE_PLUGIN_DIR.$profile_image_path.$institute_user_id.'/gallery/';
		$file_path = $gallary_dir.$file_name;
		
		if(!is_file($file_path)) {
		
			$return_message['error_type'] = 'file_not_found';
			$return_message['message'] = 'Error! File not found in the server. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(!unlink($file_path)) {
			$return_message['error_type'] = 'file_not_deleted';
			$return_message['message'] = 'Error! File could not be deleted. Please try again.';			
			echo json_encode($return_message );
			die();
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'File was deleted successfully!';			
		echo json_encode($return_message );
		die();
	}

	public static function fn_save_institute_class_image_ajax() {
		global $wpdb;
			
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$profile_dir = PEDAGOGE_PLUGIN_DIR.'storage/uploads/images/'.$institute_user_id;
		$gallery_dir = PEDAGOGE_PLUGIN_DIR.'storage/uploads/images/'.$institute_user_id.'/gallery';
		$gallery_url = PEDAGOGE_PLUGIN_URL.'/storage/uploads/images/'.$institute_user_id.'/gallery/';
		if(!is_dir($profile_dir)){
		    mkdir($profile_dir);
		}
		
		if(!is_dir($gallery_dir)){
		    mkdir($gallery_dir);
		}
		
		if(isset($_FILES['gallery_image']) && $_FILES['gallery_image']["error"] <= 0) {
			$image_name = $_FILES['gallery_image']['name'];
			$extension = explode('.', $image_name);
			$extension = $extension[count($extension)-1];
			
			$new_file_name = '';
			$new_file_path = '';
			
			do{
				$random_name = self::fn_random_string(8);
				$new_file_name = $random_name.'.'.$extension;
				$new_file_path = $gallery_dir.'/'.$new_file_name;
			}while(is_file($new_file_path));
						
			$is_success = move_uploaded_file($_FILES['gallery_image']["tmp_name"], $new_file_path);
			if($is_success) {
				$image_url = $gallery_url.$new_file_name;
				$return_message['error'] = FALSE;
				$return_message['message'] = 'Image was uploaded successfully!';
				$return_message['data'] = array(
					'image_url'=>$image_url,
					'image_file_name'=>$new_file_name
				);
				
				echo json_encode($return_message );
				die();
			} else {
				$return_message['error_type'] = 'file_upload_error2';
				$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
				
				echo json_encode($return_message );
				die();
			}
			
		} else {
			$return_message['error_type'] = 'file_upload_error';
			$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
	}

	public static function fn_random_string($length) {
	    $key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));
	
	    for ($i = 0; $i < $length; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }
	
	    return $key;
	}

	public static function fn_save_institute_final_details_ajax() {
		global $wpdb;
			
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		$institute_data = json_decode( stripslashes( $_POST['institute_data'] ), TRUE);
		
		$txt_fees_structure_description  = sanitize_text_field($institute_data['txt_fees_structure_description']);		
		$chk_payment_installments = sanitize_text_field($institute_data['chk_payment_installments']);
		$chk_allow_demo_classes  = sanitize_text_field($institute_data['chk_allow_demo_classes']);
		$price_per_demo_class = sanitize_text_field($institute_data['price_per_demo_class']);
		$achievements_array = $institute_data['achievements_array'];
		
		$institute_data = array(
			'allow_installments' => $chk_payment_installments,
			'about_fees_structure' => $txt_fees_structure_description,
			'demo_allowed' => $chk_allow_demo_classes,
			'price_per_demo_class' => $price_per_demo_class									
		);
		$data_format_array = array(
			'%s',
			'%s',
			'%s',
			'%s'	
		);
		$wpdb->update('pdg_institutes', $institute_data, array('institute_id'=>$institute_id), $data_format_array, array('%d'));
		
		$wpdb->get_results("delete from pdg_teaching_achievements where user_id=$institute_user_id");
		
		//Achievements
		if(is_array($achievements_array)) {
			foreach($achievements_array as $key=>$value) {
				$achievement = sanitize_text_field($value);
				$insert_array = array(
					'user_id'=>$institute_user_id,
					'achievement'=>$achievement
				);
				$wpdb->insert('pdg_teaching_achievements', $insert_array, array('%d', '%s'));
			}	
		}

		/******************* MASTER TABLE INSERTION FOR INSTITUTION ***************************/

		$query_model = new ModelQuery();
		//global $wpdb;

		/************ GET INSTITUTE DATA FROM VIEW TABLE *************/

		$sql_view = "SELECT institute_name,profile_slug,user_role_name,teaching_xp,localities,subjects FROM view_pdg_institutes_extented WHERE user_id = '".$institute_user_id."'";
		$result_view_data = $wpdb->get_results($sql_view);

		$inst_name = $result_view_data[0]->institute_name;
		$profile_slug = $result_view_data[0]->profile_slug;
		$user_type = $result_view_data[0]->user_role_name;
		$experience = $result_view_data[0]->teaching_xp;
		$localities = $result_view_data[0]->localities;
		$subjects = $result_view_data[0]->subjects;
		$inst_loc_type = $query_model->get_location_type($institute_user_id);
		//$inst_fees = $query_model->get_teacher_fees($institute_user_id);
		$res_fees = $wpdb->get_results("SELECT * FROM `pdg_batch_fees` WHERE tuition_batch_id IN (SELECT tuition_batch_id FROM `pdg_tuition_batch` WHERE tutor_institute_user_id = '".$institute_user_id."')");
		$inst_fees = $res_fees[0]->fees_amount;
		$sql_inst_chk="SELECT * FROM pdg_teacher_institute_master WHERE user_id='".$institute_user_id."'";
	 	$res_inst = $wpdb->get_results($sql_inst_chk);

	 	if(empty($res_inst))
	 	{

			$sql_master = "INSERT INTO pdg_teacher_institute_master SET user_id='".$institute_user_id."',teacher_institute_name='".$inst_name."',user_type='".$user_type."',profile_slug='".$profile_slug."',gender='',localities='".$localities."',subjects='".$subjects."',fees='".$inst_fees."',experience='".$experience."',standard='',location_type='".$inst_loc_type."'";
	 	}
	 	else
	 	{
	 		$sql_master = "UPDATE pdg_teacher_institute_master SET teacher_institute_name='".$inst_name."',profile_slug='".$profile_slug."',gender='',localities='".$localities."',subjects='".$subjects."',fees='".$inst_fees."',experience='".$experience."',standard='',location_type='".$inst_loc_type."' WHERE user_id='".$institute_user_id."'";
	 	}
	 	//echo $sql_master;
		$res_master = $wpdb->get_results($sql_master);

		/******************* END OF MASTER TABLE INSERTION FOR INSTITUTION ***************************/

		//Send Profile update email to user/admin
		$mail_data = array(
			'user_role' => 'institution',
			'user_id' => $institute_user_id,
		);
		ControllerPrivateteacher::fn_send_profile_mail($mail_data);
		$return_message['sql'] = $sql_master;
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Data was saved successfully!';
		echo json_encode($return_message );
		die();
	}	
	
	public static function fn_save_institute_profile_image_ajax() {
		global $wpdb;
			
		$return_message = array(
			'error' => TRUE,
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$institute_user_id = $_POST['hidden_dynamic_user_var'];
		$institute_user_info_id = $_POST['hidden_dynamic_user_info_var'];
		$institute_id = $_POST['hidden_dynamic_insti_var'];
		$institute_edit_secret_key = $_POST['hidden_dynamic_profile_edit_secret_key'];
		
		if( !is_numeric($institute_user_id) || $institute_user_id <= 0 || !is_numeric($institute_user_info_id) || $institute_user_info_id <= 0) {
				
			$return_message['error_type'] = 'userid_not_found';
			$return_message['message'] = 'Error! User ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if( !is_numeric($institute_id) || $institute_id <= 0) {
				
			$return_message['error_type'] = 'institute_id_not_found';
			$return_message['message'] = 'Error! Institute ID not available! Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user = get_user_by('id', $institute_user_id);
		$users_email = $wp_user->user_email;
		if (!wp_check_password( $users_email, $institute_edit_secret_key) ) {
		    $return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! Profile Edit secret do no match! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$profile_dir = PEDAGOGE_PLUGIN_DIR.'storage/uploads/images/'.$institute_user_id;
		$gallery_dir = PEDAGOGE_PLUGIN_DIR.'storage/uploads/images/'.$institute_user_id.'/gallery';
		
		if(!is_dir($profile_dir)){
		    mkdir($profile_dir);
		}
		
		if(!is_dir($gallery_dir)){
		    mkdir($gallery_dir);
		}
		
		$avatar_data = json_decode(stripslashes($_POST['avatar_data']));
		$errorCode = $_FILES["profile_image"]['error'];
		$file = $_FILES["profile_image"];
		if($errorCode === UPLOAD_ERR_OK) {
			$type = exif_imagetype($file['tmp_name']);
			if($type) {
				
				//$extension = image_type_to_extension(IMG_JPG);
				//$file_name =
				
				$profile_image = $profile_dir.'/profile.jpg';
				
				$profile_image_url = PEDAGOGE_PLUGIN_URL.'/storage/uploads/images/'.$institute_user_id.'/profile.jpg';
				
				if (file_exists($profile_image)) {
		            unlink($profile_image);
		        }
				move_uploaded_file($_FILES["profile_image"]["tmp_name"], $profile_image);
				$crop_result = self::img_crop($profile_image, $profile_image, $avatar_data);
				if($crop_result) {
					$file_timestamp = date('U',filemtime($profile_image));
					$return_message['error'] = FALSE;
					$return_message['message'] = 'Profile was set sucessfully!';
					$return_message['data'] = $profile_image_url.'?'.$file_timestamp;
					echo json_encode($return_message );
					die();
				} else {
					$return_message['error_type'] = 'file_upload_failed';
					$return_message['message'] = 'Error! Please upload .jpg, .png or .gif type of images.';
					
					echo json_encode($return_message );
					die();
				}
				
			} else {
				$return_message['error_type'] = 'file_upload_failed';
				$return_message['message'] = 'Error! Please upload .jpg, .png or .gif type of images.';
				
				echo json_encode($return_message );
				die();
			}
		} else {
			$return_message['error_type'] = 'file_upload_failed';
			$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		

		$return_message['error'] = FALSE;
		$return_message['message'] = 'Success!';
		
		echo json_encode($return_message );
		die();
	}

	static function img_crop($src, $dst, $data) {
    if (!empty($src) && !empty($dst) && !empty($data)) {
      	
      $src_img = imagecreatefromjpeg($src);
		
      if (!$src_img) {        
        return FALSE;
      }

      $size = getimagesize($src);
      $size_w = $size[0]; // natural width
      $size_h = $size[1]; // natural height

      $src_img_w = $size_w;
      $src_img_h = $size_h;

      $degrees = $data -> rotate;

      // Rotate the source image
      if (is_numeric($degrees) && $degrees != 0) {
        // PHP's degrees is opposite to CSS's degrees
        $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );

        imagedestroy($src_img);
        $src_img = $new_img;

        $deg = abs($degrees) % 180;
        $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

        $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
        $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);

        // Fix rotated image miss 1px issue when degrees < 0
        $src_img_w -= 1;
        $src_img_h -= 1;
      }

      $tmp_img_w = $data -> width;
      $tmp_img_h = $data -> height;
      $dst_img_w = 220;
      $dst_img_h = 220;

      $src_x = $data -> x;
      $src_y = $data -> y;

      if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
        $src_x = $src_w = $dst_x = $dst_w = 0;
      } else if ($src_x <= 0) {
        $dst_x = -$src_x;
        $src_x = 0;
        $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
      } else if ($src_x <= $src_img_w) {
        $dst_x = 0;
        $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
      }

      if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
        $src_y = $src_h = $dst_y = $dst_h = 0;
      } else if ($src_y <= 0) {
        $dst_y = -$src_y;
        $src_y = 0;
        $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
      } else if ($src_y <= $src_img_h) {
        $dst_y = 0;
        $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
      }

      // Scale to destination position and size
      $ratio = $tmp_img_w / $dst_img_w;
      $dst_x /= $ratio;
      $dst_y /= $ratio;
      $dst_w /= $ratio;
      $dst_h /= $ratio;

      $dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);

      // Add transparent background to destination image
      imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
      imagesavealpha($dst_img, true);

      $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

      if ($result) {
        if (!imagejpeg($dst_img, $dst)) {
          //$this -> msg = "Failed to save the cropped image file";
          imagedestroy($src_img);
      		imagedestroy($dst_img);
          return FALSE;
        }
      } else {
      	imagedestroy($src_img);
      	imagedestroy($dst_img);
		return FALSE;
        //$this -> msg = "Failed to crop the image file";
      }
		
      imagedestroy($src_img);
      imagedestroy($dst_img);
	  return TRUE;
    } else {
    	return FALSE;
    }	
  }
}
