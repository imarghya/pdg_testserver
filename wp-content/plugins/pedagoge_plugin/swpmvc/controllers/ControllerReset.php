<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerReset extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Password Reset:';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';

	}

	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page

		$file_version = '0.2';

		parent::fn_themes_ver2_load_scripts();
		$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine_en'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['password_reset'] = PEDAGOGE_ASSETS_URL . "/js/password_reset.js?ver=" . $this->fixed_version . "." . $file_version;

		$this->app_css['ver2_login'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/login.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css?ver=' . $this->fixed_version . "." . $file_version;

	}

	public function fn_get_header() {
		return $this->load_view( 'ver2/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'ver2/footer' );
	}

	public function fn_get_content() {
		global $wpdb;

		$content = '';
		$reset_key = '';
		if ( is_user_logged_in() ) {

			$template_vars['pdg_user_id'] = $this->app_data['pdg_current_user']->ID;
			$template_vars['pdg_user_email'] = $this->app_data['pdg_current_user']->user_email;

			$content = $this->load_view( 'password_reset/ver2/password_form', $template_vars );

		} else {

			if ( ! isset( $_GET['reset_key'] ) ) {

				$content = $this->load_view( 'password_reset/ver2/reset_link_form' );

			} else {
				$reset_key = sanitize_text_field( $_GET['reset_key'] );

				$columns_array = array (
					'activation_key' => $reset_key,
					'deleted'        => 'no'
				);

				$master_model = new ModelMaster();
				$user_data = $master_model->get_records_by_table_name( 'pdg_user_info', $columns_array );
				if ( empty( $user_data ) || trim($reset_key) == "") {
					$invalid_key_contemt = '
						<div class="col-xs-12 nomargin-bottom-20">
							<div class="panel panel-danger">
								<div class="panel-heading">Invalid password reset key!</div>
								<div class="panel-body">
									<p class="text-black">Check your email for the password reset url.</p>
								</div>
							</div>
						</div>
					';
					$template_vars['invalid_key_contemt'] = $invalid_key_contemt;
					$content = $this->load_view( 'password_reset/ver2/reset_link_form', $template_vars );

				} else {
					$user_id = $user_email = '';
					foreach ( $user_data as $user_info_data ) {
						$user_id = $user_info_data->user_id;
						$user_email = $user_info_data->user_email;
					}

					$template_vars['pdg_user_id'] = $user_id;
					$template_vars['pdg_user_email'] = $user_email;
					$content = $this->load_view( 'password_reset/ver2/password_form', $template_vars );
				}
			}
		}

		return $content;
	}

	public static function send_password_reset_link_ajax() {
		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);

		$email_address = sanitize_email( $_POST['email_address'] );

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}
		if ( ! filter_var( $email_address, FILTER_VALIDATE_EMAIL ) === true ) {
			$return_message['message'] = 'Invalid Email Address';
			echo json_encode( $return_message );
			die();
		}
		/**
		 * 1. check if email exist or not.
		 * 2. change the activation key
		 * 3. send email
		 */

		$columns_array = array (
			'user_email' => $email_address
		);

		$master_model = new ModelMaster();
		$user_data = $master_model->get_records_by_table_name( 'pdg_user_info', $columns_array );

		if ( empty( $user_data ) ) {
			$return_message['error_type'] = 'email_not_found';
			$return_message['message'] = 'This email address is not registered in our website! Try again.';
			echo json_encode( $return_message );
			die();
		}

		self::mail_password_reset_link( $user_data );

	}

	public static function send_password_reset_link_for_username_ajax() {
		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);

		$user_name = sanitize_text_field( $_POST['username'] );

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		/**
		 * 1. check if email exist or not.
		 * 2. change the activation key
		 * 3. send email
		 */
		$columns_array = array (
			'user_name' => $user_name
		);

		$master_model = new ModelMaster();
		$user_data = $master_model->get_records_by_table_name( 'pdg_user_info', $columns_array );


		if ( empty( $user_data ) ) {
			$return_message['error_type'] = 'email_not_found';
			$return_message['message'] = 'This user name is not registered in our website! Try again.';
			echo json_encode( $return_message );
			die();
		}

		self::mail_password_reset_link( $user_data );
	}

	public static function mail_password_reset_link( $user_data ) {

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);

		$email_address = $user_info_id = $user_id = $user_display_name = $activation_key = $password_reset_key = '';

		foreach ( $user_data as $user_info_data ) {
			$user_info_id = $user_info_data->personal_info_id;
			$user_id = $user_info_data->user_id;
			$email_address = $user_info_data->user_email;
			$user_display_name = $user_info_data->display_name;
			$password_reset_key = $activation_key = $user_info_data->activation_key;
		}

		$password_reset_url = add_query_arg( 'reset_key', $password_reset_key, home_url( '/reset' ) );
		$mail_data = array (
			'to_address'         => $email_address,
			'mail_type'          => 'password_reset_user',
			'password_reset_url' => $password_reset_url
		);
		$pedagoge_mailer = new PedagogeMailer( $mail_data );
		$mail_process_completed = $pedagoge_mailer->sendmail();

		if ( $mail_process_completed['error'] != true ) {
			$return_message['error'] = false;
			$return_message['message'] = '<strong>Password Reset link was emailed successfully!</strong> <br/> Check your inbox for the further instructions';
			echo json_encode( $return_message );
			die();
		}

		$return_message['error_type'] = 'email_not_found';
		$return_message['message'] = 'Error generating password reset email! Please try again or contact us.';
		echo json_encode( $return_message );
		die();
	}


	public static function change_password_ajax() {
		global $wpdb;

		$login_url = home_url( '/login' );

		$return_message = array (
			'error'        => true,
			'error_type'   => '',
			'message'      => '',
			'redirect_url' => $login_url
		);

		$password = sanitize_text_field( $_POST['password'] );
		$user_id = sanitize_text_field( $_POST['user_id'] );
		$encrypted_email = sanitize_text_field( $_POST['encrypted_email'] );

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		/**
		 * 1. Load user based on user id
		 * 2. check if encrypted email matches email of the loaded user
		 * 3. change password
		 * 4. send password confirmation email
		 */
		if ( ! is_numeric( $user_id ) ) {
			$return_message['error_type'] = 'user_id_not_numeric';
			$return_message['message'] = 'User not found! Please check your email for password reset link.';
			echo json_encode( $return_message );
			die();
		}
		$columns_array = array (
			'user_id' => $user_id
		);

		$master_model = new ModelMaster();
		$user_data = $master_model->get_records_by_table_name( 'pdg_user_info', $columns_array );

		if ( empty( $user_data ) ) {
			$return_message['error_type'] = 'user_not_found';
			$return_message['message'] = 'User not found! Please check your email for password reset link.';
			echo json_encode( $return_message );
			die();
		}

		$user_info_id = $user_display_name = $user_email = '';

		foreach ( $user_data as $user_info_data ) {
			$user_info_id = $user_info_data->personal_info_id;

			$user_display_name = $user_info_data->display_name;
			$user_email = $user_info_data->user_email;
		}
		require_once( ABSPATH . 'wp-includes/class-phpass.php' );
		$wp_hasher = new PasswordHash( 8, true );

		if ( $wp_hasher->CheckPassword( $user_email, $encrypted_email ) ) {

			/**
			 * 1. change password
			 * 2. log user out
			 * 3. change activation key
			 * 4. send email
			 * 5. Show confirmation
			 * 6. redirect to login
			 */
			wp_set_password( $password, $user_id );
			$sessions = WP_Session_Tokens::get_instance( $user_id );
			// we have got the sessions, destroy them all!

			$sessions->destroy_all();

			$salt = wp_generate_password( 20 ); // 20 character "random" string
			$password_reset_key = sha1( $salt . $user_email . uniqid( time(), true ) );

			$data_array = array (
				'activation_key' => $password_reset_key
			);
			$data_format_array = array (
				'%s',
			);
			$where_data_array = array (
				'personal_info_id' => $user_info_id
			);
			$where_data_format_array = array (
				'%d'
			);

			$result = $wpdb->update( 'pdg_user_info', $data_array, $where_data_array, $data_format_array, $where_data_format_array );
			if ( false === $result ) {
				$password_reset_key = $activation_key;
			}

			$member_update_result = $wpdb->update( 'members', array ( 'password_reset' => 'yes' ), array ( 'email' => $user_email ), array ( '%s' ), array ( '%s' ) );

			$mail_data = array (
				'to_address' => $user_email,
				'mail_type'  => 'password_reset_confirmation',
			);
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();
			
			$return_message['error'] = false;
			$return_message['message'] = '
				<strong>Password was changed successfully!</strong>
				You need to login! Please wait, redirecting!
			';
			echo json_encode( $return_message );
			die();


		} else {

			//error - password did not match
			$return_message['error_type'] = 'security_violation_error';
			$return_message['message'] = 'User not found! Please check your email for password reset link.';
			echo json_encode( $return_message );
			die();
		}

	}

}
