<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerLogout extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		// nothing to do here.
		// Only master controller gets autoconstructed.		 
	}
	
	public function fn_construct_class() {
		//Initialize Master Controller
		parent::__construct(); 
		
		//Page Title
		$this->title = 'Settings Page:';
		
		//You can load your custom assets (css/js) specific to this page. 
		//Master page has already loaded all the required scripts for a blank page.
		$this->fn_load_scripts(); 
		
		//Set this variable to define body class of your page.
		$this->body_class .= ' page-register login-alt page-header-fixed';
		
		//Register variables required in the global scope. 
		//Variables are stored in $this->app_data array variable.
		$this->fn_register_common_variables();	

			
	}
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		//return $this->load_view('match/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		//return $this->load_view('match/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		//$template_vars['example_string'] = 'Hello World!';
		//$template_vars['example_number'] = 4;
		
		//$content = $this->load_view('settings/index', $template_vars);		
		$content = $this->load_view('logout/index');	
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		//Available for the page at global scope.
		$this->app_data['global_example_string'] = 'example string';
		
		//If you wish to include a dynamic inline javascript code then use the variable 'inline_js' available in the $app_data variable.
		// This variable is a string, so append your script string to it. e.g.
		$this->app_data['inline_js'] .= '<script type="text/javascript">console.log("Testing");</script>';
	}
	
	// Example of Ajax request processing
	
	
	
}
		
		
		
	




