<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerSearch extends ControllerMaster implements ControllerMasterInterface {

	public $return_empty_search_result = false;

	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Search';
		$this->body_class .= ' page-register login-alt page-header-fixed';

		$this->fn_load_scripts();
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		$search_version = '4.15';
		//Load Styles and Scripts for Login and Register Page

		parent::fn_themes_ver2_load_scripts();

		$this->app_css['ver2_search'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/search.css?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_css['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['validationEngine_en'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['ver2_social_login'] = PEDAGOGE_ASSETS_URL . '/js/social_login.js?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['ver2_modals_callback'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/modals_callback.js?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_css['ver2_search_common_ui'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/search-common-ui.css?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['ver2_search'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/search.js?ver=' . $this->fixed_version . "." . $search_version;
		$this->app_js['ver2_search_common_ui'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/search-common-ui.js?ver=' . $this->fixed_version . "." . $search_version;

		/** Load footer js **/
	}

	/**
	 * Function to register common variables to global application variables registry
	 */
	private function fn_register_common_variables() {
		global $wpdb;

		$current_user = wp_get_current_user();

		$current_user_db_info = '';

		$current_user_id = $current_user->ID;
		$current_user_role = '';
		if ( $current_user_id > 0 ) {

			$current_user_db_info = $wpdb->get_results( "select * from view_pdg_user_info where user_id=$current_user_id" );
			if ( ! empty( $current_user_db_info ) ) {
				$this->app_data['current_user_db_info'] = $current_user_db_info;
				foreach ( $current_user_db_info as $user_db_info ) {
					$current_user_role = $user_db_info->user_role_name;
				}
			}
		}


		$class_timing_data = PDGManageCache::fn_load_cache( 'pdg_class_timing' );

		$class_type_data = PDGManageCache::fn_load_cache( 'pdg_subject_type' );

		$academic_board_data = PDGManageCache::fn_load_cache( 'pdg_academic_board' );

		$teaching_xp_data = PDGManageCache::fn_load_cache( 'pdg_teaching_xp' );

		$str_dynamic_js = '
			<script type="text/javascript" charset="utf-8">
				var $current_user_id = ' . $current_user_id . ',
				$current_user_role = "' . $current_user_role . '";
			</script>
		';


		$this->app_data['class_timing_data'] = $class_timing_data;
		$this->app_data['class_type_data'] = $class_type_data;
		$this->app_data['academic_board_data'] = $academic_board_data;
		$this->app_data['teaching_xp_data'] = $teaching_xp_data;

		$this->app_data['dynamic_js'] = $str_dynamic_js;
	}


	public function fn_get_header() {
		if ( isset( $this->app_data['seo_title'] ) ) {
			$this->title = $this->app_data['seo_title'];
		}

		return $this->load_view( 'ver2/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'ver2/footer' );
	}

	public static function is_numeric_callback( $param ) {
		return ( ( is_int( $param ) || ctype_digit( $param ) ) && (int) $param > 0 );
	}

	public function fn_get_content() {
		global $wpdb;

		$search_result_items = array ();
		$loggedin_user_id = 0;
		$loggedin_user_role = '';

		if ( isset( $this->app_data['current_user_db_info'] ) ) {
			foreach ( $this->app_data['current_user_db_info'] as $user_db_info ) {
				$loggedin_user_role = $user_db_info->user_role_name;
				$loggedin_user_id = $user_db_info->user_id;
			}
		}

		$searched_subject_name = ( isset( $_GET['pdg_subject'] ) && ! is_array( $_GET['pdg_subject'] ) ) ? sanitize_text_field( $_GET['pdg_subject'] ) : '';
		$searched_locality_name = ( isset( $_GET['pdg_locality'] ) && ! is_array( $_GET['pdg_locality'] ) ) ? sanitize_text_field( $_GET['pdg_locality'] ) : '';
		$searched_teacher_name = '';

		$filter_institute_teacher_array[0] = isset( $this->app_data['seo_role'] ) ? $this->app_data['seo_role'] : '';

		$searchData = array (
			'no_of_records_per_page'         => 20,
			'search_type'                    => 'filter',
			'subject_name'                   => $searched_subject_name,
			'locality_names'                 => $searched_locality_name,
			'loggedin_user_id'               => $loggedin_user_id,
			'loggedin_user_role'             => $loggedin_user_role,
			'filter_institute_teacher_array' => $filter_institute_teacher_array,
			'pagination_no'                  => 0
		);

		/*
		 * NB: This is how it works.
		 * this function ie. "fn_get_content" is loaded first and the initial load condition variables are passed on through the "fn_search_ajax" function.
		 * The return values which are in the form of array is passed on to "search/ver2/page-search" through app_data['search_result_items'] variable.
		 * search_result_items is then parsed using moustache php parser.
		 *
		 * Another working principle which has to be noted here:
		 * Search page is programmed in such a way that the javascript parse the template using the ajax returned json files, which wont be parsed by goole bots.
		 * So we are forced to write up this initial condition as well.
		 * Jquery sends and recieves the search data in the initial condition too but that need not be displayed in the initial search result(else double result might come up).
		 * So jquery checks for the class ".search_result_item_cards.initialLoad", if this exists then the first load condition is not loaded why jquery because it is already loaded via php
		 * This is just one time for initial loading other wise it's fine
		 * */
		$search_result_items = self::fn_search_ajax( $searchData );
		$this->app_data['search_result_items'] = $search_result_items;
		$this->app_data['searched_subject_name'] = $searched_subject_name;
		$this->app_data['searched_locality_name'] = $searched_locality_name;

		#desktop or larger devices
		return $this->load_view( 'search/ver2/page-search' );
	}

	/**
	 * Note to future developer - This is a hack job. I did not have time to think better.
	 * You need to migrate to complete SQL queries for performance and better result.
	 * You should never continue with the current code.
	 * If you get a chance - rewrite everything.
	 * Date - 18/04/2016 18:15
	 */
	public static function fn_search_ajax( $customdata = null ) {
		$dataAvailableFlag = false;
		$customdataAvailableFlag = false;
		$search_data = array ();
		if ( isset( $_POST['searchData'] ) ) {
			$dataAvailableFlag = true;
			$search_data = $_POST['searchData'];

		} else if ( is_array( $customdata ) && ! empty( $customdata ) ) {
			$dataAvailableFlag = $customdataAvailableFlag = true;
			$search_data = $customdata;
		}

		if ( ! $dataAvailableFlag ) {
			exit;
		}

		$no_of_records_per_page = isset( $search_data['no_of_records_per_page'] ) && ! is_array( $search_data['no_of_records_per_page'] ) && is_numeric( $search_data['no_of_records_per_page'] ) && $search_data['no_of_records_per_page'] < 64 ? (int) sanitize_text_field( $search_data['no_of_records_per_page'] ) : 16;
		$loggedin_user_id = isset( $search_data['current_user_id'] ) && ! is_array( $search_data['current_user_id'] ) && is_numeric( $search_data['current_user_id'] ) ? (int) sanitize_text_field( $search_data['current_user_id'] ) : 0;
		$loggedin_user_role = isset( $search_data['current_user_role'] ) && ! is_array( $search_data['current_user_role'] ) ? sanitize_text_field( $search_data['current_user_role'] ) : '';
		$subject_name = isset( $search_data['subject_name'] ) && ! is_array( $search_data['subject_name'] ) ? sanitize_text_field( trim( $search_data['subject_name'] ) ) : '';
		$locality_names = isset( $search_data['locality_names'] ) && ! is_array( $search_data['locality_names'] ) ? sanitize_text_field( trim( $search_data['locality_names'] ) ) : '';
		$institute_teacher_name = isset( $search_data['institute_teacher_name'] ) && ! is_array( $search_data['institute_teacher_name'] ) ? sanitize_text_field( trim( $search_data['institute_teacher_name'] ) ) : '';
		$pagination_no = isset( $search_data['pagination_no'] ) && ! is_array( $search_data['pagination_no'] ) && is_numeric( $search_data['pagination_no'] ) && $search_data['pagination_no'] >= 0 ? sanitize_text_field( (int) $search_data['pagination_no'] ) : 0;
		//todo: page no start with 0 here and in the prev ver with 1
		$search_type = isset( $search_data['search_type'] ) && ! is_array( $search_data['search_type'] ) ? sanitize_text_field( $search_data['search_type'] ) : 'filter';

		$filter_institute_teacher_array = isset( $search_data['filter_institute_teacher_array'] ) && is_array( $search_data['filter_institute_teacher_array'] ) && isset( $search_data['filter_institute_teacher_array'][0] ) ? $search_data['filter_institute_teacher_array'] : array ( 0 => '' );
		$location_type_array = isset( $search_data['location_type_array'] ) && is_array( $search_data['location_type_array'] ) ? $search_data['location_type_array'] : array ();
		$class_type_array = isset( $search_data['class_type_array'] ) && is_array( $search_data['class_type_array'] ) ? $search_data['class_type_array'] : array ();
		$teaching_xp_array = isset( $search_data['teaching_xp_array'] ) && is_array( $search_data['teaching_xp_array'] ) ? $search_data['teaching_xp_array'] : array ();
		$academic_board_array = isset( $search_data['academic_board_array'] ) && is_array( $search_data['academic_board_array'] ) ? $search_data['academic_board_array'] : array ();
		$fees_range_array = isset( $search_data['fees_range_array'] ) && is_array( $search_data['fees_range_array'] ) ? $search_data['fees_range_array'] : array ();
		$days_taught_array = isset( $search_data['days_taught_array'] ) && is_array( $search_data['days_taught_array'] ) ? $search_data['days_taught_array'] : array ();
		$sort_exp_array = isset( $search_data['sort_exp_array'] ) && is_array( $search_data['sort_exp_array'] ) ? $search_data['sort_exp_array'] : array ();
		$sort_reviews_array = isset( $search_data['sort_reviews_array'] ) && is_array( $search_data['sort_reviews_array'] ) ? $search_data['sort_reviews_array'] : array ();
		$sort_fee_array = isset( $search_data['sort_fee_array'] ) && is_array( $search_data['sort_fee_array'] ) ? $search_data['sort_fee_array'] : array ();

		//todo: make the var names same
		$search_data = array (
			'subject_name'                   => $subject_name,
			'found_locality_name'            => '',
			'institute_teacher_name'         => $institute_teacher_name,
			'class_timing_array'             => array (),
			'locality_names'                 => $locality_names,
			'location_type_array'            => $location_type_array,
			'search_type'                    => $search_type,
			'loggedin_user_id'               => $loggedin_user_id,
			'loggedin_user_role'             => $loggedin_user_role,
			'filter_institute_teacher_array' => $filter_institute_teacher_array,
			'teaching_xp_array'              => $teaching_xp_array,
			'class_type_array'               => $class_type_array,
			'academic_board_array'           => $academic_board_array,
			'fees_range_array'               => $fees_range_array,
			'days_taught_array'              => $days_taught_array,
			'sort_exp_array'                 => $sort_exp_array,
			'sort_reviews_array'             => $sort_reviews_array,
			'sort_fee_array'                 => $sort_fee_array,
			'pagination_no'                  => $pagination_no,
			'no_of_records_per_page'         => $no_of_records_per_page,
		);

		$return_data = array (
			'status'  => '',
			'type'    => '',
			'message' => '',
			'items'   => ''
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $customdataAvailableFlag && ! $is_valid_referer ) {
			$status = "error";
			$message = "Invalid form";
			$type = "security";
			$return_data = array (
				'status'  => $status,
				'type'    => $type,
				'message' => $message,
				'items'   => ''
			);
		} else {
			$return_data = self::fn_return_search_content( $search_data );
		}
		if ( $customdataAvailableFlag ) {
			return $return_data;
		}
		print ( json_encode( $return_data ) );
		exit;
	}

	public static function fn_return_search_content( $search_data ) {
		global $wpdb;
		$no_of_records_per_page = isset( $search_data['no_of_records_per_page'] ) && ! is_array( $search_data['no_of_records_per_page'] ) && is_numeric( $search_data['no_of_records_per_page'] ) && $search_data['no_of_records_per_page'] < 64 ? (int) sanitize_text_field( $search_data['no_of_records_per_page'] ) : 16;
		$loggedin_user_id = isset( $search_data['current_user_id'] ) && ! is_array( $search_data['current_user_id'] ) && is_numeric( $search_data['current_user_id'] ) ? (int) sanitize_text_field( $search_data['current_user_id'] ) : 0;
		$loggedin_user_role = isset( $search_data['current_user_role'] ) && ! is_array( $search_data['current_user_role'] ) ? sanitize_text_field( $search_data['current_user_role'] ) : '';
		$subject_name = isset( $search_data['subject_name'] ) && ! is_array( $search_data['subject_name'] ) ? sanitize_text_field( trim( $search_data['subject_name'] ) ) : '';
		$locality_names = isset( $search_data['locality_names'] ) && ! is_array( $search_data['locality_names'] ) ? sanitize_text_field( trim( $search_data['locality_names'] ) ) : '';
		$institute_teacher_name = isset( $search_data['institute_teacher_name'] ) && ! is_array( $search_data['institute_teacher_name'] ) ? sanitize_text_field( trim( $search_data['institute_teacher_name'] ) ) : '';
		$pagination_no = isset( $search_data['pagination_no'] ) && ! is_array( $search_data['pagination_no'] ) && is_numeric( $search_data['pagination_no'] ) && $search_data['pagination_no'] >= 0 ? sanitize_text_field( (int) $search_data['pagination_no'] ) : 0;
		//todo: page no start with 0 here and in the prev ver with 1
		$search_type = isset( $search_data['search_type'] ) && ! is_array( $search_data['search_type'] ) ? sanitize_text_field( $search_data['search_type'] ) : 'filter';

		$filter_institute_teacher_array = isset( $search_data['filter_institute_teacher_array'] ) && is_array( $search_data['filter_institute_teacher_array'] ) && isset( $search_data['filter_institute_teacher_array'][0] ) ? $search_data['filter_institute_teacher_array'] : array ( 0 => '' );
		$location_type_array = isset( $search_data['location_type_array'] ) && is_array( $search_data['location_type_array'] ) ? $search_data['location_type_array'] : array ();
		$class_type_array = isset( $search_data['class_type_array'] ) && is_array( $search_data['class_type_array'] ) ? $search_data['class_type_array'] : array ();
		$teaching_xp_array = isset( $search_data['teaching_xp_array'] ) && is_array( $search_data['teaching_xp_array'] ) ? $search_data['teaching_xp_array'] : array ();
		$academic_board_array = isset( $search_data['academic_board_array'] ) && is_array( $search_data['academic_board_array'] ) ? $search_data['academic_board_array'] : array ();
		$fees_range_array = isset( $search_data['fees_range_array'] ) && is_array( $search_data['fees_range_array'] ) ? $search_data['fees_range_array'] : array ();
		$days_taught_array = isset( $search_data['days_taught_array'] ) && is_array( $search_data['days_taught_array'] ) ? $search_data['days_taught_array'] : array ();
		$sort_exp_array = isset( $search_data['sort_exp_array'] ) && is_array( $search_data['sort_exp_array'] ) ? $search_data['sort_exp_array'] : array ();
		$sort_reviews_array = isset( $search_data['sort_reviews_array'] ) && is_array( $search_data['sort_reviews_array'] ) ? $search_data['sort_reviews_array'] : array ();
		$sort_fee_array = isset( $search_data['sort_fee_array'] ) && is_array( $search_data['sort_fee_array'] ) ? $search_data['sort_fee_array'] : array ();

		$start_from = $pagination_no * ( $no_of_records_per_page );

		$filter_institute_teacher = $filter_institute_teacher_array[0];
		if ( ! empty( $filter_institute_teacher ) ) {
			switch ( $filter_institute_teacher ) {
				case 'teacher':
				case 'institution':
					break;
				default:
					$filter_institute_teacher = '';
					break;
			}
		}

		$found_users_id_array = array ();
		$is_filter_set = false;

		/**
		 * Subjects Filtering
		 */


		if ( ! empty( $subject_name ) ) {
			$subject_name = trim( $subject_name, "," );
			$subject_id_array = array ();
			$subject_like_array = array ();

			foreach ( explode( ",", $subject_name ) as $sub_val ) {
				if ( trim( $sub_val ) != "" ) {
					$subject_like_array[] = "subject_name LIKE '$sub_val'";    // nirajkvinit 20/01/2016
				}
			}
			$subject_names_db = $wpdb->get_results( "SELECT subject_name_id FROM pdg_subject_name where " . implode( " OR ", $subject_like_array ) . "" );
			if ( empty( $subject_names_db ) ) {
				// return empty search result
				return self::fn_empty_search_result( 1 );
			}

			foreach ( $subject_names_db as $db_subject_name ) {
				$subject_name_id = $db_subject_name->subject_name_id;
				if ( ! in_array( $subject_name_id, $subject_id_array ) ) {
					$subject_id_array[] = $subject_name_id;
				}
			}

			$str_subject_id_array = implode( ',', $subject_id_array );

			$arr_users_by_subjects = array ();
			$str_sql = "
				select distinct(tutor_institute_user_id) as found_user_id from view_pdg_batch_subject where subject_name_id in ($str_subject_id_array)
			";
			if ( ! empty( $filter_institute_teacher ) ) {
				$str_sql .= " and user_role_name = '$filter_institute_teacher'";
			}
			$found_users_by_subjects = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_subjects ) ) {
				foreach ( $found_users_by_subjects as $found_user ) {
					$found_user_id = $found_user->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_subjects ) ) {
						$arr_users_by_subjects[] = $found_user_id;
					}
				}
			} else {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 2 );
			}
			$is_filter_set = true;
			$found_users_id_array = $arr_users_by_subjects;
		}

		/**
		 * Locality by name Filtering
		 */

		if ( ! empty( $found_locality_name ) ) {

			$localities_id_array = array ();

			$localities_names_db = $wpdb->get_results( "SELECT locality_id FROM pdg_locality where locality like '%$found_locality_name%'" );
			if ( empty( $localities_names_db ) ) {
				// return empty search result
				return self::fn_empty_search_result( 3 );
			}

			foreach ( $localities_names_db as $db_locality_name ) {
				$locality_name_id = $db_locality_name->locality_id;
				if ( ! in_array( $locality_name_id, $localities_id_array ) ) {
					$localities_id_array[] = $locality_name_id;
				}
			}

			$str_locality_id_array = implode( ',', $localities_id_array );


			$arr_users_by_locality = array ();
			$str_sql = "
				select distinct(tutor_institute_user_id) as found_user_id from view_pdg_tutor_location where locality_id in ($str_locality_id_array)
			";
			if ( ! empty( $filter_institute_teacher ) ) {
				$str_sql .= " and user_role_name = '$filter_institute_teacher'";
			}
			$found_users_by_locality_name = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_locality_name ) ) {
				foreach ( $found_users_by_locality_name as $locations_search_data ) {
					$found_user_id = $locations_search_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_locality ) ) {
						$arr_users_by_locality[] = $found_user_id;
					}
				}
			} else {
				return self::fn_empty_search_result( 4 );
			}
			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_locality );
				if ( empty( $found_users_id_array ) ) {
					return self::fn_empty_search_result( 5 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_locality;
			}
		}


		/**
		 * Locations type Filtering
		 */
		if ( ! empty( $location_type_array ) && is_array( $location_type_array ) ) {
			$str_location_type_sql = "select distinct(tutor_institute_user_id) as found_user_id from view_pdg_tutor_location where ";
			$str_location_type_sql .= ' tuition_location_type IN ("' . implode( '", "', $location_type_array ) . '")';

			if ( ! empty( $filter_institute_teacher ) ) {
				$str_location_type_sql .= " and user_role_name = '$filter_institute_teacher'";
			}

			$arr_users_by_location_type = array ();
			$found_users_by_location_type = $wpdb->get_results( $str_location_type_sql );
			if ( ! empty( $found_users_by_location_type ) ) {
				foreach ( $found_users_by_location_type as $locations_search_data ) {
					$found_user_id = $locations_search_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_location_type ) ) {
						$arr_users_by_location_type[] = $found_user_id;
					}
				}
			} else {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 6 );
			}
			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_location_type );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 7 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_location_type;
			}
		}

		/**
		 * Locality filtering
		 */
		if ( ! empty( $locality_names ) ) {
			$locality_names = trim( $locality_names, "," );
			$searched_locality_id_array = explode( ',', $locality_names );

			$filtered_searched_locality_id_array = array ();
			if ( count( $searched_locality_id_array ) > 0 ) {
				$filtered_searched_locality_id_array = array_filter( $searched_locality_id_array, array (
					__CLASS__,
					'is_numeric_callback'
				) );
			}
			if ( count( $filtered_searched_locality_id_array ) > 0 ) {
				$str_locality_sql = "
		 		select distinct(tutor_institute_user_id) as found_user_id 
		 		from view_pdg_tutor_location 
		 		where 
		 		locality_id in (" . implode( ', ', $filtered_searched_locality_id_array ) . ") 
		 	";
				if ( ! empty( $filter_institute_teacher ) ) {
					$str_locality_sql .= " and user_role_name = '$filter_institute_teacher'";
				}

				$arr_users_by_locality = array ();
				$found_users_by_locality = $wpdb->get_results( $str_locality_sql );
				if ( ! empty( $found_users_by_locality ) ) {
					foreach ( $found_users_by_locality as $locations_search_data ) {
						$found_user_id = $locations_search_data->found_user_id;
						if ( ! in_array( $found_user_id, $arr_users_by_locality ) ) {
							$arr_users_by_locality[] = $found_user_id;
						}
					}
				} else {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 8 );
				}
				if ( $is_filter_set ) {
					//intersect
					$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_locality );
					if ( empty( $found_users_id_array ) ) {
						/**
						 * Do not search more and send nothing found
						 * Because intersecting with empty array will result in empty result anyway
						 */
						return self::fn_empty_search_result( 9 );
					}
				} else {
					$is_filter_set = true;
					$found_users_id_array = $arr_users_by_locality;
				}
			}
		}

		/**
		 * Teaching XP filtering
		 */
		if ( ! empty( $teaching_xp_array ) && is_array( $teaching_xp_array ) ) {

			$teacher_sql = "select user_id from pdg_teacher where teaching_xp_id in (" . implode( ', ', $teaching_xp_array ) . ")";
			$institute_sql = "select user_id from pdg_institutes where avg_teaching_xp_id in (" . implode( ', ', $teaching_xp_array ) . ")";
			$arr_users_by_teaching_xp = array ();
			$str_sql = "";

			if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'teacher' ) {
				$str_sql = $teacher_sql;
				$found_users_by_teaching_xp = $wpdb->get_results( $str_sql );
				if ( ! empty( $found_users_by_teaching_xp ) ) {
					foreach ( $found_users_by_teaching_xp as $found_user_data ) {
						$found_user_id = $found_user_data->user_id;
						if ( ! in_array( $found_user_id, $arr_users_by_teaching_xp ) ) {
							$arr_users_by_teaching_xp[] = $found_user_id;
						}
					}
				}
			}
			if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'institution' ) {
				$str_sql = $institute_sql;
				$found_users_by_teaching_xp = $wpdb->get_results( $str_sql );
				if ( ! empty( $found_users_by_teaching_xp ) ) {
					foreach ( $found_users_by_teaching_xp as $found_user_data ) {
						$found_user_id = $found_user_data->user_id;
						if ( ! in_array( $found_user_id, $arr_users_by_teaching_xp ) ) {
							$arr_users_by_teaching_xp[] = $found_user_id;
						}
					}
				}
			}

			if ( empty( $arr_users_by_teaching_xp ) ) {
				return self::fn_empty_search_result( 10 );
			}

			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_teaching_xp );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 11 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_teaching_xp;
			}
		}

		/**
		 * Name Filtering
		 */
		if ( ! empty( $institute_teacher_name ) ) {

			$arr_users_by_name = array ();

			$str_teacher_sql = "
				select * from view_pdg_teacher_names where teacher_name like '%$institute_teacher_name%'
			";
			$str_institute_sql = "
				select institute_name as teacher_name, user_id from pdg_institutes where institute_name like '%$institute_teacher_name%'
			";

			if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'teacher' ) {
				$found_teacher_data = $wpdb->get_results( $str_teacher_sql );
				if ( ! empty( $found_teacher_data ) ) {
					foreach ( $found_teacher_data as $name_data ) {
						$found_name = $name_data->teacher_name;
						$found_user_id = $name_data->user_id;
						if ( ! in_array( $found_user_id, $arr_users_by_name ) ) {
							$arr_users_by_name[] = $found_user_id;
						}
					}
				}
			}

			if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'institution' ) {
				$found_institute_data = $wpdb->get_results( $str_institute_sql );
				if ( ! empty( $found_institute_data ) ) {
					foreach ( $found_institute_data as $name_data ) {
						$found_name = $name_data->teacher_name;
						$found_user_id = $name_data->user_id;
						if ( ! in_array( $found_user_id, $arr_users_by_name ) ) {
							$arr_users_by_name[] = $found_user_id;
						}
					}
				}
			}

			if ( empty( $arr_users_by_name ) ) {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 12 );
			}

			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_name );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 13 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_name;
			}
		}

		/**
		 * Class Type filtering
		 */
		if ( ! empty( $class_type_array ) && is_array( $class_type_array ) ) {

			$str_sql = " 
				select distinct(tutor_institute_user_id) as found_user_id
				from view_pdg_batch_subject where subject_type_id in (" . implode( ', ', $class_type_array ) . ")
			";

			if ( ! empty( $filter_institute_teacher ) ) {
				$str_sql .= " and user_role_name = '$filter_institute_teacher'";
			}

			$arr_users_by_subject_type = array ();
			$found_users_by_subject_type = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_subject_type ) ) {
				foreach ( $found_users_by_subject_type as $found_user_data ) {
					$found_user_id = $found_user_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_subject_type ) ) {
						$arr_users_by_subject_type[] = $found_user_id;
					}
				}
			} else {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 14 );
			}

			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_subject_type );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 15 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_subject_type;
			}
		}

		/**
		 * Academic Board filtering
		 */
		if ( ! empty( $academic_board_array ) && is_array( $academic_board_array ) ) {

			$str_sql = " 
				select distinct(tutor_institute_user_id) as found_user_id
				from view_pdg_batch_subject where academic_board_id in (" . implode( ', ', $academic_board_array ) . ")
			";

			if ( ! empty( $filter_institute_teacher ) ) {
				$str_sql .= " and user_role_name = '$filter_institute_teacher'";
			}

			$arr_users_by_academic_board = array ();
			$found_users_by_academic_board = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_academic_board ) ) {
				foreach ( $found_users_by_academic_board as $found_user_data ) {
					$found_user_id = $found_user_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_academic_board ) ) {
						$arr_users_by_academic_board[] = $found_user_id;
					}
				}
			} else {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 16 );
			}

			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_academic_board );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 17 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_academic_board;
			}
		}

		/**
		 * Class Timing filtering
		 */
		if ( ! empty( $class_timing_array ) && is_array( $class_timing_array ) ) {

			$str_sql = " 
				select distinct(tutor_institute_user_id) as found_user_id
				from view_pdg_batch_class_timing where class_timing_id in (" . implode( ', ', $class_timing_array ) . ")
			";

			if ( ! empty( $filter_institute_teacher ) ) {
				$str_sql .= " and user_role_name = '$filter_institute_teacher'";
			}

			$arr_users_by_class_timing = array ();
			$found_users_by_class_timing = $wpdb->get_results( $str_sql );
			if ( ! empty( $found_users_by_class_timing ) ) {
				foreach ( $found_users_by_class_timing as $found_user_data ) {
					$found_user_id = $found_user_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_class_timing ) ) {
						$arr_users_by_class_timing[] = $found_user_id;
					}
				}
			} else {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 18 );
			}

			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_class_timing );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 19 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_class_timing;
			}
		}

		/**
		 * Days taught filtering
		 */

		if ( ! empty( $days_taught_array ) && is_array( $days_taught_array ) ) {
			$arr_users_by_course_days = array ();
			$str_course_days_sql = " 
				select distinct(tutor_institute_user_id) as found_user_id
				from pdg_tuition_batch where 
			";

			foreach ( $days_taught_array as $day_name ) {
				$str_course_days_sql .= " course_days like '%$day_name%' or ";
			}

			$str_course_days_sql = rtrim( trim( $str_course_days_sql ), 'or' );

			if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'teacher' ) {
				$str_teacher_sql = "select user_id from pdg_teacher where user_id in ($str_course_days_sql)";
				$teacher_data = $wpdb->get_results( $str_teacher_sql );
				foreach ( $teacher_data as $teacher ) {
					$teacher_user_id = $teacher->user_id;
					if ( ! in_array( $teacher_user_id, $arr_users_by_course_days ) ) {
						$arr_users_by_course_days[] = $teacher_user_id;
					}
				}
			}
			if ( empty( $filter_institute_teacher ) || $filter_institute_teacher == 'institution' ) {
				$str_institute_sql = "select user_id from pdg_institutes where user_id in ($str_course_days_sql)";
				$institute_data = $wpdb->get_results( $str_institute_sql );
				foreach ( $institute_data as $institute ) {
					$institute_user_id = $institute->user_id;
					if ( ! in_array( $institute_user_id, $arr_users_by_course_days ) ) {
						$arr_users_by_course_days[] = $institute_user_id;
					}
				}
			}

			if ( empty( $arr_users_by_course_days ) ) {
				return self::fn_empty_search_result( 20 );
			}

			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_course_days );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 21 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_course_days;
			}
		}

		/**
		 * Fees Range filtering
		 */
		if ( ! empty( $fees_range_array ) && is_array( $fees_range_array ) ) {

			$str_sql = " 
				select distinct(tutor_institute_user_id) as found_user_id
				from view_pdg_batch_fees where fees_type_id <> 1 
			";
			$min_amt = 0;
			$max_amt = 0;
			$super_max_set = false;
			$is_min_set = false;
			foreach ( $fees_range_array as $fees_range_id ) {
				switch ( $fees_range_id ) {
					case 1:
						if ( ! $is_min_set ) {
							$min_amt = 0;
							$is_min_set = true;
						}
						if ( $max_amt < 100 ) {
							$max_amt = 100;
						}
						break;
					case 2:
						//$str_sql .= " and fees_amount between 101 and 300 ";
						if ( ! $is_min_set ) {
							$min_amt = 101;
							$is_min_set = true;
						}
						if ( $max_amt < 300 ) {
							$max_amt = 300;
						}
						break;
					case 3:
						//$str_sql .= " and fees_amount between 301 and 500 ";
						if ( ! $is_min_set ) {
							$min_amt = 301;
							$is_min_set = true;
						}
						if ( $max_amt < 500 ) {
							$max_amt = 500;
						}
						break;
					case 4:
						//$str_sql .= " and fees_amount between 501 and 1000 ";
						if ( ! $is_min_set ) {
							$min_amt = 501;
							$is_min_set = true;
						}
						if ( $max_amt < 1000 ) {
							$max_amt = 1000;
						}
						break;
					case 5:
						//$str_sql .= " and fees_amount between 1001 and 2000 ";
						if ( ! $is_min_set ) {
							$min_amt = 1001;
							$is_min_set = true;
						}
						if ( $max_amt < 2000 ) {
							$max_amt = 2000;
						}
						break;
					case 6:
						//$str_sql .= " and fees_amount between 2001 and 5000 ";
						if ( ! $is_min_set ) {
							$min_amt = 2001;
							$is_min_set = true;
						}
						if ( $max_amt < 5000 ) {
							$max_amt = 5000;
						}
						break;
					case 7:
						//$str_sql .= " and fees_amount between 5001 and 10000 ";
						if ( ! $is_min_set ) {
							$min_amt = 5001;
							$is_min_set = true;
						}
						if ( $max_amt < 10000 ) {
							$max_amt = 10000;
						}
						break;
					case 8:
						//$str_sql .= " and fees_amount between 10001 and 20000 ";
						if ( ! $is_min_set ) {
							$min_amt = 10001;
							$is_min_set = true;
						}
						if ( $max_amt < 20000 ) {
							$max_amt = 20000;
						}
						break;
					case 9:
						//$str_sql .= " and fees_amount between 20001 and 40000 ";
						if ( ! $is_min_set ) {
							$min_amt = 20001;
							$is_min_set = true;
						}
						if ( $max_amt < 40000 ) {
							$max_amt = 40000;
						}
						break;
					case 10:
						//$str_sql .= " and fees_amount between 40001 and 70000 ";
						if ( ! $is_min_set ) {
							$min_amt = 40001;
							$is_min_set = true;
						}
						if ( $max_amt < 70000 ) {
							$max_amt = 70000;
						}
						break;
					case 11:
						//$str_sql .= " and fees_amount between 70001 and 100000 ";
						if ( ! $is_min_set ) {
							$min_amt = 70001;
							$is_min_set = true;
						}
						if ( $max_amt < 100000 ) {
							$max_amt = 100000;
						}
						break;
					case 12:
						//$str_sql .= " and fees_amount > 100001 ";
						/*if(!$is_min_set) {
							$min_amt = 100001;
							$is_min_set = TRUE;
						}*/
						$super_max_set = true;
						break;
				}
			}

			if ( $super_max_set && ! $is_min_set ) {
				$str_sql .= " and fees_amount > 100001";
			} else if ( ! $super_max_set && $is_min_set ) {
				$str_sql .= " and fees_amount between " . $min_amt . " and " . $max_amt;
			} else if ( $super_max_set && $is_min_set ) {
				$str_sql .= " and fees_amount between " . $min_amt . " and " . $max_amt . " or fees_amount > 100001";
			}

			if ( ! empty( $filter_institute_teacher ) ) {
				$str_sql .= " and user_role_name = '$filter_institute_teacher'";
			}

			$arr_users_by_fees_range = array ();
			$found_users_by_fees_range = $wpdb->get_results( $str_sql );


			if ( ! empty( $found_users_by_fees_range ) ) {
				foreach ( $found_users_by_fees_range as $found_user_data ) {
					$found_user_id = $found_user_data->found_user_id;
					if ( ! in_array( $found_user_id, $arr_users_by_fees_range ) ) {
						$arr_users_by_fees_range[] = $found_user_id;
					}
				}
			} else {
				/**
				 * Do not search more and send nothing found
				 * Because intersecting with empty array will result in empty result anyway
				 */
				return self::fn_empty_search_result( 22 );
			}
			if ( $is_filter_set ) {
				//intersect
				$found_users_id_array = array_intersect( $found_users_id_array, $arr_users_by_fees_range );
				if ( empty( $found_users_id_array ) ) {
					/**
					 * Do not search more and send nothing found
					 * Because intersecting with empty array will result in empty result anyway
					 */
					return self::fn_empty_search_result( 23 );
				}
			} else {
				$is_filter_set = true;
				$found_users_id_array = $arr_users_by_fees_range;
			}
		}


		$found_content = array ();
		$data_to_process = array (
			'found_users'            => $found_users_id_array,
			'sort_exp_array'         => $sort_exp_array,
			'sort_fee_array'         => $sort_fee_array,
			'sort_reviews_array'     => $sort_reviews_array,
			'start_from'             => $start_from,
			'no_of_records_per_page' => $no_of_records_per_page,
			'loggedin_user_id'       => $loggedin_user_id,
			'loggedin_user_role'     => $loggedin_user_role,
			'pagination_no'          => $pagination_no
		);
		$return_message['status'] = "success";
		$return_message['type'] = 'none';
		$return_message['message'] = '';
		$return_message['items'] = array ();

		if ( empty( $found_users_id_array ) ) {
			$found_content = PDGSearchContent::fn_initial_content( $data_to_process );
		} else {
			if ( count( $found_users_id_array ) < $no_of_records_per_page ) {
				$return_message['type'] = 'lastpage';
				$return_message['message'] = 'Last page';
			}
			$found_content = PDGSearchContent::fn_return_search_result_html( $data_to_process );
		}

		if ( empty( $found_content ) ) {
			$return_message['status'] = "error";
			$return_message['type'] = 'endofitem';
			$return_message['message'] = 'End of search results';
		} else {
			$return_message['items'] = $found_content;
		}

		return $return_message;
	}

	public static function fn_empty_search_result( $debugging_value ) {
		return $return_data = array (
			'status'  => 'error',
			'type'    => 'empty',
			'message' => 'No result found',
			'items'   => '',
		);
	}


	public static function fn_teacher_names_data() {
		global $wpdb;
		$names_array = array ();

		$str_teacher_sql = "
			select * from view_pdg_teacher_names
		";
		$str_institute_sql = "
			select institute_name as teacher_name, user_id from pdg_institutes 
		";
		$found_teacher_data = $wpdb->get_results( $str_teacher_sql );

		if ( ! empty( $found_teacher_data ) ) {
			foreach ( $found_teacher_data as $name_data ) {
				$found_name = trim( $name_data->teacher_name );
				if ( ! in_array( $found_name, $names_array ) ) {
					$names_array[] = $found_name;
				}
			}
		}

		$found_institute_data = $wpdb->get_results( $str_institute_sql );

		if ( ! empty( $found_institute_data ) ) {
			foreach ( $found_institute_data as $name_data ) {
				$found_name = trim( $name_data->teacher_name );
				if ( ! in_array( $found_name, $names_array ) ) {
					$names_array[] = $found_name;
				}
			}
		}

		return $names_array;
	}

	public static function _ld_ajax() {
		$status = "error";
		$message = "Invalid form";
		$type = "security";
		$return_data = array (
			'status'  => $status,
			'type'    => $type,
			'message' => $message,
			'items'   => array (),
		);
		if ( ! isset( $_POST['values']['ld_'] ) && ! ( $_POST['values']['ld_'] ) ) {
			print json_encode( $return_data );
			exit;
		}

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {
			print json_encode( $return_data );
			exit;
		}

		$locality_data_array = PDGManageCache::fn_load_cache( 'registered_locality', true );
		$locality_data_filtered_array = array ();
		foreach ( $locality_data_array as $locality ) {
			$locality_data_filtered_array [] = array (
				'locality_id' => $locality['locality_id'],
				'locality'    => $locality['locality'],
			);
		}
		$return_data = array (
			'status'  => "success",
			'type'    => "none",
			'message' => "done",
			'items'   => $locality_data_filtered_array,
		);
		print json_encode( $return_data );
		exit;
	}

	public static function _sd_ajax() {
		$status = "error";
		$message = "Invalid form";
		$type = "security";
		$return_data = array (
			'status'  => $status,
			'type'    => $type,
			'message' => $message,
			'items'   => array (),
		);
		if ( ! isset( $_POST['values']['sd_'] ) && ! ( $_POST['values']['sd_'] ) ) {
			print json_encode( $return_data );
			exit;
		}

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {
			print json_encode( $return_data );
			exit;
		}

		$subject_data = PDGManageCache::fn_load_cache( 'registered_subjects' );
		$return_data = array (
			'status'  => "success",
			'type'    => "none",
			'message' => "done",
			'items'   => $subject_data,
		);
		print json_encode( $return_data );
		exit;
	}

	public static function _td_ajax() {
		$status = "error";
		$message = "Invalid form";
		$type = "security";
		$return_data = array (
			'status'  => $status,
			'type'    => $type,
			'message' => $message,
			'items'   => array (),
		);
		if ( ! isset( $_POST['values']['td_'] ) && ! ( $_POST['values']['td_'] ) ) {
			print json_encode( $return_data );
			exit;
		}

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {
			print json_encode( $return_data );
			exit;
		}

		$teacher_data_array = PDGManageCache::fn_load_cache( 'teacher_names_data', true );
		$teacher_data_filtered_array = array ();
		foreach ( $teacher_data_array as $teacher ) {
			if ( trim( $teacher ) !== "" ) {
				$teacher_data_filtered_array [] = array (
					'teacher' => $teacher,
				);
			}
		}
		$return_data = array (
			'status'  => "success",
			'type'    => "none",
			'message' => "done",
			'items'   => $teacher_data_filtered_array,
		);
		print json_encode( $return_data );
		exit;
	}

	public static function search_recommend_teacher_ajax_ver2() {
		global $wpdb;

		if ( isset( $_POST['recommendData'] ) ) {
			$recommend_data = $_POST['recommendData'];
		} else {
			die();
		}
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => ''
		);

		$recommended_by = $recommend_data['recommended_by'];
		$recommended_to = $recommend_data['recommended_to'];
		$has_recommended = $recommend_data['has_recommended'];

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( ! is_numeric( $recommended_by ) && ! is_numeric( $recommended_to ) ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Data was incomplete! Please try again.';

			echo json_encode( $return_message );
			die();
		}
		$str_sql = "delete from pdg_recommendations where recommended_by=$recommended_by and recommended_to=$recommended_to";
		$wpdb->get_results( $str_sql );
		switch ( $has_recommended ) {
			case 'no':
				//insert
				$insert_array = array (
					'recommended_by' => $recommended_by,
					'recommended_to' => $recommended_to
				);
				$wpdb->insert( 'pdg_recommendations', $insert_array, array ( '%d', '%d' ) );
				break;
			case 'yes':
				break;
			default:
				$return_message['error_type'] = 'data_incomplete';
				$return_message['message'] = 'Data was incomplete. Recommendation not provided! Please try again.';

				echo json_encode( $return_message );
				die();
				break;
		}

		/**
		 * Find Teacher Name Email
		 * Find User Name Email
		 * Create Profile Link
		 */
		$user_name = '';
		$user_email = '';

		$teacher_email = '';
		$teacher_name = '';
		$teacher_profile_link = '';

		$teacher_data = $wpdb->get_results( "select * from view_pdg_teachers_institutes where user_id=$recommended_to" );
		$user_data = $wpdb->get_results( "select * from view_pdg_user_info where user_id = $recommended_by" );
		if ( ! empty( $teacher_data ) && ! empty( $user_data ) ) {
			$user_role = '';
			$user_slug = '';
			foreach ( $teacher_data as $teacher_info ) {
				$teacher_email = $teacher_info->user_email;
				$teacher_name = $teacher_info->teacher_name;
				$user_role = $teacher_info->user_role_name;
				$user_slug = $teacher_info->profile_slug;
			}

			switch ( $user_role ) {
				case 'teacher':
					$teacher_profile_link = home_url( '/teacher/' . $user_slug );
					break;
				case 'institution':
					$teacher_profile_link = home_url( '/institute/' . $user_slug );
					break;
			}

			foreach ( $user_data as $user_info ) {
				$user_email = $user_info->user_email;
				$user_name = $user_info->first_name . ' ' . $user_info->last_name;
			}

			$mail_data = array (
				'to_address'                      => '',
				'mail_type'                       => '',
				'teacher_institute_name'          => $teacher_name,
				'teacher_institute_email_address' => $teacher_email,
				'teacher_type'                    => $user_role,
				'user_name'                       => $user_name,
				'user_email_address'              => $user_email,
				'teacher_institute_profile_link'  => $teacher_profile_link,
			);


			//send recommendation mail to teacher
			$mail_data['to_address'] = $teacher_email;
			$mail_data['mail_type'] = 'recommendation_mail_to_teacher';
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();

			//send recommendation mail to user
			$mail_data['to_address'] = $user_email;
			$mail_data['mail_type'] = 'recommendation_mail_to_user';
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();

			//send recommendation mail to admin
			$mail_data['to_address'] = 'ratings@pedagoge.com';
			$mail_data['mail_type'] = 'recommendation_mail_to_admin';
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();
		}

		$return_message['error'] = false;
		$return_message['message'] = 'Your recommendation was recorded successfully!';

		echo json_encode( $return_message );
		die();
	}

	public static function search_rating_and_review_ajax_ver2() {
		global $wpdb;
		if ( isset( $_POST['reviewData'] ) ) {
			$review_data = $_POST['reviewData'];
		} else {
			die();
		}
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => ''
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		$rated_by = $review_data['rated_by'];
		$rated_to = $review_data['rated_to'];
		$review_text = sanitize_text_field( trim( $review_data['review_text'] ) );
		//$average_rating = $review_data['average_rating'];
		$rating_data = $review_data['rating_data'];
		$overall_rating = $review_data['overall_rating'];
		$is_anonymous = $review_data['is_anonymous'];

		if ( ! is_numeric( $rated_by ) || $rated_by <= 0 ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Invalid information. Please reload the page and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( ! is_numeric( $rated_to ) || $rated_to <= 0 ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Invalid information. Please reload the page and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $rating_data ) || ! is_array( $rating_data ) ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Review information could not be updated. Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$is_rating_below_average = false;
		$should_process = true;
		$rating1 = $rating2 = $rating3 = $rating4 = 0;

		foreach ( $rating_data as $key => $ratings ) {
			$rating_name = $ratings['name'];
			$rating_value = $ratings['value'];
			if ( ! is_numeric( $rating_value ) ) {
				$rating_value = 0;
			}
			switch ( $rating_name ) {
				case 'star1':
					$rating1 = $rating_value;
					break;
				case 'star2':
					$rating2 = $rating_value;
					break;
				case 'star3':
					$rating3 = $rating_value;
					break;
				case 'star4':
					$rating4 = $rating_value;
					break;
			}
		}

		if ( $overall_rating >= 3.5 ) {
			$should_process = true;
			$is_rating_below_average = false;
		}


		$total_rating = $rating1 + $rating2 + $rating3 + $rating4;
		$average_rating = 0;
		if ( $total_rating > 0 ) {
			$average_rating = round( ( $total_rating / 4 ), 1 );
		}

		if ( ! $should_process ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Incomplete data. Please try again!';

			echo json_encode( $return_message );
			die();
		}

		if ( $is_rating_below_average && strlen( $review_text ) < 50 ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'You need to write a valid reason as to why you have rated below 3.5.';

			echo json_encode( $return_message );
			die();
		}

		/**
		 * Get User Name and Email
		 * Get Teacher Name and Email
		 */

		$teacher_sql = "
			select
				view_pdg_user_info.user_email,
				view_pdg_user_info.user_role_display,
				view_pdg_user_info.mobile_no,
				view_teacher_institute_combined_names.teacher_name
			from view_pdg_user_info
			left join view_teacher_institute_combined_names on view_pdg_user_info.user_id = view_teacher_institute_combined_names.user_id
			where view_pdg_user_info.user_id = $rated_to
		";

		$student_sql = "
			select
				view_pdg_user_info.user_email,
				view_pdg_user_info.user_role_display,
				view_pdg_user_info.mobile_no,
				view_pdg_user_info.first_name,
				view_pdg_user_info.last_name
			from view_pdg_user_info where user_id = $rated_by
		";

		$teacher_data = $wpdb->get_results( $teacher_sql );
		$student_data = $wpdb->get_results( $student_sql );

		if ( empty( $teacher_data ) || empty( $student_data ) ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = "Invalid teacher information. Please try again.";

			echo json_encode( $return_message );
			die();
		}

		$str_sql = "delete from pdg_reviews where reviewer_user_id=$rated_by and tutor_institute_user_id=$rated_to";
		$wpdb->get_results( $str_sql );

		$security_key = md5( time() );

		$insert_array = array (
			'review_text'             => $review_text,
			'reviewer_user_id'        => $rated_by,
			'tutor_institute_user_id' => $rated_to,
			'rating1'                 => $rating1,
			'rating2'                 => $rating2,
			'rating3'                 => $rating3,
			'rating4'                 => $rating4,
			'average_rating'          => $average_rating,
			'overall_rating'          => $overall_rating,
			'is_anonymous'            => $is_anonymous,
			'security_key'            => $security_key
		);
		$format_array = array (
			'%s',
			'%d',
			'%d',
			'%f',
			'%f',
			'%f',
			'%f',
			'%f',
			'%f',
			'%s',
			'%s',
		);
		$wpdb->insert( 'pdg_reviews', $insert_array, $format_array );
		$review_id = $wpdb->insert_id;
		if ( is_numeric( $review_id ) && $review_id > 0 ) {
			//review_submission_to_admin

			$mail_data = array (
				'to_address'   => 'ratings@pedagoge.com',
				'mail_type'    => 'review_submission_to_admin',
				'review_data'  => $insert_array,
				'teacher_data' => $teacher_data,
				'student_data' => $student_data,
				'review_id'    => $review_id,
			);
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$mail_process_completed = null;

			$reviewer_email = '';
			foreach ( $student_data as $student_info ) {
				$reviewer_email = $student_info->user_email;
			}

			//review_submission_notification_to_reviewer
			$mail_data = array (
				'to_address' => $reviewer_email,
				'mail_type'  => 'review_submission_notification_to_reviewer',
			);
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$mail_process_completed = null;

			$return_message['error'] = false;
			$return_message['message'] = 'Great! Your review was submitted successfully! It will be plublished after verification!';
			//$return_message['data'] = $total_review_count;

			echo json_encode( $return_message );
			die();
		}

		$return_message['error_type'] = 'data_not_saved';
		$return_message['message'] = 'Your review could not be saved. Please try again.';

		echo json_encode( $return_message );
		die();

	}

	public static function fn_load_subjects_localities() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => ''
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! Security Code was not correct! Please try again.';

			echo json_encode( $return_message );
			die();
		}


		$subject_data = PDGManageCache::fn_load_cache( 'pdg_subject_name' );
		$locality_data = PDGManageCache::fn_load_cache( 'pdg_locality' );
		$teacher_names_data = PDGManageCache::fn_load_cache( 'teacher_names_data', true );

		$localities_array = array ();
		foreach ( $locality_data as $found_locality ) {
			$locality_name = $found_locality->locality;
			$locality_id = $found_locality->locality_id;
			$localities_array[] = array (
				'id'   => $locality_id,
				'text' => $locality_name
			);
		}

		$subjects_array = array ();
		foreach ( $subject_data as $subject ) {
			$subject_name = $subject->subject_name;
			if ( ! in_array( $subject_name, $subjects_array ) ) {
				$subjects_array[] = $subject_name;
			}
		}

		$return_message['error'] = false;

		$return_message['message'] = 'Data Loaded';
		$return_message['data'] = array (
			'subjects'      => $subjects_array,
			'locality'      => $localities_array,
			'teacher_names' => $teacher_names_data,
		);

		echo json_encode( $return_message );
		die();
	}

	/**
	 * Author - Niraj; Created 12-Dec-2016
	 * Modified - Ganesh; Modified 13-Dec-2016
	 * Optimized: inputted locality is now checked to see if only numerical values are passed.
	 */
	public static function fn_search_clubbed_locality_ajax() {
		/**
		 * Get localities
		 */
		global $wpdb;
		if ( isset( $_POST['searchData'] ) ) {
			$search_data = $_POST['searchData'];
		} else {
			die();
		}
		$no_of_records_per_page = isset( $search_data['no_of_records_per_page'] ) && ! is_array( $search_data['no_of_records_per_page'] ) && is_numeric( $search_data['no_of_records_per_page'] ) && $search_data['no_of_records_per_page'] < 64 ? (int) sanitize_text_field( $search_data['no_of_records_per_page'] ) : 16;
		$loggedin_user_id = isset( $search_data['current_user_id'] ) && ! is_array( $search_data['current_user_id'] ) && is_numeric( $search_data['current_user_id'] ) ? (int) sanitize_text_field( $search_data['current_user_id'] ) : 0;
		$loggedin_user_role = isset( $search_data['current_user_role'] ) && ! is_array( $search_data['current_user_role'] ) ? sanitize_text_field( $search_data['current_user_role'] ) : '';
		$subject_name = isset( $search_data['subject_name'] ) && ! is_array( $search_data['subject_name'] ) ? sanitize_text_field( trim( $search_data['subject_name'] ) ) : '';
		$locality_names = isset( $search_data['locality_names'] ) && ! is_array( $search_data['locality_names'] ) ? sanitize_text_field( trim( $search_data['locality_names'] ) ) : '';
		$institute_teacher_name = isset( $search_data['institute_teacher_name'] ) && ! is_array( $search_data['institute_teacher_name'] ) ? sanitize_text_field( trim( $search_data['institute_teacher_name'] ) ) : '';
		$pagination_no = isset( $search_data['pagination_no'] ) && ! is_array( $search_data['pagination_no'] ) && is_numeric( $search_data['pagination_no'] ) && $search_data['pagination_no'] >= 0 ? sanitize_text_field( (int) $search_data['pagination_no'] ) : 0;
		//todo: page no start with 0 here and in the prev ver with 1
		$search_type = isset( $search_data['search_type'] ) && ! is_array( $search_data['search_type'] ) ? sanitize_text_field( $search_data['search_type'] ) : 'filter';

		$filter_institute_teacher_array = isset( $search_data['filter_institute_teacher_array'] ) && is_array( $search_data['filter_institute_teacher_array'] ) && isset( $search_data['filter_institute_teacher_array'][0] ) ? $search_data['filter_institute_teacher_array'] : array ( 0 => '' );
		$location_type_array = isset( $search_data['location_type_array'] ) && is_array( $search_data['location_type_array'] ) ? $search_data['location_type_array'] : array ();
		$class_type_array = isset( $search_data['class_type_array'] ) && is_array( $search_data['class_type_array'] ) ? $search_data['class_type_array'] : array ();
		$teaching_xp_array = isset( $search_data['teaching_xp_array'] ) && is_array( $search_data['teaching_xp_array'] ) ? $search_data['teaching_xp_array'] : array ();
		$academic_board_array = isset( $search_data['academic_board_array'] ) && is_array( $search_data['academic_board_array'] ) ? $search_data['academic_board_array'] : array ();
		$fees_range_array = isset( $search_data['fees_range_array'] ) && is_array( $search_data['fees_range_array'] ) ? $search_data['fees_range_array'] : array ();
		$days_taught_array = isset( $search_data['days_taught_array'] ) && is_array( $search_data['days_taught_array'] ) ? $search_data['days_taught_array'] : array ();
		$sort_exp_array = isset( $search_data['sort_exp_array'] ) && is_array( $search_data['sort_exp_array'] ) ? $search_data['sort_exp_array'] : array ();
		$sort_reviews_array = isset( $search_data['sort_reviews_array'] ) && is_array( $search_data['sort_reviews_array'] ) ? $search_data['sort_reviews_array'] : array ();
		$sort_fee_array = isset( $search_data['sort_fee_array'] ) && is_array( $search_data['sort_fee_array'] ) ? $search_data['sort_fee_array'] : array ();


		$locality_names = trim( trim( $locality_names ), ',' );
		$searched_locality_id_array = explode( ',', $locality_names );

		$filtered_searched_locality_id_array = array ();
		if ( count( $searched_locality_id_array ) > 0 ) {
			$filtered_searched_locality_id_array = array_filter( $searched_locality_id_array, array (
				__CLASS__,
				'is_numeric_callback'
			) );
		}

		if ( count( $filtered_searched_locality_id_array ) <= 0 ) {
			$status = "success";
			$message = "No locality available.";
			$type = "empty_locality_input_clubbed_locality";
			$return_data = array (
				'status'  => $status,
				'type'    => $type,
				'message' => $message,
				'items'   => ''
			);

			print ( json_encode( $return_data ) );
			exit;
		}

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {
			$status = "error";
			$message = "Invalid form";
			$type = "security";
			$return_data = array (
				'status'  => $status,
				'type'    => $type,
				'message' => $message,
				'items'   => ''
			);
			print ( json_encode( $return_data ) );
			exit;
		}
		$srt_filtered_searched_locality_id_array = implode( ', ', $filtered_searched_locality_id_array );
		$str_sql = "select * from pdg_clubbed_locality where master_locality_id in ($srt_filtered_searched_locality_id_array) and child_locality_id not in ($srt_filtered_searched_locality_id_array)";

		$clubbed_locality_array = array ();
		$clubbed_locality_db = $wpdb->get_results( $str_sql );
		if ( empty( $clubbed_locality_db ) ) {
			$status = "success";
			$message = "No suggestions available.";
			$type = "empty_clubbed_locality";
			$return_data = array (
				'status'  => $status,
				'type'    => $type,
				'message' => $message,
				'items'   => ''
			);

			print ( json_encode( $return_data ) );
			exit;
		}

		foreach ( $clubbed_locality_db as $clubbed_locality ) {
			$locality_id = $clubbed_locality->child_locality_id;
			if ( ! in_array( $locality_id, $clubbed_locality_array ) ) {
				$clubbed_locality_array[] = $locality_id;
			}
		}

		$str_clubbed_locality = implode( ',', $clubbed_locality_array );
		$search_data = array (
			'subject_name'                   => $subject_name,
			'found_locality_name'            => '',
			'institute_teacher_name'         => $institute_teacher_name,
			'class_timing_array'             => array (),
			'locality_names'                 => $str_clubbed_locality,
			'location_type_array'            => $location_type_array,
			'search_type'                    => $search_type,
			'loggedin_user_id'               => $loggedin_user_id,
			'loggedin_user_role'             => $loggedin_user_role,
			'filter_institute_teacher_array' => $filter_institute_teacher_array,
			'teaching_xp_array'              => $teaching_xp_array,
			'class_type_array'               => $class_type_array,
			'academic_board_array'           => $academic_board_array,
			'fees_range_array'               => $fees_range_array,
			'days_taught_array'              => $days_taught_array,
			'sort_exp_array'                 => $sort_exp_array,
			'sort_reviews_array'             => $sort_reviews_array,
			'sort_fee_array'                 => $sort_fee_array,
			'pagination_no'                  => 0,
			'no_of_records_per_page'         => $no_of_records_per_page,
		);

		$return_data = self::fn_return_search_content( $search_data );
		$return_data['type'] = 'clubbed_locality_end';
		print ( json_encode( $return_data ) );
		exit;
	}

	public static function search_recommend_teacher_ajax() {
		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);

		$recommended_by = $_POST['recommended_by'];
		$recommended_to = $_POST['recommended_to'];
		$has_recommended = $_POST['has_recommended'];

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( ! is_numeric( $recommended_by ) && ! is_numeric( $recommended_to ) ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! Data was incomplete! Please try again.';

			echo json_encode( $return_message );
			die();
		}
		$str_sql = "delete from pdg_recommendations where recommended_by=$recommended_by and recommended_to=$recommended_to";
		$wpdb->get_results( $str_sql );
		switch ( $has_recommended ) {
			case 'no':
				//insert
				$insert_array = array (
					'recommended_by' => $recommended_by,
					'recommended_to' => $recommended_to
				);
				$wpdb->insert( 'pdg_recommendations', $insert_array, array ( '%d', '%d' ) );
				break;
			case 'yes':
				break;
			default:
				$return_message['error_type'] = 'data_incomplete';
				$return_message['message'] = 'Error! Data was incomplete. Recommendation not provided! Please try again.';

				echo json_encode( $return_message );
				die();
				break;
		}

		$return_message['error'] = false;
		$return_message['message'] = 'Success! Your recommendation was recorded successfully!';

		echo json_encode( $return_message );
		die();
	}

	public static function search_rating_and_review_ajax() {
		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => 'Error message!',
			'data'       => ''
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		$rated_by = $_POST['rated_by'];
		$rated_to = $_POST['rated_to'];
		$review_text = sanitize_text_field( $_POST['review_text'] );
		//$average_rating = $_POST['average_rating'];
		$rating_data = $_POST['rating_data'];
		$overall_rating = $_POST['overall_rating'];
		$is_anonymous = $_POST['is_anonymous'];

		if ( ! is_numeric( $rated_by ) || $rated_by <= 0 ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! Your profile information could not be verified. Please reload the page and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( ! is_numeric( $rated_to ) || $rated_to <= 0 ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! Teacher/Institute profile information could not be verified. Please reload the page and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $rating_data ) || ! is_array( $rating_data ) ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! Review information could not be updated. Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$is_rating_below_average = false;
		$should_process = true;
		$rating1 = $rating2 = $rating3 = $rating4 = 0;

		foreach ( $rating_data as $key => $ratings ) {
			$rating_name = $ratings['name'];
			$rating_value = $ratings['value'];
			if ( ! is_numeric( $rating_value ) ) {
				$rating_value = 0;
			}
			switch ( $rating_name ) {
				case 'star1':
					$rating1 = $rating_value;
					break;
				case 'star2':
					$rating2 = $rating_value;
					break;
				case 'star3':
					$rating3 = $rating_value;
					break;
				case 'star4':
					$rating4 = $rating_value;
					break;
			}
		}

		if ( $overall_rating >= 3.5 ) {
			$should_process = true;
			$is_rating_below_average = false;
		}


		$total_rating = $rating1 + $rating2 + $rating3 + $rating4;
		$average_rating = 0;
		if ( $total_rating > 0 ) {
			$average_rating = round( ( $total_rating / 4 ), 1 );
		}

		if ( ! $should_process ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! Incomplete data. Please try again!';

			echo json_encode( $return_message );
			die();
		}

		if ( $is_rating_below_average && strlen( $review_text ) < 10 ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! You need to write a valid reason as to why you have rated below 3.5.';

			echo json_encode( $return_message );
			die();
		}

		/**
		 * Get User Name and Email
		 * Get Teacher Name and Email
		 */

		$teacher_sql = "
			select
				view_pdg_user_info.user_email,
				view_pdg_user_info.user_role_display,
				view_pdg_user_info.mobile_no,
				view_teacher_institute_combined_names.teacher_name
			from view_pdg_user_info
			left join view_teacher_institute_combined_names on view_pdg_user_info.user_id = view_teacher_institute_combined_names.user_id
			where view_pdg_user_info.user_id = $rated_to
		";

		$student_sql = "
			select
				view_pdg_user_info.user_email,
				view_pdg_user_info.user_role_display,
				view_pdg_user_info.mobile_no,
				view_pdg_user_info.first_name,
				view_pdg_user_info.last_name
			from view_pdg_user_info where user_id = $rated_by
		";

		$teacher_data = $wpdb->get_results( $teacher_sql );
		$student_data = $wpdb->get_results( $student_sql );

		if ( empty( $teacher_data ) || empty( $student_data ) ) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = "Error! Your information or Teacher's Information was not available in the database. Please try again.";

			echo json_encode( $return_message );
			die();
		}

		$str_sql = "delete from pdg_reviews where reviewer_user_id=$rated_by and tutor_institute_user_id=$rated_to";
		$wpdb->get_results( $str_sql );

		$security_key = md5( time() );

		$insert_array = array (
			'review_text'             => $review_text,
			'reviewer_user_id'        => $rated_by,
			'tutor_institute_user_id' => $rated_to,
			'rating1'                 => $rating1,
			'rating2'                 => $rating2,
			'rating3'                 => $rating3,
			'rating4'                 => $rating4,
			'average_rating'          => $average_rating,
			'overall_rating'          => $overall_rating,
			'is_anonymous'            => $is_anonymous,
			'security_key'            => $security_key
		);
		$format_array = array (
			'%s',
			'%d',
			'%d',
			'%f',
			'%f',
			'%f',
			'%f',
			'%f',
			'%f',
			'%s',
			'%s',
		);
		$wpdb->insert( 'pdg_reviews', $insert_array, $format_array );
		$review_id = $wpdb->insert_id;
		if ( is_numeric( $review_id ) && $review_id > 0 ) {

			//review_submission_to_admin

			$mail_data = array (
				'to_address'   => 'ratings@pedagoge.com',
				'mail_type'    => 'review_submission_to_admin',
				'review_data'  => $insert_array,
				'teacher_data' => $teacher_data,
				'student_data' => $student_data,
				'review_id'    => $review_id,
			);
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$mail_process_completed = null;

			$reviewer_email = '';
			foreach ( $student_data as $student_info ) {
				$reviewer_email = $student_info->user_email;
			}

			//review_submission_notification_to_reviewer
			$mail_data = array (
				'to_address' => $reviewer_email,
				'mail_type'  => 'review_submission_notification_to_reviewer',
			);
			$pedagoge_mailer = new PedagogeMailer( $mail_data );
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$mail_process_completed = null;

			$return_message['error'] = false;
			$return_message['message'] = 'Great! Your review was submitted successfully! It will be plublished after verification!';
			//$return_message['data'] = $total_review_count;

			echo json_encode( $return_message );
			die();
		}

		$return_message['error_type'] = 'data_not_saved';
		$return_message['message'] = 'Error! Your review could not be saved. Please try again.';

		echo json_encode( $return_message );
		die();

	}

	public static function show_interest_ajax() {

		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! Security Code was not correct! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$txt_modal_phone = sanitize_text_field( $_POST['modal_phone'] );
		$txt_modal_user_name = sanitize_text_field( $_POST['modal_user_name'] );
		$txt_modal_locality = sanitize_text_field( $_POST['modal_locality'] );
		$txt_modal_request = sanitize_text_field( $_POST['modal_request'] );


		if ( empty( $txt_modal_phone ) || empty( $txt_modal_locality ) ) {

			$return_message['message'] = '<h3>Please fill the form correctly and try again!</h3>';
			echo json_encode( $return_message );
			die();
		}

		$template_vars['modal_user_name'] = $txt_modal_user_name;
		$template_vars['modal_phone'] = $txt_modal_phone;
		$template_vars['modal_locality'] = $txt_modal_locality;
		$template_vars['modal_request'] = $txt_modal_request;
		$template_vars['modal_email'] = '';
		$template_vars['modal_name'] = '';

		$show_interest_header = PDGViewLoader::load_view( 'email_templates/email_header' );
		$show_interest_content = PDGViewLoader::load_view( 'email_templates/show_interest', $template_vars );
		$show_interest_footer = PDGViewLoader::load_view( 'email_templates/email_footer' );
		$show_interest_content = $show_interest_header . $show_interest_content . $show_interest_footer;


		$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
		$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

		add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html"; ' ) );
		wp_mail( 'hello@pedagoge.com', 'Pedagoge No search found Mail', $show_interest_content, $headers );


		$return_message['error'] = false;
		$return_message['message'] = '<h3>Your message was sent successfully!</h3>';


		echo json_encode( $return_message );
		die();

	}

	public static function fn_find_teacher_name_ajax() {

		$data = PDGManageCache::fn_load_cache( 'teacher_names_data', true );

		$input = preg_quote( $search_term, '~' ); // don't forget to quote input string!

		//$result = preg_filter('~' . $input . '~', '$0', $data);
		$result = preg_grep( '~' . $input . '~', $data );

		echo json_encode( $names_array );
		die();
	}
}
