<?php
session_start();
$_SESSION['member_id']='';
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerLogin extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Login';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		
		$file_version = '0.8';

		parent::fn_themes_ver2_load_scripts();
		$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine_en'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->footer_js['blockui'] = BOWER_ROOT_URL . "/blockUI/jquery.blockUI.js?ver=" . $this->fixed_version;
		$this->app_js['sha512'] = PEDAGOGE_ASSETS_URL . "/js/ankitesh/sha512.js?ver=" . $this->fixed_version . "." . $file_version;
		$this->app_js['pedagoge_users_login'] = PEDAGOGE_ASSETS_URL . "/js/user_login.js?ver=" . $this->fixed_version . "." . $file_version;
		$this->app_js['social_login'] = PEDAGOGE_ASSETS_URL . "/js/social_login.js?ver=" . $this->fixed_version . "." . $file_version;

		$this->app_css['ver2_login'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/login.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css?ver=' . $this->fixed_version . "." . $file_version;
	}

	public function fn_get_header() {
		return $this->load_view( 'ver2/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'ver2/footer' );
	}

	public function fn_get_content() {
		/**
		 * 1. if account is not activated then ask him to activate the account.
		 * 2. in account activation have option to send activation link.
		 */
		$user_roles_model = new ModelUserRole();
		$user_role = $user_roles_model->get_public_roles();
		$template_vars['user_role'] = $user_role;

		if ( is_user_logged_in() ) {

			$content = '
				<h1>You are already logged in!</h1>
				<p><strong>Please wait.. redirecting...</strong></p>
				<!--<META http-equiv="refresh" content="3; URL=' . home_url( '/profile' ) . '">-->
			';

		} else {
			$content = $this->load_view( 'login/ver2/index', $template_vars );
		}

		return $content;
	}

	/**
	 * Function to register common variables to global application variables registry
	 */
	private function fn_register_common_variables() {
		if ( isset( $_GET['nexturl'] ) ) {
			$last_URL = $_GET['nexturl'];
		} else {
			$last_URL = '/';
		}
		$this->app_data['nexturl'] = $last_URL;
	}

	public static function user_login_ajax() {
		global $wpdb;
		$home_url = home_url();

		$return_message = array (
			'error'      => true,
			'loggedin'   => false,
			'error_type' => '',
			'message'    => '',
			'url'        => $home_url,
		);

		$user_name = sanitize_user( $_POST['user_name'] );
		$user_pass = sanitize_text_field( $_POST['password'] );
		$remember = $_POST['remember'];
		$hidden_hashed_password = $_POST['hidden_hashed_password'];
		$nexturl = sanitize_text_field( $_POST['nexturl'] );

		$server_name = $_SERVER['SERVER_NAME'];
		$redirect_to = '';
		if ( $server_name == 'www.pedagoge.com' ) {
			$redirect_to = $nexturl;
		} else {
			//$redirect_to = substr( $nexturl, 9 );
			$redirect_to = $nexturl;
		}


		$is_email = false;

		switch ( $remember ) {
			case 'true':
				$remember = true;

				break;
			case 'false':
				$remember = false;

				break;
			default:
				$remember = true;

				break;
		}

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		/**
		 * check if username or useremail address is available or not.
		 * 1. get user object by login/username
		 * 2. if not available then get user object by email address
		 * 3. if not available then throw error.
		 * 4. if user found then extract his username and authenticate
		 */

		$found_user = '';
		if ( ! filter_var( $user_name, FILTER_VALIDATE_EMAIL ) === false ) {
			$is_email = true;
			$found_user = get_user_by( 'email', $user_name );
		} else {
			$found_user = get_user_by( 'login', $user_name );
		}


		if ( ! is_a( $found_user, 'WP_User' ) ) {
			//throw wrong username/email error
			$return_message['error_type'] = 'wrong_username';
			$return_message['message'] = 'Error! Wrong username/email! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$user_login = $found_user->user_login;
		$found_user_id = $found_user->ID;

		$credentials = array ();
		$credentials['user_login'] = $user_login;
		$credentials['user_password'] = $user_pass;
		$credentials['remember'] = $remember;

		$user_signon = wp_signon( $credentials, false );

		if ( is_wp_error( $user_signon ) ) {

			/**
			 * 1. Check if it is an email
			 * 2. Check old DB and find user by email
			 * 3. get all data and hash new password
			 * 4. if it matches then login
			 */
			if ( $is_email ) {

				if ( ! is_numeric( $found_user_id ) || $found_user_id <= 0 ) {
					$return_message['error_type'] = 'wrong_password';
					$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

					$return_message['message'] = '
						Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
						Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
					';

					echo json_encode( $return_message );
					die();
				}

				$str_sql = "select * from members where email = '$user_name'";

				$old_member_data = $wpdb->get_results( $str_sql );
				if ( empty( $old_member_data ) ) {

					$return_message['error_type'] = 'wrong_password';
					$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

					$return_message['message'] = '
						Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
						Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
					';
					echo json_encode( $return_message );
					die();
				} else {

					$old_password = '';
					$old_salt = '';
					$is_password_reset = '';
					foreach ( $old_member_data as $member_data ) {
						$old_password = $member_data->password;
						$old_salt = $member_data->salt;
						$is_password_reset = $member_data->password_reset;
					}

					$old_salted_password = hash( 'sha512', $hidden_hashed_password . $old_salt );

					if ( $old_password == $old_salted_password ) {
						/**
						 * Login now and redirect to password reset
						 */

						wp_set_auth_cookie( $found_user_id, $remember );
						wp_set_current_user( $found_user_id );

						$return_message['url'] = home_url( '/profile' );

						$return_message['loggedin'] = true;
						$return_message['error'] = false;
						$return_message['message'] = 'Login Successful! Please wait... redirecting!';
						echo json_encode( $return_message );
						die();


					} else {
						$return_message['error_type'] = 'wrong_password';
						$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

						$return_message['message'] = '
							Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
							Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
						';
						echo json_encode( $return_message );
						die();
					}

				}
			} else {

				$return_message['error_type'] = 'wrong_password';
				$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

				$return_message['message'] = '
					Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
					Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
				';
				echo json_encode( $return_message );
				die();
			}


		} else {
			wp_set_current_user( $user_signon->ID );
			$loggedin_userid = $user_signon->ID;
			if ( ! empty( $redirect_to ) ) {
				$redirect_to = home_url( $redirect_to );
			}

			/**
			 * Find user role
			 */


			$teacher_data = $wpdb->get_results( "select * from pdg_teacher where user_id = $loggedin_userid" );
			$student_data = $wpdb->get_results( "select * from pdg_student where user_id = $loggedin_userid" );
			if ( empty( $teacher_data )  && empty( $student_data )) {
				$institute_data = $wpdb->get_results( "select * from pdg_institutes where user_id = $loggedin_userid" );
				if ( ! empty( $institute_data ) ) {
					$is_active = '';
					$is_approved = '';
					$profile_slug = '';
					foreach ( $institute_data as $institute_info ) {
						$is_active = $institute_info->active;
						$is_approved = $institute_info->approved;
						$profile_slug = $institute_info->profile_slug;
					}
					if ( $is_active == 'no' || $is_approved == 'no' ) {
						$redirect_to = home_url( '/profile' );
					} else {
						$redirect_to = home_url( '/institute/' . $profile_slug );
					}
				}
			}
			//elseif(){
			elseif ( !empty( $student_data ) ) {
				$_SESSION['member_id']=$loggedin_userid;
				if($nexturl != "/")
				{
					$redirect_to = home_url($nexturl);
				}
				else
				{
					$redirect_to = home_url( '/memberdashboard/?query_type=active' );
				}
				//$redirect_to = home_url( '/memberdashboard/?query_type=active' );
					
			}
			else {
				$is_active = '';
				$is_approved = '';
				$profile_slug = '';
				$_SESSION['member_id']=$loggedin_userid;
				foreach ( $teacher_data as $teacher_info ) {
					$is_active = $teacher_info->active;
					$is_approved = $teacher_info->approved;
					$profile_slug = $teacher_info->profile_slug;
				}
				if ( $is_active == 'no' || $is_approved == 'no' ) {
					$redirect_to = home_url( '/profile' );
				} else {
					if ( $_POST['nexturl']!='/' ) {
				         $redirect_to =  home_url( $_POST['nexturl']);
			                }
					else{
					$redirect_to = home_url( '/teacher/' . $profile_slug );
					}
					
				}
			}
			//if ( empty( $redirect_to ) ) {
			//	
			//	
			//}
			$return_message['url'] = $redirect_to;
			$return_message['loggedin'] = true;
			$return_message['error'] = false;
			$return_message['message'] = 'Login Successful! Please wait... redirecting!';

			echo json_encode( $return_message );
			die();
		}

		die();
	}

	function fn_social_login() {
		global $wpdb;
		$home_url = home_url( '/login' );

		$return_message = array (
			'error'      => true,
			'loggedin'   => false,
			'error_type' => '',
			'message'    => '',
			'url'        => $home_url,
		);

		$social_user_id = sanitize_text_field( $_POST['social_user_id'] );
		$social_user_first_name = sanitize_text_field( $_POST['social_user_first_name'] );
		$social_user_last_name = sanitize_text_field( $_POST['social_user_last_name'] );
		$social_user_full_name = sanitize_text_field( $_POST['social_user_full_name'] );
		$social_user_email = sanitize_user( $_POST['social_user_email'] );

		$social_login_location = sanitize_text_field( $_POST['social_login_location'] );

		$social_location = 'NA';
		switch ( $social_login_location ) {
			case 'google':
			case 'facebook':
				$social_location = $social_login_location;
				break;
			default:
				$social_location = 'NA';
				break;
		}

		$remember = true;

		/**
		 * check if username or useremail address is available or not.
		 * 1. get user object by login/username
		 * 2. if not available then get user object by email address
		 * 3. if not available then throw error.
		 * 4. if user found then extract his username and authenticate
		 */

		$found_user = '';
		if ( ! filter_var( $social_user_email, FILTER_VALIDATE_EMAIL ) === false ) {
			$found_user = get_user_by( 'email', $social_user_email );
		} else {
			// false error - email is not valid			
			$return_message['message'] = 'Email address is not valid. Please try again.';
			echo json_encode( $return_message );
			die();
		}

		if ( ! is_a( $found_user, 'WP_User' ) ) {
			//  create the user

			$userdata = array (
				'user_login' => $social_user_id,
				'user_email' => $social_user_email,
				'role'       => 'subscriber',
				'user_pass'  => $social_user_id
			);

			$saved_user_id = wp_insert_user( $userdata );

			if ( ! is_wp_error( $saved_user_id ) && $saved_user_id != 0 ) {

				//updated first/last name
				$user_data = array (
					'ID'           => $saved_user_id,
					'display_name' => $social_user_full_name,
					'first_name'   => $social_user_first_name,
					'last_name'    => $social_user_last_name,
				);
				wp_update_user( $user_data );

				$social_login_data = array (
					'user_id'         => $saved_user_id,
					'social_location' => $social_location
				);
				ControllerSignup::social_login_save_info($social_login_data);

				$str_save_user_msg = 'Success! Your information was registered successfully! <br/> Please wait logging you in!';
				$user_roles_model = new ModelUserRole();
				$user_roles = $user_roles_model->get_public_roles();
				$str_user_roles = '';

				foreach ( $user_roles as $user_role ) {
					$str_user_roles .= '<option value="' . $user_role->user_role_name . '">' . $user_role->user_role_display . '</option>';
				}

				$hidden_user_email = wp_hash_password( $social_user_email );
				$str_signup_form = '
						<div class="col-xs-12 margin-top-10" id="div_continue_user"></div>
						<div class="form-group no-margin">
							<div class="col-xs-12">
								<input type="text" maxlength="10" name="txt_social_login_mobile_number" id="txt_social_login_mobile_number" class="form-control txt_social_login_mobile_number positive" placeholder="10 Digits Mobile Number" />
								<select type="role" name="select_user_role" id="select_social_user_role" class="form-control">
									<option selected="selected" disabled value="">Sign Up as</option>
									' . $str_user_roles . '
								</select>
								<button class="btn white-text text-bold btn-lg btn-block cmd_continue_user dark-green-btn-shade" id="cmd_continue_user" data-loading-text="Updating profile...">Continue</button>
								<input type="hidden" id="social_login_user_secred_key" value="' . $hidden_user_email . '" />
								<input type="hidden" id="social_login_user_id" value="' . $saved_user_id . '" />
							</div>
						</div>';

				$return_message['url'] = home_url( '/' );
				$return_message['error'] = false;
				$return_message['message'] = $str_save_user_msg;
				$return_message['signup'] = true;
				$return_message['signup_form'] = $str_signup_form;

				$user_info = array ();
				$user_info['user_login'] = $social_user_id;
				$user_info['user_password'] = $social_user_id;
				$user_info['remember'] = 'true';

				$user_signon = wp_signon( $user_info, false );

				//Send Signup Notification				
				$new_mail_data = array (
					'to_address' => 'ask@pedagoge.com',
					'mail_type'  => 'user_signup_and_account_activation_notice',
					'user_name'  => $social_user_id . ' (' . $social_user_full_name . ') - Via Social Login (' . $social_location . ')',
					'user_email' => $social_user_email
				);
				$pedagoge_mailer = new PedagogeMailer( $new_mail_data );
				$mail_process_completed = $pedagoge_mailer->sendmail();
				$pedagoge_mailer = null;

				echo json_encode( $return_message );
				die();

			} else {

				$error_msg = '';
				if ( is_object( $saved_user_id ) ) {
					$error_msg = $saved_user_id->get_error_message();
				}

				$str_save_user_msg = 'Error registering your details! Please try again!';

				$return_message['message'] = $str_save_user_msg . $error_msg;
				echo json_encode( $return_message );
				die();
			}

		} else {
			// login	
			$found_user_id = $found_user->ID;
			wp_set_auth_cookie( $found_user_id, $remember );
			wp_set_current_user( $found_user_id );

			$return_message['url'] = home_url( '/' );
			$return_message['loggedin'] = true;
			$return_message['error'] = false;
			$return_message['message'] = 'Login Successful! Please wait... redirecting!';
			echo json_encode( $return_message );
			die();
		}
		die();
	}

	public static function fn_social_signup() {
		global $wpdb;
		$home_url = home_url( '/profile' );

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'url'        => $home_url,
		);

		$social_user_role = sanitize_text_field( $_POST['social_user_role'] );
		$social_user_id = sanitize_text_field( $_POST['social_login_user_id'] );
		$social_user_secret = sanitize_text_field( $_POST['social_login_user_secred_key'] );
		
		$mobile_number = sanitize_text_field( $_POST['mobile_number'] );
		if(!is_numeric($mobile_number) && strlen($mobile_number) != 10) {
			$return_message['error_type'] = 'empty_mobile_no';
			$return_message['message'] = 'Please input your 10 digit mobile number.';

			echo json_encode( $return_message );
			die();
		}
		
		/**
		 * Check for duplicate phone number
		 */
		$str_duplicate_mobile_sql = "select user_id from pdg_user_info where mobile_no like '$mobile_number';";
		$duplicate_mobile_no = $wpdb->get_results($str_duplicate_mobile_sql);
		if(!empty($duplicate_mobile_no)) {
			$return_message['message'] = 'Error! This mobile number is already registered! Please input another mobile no and try again.';
			echo json_encode( $return_message );
			die();
		}

		$wp_user = get_user_by( 'id', $social_user_id );
		$users_email = $wp_user->user_email;
		require_once( ABSPATH . 'wp-includes/class-phpass.php' );
		$wp_hasher = new PasswordHash( 8, true );

		if ( ! $wp_hasher->CheckPassword( $users_email, $social_user_secret ) ) {
			$return_message['error_type'] = 'secret_unmatched';
			$return_message['message'] = 'Error! User\'s secret do no match! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		switch ( $social_user_role ) {
			case 'teacher':
			case 'institution':
			case 'student':
			case 'guardian':
				break;
			default:
				$return_message['error_type'] = 'user_role';
				$return_message['message'] = 'Error! You must select a user role. Please try again.';

				echo json_encode( $return_message );
				die();
				break;
		}

		//validate user

		$user_role_id = '';

		$user_role_columns_array = array (
			'user_role_name' => $social_user_role,
			'deleted'        => 'no'
		);

		$master_model = new ModelMaster( 'pdg_user_role' );
		$user_role_data = $master_model->get_records_by_table_name( 'pdg_user_role', $user_role_columns_array );

		if ( ! empty( $user_role_data ) ) {
			foreach ( $user_role_data as $user_role_info ) {
				$user_role_id = $user_role_info->user_role_id;
			}
		}

		wp_update_user( array ( 'ID' => $social_user_id, 'role' => $social_user_role ) );

		$salt = wp_generate_password( 20 ); // 20 character "random" string
		$key = sha1( $salt . $users_email . uniqid( time(), true ) );

		$data_array = array (
			'user_id'        => $social_user_id,
			'user_role_id'   => $user_role_id,
			'activation_key' => $key,
			'mobile_no' => $mobile_number,
			'profile_activated' => 'yes'
		);
		$data_format_array = array (
			'%d',
			'%d',
			'%s',
			'%s',
			'%s'
		);

		$result = $wpdb->insert( 'pdg_user_info', $data_array, $data_format_array );

		if ( false === $result ) {
			//do nothing
		} else {

			switch ( $social_user_role ) {
				case 'teacher':
					$insert_array = array (
						'user_id' => $social_user_id,
					);
					$wpdb->insert( 'pdg_teacher', $insert_array, array ( '%d' ) );
					break;
				case 'institution':
					$insert_array = array (
						'user_id' => $social_user_id,
						'city_id' => 1
					);
					$wpdb->insert( 'pdg_institutes', $insert_array, array ( '%d', '%d' ) );
					break;
				case 'student':
					break;
				case 'guardian':
					break;
			}

		}
		//Send Welcome Email
		ControllerActivate::fn_send_welcome_email($users_email);	

		$return_message['error'] = false;
		$return_message['message'] = 'Thank you for updating your profile. Please wait redirecting you.';
		echo json_encode( $return_message );
		die();
	}
}
