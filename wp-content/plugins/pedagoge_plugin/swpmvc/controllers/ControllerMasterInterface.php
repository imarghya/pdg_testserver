<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Default Interface for the the Page Controllers. Page Controllers must implement this interface and all of its functions.
 * 
 * @author Niraj Kumar 
 * @todo Could not Implement properly due to lack of time. Implement properly. 
 */
interface ControllerMasterInterface {
	
	/**
	 * All Page Controller class must implement this function. This function gets called when Router creates object from a Controller Class and calls this function for initial setup to build a page.
	 * 
	 * @author Niraj Kumar
	 */
	public function fn_construct_class();
	
	/**
	 * All Page Controller class must implement this function. This function returns the string containing html for the Header section of a page.
	 * 
	 * @author Niraj Kumar
	 */
	public function fn_get_header();
	
	/**
	 * All Page Controller class must implement this function. This function returns the string containing html for the Content section of a page.
	 * 
	 * @author Niraj Kumar
	 */
	public function fn_get_content();
	
	/**
	 * All Page Controller class must implement this function. This function returns the string containing html for the Footer section of a page.
	 * 
	 * @author Niraj Kumar
	 */
	public function fn_get_footer();	
}
