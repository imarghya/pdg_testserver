<?php
session_start();
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerTeacherdashboard extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		// nothing to do here.
		// Only master controller gets autoconstructed.		 
	}
	
	public function fn_construct_class() {
		//Initialize Master Controller
		parent::__construct(); 
		
		//Page Title
		$this->title = 'Settings Page:';
		
		//You can load your custom assets (css/js) specific to this page. 
		//Master page has already loaded all the required scripts for a blank page.
		$this->fn_load_scripts(); 
		
		//Set this variable to define body class of your page.
		$this->body_class .= ' page-register login-alt page-header-fixed';
		
		//Register variables required in the global scope. 
		//Variables are stored in $this->app_data array variable.
		$this->fn_register_common_variables();	

			
	}
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		//$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		return $this->load_view('teacherdashboard/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		return $this->load_view('teacherdashboard/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		//$template_vars['example_string'] = 'Hello World!';
		//$template_vars['example_number'] = 4;
		
		//$content = $this->load_view('settings/index', $template_vars);		
		$content = $this->load_view('teacherdashboard/index');	
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		//Available for the page at global scope.
		$this->app_data['global_example_string'] = 'example string';
		
		//If you wish to include a dynamic inline javascript code then use the variable 'inline_js' available in the $app_data variable.
		// This variable is a string, so append your script string to it. e.g.
		$this->app_data['inline_js'] .= '<script type="text/javascript">console.log("Testing");</script>';
	}
	
	// Example of Ajax request processing
	//Get Discover Query-------------------------------------------
	//Save query---------------------------------------------------
	public function save_query(){
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$teacher_model = new ModelTeacher();
	$res=$teacher_model->save_query($query_id,$teacher_id);
	echo 'success';
	die();
	}
	//-------------------------------------------------------------
	//UnSave query---------------------------------------------------
	public function unsave_query(){
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$teacher_model = new ModelTeacher();
	$res=$teacher_model->unsave_query($query_id,$teacher_id);
	echo 'success';
	die();
	}
	//-------------------------------------------------------------
	//Show interested----------------------------------------------
	public function show_intersted(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$student_id=$_POST['student_id'];
	$min_fees=$_POST['min_fees'];
	$max_fees=$_POST['max_fees'];
	$fees_date=$_POST['fees_date'];
	$mobile_number=$_POST['mobile_number'];
	$email_id=$_POST['email_id'];
	$message=$_POST['message'];
	
	//Check Logged In User
	if($teacher_id != 0){
		$res=$teacher_model->show_intersted($teacher_id,$query_id,$student_id,$min_fees,$max_fees,$fees_date,$mobile_number,$email_id,$message);
		echo 'success';
	}
	else
	{
		echo 'fail';
	}
	//$res=$teacher_model->show_intersted($teacher_id,$query_id,$student_id,$min_fees,$max_fees,$fees_date,$mobile_number,$email_id,$message);
	//echo 'success';
	die();	
	}
	//-------------------------------------------------------------
	//For Reffer teacher-------------------------------------------
	public function reffer_teacher(){
	$teacher_model = new ModelTeacher();
	$teacher_by_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$teacher_name=$_POST['name'];
	$mobile_number=$_POST['mobile_number'];
	$email_id=$_POST['email_id'];
	echo $res=$teacher_model->reffer_teacher($query_id,$teacher_by_id,$teacher_name,$mobile_number,$email_id);
	//$get_teacher_name=$teacher_model->get_user_name_nick($teacher_by_id);
	//$by_teacher_name=$get_teacher_name[0]->display_name;
	//Send SMS----------------------------------------------------
	//$query_url=home_url().'/teacherdashboard/?page_type=referrals&query_id='.$query_id;
	//$sms_text1='Mr '.$by_teacher_name.' referred you as a teacher for a Query on Pedagoge. View Query:- '.$query_url;
	////send sms--------------------------
	//$api_key = '559E5AAED983CA';
	//$from = 'PDAGOG';
	//$sms_text = urlencode($sms_text1);
	////$student_phone='9674775108';
	//
	//$ch = curl_init();
	//curl_setopt($ch,CURLOPT_URL, "http://www.sambsms.com/app/smsapi/index.php");
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	//curl_setopt($ch, CURLOPT_POST, 1);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=7&type=text&contacts=".$mobile_number."&senderid=".$from."&msg=".$sms_text);
	//$response = curl_exec($ch);
	//curl_close($ch);
	//
	//$message='<!doctype html>
	//<html>
	//<head>
	//<meta charset="utf-8">
	//<title>Pedagoge</title>
	//<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
	//<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
	//</head>
	//<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;">
	//		<section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;">
	//		<div style="padding:0 20px; margin:0 -20px;width:100%;">
	//		<div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;">
	//		<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">                    
	//		</div>
	//		<div style="position:relative;background:#fff;">
	//		    <div style="margin:15px 0; text-align:center;">
	//			<h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Query Referred:</h1>
	//			<p>Mr '.$by_teacher_name.' referred you as a teacher for a Query on Pedagoge.</p>
	//			<p>View Query: <a href="'.$query_url.'" style="color:#13a89e;text-decoration:none;font-weight:bold;">'.$query_url.'</a></p>
	//			<div style="margin:30px 0 15px">
	//				<ul style="list-style-type:none;text-align:center; padding:0;">
	//				<li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	//				<li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	//				<li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
	//			    </ul>
	//			    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p>
	//			    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p>
	//			    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p>
	//			    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p>
	//			</div>
	//		    </div>
	//		</div>
	//	    </div>
	//	</section>
	//</body>
	//</html>';
	//
	//$from='admin@navkiraninfotech.com';
	//$subject='Referr Query';
	//$headers  = "MIME-Version: 1.0" . "\r\n";
	//$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	//$headers .= "From: ". $from. "\r\n";
	//$headers .= "Reply-To: ". $from. "\r\n";
	//$headers .= "X-Mailer: PHP/" . phpversion();
	//$headers .= "X-Priority: 1" . "\r\n";
	//
	//$status=mail($email_id, $subject, $message, $headers);
	//----------------------------------------------------------
	die();	
	}
	//-------------------------------------------------------------
	//Get discover filter queries-------------------------------------------
	public function get_discover_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_discover_filter_query($localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	$is_interested=$teacher_model->chk_int($res->id,$teacher_id);
	$idd=0;
	$int_text='Show Interest';

	$board_university_name = '';
	if($res->board != 0)
	{
		$board_university_name = $teacher_model->get_board_name($res->board);
	}
	if($res->university != 0){
		$board_university_name = $teacher_model->get_university_name($res->university);
	}
	
	if($is_interested==1){
	$idd=1;
	$int_text='Interested';			    
	}
	?>
	<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
			    <div class="inner_container margin_top_10" id="q_show_<?php echo $res->id; ?>">
			    <div class="row">
			    <div class="col-xs-12 inner_container_text blue_border">
			    
				<div class="row">
				     <div class="col-xs-12 blue_background top_radius h1_heading">
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
				     </div>
				</div>
				<div class="row margin_top_10">
				     <div class="col-md-8 col-sm-8 col-xs-7 localities">
					    <p>Year/Standard: <?php echo $res->standard; ?><?php if($res->age!=0){ echo $res->age.' Years'; } ?></p>
					    <div class="preferred_location">
						<p>Preferred Localities:</p> 
						<ul class="area">
						    <?php foreach($localities AS $loc){ ?>
							<li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
			                             <?php } ?>
						</ul>
					    </div>
				     </div>
				     <div class="col-md-4 col-sm-4 col-xs-5">
					<ul class="pull-right side_right_menu text-center">
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background expand">Expand</a></li>
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background save">Save this</a></li>
					    <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="blue_background showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
					</ul>
				     </div>
				</div>
				
			     </div>
			    </div>
			    </div> 
                     <!--  Expand View -->
			    <div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
				 <div class="row">
				     <div class="col-xs-12 inner_container_text blue_border">
				     
					 <div class="row">
					      <div class="col-xs-12 blue_background top_radius h1_heading">
						 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
					      </div>
					 </div>
					 <div class="row margin_top_10">
					      <div class="col-md-6 col-sm-6 col-xs-12 localities">
						     <p>Year/Standard: <?php echo $res->standard; ?><?php if($res->age!=0){ echo $res->age.' Years'; } ?></p>
						     <p>Board/University: <?php echo $board_university_name; ?></p>
						     <div class="preferred_location">
							 <p>Locality Type:</p> 
							 <ul class="area">
							<?php if($res->myhome==1){ ?>
							<li><a href="javascript:void(0);" class="blue_background">My Home</a></li>
							<?php } ?>
							<?php if($res->tutorhome==1){ ?>
							<li><a href="javascript:void(0);" class="blue_background">Tutor's Home</a></li>
							<?php } ?>
							<?php if($res->institute==1){ ?>
							<li><a href="javascript:void(0);" class="blue_background">Institue</a></li>
							<?php } ?> 
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p>Preferred Localities:</p> 
							 <ul class="area">
							     <?php foreach($localities AS $loc){ ?>
							     <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
			                                     <?php } ?>
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
						     </div>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
						     <h5>Preferences: </h5>
						 <p class="text-justify"><?php echo trim($res->requirement); ?></p>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12">
						 <ul class="pull-right side_right_menu side_right_menu_media text-center">
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background collapse">Collapse</a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background save">Save this</a></li>
						     <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="blue_background showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="blue_background referSomeone">Refer someone</a></li>
						 </ul>
					      </div>
					 </div>
					 
				      </div>
				</div>
			    </div>
                       </span>
       <?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
	<?php
	die();		
	}
	//-------------------------------------------------------------
        //Get Saved filter queries-------------------------------------------
	public function get_saved_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_saved_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	if($res->past!=''){
	$border_class='gray_background';
	$text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
	}
	else{
		$border_class='blue_background';
		$text='';
	}
	$is_interested=$teacher_model->chk_int($res->queryid,$teacher_id);
	$idd=0;
	$int_text='Show Interest';
	if($is_interested==1){
	$idd=1;
	$int_text='Interested';			    
	}
	?>
	<span id="qspan_id_<?php echo $res->queryid; ?>" style="display: block;">
			    <div class="inner_container margin_top_10" id="q_show_<?php echo $res->queryid; ?>">
			    <div class="row">
			    <div class="col-xs-12 inner_container_text blue_border">
			    
				<div class="row">
				     <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
				     </div>
				</div>
				<div class="row margin_top_10">
				     <div class="col-md-8 col-sm-8 col-xs-7 localities">
					    <p>Year/Standard: <?php echo $res->standard; ?></p>
					    <div class="preferred_location">
						<p>Preferred Localities:</p> 
						<ul class="area">
						    <?php foreach($localities AS $loc){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                             <?php } ?>
						</ul>
					    </div>
				     </div>
				     <div class="col-md-4 col-sm-4 col-xs-5">
					<ul class="pull-right side_right_menu text-center">
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->queryid; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->queryid; ?>" class="<?php echo $border_class; ?> unsave">UNSAVE</a></li>
					    <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->queryid; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->queryid; ?>"><?php echo $int_text; ?></a></li>
					</ul>
				     </div>
				</div>
				
			     </div>
			    </div>
			    </div> 
                     <!--  Expand View -->
			    <div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->queryid; ?>">
				 <div class="row">
				     <div class="col-xs-12 inner_container_text blue_border">
				     
					 <div class="row">
					      <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
						 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
					      </div>
					 </div>
					 <div class="row margin_top_10">
					      <div class="col-md-6 col-sm-6 col-xs-12 localities">
						     <p>Year/Standard: <?php echo $res->standard; ?></p>
						     <p>Board/University: <?php echo $res->academic_board; ?></p>
						     <div class="preferred_location">
							 <p>Locality Type:</p> 
							 <ul class="area">
							<?php if($res->myhome==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">My Home</a></li>
							<?php } ?>
							<?php if($res->tutorhome==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">Tutor's Home</a></li>
							<?php } ?>
							<?php if($res->institute==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">Institue</a></li>
							<?php } ?> 
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p>Preferred Localities:</p> 
							 <ul class="area">
							     <?php foreach($localities AS $loc){ ?>
							     <li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                                     <?php } ?>
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
						     </div>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
						     <h5>Preferences: </h5>
						 <p class="text-justify"><?php echo trim($res->requirement); ?></p>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12">
						 <ul class="pull-right side_right_menu side_right_menu_media text-center">
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->queryid; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->queryid; ?>" class="<?php echo $border_class; ?> unsave">UNSAVE</a></li>
						     <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->queryid; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?> showInterest showInterestBtn<?php echo $res->queryid; ?>"><?php echo $int_text; ?></a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->queryid; ?>" class="<?php echo $border_class; ?> referSomeone">Refer someone</a></li>
						 </ul>
					      </div>
					 </div>
					 
				      </div>
				</div>
			    </div>
                        </span>

	<?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
	<?php
	die();		
	}
	//-----------------------------------------------------------------------
	//For uninterest---------------------------------------------------------
	public function un_intersted(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$student_id=$_POST['student_id'];
	$results=$teacher_model->un_intersted($teacher_id,$student_id,$query_id);
	echo "success";
	die();	
	}
	//-----------------------------------------------------------------------
	//For archive Query------------------------------------------------------
	public function query_archive(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$results=$teacher_model->query_archive($teacher_id,$query_id);
	echo "success";
	die();
	}
	//-----------------------------------------------------------------------
	//For unarchive teacher Query------------------------------------------------------
	public function query_unarchive(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$results=$teacher_model->query_unarchive($teacher_id,$query_id);
	echo "success";
	die();
	}
	//--------------------------------------------------------------------------------
	//For Ignore student response------------------------------------------------------
	public function ignore_response(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$student_id=$_POST['student_id'];
	$results=$teacher_model->ignore_response($teacher_id,$student_id,$query_id);
	echo "success";
	die();	
	}
	//-----------------------------------------------------------------------
        //-----------------------------------------------------------------------
        //Get Interested by you filter queries-------------------------------------------
	public function get_interested_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_interested_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	    $border_class='blue_background';
	    $text='';
	    if($res->matched!=''){
	    $border_class='interest_green_background';
	    }
	    if($res->past!=''){
            $border_class='gray_background';
	    $text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
            }
	?>
	<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
	    <div class="inner_container margin_top_20" id="q_show_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text blue_border">
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-8 col-sm-8 col-xs-7 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                <?php } ?>
				     </ul>
				 </div>
			  </div>
			  <div class="col-md-4 col-sm-4 col-xs-5">
			     <ul class="pull-right side_right_menu text-center">
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
				 <!--<li><a href="#">Save this</a></li>-->
				 <?php if($res->past==''){ ?>
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->fk_student_id; ?>" class="<?php echo $border_class; ?> uninterest">Uninterested</a></li>
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
				 <?php } ?>
			     </ul>
			  </div>
		     </div>
		  </div>
	    </div>
	</div> 
       <!--  Expand View -->
	<div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text interest_green_border">
		 
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-6 col-sm-6 col-xs-12 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <p>Board/University: <?php echo $res->academic_board; ?></p>
				 <div class="preferred_location">
				     <p>Locality Type:</p> 
				     <ul class="area">
					<?php if($res->myhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">My Home</a></li>
					<?php } ?>
					<?php if($res->tutorhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">Tutor's Home</a></li>
					<?php } ?>
					<?php if($res->institute==1){ ?>
					<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">Institue</a></li>
					<?php } ?> 
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
					<?php } ?>
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p class="width_100">Date of commencement: <strong class="interest_green_color"><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
				 </div>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 interest preference">
			    <h5>Preferences: </h5>
			     <p class="text-justify"><?php echo trim($res->requirement); ?></p>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12">
			     <ul class="pull-right side_right_menu side_right_menu_media text-center">
				 <li class="interest_background"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
				  <?php if($res->past==''){ ?>
				 <li class="interest_background"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?>" student-id="<?php echo $res->fk_student_id; ?>">Uninterested</a></li>
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> archive">Archive</a></li>
				 <?php } ?>
				 <?php if($res->matched!='' && $res->past==''){ ?>
				 <li class="interest_background"><a href="javascript:void(0);" class="<?php echo $border_class; ?> ask_review" student-id="<?php echo $res->fk_student_id; ?>">Ask Reviews</a></li>
				 <?php } ?>
			     </ul>
			  </div>
		     </div>
		     
		  </div>
	    </div>
	</div>
      </span>

	<?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
	<?php
	die();		
	}
	//------------------------------------------------------------------------------
	//Get Interested in you filter queries-------------------------------------------
	public function get_interested_in_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_interested_in_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	    $border_class='blue_background';
	    $text='';
	    if($res->matched!=''){
	    $border_class='interest_green_background';
	    }
	    if($res->past!=''){
            $border_class='gray_background';
	    $text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
            }

        $is_interested=$teacher_model->chk_int($res->id,$teacher_id);
		$idd=0;
		$int_text='Show Interest';
		if($is_interested==1){
		$idd=1;
		$int_text='Interested';			    
		}

	?>
	<span id="qspan_id2_<?php echo $res->id; ?>" style="display: block;">
	    <div class="inner_container margin_top_20" id="q_show2_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text blue_border">
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-8 col-sm-8 col-xs-7 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
			                <?php } ?>
				     </ul>
				 </div>
			  </div>
			  <div class="col-md-4 col-sm-4 col-xs-5">
			     <ul class="pull-right side_right_menu text-center">
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
				 <!--<li><a href="javascript:void(0);">Save this</a></li>-->
				 <li><a href="javascript:void(0);" idd="<?php echo $idd; ?>" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->student_id; ?>" class="<?php echo $border_class; ?>  showInterest showInterestBtn<?php echo $res->id; ?>"><?php echo $int_text; ?></a></li>
				 <?php if($res->past==''){ ?>
				 <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->studentid; ?>" class="<?php echo $border_class;?> ignore">Ignore</a></li>
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?>archive">Archive</a></li>
				 <?php } ?>
			     </ul>
			  </div>
		     </div>
		  </div>
	    </div>
	</div> 
       <!--  Expand View -->
	<div class="inner_container margin_top_10" style="display: none;" id="q_hide2_<?php echo $res->id; ?>">
	     <div class="row">
		 <div class="col-xs-12 inner_container_text interest_green_border">
		 
		     <div class="row">
			  <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
			     <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
			  </div>
		     </div>
		     <div class="row margin_top_10">
			  <div class="col-md-6 col-sm-6 col-xs-12 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <p>Board/University: <?php echo $res->academic_board; ?></p>
				 <div class="preferred_location">
				     <p>Locality Type:</p> 
				     <ul class="area">
					<?php if($res->myhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?php echo $border_class; ?>">My Home</a></li>
					<?php } ?>
					<?php if($res->tutorhome==1){ ?>
					<li><a href="javascript:void(0);" class="<?php echo $border_class; ?>">Tutor's Home</a></li>
					<?php } ?>
					<?php if($res->institute==1){ ?>
					<li><a href="javascript:void(0);" class="<?php echo $border_class; ?>">Institue</a></li>
					<?php } ?> 
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="<?php echo $border_class; ?>"><?php echo $loc; ?></a></li>
					<?php } ?>
				     </ul>
				 </div>
				 <div class="preferred_location">
				     <p class="width_100">Date of commencement: <strong class="interest_green_color"><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
				 </div>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 interest preference">
			    <h5>Preferences: </h5>
			     <p class="text-justify"><?php echo trim($res->requirement); ?></p>
			  </div>
			  <div class="col-md-3 col-sm-3 col-xs-12">
			     <ul class="pull-right side_right_menu side_right_menu_media text-center">
				 <li class="interest_background"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?>collapse">Collapse</a></li>
				  <?php if($res->past==''){ ?>
				 <li class="interest_background"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" student-id="<?php echo $res->studentid; ?>" class="<?php echo $border_class; ?>ignore">Ignore</a></li>
				 <!--<li class="interest_background"><a href="javascript:void(0);" data-toggle="modal" data-target="javascript:void(0);showInterest">Ask Reviews</a></li>-->
				 <?php }else{ ?>
				 <li class="gray"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?>archive">Archive</a></li>
				 <?php } ?>
				 <!--<li><a href="javascript:void(0);" data-toggle="modal" data-target="javascript:void(0);referSomeone">Refer someone</a></li>-->
			     </ul>
			  </div>
		     </div>
		     
		  </div>
	    </div>
	   </div>
         </span>

	<?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
	<?php
	die();		
	}
	//-------------------------------------------------------------------------------
	//Get Refeerals in you filter queries-------------------------------------------
	public function get_referrals_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_referrals_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	?>
	<li>
	<div class="row">
	     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
		<div class="row">
		     <div class="col-md-4 col-sm-12 col-xs-12 referral_name">
			 <a href="javascript:void(0);"><?php echo $res->name; ?></a>
		     </div>
		      <div class="col-md-4 col-sm-12 col-xs-12">
			 <p>[<?php echo $res->phone; ?>]</p>
		     </div>
		      <div class="col-md-4 col-sm-12 col-xs-12">
			 <p><?php echo $res->email; ?></p>
		     </div>
		 </div>
	     </div>
	     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
		     <div class="row">
		     <div class="col-md-8 col-sm-12 col-xs-12">
			 <p>was referred by you on <span><?php echo date('d-m-Y',strtotime($res->created_at)); ?></span></p>
		     </div>
		      <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
			 <a href="javascript:void(0);" id="<?php echo $res->id; ?>by" class="view_query">View Query</a>
		     </div>
		 </div>
	     </div>
	  </div>
	<div id="show<?php echo $res->id; ?>by" class="col-xs-12 margin_top_20 inner_container_text blue_border admin_referial_expend text-left" style="display: none;">
		<div class="row">
		     <div class="col-xs-12 blue_background top_radius h1_heading">
			<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
		     </div>
		</div>
		<div class="row margin_top_10">
		     <div class="col-md-8 col-sm-10 col-xs-12 localities">
			    <p>Year/Standard: <?php echo $res->standard; ?></p>
			    <div class="preferred_location">
				<p>Preferred Localities:</p>
				<ul class="area">
				     <?php foreach($localities AS $loc){ ?>
				       <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
				     <?php } ?>
				</ul>
			    </div>
		     </div>
		</div>
        </div>
        </li>
	<?php }}else{ ?>
	<li>
		<div class="row">
		     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">No Referral Found!</div>
		</div>
	</li>
	<?php
	}
	die();	
	}
	
	//----------------------------------------------------------------------------
	//Get Refeerals in you filter queries-------------------------------------------
	public function get_referrals_inyou_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_referrals_inyou_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	?>
	<li>
	<div class="row">
	     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
		<div class="row">
		     <div class="col-md-4 col-sm-12 col-xs-12 referral_name">
			 <a href="javascript:void(0);"><?php echo $res->display_name; ?></a>
		     </div>
		      <div class="col-md-4 col-sm-12 col-xs-12">
			 <p>[<?php echo $res->mobile_no; ?>]</p>
		     </div>
		      <div class="col-md-4 col-sm-12 col-xs-12">
			 <p><?php echo $res->user_email; ?></p>
		     </div>
		 </div>
	     </div>
	     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">
		     <div class="row">
		     <div class="col-md-8 col-sm-12 col-xs-12">
			 <p>was referred by you on <span><?php echo date('d-m-Y',strtotime($res->created_at)); ?></span></p>
		     </div>
		      <div class=" col-md-4 col-sm-12 col-xs-12 referral_query">
			 <a href="javascript:void(0);" id="<?php echo $res->id; ?>in" class="view_query">View Query</a>
		     </div>
		 </div>
	     </div>
	  </div>
	<!--	-----------New Style ---------  -->
		<div id="show<?php echo $res->id; ?>in" class="col-xs-12 margin_top_20 inner_container_text blue_border admin_referial_expend text-left" style="display: none;">
			 <div class="row">
			      <div class="col-xs-12 blue_background top_radius h1_heading">
				 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span></h2>
			      </div>
			 </div>
			 							<div class="row margin_top_10">
										       <div class="col-md-8 col-sm-8 col-xs-12 localities">
											      <p>Year/Standard: <?php echo $res->standard; ?></p>
											      <p>Board/University: <?php echo $res->academic_board; ?></p>
											      <div class="preferred_location">
												  <p>Locality Type:</p> 
												  <ul class="area">
												 <?php if($res->myhome==1){ ?>
												 <li><a href="javascript:void(0);" class="blue_background">My Home</a></li>
												 <?php } ?>
												 <?php if($res->tutorhome==1){ ?>
												 <li><a href="javascript:void(0);" class="blue_background">Tutor's Home</a></li>
												 <?php } ?>
												 <?php if($res->institute==1){ ?>
												 <li><a href="javascript:void(0);" class="blue_background">Institue</a></li>
												 <?php } ?> 
												  </ul>
											      </div>
											      <div class="preferred_location">
												  <p>Preferred Localities:</p> 
												  <ul class="area">
												      <?php foreach($localities AS $loc){ ?>
												      <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
												      <?php } ?>
												  </ul>
											      </div>
											      <div class="preferred_location">
												  <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
											      </div>
										       </div>
										       <div class="col-md-4 col-sm-4 col-xs-12 margin_bottom_20 preference">
											      <h5>Preferences: </h5>
											  <p class="text-justify"><?php echo trim($res->requirement); ?></p>
										       </div>
										       
										  </div>

			 
	       </div>
	      <!--	------------------------	-->
        </li>
	<?php }}else{ ?>
	<li>
		<div class="row">
		     <div class="col-sm-6 col-xs-12 col-xs-text-center col-sm-text-left col-md-text-center">No Referral Found!</div>
		</div>
	</li>
	<?php
	}
	die();	
	}
	
	//----------------------------------------------------------------------------
	
	//Get Archived filter queries-------------------------------------------
	public function get_archived_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_archived_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	?>
	<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
	    <div class="inner_container margin_top_10">
	       <div class="row">
		   <div class="col-xs-12 inner_container_text gray_border">
		       <div class="row">
			    <div class="col-xs-12 gray_background top_radius h1_heading">
			       <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?></span> <strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong></h2>
			    </div>
		       </div>
		       <div class="row margin_top_10">
			    <div class="col-md-8 col-sm-8 col-xs-7 localities">
				 <p>Year/Standard: <?php echo $res->standard; ?></p>
				 <div class="preferred_location">
				     <p>Preferred Localities:</p> 
				     <ul class="area">
					<?php foreach($localities AS $loc){ ?>
					    <li><a href="javascript:void(0);" class="gray_background"><?php echo $loc; ?></a></li>
			                <?php } ?>
				     </ul>
				 </div>
			  </div>
			    <div class="col-md-4 col-sm-4 col-xs-5">
			       <ul class="pull-right side_right_menu text-center">
				   <li class="gray margin_top_20"><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="gray_background unarchive">Unarchive</a></li>
			       </ul>
			    </div>
		       </div>
		       
		    </div>
	      </div>
	  </div>
	  </span>
	 <?php }}else{ echo "<h6>No Query Found!</h6>"; }
	  die();	
	 }
	
	//----------------------------------------------------------------------------
	
	//Get Matched filter queries-------------------------------------------
	public function get_match_filter_query(){
	$teacher_model = new ModelTeacher();
	$query_model = new ModelQuery();
	$teacher_id=$_SESSION['member_id'];
	$localitys=$_POST['locality'];
	$subjects=$_POST['subject'];
	$place1=$_POST['place1'];
	$place2=$_POST['place2'];
	$place3=$_POST['place3'];
        $results=$teacher_model->get_match_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3);
	if(count($results)>0){
	foreach($results AS $res){
	$localities = $query_model->get_q_locality($res->localities);
	    $border_class='blue_background';
	    $text='';
	    if($res->matched!=''){
	    $border_class='interest_green_background';
	    }
	    if($res->past!=''){
            $border_class='gray_background';
	    $text='<strong class="text-uppercase col-xs-float-none col-sm-float-right col-md-float-right color_white">This Query is no more active</strong>';
            }
	?>
	<span id="qspan_id_<?php echo $res->id; ?>" style="display: block;">
		    <div class="inner_container margin_top_10" id="q_show_<?php echo $res->id; ?>">
			<div class="row">
			    <div class="col-xs-12 inner_container_text blue_border">
				<div class="row">
				     <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
				     </div>
				</div>
				<div class="row margin_top_10">
				     <div class="col-md-8 col-sm-8 col-xs-7 localities">
					    <p>Year/Standard: <?php echo $res->standard; ?></p>
					    <div class="preferred_location">
						<p>Preferred Localities:</p> 
						<ul class="area">
						    <?php foreach($localities AS $loc){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                             <?php } ?>
						</ul>
					    </div>
				     </div>
				     <div class="col-md-4 col-sm-4 col-xs-5">
					<ul class="pull-right side_right_menu text-center">
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> expand">Expand</a></li>
					    <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> unmatch">Unmatch</a></li>
					</ul>
				     </div>
				</div>
				
			     </div>
			    </div>
			    </div> 
                     <!--  Expand View -->
			    <div class="inner_container margin_top_10" style="display: none;" id="q_hide_<?php echo $res->id; ?>">
				 <div class="row">
				     <div class="col-xs-12 inner_container_text blue_border">
				     
					 <div class="row">
					      <div class="col-xs-12 <?php echo $border_class; ?> top_radius h1_heading">
						 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $res->subject_name; ?> <?php echo $text; ?></span></h2>
					      </div>
					 </div>
					 <div class="row margin_top_10">
					      <div class="col-md-6 col-sm-6 col-xs-12 localities">
						     <p>Year/Standard: <?php echo $res->standard; ?></p>
						     <p>Board/University: <?php echo $res->academic_board; ?></p>
						     <div class="preferred_location">
							 <p>Locality Type:</p> 
							 <ul class="area">
							<?php if($res->myhome==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">My Home</a></li>
							<?php } ?>
							<?php if($res->tutorhome==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">Tutor's Home</a></li>
							<?php } ?>
							<?php if($res->institute==1){ ?>
							<li><a href="javascript:void(0);" class="<?php echo $border_class; ?> ">Institue</a></li>
							<?php } ?> 
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p>Preferred Localities:</p> 
							 <ul class="area">
							     <?php foreach($localities AS $loc){ ?>
							     <li><a href="javascript:void(0);" class="<?php echo $border_class; ?> "><?php echo $loc; ?></a></li>
			                                     <?php } ?>
							 </ul>
						     </div>
						     <div class="preferred_location">
							 <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($res->start_date), "d/m/Y"); ?></strong></p> 
						     </div>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
						     <h5>Preferences: </h5>
						 <p class="text-justify"><?php echo trim($res->requirement); ?></p>
					      </div>
					      <div class="col-md-3 col-sm-3 col-xs-12">
						 <ul class="pull-right side_right_menu side_right_menu_media text-center">
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> collapse">Collapse</a></li>
						     <li><a href="javascript:void(0);" q_id="<?php echo $res->id; ?>" class="<?php echo $border_class; ?> unmatch">Unmatch</a></li>
						     <li class="interest_background"><a href="javascript:void(0);" class="<?php echo $border_class; ?> ask_review" student-id="<?php echo $res->fk_student_id; ?>">Ask Reviews</a></li>
						 </ul>
					      </div>
					 </div>
				      </div>
				</div>
			    </div>
			</span>

	<?php }}else{ echo "<h6>No Query Found!</h6>"; }?>
	<?php
	die();		
	}
	//-------------------------------------------------------------------------------
	//For unmatch teacher Query------------------------------------------------------
	public function query_unmatch(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$results=$teacher_model->query_unmatch($teacher_id,$query_id);
	echo "success";
	die();
	}
	//--------------------------------------------------------------------------------
	//For Ask Review------------------------------------------------------------------
	public function ask_review(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$query_id=$_POST['query_id'];
	$student_id=$_POST['student_id'];
	$st_email_ph=$teacher_model->get_student_email_ph($student_id);
	$student_email=$st_email_ph[0]->user_email;
	$student_phone=$st_email_ph[0]->mobile_no;
	$student_name=$st_email_ph[0]->display_name;
	$teacher_slug=$teacher_model->get_teacher_slug($teacher_id);
	//Send SMS----------------------------------------------------
	$teacher_url=home_url().'/teacher/'.$teacher_slug;
	$sms_text1='Please Give Review My Profile:- '.$teacher_url;
	//send sms--------------------------
	$api_key = '559E5AAED983CA';
	$from = 'PDAGOG';
	$sms_text = urlencode($sms_text1);
	//$student_phone='9674775108';
	
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, "http://www.sambsms.com/app/smsapi/index.php");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=7&type=text&contacts=".$student_phone."&senderid=".$from."&msg=".$sms_text);
	$response = curl_exec($ch);
	curl_close($ch);
	//echo $response;
	//----------------------------------------------------------
	//Send Email------------------------------------------------
	//$message=$sms_text;
	
	$message='<!doctype html>
	<html>
	<head>
	<meta charset="utf-8">
	<title>Pedagoge</title>
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
	</head>
	<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;">
			<section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;">
			<div style="width:100%;">
			<div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;">
			<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">                    
			</div>
			<div style="position:relative;background:#fff;">
			    <div style="margin:15px 0; text-align:center;">
				<h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Ask Reviews:</h1>
				<p>'.ucfirst($student_name).' is asking for review.</p>
				<p>View profile: <a href="'.$teacher_url.'" style="color:#13a89e;text-decoration:none;font-weight:bold;">'.$teacher_url.'</a></p>
				<div style="margin:30px 0 15px">
					<ul style="list-style-type:none;text-align:center; padding:0;">
					<li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="Twtter" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li>
					<li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="FB" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li>
					<li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="Instagram" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></a></li>
				    </ul>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p>
				</div>
			    </div>
			</div>
		    </div>
		</section>
	</body>
	</html>';
	
	$from='noreply@pedagoge.com';
	$subject='Ask Review';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	$headers .= "Bcc: arghya.techexactly@gmail.com\r\n";
	$status=mail($student_email, $subject, $message, $headers);
	//----------------------------------------------------------
	//-----------------------------------------------------------
	
	echo "success";
	die();	
	}
	//--------------------------------------------------------------------------------
	//GET aJAX notification ----------------------------------------------------------
	public function get_ajax_notification(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$notifications=$teacher_model->get_all_notification($teacher_id);
	?>
	<?php if(count($notifications)>0){ foreach($notifications AS $noti){ ?>
                        <li id="noti<?php echo $noti->id; ?>">
                            	<div class="bell_icon">
                                	<i class="fa fa-bell-o" aria-hidden="true"></i>
                                </div>
                                <div class="notification_details_para">
                                	<p><?php echo $noti->notification; ?></p>
                                </div>
                                <div class="navigation_delate">
                                	<button class="delete_noti" idd="<?php echo $noti->id; ?>" type="button">X</button>
                                </div>
                            </li>
                        <?php } }else{?>
			<li>
                            	<div class="bell_icon">
                                	<i class="fa fa-bell-o" aria-hidden="true"></i>
                                </div>
                                <div class="notification_details_para">
                                	<p>You have no notification to show!</p>
                                </div>
                                <div class="navigation_delate">
                                	<!--<button type="button">X</button>-->
                                </div>
                        </li>
			<?php } ?>
	<?php
	die();		
	}
	//--------------------------------------------------------------------------------
	//Get ajax notification count-----------------------------------------------------
	public function get_ajax_notification_count(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	echo $notification_count=$teacher_model->get_count_notification($teacher_id);
	die();
	}
	//--------------------------------------------------------------------------------
	//Delete notification-------------------------------------------------------------
	public function delete_notification(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$noti_id=$_POST['noti_id'];
	$teacher_model->delete_notification($noti_id);
	die();	
	}
	//--------------------------------------------------------------------------------
	//Delete notification-------------------------------------------------------------
	public function view_notification(){
	$teacher_model = new ModelTeacher();
	$teacher_id=$_SESSION['member_id'];
	$teacher_model->view_notification($teacher_id);
	die();	
	}
	//--------------------------------------------------------------------------------

	//Query View
	public function save_query_view(){
	$user_id=$_SESSION['member_id'];
	$user = new WP_User( $user_id );
	$user_role = $user->roles[0];

	$query_id = $_POST['query_id'];
	$teacher_model = new ModelTeacher();
	$res = $teacher_model->save_query_view($query_id,$user_id,$user_role);
	echo 'success';
	die();
	}
}
?>