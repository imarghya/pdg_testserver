<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerProfileStudent extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge Student/Guardian Profile:';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}
	
	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$profile_version = '0.2';
		$this->css_assets['cropper'] =  $this->registered_css['cropper']."?ver=".$this->fixed_version;
		$this->css_assets['fontawesome'] =  $this->registered_css['fontawesome']."?ver=".$this->fixed_version;
		$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;
		$this->css_assets['bsdatetimepicker'] = $this->registered_css['bsdatetimepicker'].'?ver='.$this->fixed_version;
		
		$this->footer_js['bootbox'] = $this->registered_js['bootbox']."?ver=".$this->fixed_version;
		$this->footer_js['cropper'] = $this->registered_js['cropper']."?ver=".$this->fixed_version;
		$this->footer_js['moment'] = $this->registered_js['moment'].'?ver='.$this->fixed_version;
		$this->footer_js['bsdatetimepicker'] = $this->registered_js['bsdatetimepicker'].'?ver='.$this->fixed_version;
		$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
		$this->app_js['pedagoge_profile'] = PEDAGOGE_ASSETS_URL."/js/students_profile.js?ver=".$profile_version;

		/**
		 * This following assets are required for header & footer made with ProUI.
		 */

		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
		
		$this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;


	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}
	
	public function fn_get_content() {
		
		$current_user_id = $this->app_data['pdg_current_user']->ID;
		
		if(empty($current_user_id) || !is_numeric($current_user_id)) {
			return '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-danger">
								<div class="panel-heading">You are not logged in.</div>
								<div class="panel-body">
									<br />
									<h2>You are not logged in.</h2>
								</div>
							</div>
						</div>
					</div>
				</section>
			';
		}
		$view_pdg_student = new ModelMaster('view_pdg_student');
		$view_pdg_student->set_primary_key('student_id');
		$student_info = $view_pdg_student->find_one_by('user_id', $current_user_id);
		
		
		$content = '';
		if(!empty($student_info)) {
			$content = '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-success">
								<div class="panel-heading">Profile already created!</div>
								<div class="panel-body">
									<br />
									<h2>Success! Your profile has already been created successfully!</h2>
									<h3>Please wait.... Redirecting</h3>
									<META http-equiv="refresh" content="4; URL='.home_url('/').'">
								</div>
							</div>
						</div>
					</div>
				</section>
			';	
		} else {
			
			$city_model = new ModelMaster('view_pdg_city');
			$city_data = $city_model->set_primary_key('city_id')->all();			
			$this->app_data['city_data'] = $city_data;	
			
			$view_pdg_user_info = new ModelMaster('view_pdg_user_info');
			$view_pdg_user_info->set_primary_key('user_id');			
			$student_info = $view_pdg_user_info->find_one_by('user_id', $current_user_id);
			
			
			
			$this->app_data['student_info'] = $student_info;
			
			$content = $this->load_view('profile/students/index');
		}
		
		return $content;
	}
	
		
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {		
	}
	
	
	public static function fn_save_student_profile_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => '',
			'data' => null
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}		
				
		$student_data = json_decode( stripslashes( $_POST['student_data'] ), TRUE);
		$hidden_user_id = $student_data['hidden_user_id'];
		$hidden_profile_info_id = $student_data['hidden_profile_info_id'];
		if(!is_numeric($hidden_profile_info_id) && !is_numeric($hidden_user_id)) {
			$return_message['error_type'] = 'user_not_found';
			$return_message['message'] = 'Error! User info not found! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$profile_dir = PEDAGOGE_PLUGIN_DIR.'storage/uploads/images/'.$hidden_user_id;	
		
		if(!is_dir($profile_dir)){
		    mkdir($profile_dir);
		}		
		
		if($_FILES["student_profile_image"]["error"] <= 0) {
			$profile_image = $profile_dir.'/profile.jpg';
			$is_success = move_uploaded_file($_FILES["student_profile_image"]["tmp_name"], $profile_image);
		}
		
		
		$txt_first_name = sanitize_text_field($student_data['txt_first_name']);
		$txt_last_name = sanitize_text_field($student_data['txt_last_name']);
		$txt_mobile = sanitize_text_field($student_data['txt_mobile']);
		$txt_alternative_contact_no = sanitize_text_field($student_data['txt_alternative_contact_no']);
		$txt_dob = sanitize_text_field($student_data['txt_dob']);
		$select_gender = sanitize_text_field($student_data['select_gender']);
		$txt_address = sanitize_text_field($student_data['txt_address']);
		$select_city = sanitize_text_field($student_data['select_city']);
		//pedagoge_applog(print_r($student_data, TRUE));

		/**
		 * Check to see if the provided phone no is duplicate or not.
		 */
		if(empty($txt_mobile)) {			
			$return_message['message'] = 'Error! Phone no is required! Please input phone no.';
			echo json_encode($return_message );
			die();
		} else {
			$str_sql = "select mobile_no from pdg_user_info where mobile_no like '$txt_mobile' && user_id != $hidden_user_id";
			$is_duplicate_phone = $wpdb->get_results($str_sql);
			
			if(!empty($is_duplicate_phone)) {
				$return_message['error_type'] = 'duplicate_phone';
				$return_message['message'] = 'Error! Phone no is duplicate! Please try again.';
	
				echo json_encode($return_message );
				die();
			}
		}
		
		$user_data = array(
			'mobile_no' =>$txt_mobile,
			'alternative_contact_no' =>$txt_alternative_contact_no,
			'date_of_birth' =>date('Y-m-d', strtotime($txt_dob)),
			'gender' =>$select_gender,
			'current_address' => $txt_address,
			'current_address_city' => $select_city		
		);
		//pedagoge_applog(print_r($user_data, TRUE));
		
		update_user_meta($hidden_user_id, 'first_name', $txt_first_name);
		update_user_meta($hidden_user_id, 'last_name', $txt_last_name);

		$user_model = new ModelUserInfo();
		
		$user_model->update($user_data, $hidden_profile_info_id, array('mobile_no'=>$txt_mobile));

		$student_model = new ModelMaster('pdg_student');
		$student_model->set_primary_key('student_id')
					->set_columns_datatype(array('student_id'=>'%d','user_id'=>'%d', 'profile_slug'=>'%s'));
		$student_model->save(array('user_id'=>$hidden_user_id), array('user_id'=>$hidden_user_id) );
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Your profile was created successfully!';
		echo json_encode($return_message);
		die();
		
	}

	
}
