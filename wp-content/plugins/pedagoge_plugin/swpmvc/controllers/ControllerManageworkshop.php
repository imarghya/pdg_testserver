<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerManageworkshop extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge Workshops Management:';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}
	
	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$manage_workshop_version = '0.5';
		
		
		unset($this->css_assets['pace']);
		unset($this->css_assets['ankitesh-validationEngine']);
		unset($this->header_js['pace']);
		unset($this->footer_js['offcanvasmenueffects']);
		unset($this->footer_js['jquery-ui']);
		
		unset($this->footer_js['validationengine']);
		unset($this->footer_js['validationengine-en']);
		
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
		$this->css_assets['bsdatetimepicker'] = $this->registered_css['bsdatetimepicker'].'?ver='.$this->fixed_version;
		$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;
		
	    $this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['moment'] = $this->registered_js['moment'].'?ver='.$this->fixed_version;
		$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;	
		$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
		$this->footer_js['blockui'] = BOWER_ROOT_URL."/blockUI/jquery.blockUI.js?ver=".$this->fixed_version;
		$this->footer_js['bootbox'] = $this->registered_js['bootbox']."?ver=".$this->fixed_version;
		$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
		$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
		
		/**
		 * Cropper
		 */
		$this->css_assets['cropper'] =  $this->registered_css['cropper'].'?ver='.$this->fixed_version;
		$this->footer_js['cropper'] = $this->registered_js['cropper'].'?ver='.$this->fixed_version;
		
		/**
		 * Validation Engine
		 */		
		$this->css_assets['validationEngine']   = get_template_directory_uri() . "/assets/css/validationEngine/validationEngine.jquery.css?ver=" . $this->fixed_version;
		$this->footer_js['validationEngine']    = get_template_directory_uri() . "/assets/js/validationEngine/jquery.validationEngine.js?ver=" . $this->fixed_version;
		$this->footer_js['validationEngine-en'] = get_template_directory_uri() . "/assets/js/validationEngine/jquery.validationEngine-en.js?ver=" . $this->fixed_version;
		
		$this->app_css['workshop'] = PEDAGOGE_ASSETS_URL."/css/workshop.css?ver=".$manage_workshop_version;
		$this->app_js['manage_workshop'] = PEDAGOGE_ASSETS_URL."/js/manage_workshop_ver2.js?ver=".$manage_workshop_version;
	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');		
	}
	
	public function fn_get_content() {		
		global $wpdb;	
		$content = '';
		
		if(isset($_GET['edit']) && isset($_GET['user_id']) && isset($_GET['workshop_id'])) {
			//only admin can access this via Edit URL
			if(current_user_can('manage_options') || current_user_can('pdg_cap_workshop_edit')) {
				$user_id = $_GET['user_id'];
				if(is_numeric($user_id) && $user_id > 0) {
					//load workshop_info table
					if(!empty($this->app_data['pdg_current_user'])) {
						$content = $this->load_view('workshop/index');
						return $content;	
					}
				}
			}
		}
		$current_user_id = '';
		if(isset($this->app_data['pdg_current_user']->ID)) {
			$current_user_id = $this->app_data['pdg_current_user']->ID;
		}
		
		/**
		 * Get current user role
		 * only teacher/institute/trainer can create workshop
		 */
		$is_user_allowed_to_create_workshop = FALSE;
		if(is_numeric($current_user_id)) {
			$current_user_info_db = $wpdb->get_results("select * from view_pdg_user_info where user_id = $current_user_id");
			$current_user_role = '';
			foreach($current_user_info_db as $current_user_info) {
				$current_user_role = $current_user_info->user_role_name;
			}
			switch($current_user_role) {
				case 'workshop':
				case 'institution':
				case 'teacher':
				case 'trainer':
					$is_user_allowed_to_create_workshop = TRUE;
					break;
				default:
					$is_user_allowed_to_create_workshop = FALSE;
					break;
			}
		}
		
		if($is_user_allowed_to_create_workshop) {
			$content = $this->load_view('workshop/index');	
		} else {
			$content = '
				<section class="site-section site-section-top"  id="top">
					<div class="panel panel-danger">
						<div class="panel-body">
							<div class="alert alert-warning">
								<strong>
									Only Teacher / Institution / Trainers can create and manage workshops.
								</strong>
							</div>
						</div>
					</div>
				</section>
			';
		}
		
		return $content;
	}
	
		
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		global $wpdb;	
		
		$current_user_id = '';
		
		$this->app_data['teaching_xp_data'] = PDGManageCache::fn_load_cache('pdg_teaching_xp');
		$this->app_data['found_localities'] = PDGManageCache::fn_load_cache('pdg_locality');
		$this->app_data['qualification_data'] = PDGManageCache::fn_load_cache('pdg_qualification');
		$this->app_data['pdg_city'] = PDGManageCache::fn_load_cache('pdg_city');
		$this->app_data['pdg_workshop_categories'] = PDGManageCache::fn_load_cache('pdg_workshop_categories');
		
		if(isset($_GET['edit']) && isset($_GET['user_id'])) {
			//only admin can access this via Edit URL
			if(current_user_can('manage_options') || current_user_can('pdg_cap_workshop_edit')) {
				$user_id = $_GET['user_id'];
				$this->app_data['pdg_current_user'] = '';
				
				if(is_numeric($user_id) && $user_id > 0) {
					$user_data = get_user_by('id', $user_id);
					if($user_data) {
						$this->app_data['pdg_current_user'] = $user_data;
						$current_user_id = $this->app_data['pdg_current_user']->ID;
					}
				}
			} else {
				$current_user_id = $this->app_data['pdg_current_user']->ID;
			}
		} else {
			$current_user_id = $this->app_data['pdg_current_user']->ID;
		}

		if(!empty($current_user_id)) {			
			
			$trainer_information = $wpdb->get_results("select * from pdg_workshop_trainer_info where user_id=$current_user_id");
			
			if(!empty($trainer_information)) {
				$this->app_data['trainer_information'] = $trainer_information;	
			}			
			
			/**
			 * Load trainer qualifications
			 */
			$trainer_qualification = $wpdb->get_results("select * from pdg_workshop_qualification where user_id = $current_user_id");
			if(!empty($trainer_qualification)) {
				$this->app_data['trainer_qualification'] = $trainer_qualification;
			}
			
			/**
			 * Load Workshops List
			 */
			 $this->app_data['workshop_list_html'] = ControllerManageworkshop::fn_return_workshops_list_table($current_user_id);
			 
			 if(isset($_GET['workshop_id'])) {
			 	$editable_workshop_id = $_GET['workshop_id']; 
				if(is_numeric($editable_workshop_id) && $editable_workshop_id > 0) {					
					$str_script = '
						<script type="text/javascript">
							$(document).ready(function(){
								setTimeout(function(){
									console.log("Dynamic Script Ran!");
									$(".cmd_edit_workshop").each(function(index){
										var workshopButton = $(this);
										var editableWorkshopID = workshopButton.data("workshop_id");
										if(editableWorkshopID == '.$editable_workshop_id.') {
											workshopButton.click();
											return;
										}			
									});
								}, 200);
							});
						</script>
					';
					$this->app_data['dynamic_js'] = $str_script;
				}
			}
		}
	}

	public static function save_workshop_trainer_info_ajax() {
		global $wpdb;
		
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		$hidden_dynamic_user_var = sanitize_text_field($_POST['hidden_dynamic_user_var']);
		$hidden_dynamic_profile_edit_secret_key = sanitize_text_field($_POST['hidden_dynamic_profile_edit_secret_key']);
		
		$profile_secrets_matched = PedagogeUtilities::user_secret_key_match($hidden_dynamic_user_var, $hidden_dynamic_profile_edit_secret_key);
		
		if($profile_secrets_matched['error']) {
			echo json_encode($profile_secrets_matched );
			die();
		}
				
		$trainer_form_data = array();
		parse_str( $_POST['trainer_data'], $trainer_form_data );		
		
		$select_professional_qualification = $_POST['professional_qualifications'];
		
		$txt_trainer_name =  isset($trainer_form_data['txt_trainer_name']) ? sanitize_text_field($trainer_form_data['txt_trainer_name']) : ''; 
		$txt_trainer_contact_no = isset($trainer_form_data['txt_trainer_contact_no']) ? sanitize_text_field($trainer_form_data['txt_trainer_contact_no']) : '';
		$txt_trainer_email_address = isset($trainer_form_data['txt_trainer_email_address']) ? sanitize_text_field($trainer_form_data['txt_trainer_email_address']) : '';
		$select_average_teaching_experience = isset($trainer_form_data['select_average_teaching_experience']) ? $trainer_form_data['select_average_teaching_experience'] : '';
		$select_workshop_trainer_gender = isset($trainer_form_data['select_workshop_trainer_gender']) ? sanitize_text_field($trainer_form_data['select_workshop_trainer_gender']) : '';
		$select_graduation = isset($trainer_form_data['select_graduation']) ? $trainer_form_data['select_graduation'] : '';
		$select_postgraduation = isset($trainer_form_data['select_postgraduation']) ? $trainer_form_data['select_postgraduation'] : '';
		$txt_about_trainer = isset($trainer_form_data['txt_about_trainer']) ? sanitize_textarea_field(trim($trainer_form_data['txt_about_trainer'])) : '';
		$txt_present_place_of_work = isset($trainer_form_data['txt_present_place_of_work']) ? sanitize_text_field($trainer_form_data['txt_present_place_of_work']) : '';
		$txt_past_place_of_work = isset($trainer_form_data['txt_past_place_of_work']) ? sanitize_text_field($trainer_form_data['txt_past_place_of_work']) : '';
		
		
		if(empty($txt_trainer_name) || empty($txt_trainer_contact_no) || empty($txt_trainer_email_address) || empty($txt_about_trainer)){
			$return_message['error_type'] = 'empty_data';
			$return_message['message'] = 'Error! Please input all the required fields and try again.';
			
			echo json_encode($return_message );
			die();
		}
				
		/**
		 * Find workshop trainer's information.
		 * If available then update else insert
		 */
		$data_array = array(
			'trainer_name' => $txt_trainer_name,
			'trainer_contact_no' => $txt_trainer_contact_no,
			'trainer_email' => $txt_trainer_email_address,
			'trainer_experience' => $select_average_teaching_experience,
			'about_trainer' => $txt_about_trainer,
			'present_place_of_work' => $txt_present_place_of_work,
			'past_place_of_work' => $txt_past_place_of_work,
			'gender' => $select_workshop_trainer_gender
		);
		$data_format_array = array(
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
		);
		$workshop_trainer_information_db = $wpdb->get_results("select * from pdg_workshop_trainer_info where user_id = $hidden_dynamic_user_var");
		
		if(empty($workshop_trainer_information_db)) {
			//insert
			$data_array['user_id'] = $hidden_dynamic_user_var;
			$data_format_array[] = '%d';
			
			$wpdb->insert('pdg_workshop_trainer_info', $data_array, $data_format_array);			
		} else {
			//update
			$where_data_array = array(
				'user_id' => $hidden_dynamic_user_var,
			);
			$where_data_format_array[] = '%d';
			$wpdb->update('pdg_workshop_trainer_info', $data_array, $where_data_array, $data_format_array, $where_data_format_array);
		}
		
		//update qualifications
		$wpdb->get_results("delete from pdg_workshop_qualification where user_id = $hidden_dynamic_user_var");
		if(is_numeric($select_graduation)) {
			$insert_array = array(
				'user_id'=>$hidden_dynamic_user_var,
				'qualification_id'=>$select_graduation,
			);
			$wpdb->insert('pdg_workshop_qualification',$insert_array);
		}
		
		if(is_numeric($select_postgraduation)) {
			$insert_array = array(
				'user_id'=>$hidden_dynamic_user_var,
				'qualification_id'=>$select_postgraduation,
			);
			$wpdb->insert('pdg_workshop_qualification',$insert_array);
		}
		
		foreach($select_professional_qualification as $professional_qualification) {
			$insert_array = array(
				'user_id'=>$hidden_dynamic_user_var,
				'qualification_id'=>$professional_qualification,
			);
			$wpdb->insert('pdg_workshop_qualification',$insert_array);
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Profile updated successfully!';		
		echo json_encode($return_message );
		die();

	}

	public static function fn_return_workshops_list_table($user_id) {
		global $wpdb;
		$str_return = '<h3 class="alert alert-info">Create a workshop!</h3>';
		if(is_numeric($user_id) && $user_id > 0) {
			
			$str_sql = "
				select * from view_pdg_workshops 
				where 
					view_pdg_workshops.user_id = $user_id 
					AND view_pdg_workshops.deleted = 'no'
				order by view_pdg_workshops.workshop_id DESC
			";
			
			$workshops_list_db = $wpdb->get_results($str_sql);
			
			if(!empty($workshops_list_db)) {
					
				$str_return = '
					<div class="table-responsive">
				    	<table id="table_workshops_list" class="table table-striped table-bordered" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					            	<th>ID</th>
					            	<th>Name</th>
					            	<th>Start</th>
					            	<th>End</th>
					            	<th>workshop_category_id</th>
					            	<th>Category</th>
					            	<th>workshop_address</th>
					            	<th>workshop_city_id</th>
					            	<th>City</th>
					            	<th>workshop_locality_id</th>
					            	<th>Locality</th>
					            	<th>workshop_location_landmark</th>
					            	<th>workshop_pincode</th>
					            	<th>about_workshop</th>
					            	<th>workshop_instructions</th>
					            	<th>Type</th>
					            	<th>Seats</th>
					            	<th>price</th>
					            	<th>Pay Online</th>
					            	<th>Pay @ Venue</th>
					            	<th>Tags</th>
					            	<th>Approved</th>
					            	<th>Active</th>
					            	<th>Requested_to_be_featured</th>
					            	<th>Action</th>
					            </tr>
					        </thead>
					        <tbody>
				';
				foreach($workshops_list_db as $workshop_info) {
					$workshop_id = $workshop_info->workshop_id;
					$workshop_view_url = home_url('/workshop/?workshop_id='.$workshop_id);
					$str_return.='
						<tr>
							<td>'.$workshop_id.'</td>
							<td>'.stripslashes($workshop_info->workshop_name).'</td>
							<td>'.$workshop_info->start_date.'</td>
							<td>'.$workshop_info->end_date.'</td>
							<td>'.$workshop_info->category_id.'</td>
							<td>'.$workshop_info->workshop_category.'</td>
							<td>'.stripslashes($workshop_info->workshop_address).'</td>
							<td>'.$workshop_info->workshop_city.'</td>
							<td>'.$workshop_info->city_name.'</td>
							<td>'.$workshop_info->workshop_locality.'</td>
							<td>'.$workshop_info->locality.'</td>
							<td>'.stripslashes($workshop_info->workshop_location_landmark).'</td>
							<td>'.$workshop_info->workshop_pincode.'</td>
							<td>'.stripslashes($workshop_info->about_workshop).'</td>
							<td>'.stripslashes($workshop_info->workshop_instructions).'</td>
							<td>'.$workshop_info->workshop_type.'</td>
							<td>'.$workshop_info->workshop_seats.'</td>
							<td>'.$workshop_info->workshop_price.'</td>
							<td>'.$workshop_info->pay_online.'</td>
							<td>'.$workshop_info->pay_at_venue.'</td>
							<td>'.stripslashes($workshop_info->tags).'</td>
							<td>'.$workshop_info->is_approved.'</td>
							<td>'.$workshop_info->is_active.'</td>
							<td>'.$workshop_info->requested_to_be_featured.'</td>
							<td>
								<div style="width:130px!important;" class="btn-toolbar">
									<div class="btn-group" data-toggle="buttons">
										<a class="btn btn-info cmd_view_workshop" title="View Workshop" href="'.$workshop_view_url.'" target="_blank"><i class="fa fa-eye"></i></a>
										<button class="btn btn-success cmd_edit_workshop" title="Edit Workshop" data-workshop_id="'.$workshop_id.'"><i class="fa fa-pencil-square-o"></i></button>
										<button class="btn btn-danger cmd_delete_workshop" title="Delete Workshop"><i class="fa fa-trash-o"></i></button>
									</div>
								</div>
								
							</td>
						</tr>
					';
				}
				
				$str_return.='
							</tbody>
					    </table>
				    </div>
				';
			}	
		}
		return $str_return;
	}

	public static function fn_save_workshop_images($workshop_id, $workshop_images) {
		global $wpdb;		
		
		$wpdb->get_results("delete from pdg_workshop_images where workshop_id = $workshop_id");
		if(is_array($workshop_images)) {
			foreach($workshop_images as $index=>$key) {
				$image_name = sanitize_text_field($key['name']);
				$image_url = sanitize_text_field($key['url']);
				if(!empty($image_name) || !empty($image_url)) {
					self::insert_workshop_image_info($workshop_id, $image_url, $image_name);
				}
			}
		}
	}
	
	public static function insert_workshop_image_info($workshop_id, $image_url, $image_name) {
		global $wpdb;
		$insert_array = array(
			'image_url' => $image_url,
			'image_name' => $image_name,
			'workshop_id' =>$workshop_id
		);
		
		$format_array = array('%s', '%s', '%d');
		$wpdb->insert('pdg_workshop_images', $insert_array, $format_array);
	}

	public static function reload_workshops_ajax() {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
				
		//check whether users secret key matches or not
		$hidden_dynamic_user_var = sanitize_text_field($_POST['hidden_dynamic_user_var']);
		$hidden_dynamic_profile_edit_secret_key = sanitize_text_field($_POST['hidden_dynamic_profile_edit_secret_key']);
		
		$profile_secrets_matched = PedagogeUtilities::user_secret_key_match($hidden_dynamic_user_var, $hidden_dynamic_profile_edit_secret_key);
		
		if($profile_secrets_matched['error']) {
			echo json_encode($profile_secrets_matched );
			die();
		}
				
		$return_message['error'] = FALSE;		
		$return_message['message'] = 'Workshops list loaded successfully!';
		$return_message['data']['html'] = self::fn_return_workshops_list_table($hidden_dynamic_user_var);
		echo json_encode($return_message );
		die();
	}
	
	public static function upload_workshop_images_ajax() {
		global $wpdb;
			
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		$workshop_id = $_POST['workshop_id'];

		$gallery_dir = PEDAGOGE_PLUGIN_DIR.'storage/workshop_images';		
		$gallery_url = PEDAGOGE_PLUGIN_URL.'/storage/workshop_images/';
		
		if(!is_dir($gallery_dir)){
		    $is_created = mkdir($gallery_dir);
		}
		
		if(isset($_FILES['workshop_image']) && $_FILES['workshop_image']["error"] <= 0) {
			$image_name = $_FILES['workshop_image']['name'];
			$extension = explode('.', $image_name);
			$extension = $extension[count($extension)-1];
			
			$new_file_name = '';
			$new_file_path = '';
			
			do {
				$random_name = ControllerPrivateteacher::fn_random_string(8);
				$new_file_name = $random_name.'.'.$extension;
				$new_file_path = $gallery_dir.'/'.$new_file_name;
				
			} while(is_file($new_file_path));
			
			$is_success = move_uploaded_file($_FILES['workshop_image']["tmp_name"], $new_file_path);
			if($is_success) {
				$image_url = $gallery_url.$new_file_name;
				$return_message['error'] = FALSE;
				$return_message['message'] = 'Image was uploaded successfully!';
				$return_message['data'] = array(
					'image_url'=>$image_url,
					'image_file_name'=>$new_file_name
				);
				
				if(is_numeric($workshop_id) && $workshop_id > 0) {
					self::insert_workshop_image_info($workshop_id, $image_url, $new_file_name);
				}
				
				echo json_encode($return_message );
				die();
			} else {
				$return_message['error_type'] = 'file_upload_error2';
				$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
				
				echo json_encode($return_message );
				die();
			}
			
		} else {
			$return_message['error_type'] = 'file_upload_error';
			$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
	}

	public static function fn_delete_workshop_image_ajax() {
		global $wpdb;
			
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();	

		$file_name = sanitize_text_field($_POST['image_file_name']);
		
		$wpdb->get_results("delete from pdg_workshop_images where image_name = '$file_name'");
		 
		$gallary_dir = PEDAGOGE_PLUGIN_DIR.'storage/workshop_images/';
		$file_path = $gallary_dir.$file_name;
		
		if(is_file($file_path)) {
			if(!unlink($file_path)) {
				$return_message['error_type'] = 'file_not_deleted';
				$return_message['message'] = 'Error! File could not be deleted. Please try again.';			
				echo json_encode($return_message );
				die();
			}
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'File was deleted successfully!';			
		echo json_encode($return_message );
		die();
	}
	
	public static function fn_load_workshop_gallery_ajax() {
		global $wpdb;
			
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();

		$workshop_id = $_POST['workshop_id'];
		
		if(!is_numeric($workshop_id) || $workshop_id<=0) {
			$return_message['message'] = 'Incorrect workshop ID';			
			echo json_encode($return_message );
			die();
		}
		
		$str_return = '';
		$str_sql = "select * from pdg_workshop_images where workshop_id = $workshop_id";
		
		$workshop_images = $wpdb->get_results($str_sql);
		
		foreach($workshop_images as $images) {
				
			$image_file_name = $images->image_name;
			$image_url = $images->image_url;
			
			$str_return.='
				<div class="col-sm-6 col-md-4">
					<div class="thumbnail">
						<img src="'.$image_url.'" class="img-responsive thumbbox" style="width:100%;" />
						<hr />
						<div class="caption">
							<p>
								<button class="btn btn-danger col-xs-12 col-md-12 cmd_remove_workshop_image" data-file_name="'.$image_file_name.'"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>
							</p>
						</div>
					</div>
				</div>
			';
		}
		
		$profile_image_file = self::get_workshop_profile_image($workshop_id);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Images Loaded!';
		$return_message['data'] = array(
			'gallery' => $str_return,
			'profile' => $profile_image_file
		);
		echo json_encode($return_message );
		die();
		
	}
	
	public static function get_workshop_profile_image($workshop_id) {
		$profile_image_file = PEDAGOGE_ASSETS_URL."/images/250pxw160pxh.png";
		//find if there are any image in the profile dir of the workshop or not. 
		$workshop_dir = PEDAGOGE_PLUGIN_DIR.'storage/workshop_images/'.$workshop_id.'/profile/';		
		$workshop_url = PEDAGOGE_PLUGIN_URL.'/storage/workshop_images/'.$workshop_id.'/profile';
		if(is_dir($workshop_dir)) {
			
			$files = scandir ($workshop_dir);
			$firstFile = $workshop_dir.$files[2];			
			if(is_file($firstFile)) {
				$profile_image_file = $workshop_url.'/'.$files[2];	
			}			
			
		}
		return $profile_image_file;
	}
	
	public static function save_workshop_details_ajax() {
		global $wpdb;
		
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		//check whether users secret key matches or not
		$hidden_dynamic_user_var = sanitize_text_field($_POST['hidden_dynamic_user_var']);
		$hidden_dynamic_profile_edit_secret_key = sanitize_text_field($_POST['hidden_dynamic_profile_edit_secret_key']);
		
		$profile_secrets_matched = PedagogeUtilities::user_secret_key_match($hidden_dynamic_user_var, $hidden_dynamic_profile_edit_secret_key);
		
		if($profile_secrets_matched['error']) {
			echo json_encode($profile_secrets_matched );
			die();
		}
		
		/**
		 * If trainer profile information is not available then do not store workshop information!
		 */
		$workshop_trainer_info = $wpdb->get_results("select trainer_id from pdg_workshop_trainer_info where user_id = $hidden_dynamic_user_var");
		if(empty($workshop_trainer_info)) {
			$return_message['message'] = 'Please updated your trainer profile first and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		$workshop_form_data = array();
		parse_str( $_POST['workshop_data'], $workshop_form_data );		
		
		// Extract data, validate and then save into the database.	
		$pay_online = isset($workshop_form_data['chk_workshop_pay_online']) ? 'yes' : 'no';
		$pay_at_venue = isset($workshop_form_data['chk_workshop_pay_at_venue']) ? 'yes' : 'no';
		$requested_to_be_featured = isset($workshop_form_data['chk_workshop_request_featured']) ? 'yes' : 'no';
		
		$txt_workshop_name = isset($workshop_form_data['txt_workshop_name']) ? sanitize_text_field($workshop_form_data['txt_workshop_name']) : '';		

		$txt_workshop_start_date_dd = isset($workshop_form_data['txt_workshop_start_date_dd']) ? $workshop_form_data['txt_workshop_start_date_dd'] : '';
		$txt_workshop_start_date_mm = isset($workshop_form_data['txt_workshop_start_date_mm']) ? $workshop_form_data['txt_workshop_start_date_mm'] : '';
		$txt_workshop_start_date_yyyy = isset($workshop_form_data['txt_workshop_start_date_yyyy']) ? $workshop_form_data['txt_workshop_start_date_yyyy'] : '';			
				
		$txt_workshop_end_date_dd = isset($workshop_form_data['txt_workshop_end_date_dd']) ? $workshop_form_data['txt_workshop_end_date_dd'] : '';
		$txt_workshop_end_date_mm = isset($workshop_form_data['txt_workshop_end_date_mm']) ? $workshop_form_data['txt_workshop_end_date_mm'] : '';
		$txt_workshop_end_date_yyyy = isset($workshop_form_data['txt_workshop_end_date_yyyy']) ? $workshop_form_data['txt_workshop_end_date_yyyy'] : '';
				
		$select_workshop_category = isset($workshop_form_data['select_workshop_category']) ? $workshop_form_data['select_workshop_category'] : '';
		$txt_about_workshop = isset($workshop_form_data['txt_about_workshop']) ? sanitize_textarea_field($workshop_form_data['txt_about_workshop']) : '';
		$txt_workshop_prerequisits = isset($workshop_form_data['txt_workshop_prerequisits']) ? sanitize_textarea_field($workshop_form_data['txt_workshop_prerequisits']) : '';
		$txt_workshop_tags = isset($workshop_form_data['txt_workshop_tags']) ? sanitize_text_field($workshop_form_data['txt_workshop_tags']) : '';
		$txt_workshop_address = isset($workshop_form_data['txt_workshop_address']) ? sanitize_textarea_field($workshop_form_data['txt_workshop_address']) : '';
		$select_workshop_city = isset($workshop_form_data['select_workshop_city']) ? $workshop_form_data['select_workshop_city'] : '';
		$txt_workshop_location_pincode = isset($workshop_form_data['txt_workshop_location_pincode']) ? sanitize_text_field($workshop_form_data['txt_workshop_location_pincode']) : '';
		$select_workshop_locality = isset($workshop_form_data['select_workshop_locality']) ? $workshop_form_data['select_workshop_locality'] : '';
		$txt_workshop_landmark = isset($workshop_form_data['txt_workshop_landmark']) ? sanitize_text_field($workshop_form_data['txt_workshop_landmark']) : '';
		$txt_workshop_seats = isset($workshop_form_data['txt_workshop_seats']) ? $workshop_form_data['txt_workshop_seats'] : '';
		$select_workshop_type = isset($workshop_form_data['select_workshop_type']) ? sanitize_text_field($workshop_form_data['select_workshop_type']) : '';		
		$txt_workshop_price = isset($workshop_form_data['txt_workshop_price']) ? $workshop_form_data['txt_workshop_price'] : '';					
		$hidden_workshop_id = isset($workshop_form_data['hidden_workshop_id']) ? $workshop_form_data['hidden_workshop_id'] : '';
		
		/**
		 * Implement Start Date End Date logic
		 * Validate data 
		 */
		 
		$workshop_start_date = '';
		if(checkdate($txt_workshop_start_date_mm, $txt_workshop_start_date_dd, $txt_workshop_start_date_yyyy)) {
			$workshop_start_date = $txt_workshop_start_date_yyyy.'-'.$txt_workshop_start_date_mm.'-'.$txt_workshop_start_date_dd;
		} else {
			$return_message['message'] = 'Workshop Start Date is not in correct format. Please input correct date and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		$workshop_end_date = '';
		if(checkdate($txt_workshop_end_date_mm, $txt_workshop_end_date_dd, $txt_workshop_end_date_yyyy)) {
			$workshop_end_date = $txt_workshop_end_date_yyyy.'-'.$txt_workshop_end_date_mm.'-'.$txt_workshop_end_date_dd;
		} else {
			$return_message['message'] = 'Workshop End Date is not in correct format. Please input correct date and try again!';		
			echo json_encode($return_message );
			die();
		}
		//start date cannot be old date
		if(strtotime($workshop_start_date) < strtotime('now')) {
			$return_message['message'] = 'Workshop Start Date should be greater than today. Please input correct dates and try again!';		
			echo json_encode($return_message );
			die();
		}
		//compare dates
		if(strtotime($workshop_start_date) > strtotime($workshop_end_date)) {
			$return_message['message'] = 'Workshop Start Date should be lower or equal to End Date. Please input correct dates and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		if(empty($txt_workshop_name) || empty($txt_about_workshop) || empty($txt_workshop_address) || empty($txt_workshop_location_pincode)) {
			$return_message['message'] = 'Please input required fields and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		if(!is_numeric($select_workshop_category)) {
			$return_message['message'] = 'Please input workshop category and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		if(!is_numeric($select_workshop_city)) {
			$return_message['message'] = 'Please input workshop City and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		if(!is_numeric($txt_workshop_seats)) {
			$return_message['message'] = 'Please input workshop Seats and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		if($select_workshop_type == 'paid') {
			if($txt_workshop_price <= 0) {
				$return_message['message'] = 'Please input workshop price!';		
				echo json_encode($return_message );
				die();	
			}			
		} else if($select_workshop_type == 'free') {
			$txt_workshop_price = 0;
			$pay_online = 'no';
			$pay_at_venue = 'no';
		} else {
			$return_message['message'] = 'Please select workshop type and try again!';		
			echo json_encode($return_message );
			die();
		}
		
		$workshop_data_array = array(
			'user_id' => $hidden_dynamic_user_var,			
			'workshop_name' => $txt_workshop_name,
			'category_id' => $select_workshop_category,
			'start_date' => $workshop_start_date,
			'end_date' => $workshop_end_date,
			'workshop_address' => $txt_workshop_address,
			'workshop_city' => $select_workshop_city,
			'workshop_locality' => $select_workshop_locality,
			'workshop_location_landmark' => $txt_workshop_landmark,
			'workshop_pincode' => $txt_workshop_location_pincode,
			'about_workshop' => $txt_about_workshop,
			'workshop_instructions' => $txt_workshop_prerequisits,
			'workshop_type' => $select_workshop_type,
			'workshop_seats' => $txt_workshop_seats,
			'workshop_price' => $txt_workshop_price,
			'pay_online' => $pay_online,
			'pay_at_venue' => $pay_at_venue,
			'tags' => $txt_workshop_tags,
			'requested_to_be_featured' => $requested_to_be_featured
		);
		$data_type_array = array(
			'%d',
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%d',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%f',
			'%s',
			'%s',
			'%s',
			'%s',
		);
		
		if(is_numeric($hidden_workshop_id) && $hidden_workshop_id > 0) {
			//update
			$workshop_data_array['is_approved'] = 'no'; //After update workshop would lose its approved status and admin has to approve the workshop again.
			$data_type_array[] = '%s';
			$wpdb->update('pdg_workshops', $workshop_data_array, array('workshop_id'=>$hidden_workshop_id), $data_type_array, array('%d'));
			ControllerManageworkshop::workshop_emails($hidden_workshop_id, 'workshop_update_admin');
		} else {
			//create			 
			$wpdb->insert('pdg_workshops', $workshop_data_array, $data_type_array);
			$hidden_workshop_id = $wpdb->insert_id;			
			ControllerManageworkshop::workshop_emails($hidden_workshop_id, 'workshop_create_admin');
			ControllerManageworkshop::workshop_emails($hidden_workshop_id, 'workshop_create_user');
		}

		if(empty($workshop_start_date)) {
			$wpdb->get_results("update pdg_workshops set start_date = NULL where workshop_id = $hidden_workshop_id");	
		}
		if(empty($workshop_end_date)) {
			$wpdb->get_results("update pdg_workshops set end_date = NULL where workshop_id = $hidden_workshop_id");	
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Workshop Information was saved successfully!';
		$return_message['data']['workshop_id'] = $hidden_workshop_id;
		echo json_encode($return_message );
		die();
	}
	
	public static function delete_workshop_ajax() {
		/**
		 * Only user who created this workshop
		 * or who is admin or who has pdg_cap_workshop_delete capability
		 * can delete a workshop 
		 */
		 
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		$can_delete_workshop = FALSE;
		$workshop_id = $_POST['workshop_id'];
		
		if(current_user_can('manage_option') || current_user_can('pdg_cap_workshop_delete')) {
			$can_delete_workshop = TRUE;
		} else {
			//check whether users secret key matches or not
			$hidden_dynamic_user_var = sanitize_text_field($_POST['hidden_dynamic_user_var']);
			$hidden_dynamic_profile_edit_secret_key = sanitize_text_field($_POST['hidden_dynamic_profile_edit_secret_key']);
			
			$profile_secrets_matched = PedagogeUtilities::user_secret_key_match($hidden_dynamic_user_var, $hidden_dynamic_profile_edit_secret_key);
			
			if($profile_secrets_matched['error']) {
				$can_delete_workshop = FALSE;
			} else {
				$can_delete_workshop = TRUE;
			}
		}
		
		if(!is_numeric($workshop_id) || $workshop_id <= 0) {
			$can_delete_workshop = FALSE;
		}
		if(!$can_delete_workshop) {
			$return_message['message'] = 'Error! Workshop could not be deleted! Please try again!';
			echo json_encode($return_message );
			die();
		} else {
			ControllerManageworkshop::delete_workshop($workshop_id);
			
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Workshop Information was deleted successfully!';		
			echo json_encode($return_message );
			die();
		}
	}
	
	public static function delete_workshop($workshop_id) {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		/**
		 * pdg_workshop_images
		 * pdg_workshop_seat_booking
		 * pdg_workshops
		 */
		 $workshop_images_path = PEDAGOGE_PLUGIN_DIR.'storage/workshop_images/';
		 if(is_numeric($workshop_id) && $workshop_id > 0) {		 		
			$wpdb->get_results("delete from pdg_workshop_seat_booking where workshop_id = $workshop_id");
			$wpdb->get_results("delete from pdg_workshops where workshop_id = $workshop_id");
			
			$workshop_images = $wpdb->get_results("select * from pdg_workshop_images where workshop_id = $workshop_id");
				
			foreach($workshop_images as $workshop_image) {
				$image_name = $workshop_image->image_name;
				$image_path = $workshop_images_path.$image_name;
				if(file_exists($image_path)) {
					unlink($image_path);
				}
			}
			
			$wpdb->get_results("delete from pdg_workshop_images where workshop_id = $workshop_id");
						
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Workshop and its related data deleted successfully!';
			
		 } else {
		 	$return_message['message'] = 'Workshop ID is not correct!';
		 }
		 return $return_message;
	}

	public static function workshop_approve_ajax() {
		global $wpdb;
		
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		$action_type = trim($_POST['action_type']);
		$action_status = $_POST['action_status'];
		$workshop_id = $_POST['workshopID'];
		
		$str_sql = '';
		
		switch($action_type) {
			case 'approve':				
				$str_sql = "update pdg_workshops set is_approved='$action_status' where workshop_id = $workshop_id";
				$wpdb->get_results($str_sql);
				if($action_status == 'yes') {
					ControllerManageworkshop::workshop_emails($workshop_id, 'workshop_approval_user');	
				}
				break;
			case 'featured':				
				$str_sql = "update pdg_workshops set is_featured='$action_status' where workshop_id = $workshop_id";				
				$wpdb->get_results($str_sql);
				break; 
			case 'active':				
				$str_sql = "update pdg_workshops set is_active='$action_status' where workshop_id = $workshop_id";				
				$wpdb->get_results($str_sql);
				break; 
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'DB Action completed!';
		echo json_encode($return_message);
		die();
	}
	
	public static function workshop_emails($workshop_id, $mail_type, $additional = array()) {
		global $wpdb;
		
		$return_message = PedagogeUtilities::return_message_format();
		
		if(!is_numeric($workshop_id)) {
			return $return_message;
		}
		
		$workshop_data = $wpdb->get_results("select * from view_pdg_workshops where workshop_id = $workshop_id");
		
		$trainer_user_id = '';
		$workshop_name = '';		
		$start_date = '';
		$end_date = '';
		$workshop_seats = '';
		$workshop_type = '';
		$city_name = '';
		$locality = '';
		
		$requested_for_featured = '';
		
		$trainer_name = '';
		$trainer_email = '';
		
		foreach($workshop_data as $workshop_info) {
			$trainer_user_id = $workshop_info->user_id;
			$workshop_name = $workshop_info->workshop_name;
			$start_date = $workshop_info->start_date;
			$end_date = $workshop_info->end_date;
			$workshop_seats = $workshop_info->workshop_seats;
			$workshop_type = $workshop_info->workshop_type;
			$city_name = $workshop_info->city_name;
			$locality = $workshop_info->locality;
			$requested_for_featured = $workshop_info->requested_to_be_featured;
		}
		
		if(is_numeric($trainer_user_id)) {
			$trainer_data = $wpdb->get_results("select * from pdg_workshop_trainer_info where user_id = $trainer_user_id");
			foreach($trainer_data as $trainer_info) {
				$trainer_name = $trainer_info->trainer_name;
				$trainer_email = $trainer_info->trainer_email;
			}
		}
		
		$workshop_view_link = home_url('/workshop/');
		$workshop_view_link = $workshop_view_link."?workshop_id=$workshop_id";
		$workshop_edit_link = home_url('/manageworkshop/');
		$workshop_edit_link = $workshop_edit_link."?edit&user_id=$trainer_user_id&workshop_id=$workshop_id";
		
		$workshop_complete_data = array(
			'workshop_id' => $workshop_id,
			'trainer_user_id' => $trainer_user_id,
			'workshop_name' => $workshop_name,		
			'start_date' => $start_date,
			'end_date' => $end_date,
			'workshop_seats' => $workshop_seats,
			'workshop_type' => $workshop_type,
			'city_name' => $city_name,
			'locality' => $locality,
			'trainer_name' => $trainer_name,
			'trainer_email' => $trainer_email,
			'requested_to_be_featured' => $requested_for_featured,
			'workshop_view_url' => $workshop_view_link,
			'workshop_edit_url' => $workshop_edit_link,
		);
		$mail_data = array(
			'mail_type' => $mail_type,
			'workshop_data' => $workshop_complete_data,
			'additional' => $additional
		);
		$admin_email = 'ask@pedagoge.com';
		switch($mail_type) {
			case 'workshop_approval_user':
				$mail_data['to_address'] = $trainer_email;
				break;
			case 'workshop_booking_admin':
				$mail_data['to_address'] = $admin_email;
				break;
			case 'workshop_booking_user':
				$mail_data['to_address'] = $trainer_email;
				break;
			case 'workshop_create_admin':
				$mail_data['to_address'] = $admin_email;
				break;
			case 'workshop_create_user':
				$mail_data['to_address'] = $trainer_email;
				break;
			case 'workshop_expiry_user':
				$mail_data['to_address'] = $trainer_email;
				break;
			case 'workshop_update_admin':
				$mail_data['to_address'] = $admin_email;
				break;
		}
		$pedagoge_mailer = new PedagogeMailer($mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();
		
		$return_message['error'] = FALSE;
		return $return_message;
		
	}
	
	public static function expire_workshop() {
		global $wpdb;
		
		$str_sql = "SELECT * FROM pdg_workshops WHERE is_active = 'yes' and end_date < CURDATE()";
		$workshops_db = $wpdb->get_results($str_sql);
		foreach($workshops_db as $workshop_info) {
			$workshop_id = $workshop_info->workshop_id;
			$wpdb->get_results("update pdg_workshops set is_active='no' where workshop_id=$workshop_id");
			ControllerManageworkshop::workshop_emails($workshop_id, 'workshop_expiry_user');
		}
	}

	public static function upload_workshop_profile_image_ajax() {
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		$workshop_id = $_POST['workshop_id'];

		$workshop_dir = PEDAGOGE_PLUGIN_DIR.'storage/workshop_images/'.$workshop_id;		
		$workshop_url = PEDAGOGE_PLUGIN_URL.'/storage/workshop_images/'.$workshop_id.'/profile';
		
		if(!is_dir($workshop_dir)){
		    mkdir($workshop_dir);
		}
		$workshop_dir = $workshop_dir.'/profile';
		if(!is_dir($workshop_dir)){
		    mkdir($workshop_dir);
		} else {
			/**
			 * find all files and delete
			 */
			$cdir = scandir($workshop_dir); 
			foreach ($cdir as $key => $value) { 
				if (!in_array($value, array(".",".."))) {
					$file_to_delete = $workshop_dir.'/'.$value;
			         if(file_exists($file_to_delete)) {
			         	unlink($file_to_delete);
			         } 
			      } 
			   } 
		}
		
		if(isset($_FILES['workshop_image']) && $_FILES['workshop_image']["error"] <= 0) {
			$image_name = $_FILES['workshop_image']['name'];
			$extension = explode('.', $image_name);
			$extension = $extension[count($extension)-1];
			
			$new_file_name = $image_name;
			$new_file_path = $workshop_dir.'/'.$new_file_name;
			$is_success = move_uploaded_file($_FILES['workshop_image']["tmp_name"], $new_file_path);
			
			if($is_success) {
				$image_url = $workshop_url.'/'.$new_file_name;
				$return_message['error'] = FALSE;
				$return_message['message'] = 'Image was uploaded successfully!';
				$return_message['data'] = array(
					'image_url'=>$image_url,
					'image_file_name'=>$new_file_name
				);
				echo json_encode($return_message );
				die();
			} else {
				$return_message['error_type'] = 'file_upload_error2';
				$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
				
				echo json_encode($return_message );
				die();
			}			
		} else {
			$return_message['error_type'] = 'file_upload_error';
			$return_message['message'] = 'Error! Image could not be uploaded! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
	}

}
