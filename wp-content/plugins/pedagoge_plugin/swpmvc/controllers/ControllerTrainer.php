<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerTrainer extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Trainer';
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_load_scripts();
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$trainer_version = '1.5';

		unset( $this->css_assets['waves'] );
		unset( $this->css_assets['ankitesh-validationEngine'] );
		unset( $this->css_assets['offcanvasmenueffects'] );
		unset( $this->css_assets['3d-bold-navigation'] );
		unset( $this->css_assets['ankitesh-search'] );
		unset( $this->css_assets['ankitesh-modern'] );
		unset( $this->css_assets['ankitesh-theme-green'] );
		unset( $this->css_assets['ankitesh-custom'] );
		unset( $this->header_js['ankitesh-forms'] );
		unset( $this->footer_js['validationengine'] );
		unset( $this->footer_js['validationengine-en'] );
		unset( $this->footer_js['ankitesh-modern'] );
		unset( $this->footer_js['offcanvasmenueffects'] );


		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL . "/css/plugins.css?ver=" . $this->fixed_version;
		$this->css_assets['proui_main']    = PEDAGOGE_PROUI_URL . "/css/main.css?ver=" . $this->fixed_version;
		$this->css_assets['proui_themes']  = PEDAGOGE_PROUI_URL . "/css/themes.css?ver=" . $this->fixed_version;

		$this->css_assets['star_rating'] = BOWER_ROOT_URL . "/bootstrap-star-rating/css/star-rating.min.css?ver=" . $this->fixed_version;
		//$this->css_assets['star_rating_fa_theme'] = BOWER_ROOT_URL."/bootstrap-star-rating/css/theme-krajee-fa.min.css?ver=".$this->fixed_version;
		$this->css_assets['custom']         = PEDAGOGE_PROUI_URL . "/css/custom.css?ver=" . $this->fixed_version;
		$this->css_assets['custom_backend'] = PEDAGOGE_PROUI_URL . "/css/custom_backend.css?ver=" . $this->fixed_version;
		//$this->css_assets['bxslider'] = BOWER_ROOT_URL."/bxslider/bx_styles/bx_styles.css?ver=".$this->fixed_version;

		$this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL . "/js/plugins.js?ver=" . $this->fixed_version;
		$this->footer_js['app_js']       = PEDAGOGE_PROUI_URL . "/js/app.js?ver=" . $this->fixed_version;
		$this->footer_js['backstretch']  = PEDAGOGE_PROUI_URL . "/js/jquery.backstretch.min.js?ver=" . $this->fixed_version;
		$this->footer_js['bootbox']      = PEDAGOGE_PROUI_URL . "/js/bootbox.min.js?ver=" . $this->fixed_version;

	}

	private function fn_register_common_variables() {
		global $wpdb;

		//$current_user = wp_get_current_user();

		$current_user_db_info = '';

		$current_user_id = $this->app_data['pdg_current_user']->ID;

		if ( $current_user_id > 0 ) {
			$str_sql              = "select * from view_pdg_user_info where user_id=$current_user_id";
			$current_user_db_info = $wpdb->get_results( $str_sql );

			if ( ! empty( $current_user_db_info ) ) {
				$this->app_data['current_user_db_info'] = $current_user_db_info;
			}
		}
		$this->app_data['dynamic_js'] = null;
	}

	public function fn_get_header() {
		return $this->load_view( 'search/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'search/footer' );
	}

	public function fn_get_content() {
		global $wpdb;

		$str_content = '
			<br />
			<br /><br /><br /><br /><br /><br />
			<section class="site-content site-section">
                <div class="container">
		
					<div class="alert alert-warning">
						<h2>Sorry, Profile information is not available!</h2>
						<p><strong>Trainer may have deactivated the profile or the profile was not approved. Please visit this profile later.</strong></p>
					</div>
				</div>
			</section>
			<br /><br /><br /><br /><br /><br />
		';

		$is_current_user_admin = current_user_can( 'manage_options' );
		$current_user_id       = $this->app_data['pdg_current_user']->ID;
		$profile_slug          = sanitize_text_field( $this->app_data['pdg_profile_slug'] );
		$trainer_id            = '';
		$user_id               = '';
		$is_active             = '';
		$is_approved           = '';
		$trainer_domain        = array ();

		$view_trainer_data = $wpdb->get_results( "select * from view_pdg_trainer where profile_slug = '$profile_slug'" );
		if ( empty( $view_trainer_data ) ) {
			return $str_content;
		}

		foreach ( $view_trainer_data as $trainer_info ) {
			$is_active                                                          = $trainer_info->active;
			$is_approved                                                        = $trainer_info->approved;
			$trainer_id                                                         = $trainer_info->trainer_id;
			$user_id                                                            = $trainer_info->user_id;
			$trainer_domain[ $trainer_info->trainer_domain_list_trainer_xp_id ] = array ( $trainer_info->trainer_domain_list_domain => $trainer_info->trainer_domain_list_trainer_experience );
			$this->app_data['trainer_domain']                                   = $trainer_domain;

			$qualification_model                  = new ModelMaster( 'pdg_qualification' );
			$qualification_data                   = $qualification_model->all();
			$this->app_data['qualification_data'] = $qualification_data;

			$training_experience_model             = new ModelMaster( 'pdg_trainer_xp_list' );
			$training_experience                   = $training_experience_model->all();
			$this->app_data['training_experience'] = $training_experience;

			$pdg_domain_list_model         = new ModelMaster( 'pdg_domain_list' );
			$domain_list                   = $pdg_domain_list_model->all();
			$this->app_data['domain_list'] = $domain_list;

			$pdg_companies_logos               = new ModelMaster( 'pdg_companies_logos' );
			$companies_logos                   = $pdg_companies_logos->all();
			$this->app_data['companies_logos'] = $companies_logos;

			$trainer_domain_list_sql = "
				select  
					domain_type, domain_id
				from 
					view_pdg_trainer_domain_list
				where
					trainer_id = " . $trainer_id;

			$trainer_domain_list = $wpdb->get_results( $trainer_domain_list_sql );
			if ( ! empty( $trainer_domain_list ) ) {
				$this->app_data['trainer_domain_list'] = $trainer_domain_list;
			}

			$str_trainer_qualification_sql = "
				select 
				    pdg_trainer_qualification.qualification_id,
				    pdg_qualification.qualification,
					pdg_qualification.qualification_type
				from 
				    pdg_trainer_qualification
				left join 
					pdg_qualification on pdg_qualification.qualification_id = pdg_trainer_qualification.qualification_id
				where 
					pdg_trainer_qualification.trainer_id = " . $trainer_id;

			$trainer_qualification_modes = $wpdb->get_results( $str_trainer_qualification_sql );
			if ( ! empty( $trainer_qualification_modes ) ) {
				$this->app_data['trainer_qualification_modes'] = $trainer_qualification_modes;
			}


			$sql_user_video = "
				SELECT 
					* 
				FROM 
					pdg_user_video
				where 
					user_id = " . $user_id . " AND deleted = 'no'";

			$user_video_result = $wpdb->get_results( $sql_user_video );
			if ( ! empty( $user_video_result ) ) {
				$this->app_data['user_video_result'] = $user_video_result;
			}

		}

		if ( $is_active == 'no' || $is_approved == 'no' ) {
			return $str_content;
		}

		$this->app_data['pdg_profile_id']             = $trainer_id; //set it so that old code can access it.
		$trainer_qualification_data                   = $wpdb->get_results( "select * from pdg_trainer_qualification where trainer_id = $trainer_id" );
		$this->app_data['view_trainer_data']          = $view_trainer_data;
		$this->app_data['trainer_qualification_data'] = $trainer_qualification_data;
		$this->app_data['is_trainer_profile_private'] = false;

		if ( $is_current_user_admin || ( $current_user_id == $user_id ) ) {
			$this->app_data['is_trainer_profile_private'] = true;
			$pdg_trainer_references_types                 = new ModelMaster( 'pdg_trainer_references_types' );
			$trainer_references_types                     = $pdg_trainer_references_types->all();
			$this->app_data['trainer_references_types']   = $trainer_references_types;

			$sql_pdg_trainer_references = "
				SELECT 
					* 
				FROM 
					view_pdg_trainer_references
				where 
					trainer_id = " . $trainer_id;

			$view_pdg_trainer_references = $wpdb->get_results( $sql_pdg_trainer_references );
			if ( ! empty( $view_pdg_trainer_references ) ) {
				$this->app_data['view_pdg_trainer_references'] = $view_pdg_trainer_references;
			}
		}
		//Load the profile of the trainer
		$str_content_return_view = $this->load_view( 'public/trainer/index' );

		if ( ! $str_content_return_view ) {
			return $str_content;
		}

		return $str_content_return_view;
	}
}