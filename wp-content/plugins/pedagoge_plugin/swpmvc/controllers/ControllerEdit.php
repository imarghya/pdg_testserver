<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerEdit extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge Profile Edits';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}
	
	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$activate_version = '0.1';
		
		unset($this->css_assets['waves']);
		unset($this->css_assets['ankitesh-validationEngine']);
		unset($this->css_assets['offcanvasmenueffects']);
		unset($this->css_assets['3d-bold-navigation']);
		unset($this->css_assets['ankitesh-search']);
		unset($this->css_assets['ankitesh-modern']);
		unset($this->css_assets['ankitesh-theme-green']);
		unset($this->css_assets['ankitesh-custom']);		
		unset($this->header_js['ankitesh-forms']);
		unset($this->footer_js['validationengine']);
		unset($this->footer_js['validationengine-en']);
		unset($this->footer_js['ankitesh-modern']);
		unset($this->footer_js['offcanvasmenueffects']);
		
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
		//$this->css_assets['custom_backend'] = PEDAGOGE_PROUI_URL."/css/custom_backend.css?ver=".$this->fixed_version;
		
	    $this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		$this->app_js['pedagoge_activate'] = PEDAGOGE_ASSETS_URL."/js/user_activate.js?ver=".$activate_version;
				
	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}
	
	public function fn_get_content() {
		global $wpdb;
		
		$content = '
			<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="panel panel-danger">
								<div class="panel-heading">Profiles Edit Error!</div>
								<div class="panel-body">
									<p class="alert alert-error">You must be an Admin to edit profiles</p>
								</div>
							</div>
						</div>
					</div>
				</section>
		';
		if(current_user_can('manage_options')) {
			$content = '
				<section class="site-section site-section-top">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="panel panel-danger">
									<div class="panel-heading">Profiles Edit is under construction!</div>
									<div class="panel-body">
										<p class="alert alert-error">This section is still under construction!</p>
									</div>
								</div>
							</div>
						</div>
					</section>
			';	
		}
			
		return $content;
	}
			
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
	}

}
