<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * This class handles website's reviews related functionality, events and actions.
 * Dashboad reviews reports and reviews import codes included here.
 * 
 * @author nirajkvinit
 */
class ControllerReviews extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	/**
	 * Initial page construction function for the route /review.
	 * It loads triggers script loading and sets up Parent Class's codes.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Reviews Management System:';
		$this->fn_load_scripts();
	}
	
	/**
	 * Loads scripts related to /reviews route.
	 * 
	 * @author nirajkvinit
	 */
	private function fn_load_scripts() {
		$settings_ver = '0.1';
		
		unset($this->css_assets['waves']);
		unset($this->css_assets['ankitesh-validationEngine']);
		unset($this->css_assets['offcanvasmenueffects']);
		unset($this->css_assets['3d-bold-navigation']);
		unset($this->css_assets['ankitesh-search']);
		unset($this->css_assets['ankitesh-modern']);
		unset($this->css_assets['ankitesh-theme-green']);
		unset($this->css_assets['ankitesh-custom']);		
		unset($this->header_js['ankitesh-forms']);
		unset($this->footer_js['validationengine']);
		unset($this->footer_js['validationengine-en']);
		unset($this->footer_js['ankitesh-modern']);
		unset($this->footer_js['offcanvasmenueffects']);
		
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
				
	    $this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		if(current_user_can('manage_options')) {
			$this->footer_js['bootbox'] = $this->registered_js['bootbox'].'?ver='.$this->fixed_version;
			$this->app_js['settings'] = PEDAGOGE_ASSETS_URL."/js/reviews.js?ver=".$settings_ver;	
		}
	}
	
	/**
	 * Loads /reviews route's header section view.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_get_header() {
		return $this->load_view('search/header');
	}
	
	/**
	 * Loads /reviews route's content section view.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_get_content() {
		global $wpdb;
		$content = '
			<section class="site-section site-section-top">
				<hr />
				<div class="alert alert-warning">Error! Review action is not set. Please try again.</div>
			</section>
		';
		if(current_user_can('manage_options')) {
			if(isset($_GET['id']) && isset($_GET['action'])) {
				$action = $_GET['action'];
				$id = $_GET['id'];
				if(!is_numeric($id) || $id<=0) {
					$content = '
						<section class="site-section site-section-top">
							<hr />
							<div class="alert alert-warning">Error! Review ID is not set. Please try again.</div>
						</section>
					';
					return $content;
				}
				
				switch($action) {
					case 'accept':
						$str_sql = "update pdg_reviews set is_approved='yes', is_rejected='no' where review_id=$id";
						$wpdb->get_results($str_sql);
						$content = '
							<section class="site-section site-section-top">
								<hr />
								<div class="alert alert-success">Review has been approved successfully!</div>
							</section>
						';
						
						//$this->fn_accepted_review_email($id);
						ControllerReviews::fn_accepted_review_email($id);
						return $content;
						break;
					case 'reject':
						$str_sql = "update pdg_reviews set is_rejected='yes', is_approved='no' where review_id=$id";
						$wpdb->get_results($str_sql);
						$content = '
							<section class="site-section site-section-top">
								<div class="row">
									<div class="col-md-8 col-md-offset-2">
										<hr />
										<div class="alert alert-success">Review has been rejected successfully!</div>
										<hr />
										<div class="row">
											<div class="col-md-12 div_rejection_successful">
												<label for="txt_review_rejection_reason">Please mail to the user as why his/her review was rejected.</label>
												<textarea id="txt_review_rejection_reason" class="col-md-12 form-control"  MaxLength="2000" /></textarea>
												<br />
												<button data-loading-text="Sending..." class="btn btn-success cmd_send_rejection_mail col-md-12 col-sm-12 col-xs-12">Send Rejection Mail</button>
												<hr />
												<input type="hidden" id="hidden_review_id" value="'.$id.'" />
												<div class="div_rejection_result"></div>
											</div>
										</div>
									</div>
								</div>
								
							</section>
						';
						return $content;
						break;
					default:
						$content = '
							<section class="site-section site-section-top">
								<hr />
								<div class="alert alert-warning">Error! Review action is not set. Please try again.</div>
							</section>
						';
						return $content;
						break;
				}
				
			} else {
				//pedagoge_applog('action is not set');
			} 
		} else {
			$content = '<section class="site-section site-section-top"><h2 class="alert alert-error">Sorry! You do not have the priviledge to access this page.</h2></section>';
		}
		
		return $content;
	}
	
	/**
	 * Loads /reviews route's footer section view.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}
	
	/**
	 * Ajax function to reject a review from Admin Panel
	 * @todo check whether this function is required or not.
	 * 
	 * @author nirajkvinit
	 */
	public static function fn_reject_review_ajax() {
		global $wpdb;
		
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$rejection_text = sanitize_text_field($_POST['review_text']);
		$review_id = $_POST['review_id'];
		
		if(empty($rejection_text) || !is_numeric($review_id)) {
			$return_message['error_type'] = 'incomplete_data';
			$return_message['message'] = 'Data is incomplete. Please try again.';
			
			echo json_encode($return_message );
			die();
		}

		$found_review = $wpdb->get_results("select * from view_pdg_reviews where review_id = $review_id");
		if(empty($found_review)) {
			$return_message['error_type'] = 'review_not_found';
			$return_message['message'] = 'Review Data is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$student_email = '';
		foreach($found_review as $review_info) {
			$student_email = $review_info->student_email;
		}
		
		/**
		 * Send Mail
		 */
		if(empty($student_email)) {
			$return_message['error_type'] = 'email_error';
			$return_message['message'] = 'Students email is not available. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		$mail_data = array(
			'to_address' => $student_email,
			'mail_type' => 'review_rejection_notification_to_reviewer',
			'rejection_text' => $rejection_text
		);
		$pedagoge_mailer = new PedagogeMailer($mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Review rejection mail was sent successfully!';
				
		echo json_encode($return_message );
		die();
		
	}
	
	/**
	 * This ajax function sends emails to various parties in the event of review acceptance. 
	 * 
	 * @author nirajkvinit
	 */
	public static function fn_accepted_review_email($review_id) {
		global $wpdb;
		
		$found_review = $wpdb->get_results("select * from view_pdg_reviews where review_id = $review_id");
		if(empty($found_review)) {
			return;
		}
		
		$student_email = '';
		$teacher_email = '';
		$teacher_institute_role = '';
		$teacher_institute_user_id = '';
		foreach($found_review as $review_info) {
			$student_email = $review_info->student_email;
			$teacher_email = $review_info->teacher_email;
			$teacher_institute_role = $review_info->teacher_role;
			$teacher_institute_user_id = $review_info->tutor_institute_user_id;
		}
		
		$teacher_institute_profile_link = '';
		switch($teacher_institute_role) {
			case 'teacher':
				$teacher_institute_profile_link = $wpdb->get_var("select profile_slug from pdg_teacher where user_id = $teacher_institute_user_id");
				$teacher_institute_profile_link = home_url('/teacher/'.$teacher_institute_profile_link);
				break;
			case 'institution':
				$teacher_institute_profile_link = $wpdb->get_var("select profile_slug from pdg_institutes where user_id = $teacher_institute_user_id");
				$teacher_institute_profile_link = home_url('/institute/'.$teacher_institute_profile_link);
				break;
		}
		
		//review_approval_notification_to_reviewer
		$mail_data = array(
			'to_address' => $student_email,
			'mail_type' => 'review_approval_notification_to_reviewer',
			'profile_link' => $teacher_institute_profile_link
		);
		$pedagoge_mailer = new PedagogeMailer($mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;
		
		//review_approval_notification_to_reviewee
		$mail_data = array(
			'to_address' => $teacher_email,
			'mail_type' => 'review_approval_notification_to_reviewee',
			'profile_link' => $teacher_institute_profile_link
		);
		$pedagoge_mailer = new PedagogeMailer($mail_data);
		$mail_process_completed = $pedagoge_mailer->sendmail();
		$mail_process_completed = null;		
	}
	
	/**
	 * Ajaxified function to fetch reviews data for dashboard's reviews datatable
	 * 
	 * @author nirajkvinit 
	 */
	public static function fn_reviews_list_datatables() {
		global $wpdb;
		$table = 'view_pdg_reviews';
		// Table's primary key
		$primaryKey = 'review_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'review_id', 'dt' => 0 ),			
			array( 'db' => 'student_email',   'dt' => 1 ),
			array( 'db' => 'student_role',  'dt' => 2 ),
			array( 'db' => 'student_name',   'dt' => 3 ),
			array( 'db' => 'teacher_email',  'dt' => 4 ),
			array( 'db' => 'teacher_role',   'dt' => 5 ),
			array( 'db' => 'teacher_name',   'dt' => 6 ),			
			array( 'db' => 'review_text',   'dt' => 7 ),			
			array( 'db' => 'reviewer_user_id',  'dt' => 8 ),
			array( 'db' => 'tutor_institute_user_id',   'dt' => 9 ),			
			array( 'db' => 'rating1',   'dt' => 10 ),
			array( 'db' => 'rating2',  'dt' => 11 ),
			array( 'db' => 'rating3',   'dt' => 12 ),
			array( 'db' => 'rating4',  'dt' => 13 ),
			array( 'db' => 'average_rating',   'dt' => 14 ),
			array( 'db' => 'overall_rating',  'dt' => 15 ),
			array( 'db' => 'is_anonymous',  'dt' => 16 ),
			array( 'db' => 'is_approved',   'dt' => 17 ),
			array( 'db' => 'is_rejected',  'dt' => 18 ),
			array( 'db' => 'deleted',   'dt' => 19 ),			
			array( 'db' => 'review_id',   'dt' => 20 ),
			
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	/**
	 * Ajaxified function to fetch teacher institutes data for dashboard's reviews datatable
	 * 	This is used when manually creating a review from dashboard, to search for teacher and institutes.
	 * 
	 * @author nirajkvinit 
	 */
	public static function fn_teachers_institutes_list_datatables() {
		global $wpdb;
		$table = 'view_pdg_teachers_institutes';
		// Table's primary key
		$primaryKey = 'user_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'user_id', 'dt' => 0 ),			
			array( 'db' => 'teacher_name',   'dt' => 1 ),
			array( 'db' => 'user_email',  'dt' => 2 ),
			array( 'db' => 'mobile_no',   'dt' => 3 ),
			array( 'db' => 'user_role_id',  'dt' => 4 ),
			array( 'db' => 'user_role_name',   'dt' => 5 ),
			array( 'db' => 'user_role_display',   'dt' => 6 ),			
			array( 'db' => 'user_id',   'dt' => 7 ),
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	/**
	 * Ajaxified function to fetch students guardians data for dashboard's reviews datatable
	 * 	This is used when manually creating a review from dashboard, to search for student/guardian.
	 * 
	 * @author nirajkvinit 
	 */
	public static function fn_student_guardians_list_datatables() {
		global $wpdb;
		$table = 'view_pdg_student_guardians';
		// Table's primary key
		$primaryKey = 'user_id';
		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'user_id', 'dt' => 0 ),			
			array( 'db' => 'name',   'dt' => 1 ),
			array( 'db' => 'user_email',  'dt' => 2 ),
			array( 'db' => 'mobile_no',   'dt' => 3 ),
			array( 'db' => 'user_role_id',  'dt' => 4 ),
			array( 'db' => 'user_role_name',   'dt' => 5 ),
			array( 'db' => 'user_role_display',   'dt' => 6 ),			
			array( 'db' => 'user_id',   'dt' => 7 ),
		);
		// SQL server connection information
		$sql_details = array(
			'user' => DB_USER,
			'pass' => DB_PASSWORD,
			'db'   => DB_NAME,
			'host' => DB_HOST
		);
		
		echo json_encode(
			DataTablesController::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
		);
		die();
	}
	
	/**
	 * Ajaxified function to save Reviews.
	 * 	This is used when manually creating a review from dashboard.
	 * 
	 * @todo Check if this can be used in all reviews postings.
	 * 
	 * @author nirajkvinit 
	 */
	public static function save_rating_and_review_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}

		$rating_id = $_POST['rating_id'];
		$teacher_user_id = $_POST['teacher_userid'];
		$student_user_id = $_POST['student_userid'];
		
		$star1 = $_POST['star1'];
		$star2 = $_POST['star2'];
		$star3 = $_POST['star3'];
		$star4 = $_POST['star4'];
		$overall_stars = $_POST['overall_rating'];
		
		$txt_modal_review_text = sanitize_text_field($_POST['review_text']);		
		$anonymous = sanitize_text_field($_POST['is_anonymous']);
		$approved = sanitize_text_field($_POST['is_approved']);
		$rejected = sanitize_text_field($_POST['is_rejected']);
		
		if(empty($teacher_user_id) || $teacher_user_id <= 0) {
			$return_message['error_type'] = 'empty_teacher';
			$return_message['message'] = 'Teacher/Institute is not selected. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(empty($student_user_id) || $student_user_id <= 0) {
			$return_message['error_type'] = 'empty_student';
			$return_message['message'] = 'Student/Guardian is not selected. Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		if(strlen($txt_modal_review_text) < 5) {
			$return_message['error_type'] = 'data_incomplete';
			$return_message['message'] = 'Error! You need to input your review.';
			
			echo json_encode($return_message );
			die();
		}
		
		/**
		 * One person can rate only one teacher/institute
		 * find the combination. if combination is already available then throw error!
		 */
		 
		$duplicate_review_db = $wpdb->get_results("select review_id from pdg_reviews where reviewer_user_id = $student_user_id and tutor_institute_user_id = $teacher_user_id");
		
		
		$security_key = md5(time());
		
		$insert_array = array(
			'review_text' => $txt_modal_review_text,
			'reviewer_user_id' => $student_user_id,
			'tutor_institute_user_id' => $teacher_user_id,
			'rating1' => $star1,
			'rating2' => $star2,
			'rating3' => $star3,
			'rating4' => $star4,
			'average_rating' => $overall_stars,
			'overall_rating' => $overall_stars,
			'is_anonymous' => $anonymous,
			'security_key' => $security_key,
			'is_approved' => $approved,
			'is_rejected' => $rejected,
			'actual_review_date' => $actual_review_date,
		);
		
		$format_array = array(
			'%s',
			'%d',
			'%d',
			'%f',
			'%f',
			'%f',
			'%f',
			'%f',
			'%f',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
		);
		
		if(empty($rating_id)) {
			
			//If duplicate review found throw error
			if(!empty($duplicate_review_db)) {
				$return_message['error_type'] = 'duplicate_review';
				$return_message['message'] = 'This Student/Guardian has already reviewed this Teacher/Institute.';
				
				echo json_encode($return_message );
				die();
			}
			
			//insert
			$wpdb->insert('pdg_reviews', $insert_array, $format_array);
			$rating_id = $wpdb->insert_id;
			
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Review was posted successfully!';
			
			if($approved == 'yes' && $rejected == 'no') {
				ControllerReviews::fn_accepted_review_email($rating_id);
			}
			
			echo json_encode($return_message );
			die();	
			
		} else {
			
			//validate $rating_id 
			
			if(!is_numeric($rating_id) || $rating_id <= 0) {
				$return_message['error_type'] = 'review_id_not_correct';
				$return_message['message'] = 'Review ID is not set/correct. Please try again.';
				
				echo json_encode($return_message );
				die();
			}
			
			//check for duplicate review			
			
			$duplicate_review_id = '';
			foreach($duplicate_review_db as $duplicate_review) {
				$duplicate_review_id = $duplicate_review->review_id;
			}
			
			if($duplicate_review_id != $rating_id) {
				$return_message['error_type'] = 'duplicate_review';
				$return_message['message'] = 'Duplicate review found. Please try again.';
				
				echo json_encode($return_message );
				die();
			}

			//update
			
			$wpdb->update('pdg_reviews', $insert_array, array('review_id' => $rating_id), $format_array, array('%d'));
			
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Review was updated successfully!';
			
			if($approved == 'yes' && $rejected == 'no') {
				ControllerReviews::fn_accepted_review_email($rating_id);
			}
			
			echo json_encode($return_message );
			die();
		}
		die();
	}
	
	/**
	 * Ajaxified function to manually delete a review from the dashboard.
	 * 
	 * @todo ensure that only agents with proper capabilities can delete reviews.
	 * 
	 * @author nirajkvinit 
	 */
	public static function delete_rating_and_review_ajax() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}

		$rating_id = $_POST['rating_id'];
		
		if(!is_numeric($rating_id) || $rating_id<=0) {
			$return_message['error_type'] = 'rating_id_error';
			$return_message['message'] = 'Rating ID is incorrect! Please try again.';
			echo json_encode($return_message );
			die();
		}
		
		$wpdb->get_results("delete from pdg_reviews where review_id = $rating_id");
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Review was deleted successfully!';
		
		echo json_encode($return_message );
		die();
		
	}
	
	/**
	 * Ajaxified function to upload excel file to the server containing reviews created manually.
	 * 
	 * @author nirajkvinit 
	 */
	public static function fn_import_review_file() {
		global $wpdb;
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$review_file_path = PEDAGOGE_PLUGIN_DIR.'storage/uploads/uploaded_review.xls';
		
		
		if(isset($_FILES['review_file']) && $_FILES['review_file']["error"] <= 0) {
			
			$is_success = move_uploaded_file($_FILES['review_file']["tmp_name"], $review_file_path);
			
			if($is_success) {
					
				self::fn_process_uploaded_review_file();
								
				$return_message['error'] = FALSE;
				$return_message['message'] = 'Review File was uploaded successfully! Now process the file.';
				echo json_encode($return_message );
				die();
			} else {
				$return_message['error_type'] = 'file_upload_error2';
				$return_message['message'] = 'Error! Review File could not be uploaded! Please try again.';				
				echo json_encode($return_message );
				die();
			}
			
		} else {
			$return_message['error_type'] = 'file_upload_error';
			$return_message['message'] = 'Error! Review file could not be uploaded! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
	}
	
	/**
	 * This function runs after successful upload of the excel file containing manually created reviews data.
	 * 	It creates records in the imported_reviews_new table based on the data found in the excel rows, which can be processed later.
	 * 
	 * @author nirajkvinit 
	 */
	public static function fn_process_uploaded_review_file() {
		global $wpdb;
		
		$fileName = PEDAGOGE_PLUGIN_DIR.'storage/uploads/uploaded_review.xls';
		
		try {
		    $inputFileType = PHPExcel_IOFactory::identify($fileName);
		    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		    $objPHPExcel = $objReader->load($fileName);
		} catch(Exception $e) {
		    die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}
		
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0);
		if(empty($sheet)) {
			//pedagoge_applog('error readin excel file');
			return;
		}
		
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		
		$empty_rows_count = 0;
		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= $highestRow; $row++){ 
		    //  Read a row of data into an array
		    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		    		    
		    if($row==1) {		    	
		    	continue;
		    }
			
			//pedagoge_applog(print_r($rowData, TRUE));
			
			$first_name = ucwords(trim($rowData[0][3]));
			$last_name = ucwords(trim($rowData[0][4]));
			$student_role = strtolower(trim($rowData[0][5]));
			$phone = trim($rowData[0][6]);
			$student_email = strtolower(trim($rowData[0][7]));
			$gender = strtolower(trim($rowData[0][8]));
			$teacher_email = strtolower(trim($rowData[0][12]));			
			$rating1 = intval(trim($rowData[0][13]));
			$rating2 = intval(trim($rowData[0][14]));
			$rating3 = intval(trim($rowData[0][15]));
			$rating4 = intval(trim($rowData[0][16]));
			$avg_rating = ($rating1+$rating2+$rating3+$rating4)/4;
			$review = trim($rowData[0][18]);
			$actual_review_date = trim($rowData[0][19]);

			switch($gender) {
				case 'm':
					$gender='Male';
					break;
				case 'f':
					$gender='Female';
					break;
				default:
					$gender='Male';
					break;
			}
			switch($student_role) {
				case 's':
					$student_role = 'student';
					break;
				case 'g':
					$student_role = 'guardian';
					break;
				default:
					$student_role = 'student';
					break;
			}
			if(empty($student_email) || empty($teacher_email) || empty($review)) {
				continue;
			}
			
			$insert_array = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'phone' => $phone,
				'student_email' => $student_email,
				'gender' => $gender,
				'teacher_email' => $teacher_email,
				'rating1' => $rating1,
				'rating2' => $rating2,
				'rating3' => $rating3,
				'rating4' => $rating4,
				'avg_rating' => $avg_rating,
				'review' => $review,
				'reviewer_role' => $student_role,
				'actual_review_date' => $actual_review_date
			);
			$insert_data_format = array(
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%f',
				'%f',
				'%f',
				'%f',
				'%f',
				'%s',
				'%s',
				'%s'
			);
			$wpdb->insert('imported_reviews_new', $insert_array, $insert_data_format);
		}
	}
	
	/**
	 * Ajaxified function to insert reviews in the application from imported_reviews_new table.
	 * Data in the table imported_reviews_new is inserted via excel upload containing manually input reviews.
	 * 
	 * Whenever this function runs, it sets the field migrated to yes for a row in the event of successful migration.
	 * 
	 * @author nirajkvinit 
	 */
	public static function fn_process_imported_reviews() {
		global $wpdb;
		
		$str_imported_reviews = "
			select * from imported_reviews_new where
			migrated = 'no' and migration_error = 'no' limit 10
		";
		$imported_review_db = $wpdb->get_results($str_imported_reviews);
		//pedagoge_applog(print_r($imported_review_db, TRUE));
		if(empty($imported_review_db)) {
			$error_count = $wpdb->get_var("select count(id) from imported_reviews_new where migration_error = 'yes'");
			echo 'Review imports failed records count: '.$error_count.'<br/>';
			echo 'All reviews have been imported';
			die();
		}
		
		//Create existing reviews collection which will be used for duplicate reviews checking
		$array_existing_reviews = array();
		$existing_reviews_db = $wpdb->get_results("select reviewer_user_id, tutor_institute_user_id from pdg_reviews");
		
		foreach($existing_reviews_db as $existing_review) {
			$reviewer_user_id = $existing_review->reviewer_user_id;
			$reviewee_user_id = $existing_review->tutor_institute_user_id;
			$unique_review_key = $reviewer_user_id.'#'.$reviewee_user_id;
			if(!array_key_exists($unique_review_key, $array_existing_reviews)) {
				$array_existing_reviews[] = $unique_review_key;
			}
		}
		
		$teachers_array = array();
		$students_array = array();
		
		$user_roles_db = $wpdb->get_results("select * from pdg_user_role");
		$user_roles_array = array();
		foreach($user_roles_db as $user_role) {
			$user_role_id = $user_role->user_role_id;
			$user_role_name = $user_role->user_role_name;
			if(!array_key_exists($user_role_name, $user_roles_array)) {
				$user_roles_array[$user_role_name] = $user_role_id;
			}
		}
		
		$registered_user_names = array();
		$registered_users_db = $wpdb->get_results("select user_login from wp_users");
		foreach($registered_users_db as $registered_users) {
			$registered_user_names[] = trim($registered_users->user_login);
		}
		
		$str_users_info = "
			select user_id, user_email, user_role_name from view_pdg_user_info
		";
		$users_db = $wpdb->get_results($str_users_info);
		
		$user_roles_db = $wpdb->get_results("select * from pdg_user_role");
		$user_roles_array = array();
		foreach($user_roles_db as $user_role) {
			$user_role_id = $user_role->user_role_id;
			$user_role_name = $user_role->user_role_name;
			if(!array_key_exists($user_role_name, $user_roles_array)) {
				$user_roles_array[$user_role_name] = $user_role_id;
			}
		}
		
		foreach($users_db as $user_info) {
			$user_id = $user_info->user_id;
			$user_email = $user_info->user_email;
			$user_role = $user_info->user_role_name;
			switch($user_role) {
				case 'student':
				case 'guardian':
					if(!array_key_exists($user_email, $students_array)) {
						$students_array[$user_email] = $user_id;
					}
					break;
				case 'institution':
				case 'teacher':
					if(!array_key_exists($user_email, $teachers_array)) {
						$teachers_array[$user_email] = $user_id;
					}
					break;
				default:
					if(!array_key_exists($user_email, $students_array)) {
						$students_array[$user_email] = $user_id;
					}
					break;
			}
		}
		
		foreach($imported_review_db as $imported_review) {
			$imported_rating_id = $imported_review->id;
			$first_name = $imported_review->first_name;
			$last_name = $imported_review->last_name;
			$phone = $imported_review->phone;
			$student_email = $imported_review->student_email;
			$gender = $imported_review->gender;
			$teacher_email = $imported_review->teacher_email;
			$rating1 = $imported_review->rating1;
			$rating2 = $imported_review->rating2;
			$rating3 = $imported_review->rating3;
			$rating4 = $imported_review->rating4;
			$reviewer_user_role = $imported_review->reviewer_role;
			$actual_review_date = $imported_review->actual_review_date;			
			$avg_rating = $imported_review->avg_rating;
			
			if(!is_numeric($avg_rating) || $avg_rating<=3) {
				$rating_message = 'Ratings values not corrrect';
				self::fn_rating_import_error($imported_rating_id, $rating_message);
				continue;
			}
			
			$review = $imported_review->review;
			if(empty($teacher_email) || !array_key_exists($teacher_email, $teachers_array)) {
				$rating_message = 'Teacher email address is not available.';
				self::fn_rating_import_error($imported_rating_id, $rating_message);
				continue;
			}
			$teacher_user_id = $teachers_array[$teacher_email];
			
			if(!array_key_exists($student_email, $students_array)) {
				//create user and update $students_array array
				$student_user_name = sanitize_title($first_name.'_'.$last_name);
				$student_user_name = str_replace('-', '_', $student_user_name);
				if(in_array($student_user_name, $registered_user_names)) {
					/**
					 * randomize username
					 */
					$random_no = wp_generate_password(3);
					$student_user_name .= $random_no;					
				}
				
				$password = wp_generate_password(8);
			
				$userdata = array(
				    'user_login'  =>  $student_user_name,			    
				    'user_email'  =>  $student_email,
				    'first_name'  =>  $first_name,
				    'last_name'  =>  $last_name,
				    'display_name'  =>  $first_name.' '.$last_name,
				    'role'  =>  $reviewer_user_role,
				    'user_pass' =>$password
				);
				
				$saved_user_id = wp_insert_user($userdata);
				if(!is_numeric($saved_user_id) || $saved_user_id==0) {
					/**
					 * fail transaction
					 */
					$rating_message = 'student info could not be saved';
					self::fn_rating_import_error($imported_rating_id, $rating_message);
					continue;
					
				} else {
					$students_array[$student_email] = $saved_user_id;
					
					$salt = wp_generate_password(20); // 20 character "random" string
					$activationkey = sha1($salt . $student_email . uniqid(time(), true));
					
					$user_role_id = $user_roles_array['student'];
					
					$data_array = array(
						'user_id' => $saved_user_id,
						'user_role_id' => $user_role_id,
						'activation_key' => $activationkey,
						'mobile_no' => $phone,
						'gender' => $gender,
						'profile_activated' => 'yes'		
					);
					$data_format_array = array(
						'%d',
						'%d',
						'%s',
						'%s',
						'%s',
						'%s',
					);
					
					$result = $wpdb->insert('pdg_user_info', $data_array, $data_format_array);
					
					if($select_student_create_role=='student') {
						$student_data_array = array(
							'user_id' => $saved_user_id,
							'profile_slug' => $student_user_name,						
						);
						$student_data_format_array = array(
							'%d',					
							'%s',
						);				
						$result = $wpdb->insert('pdg_student', $student_data_array, $student_data_format_array);	
					} else if($select_student_create_role=='guardian') {
						$student_data_array = array(
							'user_id' => $saved_user_id,
							'profile_slug' => $student_user_name,						
						);
						$student_data_format_array = array(
							'%d',					
							'%s',
						);				
						$result = $wpdb->insert('pdg_guardian', $student_data_array, $student_data_format_array);
					}
				}
			}

			$student_user_id = array_key_exists($student_email, $students_array) ? $students_array[$student_email] : 0;
			
			if(!is_numeric($student_user_id) || $student_user_id <= 0) {
				$rating_message = 'student info not available';
				self::fn_rating_import_error($imported_rating_id, $rating_message);
				continue;
			}
			
			/**
			 * Check for duplicate reviews
			 */
			$unique_review_key = $student_user_id.'#'.$teacher_user_id;
			if(array_key_exists($unique_review_key, $array_existing_reviews)) {
				$rating_message = 'Duplicate review warning!';
				self::fn_rating_import_error($imported_rating_id, $rating_message);
				continue;
			}
			
			$review_insert_array = array(
				'review_text' => $review,
				'reviewer_user_id' => $student_user_id,
				'tutor_institute_user_id' => $teacher_user_id,
				'rating1' => $rating1,
				'rating2' => $rating2,
				'rating3' => $rating3,
				'rating4' => $rating4,
				'average_rating' => $avg_rating,
				'overall_rating' => $avg_rating,
				'is_approved' => 'yes',
				'actual_review_date' => $actual_review_date
			);
			$review_insert_format_array = array(
				'%s',
				'%d',
				'%d',
				'%f',
				'%f',
				'%f',
				'%f',
				'%f',
				'%f',
				'%s',
				'%s'
			);
			
			$wpdb->insert('pdg_reviews', $review_insert_array, $review_insert_format_array);
			$imported_review_id = $wpdb->insert_id;
			if(!is_numeric($imported_rating_id) || $imported_rating_id<=0) {
				$rating_message = 'rating could not be imported';
				self::fn_rating_import_error($imported_rating_id, $rating_message);
				continue;
			} else {
				$data_array = array(
					'migrated' => 'yes',
				);
				$where_data = array(
					'id' => $imported_rating_id
				);
				$wpdb->update('imported_reviews_new', $data_array, $where_data, array('%s', '%s'),array('%d'));
			}
		}
		echo 'Review import done... click once again.';
		die();
	}
	
	/**
	 * This function logs reviews import error and is called by the 
	 * function fn_process_imported_reviews in the event of failed review import.
	 * 
	 * @author nirajkvinit
	 */
	public static function fn_rating_import_error($rating_id, $rating_message) {
		global $wpdb;
		
		$data_array = array(
			'migration_error' => 'yes',
			'migration_error_text' => $rating_message
		);
		$where_data = array(
			'id' => $rating_id
		);
		$wpdb->update('imported_reviews_new', $data_array, $where_data, array('%s', '%s'),array('%d'));
	}
	
}
