<?php
session_start();
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerTeacher extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Teachers';
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_load_scripts();
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		$file_version = '0.2';

		//parent::fn_themes_ver2_load_scripts();
		//$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_js['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_js['validationEngine_en'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_js['rateyo'] = 'https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js';
		//$this->app_js['addthis'] = 'https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585b9d7903b68dc8';
		//
		//$this->app_js['ver2_social_login'] = PEDAGOGE_ASSETS_URL . '/js/social_login.js?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_js['ver2_modals_callback'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/modals_callback.js?ver=' . $this->fixed_version . "." . $file_version;
		//
		//$this->app_js['ver2_search_common_ui'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/search-common-ui.js?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_js['ver2_profile'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/profile.js?ver=' . $this->fixed_version . "." . $file_version;
		//
		//$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
		//
		//$this->css_assets['fontawesome'] = $this->registered_css['fontawesome']."?ver=".$this->fixed_version;
		//
		//$this->app_css['ver2_profile'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/profile.css?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_css['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css?ver=' . $this->fixed_version . "." . $file_version;
		//$this->app_css['rateyo'] = 'https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css';
	}

	private function fn_register_common_variables() {
		global $wpdb;
		$current_user_db_info = '';

		$current_user_id = $this->app_data['pdg_current_user']->ID;
		$current_user_role = '';

		if ( $current_user_id > 0 ) {
			$str_sql = "select * from view_pdg_user_info where user_id=$current_user_id";
			$current_user_db_info = $wpdb->get_results( $str_sql );

			if ( ! empty( $current_user_db_info ) ) {
				$this->app_data['current_user_db_info'] = $current_user_db_info;
				foreach ( $current_user_db_info as $user_db_info ) {
					$current_user_role = $user_db_info->user_role_name;
				}
			}
		}

		$str_dynamic_js = '
			<script type="text/javascript" charset="utf-8">
				var $current_user_id = ' . $current_user_id . ',
				$current_user_role = "' . $current_user_role . '";
			</script>
		';

		$this->app_data['dynamic_js'] = $str_dynamic_js;
	}

	public function fn_get_header() {
		global $wpdb;
		/**
		 * Issue no 65
		 * Updated for SEO Title of Teacher
		 */
		$profile_slug = sanitize_text_field( $this->app_data['pdg_profile_slug'] );
		$found_teacher_data = $wpdb->get_results( "select * from view_pdg_teachers where profile_slug = '$profile_slug'" );
		$this->app_data['found_teacher_data'] = $found_teacher_data;

		$first_name = '';
		$last_name = '';
		$seo_meta = '';
		foreach ( $found_teacher_data as $teacher_data ) {
			$first_name = $teacher_data->first_name;
			$last_name = $teacher_data->last_name;
			$seo_meta = $teacher_data->about_coaching;
		}
		if ( ! empty( $seo_meta ) ) {
			$this->app_data['seo_meta'] = $seo_meta;
		}

		if ( ! empty( $first_name ) ) {
			$this->title = 'Pedagoge Teacher\'s Profile : ' . $first_name . ' ' . $last_name;
		}

		return $this->load_view( 'public/ver2/teacher/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'public/ver2/teacher/footer' );
	}
        
	
	//-----------------------------------------------------------------
	public function send_teacher_email_verification(){
	$teacher_id=$_SESSION['member_id'];
	$email_id=$_POST['email_id'];
	$link= get_site_url().'/emailverification/?teacher='.$teacher_id;
	$message='<!doctype html>
		<html>
		<head>
		<meta charset="utf-8">
		<title>Pedagoge</title>
		<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
		</head>
		<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;">
				<section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;">
				<div style="padding:0 20px; margin:0 -20px;width:100%;">
				<div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;">
					<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">                    
				</div>
				<div style="position:relative;background:#fff;">
					<div style="position:relative;background-color:#13a89e;top:-60px; width:80%; display:table;margin:0 auto; left:0; right:0;padding:8px 8px 0 8px;-webkit-box-shadow: 0px 5px 32px -4px rgba(0,0,0,0.7); -moz-box-shadow: 0px 5px 32px -4px rgba(0,0,0,0.7); box-shadow: 0px 5px 32px -4px rgba(0,0,0,0.7);">
					<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/text.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">
					<a href="'.$link.'" style="display:block;text-align:center;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/verify.png" alt="Pedagoge" style="margin:40px 0 0;border:0;vertical-align:middle;max-width:100%;"></a>
				    </div>
				    <div style="margin:0 0 15px; text-align:center;">
					<h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Simply verify your email and you�re good to go.</h1>
					<div style="margin:30px 0 15px">
						<ul style="list-style-type:none;text-align:center; padding:0;">
						<li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<!--<li style="display:inline-block;margin-right:5px;"><a href="#" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-link" aria-hidden="true"></i></a></li>-->
						<li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					    </ul>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p> 
					</div>
				    </div>
				</div>
			    </div>
			</section>
		</body>
		</html>';
	
	//Send mail------------------------
	$from='noreply@pedagoge.com';
	$subject='Email Verification';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	
	$status=mail($email_id, $subject, $message, $headers);
	//---------------------------------
	die();
	}
	//-----------------------------------------------------------------
	//-----------------------------------------------------------------
	//Phone verification-----------------------------------------------
	public function send_otp_code(){
	$phone_no=$_POST['phone_no'];
	$otp_code=rand(000000,999999);
	$sms_text1="Your OTP Code is:".$otp_code;
	//send sms--------------------------
	$api_key = '559E5AAED983CA';
	$contacts = $phone_no;
	$from = 'PDAGOG';
	$sms_text = urlencode($sms_text1);
	
	//Submit to server
	
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, "http://www.sambsms.com/app/smsapi/index.php");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=7&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
	$response = curl_exec($ch);
	curl_close($ch);
	//echo $response;
	//----------------------------------
	//$otp_code=123;
	$_SESSION['otp_code']=$otp_code;
	die();
	}
	//-----------------------------------------------------------------
	//Check otp code---------------------------------------------------
	public function check_otp_code(){
	$teacher_id=$_SESSION['member_id'];
	$teacher_model = new ModelTeacher();
	$otp_codee=$_POST['otp_code'];
	if($otp_codee==$_SESSION['otp_code']){
	$res=$teacher_model->phone_verification($teacher_id);
	echo "success";	
	}
	else{
	echo "OTP Code Mismatch!!";	
	}
	die();
	}
	//-----------------------------------------------------------------
	
	public function fn_get_content() {

		global $wpdb;

		$str_content = '

			<br />

			<br /><br /><br /><br /><br /><br />

			<section class="site-content site-section">

                <div class="container">

		

					<div class="alert alert-warning">

						<h2>Sorry, Profile information is not available!</h2>

						<p><strong>Teacher may have deactivated the profile or the profile was not approved. Please visit this profile later.</strong></p>

					</div>

				</div>

			</section>

			<br /><br /><br /><br /><br /><br />

		';



		$is_current_user_admin = current_user_can( 'manage_options' ) || current_user_can( 'view_teacher_institute_personal_info' );

		$current_user_id = $this->app_data['pdg_current_user']->ID;

		$profile_slug = sanitize_text_field( $this->app_data['pdg_profile_slug'] );

		$found_teacher_id = $teacher_id = '';

		$teacher_user_id = '';

		$is_active = '';

		$is_approved = '';

		$found_teacher_data = $this->app_data['found_teacher_data'];



		if ( empty( $found_teacher_data ) ) {

			return $str_content;

		}

		foreach ( $found_teacher_data as $found_teacher ) {

			$found_teacher_id = $found_teacher->teacher_id;

			$teacher_user_id = $found_teacher->user_id;

			$is_active = $found_teacher->active;

			$is_approved = $found_teacher->approved;

		}

		if ( ! $is_current_user_admin ) {

			if ( $is_active == 'no' || $is_approved == 'no' ) {

				return $str_content;

			}

		}



		$this->app_data['pdg_profile_id'] = $teacher_id = $found_teacher_id; //set it so that old code can access it.		



		$teaching_achievments_data = $wpdb->get_results( "select * from pdg_teaching_achievements where user_id=$teacher_user_id and deleted='no'" );

		$tuition_batch_data = $wpdb->get_results( "select * from view_pdg_tuition_batch where tutor_institute_user_id=$teacher_user_id" );



		$batches_id_array = array ();

		$teacher_about_the_course = '';

		foreach ( $tuition_batch_data as $tuition_batch ) {

			$batches_id_array[] = $tuition_batch->tuition_batch_id;

			$teacher_about_the_course = $tuition_batch->about_course;

		}



		$tutor_locality_data = '';

		$tutor_batch_fees_data = '';

		$tuition_batch_subject_data = '';

		$tuition_batch_class_timing_data = '';

		if ( count( $batches_id_array ) > 0 ) {

			$str_locality_sql = "

				select * from view_pdg_tutor_location where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")

			";



			$tutor_locality_data = $wpdb->get_results( $str_locality_sql );



			$str_batch_fees_sql = "

				select * from view_pdg_batch_fees  where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")

			";



			$tutor_batch_fees_data = $wpdb->get_results( $str_batch_fees_sql );



			$str_batch_subjects_sql = "

				select * from view_pdg_batch_subject where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")

			";



			$tuition_batch_subject_data = $wpdb->get_results( $str_batch_subjects_sql );



			$str_batch_timing_sql = "

				select * from view_pdg_batch_class_timing where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")

			";



			$tuition_batch_class_timing_data = $wpdb->get_results( $str_batch_timing_sql );

		}



		$teacher_qualification_data = $wpdb->get_results( "select * from view_pdg_teacher_qualification where teacher_id = $teacher_id" );



		$recommendations_sql = "select * from pdg_recommendations where recommended_to=$teacher_user_id";

		$recommended_data = $wpdb->get_results( $recommendations_sql );



		$rating_sql = "

			select * from pdg_reviews where tutor_institute_user_id=$teacher_user_id;

		";

		$rating_data = $wpdb->get_results( $rating_sql );



		$this->app_data['teacher_data'] = $found_teacher_data;

		$this->app_data['achievement_data'] = $teaching_achievments_data;

		$this->app_data['tutor_locality_data'] = $tutor_locality_data;

		$this->app_data['tuition_batch_data'] = $tuition_batch_data;

		$this->app_data['teacher_qualification_data'] = $teacher_qualification_data;

		$this->app_data['tutor_batch_fees_data'] = $tutor_batch_fees_data;

		$this->app_data['tuition_batch_subject_data'] = $tuition_batch_subject_data;

		$this->app_data['tuition_batch_class_timing_data'] = $tuition_batch_class_timing_data;

		$this->app_data['recommendations_data'] = $recommended_data;

		$this->app_data['rating_data'] = $rating_data;

		//$this->app_data['$classes_details_data'] = $classes_details_data;





		/**

		 * If found teacher user id and current userid is the same or admin then load profile with personal data

		 * if found teacher's user id does not match then load public profile

		 */



		if ( $is_current_user_admin || ( $current_user_id == $teacher_user_id ) ) {

			// load public profile with personal data



			$date_of_birth = '';

			$permanent_address = '';

			$permanent_city = '';

			$current_address = '';

			$current_city = '';

			$mobile = '';

			$alternative_contact_no = '';

			$email = '';

			$gender = '';



			$teacher_extra_sql = "

				select 

					pdg_user_info.*, 

					p1.city_name as current_city, 

					p2.city_name as permanent_city, 

					view_wp_users_info.user_email 

				from 

					pdg_user_info 

				left join pdg_city p1 on p1.city_id = pdg_user_info.current_address_city 

				left join pdg_city p2 on p2.city_id = pdg_user_info.permanent_address_city 

				left join view_wp_users_info on pdg_user_info.user_id = view_wp_users_info.user_id 

				where pdg_user_info.user_id= $teacher_user_id";



			$teacher_personal_data = $wpdb->get_results( $teacher_extra_sql );



			foreach ( $teacher_personal_data as $teacher_info ) {

				$date_of_birth = $teacher_info->date_of_birth;

				$permanent_address = $teacher_info->permanent_address;

				$permanent_city = $teacher_info->permanent_city;

				$current_address = $teacher_info->current_address;

				$current_city = $teacher_info->current_city;

				$mobile = $teacher_info->mobile_no;

				$email = $teacher_info->user_email;

				$gender = $teacher_info->gender;

				$alternative_contact_no = $teacher_info->alternative_contact_no; 

			}

			$this->app_data['private_profile'] = true;

			$this->app_data['permanent_city'] = $permanent_city;

			$this->app_data['mobile_no'] = $mobile;

			$this->app_data['user_email'] = $email;

			$this->app_data['gender'] = $gender;

			$this->app_data['alternative_contact_no'] = $alternative_contact_no;



			$this->app_data['date_of_birth'] = $date_of_birth;

			if ( empty( $date_of_birth ) ) {

				$this->app_data['date_of_birth'] = "N/A";

			}



			$this->app_data['permanent_address'] = $permanent_address;

			if ( empty( $permanent_address ) ) {

				$this->app_data['permanent_address'] = "N/A";

			}



			$this->app_data['current_address'] = $current_address;

			if ( empty( $current_address ) ) {

				$this->app_data['current_address'] = "N/A";

			}



			$this->app_data['current_city'] = $current_city;

			if ( empty( $current_city ) ) {

				$this->app_data['current_city'] = "N/A";

			}



			$this->app_data['permanent_city'] = $permanent_city;

			if ( empty( $permanent_city ) ) {

				$this->app_data['permanent_city'] = "N/A";

			}



			if ( $is_current_user_admin ) {

				$this->app_data['teacher_notes'] = ControllerTeacher::fn_return_teacher_notes_html( $teacher_user_id );

			}

		}

		$str_content = $this->load_view( 'public/ver2/teacher/index' );

		return $str_content;

	}

	public static function fn_return_reviews_content( $rating_data ) {
		global $wpdb;

		$str_recommendations_html = '<div class="alert alert-info">No review found!</div>';

		if ( empty( $rating_data ) ) {
			return $str_recommendations_html;
		}
		$rated_user_array = array ();
		foreach ( $rating_data as $ratings ) {
			$rated_by = $ratings->reviewer_user_id;
			if ( ! in_array( $rated_by, $rated_user_array ) ) {
				$rated_user_array[] = $rated_by;
			}
		}


		$str_found_user_sql = " select * from view_pdg_user_info where user_id in (" . implode( ',', $rated_user_array ) . ")";

		$found_users_db = $wpdb->get_results( $str_found_user_sql );
		$found_users_array = array ();

		if ( ! empty( $found_users_db ) ) {

			foreach ( $found_users_db as $found_users ) {
				$found_user_id = $found_users->user_id;
				$first_name = $found_users->first_name;
				$last_name = $found_users->last_name;
				if ( ! array_key_exists( $found_user_id, $found_users_array ) ) {
					$found_users_array[ $found_user_id ] = $first_name . ' ' . $last_name;
				}
			}

			$str_recommendations_html = '';
			foreach ( $rating_data as $ratings ) {
				$rated_by_user_id = $ratings->reviewer_user_id;
				$star_rating = $ratings->overall_rating;
				$is_approved = $ratings->is_approved;
				$review_text = $ratings->review_text;
				$is_anonymous = $ratings->is_anonymous;
				if ( $is_approved == 'yes' ) {
					$user_full_name = "Anonymous";
					if ( $is_anonymous == 'no' ) {
						$user_full_name = $found_users_array[ $rated_by_user_id ];
					}

					$found_user_image = PEDAGOGE_ASSETS_URL . '/images/student.png';
					$str_recommendations_html .= '
						<div class="row">
                            <div class="col-md-1 col-md-offset-1 col-xs-6 col-xs-offset-3">
                                <img src="' . $found_user_image . '" class="img-responsive  img-round">
                            </div>

                            <div class="col-md-9">
								<div class="row">
									<div class="col-md-7">' . $user_full_name . '</div>
									<div class="col-md-4">
										<input value="' . $star_rating . '" class="rating-loading rated_by_user" data-size="xs">
										
									</div>
									<div class="col-md-1">(' . $star_rating . ')</div>
								</div>
								<div class="row">
									<div class="col-md-12 text-justify pull-left">
										' . $review_text . '
									</div>
								</div>
                            </div>
                        </div><hr>
					';
				}
			}
		}
		$str_recommendations_html = trim( $str_recommendations_html );
		if ( empty( $str_recommendations_html ) ) {
			$str_recommendations_html = '<div class="alert alert-info">No review found!</div>';
		}

		return $str_recommendations_html;
	}

	public static function fn_return_reviews_content_new( $rating_data ) {
		global $wpdb;

		$str_recommendations_html = 'No reviews yet.';

		if ( empty( $rating_data ) ) {
			return $str_recommendations_html;
		}
		$rated_user_array = array ();
		foreach ( $rating_data as $ratings ) {
			$rated_by = $ratings->reviewer_user_id;
			if ( ! in_array( $rated_by, $rated_user_array ) ) {
				$rated_user_array[] = $rated_by;
			}
		}

		$str_found_user_sql = " select * from view_pdg_user_info where user_id in (" . implode( ',', $rated_user_array ) . ")";

		$found_users_db = $wpdb->get_results( $str_found_user_sql );
		$found_users_array = array ();

		if ( ! empty( $found_users_db ) ) {

			foreach ( $found_users_db as $found_users ) {
				$found_user_id = $found_users->user_id;
				$first_name = $found_users->first_name;
				$last_name = $found_users->last_name;
				if ( ! array_key_exists( $found_user_id, $found_users_array ) ) {
					$found_users_array[ $found_user_id ] = $first_name . ' ' . $last_name;
				}
			}

			$str_recommendations_html = '';
			foreach ( $rating_data as $ratings ) {
				$rated_by_user_id = $ratings->reviewer_user_id;
				$star_rating = $ratings->overall_rating;
				$is_approved = $ratings->is_approved;
				$review_text = $ratings->review_text;
				$is_anonymous = $ratings->is_anonymous;
				$rating_date_time = date('d-m-Y',strtotime($ratings->created));
				
				if ( $is_approved == 'yes' ) {
					$user_full_name = "Anonymous";
					if ( $is_anonymous == 'no' ) {
						$user_full_name = $found_users_array[ $rated_by_user_id ];
					}

					$found_user_image = PEDAGOGE_ASSETS_URL . '/images/student.png';
					
					$str_recommendations_html .= '<!--reviews-items-->
						<div class="list-group">
							<div class="list-group-item">
								<div class="container-fluid col-xs-3 col-sm-1 no-padding-left">
									<div class="row-picture text-center center-block">
										<img src="' . $found_user_image . '" class="circle" alt="Reviews Icon">
										<p class="blue-shade-color item-name text-center text-bold">' . $user_full_name . '</p>
										<p class="black-shade-color item-person-type text-center">Student</p>
									</div>
								</div>
								<div class="container-fluid col-xs-9">
									<div class="row-content reviews_item_rating">
										<p class="list-group-item-heading inline reviews_item_rating">
											'.self::rating_stars_render( PDGSearchContent::pdg_avg_rating_to_stars( round( $star_rating * 2, 0 ) / 2 ) ).'&nbsp;
												<span class="star-rating-text inline">('.$star_rating . ')</span>
										</p>
										<p class="list-group-item-text">' . stripcslashes( $review_text ) . '</p><br />
										<p class="list-group-item-text">Dated: '.$rating_date_time.'</p>
									</div>
								</div>
							</div>

							<div class="list-group-separator"></div>
						</div>
						<!--/reviews-items-->';
				}
			}
		}
		$str_recommendations_html = trim( $str_recommendations_html );
		if ( empty( $str_recommendations_html ) ) {
			$str_recommendations_html = 'No reviews yet.';
		}

		return $str_recommendations_html;
	}

	public static function rating_stars_render( $data ) {
		if ( ! is_array( $data ) ) {
			return '';
		}
		$return_data = '';
		foreach ( $data as $rating_stars_render_array_val ) {
			$return_data .= '<img src="' . PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/images/search/' . $rating_stars_render_array_val['show_stars'] . '.png">';
		}

		return $return_data;
	}

	public static function locality_wise_data( $data ) {
		if ( ! is_array( $data ) ) {
			return false;
		}
		$returnData = '';
		foreach ( $data['locality_array'] as $locality ) {
			$subjects = $data['subjects'] !== 0 && $data['subjects'] !== '0' && empty( $data['subjects'] ) ? 'N/A' : $data['subjects'];
			$fees_types = $data['fees_types'] !== 0 && $data['fees_types'] !== '0' && empty( $data['fees_types'] ) ? 'N/A' : $data['fees_types'];
			$locality_str = $locality['locality'] !== 0 && $locality['locality'] !== '0' && empty( $locality['locality'] ) ? 'N/A' : $locality['locality'];
			$typeoflocation = $locality['typeoflocation'] !== 0 && $locality['typeoflocation'] !== '0' && empty( $locality['typeoflocation'] ) ? 'N/A' : $locality['typeoflocation'];
			$class_capacity = $data['class_capacity'] !== 0 && $data['class_capacity'] !== '0' && empty( $data['class_capacity'] ) ? 'N/A' : $data['class_capacity'];
			$course_length = $data['course_length'] !== 0 && $data['course_length'] !== '0' && empty( $data['course_length'] ) ? 'N/A' : $data['course_length'];
			$course_length_type = $data['course_length_type'] !== 0 && $data['course_length_type'] !== '0' && ( empty( $data['course_length_type'] ) || trim( $data['course_length_type'] ) == "" ) ? '' : ucfirst( trim( $data['course_length_type'] ) );
			$class_type = empty($data['class_type']) ? 'N/A' : $data['class_type']; 
			$locality_fees = $locality['locality_fees'];
			
			if(is_numeric($locality_fees)) {
				$fees_types = $locality_fees.'/Month';
			}
			
			$course_length_str = $course_length . " " . $course_length_type;
			if ( $course_length == "" || $course_length == "N/A" ) {
				$course_length_str = "N/A";
			}

			switch ( trim( strtolower( $course_length_str ) ) ) {
				case '1 years':
					$course_length_str = "1 Year";
					break;
				case '1 months':
					$course_length_str = "1 Month";
					break;
				case '1 days':
					$course_length_str = "1 Day";
					break;
				default:
					break;
			}

			$class_duration = $data['class_duration'] !== 0 && empty( $data['class_duration'] ) ? 'N/A' : $data['class_duration'];
			switch ( (float) $class_duration ) {
				case 1:
					$class_duration = $class_duration . " Hr";
					break;
				case 0:
					break;
				default:
					$class_duration = $class_duration . " Hrs";
					break;
			}
			
			$returnData .= '
				<div class="col-xs-12 course_item_block_wrapper">
					<div class="col-xs-0 hide course_item_block">
						<div class="inline text-bold item-desc">Subject</div>
						:
						<div class="xs-inline item text-left subjects">' . $subjects . '</div>
					</div>
					<div class="col-sm-2 course_item_block">
						<div class="inline text-bold item-desc">Fees</div>
						:
						<div class="xs-inline item text-left fees">' . $fees_types . '</div>
					</div>
					<div class="col-sm-2 course_item_block  ">
						<div class="inline text-bold item-desc">Location</div>
						:
						<div class="xs-inline item text-left locality">' . $locality_str . '</div>
					</div>
					<div class="col-sm-2 course_item_block">
						<div class="inline text-bold item-desc">Type of Location</div>
						:
						<div class="xs-inline item text-left typeoflocation">' . $typeoflocation . '</div>
					</div>
					<div class="col-sm-2 course_item_block">
						<div class="inline text-bold item-desc">Batch Size</div>
						:
						<div class="xs-inline item text-left class_capacity">' . $class_capacity . '</div>
					</div>
					<div class="col-sm-2 course_item_block  ">
						<div class="inline text-bold item-desc">Length</div>
						:
						<div class="xs-inline item text-left course_length">' . $course_length_str . '</div>
					</div>
					<div class="col-sm-2 course_item_block  ">
						<div class="inline text-bold item-desc">Time</div>
						:
						<div class="xs-inline item text-left class_duration">' . $class_duration . '</div>
					</div>
					<div class="col-sm-12 course_item_block  ">
						<div class="inline text-bold item-desc">Classes Taught</div>
						:
						<div class="xs-inline item text-left class_duration">'.$class_type.'</div>
					</div>
					<div class="col-xs-12 center-block text-center">
						<button type="button" class="btn btn-raised btn-sm white-text take_this_course_btn blue-shade" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#takethiscourse-callback-profile-modal">
							Take This Course
						</button>
					</div>
				</div>';
		}

		return $returnData;
	}

	public static function fn_return_teacher_notes( $teacher_user_id ) {
		global $wpdb;
		$teacher_notes_db = '';
		if ( is_numeric( $teacher_user_id ) && $teacher_user_id > 1 ) {
			$teacher_notes_db = $wpdb->get_results( "select * from pdg_teacher_notes where user_id = $teacher_user_id order by note_id desc" );
		}

		return $teacher_notes_db;
	}

	public static function fn_return_teacher_notes_html( $teacher_user_id ) {
		$notes_db = self::fn_return_teacher_notes( $teacher_user_id );

		$str_return = 'Not Available';

		if ( ! empty( $notes_db ) ) {
			$str_return = '';
			foreach ( $notes_db as $note_info ) {
				$note = stripslashes($note_info->note);
				$note_taker_name = stripslashes($note_info->note_taker_name);
				$note_timestamp = $note_info->note_date_time;
				$str_return .= '
					<div class="well">
						' . $note . '					
						<div style="text-align:right;">
							-- <strong>' . $note_taker_name . '</strong> on ' . date( "m-d-Y", strtotime( $note_timestamp ) ) . '
						</div>
					</div>
				';
			}
		}

		return $str_return;
	}

	public static function fn_return_teacher_notes_ajax() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => '',
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		$teacher_user_id = sanitize_text_field( $_POST['teacherInfo']['teacher_user_id'] );

		if ( ! is_numeric( $teacher_user_id ) || $teacher_user_id <= 1 ) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Teacher User ID is not correct. Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$teacher_note = self::fn_return_teacher_notes_html( $teacher_user_id );
		$return_message['error'] = false;
		$return_message['message'] = 'Notes reloaded successfully!';
		$return_message['data'] = $teacher_note;

		echo json_encode( $return_message );
		die();
	}

	public static function fn_save_teacher_notes_ajax() {
		global $wpdb;
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => '',
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}
		$teacher_user_id = sanitize_text_field( $_POST['notesData']['teacher_user_id'] );
		$notes_text = sanitize_text_field( $_POST['notesData']['teacher_note'] );
		$notes_taker = sanitize_text_field( $_POST['notesData']['note_taker'] );

		if ( ! is_numeric( $teacher_user_id ) || $teacher_user_id <= 1 ) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Teacher User ID is not correct. Please try again.';

			echo json_encode( $return_message );

			die();
		}

		if ( empty( $notes_text ) ) {
			$return_message['error_type'] = 'incomplete';
			$return_message['message'] = 'Please input notes text and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $notes_taker ) ) {
			$return_message['error_type'] = 'incomplete';
			$return_message['message'] = 'Please input your name and try again.';
			echo '<script type="text/javascript"> console.log(this);</script>';

			echo json_encode( $return_message );
			die();
		}

		$insert_array = array (
			'note_taker_name' => $notes_taker,
			'note'            => $notes_text,
			'user_id'         => $teacher_user_id,
		);
		$insert_array_data_format = array ( '%s', '%s', '%d' );
		$wpdb->insert( 'pdg_teacher_notes', $insert_array, $insert_array_data_format );

		$return_message['error'] = false;
		$return_message['message'] = 'Note was saved successfully!';

		echo json_encode( $return_message );
		die();
	}
    
    /**
      * internal rating && review 
      * @author Pritam Ghosh
      */
    public static function fn_return_teacher_internal_rating( $teacher_user_id ) {
		global $wpdb;
		$teacher_internal_rating_db = '';
		if ( is_numeric( $teacher_user_id ) && $teacher_user_id > 1 ) {
			$teacher_internal_rating_db = $wpdb->get_results( "select * from pdg_teacher_internal_rating where user_id = $teacher_user_id order by internal_rating_id desc" );
		}

		return $teacher_internal_rating_db;
	}

    public static function fn_return_teacher_internal_rating_html( $teacher_user_id ) {
		$internal_rating_db = self::fn_return_teacher_internal_rating( $teacher_user_id );

		$str_return = 'Not Available';

		if ( ! empty( $internal_rating_db ) ) {
			$str_return = '';
			foreach ( $internal_rating_db as $internal_rating ) {
				$average_rating = $internal_rating->average_rating;
				$internal_review = stripslashes($internal_rating->internal_review);
				$rating_giver_name = stripslashes($internal_rating->rating_giver_name);
				$internal_rating_date_time = $internal_rating->internal_rating_date_time;
				$str_return .= '
					<div class="well">
					    <div>
					        Rated&nbsp&nbsp 
					        <span style="background-color:#216f3b;border:1px;color:#fff">' . $average_rating . '/5</span>
					    </div>
					     
						' . $internal_review . '
						<div style="text-align:right;">
							-- <strong>' . $rating_giver_name . '</strong> on ' . date( "m-d-Y", strtotime( $internal_rating_date_time ) ) . '
						</div>
					</div>
				';
			}
		}

		return $str_return;
		
	}

	public static function fn_return_teacher_internal_rating_ajax() {
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => '',
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		$teacher_user_id = sanitize_text_field( $_POST['teacherInfo']['teacher_user_id'] );

		if ( ! is_numeric( $teacher_user_id ) || $teacher_user_id <= 1 ) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Teacher User ID is not correct. Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$teacher_internal_rating = self::fn_return_teacher_internal_rating_html( $teacher_user_id );
		$return_message['error'] = false;
		$return_message['message'] = 'Rating reloaded successfully!';
		$return_message['data'] = $teacher_internal_rating;

		echo json_encode( $return_message );
		die();
	}

	/* internal rating insertion */
	public static function fn_save_teacher_internal_rating_ajax() {
		global $wpdb;
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => '',
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		$teacher_user_id = sanitize_text_field( $_POST['ratingData']['teacher_user_id'] );
		$punctuality_rating = sanitize_text_field( $_POST['ratingData']['punctuality_rating']);
		$responsiveness_rating = sanitize_text_field( $_POST['ratingData']['responsiveness_rating']);
		$behaviour_rating = sanitize_text_field( $_POST['ratingData']['behaviour_rating']);
		$communication_skills_rating = sanitize_text_field( $_POST['ratingData']['communication_skills_rating']);
		//$average_rating = sanitize_text_field( $_POST['ratingData']['average_rating']);
		$teacher_reviews_text = sanitize_text_field( $_POST['ratingData']['teacher_reviews_text'] );
		$rating_giver_name = sanitize_text_field( $_POST['ratingData']['rating_giver_name'] );
                $classes_accepted = sanitize_text_field( $_POST['ratingData']['classes_accepted'] );
		$classes_provided = sanitize_text_field( $_POST['ratingData']['classes_provided'] );
		$demo_converted = sanitize_text_field( $_POST['ratingData']['demo_converted'] );
		$demo_provided = sanitize_text_field( $_POST['ratingData']['demo_provided'] );
		$classes_ongoing = sanitize_text_field( $_POST['ratingData']['classes_ongoing'] );
		$payment_plan = sanitize_text_field( $_POST['ratingData']['payment_plan'] );
		$average_rating=($punctuality_rating+$responsiveness_rating+$behaviour_rating+$communication_skills_rating)/4;
		if ( ! is_numeric( $teacher_user_id ) || $teacher_user_id <= 1 ) {
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Teacher User ID is not correct. Please try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $punctuality_rating ) && empty( $responsiveness_rating ) && empty( $behaviour_rating ) && empty( $communication_skills_rating ) ) {
			$return_message['error_type'] = 'incomplete';
			$return_message['message'] = 'Please input ratings and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $teacher_reviews_text ) ) {
			$return_message['error_type'] = 'incomplete';
			$return_message['message'] = 'Please input review text and try again.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $rating_giver_name ) ) {
			$return_message['error_type'] = 'incomplete';
			$return_message['message'] = 'Please input your name and try again.';

			echo json_encode( $return_message );
			die();
		}

		$insert_array = array (
			'user_id'         => $teacher_user_id,
			'punctuality_rating' => $punctuality_rating,
			'responsiveness_rating' => $responsiveness_rating,
			'behaviour_rating' => $behaviour_rating,
			'communication_skills_rating' => $communication_skills_rating,
			'average_rating' => $average_rating,
			'internal_review' => $teacher_reviews_text,
			'rating_giver_name' => $rating_giver_name,
			'classes_accepted'  => $classes_accepted,
			'classes_provided' => $classes_provided,
			'demo_converted'  => $demo_converted,
			'demo_provided' => $demo_provided,
			'classes_ongoing' => $classes_ongoing,
			'payment_plan' => $payment_plan,
		);
		$insert_array_data_format = array ( '%d','%d','%d','%d','%d','%f','%s','%s','%d','%d','%d','%d','%d','%s' );
		$wpdb->insert( 'pdg_teacher_internal_rating', $insert_array, $insert_array_data_format );

		$return_message['error'] = false;
		$return_message['message'] = 'Rating was saved successfully!';

		echo json_encode( $return_message );
		die();
	}

	/* internal rating insertion end */

	public static function get_batch_school_information($tuition_batch_data) {
		global $wpdb;
		$str_schools = 'N/A';
		if(!empty($tuition_batch_data)) {
			$batch_id_array = array();
			foreach($tuition_batch_data as $batch_data) {				
				$batch_id = $batch_data->tuition_batch_id;
				if(!in_array($batch_id, $batch_id_array)) {
					$batch_id_array[] = $batch_id;
				}
			}
			$str_sql = "
				select 
					pdg_batch_school.*,
					pdg_schools.school_name
				from pdg_batch_school
				left join pdg_schools on pdg_schools.school_id = pdg_batch_school.pdg_school_id
				where pdg_batch_school.pdg_batch_id in (".implode(',', $batch_id_array).")				
			";
			
			//pedagoge_applog($str_sql);
			$schools_data = $wpdb->get_results($str_sql);
			if(!empty($schools_data)) {
				$str_schools = '';
				foreach($schools_data as $school_info) {
					$str_schools.= $school_info->school_name.', ';
				}
			}
		}
		return $str_schools;
	}
	
	/**
	 * Function to return link of empty profile images
	 * 
	 * @param role - teacher/institute
	 * @param gender - default is male
	 */
	public static function get_empty_profile_picture($role, $gender='male') {		
		$profile_pic = $male_teacher_profile = PEDAGOGE_ASSETS_URL . '/images/male_teacher.png';
		$institute_profile = PEDAGOGE_ASSETS_URL . '/images/institute.png';
		$female_teacher_profile = PEDAGOGE_ASSETS_URL . '/images/female_teacher.png';
				
		switch($role) {
			case 'institute':
				$profile_pic = $institute_profile;
				break;
			case 'teacher':
				if($gender == 'female') {
					$profile_pic = $female_teacher_profile;
				} 
				break;			
		}
		return $profile_pic;
	}
	
	
	/**

     * Function to return class details

     */

	public static function fn_get_lastInsertedRating( $teacher_user_id ) {

		global $wpdb;

		$rating_details = array();



        $punctuality_rating = "select punctuality_rating from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `punctuality_rating` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

        if ( $punctuality_rating = $wpdb->get_var( $punctuality_rating ) ) {

        	$rating_details[0] = $punctuality_rating ;

        }

        else {

        	$rating_details[0] = 0 ;

        }



        $responsiveness_rating = "select responsiveness_rating from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `responsiveness_rating` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

        if ( $responsiveness_rating = $wpdb->get_var( $responsiveness_rating ) ) {

        	$rating_details[1] = $responsiveness_rating ;

        }

        else {

        	$rating_details[1] = 0 ;

        }



        $behaviour_rating = "select behaviour_rating from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `behaviour_rating` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

        if ( $behaviour_rating = $wpdb->get_var( $behaviour_rating ) ) {

        	$rating_details[2] = $behaviour_rating ;

        }

        else {

        	$rating_details[2] = 0 ;

        }



        $communication_skills_rating = "select communication_skills_rating from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `communication_skills_rating` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

        if ( $communication_skills_rating = $wpdb->get_var( $communication_skills_rating ) ) {

        	$rating_details[3] = $communication_skills_rating ;

        }

        else {

        	$rating_details[3] = 0 ;

        }



        return $rating_details;

	}



    

    /**

     * Function to return class details

     */

	public static function fn_get_classesDetails( $teacher_user_id ) {

		global $wpdb;

		$classes_details_data = array();



        $classes_accepted = "select classes_accepted from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `classes_accepted` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

        if ( $classes_accepted = $wpdb->get_var( $classes_accepted ) ) {

        	$classes_details_data[0] = $classes_accepted ;

        }

        else {

        	$classes_details_data[0] = 0 ;

        }

        

		$classes_provided = "select classes_provided from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `classes_provided` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

		if ( $classes_provided = $wpdb->get_var( $classes_provided ) ) {

        	$classes_details_data[1] = $classes_provided ;

        }

        else {

        	$classes_details_data[1] = 0 ;

        }

		

		$demo_converted = "select demo_converted from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `demo_converted` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

		if ( $demo_converted = $wpdb->get_var( $demo_converted ) ) {

        	$classes_details_data[2] = $demo_converted ;

        }

        else {

        	$classes_details_data[2] = 0 ;

        }

		

		$demo_provided = "select demo_provided from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `demo_provided` <> 0 ORDER by internal_rating_date_time DESC limit 0,1";

		if ( $demo_provided = $wpdb->get_var( $demo_provided ) ) {

        	$classes_details_data[3] = $demo_provided ;

        }

        else {

        	$classes_details_data[3] = 0 ;

        }

		

		$classes_ongoing = "select classes_ongoing from pdg_teacher_internal_rating where user_id=$teacher_user_id ORDER by internal_rating_date_time DESC limit 0,1";

		if ( $classes_ongoing = $wpdb->get_var( $classes_ongoing ) ) {

        	$classes_details_data[4] = $classes_ongoing ;

        }

        else {

        	$classes_details_data[4] = 0 ;

        }



		return $classes_details_data;

	}



   /**

     * Function to return last payment plan for teacher

     */



	public static function fn_get_lastInsertedPaymentPlan( $teacher_user_id ) {

		global $wpdb;



		$payment_plan = "select payment_plan from pdg_teacher_internal_rating where user_id=$teacher_user_id AND `payment_plan` IS NOT NULL ORDER by internal_rating_date_time DESC limit 0,1";



		if ( $payment_plan = $wpdb->get_var( $payment_plan ) ) {

        	$payment_plan = $payment_plan ;

        }

        else {

        	$payment_plan = 0 ;

        }

        return $payment_plan;

	}

}

