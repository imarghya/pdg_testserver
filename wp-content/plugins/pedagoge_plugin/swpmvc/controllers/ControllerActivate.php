<?php
/**
 * Contains the class to generate the page for the route '/activate'.
 * 
 * @author Niraj Kumar 
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Contains functions to generate the page for the route '/activate' and mail a account verification/activation link. Allows users to verify and activate their account by following the account verification link mailed to them.
 * 
 * @author Niraj Kumar  
 */
class ControllerActivate extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge Account Activation';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}
	
	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$activate_version = '0.1';
		
		unset($this->css_assets['waves']);
		unset($this->css_assets['ankitesh-validationEngine']);
		unset($this->css_assets['offcanvasmenueffects']);
		unset($this->css_assets['3d-bold-navigation']);
		unset($this->css_assets['ankitesh-search']);
		unset($this->css_assets['ankitesh-modern']);
		unset($this->css_assets['ankitesh-theme-green']);
		unset($this->css_assets['ankitesh-custom']);		
		unset($this->header_js['ankitesh-forms']);
		unset($this->footer_js['validationengine']);
		unset($this->footer_js['validationengine-en']);
		unset($this->footer_js['ankitesh-modern']);
		unset($this->footer_js['offcanvasmenueffects']);
		
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
		//$this->css_assets['custom_backend'] = PEDAGOGE_PROUI_URL."/css/custom_backend.css?ver=".$this->fixed_version;
		
	    $this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		$this->app_js['pedagoge_activate'] = PEDAGOGE_ASSETS_URL."/js/user_activate.js?ver=".$activate_version;
				
	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}
	
	public function fn_get_content() {
		global $wpdb;
		$content = '';
		
		$activation_key = '';
		if(isset($_GET['activation_key'])) {
			$activation_key = sanitize_text_field($_GET['activation_key']);
			
			$columns_array = array(
				'activation_key' => $activation_key,
				'deleted' => 'no'
			);
			
			$master_model = new ModelMaster();
			$user_info_data = $master_model->get_records_by_table_name('view_pdg_user_info', $columns_array);
			if(empty($user_info_data)) {
				$content = '				
					<section class="site-section site-section-top">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<div class="panel panel-danger">
									<div class="panel-heading">Invalid Activation Key!</div>
									<div class="panel-body">
										<h4>Invalid Activation Key!</h4> 
										<p>This activation key is invalid. Please check your email for proper account activation link!</p> 
										<p><strong>Redirecting now...</strong></p>
										<META http-equiv="refresh" content="3; URL='.home_url('/profile').'">
									</div>
								</div>
							</div>
						</div>
					</section>	
				';
				return $content;
			} else {
				
				$personal_info_id = $user_email = '';
				foreach($user_info_data as $user_info) {
					$personal_info_id = $user_info->personal_info_id;
					$user_email = $user_info->user_email;
				}
				
				if(!is_numeric($personal_info_id)) {
					$content = '
					
						<section class="site-section site-section-top">
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<div class="panel panel-danger">
										<div class="panel-heading">User Not Found!</div>
										<div class="panel-body">
											<h4>User Not Found!</h4> 
											<p>This activation key is not connected with any user. Please check your email for proper account activation link!</p> 
											<p><strong>Redirecting now...</strong></p>
											<META http-equiv="refresh" content="3; URL='.home_url('/profile').'">
										</div>
									</div>
								</div>
							</div>
						</section>
					';
					return $content;
				}
				
				$salt = wp_generate_password(20); // 20 character "random" string
				$changed_activation_key = sha1($salt . $user_email . uniqid(time(), true));
				
				$data_array = array(
					'profile_activated' => 'yes',
					'activation_key' => $changed_activation_key
				);
				
				$data_format_array = array(					
					'%s',
					'%s'					
				);
				
				$where_data_array = array(
					'personal_info_id' => $personal_info_id
				);
				$where_data_format_array = array(
					'%d'
				);
				
				$result = $wpdb->update( 'pdg_user_info', $data_array, $where_data_array, $data_format_array, $where_data_format_array );
				
				if(FALSE === $result) {
					$content = '
						<section class="site-section site-section-top">
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<div class="panel panel-danger">
										<div class="panel-heading">Profile activation error!</div>
										<div class="panel-body">
											<h4>Profile activation error!</h4> 
											<p>Sorry! Profile could not be activated due to database error! Please try again!</p> 
											<p><strong>Redirecting now...</strong></p>
											<META http-equiv="refresh" content="3; URL='.home_url('/profile').'">
										</div>
									</div>
								</div>
							</div>
						</section>	
					';
					return $content;
				} else {
										
					$content='
						<section class="site-section site-section-top">
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<div class="panel panel-success">
										<div class="panel-heading">Profile activation was successful!</div>
										<div class="panel-body">
											<h4>Profile activation was successful!</h4> 
											<p><strong>Redirecting now...</strong></p>
											<META http-equiv="refresh" content="3; URL='.home_url('/profile').'">
										</div>
									</div>
								</div>
							</div>
						</section>						
					';
					ControllerActivate::fn_send_welcome_email($user_email);
					return $content;
				}
			}			
		} else {
			//show activation form
			$current_user_id = $this->app_data['pdg_current_user']->ID;
			
			$str_profile_active_message = '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="panel panel-success">
								<div class="panel-heading">Your profile was already activated.</div>
								<div class="panel-body">
									<h4>Your profile was already activated.</h4>
									<p><strong>Redirecting now...</strong></p>
									<META http-equiv="refresh" content="3; URL='.home_url('/profile').'">
								</div>
							</div>
						</div>
					</div>
				</section>				
			';
			
			if(!empty($current_user_id) && $current_user_id!=0) {
								
				$master_model = new ModelMaster();
				$pdg_user_info = $master_model->get_records_by_table_name('view_pdg_user_info', array('user_id'=>$current_user_id));
				if(!empty($pdg_user_info)) {
						
					$email_address = '';
					$activation_key = '';
					$profile_activated = 'no';
					//pedagoge_applog(print_r($pdg_user_info, TRUE));
					foreach($pdg_user_info as $user_info) {
						$email_address = $user_info->user_email;
						$activation_key = $user_info->activation_key;
						$profile_activated = $user_info->profile_activated;
					}
					
					if($profile_activated == 'yes') {
						return $str_profile_active_message;
					} else {
						$template_vars = array(
							'activation_key' => $activation_key,
							'email_address' => $email_address
						);
						
						return $this->load_view('account_activation/index', $template_vars);	
					}
					
				} else {					
					return $str_profile_active_message;	
				}				
				
			} else {
				
				return $str_profile_active_message;
			}
		}
			
		return $content;
	}
	
	public static function send_activation_email() {
		
		$return_message = array(
			'error' => TRUE,
			'loggedin' => FALSE,
			'error_type' => '',
			'message' => ''
		);
		
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$secret_key = sanitize_text_field($_POST['account_activation_key']);
		$email_address = sanitize_email($_POST['email_address']);
		
		if(!empty($secret_key) && !empty($email_address)) {
			//send mail
			$account_activation_url = add_query_arg( 'activation_key', $secret_key, home_url('/activate') );
			
			//Send Account Activation Email
			//$account_activation_url = add_query_arg( 'activation_key', $key, home_url('/activate') );
			$mail_data = array(
				'to_address' => $email_address,
				'mail_type' => 'user_signup_and_account_activation',
				'account_activation_url' => $account_activation_url
			);
			$pedagoge_mailer = new PedagogeMailer($mail_data);
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$pedagoge_mailer = null;
			
			$return_message['error'] = FALSE;
			$return_message['message'] = 'Account activation email was sent successfully! Please check your inbox.';
			
			echo json_encode($return_message );
			die();
			
		} else {
			//send error
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Error! Something did not work properly! Please try again.';
			
			echo json_encode($return_message );
			die();
		}
		
		echo 'Success!';
		die();
	}
		
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
	}
	
	public static function fn_send_welcome_email($user_email) {
		//account_activated_welcome_email
		//account_activation_notification	
		if(!empty($user_email)) {
			// send welcome email to user
			$mail_data = array(
				'to_address' => $user_email,
				'mail_type' => 'account_activated_welcome_email',
			);
			$pedagoge_mailer = new PedagogeMailer($mail_data);
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$pedagoge_mailer = null;
			
			// send account activation confirmation
			$mail_data = array(
				'to_address' => 'ask@pedagoge.com',
				'mail_type' => 'account_activation_notification',
				'user_email' => $user_email
			);
			$pedagoge_mailer = new PedagogeMailer($mail_data);
			$mail_process_completed = $pedagoge_mailer->sendmail();
			$pedagoge_mailer = null;	
			
		}
	}

}
