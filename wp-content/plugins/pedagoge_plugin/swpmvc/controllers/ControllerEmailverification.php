<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerEmailverification extends ControllerMaster implements ControllerMasterInterface {
	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Workshop';
		$this->body_class .= ' page-register login-alt page-header-fixed';
		//$this->fn_load_scripts();
		//$this->fn_register_common_variables();
	}
	
	
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		//$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		return $this->load_view('email_verification/teacher/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		return $this->load_view('email_verification/teacher/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		//$template_vars['example_string'] = 'Hello World!';
		//$template_vars['example_number'] = 4;
		
		//$content = $this->load_view('settings/index', $template_vars);		
		$content = $this->load_view('email_verification/teacher/index');	
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	
	// Example of Ajax request processing
	
	
	
}
		
		
		
	




