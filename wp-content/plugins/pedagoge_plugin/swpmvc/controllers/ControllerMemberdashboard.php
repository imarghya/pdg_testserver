<?php
session_start();
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerMemberdashboard extends ControllerMaster implements ControllerMasterInterface {
	
	
	public function __construct() {
		// nothing to do here.
		// Only master controller gets autoconstructed.		 
	}
	
	public function fn_construct_class() {
		//Initialize Master Controller
		parent::__construct(); 
		
		//Page Title
		$this->title = 'Settings Page:';
		
		//You can load your custom assets (css/js) specific to this page. 
		//Master page has already loaded all the required scripts for a blank page.
		$this->fn_load_scripts(); 
		
		//Set this variable to define body class of your page.
		$this->body_class .= ' page-register login-alt page-header-fixed';
		
		//Register variables required in the global scope. 
		//Variables are stored in $this->app_data array variable.
		$this->fn_register_common_variables();	

			
	}
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		//$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		return $this->load_view('memberdashboard/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		return $this->load_view('memberdashboard/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		//$template_vars['example_string'] = 'Hello World!';
		//$template_vars['example_number'] = 4;
		
		//$content = $this->load_view('settings/index', $template_vars);		
		$content = $this->load_view('memberdashboard/index');	
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		//Available for the page at global scope.
		$this->app_data['global_example_string'] = 'example string';
		
		//If you wish to include a dynamic inline javascript code then use the variable 'inline_js' available in the $app_data variable.
		// This variable is a string, so append your script string to it. e.g.
		$this->app_data['inline_js'] .= '<script type="text/javascript">console.log("Testing");</script>';
	}
	
	// Example of Ajax request processing
	
	//********************************************************New Query*********************************************************
	//For get new query subject from------------------------
	public function get_subject_from(){
	 $x=1;
	 $i=1;
	 $subject_name=array();
	 $subjects=$_POST['subjects'];
	 $selected_sub=explode(",",$subjects);
		$str="";
		$num_sub=0;
		foreach($selected_sub as $selsub) {
			$str.="'".$selsub."',";
				$num_sub++;
		}
	  $str=rtrim($str,",");
		
	 $query_model = new ModelQuery();
	 $result=$query_model->get_subject_with_type($str);
	 //print_r($result);
	 //exit;
	 foreach($result AS $res){ $subject_name[$i]=$res->subject_name; $i++;}
	 foreach($result AS $res){
	 $course_type_id_arr=$query_model->course_type_id_arr($res->subject_name_id);
	 //print_r($course_type_id_arr);
	 ?>
	 
	  <aside class="model_wrapper">
	       <div class="modal fade" data-backdrop="static" data-keyboard="false"  id="subjectSelect<?= $x; ?>" role="dialog">
	       <div class="modal-dialog modal-dialog-next">
	       <!-- Modal content-->
	       <div class="modal-content blue_border">
	       <div class="modal-header">
	       <button type="button" class="close" data-dismiss="modal">&times;</button>
	       <ul class="acadamic_subject_form">
	       
	       <?php
	       if($x==1){
	       ?>
		<li><a href="#"  data-toggle="modal" data-target="#"><i ></i></a></li>
	       <?php										
	       }else{
	       ?>
		<li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelect<?= $x-1; ?>" data-dismiss="modal">
		
        <i class="fa fa-caret-left" aria-hidden="true"></i>
		<div id="selsubjectleft<?= $x; ?>"><?php echo $subject_name[$x-1]; ?></div>
		
		
	       </a>
	       </li>
	       <?php
	       }	
	       ?>
	       <li class="active_subject"><a href="#"><div id="mysubject"><div id="selsubject<?= $x; ?>"><?php echo $subject_name[$x]; ?></div></div></a></li>
	       <?php if(count($result) >1 && count($result)!=$x ){ ?>
	       <li>
	       <a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelect<?= $x+1 ?>" data-dismiss="modal" id="next<?= $x ?>">
	       <div id="selsubjectright<?= $x ?>"><?php echo $subject_name[$x+1]; ?></div>
	       <i class="fa fa-caret-right" aria-hidden="true"></i>
	       </a>
	       </li>
	       <?php } ?>
	       </ul>
	       </div>
		<form id="frm_submit_new<?= $x; ?>" method="post" class="subject_form" idd="<?= $x; ?>" subject="<?php echo $res->subject_name_id; ?>" totalx="<?php echo count($result); ?>">
			<input type="hidden" id="query_id" name="query_id" value="" />
			<div class="modal-body no_padding_top_bottom">
			<div class="row">
			
			<?php
			if($x==1){}else{
			?>
			<div class="col-sm-12 subject_type no_full_width">
			<label for="sameAs">Same as Previous</label> <input class="non_grid_input same_as_prv" idd="<?php echo $x-1; ?>" type="checkbox" name="sameAs">
			</div>
			<?php
			}
			?>
			<?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
			 <div class="col-sm-12 subject_type">
			 <input placeholder="Age"  value="" required maxlength="2" name="age" id="age_<?php echo $x; ?>" pattern="\d*" class="validate[required,custom[integer],max[2]] text-input non_grid_input black_border non_grid_input black_border" type="text">
			 </div>
			 <?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
			 
			<div class="col-sm-12 subject_type">
			<select  name="school" id="school_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]" >
			<option value="">SCHOOL/COLLEGE</option>
			<optgroup label="SCHOOL">
			<?php
			 $schools=$query_model->get_school_list();     
			 foreach($schools as $r){
			?>
			<option value="school-<?= $r->school_id ?>" data-type="school"><?= $r->school_name ?></option>
			<?php
			}
			?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
				$colleges = $query_model->get_college_list();     
				foreach($colleges as $college){
			?>
				<option value="college-<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="board" id="board_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">BOARD/UNIVERSITY</option>
			<optgroup label="BOARD">
			<?php
			 $boards=$query_model->get_board_list();
			 foreach($boards as $r){
			?>
			<option value="board-<?= $r->academic_board_id ?>" data-type="board"><?= $r->academic_board ?></option>
			<?php
			 }
			?>
			</optgroup>
			<optgroup label="UNIVERSITY">
			<?php
			$university = $query_model->get_university_list();
			foreach($university as $u){
			?>
				<option value="university-<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="standard" id="standard_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">CLASS/YEAR</option>
			<?php /*?><?php
			 $standards=$query_model->get_standard_list($res->subject_name_id);  
			 foreach($standards as $r){
			?>
			<option value="<?= $r->subject_type ?>"><?= $r->subject_type ?></option>
			<?php
			 }
			?>
			<?php */?>
			<?php
			$a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			$a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
			//$standards=$query_model->get_standard_list($res->subject_name_id);  
			 ?>
			 <optgroup label="SCHOOL">
			 <?php
			 foreach($a_school_standards as $s){
			 ?>
			 <option value="<?= $s ?>"><?= $s ?></option>
			 <?php
			 }
			 ?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
			 foreach($a_college_standards as $c){
			 ?>
			 <option value="<?= $c ?>"><?= $c ?></option>
			 <?php
			 }
			 ?>
			</optgroup>
			</select>
			</div>
			<?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
			<div class="col-sm-12 subject_type">
			 <input placeholder="Age"  value="" maxlength="2" required name="age" id="age_<?php echo $x; ?>" pattern="\d*" class="validate[required,custom[integer],max[100]] text-input non_grid_input black_border non_grid_input black_border" type="text">
			 </div>
			
			<div class="col-sm-12 subject_type">
			<select name="school" id="school_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]" >
			<option value="">SCHOOL/COLLEGE</option>
			<optgroup label="SCHOOL">
			<?php
			 $schools=$query_model->get_school_list();     
			 foreach($schools as $r){
			?>
			<option value="school-<?= $r->school_id ?>" data-type="school"><?= $r->school_name ?></option>
			<?php
			}
			?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
				$colleges = $query_model->get_college_list();     
				foreach($colleges as $college){
			?>
				<option value="college-<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="board" id="board_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">BOARD/UNIVERSITY</option>
			<optgroup label="BOARD">
			<?php
			 $boards=$query_model->get_board_list();
			 foreach($boards as $r){
			?>
			<option value="board-<?= $r->academic_board_id ?>" data-type="board"><?= $r->academic_board ?></option>
			<?php
			 }
			?>
			</optgroup>
			<optgroup label="UNIVERSITY">
			<?php
			$university = $query_model->get_university_list();
			foreach($university as $u){
			?>
				<option value="university-<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="standard" id="standard_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">CLASS/YEAR</option>
			<?php /*?><?php
			 $standards=$query_model->get_standard_list($res->subject_name_id);  
			 foreach($standards as $r){
			?>
			<option value="<?= $r->subject_type; ?>"><?= $r->subject_type; ?></option>
			<?php
			 }
			?>
			<?php */?>
			<?php
			$a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			$a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
			//$standards=$query_model->get_standard_list($res->subject_name_id);  
			 ?>
			 <optgroup label="SCHOOL">
			 <?php
			 foreach($a_school_standards as $s){
			 ?>
			 <option value="<?= $s ?>"><?= $s ?></option>
			 <?php
			 }
			 ?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
			 foreach($a_college_standards as $c){
			 ?>
			 <option value="<?= $c ?>"><?= $c ?></option>
			 <?php
			 }
			 ?>
			</optgroup>
			</select>
			</div>
			<?php } ?>
			<div class="col-sm-12 subject_type">
			<p class="text-center option_heading">Select your preferred option</p>
			<ul class="place_option">
			<input id="prefer_location_<?php echo $x; ?>" type="text" name="" value="" required style="position:absolute; border: 0; opacity: 0; background: #f1f1f2; left:0; right:0;" />
			<li><input class="prefer_location" type="checkbox" name="myhome" value="1" id="myhome_<?php echo $x; ?>" data-tt="<?= $x;?>"><label for="myhome_<?php echo $x; ?>">My Home</label></li>
			<li><input type="checkbox" class="prefer_location" name="tutorhome" value="1" id="tutorhome_<?php echo $x; ?>" data-tt="<?= $x;?>"><label for="tutorhome_<?php echo $x; ?>">Tutors Home</label></li>
			<li><input class="prefer_location" type="checkbox" name="institute" value="1" id="institute_<?php echo $x; ?>" data-tt="<?= $x;?>"><label for="institute_<?php echo $x; ?>">Institute</label></li>
			</ul>
			<!--<input type="hidden" name="localities" id="localities">-->
			<div class="col-sm-12 no_padding subject_type angle">
			<input id="hid_localities_<?php echo $x; ?>" type="text" name="" value="" required style="position:absolute; border: 0; opacity: 0; background: #f1f1f2;">
			<select name="localities[]" id="localities_<?php echo $x; ?>" class="non_grid_input black_border form-control mlc sub_locality" multiple>
			<option value="">MENTION LOCALITIES</option>
			<?php
			 $localities=$query_model->get_locality_list();     
			 foreach($localities as $r){
			?>
			<option value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
			<?php
			 }
			?>
			</select>
			</div>
			
			<input required value="" class="mentions_localitites black_border validate[required] new_query_start_date" id="start_date_<?php echo $x; ?>" name="start_date"  placeholder="When do you want the classes to begin" type="text" />
			<textarea class="validate[required] text-input mentions_localitites black_border min_height_130" id="requirement_<?php echo $x; ?>" name="requirement" placeholder="Please mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching."></textarea>
			</div>
			</div>
			</div>
			<div class="modal-footer no_padding_top_bottom">
			 <?php //if(count($result)==$x){ ?>
			 <!--<button type="submit" idd="<?= $x; ?>" subject="<?php echo $res->subject_name_id; ?>" data-toggle="modal" data-target="#nameaddress" data-dismiss="modal" class="data_submit qdata_submit blue_background color_white">Submit</button>-->
			 <?php //}else{ ?>
			 <button type="submit" idd="<?= $x; ?>" subject="<?php echo $res->subject_name_id; ?>"  class="data_submit qdata_submit blue_background color_white">Submit</button>
			 <?php //} ?>
			</div>
		</form>
	       </div>
	       </div>      
	       </div>
	       </aside>
	 <?php
	 $x++;	
	 }
	 $stu_data=$query_model->get_student_details($_SESSION['member_id']);
	 foreach($stu_data AS $stu){
	 ?>
	<aside class="model_wrapper">
	<div class="modal fade" id="nameaddress" name="nameaddress" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-dialog-next">
	<!-- Modal content-->
	<div class="modal-content blue_border">
	<form method="post" id="frmnameemail">
	<input type="hidden" id="x" value="<?= $x-1; ?>">
	<div class="modal-body no_padding_top_bottom">
	<div class="row">
	<div class="col-sm-12 subject_type address_type">
	<input placeholder="Name" required id="studname" name="studname" class="validate[required] text-input non_grid_input black_border" type="text" readonly value="<?php echo $stu->display_name; ?>" />
    
    
    <div class="goback_submit">
		<input placeholder="Phone Number" pattern="[0-9]{10}" required id="phonenum" name="phonenum" class="validate[required,custom[phone]] text-input non_grid_input black_border" type="text" value="<?php echo $stu->mobile_no; ?>" readonly /><a href="javascript:void(0);" data-toggle="modal" data-target="#stu_phone_verify_modal" title="Change Mobile"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
        
    </div>
    
    <div class="goback_submit">
	<input placeholder="Email Id" required  id="emailid" name="emailid" class="validate[required,custom[email]] text-input non_grid_input black_border" type="email" value="<?php echo $stu->user_email; ?>" readonly /><a href="javascript:void(0);" data-toggle="modal" data-target="#stu_email_verify_modal" title="Change Email"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
    </div>
    
	</div>
	</div>
	</div>
	<div class="modal-footer no_padding_top_bottom">
	<button type="button"  data-toggle="modal" data-target="#subjectSelect<?php echo $x-1; ?>" data-dismiss="modal"  class="data_submit blue_background color_white">Go Back</button>
	<button type="submit" id="submit_all_new_query" class="data_submit blue_background color_white">Submit</button>
	</div>
	</form>
	</div>
	</div>
	</div>
	</aside>

	<div class="modal fade" id="stu_phone_verify_modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
       <div class="modal-content">
         
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h5 class="modal-title phone_veri" id="avatar-modal-label">Phone Verification</h5>
             </div>

            <form class="avatar-form" id="otp_form_submit" enctype="multipart/form-data" method="post">
	        	<div class="modal-body">
	            	<div class="form-group margin_top_10">
	                	<label for="recipient-name" class="form-control-label">Enter Your Phone Number:</label>
	                	<input type="text" required class="form-control" value="" id="stu_phone_number">
	             	</div>
	             	<button type="button" id="stu_verify_phone" class="btn btn-info">Verify</button>
                	<!--<button type="submit" id="st_add_loc_btn" class="btn btn-primary">Submit</button>-->
	        	</div>
         	</form>
              <form class="avatar-form" id="otp_form_submit" enctype="multipart/form-data" method="post">
              <div class="modal-body">
				<span class="otp_heading">An OTP will be sent to your your New phone number : <span id="mob_no"></span></span>
                <div class="form-group margin_top_10">
                    <label for="recipient-name" class="form-control-label">Enter OTP Code:</label>
                    <input type="text" required class="form-control" value="" id="otp_code">
                </div>
             </div>
	      <div class="modal-footer">
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <!--<button type="button" id="resend_otp" class="btn btn-info">Resend OTP</button>-->
                <button type="button" id="stu_check_otp" class="btn btn-primary">Submit</button>
              </div>
             </form>
       </div>
    </div>
</div>
<div class="modal fade" id="stu_email_verify_modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
       <div class="modal-content">
         
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h5 class="modal-title phone_veri" id="avatar-modal-label">Email Verification</h5>
             </div>

            <form class="avatar-form" id="otp_form_submit" enctype="multipart/form-data" method="post">
	        	<div class="modal-body">
	            	<div class="form-group margin_top_10">
	                	<label for="recipient-name" class="form-control-label">Enter Your Email:</label>
	                	<input type="text" required class="form-control" value="" id="stu_email">
	             	</div>
	             	<button type="button" id="stu_verify_email" class="btn btn-info">Verify</button>
                	<!--<button type="submit" id="st_add_loc_btn" class="btn btn-primary">Submit</button>-->
	        	</div>
         	</form>
              <form class="avatar-form" id="otp_form_submit" enctype="multipart/form-data" method="post">
              <div class="modal-body">
				<span class="otp_heading">A verification code Will be sent to your Mail ID : <span id="mob_no"></span></span>
                <div class="form-group margin_top_10">
                    <label for="recipient-name" class="form-control-label">Enter Code:</label>
                    <input type="text" required class="form-control" value="" id="email_code">
                </div>
             </div>
	      <div class="modal-footer">
                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <!--<button type="button" id="resend_otp" class="btn btn-info">Resend OTP</button>-->
                <button type="button" id="stu_check_email" class="btn btn-primary">Submit</button>
              </div>
             </form>
       </div>
    </div>
</div>
	 <?php
	 }
	 die();
	}

	public function send_otp_code(){
		$phone_no=$_POST['phone_no'];
		$otp_code=rand(000000,999999);
		$sms_text1="Your OTP Code is:".$otp_code;
		//send sms--------------------------
		$api_key = '559E5AAED983CA';
		$contacts = $phone_no;
		$from = 'PDAGOG';
		$sms_text = urlencode($sms_text1);
		//Submit to server
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, "http://www.sambsms.com/app/smsapi/index.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=7&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
		$response = curl_exec($ch);
		curl_close($ch);
		//echo $response;
		//----------------------------------
		//$otp_code=123;
		$_SESSION['stu_otp_code'] = $otp_code;
		die();
	}

	//-----------------------------------------------------------------
	//Check otp code---------------------------------------------------
	public function check_otp_code(){
		//$teacher_id=$_SESSION['member_id'];
		//$teacher_model = new ModelTeacher();
		$otp_code = $_POST['otp_code'];
		if($otp_code == $_SESSION['stu_otp_code']){
			//$res=$teacher_model->phone_verification($teacher_id);
			echo "success"; 
		}
		else{
			echo "fail"; 
		}
	die();
	}

	public function send_email_code(){
		$email_id = $_POST['email'];
		$email_code = rand(000000,999999);
		$email_text = "Your Verification Code is:".$email_code;
		$message='<!doctype html>
		<html>
		<head>
		<meta charset="utf-8">
		<title>Pedagoge</title>
		<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
		</head>
		<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;">
				<section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;">
				<div style="width:100%;">
				<div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;">
					<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">                    
				</div>
				<div style="position:relative;background:#fff;">
					<div style="position:relative;background-color:#13a89e;top:-60px; width:80%; display:table;margin:0 auto; left:0; right:0;padding:8px 8px 0 8px;-webkit-box-shadow: 0px 5px 32px -4px rgba(0,0,0,0.7); -moz-box-shadow: 0px 5px 32px -4px rgba(0,0,0,0.7); box-shadow: 0px 5px 32px -4px rgba(0,0,0,0.7);">
					<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/text.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">
				    </div>
				    <div style="margin:0 0 45px; text-align:center;">
					<a href="javascript:void(0)" style="display:block;text-align:center; font-weight:bold; padding:10px 15px; border:0px solid #13a89e; color:#000;"><strong>'.$email_text.'</strong></a>
					<h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Simply verify your email and you’re good to go.</h1>
					<div style="margin:30px 0 15px">
						<ul style="list-style-type:none;text-align:center; padding:0;">
						<li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="Twtter" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li>
						<li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="FB" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li>
						<!--<li style="display:inline-block;margin-right:5px;"><a href="#" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><i class="fa fa-link" aria-hidden="true"></i></a></li>-->
						<li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="Instagram" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></i></a></li>
					    </ul>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p>
					    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p> 
					</div>
				    </div>
				</div>
			    </div>
			</section>
		</body>
		</html>';

		//Send mail------------------------
		$from='noreply@pedagoge.com';
		$subject='Email Verification';
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From: ". $from. "\r\n";
		$headers .= "Reply-To: ". $from. "\r\n";
		$headers .= "X-Mailer: PHP/" . phpversion();
		$headers .= "X-Priority: 1" . "\r\n";
		
		$status=mail($email_id, $subject, $message, $headers);
		$_SESSION['stu_email_code'] = $email_code;
		die();
	}

	//-----------------------------------------------------------------
	//Check otp code---------------------------------------------------
	public function check_email_code(){
		//$teacher_id=$_SESSION['member_id'];
		//$teacher_model = new ModelTeacher();
		$email_code = $_POST['email_code'];
		if($email_code == $_SESSION['stu_email_code']){
			//$res=$teacher_model->phone_verification($teacher_id);
			echo "success"; 
		}
		else{
			echo "fail"; 
		}
	die();
	}

	//For Submit Query subject wise data
	public function submit_stu_subject_data(){
	$x=$_POST["x"];
	$subject=$_POST["subject"];
	$_SESSION['subject'.$x]=$subject;
	
	$age=@$_POST["age"];
	$_SESSION['age'.$x]=$age;
	
	$school=$_POST["school"];
	$_SESSION['school'.$x]=$school;
	$school = substr($school, strpos($school, "-") + 1);

	$school_type = $_POST['school_type'];
	$_SESSION['school_type'.$x] = $school_type;

	$board=$_POST["board"];
	$_SESSION['board'.$x]=$board;
	$board = substr($board, strpos($board, "-") + 1);

	$board_type = $_POST["board_type"];
	$_SESSION['board_type'.$x] = $board_type;
	
	$standard=$_POST["standard"];
	$_SESSION['standard'.$x]=$standard;
	
	$myhome=$_POST["myhome"];
	$_SESSION['myhome'.$x]=$myhome;
	
	$tutorhome=$_POST["tutorhome"];
	$_SESSION['tutorhome'.$x]=$tutorhome;
	
	$institute=$_POST["institute"];
	$_SESSION['institute'.$x]=$institute;
	
	$localities=$_POST["localities"];
	$localities=implode(",",$localities);
	$_SESSION['localities'.$x]=$localities;
	
	$start_date=$_POST["start_date"];
	$_SESSION['start_date'.$x]=$start_date;
	
	$requirement=$_POST["requirement"];
	$_SESSION['requirement'.$x]=$requirement;
	
	$query_id = $_POST['query_id'];
	
	$query_model = new ModelQuery();
	$result=$query_model->submit_stu_subject_data($_SESSION['member_id'],$subject,$age,$school,$board,$standard,$myhome,$tutorhome,$institute,$localities,$start_date,$requirement,$school_type,$board_type,$query_id);
	echo $result;
	//echo $_SESSION['school'.$x];
	//echo $x.'|'.$subject.'|'.$age.'|'.$school.'|'.$board.'|'.$standard.'|'.$myhome.'|'.$tutorhome.'|'.$institute.'|'.$localities.'|'.$start_date.'|'.$requirement;
	die();
	}
	//------------------------------------------------------
	//For get subject session data--------------------------
	public function get_q_subject_data(){
	$x=$_POST['x'];
	//echo $x;
	//             0                       1                        2                           3                           4                             5                           6                               7                              8                              9
	echo $_SESSION['age'.$x].'|'.$_SESSION['school'.$x].'|'.$_SESSION['board'.$x].'|'.$_SESSION['standard'.$x].'|'.$_SESSION['myhome'.$x].'|'.$_SESSION['tutorhome'.$x].'|'.$_SESSION['institute'.$x].'|'.$_SESSION['localities'.$x].'|'.$_SESSION['start_date'.$x].'|'.$_SESSION['requirement'.$x];
	die();
	}
	//------------------------------------------------------
	//For submit new query data-----------------------------
	public function update_student_data(){
	$user_id=$_SESSION['member_id'];
	$name=$_POST['name'];
	$email=$_POST['email'];
	$phone=$_POST['phone'];
	$query_ids = $_POST['query_ids'];
	$query_model = new ModelQuery();
	$result=$query_model->update_student_data($user_id,$name,$email,$phone,$query_ids);
	echo $result;
	die();
	}
	//------------------------------------------------------
	//Get Student Pagination--------------------------------
	public function get_student_pagination(){
	$query_model = new ModelQuery();
	$result=$query_model->get_student_pagination();
	$sl=1;
		if(count($result) > 0){
			
			foreach($result AS $res){
			if($sl==1){
			echo '<input id="first_query_id" type="hidden" value="'.$res->id.'"/>';	
			}
			?>
			<li class="page-item <?php if($sl==1){echo "active";} ?>"><a class="page-link" data-id="<?php echo $res->id; ?>" href="#"><?php echo $sl; ?></a></li>
			<?php
			$sl++;
			}
			
		}
			
	die();
	}
	//------------------------------------------------------
	//Get Student Query Details on Student Dashboard--------
	public function get_student_query_details(){
	$query_id=$_POST['query_id'];
	$paged = 0;
	if(isset($_POST['page']))
	{
		$paged = $_POST['page'];
	}
	$query_model = new ModelQuery();
	$queries=$query_model->get_student_query_details($query_id);
	$count_short_list=$query_model->count_student_shortlist($query_id);
	$count_interest_list=$query_model->count_student_interestinyou($query_id);
	$count_view_list = $query_model->count_student_queryview($query_id);
	foreach($queries AS $qres){
	$localities = $query_model->get_q_locality($qres->localities);
	$course_type_id_arr=$query_model->course_type_id_arr($qres->subject_name_id);
	//print_r($course_type_id_arr);
	$board_university_name = '';
	if($qres->board != 0)
	{
		$board_university_name = $query_model->get_board_name($qres->board);
	}
	if($qres->university != 0){
		$board_university_name = $query_model->get_university_name($qres->university);
	}
	?>
	<div class="col-xs-12 inner_container_text blue_border">
	   <div class="row">
	      <div class="col-xs-12 blue_background top_radius h1_heading">
		 <h2 class="color_white">Need a <span>Home Tutor</span> for <span><?php echo $subject_name=$qres->subject_name; ?></span></h2>
	      </div>
	   </div>
	   <div class="row margin_top_10">
	      <div class="col-md-6 col-sm-6 col-xs-12 localities">
		<?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
		<p>Age: <?php echo $qres->age; ?> Years</p>
		<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
		 <p>Year/Standard: <?php if($qres->age!=0){ echo $res->age.' Years / '; } ?><?php echo $standard=$qres->standard; ?></p>
		 <p>Board/University: <?php echo $board_university_name; ?></p>
		 <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
		 <p>Year/Standard: <?php echo $standard=$qres->standard; ?></p>
		 <p>Board/University: <?php echo $board_university_name; ?></p>
		 <p>Age: <?php echo $qres->age; ?> Years</p>
		 <?php } ?>
		 <div class="preferred_location">
		    <p>Locality Type:</p>
		    <ul class="area">
			<?php if($qres->myhome==1){ $myhome=1; ?>
		       <li><a href="javascript:void(0);" class="blue_background">My Home</a></li>
		       <?php } ?>
		       <?php if($qres->tutorhome==1){ $tutorhome=1; ?>
		       <li><a href="javascript:void(0);" class="blue_background">Tutor's Home</a></li>
		       <?php } ?>
		       <?php if($qres->institute==1){ $institute=1; ?>
		       <li><a href="javascript:void(0);" class="blue_background">Institue</a></li>
		       <?php } ?>
		    </ul>
		 </div>
		 <div class="preferred_location">
		    <p>Preferred Localities:</p>
		    <ul class="area">
			<?php foreach($localities AS $loc){ ?>
		       <li><a href="javascript:void(0);" class="blue_background"><?php echo $loc; ?></a></li>
		       <?php } ?>
		    </ul>
		 </div>
		 <div class="preferred_location">
		    <p class="width_100">Date of commencement: <strong><?php echo date_format(date_create($qres->start_date), "d/m/Y"); ?></strong></p>
		 </div>
	      </div>
	      <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference">
		 <h5>Preferences: </h5>
		 <p class="text-justify"><?php echo trim($qres->requirement); ?></p>
	      </div>
	      <div class="col-md-3 col-sm-3 col-xs-12">
		 <ul class="pull-right side_right_menu side_right_menu_media text-center">
		    <!--<li class="hidden"><a href="#" class="blue_border blue_color">Collapse</a></li>-->
		    <li><a href="javascript:void(0);" id="query_edit" class="blue_border blue_color">Edit</a></li>
		    <li><a href="javascript:void(0);" id="archive" class="blue_border blue_color">Archive</a></li>
		    <!--<li><a href="#" class="blue_border blue_color">Refer someone</a></li>-->
		 </ul>
	      </div>
	   </div>
	</div>
	<script>$(function(){$('.preference p').slimScroll({height: '140px',size: '5px', alwaysVisible: true,distance: '0px',color: '#2e3847', allowPageScroll: true,opacity: 1});});</script>
        <?php } //end foreach ?>
	<!-- GET Recomended List For Teacher-->
	<?php
	if($myhome==1) $prefloc="Student\'s Location";
	if($tutorhome==1) $prefloc="Own Location";
	if($myhome==1 && $tutorhome==1) $prefloc="Both own location and student\'s location";
	if($institute==1 ) $prefloc="Institute";
	if($query_id > 0)
	{
	$started = microtime(true);
	$recomended_list_teacher = $query_model->get_student_recomended_list_teacher($query_id,$subject_name,$standard,$prefloc,$localities,$paged);
	//$recomended_list_institute = $query_model->get_student_recomended_list_institute($query_id,$subject_name);
	$end = microtime(true);
	
	$difference = $end - $started;
 
	//Format the time so that it only shows 10 decimal places.
	$queryTime = number_format($difference, 10);
	 
	//Print out the seconds it took for the query to execute.
	//echo "SQL query took $queryTime seconds.";
	
	}
	$teacherinstid_arr=array();
	$count1=count($recomended_list_teacher);
	$count2=count($recomended_list_institute);
	//echo "<pre>"; print_r($recomended_list_teacher); echo "</pre>";
	//print_r($localities);

	?>
	<div class="col-xs-12 margin_top_20 inner_container_text blue_border overflow_hidden">
		<div class="row">
		   <div class="col-xs-12 tab_inner_section">
		      <div class="panel-heading h1_heading blue_background">
			 <h2 class="col-md-float-left col-sm-float-none col-xs-float-none col-xs-text-center col-sm-text-center col-md-text-left color_white">Profiles for this query :</h2>
			 <ul class="col-md-float-right col-sm-float-none col-xs-float-none col-xs-text-center col-sm-text-center col-md-text-left nav nav-tabs">
			    <li class="active"><a id="recomended_tab" href="#tab1default" data-toggle="tab">Recommended</a></li>
			    <li><a href="#tab2default" id="short_list_tab" data-toggle="tab">Shortlisted by you <span class="short_list">(<span id="short-list-count"><?php echo $count_short_list; ?></span>)</span></a></li>
			    <li><a href="#tab3default" id="interest_in_tab" data-toggle="tab">Interested in you <span class="int_list">(<span id="int-list-count"><?php echo $count_interest_list; ?></span>)</span></a> </li>
			    <li><a href="#tab4default" id="matched_tab" data-toggle="tab">Matched</a></li>
			    <li><a href="javascript:void(0);">Views <span class="int_list">(<span id="int-list-count"><?php echo $count_view_list; ?></span>)</span></a></li>
			 </ul>
			 <div class="clearfix"></div>
		      </div>
		      <div class="panel-body no_padding_left_right">
			 <div class="tab-content">
			    <div class="tab-pane fade in active" id="tab1default">
			       <!-- Tab One Start -->
			       <div class="row">
				  <div class="col-sm-3 col-xs-12 tab_filter">
				     <div class="blue_border active_blue">
					<!-- Drop Down Filter Start -->
					<button onclick="filterFunction()" class="dropdownfilter">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
					<div id="filterFunctionDropdown" class="dropdown-content-filter">
					   <label for="rec_check1">Private Tutor</label> <input type="checkbox" name="check1" id="rec_check1">
					   <label for="rec_check2">Institutions</label> <input type="checkbox" name="check2" id="rec_check2">
					   <label for="rec_check3">Invited</label> <input type="checkbox" name="check3" id="rec_check3">
					</div>
				     </div>
				     <!-- Drop Down Filter End -->
				  </div>
				  <div class="col-sm-9 col-xs-12 tab_filter_select">
				     <ul>
					<li id="chk_li1" style="display: none;">Private Tutor <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
					<li id="chk_li2" style="display: none;">Institutions <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
					<li id="chk_li3" style="display: none;">Invited <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
				     </ul>
				  </div>
			       </div>
			       <div class="row">
				  <div class="col-xs-12 author_review">
				    <ul class="author_details" id="rec_teacher" data-all="<?= $query_model->count_recomended_list_teacher($query_id,$subject_name,$standard,$prefloc,$localities);?>">
				    <?php 
				    foreach($recomended_list_teacher AS $res){
						$new_user_id = $res->user_id;
						$teacherinstid_arr[] = $new_user_id;  //for teacher
						$rate = $query_model->get_ratings($new_user_id);
						$teache_institute_name = $res->teacher_institute_name;
						$fees = $res->fees;
						if($fees==''){
							$fees=$query_model->get_teacher_fees($new_user_id);
						}
						
						$st = $res->location_type;

						if($st==''){
							$stt = $query_model->get_location_type($new_user_id);
						}
						if($st=='student'){
							$stt="Teacher's,Student's";
						}
						elseif($st=='teacher'){
							$stt='Teacher';
						}
						elseif($st=='own'){
							$stt='Teacher';
						}
						elseif($st=='Own Location'){
							$stt='Teacher';
						}
						elseif($st=="Both own location and student's location"){
							$stt="Teacher's,Student's";
						}
						else{
							$stt='Online';
						}
					?>
					<!-- Author Box Start -->
					<li class="author_box" id="author_li_<?php echo $new_user_id; ?>" data-usertype="<?= $res->user_type;?>">
					   <div class="author_image">
					    	<div class="image_holder">
					    <?php
						$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg");
						if(is_array($profile_img))
						{
							$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
						}
						else{
							if($res->user_type == "teacher")
							{
								if($res->gender == 'female')
								{
									$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
								}
								else if($res->gender == 'male')
								{
									$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
								}
							}
						}
						?>
						 <img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
					      </div>
					   </div>
					   <div class="author_rating pull-right">
					      <ul class="text-right">
						 <li class="rating">
							<?php foreach($rate['rating_star'] AS $star){ ?>
							<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
							<?php } ?>
							<span>(<?php echo $rate['rating_count']; ?>)</span>
						 </li>
						 <li><?php echo $rate['like']; ?> like(s)</li>
						 
						<?php
							if($res->user_type == "teacher")
							{
						 		$profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
						 		$profile_link = home_url().'/teacher/'.$profile_slug;
						 	}
						 	else if($res->user_type == "institution")
						 	{
						 		$profile_slug=$query_model->get_institute_profile_slug($new_user_id);
						 		$profile_link = home_url().'/institute/'.$profile_slug;
						 	}
						?>
						<li class="blue_button"><a href="<?= $profile_link; ?>" target="_blank">View Profile</a></li>
						<li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" class="short-list">Shortlist</a></li>
						<li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" st="0" type="t" class="invite">Invite</a></li>
					    </ul>
					   	</div>
					   	<div class="author_address">
					    <ul>
						<li>Name : <strong> <?php echo $teache_institute_name; ?></strong></li>
						<?php
						$alllocality = '';
						$alllocality = $res->localities;
						$alllocality = rtrim($alllocality,", ");
						$alllocality = str_replace(',', ', ', $alllocality);
						?>
						<li>Location : <strong> <?php echo $alllocality; ?></strong></li>
						<?php														
						$subjects='';
						$subjects=$res->subjects;
						$subjects = rtrim($subjects,", ");
						$subjects = str_replace(',', ', ', $subjects);
						?>
						 <li>Subject : <strong><?php echo $subjects; ?></strong></li>
					      </ul>
					   	</div>
					   	<div class="author_experience">
						<?php
						$experience=$res->experience;
						?>
					    <ul>
							<li>Experience : <strong> <?php echo $experience; ?></strong></li>
							<li>Type of location: <strong> <?php echo $stt; ?></strong></li>
							<li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
					    </ul>
					   	</div>
					</li>
				    <?php } //end teacher foreach ?>
				    </ul>
	
				      <!--Start Institute List-->
				     
				       <?php ?>
				       <ul class="author_details" id="rec_invite">
					<!--Start Invite List-->
				       <?php 
				       if($query_id > 0)
				       {
				       	$recomended_list_invite = $query_model->get_student_recomended_list_invite($subject_name,$query_id,$teacherinstid_arr);
				       	//echo "<pre>"; print_r($recomended_list_invite); echo "</pre>";
				   	   }
					   
				       ?>
				    <?php $count3=count($recomended_list_invite); 
				       foreach($recomended_list_invite AS $res){
				       	$user_id = $res->user_id;
				       	
						$rate=$query_model->get_ratings($user_id); //for invite 
					?>
					<!-- Author Box Start -->
					<li class="author_box" id="author_li_<?php echo $user_id; ?>">
					   <div class="author_image">
					      <div class="image_holder">

					    <?php
					    if($res->type=='i'){
					    	$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $user_id ."/profile.jpg";
					    }
					    else
					    {
					    	$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $user_id ."/profile.jpg");
							if(is_array($profile_img))
							{
								$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $user_id ."/profile.jpg";
							}
							else{
								if($res->gender == 'female')
								{
									$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
								}
								else if($res->gender == 'male')
								{
									$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
								}
							}
					    }
							
						?>
						<img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
					    </div>
					   	</div>
					   	<div class="author_rating pull-right">
					      <ul class="text-right">
						  <li class="rating">
							<?php foreach($rate['rating_star'] AS $star){ ?>
							<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
							<?php } ?>
							<span>(<?php echo $rate['rating_count']; ?>)</span>
						 </li>
						 <li><?php echo $rate['like']; ?> like(s)</li>
						 <?php
						 if($res->type=='i'){
						 $profile_slug=$query_model->get_institute_profile_slug($user_id);
						 $folder='institute';
						 }
						 else{
						 $profile_slug=$query_model->get_teacher_profile_slug($user_id);
						 $folder='teacher';	
						 }
						 ?>
						<li class="blue_button"><a href="<?= home_url(); ?>/<?php echo $folder; ?>/<?php echo $profile_slug; ?>" target="_blank">View Profile</a></li>
						<li class="blue_button"><a href="#" ti-id="<?php echo $user_id; ?>" class="short-list">Shortlist</a></li>
						<li class="blue_button"><a href="#" ti-id="<?php echo $user_id; ?>" st="1" class="uninvite">Uninvite</a></li>
					    </ul>
					   </div>
					   <?php
					   	$teacher_institute_name = $res->teacher_institute_name;
					   	$fees = $res->fees;
						if($fees==''){
							$fees=$query_model->get_teacher_fees($user_id);
						}

						$st = $res->location_type;
						if($st==''){
							$st = $query_model->get_location_type($user_id);
						}
						$experience = $res->experience;
					   	?>
					    <div class="author_address">
					    <ul>
						 	<li>Name : <strong> <?php echo $teacher_institute_name; ?></strong></li>
						<?php														
							$alllocality='';
							$alllocality = $res->localities;
							$alllocality = rtrim($alllocality,", ");
							$alllocality = str_replace(',', ', ', $alllocality);
						?>
						<li>Location : <strong> <?php echo $alllocality;  ?></strong></li>
						<?php														
							$subjects='';
							$subjects=$res->subjects;
							$subjects=rtrim($subjects,", ");
							$subjects = str_replace(',', ', ', $subjects);
						?>
						<li>Subject : <strong><?php echo $subjects; ?></strong></li>
					    </ul>
					   	</div>
					   	<div class="author_experience">
					    <ul>
						<li>Experience : <strong> <?php echo $experience; ?></strong></li>
						<li>Type of location: <strong> <?= $st; ?></strong></li>
						<li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
					    </ul>
					   </div>
					</li>
				       <?php } //end invite foreach ?>
					</ul>
				       <?php if($count1==0 && $count2==0 && $count3==0){ ?>
					<ul class="author_details">
						<li class="author_box" style="text-align: center;">
						No Record Found!!	
						</li>
					</ul>
					<?php } ?>
				  </div>
			       </div>

			       <?php
			       if($query_id > 0)
			       {
			       if(count($recomended_list_teacher) < $query_model->count_recomended_list_teacher($query_id,$subject_name,$standard,$prefloc,$localities))
			       {
			       ?>
			       	<div class="row margin_top_20">
			       		<div class="col-xs-12 text-center">
			       			<button class="loadmore_recommend blue_button" data-page="2">Load More</button>
			       		</div>
			   		</div>
			       <?php	
			       }
			   		}
			       ?>

			    </div>
			    <!-- Tab One End -->
			    <!-- Tab Two Start -->
			    <div class="tab-pane fade" id="tab2default">  
			    <!-- Query Shortlist Content here-->
			    </div>
			    <!-- Tab Two End -->
			    <!-- Tab Three Start -->
			    <div class="tab-pane fade" id="tab3default">
			      <!-- Query Interest in you Content here-->
			    </div>
			    <!-- Tab Three End -->
			    <!-- Tab Four Start -->
			    <div class="tab-pane fade" id="tab4default">
			     <!-- Query Matched Content here-->  
			    </div>
			    <!-- Tab Four End -->
			 </div>
		      </div>
		   </div>
		</div>
	     </div>
	<?php
	die();
	}

	/************** GET RECOMMENDED PROFILE *****************/

	public function get_loadmore_recommended_profile(){
		
		$query_id=$_POST['query_id'];
		$query_model = new ModelQuery();
		$queries=$query_model->get_student_query_details($query_id);
		foreach($queries AS $qres){
			$localities = $query_model->get_q_locality($qres->localities);
			$subject_name=$qres->subject_name;
			$standard=$qres->standard;

			if($qres->myhome==1)
			{
				$myhome=1;
			}
			if($qres->tutorhome==1)
			{
				$tutorhome=1;
			}
			if($qres->institute==1)
			{
				$institute=1;
			}
		}
		if($myhome==1) $prefloc="Student\'s Location";
		if($tutorhome==1) $prefloc="Own Location";
		if($myhome==1 && $tutorhome==1) $prefloc="Both own location and student\'s location";
		if($institute==1 ) $prefloc="Institute";
		
		$paged = 0;
		if(isset($_POST['page']))
		{
			$paged = $_POST['page'];
		}
		$recomended_list_teacher = $query_model->get_student_recomended_list_teacher($query_id,$subject_name,$standard,$prefloc,$localities,$paged);

		foreach($recomended_list_teacher AS $res){
			$new_user_id = $res->user_id;
			$teacherinstid_arr[] = $new_user_id;  //for teacher
			$rate = $query_model->get_ratings($new_user_id);
			$teache_institute_name = $res->teacher_institute_name;
			$fees = $res->fees;
			if($fees==''){
				$fees=$query_model->get_teacher_fees($new_user_id);
			}
						
			$st = $res->location_type;
			if($st==''){
				$stt = $query_model->get_location_type($new_user_id);
			}
			if($st=='student'){
				$stt="Teacher's,Student's";
			}
			elseif($st=='teacher'){
				$stt='Teacher';
			}
			elseif($st=='own'){
				$stt='Teacher';
			}
			elseif($st=='Own Location'){
				$stt='Teacher';
			}
			elseif($st=="Both own location and student's location"){
				$stt="Teacher's,Student's";
			}
			else{
				$stt='Online';
			}
			?>
			<!-- Author Box Start -->
			<li class="author_box" id="author_li_<?php echo $new_user_id; ?>" data-usertype="<?= $res->user_type;?>">
			   <div class="author_image">
			    	<div class="image_holder">
			    <?php
				$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg");
				if(is_array($profile_img))
				{
					$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
				}
				else{
					if($res->user_type == "teacher")
					{
						if($res->gender == 'female')
						{
							$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
						}
						else if($res->gender == 'male')
						{
							$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
						}
					}
				}
				?>
				 <img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
			      </div>
			   </div>
			   <div class="author_rating pull-right">
			      <ul class="text-right">
				 <li class="rating">
					<?php foreach($rate['rating_star'] AS $star){ ?>
					<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
					<?php } ?>
					<span>(<?php echo $rate['rating_count']; ?>)</span>
				 </li>
				 <li><?php echo $rate['like']; ?> like(s)</li>
				 
				<?php
					if($res->user_type == "teacher")
					{
				 		$profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
				 		$profile_link = home_url().'/teacher/'.$profile_slug;
				 	}
				 	else if($res->user_type == "institution")
				 	{
				 		$profile_slug=$query_model->get_institute_profile_slug($new_user_id);
				 		$profile_link = home_url().'/institute/'.$profile_slug;
				 	}
				?>
				<li class="blue_button"><a href="<?= $profile_link; ?>" target="_blank">View Profile</a></li>
				<li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" class="short-list">Shortlist</a></li>
				<li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" st="0" type="t" class="invite">Invite</a></li>
			    </ul>
			   	</div>
			   	<div class="author_address">
			    <ul>
				<li>Name : <strong> <?php echo $teache_institute_name; ?></strong></li>
				<?php
				$alllocality = '';
				$alllocality = $res->localities;
				$alllocality = rtrim($alllocality,", ");
				$alllocality = str_replace(',', ', ', $alllocality);
				?>
				<li>Location : <strong> <?php echo $alllocality; ?></strong></li>
				<?php														
				$subjects='';
				$subjects=$res->subjects;
				$subjects = rtrim($subjects,", ");
				$subjects = str_replace(',', ', ', $subjects);
				?>
				 <li>Subject : <strong><?php echo $subjects; ?></strong></li>
			      </ul>
			   	</div>
			   	<div class="author_experience">
				<?php
				$experience=$res->experience;
				?>
			    <ul>
					<li>Experience : <strong> <?php echo $experience; ?></strong></li>
					<li>Type of location: <strong> <?php echo $stt; ?></strong></li>
					<li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
			    </ul>
			   	</div>
			</li>
		<?php 
		} //end teacher foreach

		die();
	}

	//------------------------------------------------------
	//Get Student Shortlist---------------------------------
	public function get_student_shortlist(){
	$query_id=$_POST['query_id'];
	$query_model = new ModelQuery();
	if($query_id > 0)
	{
		$short_list_teacher = $query_model->get_student_teacher_shortlist($query_id);
		//$short_list_institute = $query_model->get_student_institute_shortlist($query_id);
		$short_list_invite = $query_model->get_student_invite_shortlist($query_id);	
		
		//echo "<pre>"; print_r($short_list_teacher); echo "</pre>";
		//echo "<pre>"; print_r($short_list_institute); echo "</pre>";
		
	}
	
	$count1=count($short_list_teacher);
	//$count2=count($short_list_institute);
	$count3=count($short_list_invite);
	
	?>
	<!-- Tab One Start -->
	<div class="row">
	   <div class="col-sm-3 col-xs-12 tab_filter">
	      <div class="blue_border active_blue">
		 <!-- Drop Down Filter Start -->
		 <button onclick="filterFunction()" class="dropdownfilter">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
		 <div id="filterFunctionDropdown" class="dropdown-content-filter">
		    <label for="rec_check1">Private Tutor</label> <input type="checkbox" name="check1" id="rec_check1">
		    <label for="rec_check2">Institutions</label> <input type="checkbox" name="check2" id="rec_check2">
		    <label for="rec_check3">Invited</label> <input type="checkbox" name="check3" id="rec_check3">
		 </div>
	      </div>
	      <!-- Drop Down Filter End -->
	   </div>
	   <div class="col-sm-9 col-xs-12 tab_filter_select">
	      <ul>
		 <li id="chk_li1" style="display: none;">Private Tutor <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
		 <li id="chk_li2" style="display: none;">Institutions <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
		 <li id="chk_li3" style="display: none;">Invited <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
	      </ul>
	   </div>
	</div>
	<div class="row">
	   <div class="col-xs-12 author_review">		  
		<ul class="author_details" id="rec_teacher">
		 <?php foreach($short_list_teacher AS $res){ 

		 	$new_user_id=$res->user_id;
			
			$rate = $query_model->get_ratings($new_user_id); //for teacher 
			$teacherinstid_arr[] = $new_user_id;  //for teacher
			$rate = $query_model->get_ratings($new_user_id);
			 
			$teacher_institute_name = $res->teacher_institute_name;
			$fees=$res->fees;
			if($fees==''){
				$fees=$query_model->get_teacher_fees($new_user_id);
			}
			
			$st= $res->location_type;
			if($st==''){
				$stt = $query_model->get_location_type($new_user_id);
			}
			if($st=='student'){
				$stt="Teacher's,Student's";
			}
			elseif($st=='teacher'){
				$stt='Teacher';
			}
			elseif($st=='own'){
				$stt='Teacher';
			}
			elseif($st=='Own Location'){
				$stt='Teacher';
			}
			elseif($st=="Both own location and student's location"){
				$stt="Teacher's,Student's";
			}
			else{
				$stt='Online';
			}
		 ?>
		   <!-- Author Box Start -->
		   <li class="author_box" id="author_li_<?php echo $new_user_id; ?>" data-usertype="<?= $res->user_type;?>">
		      <div class="author_image">
			 <div class="image_holder">
			 	<?php

			 		if($res->user_type=='institution'){
					    $profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
				    }
				    else
				    {
				    	$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg");
						if(is_array($profile_img))
						{
							$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
						}
						else{
							if($res->gender == 'female')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
							}
							else if($res->gender == 'male')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
							}
						}
				    }
				?>
			    <!--<img src="<?php echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $res->teacherinstid ."/profile.jpg"; ?>" alt="People" width="88" height="88" />-->
			    <img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
			 </div>
		      </div>
		      <div class="author_rating pull-right">
			 <ul class="text-right">
			   <li class="rating">
				<?php foreach($rate['rating_star'] AS $star){ ?>
				<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
				<span>(<?php echo $rate['rating_count']; ?>)</span>
			 </li>
			 <li><?php echo $rate['like']; ?> like(s)</li>
			    <?php $profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
			    ?>
			    <li class="blue_button"><a href="<?= home_url(); ?>/teacher/<?php echo $profile_slug; ?>" target="_blank">View Profile</a></li>
			    <li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" class="remove-shortlist">Remove</a></li>
			    <li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" st="0" type="t" class="invite">Invite</a></li>
			 </ul>
		      </div>
		      <div class="author_address">
			 <ul>
			   <li>Name : <strong> <?php echo $teacher_institute_name; ?></strong></li>
			   <?php														
			   	$alllocality='';
			   	$alllocality = $res->localities;
			   	$alllocality = rtrim($alllocality,", ");
			   	$alllocality = str_replace(",", ", ", $alllocality);
			   ?>
			   <li>Location : <strong> <?php echo $alllocality;  ?></strong></li>
			   <?php														
			   	$subjects='';
			   
			   	$subjects=$res->subjects;
				$subjects=rtrim($subjects,", ");
				$subjects = str_replace(",", ", ", $subjects);
			   ?>
			    <li>Subject : <strong><?php echo $subjects; ?></strong></li>
			 </ul>
		      </div>
		      <div class="author_experience">
			 <ul>
			 	<?php
			 		$experience=$res->experience;
				?>
			    <li>Experience : <strong> <?php echo $experience; ?></strong></li>
			    <li>Type of location: <strong> <?= $stt;?></strong></li>
			    <li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
			 </ul>
		      </div>
		   </li>
		  <?php } //end teacher foreach ?>
		 </ul>
		
			<ul class="author_details" id="rec_invite">
				<!--Start Invite List-->
			       <?php //$recomended_list_invite = $query_model->get_student_recomended_list_invite($subject_name,$query_id,$teacherinstid_arr); ?>
			       <?php 
			       //echo "<pre>"; print_r($short_list_invite); echo "</pre>";
			       foreach($short_list_invite AS $res){ 
			       	$user_id = $res->user_id;
			       	$rate=$query_model->get_ratings($res->user_id); //for invite 
			       	$rate=$query_model->get_ratings($res->i_user_id); //for invite
			       ?>
				<!-- Author Box Start -->
				<li class="author_box" id="author_li_<?php echo $user_id; ?>">
				   <div class="author_image">
				      <div class="image_holder">
				    <?php
				    if($res->type=='i'){
						$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $user_id ."/profile.jpg";
					}
					else
					{
						$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $user_id ."/profile.jpg");
						if(is_array($profile_img))
						{
							$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $user_id ."/profile.jpg";
						}
						else{
							if($res->gender == 'female')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
							}
							else if($res->gender == 'male')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
							}
						}
					}
					?>
					 
					 <img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
				      </div>
				   </div>
				   <div class="author_rating pull-right">
				      <ul class="text-right">
					 <li class="rating">
						<?php foreach($rate['rating_star'] AS $star){ ?>
						<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
						<?php } ?>
						<span>(<?php echo $rate['rating_count']; ?>)</span>
					 </li>
					 <?php
					 if($res->type=='i'){
					 $profile_slug=$query_model->get_institute_profile_slug($user_id);
					 $folder='institute';

					 }
					 else{
					 $profile_slug=$query_model->get_teacher_profile_slug($user_id);
					 $folder='teacher';	
					 }
					 ?>
					 <li class="blue_button"><a href="<?= home_url(); ?>/<?php echo $folder; ?>/<?php echo $profile_slug; ?>" target="_blank">View Profile</a></li>
					 <li class="blue_button"><a href="#" ti-id="<?php echo $user_id; ?>" class="remove-shortlist">Remove</a></li>
				         <li class="blue_button"><a href="#" ti-id="<?php echo $user_id; ?>" st="1" class="uninvite">Uninvite</a></li>
				      </ul>
				   </div>
				   <?php  
				   	$fees = $res->fees;
					if($fees == ''){
						$fees = $query_model->get_teacher_fees($user_id);
					}
					
					$st = $res->location_type;
					if($st==''){
						$st = $query_model->get_location_type($user_id);
					}
				   	?>
				   <div class="author_address">
				      <ul>
					 <li>Name : <strong> <?php echo $res->teacher_institute_name; ?></strong></li>
					 <?php														
					$alllocality='';
					
					$alllocality = $res->localities;
					$alllocality = rtrim($alllocality,", ");
					$alllocality = str_replace(",",", ",$alllocality);
					?>
					<li>Location : <strong> <?php echo $alllocality;  ?></strong></li>
					<?php														
					$subjects = '';
					$subjects = $res->subjects;
					$subjects = rtrim($subjects,", ");
					$subjects = str_replace(",",", ",$subjects);
					?>
					 <li>Subject : <strong><?php echo $subjects; ?></strong></li>
				      </ul>
				   </div>
				   <div class="author_experience">
				      <ul>
				    <?php
						$experience = $res->experience;
					?>
					<li>Experience : <strong> <?php echo $experience; ?></strong></li>
					<li>Type of location: <strong> <?php echo $st; ?></strong></li>
					<li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
				    </ul>
				   </div>
				   
				</li>
			       <?php } //end invite foreach ?>
			</ul>
			<?php if($count1==0 && $count2==0 && $count3==0){ ?>
			<ul class="author_details">
				<li class="author_box" style="text-align: center;">
				No Record Found!!	
				</li>
			</ul>
			<?php } ?>
		</div> 
	</div>

	<?php
	die();	
	}
	//------------------------------------------------------
	//Get Student Interest inyou----------------------------
	public function get_student_interestinyou(){
	$query_id=$_POST['query_id'];
	$query_model = new ModelQuery();
	if($query_id > 0)
	{
	$int_list_teacher = $query_model->get_student_teacher_intyou($query_id);
	//$int_list_institute = $query_model->get_student_institute_intyou($query_id);
	$int_list_invite = $query_model->get_student_invite_intyou($query_id);
	}
	$count1=count($int_list_teacher);
	$count2=count($int_list_institute);
	$count3=count($int_list_invite);
	//echo "<pre>"; print_r($int_list_teacher); echo "</pre>";
	?>
	<!-- Tab One Start -->
	<div class="row">
	   <div class="col-sm-3 col-xs-12 tab_filter">
	      <div class="blue_border active_blue">
		 <!-- Drop Down Filter Start -->
		 <button onclick="filterFunction()" class="dropdownfilter">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
		 <div id="filterFunctionDropdown" class="dropdown-content-filter">
		    <label for="rec_check1">Private Tutor</label> <input type="checkbox" name="check1" id="rec_check1">
		    <label for="rec_check2">Institutions</label> <input type="checkbox" name="check2" id="rec_check2">
		    <label for="rec_check3">Invited</label> <input type="checkbox" name="check3" id="rec_check3">
		 </div>
	      </div>
	      <!-- Drop Down Filter End -->
	   </div>
	   <div class="col-sm-9 col-xs-12 tab_filter_select">
	      <ul>
		 <li id="chk_li1" style="display: none;">Private Tutor <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
		 <li id="chk_li2" style="display: none;">Institutions <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
		 <li id="chk_li3" style="display: none;">Invited <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
	      </ul>
	   </div>
	</div>
	<div class="row">
	   <div class="col-xs-12 author_review">		  
		<ul class="author_details" id="rec_teacher">
		 <?php 
		 //echo "<pre>"; print_r($int_list_teacher); echo "</pre>";
		 $incrementer = 0;
		 foreach($int_list_teacher AS $res){ 
		 	//$new_user_id = ($res->new_user_id !=""?$res->new_user_id:$res->user_id);
		 	$new_user_id = $res->user_id;
		 	//$rate=$query_model->get_ratings($res->new_user_id); //for teacher 
		 	$rate=$query_model->get_ratings($new_user_id); //for teacher 
		 	$getintroteacher = $query_model->get_intro_teacher($res->fk_query_id,$res->teacherinst_id,$res->fk_student_id);

		 	$st = $query_model->get_location_type($new_user_id);
			if($st==''){
			$st= $res->hometutor;
			}
			if($st=='student'){
			$stt="Teacher's,Student's";
			}
			elseif($st=='teacher'){
			$stt='Teacher';
			}
			elseif($st=='own'){
			$stt='Teacher';
			}
			elseif($st=='Own Location'){
			$stt='Teacher';
			}
			elseif($st=="Both own location and student's location"){
			$stt="Teacher's,Student's";
			}
			else{
			$stt='Online';
			}

		 	if($incrementer < 2)
		 	{
		 		$style = "style='background:#afceb3'";
		 	}
		 	else
		 	{
		 		$style = '';
		 	}

		 	?>
		   <!-- Author Box Start -->
		   <li class="author_box" id="author_li_<?php echo $new_user_id; ?>" data-usertype="<?= $res->user_type;?>" <?= $style;?>>
		      <div class="author_image">
			 <div class="image_holder">
			 	<?php
					if($res->user_type=='institution'){
					    $profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
				    }
				    else
				    {
				    	$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg");
						if(is_array($profile_img))
						{
							$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
						}
						else{
							if($res->gender == 'female')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
							}
							else if($res->gender == 'male')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
							}
						}
				    }
				?>
				<img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
			 </div>
		      </div>
		      <div class="author_rating pull-right">
			 <ul class="text-right">
			    <li class="rating">
				<?php foreach($rate['rating_star'] AS $star){ ?>
				<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
				<span>(<?php echo $rate['rating_count']; ?>)</span>
			    </li>
			    <?php 
			    if($res->user_type=='institution'){
					$profile_slug=$query_model->get_institute_profile_slug($new_user_id);
					$folder='institute';
				}
				else{
					$profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
					$folder='teacher';	
				}
			    //$profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
			    ?>
			    <li class="blue_button"><a href="<?= home_url()."/".$folder."/".$profile_slug; ?>" target="_blank">View Profile</a></li>
			    <li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id;?>" class="stu_ignore">Ignore</a></li>
                <li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id;?>" class="<?= ($getintroteacher == 0 ? 'stu_get_intro':'')?>"><?= ($getintroteacher == 0 ? 'Get Intro':'Intro Sent')?></a></li>
			 </ul>
		      </div>
		      <div class="author_address">
			 <ul>
			    <li>Name : <strong> <?php echo $res->teacher_institute_name; ?></strong></li>
			    <?php														
			    $alllocality='';
			    $alllocality = $res->localities;
				$alllocality = rtrim($alllocality,", ");
				$alllocality = str_replace(",",", ",$alllocality);
			   ?>
			   <li>Location : <strong> <?php echo $alllocality;  ?></strong></li>
			   <?php														
			   	$subjects = '';
				$subjects = $res->subjects;
				$subjects = rtrim($subjects,", ");
				$subjects = str_replace(",",", ",$subjects);
			   ?>
			    <li>Subject : <strong><?php echo $subjects; ?></strong></li>
			 </ul>
		      </div>
		      <div class="author_experience">
			 <ul>
			    
			    <li>Experience : <strong> <?php echo $res->experience; ?></strong></li>
			    <li>Type of location: <strong> <?= $stt; ?></strong></li>
			    <?php
			    $fees=$res->fees;
				if($fees==''){
					$fees=$query_model->get_teacher_fees($new_user_id);
				}
				?>
			    <li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
			 </ul>
		      </div>
		   </li>
		  <?php 
		  	$incrementer++;
			} //end teacher foreach ?>
		 </ul>
		
			<?php if($count1==0 && $count2==0 && $count3==0){ ?>
			<ul class="author_details">
				<li class="author_box" style="text-align: center;">
				No Record Found!!	
				</li>
			</ul>
			<?php } ?>
		</div> 
	</div>

	<?php
	die();	
	}
	//------------------------------------------------------
	//Get Student Matched List------------------------------
	public function get_student_matched(){
	$query_id=$_POST['query_id'];
	$query_model = new ModelQuery();
	if($query_id > 0)
	{
	$match_list_teacher = $query_model->get_student_teacher_matched($query_id);
	//$match_list_institute = $query_model->get_student_institute_matched($query_id);
	$match_list_invite = $query_model->get_student_invite_matched($query_id);
	}
	$count1=count($match_list_teacher);
	$count2=count($match_list_institute);
	$count3=count($match_list_invite);
	?>
	<!-- Tab One Start -->
	<div class="row">
	   <div class="col-sm-3 col-xs-12 tab_filter">
	      <div class="blue_border active_blue">
		 <!-- Drop Down Filter Start -->
		 <button onclick="filterFunction()" class="dropdownfilter">Filter <i class="fa fa-filter" aria-hidden="true"></i></button>
		 <div id="filterFunctionDropdown" class="dropdown-content-filter">
		    <label for="rec_check1">Private Tutor</label> <input type="checkbox" name="check1" id="rec_check1">
		    <label for="rec_check2">Institutions</label> <input type="checkbox" name="check2" id="rec_check2">
		    <label for="rec_check3">Invited</label> <input type="checkbox" name="check3" id="rec_check3">
		 </div>
	      </div>
	      <!-- Drop Down Filter End -->
	   </div>
	   <div class="col-sm-9 col-xs-12 tab_filter_select">
	      <ul>
		 <li id="chk_li1" style="display: none;">Private Tutor <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
		 <li id="chk_li2" style="display: none;">Institutions <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
		 <li id="chk_li3" style="display: none;">Invited <span><i class="fa fa-window-close-o" aria-hidden="true"></i></span></li>
	      </ul>
	   </div>
	</div>
	<div class="row">
	   <div class="col-xs-12 author_review">		  
		<ul class="author_details" id="rec_teacher">
		 <?php foreach($match_list_teacher AS $res){ 
		 	//$new_user_id = ($res->new_user_id !=""?$res->new_user_id:$res->user_id);
		 	$new_user_id = $res->user_id;
		 	$rate=$query_model->get_ratings($new_user_id); //for teacher 
		 	$st = $res->location_type;
			if($st==''){
				$stt = $query_model->get_location_type($new_user_id);
			}
			if($st=='student'){
				$stt="Teacher's,Student's";
			}
			elseif($st=='teacher'){
				$stt='Teacher';
			}
			elseif($st=='own'){
				$stt='Teacher';
			}
			elseif($st=='Own Location'){
				$stt='Teacher';
			}
			elseif($st=="Both own location and student's location"){
				$stt="Teacher's,Student's";
			}
			else{
				$stt='Online';
			}

		 	?>
		   <!-- Author Box Start -->
		   <li class="author_box" id="author_li_<?php echo $new_user_id; ?>" data-usertype="<?= $res->user_type;?>">
		      <div class="author_image">
			 <div class="image_holder">
			 	<?php

			 		if($res->user_type=='institution'){
					    $profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
				    }
				    else
				    {
				    	$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg");
						if(is_array($profile_img))
						{
							$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
						}
						else{
							if($res->gender == 'female')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
							}
							else if($res->gender == 'male')
							{
								$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
							}
						}
				    }

				?>
				<img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
			 </div>
		      </div>
		      <div class="author_rating pull-right">
			 <ul class="text-right">
			    <li class="rating">
				<?php foreach($rate['rating_star'] AS $star){ ?>
				<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
				<span>(<?php echo $rate['rating_count']; ?>)</span>
			   </li>
			    <?php 
			    if($res->user_type=='institution'){
					$profile_slug=$query_model->get_institute_profile_slug($new_user_id);
					$folder='institute';
				}
				else{
					$profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
					$folder='teacher';	
				}

			    ?>
			    <li class="blue_button"><a href="<?= home_url()."/".$folder."/".$profile_slug; ?>" target="_blank">View Profile</a></li>
			    <li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" class="unmatch">Unmatch</a></li>
			 </ul>
		      </div>
		      <div class="author_address">
			 <ul>
			    <li>Name : <strong> <?php echo $res->teacher_institute_name; ?></strong></li>
			    <?php														
			   	$alllocality='';
			   	$alllocality = $res->localities;
				$alllocality = rtrim($alllocality,", ");
				$alllocality = str_replace(",",", ",$alllocality);
			   ?>
			   <li>Location : <strong> <?php echo $alllocality;  ?></strong></li>
			   <?php														
			   	$subjects='';
			   	$subjects = $res->subjects;
				$subjects = rtrim($subjects,", ");
				$subjects = str_replace(",",", ",$subjects);
			   ?>
			    <li>Subject : <strong><?php echo $subjects; ?></strong></li>
			 </ul>
		      </div>
		      <div class="author_experience">
			 <ul>
			    <!--<li>Experience : <strong> <?php echo $res->workexperience; ?></strong></li>-->
			    <li>Experience : <strong> <?php echo $res->experience; ?></strong></li>
			    <li>Type of location: <strong> <?= $stt; ?></strong></li>
			    <?php
			    $fees=$res->fees;
				if($fees==''){
					$fees=$query_model->get_teacher_fees($new_user_id);
				}
				?>
			    <li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
			 </ul>
		      </div>
		   </li>
		  <?php } //end teacher foreach ?>
		 </ul>
			<ul class="author_details" id="rec_invite">
				<!--Start Invite List-->
			       <?php //$recomended_list_invite = $query_model->get_student_recomended_list_invite($subject_name,$query_id,$teacherinstid_arr); ?>
			       <?php foreach($match_list_invite AS $res){ 
			       	$new_user_id = $res->user_id;
			       	$rate=$query_model->get_ratings($new_user_id); //for invite 
			       	$st = $res->location_type;
					if($st==''){
						$stt = $query_model->get_location_type($new_user_id);
					}
					if($st=='student'){
						$stt="Teacher's,Student's";
					}
					elseif($st=='teacher'){
						$stt='Teacher';
					}
					elseif($st=='own'){
						$stt='Teacher';
					}
					elseif($st=='Own Location'){
						$stt='Teacher';
					}
					elseif($st=="Both own location and student's location"){
						$stt="Teacher's,Student's";
					}
					else{
						$stt='Online';
					}
			       ?>
				<!-- Author Box Start -->
				<li class="author_box" id="author_li_<?php echo $res->idd; ?>">
				   <div class="author_image">
				      <div class="image_holder">
				     <?php
				      	if($res->user_type=='institution'){
					    $profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
					    }
					    else
					    {
					    	$profile_img = @getimagesize(plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg");
							if(is_array($profile_img))
							{
								$profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/". $new_user_id ."/profile.jpg";
							}
							else{
								if($res->gender == 'female')
								{
									$profile_img = plugins_url()."/pedagoge_plugin/assets/images/female_teacher.png";
								}
								else if($res->gender == 'male')
								{
									$profile_img = plugins_url()."/pedagoge_plugin/assets/images/male_teacher.png";
								}
							}
					    }
				    ?>
					 <img src="<?php echo $profile_img; ?>" alt="People" width="88" height="88" />
				      </div>
				   </div>
				   <div class="author_rating pull-right">
				      <ul class="text-right">
					<li class="rating">
						<?php foreach($rate['rating_star'] AS $star){ ?>
						<img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
						<?php } ?>
						<span>(<?php echo $rate['rating_count']; ?>)</span>
					 </li>
					 <?php
					 if($res->type=='i'){
					 $profile_slug=$query_model->get_institute_profile_slug($new_user_id);
					 $folder='institute';
					 }
					 else{
					 $profile_slug=$query_model->get_teacher_profile_slug($new_user_id);
					 $folder='teacher';	
					 }
					 ?>
					 <li class="blue_button"><a href="<?= home_url(); ?>/<?php echo $folder; ?>/<?php echo $profile_slug; ?>" target="_blank">View Profile</a></li>
					 <li class="blue_button"><a href="#" ti-id="<?php echo $new_user_id; ?>" class="unmatch">Unmatch</a></li>
				      </ul>
				   </div>
				   
				   <div class="author_address">
				      <ul>
					 <li>Name : <strong> <?php echo $res->teacher_institute_name; ?></strong></li>
					 <?php														
						$alllocality='';
						$alllocality = $res->localities;
						$alllocality = rtrim($alllocality,", ");
						$alllocality = str_replace(",",", ",$alllocality);
					?>
					<li>Location : <strong> <?php echo $alllocality;  ?></strong></li>
					<?php														
					$subjects='';
					$subjects = $res->subjects;
					$subjects = rtrim($subjects,", ");
					$subjects = str_replace(",",", ",$subjects);
					?>
					 <li>Subject : <strong><?php echo $subjects; ?></strong></li>
				      </ul>
				   </div>
				   <div class="author_experience">
				      <ul>
					 <!--<li>Experience : <strong> <?php echo $res->yearsofexsistance; ?></strong></li>-->
					 <li>Experience : <strong> <?php echo $res->experience; ?></strong></li>
					 <li>Type of location: <strong> <?php echo $stt; ?></strong></li>
					 <?php
					 $fees=$res->fees;
					 if($fees==''){
						$fees=$query_model->get_teacher_fees($new_user_id);
					 }
					 ?>
					 <li>Starting from (<i class="fa fa-inr" aria-hidden="true"></i>) : <strong> <?php echo $fees; ?></strong></li>
				      </ul>
				   </div>
				   
				</li>
			       <?php } //end invite foreach ?>
			</ul>
			<?php if($count1==0 && $count2==0 && $count3==0){ ?>
			<ul class="author_details">
				<li class="author_box" style="text-align: center;">
				No Record Found!!	
				</li>
			</ul>
			<?php } ?>
		</div> 
	</div>

	<?php
	die();	
	}
	//------------------------------------------------------
	
	//Archive query-----------------------------------------
	public function query_archive(){
	$query_id=$_POST['query_id'];
	$query_model = new ModelQuery();
	$result = $query_model->query_archive($query_id);
	die();	
	}
	//------------------------------------------------------
	
	//Create Shortlist query-----------------------------------------
	public function create_query_shortlist(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$query_model = new ModelQuery();
	$result = $query_model->create_query_shortlist($query_id,$ti_id);
	die();	
	}
	//---------------------------------------------------------------
	//Remove Shortlist query-----------------------------------------
	public function remove_query_shortlist(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$query_model = new ModelQuery();
	$result = $query_model->remove_query_shortlist($query_id,$ti_id);
	die();	
	}
	//---------------------------------------------------------------
	//Create Invite query-----------------------------------------
	public function create_query_invite(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$type=$_POST['type'];
	$query_model = new ModelQuery();
	$result = $query_model->create_query_invite($query_id,$ti_id,$type);
	die();	
	}
	//--------------------------------------------------------------
	//Create UnInvite query-----------------------------------------
	public function create_query_uninvite(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$type=$_POST['type'];
	$query_model = new ModelQuery();
	$result = $query_model->create_query_uninvite($query_id,$ti_id,$type);
	die();	
	}
	//--------------------------------------------------------------
	
	//Student Ignore interest query-----------------------------------------
	public function stu_ignore_interest(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$query_model = new ModelQuery();
	$result = $query_model->stu_ignore_interest($query_id,$ti_id);
	die();	
	}
	//---------------------------------------------------------------
	//Submit Student Get Intro-----------------------------------------
	public function stu_get_intro(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$query_model = new ModelQuery();
	$result = $query_model->stu_get_intro($query_id,$ti_id);

	/************** New Section For Student Notification ***************/
	$student_details=$query_model->get_student_details_by_qid($query_id);
	$student_id = $student_details[0]->user_id;
	$type = 'match';
	$student_notification = $query_model->student_notification($query_id,$student_id,$type);

	/************** New Section For Student Notification ***************/
	die();	
	}
	//--------------------------------------------------------------
	
	//Submit Unmatch-----------------------------------------
	public function stu_unmatch(){
	$query_id=$_POST['query_id'];
	$ti_id=$_POST['tiid'];
	$query_model = new ModelQuery();
	$result = $query_model->stu_unmatch($query_id,$ti_id);
	die();	
	}
	//--------------------------------------------------------------
	
	//Submit Edit Query-------------------------------------
	public function submit_edit_query(){
	//$data=urldecode($_POST['data']);
	$query_id=$_POST['query_id'];
	$clone_student_id=$_POST['clone_student_id'];
	$clone_subject=$_POST['clone_subject'];
	$clone_school=$_POST['clone_school'];
	$clone_board=$_POST['clone_board'];
	$clone_school_type = $_POST['clone_school_type'];
	$clone_board_type = $_POST['clone_board_type'];
	$clone_standard=$_POST['clone_standard'];
	$age=$_POST['age'];
	@$clone_my_home=$_POST['clone_my_home'];
	@$clone_t_home=$_POST['clone_t_home'];
	@$clone_institute=$_POST['clone_institute'];
	$clone_locality=implode(",",$_POST['clone_locality']);
	$clone_query_date=$_POST['clone_query_date'];
	$clone_requirement=$_POST['clone_requirement'];
	$clone_student_name=$_POST['clone_student_name'];
	$clone_phone_number=$_POST['clone_phone_number'];
	$clone_user_email=$_POST['clone_user_email'];
	//echo $clone_user_email;
	$query_model = new ModelQuery();
	$query_id = $query_model->submit_past_edit_query($query_id,$clone_student_id,$clone_subject,$clone_school,$clone_board,$clone_standard,$age,$clone_my_home,$clone_t_home,
						    $clone_institute,$clone_locality,$clone_query_date,$clone_requirement,$clone_student_name,$clone_phone_number,
						    $clone_user_email,$clone_school_type,$clone_board_type);
	
	die();
	}
	//---------------------------------------------------------------------------------------------------------------------
	//Unarchived Archived query--------------------------------------------------------------------------------------------
	public function query_unarchive(){
	$query_id=$_POST['query_id'];
	$query_model = new ModelQuery();
	$res = $query_model->query_unarchive($query_id);
	die();
	}
	//---------------------------------------------------------------------------------------------------------------------
	//Delete Query---------------------------------------------------------------------------------------------------------
	public function delete_query(){
	$query_id=$_POST['query_id'];
	$query_model = new ModelQuery();
	$res = $query_model->delete_query($query_id);
	die();
	}
	
	//--------------------------------------------------------------------------------
	//GET aJAX notification ----------------------------------------------------------
	public function get_student_notification(){
	$query_model = new ModelQuery();
	$student_id=$_SESSION['member_id'];
	$notifications=$query_model->get_all_student_notification($student_id);
	?>
	<?php if(count($notifications)>0){ foreach($notifications AS $noti){ ?>
                        <li id="noti<?php echo $noti->id; ?>">
                            	<div class="bell_icon">
                                	<i class="fa fa-bell-o" aria-hidden="true"></i>
                                </div>
                                <div class="notification_details_para">
                                	<p><?php echo $noti->notification; ?></p>
                                </div>
                                <div class="navigation_delate">
                                	<button class="delete_noti" idd="<?php echo $noti->id; ?>" type="button">X</button>
                                </div>
                            </li>
                        <?php } }else{?>
			<li>
                            	<div class="bell_icon">
                                	<i class="fa fa-bell-o" aria-hidden="true"></i>
                                </div>
                                <div class="notification_details_para">
                                	<p>You have no notification to show!</p>
                                </div>
                                <div class="navigation_delate">
                                	<!--<button type="button">X</button>-->
                                </div>
                        </li>
			<?php } ?>
	<?php
	die();		
	}
	//--------------------------------------------------------------------------------
	//Get ajax notification count-----------------------------------------------------
	public function get_student_ajax_notification_count(){
	$query_model = new ModelQuery();
	$student_id=$_SESSION['member_id'];
	echo $notification_count=$query_model->get_count_student_notification($student_id);
	die();
	}

	//Delete notification-------------------------------------------------------------
	public function delete_notification(){
	$query_model = new ModelQuery();
	$student_id=$_SESSION['member_id'];
	$noti_id=$_POST['noti_id'];
	$query_model->delete_notification($noti_id);
	die();	
	}

	//--------------------------------------------------------------------------------
	//View notification-------------------------------------------------------------
	public function view_notification(){
	$query_model = new ModelQuery();
	$student_id=$_SESSION['member_id'];
	$query_model->view_notification($student_id);
	die();	
	}
}		
?>
