<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerWorkshop extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Workshop';
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_load_scripts();
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		$file_version = '1.5';

		parent::fn_themes_ver2_load_scripts();
		
		$this->css_assets['fontawesome'] = $this->registered_css['fontawesome']."?ver=".$this->fixed_version;
		$this->app_css['ver2_home'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/home.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['workshop'] = PEDAGOGE_ASSETS_URL . '/css/workshop.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;
		
		/////////////////////////////////////////////////////////
		$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
		$this->footer_js['blockui'] = BOWER_ROOT_URL."/blockUI/jquery.blockUI.js?ver=".$this->fixed_version;
		$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
		$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
		
		/**
		 * Validation Engine
		 */		
		$this->css_assets['validationEngine']   = get_template_directory_uri() . "/assets/css/validationEngine/validationEngine.jquery.css?ver=" . $this->fixed_version;
		$this->footer_js['validationEngine']    = get_template_directory_uri() . "/assets/js/validationEngine/jquery.validationEngine.js?ver=" . $this->fixed_version;
		$this->footer_js['validationEngine-en'] = get_template_directory_uri() . "/assets/js/validationEngine/jquery.validationEngine-en.js?ver=" . $this->fixed_version;
		
		/**
		 * BXSLIDER
		 */		
		$this->css_assets['jquery.bxslider'] = PEDAGOGE_ASSETS_URL.'/plugins/jquery.bxslider/jquery.bxslider.css?ver='.$this->fixed_version;
		$this->footer_js['jquery.bxslider'] = PEDAGOGE_ASSETS_URL.'/plugins/jquery.bxslider/jquery.bxslider.min.js?ver='.$this->fixed_version;
		
		//$this->app_js['addthis'] = '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5901bfd6cc7d83b7?ver=' . $this->fixed_version;
		$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['ver2_social_login'] = PEDAGOGE_ASSETS_URL . '/js/social_login.js?ver=' . $this->fixed_version . "." . $file_version;		
		$this->app_js['workshop'] = PEDAGOGE_ASSETS_URL . '/js/workshop.js?ver=' . $this->fixed_version . "." . $file_version;		
	}

	private function fn_register_common_variables() {
		
		if(isset($_GET['workshop_id']) && $_GET['workshop_id'] > 0) {
			$workshop_id = $_GET['workshop_id'];
			$this->app_data['workshop_id'] = $workshop_id;
			$this->app_data['workshop_data'] = ControllerWorkshop::get_workshop_details($workshop_id);
			$registered_seats = 0;
			
			$registered_seats = $this->app_data['workshop_data']['data']['registered_seats'];
			
			$this->app_data['workshop_seats_available'] = ControllerWorkshop::return_workshop_seats_available($workshop_id, $registered_seats);
			
			if(isset($this->app_data['workshop_data']['data']['workshop_user_id'])) {
				$workshop_user_id = $this->app_data['workshop_data']['data']['workshop_user_id'];
				if(is_numeric($workshop_user_id)) {
					$this->app_data['trainer_info'] = ControllerWorkshop::get_trainer_details($workshop_user_id);
					$this->app_data['profile_data'] = ControllerWorkshop::return_teacher_institute_profile_name_link($workshop_user_id);
				}
			}			
		} else {
			$workshop_list_array = ControllerWorkshop::get_workshops_list();
			$workshop_list = $workshop_list_array['html'];
			$workshop_list_page_no = $workshop_list_array['page_no'];
			
			$featured_list_array = ControllerWorkshop::get_workshops_list(array('featured'=>'yes'));
			$featured_list = $featured_list_array['html'];
			$new_featured_list = $featured_list_array['featured_workshops'];
			$this->app_data['new_featured_list'] = $new_featured_list;
			$this->app_data['featured_workshops'] = $featured_list;
			$this->app_data['workshop_list_html'] = $workshop_list;
			$this->app_data['workshop_list_page_no'] = $workshop_list_page_no;
			$this->app_data['pdg_locality'] = PDGManageCache::fn_load_cache('pdg_locality');
			$this->app_data['pdg_city'] = PDGManageCache::fn_load_cache('pdg_city');
			$this->app_data['pdg_workshop_categories'] = PDGManageCache::fn_load_cache('pdg_workshop_categories');
		}
	}

	public function fn_get_header() {
		return $this->load_view( 'ver2/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'ver2/footer' );
	}

	public function fn_get_content() {
		global $wpdb;
		if(isset($this->app_data['workshop_id'])) {
			if($this->app_data['workshop_data']['error']) {
				//error true
				$str_content = $this->load_view( 'workshop/public/workshop_error' );
			} else {
				$view_workshop = FALSE;
				
				if(current_user_can('manage_options') || current_user_can('pdg_cap_workshop_edit')) {
					$view_workshop = TRUE;	
				} else{
					$workshop_user_id = $this->app_data['workshop_data']['data']['workshop_user_id'];
					$current_user_id = isset($this->app_data['pdg_current_user']) ? $this->app_data['pdg_current_user']->ID : '';
					
					if($workshop_user_id == $current_user_id) {
						$view_workshop = TRUE;
					} else {
						$view_workshop = TRUE;
						if(isset($this->app_data['workshop_data']['data']['is_approved'])) {
							$view_workshop = $this->app_data['workshop_data']['data']['is_approved'] == 'yes' ? TRUE : FALSE;
						}
					}					
				}
				if($view_workshop) {
					$str_content = $this->load_view( 'workshop/public/workshop_info' );	
				} else {
					$this->app_data['workshop_data']['message'] = 'This workshop has not been approved yet! Please visit this page later!';
					$str_content = $this->load_view( 'workshop/public/workshop_error' );
				}
			}
		} else {
			$str_content = $this->load_view( 'workshop/public/index' );
		}
		
		return $str_content;
	}
	
	public static function workshop_filter_ajax() {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();
		
		$workshopCategory = is_numeric($_POST['workshopCategory']) ? $_POST['workshopCategory'] : '';
		$workshopType = $_POST['workshopType'];
		$workshopCity = is_numeric($_POST['workshopCity']) ? $_POST['workshopCity'] : '';
		$workshopLocality = is_numeric($_POST['workshopLocality']) ? $_POST['workshopLocality'] : '';
		$workshopStatus = $_POST['workshopStatus'];
		$page_no = $_POST['pageNo'];
		$paginate = $_POST['paginate'];
		
		$workshop_filter = array(
			'category' => $workshopCategory,
			'type' => $workshopType,
			'city' => $workshopCity,
			'locality' => $workshopLocality,
			'page_no' => $page_no,
			'paginate' => $paginate
			);
		
		switch($workshopStatus) {
			case 'active' :
			$workshop_filter['active'] = 'yes';
			break;
			case 'inactive' :
			$workshop_filter['active'] = 'no';
			break;
			case 'all':
			$workshop_filter['active'] = 'all';
			break;
		}

		$workshop_list_array = ControllerWorkshop::get_workshops_list($workshop_filter);
		$workshop_list = $workshop_list_array['html'];
		$workshop_list_page_no = $workshop_list_array['page_no'];
		if(empty($workshop_list)) {
			$workshop_list = '<div class="col-md-12"><div class="alert alert-warning"><h3>Sorry! Workshop Information not available!</h3></div></div>';
		}
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Search complete!';
		$return_message['data'] = $workshop_list;
		$return_message['pageNo'] = $workshop_list_page_no;
		echo json_encode($return_message);
		die();
	}

	public static function get_workshops_list($workshop_data = array()) {
		global $wpdb;
		$featured_list_array = array();
		$limit_start_from = $page_no = 0;
		$no_of_records_per_page = 12;
		$is_active = 'yes';
		$is_featured = 'no';
		$workshop_category = '';
		$workshop_type = '';
		$workshop_city = '';
		$workshop_locality = '';
		$paginate = 'default';
		
		if(isset($workshop_data['paginate'])) {
			$paginate = $workshop_data['paginate'];
		}
		
		if(isset($workshop_data['no_of_records_per_page']) && $workshop_data['no_of_records_per_page'] > 0) {
			$no_of_records_per_page = $workshop_data['no_of_records_per_page'];
		}

		if(isset($workshop_data['page_no']) && is_numeric($workshop_data['page_no'])) {
			$page_no = $workshop_data['page_no'];
			$limit_start_from =  $page_no * $no_of_records_per_page;
		}
		
		$str_active_sql = " and is_active = 'yes' ";
		
		if(isset($workshop_data['active'])) {
			switch($workshop_data['active']) {
				case 'yes':
				case 'no':
				$is_active = $workshop_data['active'];
				$str_active_sql = " and is_active = '$is_active' ";
				break;
				default:
				$is_active = '';
				$str_active_sql = '';
				break;
			}
		}
		$str_featured_sql = '';
		if(isset($workshop_data['featured']) && $workshop_data['featured'] == 'yes') {
			$is_featured = 'yes';
			$str_featured_sql = " and is_featured = '$is_featured' ";
		} else {
			$str_featured_sql = " and is_featured = 'no' ";
		}
		
		$str_category_sql = '';
		if(isset($workshop_data['category']) && $workshop_data['category'] > 0) {
			$workshop_category = $workshop_data['category'];
			$str_category_sql = " and category_id = $workshop_category ";
		}
		
		$str_workshop_type_sql = '';
		if(isset($workshop_data['type'])) {
			switch($workshop_data['type']) {
				case 'free':
				case 'paid':
				$workshop_type = $workshop_data['type'];
				$str_workshop_type_sql = " and workshop_type = '$workshop_type' ";
				break;
				default:
				$workshop_type = '';
				break;
			}
		}
		$str_workshop_city_sql = '';
		if(isset($workshop_data['city']) && $workshop_data['city'] > 0) {
			$workshop_city = $workshop_data['city'];
			$str_workshop_city_sql = " and workshop_city = $workshop_city ";
		}
		
		$str_workshop_locality_sql = '';
		if(isset($workshop_data['locality']) && $workshop_data['locality'] > 0) {
			$workshop_locality = $workshop_data['locality'];
			$str_workshop_locality_sql = " and workshop_locality = $workshop_locality ";
		}
		
		$str_sql = "
		select * 
		from 
		view_pdg_workshops 
		where 
		is_approved = 'yes'  
		$str_active_sql
		$str_featured_sql
		$str_category_sql
		$str_workshop_type_sql
		$str_workshop_city_sql
		$str_workshop_locality_sql
		order by workshop_id DESC 
		limit $limit_start_from, $no_of_records_per_page
		";
		
		$str_return = '';
		$workshop_data = $wpdb->get_results($str_sql);	
		
		$str_error_msg = '';
		switch($paginate) {
			case 'yes' :
			$str_error_msg = '<strong>Sorry! No more workshops available!</strong>';
			break;
			case 'no' :
			$str_error_msg = '<strong>Sorry, we didn\'t find workshops as per your filter criteria.Please remove some filters to generalize your search.</strong>';
			break;
			default:				
			$str_error_msg = '<strong>Sorry! No more workshops available!</strong>';
			break;
		}
		
		if(empty($workshop_data)) {
			$page_no = 0;
			$str_return =  '
			<div class="col-md-12">
				<div class="bs-component">
					<div class="alert alert-dismissible alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						'.$str_error_msg.'			            
					</div>       
				</div>
			</div>
			';
		} else {
			foreach($workshop_data as $workshop_info) {
				$str_workshop_card = self::workshop_card_html($workshop_info);
				$str_return .= $str_workshop_card;
				$is_featured_local = $workshop_info->is_featured;
				if ($is_featured_local=='yes') {
					$featured_list_array[] = $str_workshop_card;
				}
			}	
		}
		
		$next_page_no = $page_no + 1;
		
		$return_data = array(
			'html' => $str_return,
			'page_no' => $next_page_no,
			'featured_workshops' => $featured_list_array 
			); 
		return $return_data;
	} 
	public static function workshop_card_html($workshop_data) {
		global $wpdb;
		$workshop_id = $workshop_data->workshop_id;
		$workshop_user_id = $workshop_data->user_id;
		$workshop_name = stripslashes($workshop_data->workshop_name);

		$about_workshop = stripslashes($workshop_data->about_workshop);
		$start_date = $workshop_data->start_date;
		$end_date = $workshop_data->end_date;
		if(!empty($start_date)) {
			$date = date_create($start_date);
			$start_date = date_format($date,"d M Y");
		}
		if(!empty($end_date)) {
			$date = date_create($end_date);
			$end_date = date_format($date,"d M Y");
		}

		$str_date_title = 'From: '.$start_date.' To: '.$end_date;

		$workshop_image = ControllerManageworkshop::get_workshop_profile_image($workshop_id);
		if( strlen( $about_workshop) > 120) {
			$about_workshop = explode( "\n", wordwrap( $about_workshop, 240));
			$about_workshop = $about_workshop[0] . '...';
		}

		$str_workshop_type = '';
		if($workshop_data->workshop_type == 'free') {
			$str_workshop_type = '&nbsp;<i class="fa fa-smile-o" aria-hidden="true"></i>&nbsp;Free&nbsp;';
		} else {
			$str_workshop_type = '
			<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;Paid&nbsp;';
		}

		$trainer_name = $workshop_data->trainer_name;
		$workshop_category = $workshop_data->workshop_category;
		$city = $workshop_data->city_name;
		$locality = $workshop_data->locality;
		$seats = $workshop_data->workshop_seats;
		$seats_available = ControllerWorkshop::return_workshop_seats_available($workshop_id, $seats);
		$pincode = $workshop_data->workshop_pincode;
		$is_featured = $workshop_data->is_featured;
		$is_active = $workshop_data->is_active;

		$str_disabled_booking_button = ' disabled';
		if($is_active == 'yes') {
			$str_disabled_booking_button = '';
		}

		$str_featured_content = 'col-md-3';
		if($is_featured == 'yes') {
			$str_featured_content = 'col-md-12';
		}

		if($seats_available <= 0) {
			$str_disabled_booking_button = ' disabled';	
		}

		$str_return = '<div class="col-xs-12 col-sm-12 '.$str_featured_content.'">
		<div class="card">
			<a class="img-card" href="#">
				<img src="'.$workshop_image.'"/>
			</a>
			<div class="card-content">
				<h4 class="card-title">
					<div class="info">
						<div class="row">
							<div class="col-sm-12">
								'.$str_workshop_type.' <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;'.$workshop_category.'
							</div>
						</div>
					</div>
					<center>
						<div class="workshop_name">
							'.$workshop_name.'
						</div>
					</center>
				</h4>
				<p>
					<center>
						<span id="icon_holder">
							<i class="fa fa-map-marker" aria-hidden="true"></i></span>
							
							<span id="address_holder">'
								.$locality.', <br />'.$city.'- '.$pincode.'
							</span>
						</center>
						<br />
						<center><i class="fa fa-calendar-o" aria-hidden="true"></i><strong>&nbsp;'.$start_date.'</strong></center>	

					</p>
				</div>
				<div class="card-read-more">
					<div class="row">
						<div class="col-md-6">
							<a  href="'.home_url('/workshop/?workshop_id='.$workshop_id).'" target="_blank" 
							class="btn btn-primary cmd_show_workshop_details"><i class="fa fa-info" aria-hidden="true"></i> info</a>
						</div>
						<div class="col-md-5">
							<a href="" class="btn btn-raised btn-sm btn-success btn-md cmd_book_workshop" '.$str_disabled_booking_button.' data-workshop_id="'.$workshop_id.'"><i class="fa fa-ticket" aria-hidden="true"></i> Reserve</a>
						</div>	
					</div>
				</div>
				<div class="card-footer">	
					<b>'.$seats_available.'</b> seats left
				</div>
			</div>
		</div>';
		return $str_return;
	}

	public static function return_workshop_seats_available($workshop_id, $seats_registered = 0) {
		global $wpdb;
		$available_seats = 0;
		if(!is_numeric($workshop_id) || $workshop_id <= 0) {
			return $available_seats;
		}
		$str_sql = "select 
		(pdg_workshops.workshop_seats - ifnull(sum(pdg_workshop_seat_booking.total_seats_booked),0) ) as seats_left
		from pdg_workshops
		left join pdg_workshop_seat_booking on pdg_workshop_seat_booking.workshop_id = pdg_workshops.workshop_id
		where pdg_workshops.workshop_id =  $workshop_id";

		$available_seats = $wpdb->get_var($str_sql);
		$available_seats = is_numeric($available_seats) ? $available_seats : 0;

		//$available_seats = $seats_registered - $booked_seats;

		return $available_seats;
	}

	public static function get_workshop_details($workshop_id) {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();

		if(!is_numeric($workshop_id) || $workshop_id <= 0) {
			$return_message['message'] = 'Error! Workshop ID format is not correct!';
			return $return_message;
		}		

		$workshop_data_db = $wpdb->get_results("select * from view_pdg_workshops where workshop_id = $workshop_id");

		if(empty($workshop_data_db)) {
			$return_message['message'] = 'Error! Workshop information could not be found!';
			return $return_message;
		}
		$workshop_user_id = '';
		$is_approved = '';
		$registered_seats = 0;
		foreach($workshop_data_db as $workshop_data) {
			$workshop_user_id = $workshop_data->user_id;
			$is_approved = $workshop_data->is_approved;
			$registered_seats = $workshop_data->workshop_seats;
		}

		$sliders = self::fn_get_workshop_gallery_slider($workshop_id);

		$available_seats = ControllerWorkshop::return_workshop_seats_available($workshop_id, $registered_seats);
		
		$return_message['error'] = FALSE;
		$return_message['data'] = array(
			'workshop_data' => $workshop_data_db,
			'slider' => $sliders,
			'workshop_user_id' => $workshop_user_id,
			'is_approved' => $is_approved,
			'registered_seats' => $registered_seats,
			'available_seats' => $available_seats,
		);

		return $return_message;
	}

	public static function fn_get_workshop_gallery_slider($workshop_id) {
		global $wpdb;

		$str_return = '
		<center><br/><h3>Sorry! No images available!</h3></center>
		';

		$gallery_db = $wpdb->get_results("select * from pdg_workshop_images where workshop_id = $workshop_id ");
		if(!empty($gallery_db)) {

			$str_images = '';
			$str_list = '';
			$counter = 0;
			foreach($gallery_db as $gallery_item) {

				$str_active = '';
				if($counter == 0) {
					$str_active = ' active';
				}

				$str_images .= '
				<div class="item '.$str_active.' text-center"> 
					<img src="'.$gallery_item->image_url.'"  data-holder-rendered="true" class=" center-block workshop_slider_image"> 
				</div>
				';
				$str_list .= '<li data-target="#workshop_carousel" data-slide-to="'.$counter.'" class=" '.$str_active.'"></li>';				
				$counter++;
			}

			$str_return = '
			<div class="bs-example" data-example-id="simple-carousel"> 
				<div class="carousel slide" id="workshop_carousel" data-ride="carousel"> 
					<ol class="carousel-indicators"> 
						'.$str_list.' 
					</ol> 
					<div class="carousel-inner" role="listbox">
						'.$str_images.'
					</div> 
					<a href="#workshop_carousel" class="left carousel-control" role="button" data-slide="prev"> 
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
						<span class="sr-only">Previous</span> 
					</a> 
					<a href="#workshop_carousel" class="right carousel-control" role="button" data-slide="next"> 
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> 
						<span class="sr-only">Next</span> 
					</a> 
				</div> 
			</div>
			';
		}
		return $str_return;
	}

	public static function get_trainer_details($trainer_user_id) {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();

		if(!is_numeric($trainer_user_id) || $trainer_user_id <= 0) {
			$return_message['message'] = 'Error! Trainer User ID format is not correct!';
			return $return_message;
		}

		$str_trainer_info_sql = "
		select
		pdg_workshop_trainer_info.*,
		pdg_teaching_xp.teaching_xp
		from pdg_workshop_trainer_info
		left join pdg_teaching_xp on pdg_teaching_xp.teaching_xp_id = pdg_workshop_trainer_info.trainer_experience
		where pdg_workshop_trainer_info.user_id = $trainer_user_id
		";

		$trainer_info_db = $wpdb->get_results($str_trainer_info_sql);
		if(empty($trainer_info_db)) {
			$return_message['message'] = 'Error! Trainer Info not available!';
			return $return_message;
		}

		$str_trainer_qualifications = "
		select				
		pdg_qualification.qualification
		from pdg_workshop_qualification
		left join pdg_qualification on pdg_qualification.qualification_id = pdg_workshop_qualification.qualification_id
		where pdg_workshop_qualification.user_id = $trainer_user_id
		order by pdg_workshop_qualification.workshop_qualification_id desc
		";

		$trainer_data_db = $wpdb->get_results($str_trainer_qualifications);
		$trainer_qualifications_db = $wpdb->get_results($str_trainer_qualifications);
		$trainer_qualifications = '';
		if(!empty($trainer_qualifications_db)) {

			$qualifications_array = array();
			foreach($trainer_qualifications_db as $qualification_info) {
				$qualifications_array[] = $qualification_info->qualification;
			}
			$trainer_qualifications = implode(', ', $qualifications_array);
		}

		$return_message['error'] = FALSE;
		$return_message['data'] = array(
			'trainer_data' => $trainer_info_db,
			'qualifications' => $trainer_qualifications
			);

		return $return_message;
	}

	public static function reserve_workshop_seats_ajax() {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();

		//check for security
		PedagogeUtilities::ajax_session_security_check();

		$seat_booking_data = array();
		parse_str( $_POST['workshop_reservation_data'], $seat_booking_data );

		$txt_participant_full_name =  isset($seat_booking_data['txt_participant_full_name']) ? sanitize_text_field($seat_booking_data['txt_participant_full_name']) : '';
		$txt_participant_email_address =  isset($seat_booking_data['txt_participant_email_address']) ? sanitize_text_field($seat_booking_data['txt_participant_email_address']) : '';
		$txt_participant_mobile_no = isset($seat_booking_data['txt_participant_mobile_no']) ? sanitize_text_field($seat_booking_data['txt_participant_mobile_no']) : '';
		$txt_participant_total_seats = isset($seat_booking_data['txt_participant_total_seats']) ? sanitize_text_field($seat_booking_data['txt_participant_total_seats']) : '';
		//modal_hidden_workshop_id
		$workshop_id = isset($seat_booking_data['modal_hidden_workshop_id']) ? sanitize_text_field($seat_booking_data['modal_hidden_workshop_id']) : '';

		if(empty($txt_participant_full_name) || empty($txt_participant_email_address)){
			$return_message['message'] = 'Error! Please input all the required fields and try again.';			
			echo json_encode($return_message );
			die();
		}

		if(!is_numeric($workshop_id) || $workshop_id <= 0) {
			$return_message['message'] = 'Error! Workshop ID is not correct!';			
			echo json_encode($return_message );
			die();
		}

		if(!is_numeric($txt_participant_total_seats) || $txt_participant_total_seats <= 0) {
			$return_message['message'] = 'Error! You must reserve atleast one seat!';			
			echo json_encode($return_message );
			die();
		}

		if(!is_numeric($txt_participant_mobile_no) || strlen($txt_participant_mobile_no) != 10) {
			$return_message['message'] = 'Error! Please input your 10 digit mobile no and try again.';			
			echo json_encode($return_message );
			die();
		}

		/**
		 * Check if seats are available for reservation or not
		 */
		$available_seats = self::return_workshop_seats_available($workshop_id);
		if($available_seats < $txt_participant_total_seats) {
			$return_message['message'] = 'Sorry! Only '.$available_seats.' seat(s) are available for booking!';			
			echo json_encode($return_message );
			die();
		}
		$str_available_seats_sql = "";
		
		
		$insert_array = array(
			'workshop_id' => $workshop_id,
			'participant_name' => $txt_participant_full_name,
			'participant_email' => $txt_participant_email_address,
			'participant_mobile' => $txt_participant_mobile_no,
			'total_seats_booked' => $txt_participant_total_seats,
			);
		$data_format = array('%d', '%s', '%s', '%s', '%d');

		$wpdb->insert('pdg_workshop_seat_booking', $insert_array, $data_format);
		
		ControllerManageworkshop::workshop_emails($workshop_id, 'workshop_booking_admin', $insert_array);
		ControllerManageworkshop::workshop_emails($workshop_id, 'workshop_booking_user', $insert_array);
		
		$return_message['error'] = FALSE;
		$return_message['message'] = 'Your booking is accepted! Booking confirmation will be sent via Email.';
		echo json_encode($return_message );
		die();
	}

	public static function paginate_workshop_ajax() {
		global $wpdb;
		$return_message = PedagogeUtilities::return_message_format();
		
		//check for security
		PedagogeUtilities::ajax_session_security_check();

		$page_no = $_POST['page_no'];
		if(!is_numeric($page_no)) {
			$page_no = 0;
		}
		$page_no++;
		
		$return_message['message'] = 'Error! Please input all the required fields and try again.';			
		echo json_encode($return_message );
		die();
	}
	
	/**
	 * Given a user ID this function will return Full Name and Profile Link for a teacher/institute.
	 * Profile link and Name will be returned only if profile information is available.
	 * 
	 * @param int Wordpress User ID based on which required information will be found and returned.
	 * @return Array Returns an array containing profile_link and profile_name if information available else return empty array.
	 * @author Niraj Kumar
	 */
	public static function return_teacher_institute_profile_name_link($user_id) {
		global $wpdb;
		$return_array = array();
		$profile_name = '';
		$profile_link = '';		
		
		if(!is_numeric($user_id) || $user_id <= 0) {
			return $return_array;
		}
		$str_sql = "SELECT * FROM view_pdg_teachers_institutes where user_id = $user_id";
		$profile_data = $wpdb->get_results($str_sql);
		
		foreach($profile_data as $profile) {
			$profile_name = $profile->teacher_name;
			$profile_slug = $profile->profile_slug;
			$profile_user_role = $profile->user_role_name;
			switch($profile_user_role) {
				case 'teacher':
					$profile_link = home_url('/teacher/'.$profile_slug);
					break;
				case 'institution':
					$profile_link = home_url('/institute/'.$profile_slug);
					break;
			}
		}
		
		$return_array = array(
			'profile_name' => $profile_name,
			'profile_link' => $profile_link
		);
		
		return $return_array;
	}
}