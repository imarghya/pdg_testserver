<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerMemberlogin extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		// nothing to do here.
		// Only master controller gets autoconstructed.		 
	}
	
	public function fn_construct_class() {
		//Initialize Master Controller
		parent::__construct(); 
		
		//Page Title
		$this->title = 'Settings Page:';
		
		//You can load your custom assets (css/js) specific to this page. 
		//Master page has already loaded all the required scripts for a blank page.
		$this->fn_load_scripts(); 
		
		//Set this variable to define body class of your page.
		$this->body_class .= ' page-register login-alt page-header-fixed';
		
		//Register variables required in the global scope. 
		//Variables are stored in $this->app_data array variable.
		$this->fn_register_common_variables();	

			
	}
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		//$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		return $this->load_view('memberlogin/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		return $this->load_view('memberlogin/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		//$template_vars['example_string'] = 'Hello World!';
		//$template_vars['example_number'] = 4;
		
		//$content = $this->load_view('settings/index', $template_vars);		
		$content = $this->load_view('memberlogin/index');	
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		//Available for the page at global scope.
		$this->app_data['global_example_string'] = 'example string';
		
		//If you wish to include a dynamic inline javascript code then use the variable 'inline_js' available in the $app_data variable.
		// This variable is a string, so append your script string to it. e.g.
		$this->app_data['inline_js'] .= '<script type="text/javascript">console.log("Testing");</script>';
	}
	
	// Example of Ajax request processing
	public static function member_login_ajax() {		
			global $wpdb;
		$home_url = home_url();
		
		

		$return_message = array (
			'error'      => true,
			'loggedin'   => false,
			'error_type' => '',
			'message'    => '',
			'url'        => $home_url,
		);

		$user_name = sanitize_user( $_POST['user_name'] );
		$user_pass = sanitize_text_field( $_POST['pass_word'] );
		
		$hidden_hashed_password = $_POST['hidden_hashed_password'];
		

		$server_name = $_SERVER['SERVER_NAME'];
		$redirect_to = '';
		if ( $server_name == 'www.pedagoge.com' ) {
			$redirect_to = $nexturl;
		} else {
			$redirect_to = substr( $nexturl, 9 );
		}


	

		

		

		$found_user = '';
		if ( ! filter_var( $user_name, FILTER_VALIDATE_EMAIL ) === false ) {
			$is_email = true;
			$found_user = get_user_by( 'email', $user_name );
		} else {
			$found_user = get_user_by( 'login', $user_name );
		}


		if ( ! is_a( $found_user, 'WP_User' ) ) {
			//throw wrong username/email error
			$return_message['error_type'] = 'wrong_username';
			$return_message['message'] = 'Error! Wrong username/email! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$user_login = $found_user->user_login;
		$found_user_id = $found_user->ID;

		$credentials = array ();
		$credentials['user_login'] = $user_login;
		$credentials['user_password'] = $user_pass;
		

		$user_signon = wp_signon( $credentials, false );

		if ( is_wp_error( $user_signon ) ) {

			/**
			 * 1. Check if it is an email
			 * 2. Check old DB and find user by email
			 * 3. get all data and hash new password
			 * 4. if it matches then login
			 */
			if ( $is_email ) {

				if ( ! is_numeric( $found_user_id ) || $found_user_id <= 0 ) {
					$return_message['error_type'] = 'wrong_password';
					$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

					$return_message['message'] = '
						Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
						Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
					';

					echo json_encode( $return_message );
					die();
				}

				$str_sql = "select * from members where email = '$user_name'";

				$old_member_data = $wpdb->get_results( $str_sql );
				if ( empty( $old_member_data ) ) {

					$return_message['error_type'] = 'wrong_password';
					$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

					$return_message['message'] = '
						Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
						Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
					';
					echo json_encode( $return_message );
					die();
				} else {

					$old_password = '';
					$old_salt = '';
					$is_password_reset = '';
					foreach ( $old_member_data as $member_data ) {
						$old_password = $member_data->password;
						$old_salt = $member_data->salt;
						$is_password_reset = $member_data->password_reset;
						$member_id=$member_data->id;
						
						
					}
					
					$old_salted_password = hash( 'sha512', $hidden_hashed_password . $old_salt );

					if ( $old_password == $old_salted_password ) {
						/**
						 * Login now and redirect to password reset
						 */

						wp_set_auth_cookie( $found_user_id, $remember );
						wp_set_current_user( $found_user_id );

						$return_message['url'] = home_url( '/profile' );

						$return_message['loggedin'] = true;
						$return_message['error'] = false;
						$return_message['message'] = 'Login Successful! Please wait... redirecting!';
						echo json_encode( $return_message );
						die();


					} else {
						$return_message['error_type'] = 'wrong_password';
						$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

						$return_message['message'] = '
							Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
							Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
						';
						echo json_encode( $return_message );
						die();
					}

				}
			} else {

				$return_message['error_type'] = 'wrong_password';
				$str_email = $is_email ? 'Email - ' . $user_name : 'User Name - ' . $user_name;

				$return_message['message'] = '
					Error! Wrong password for the ' . $str_email . '! Please try again. <br/> 
					Lost your password? <a href="' . home_url( '/reset' ) . '">Recover your password.  </a>
				';
				echo json_encode( $return_message );
				die();
			}


		} else {
			wp_set_current_user( $user_signon->ID );
			$loggedin_userid = $user_signon->ID;
			if ( ! empty( $redirect_to ) ) {
				$redirect_to = home_url( $redirect_to );
			}

			/**
			 * Find user role
			 */


			$teacher_data = $wpdb->get_results( "select * from pdg_teacher where user_id = $loggedin_userid" );
			if ( empty( $teacher_data ) ) {
				$institute_data = $wpdb->get_results( "select * from pdg_institutes where user_id = $loggedin_userid" );
				if ( ! empty( $institute_data ) ) {
					$is_active = '';
					$is_approved = '';
					$profile_slug = '';
					foreach ( $institute_data as $institute_info ) {
						$is_active = $institute_info->active;
						$is_approved = $institute_info->approved;
						$profile_slug = $institute_info->profile_slug;
					}
					if ( $is_active == 'no' || $is_approved == 'no' ) {
						$redirect_to = home_url( '/profile' );
					} else {
						$redirect_to = home_url( '/institute/' . $profile_slug );
					}
				}
			} else {
				$is_active = '';
				$is_approved = '';
				$profile_slug = '';
				foreach ( $teacher_data as $teacher_info ) {
					$is_active = $teacher_info->active;
					$is_approved = $teacher_info->approved;
					$profile_slug = $teacher_info->profile_slug;
				}
				if ( $is_active == 'no' || $is_approved == 'no' ) {
					$redirect_to = home_url( '/profile' );
					
					//$redirect_to = 'localhost/pedagoge/settings';
				} else {
					$redirect_to = home_url( '/teacher/' . $profile_slug );
				}
			}
			if ( empty( $redirect_to ) ) {
				$redirect_to = home_url( '/' );
			}
			$return_message['url'] = $redirect_to;
			$return_message['loggedin'] = true;
			$return_message['error'] = false;
			$return_message['message'] = 'Login Successful! Please wait... redirecting!';
			//$return_message['userid']=$found_user_id;
			
			$_SESSION['member_id']=$found_user_id;
			
			$return_message['userid']=$_SESSION['member_id'];

			echo json_encode( $return_message );
			die();
		}

		die();
	}
	
}
		
		
		
	




