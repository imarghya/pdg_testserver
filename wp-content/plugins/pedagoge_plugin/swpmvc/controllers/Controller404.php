<?php
/**
 * Page Generating class for 404 pages. 
 * 
 * @author Niraj Kumar
 */
 
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * When routes do not match then this controller generates and returns a 404 Error Page. This class overrides wordpress's default 404 error page. 
 * 
 * @author Niraj Kumar
 */
class Controller404 extends ControllerMaster implements ControllerMasterInterface {
	
	/**
	 * Default Constructor. Not needed
	 */
	public function __construct() {
		
	}
		
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Page not found!';
		$this->body_class .= ' page-register login-alt page-header-fixed';		
	}
	
	/**
	 * Loads view file (html) for header section of the 404 page
	 */
	public function fn_get_header() {
		return $this->load_view('login/header');		
	}
	
	/**
	 * Loads view file (html) for footer section of the 404 page
	 */
	public function fn_get_footer() {
		return $this->load_view('login/footer');
	}
	
	/**
	 * Loads view file (html) for content section of the 404 page
	 */
	public function fn_get_content() {
		return $this->load_view('404');
	}
}
