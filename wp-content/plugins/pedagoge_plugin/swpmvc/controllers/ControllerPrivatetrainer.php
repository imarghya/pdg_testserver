<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerPrivatetrainer extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Pedagoge Trainer Profile:';
		$this->fn_load_scripts();
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		unset( $this->css_assets['waves'] );
		unset( $this->css_assets['offcanvasmenueffects'] );
		unset( $this->css_assets['3d-bold-navigation'] );
		unset( $this->css_assets['ankitesh-search'] );
		unset( $this->css_assets['ankitesh-modern'] );
		unset( $this->css_assets['ankitesh-theme-green'] );
		unset( $this->css_assets['ankitesh-custom'] );
		unset( $this->css_assets['ankitesh-validationEngine'] );

		unset( $this->header_js['ankitesh-forms'] );
		unset( $this->header_js['modernizr'] );
		unset( $this->header_js['snap-svg'] );
		unset( $this->footer_js['jquery-slimscroll'] );
		unset( $this->footer_js['waves'] );
		unset( $this->footer_js['ankitesh-modern'] );
		unset( $this->footer_js['offcanvasmenueffects'] );
		unset( $this->footer_js['validationengine'] );
		unset( $this->footer_js['validationengine-en'] );

		$profile_version = '0.6';
		/**
		 * CSS Loading
		 */
		$this->css_assets['proui_plugins']    = PEDAGOGE_PROUI_URL . "/css/plugins.css?ver=" . $this->fixed_version;
		$this->css_assets['proui_main']       = PEDAGOGE_PROUI_URL . "/css/main.css?ver=" . $this->fixed_version;
		$this->css_assets['proui_themes']     = PEDAGOGE_PROUI_URL . "/css/themes.css?ver=" . $this->fixed_version;
		$this->css_assets['select2']          = $this->registered_css['select2'] . '?ver=' . $this->fixed_version;
		$this->css_assets['custom']           = PEDAGOGE_PROUI_URL . "/css/custom.css?ver=" . $this->fixed_version;
		$this->css_assets['bsdatetimepicker'] = $this->registered_css['bsdatetimepicker'] . '?ver=' . $this->fixed_version;

		$this->app_css['trainer_registration'] = PEDAGOGE_ASSETS_URL . "/css/teacher_registration.css?ver=" . $profile_version;
		/**
		 * JS Loading
		 */

		$this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL . "/js/plugins.js?ver=" . $this->fixed_version;
		$this->footer_js['app_js']       = PEDAGOGE_PROUI_URL . "/js/app.js?ver=" . $this->fixed_version;

		$this->footer_js['moment']  = $this->registered_js['moment'] . '?ver=' . $this->fixed_version;
		$this->footer_js['select2'] = $this->registered_js['select2'] . '?ver=' . $this->fixed_version;

		$this->footer_js['bootbox'] = $this->registered_js['bootbox'] . "?ver=" . $this->fixed_version;

		$this->footer_js['bsdatetimepicker'] = $this->registered_js['bsdatetimepicker'] . '?ver=' . $this->fixed_version;
		$this->footer_js['backstretch']      = PEDAGOGE_PROUI_URL . "/js/jquery.backstretch.min.js?ver=" . $this->fixed_version;

		$this->footer_js['notify'] = BOWER_ROOT_URL . "/notifyjs/dist/notify.js?ver=" . $this->fixed_version;

		$this->footer_js['blockui'] = BOWER_ROOT_URL . "/blockUI/jquery.blockUI.js?ver=" . $this->fixed_version;

		$this->css_assets['validationEngine']   = get_template_directory_uri() . "/assets/css/validationEngine/validationEngine.jquery.css?ver=" . $profile_version;
		$this->footer_js['validationEngine']    = get_template_directory_uri() . "/assets/js/validationEngine/jquery.validationEngine.js?ver=" . $profile_version;
		$this->footer_js['validationEngine-en'] = get_template_directory_uri() . "/assets/js/validationEngine/jquery.validationEngine-en.js?ver=" . $profile_version;
		/**
		 * Cropper
		 */

		$this->css_assets['cropper'] = $this->registered_css['cropper'] . '?ver=' . $this->fixed_version;
		$this->footer_js['cropper']  = $this->registered_js['cropper'] . '?ver=' . $this->fixed_version;

		$this->app_js['pedagoge_profile'] = PEDAGOGE_ASSETS_URL . "/js/trainer_private.js?ver=" . $profile_version;


	}

	public function fn_get_header() {
		return $this->load_view( 'search/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'search/footer' );
	}

	public function fn_get_content() {

		$current_user_id = $this->app_data['pdg_current_user']->ID;

		if ( is_user_logged_in() ) {
			$view_trainer_data = $this->app_data['view_trainer_data'];
			$trainer_id        = '';
			foreach ( $view_trainer_data as $trainer_info ) {
				$trainer_id = $trainer_info->trainer_id;
			}

			if ( empty( $view_trainer_data ) ) {
				return '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-error">
								<div class="panel-heading">Profile already created!</div>
								<div class="panel-body">
									<br />
									<h2>Error! Trainer Data is not set.</h2>
								</div>
							</div>
						</div>
					</div>
				</section>
			';
			} else {

				return $this->load_view( 'profile/pvttrainer/index' );
			}
		} else {
			return '
				<section class="site-section site-section-top">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-warning">
								<div class="panel-heading">User not logged in!</div>
								<div class="panel-body">
									<br />
									<h2 class="alert alert-warning">Error! User ID is not set. You need to log in!</h2>
								</div>
							</div>
						</div>
					</div>
				</section>
			';
		}
	}

	/**
	 * Function to register common variables to global application variables registry
	 */
	private function fn_register_common_variables() {
		global $wpdb;
		$current_user_id = $this->app_data['pdg_current_user']->ID;

		$view_trainer_model                  = new ModelMaster( 'view_pdg_trainer' );
		$view_trainer_data                   = $view_trainer_model->where( array ( 'user_id' => $current_user_id ) )->find();
		$this->app_data['view_trainer_data'] = $view_trainer_data;

		if ( ! empty( $view_trainer_data ) ) {
			$trainer_id     = '';
			$user_id        = '';
			$trainer_domain = array ();
			foreach ( $view_trainer_data as $trainer_info ) {
				$trainer_id                                                         = $trainer_info->trainer_id;
				$user_id                                                            = $trainer_info->user_id;
				$trainer_domain[ $trainer_info->trainer_domain_list_trainer_xp_id ] = array ( $trainer_info->trainer_domain_list_domain => $trainer_info->trainer_domain_list_trainer_experience );
			}
			$this->app_data['trainer_domain'] = $trainer_domain;

			$qualification_model                  = new ModelMaster( 'pdg_qualification' );
			$qualification_data                   = $qualification_model->all();
			$this->app_data['qualification_data'] = $qualification_data;

			$training_experience_model             = new ModelMaster( 'pdg_trainer_xp_list' );
			$training_experience                   = $training_experience_model->all();
			$this->app_data['training_experience'] = $training_experience;

			$pdg_domain_list_model         = new ModelMaster( 'pdg_domain_list' );
			$domain_list                   = $pdg_domain_list_model->all();
			$this->app_data['domain_list'] = $domain_list;

			$pdg_companies_logos               = new ModelMaster( 'pdg_companies_logos' );
			$companies_logos                   = $pdg_companies_logos->all();
			$this->app_data['companies_logos'] = $companies_logos;

			$pdg_trainer_references_types               = new ModelMaster( 'pdg_trainer_references_types' );
			$trainer_references_types                   = $pdg_trainer_references_types->all();
			$this->app_data['trainer_references_types'] = $trainer_references_types;

			$trainer_domain_list_sql = "
				select  
					domain_type, domain_id
				from 
					view_pdg_trainer_domain_list
				where
					trainer_id = " . $trainer_id;

			$trainer_domain_list = $wpdb->get_results( $trainer_domain_list_sql );
			if ( ! empty( $trainer_domain_list ) ) {
				$this->app_data['trainer_domain_list'] = $trainer_domain_list;
			}

			$str_trainer_qualification_sql = "
				select 
				    pdg_trainer_qualification.qualification_id,
				    pdg_qualification.qualification,
					pdg_qualification.qualification_type
				from 
				    pdg_trainer_qualification
				left join 
					pdg_qualification on pdg_qualification.qualification_id = pdg_trainer_qualification.qualification_id
				where 
					pdg_trainer_qualification.trainer_id = " . $trainer_id;

			$trainer_qualification_modes = $wpdb->get_results( $str_trainer_qualification_sql );
			if ( ! empty( $trainer_qualification_modes ) ) {
				$this->app_data['trainer_qualification_modes'] = $trainer_qualification_modes;
			}

			$sql_pdg_trainer_references = "
				SELECT 
					* 
				FROM 
					view_pdg_trainer_references
				where 
					trainer_id = " . $trainer_id;

			$view_pdg_trainer_references = $wpdb->get_results( $sql_pdg_trainer_references );
			if ( ! empty( $view_pdg_trainer_references ) ) {
				$this->app_data['view_pdg_trainer_references'] = $view_pdg_trainer_references;
			}

			$sql_user_video = "
				SELECT 
					* 
				FROM 
					pdg_user_video
				where 
					user_id = " . $user_id . " AND deleted = 'no'";

			$user_video_result = $wpdb->get_results( $sql_user_video );
			if ( ! empty( $user_video_result ) ) {
				$this->app_data['user_video_result'] = $user_video_result;
			}
		}

	}

	public static function fn_save_trainer_intro_ajax() {
		global $wpdb;
		$trainer_data     = array ();
		$return_message   = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => null
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message']    = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( isset( $_POST['trainer_intro_data'] ) ) {
			$trainer_data = json_decode( stripslashes( $_POST['trainer_intro_data'] ), true );
		} else {
			$return_message['error_type'] = 'trainer_intro_data';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		/*Validation starts*/

		$qualification_model                            = new ModelMaster( 'pdg_qualification' );
		$qualification_data                             = $qualification_model->all();
		$select_graduation_inlist_array                 = array ();
		$select_postgraduation_inlist_array             = array ();
		$select_professional_qualification_inlist_array = array ();

		foreach ( $qualification_data as $qualification ) {
			$qualification_name = $qualification->qualification;
			$qualification_id   = $qualification->qualification_id;
			$qualification_type = $qualification->qualification_type;
			switch ( $qualification_type ) {
				case 'bachelor':
					$select_graduation_inlist_array[ $qualification_id ] = $qualification_name;
					break;
				case 'postgrad':
					$select_postgraduation_inlist_array[ $qualification_id ] = $qualification_name;
					break;
				case 'professional_course':
					$select_professional_qualification_inlist_array[ $qualification_id ] = $qualification_name;
					break;
			}
		}

		$domain_model                     = new ModelMaster( 'pdg_domain_list' );
		$domain_data                      = $domain_model->all();
		$select_technical_domain_array    = array ();
		$select_nontechnical_domain_array = array ();

		foreach ( $domain_data as $domains ) {
			$domains_name = $domains->domain;
			$domains_id   = $domains->domain_id;
			$domains_type = $domains->domain_type;
			switch ( $domains_type ) {
				case 'technical':
					$select_technical_domain_array[ $domains_id ] = $domains_name;
					break;
				case 'nontechnical':
					$select_nontechnical_domain_array[ $domains_id ] = $domains_name;
					break;
			}
		}
		$training_experience_model                       = new ModelMaster( 'pdg_trainer_xp_list' );
		$training_experience                             = $training_experience_model->all();
		$select_average_training_experience_inlist_array = array ();

		foreach ( $training_experience as $experience ) {
			$experience_id                                                      = $experience->trainer_xp_id;
			$experience_name                                                    = $experience->trainer_experience;
			$select_average_training_experience_inlist_array [ $experience_id ] = $experience_name;
		}

		$validate = new ValidatorPedagoge( $trainer_data );

		$trainer_user_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.',
			'sql_escape'     => true
		) );

		$trainer_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_insti_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'numeric'        => true,
			'trim'           => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.',
			'sql_escape'     => true
		) );

		$check_trainer_id = $wpdb->get_var( "SELECT count(*) FROM `pdg_trainer` WHERE trainer_id = " . $trainer_id . " && user_id = " . $trainer_user_id );
		if ( ! $check_trainer_id ) {
			$return_message['error_type'] = 'trainer_id_unmatched';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}
		$trainer_details = $wpdb->get_results( "SELECT cv_path, sample_course_material_path FROM pdg_trainer WHERE deleted = 'no' AND trainer_id = " . $trainer_id );

		$trainer_user_info_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_info_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_edit_secret_key = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_profile_edit_secret_key',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user     = get_user_by( 'id', $trainer_user_id );
		$users_email = $wp_user->user_email;
		require_once( ABSPATH . 'wp-includes/class-phpass.php' );
		$wp_hasher = new PasswordHash( 8, true );
		if ( ! $wp_hasher->CheckPassword( $users_email, $trainer_edit_secret_key ) ) {
			$return_message['error_type'] = 'secret_unmatched';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$txt_trainer_first_name = $validate->set_rules( array (
			'name'           => 'txt_trainer_first_name',
			'placeholder'    => 'First Name',
			'type'           => 'custom',
			'alpha_space'    => true,
			'required'       => true,
			'trim'           => true,
			'min_length'     => 2,
			'max_length'     => 100,
			'sanitize_input' => true,
		) );

		$txt_trainer_last_name = $validate->set_rules( array (
			'name'           => 'txt_trainer_last_name',
			'placeholder'    => 'Second Name',
			'type'           => 'custom',
			'alpha_space'    => true,
			'required'       => true,
			'trim'           => true,
			'min_length'     => 1,
			'max_length'     => 100,
			'sanitize_input' => true,
		) );

		$txt_email_id = $validate->set_rules( array (
			'name'           => 'txt_email_id',
			'placeholder'    => 'Email ID',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'min_length'     => 1,
			'max_length'     => 200,
			'valid_email'    => true,
			'sanitize_input' => true,
			'is_duplicate'   => array (
				'table_name'        => 'wp_users',
				'column_name'       => 'user_email',
				'primary_key'       => 'ID',
				'primary_key_value' => $trainer_user_id,
			),
		) );

		$txt_mobile_no = $validate->set_rules( array (
			'name'           => 'txt_mobile_no',
			'placeholder'    => 'Mobile No.',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 10,
			'valid_phone'    => true,
			'sanitize_input' => true,
			'is_duplicate'   => array (
				'table_name'        => 'pdg_user_info',
				'column_name'       => 'mobile_no',
				'primary_key'       => 'personal_info_id',
				'primary_key_value' => $trainer_user_info_id,
			),
		) );

		$txt_alternate_contact_no = $validate->set_rules( array (
			'name'           => 'txt_alternate_contact_no',
			'placeholder'    => 'Alternate Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$txt_address_of_correspondance = $validate->set_rules( array (
			'name'           => 'txt_address_of_correspondance',
			'placeholder'    => 'Address of Correspondance',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'min_length'     => 2,
			'max_length'     => 1000,
			'sanitize_input' => true,
		) );

		$select_gender = $validate->set_rules( array (
			'name'           => 'select_gender',
			'placeholder'    => 'Gender',
			'type'           => 'custom',
			'required'       => true,
			'in_list'        => array ( 'male' => 'Male', 'female' => 'Female', 'other' => 'other' ),
			'trim'           => true,
			'sanitize_input' => true,
		) );


		$minDateRange  = date( "Y" ) - 100 . '-01-01';
		$maxDateRange  = date( "Y" ) - 10 . '-01-01';
		$date_of_birth = $validate->set_rules( array (
			'name'           => 'date_of_birth',
			'placeholder'    => 'Date of birth',
			'type'           => 'custom',
			'valid_date'     => true,
			'required'       => true,
			'trim'           => true,
			'date_range'     => array ( 'min' => $minDateRange, 'max' => $maxDateRange ),
			'sanitize_input' => true,
		) );

		$select_graduation = $validate->set_rules( array (
			'name'           => 'select_graduation',
			'placeholder'    => 'Graduation',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'in_list'        => $select_graduation_inlist_array,
			'greater_than'   => 0,
			'sanitize_input' => true,
		) );

		$select_postgraduation = $validate->set_rules( array (
			'name'           => 'select_postgraduation',
			'placeholder'    => 'Post Graduation',
			'type'           => 'custom',
			'trim'           => true,
			'numeric'        => true,
			'in_list'        => $select_postgraduation_inlist_array,
			'greater_than'   => 0,
			'sanitize_input' => true,
		) );

		$select_professional_qualification = $validate->set_rules( array (
			'name'           => 'select_professional_qualification',
			'placeholder'    => 'Professional Qualification',
			'type'           => 'custom',
			'trim'           => true,
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'numeric'        => true,
			'max_item_limit' => 6,
			'in_list'        => $select_professional_qualification_inlist_array,
			'greater_than'   => 0,
			'sanitize_input' => true,
		) );

		$txt_other_qualification = $validate->set_rules( array (
			'name'           => 'txt_other_qualification',
			'placeholder'    => 'Other Qualification',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 2,
			'max_length'     => 200,
			'sanitize_input' => true,
		) );

		$select_companies_served = $validate->set_rules( array (
			'name'           => 'select_companies_served',
			'placeholder'    => 'Companies Served',
			'type'           => 'custom',
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'trim'           => true,
			'required'       => true,
			'min_length'     => 2,
			'max_length'     => 40,
			'max_item_limit' => 6,
			'min_item_limit' => 1,
			'sanitize_input' => true,
		) );

		$txt_heading_of_profile = $validate->set_rules( array (
			'name'           => 'txt_heading_of_profile',
			'placeholder'    => 'Heading of Profile',
			'type'           => 'custom',
			'trim'           => true,
			'required'       => true,
			'min_length'     => 2,
			'max_length'     => 140,
			'sanitize_input' => true,
		) );

		$profile_pic_exist = file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/images/' . $trainer_user_id . '/profile.jpg' ) ? true : false;
		if ( ! $profile_pic_exist ) {

			$return_message['error_type'] = 'file_upload_profile_picture';
			$return_message['message']    = 'Please upload the profile picture.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $trainer_details[0]->cv_path ) ) {
			$return_message['error_type'] = 'file_upload_resume';
			$return_message['message']    = 'Please upload your resume.';

			echo json_encode( $return_message );
			die();
		} elseif ( ! file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/resume/' . $trainer_details[0]->cv_path ) ? true : false ) {
			$return_message['error_type'] = 'file_upload_resume';
			$return_message['message']    = 'Please upload your resume.';

			echo json_encode( $return_message );
			die();
		}

		if ( empty( $trainer_details[0]->sample_course_material_path ) ) {
			$return_message['error_type'] = 'file_upload_sample_course_material';
			$return_message['message']    = 'Please upload the sample course material.';

			echo json_encode( $return_message );
			die();
		} elseif ( ! file_exists( PEDAGOGE_PLUGIN_DIR . 'storage/uploads/sample_course_material/' . $trainer_details[0]->sample_course_material_path ) ? true : false ) {
			$return_message['error_type'] = 'file_upload_sample_course_material';
			$return_message['message']    = 'Please upload the sample course material.';

			echo json_encode( $return_message );
			die();
		}

		$select_user_video = $validate->set_rules( array (
			'name'           => 'select_user_video',
			'placeholder'    => 'Video URL(s)',
			'type'           => 'custom',
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'trim'           => true,
			'max_item_limit' => 3,
			'valid_url'      => true,
			'sanitize_input' => true,
		) );

		$select_technical_domain = $validate->set_rules( array (
			'name'           => 'select_technical_domain',
			'placeholder'    => 'Training domain - Technical',
			'type'           => 'custom',
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'in_list'        => $select_technical_domain_array,
		) );

		$select_other_technical_domain = $validate->set_rules( array (
			'name'           => 'select_other_technical_domain',
			'placeholder'    => 'Others (Training domain - Technical)',
			'type'           => 'custom',
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'trim'           => true,
			'min_length'     => 2,
			'max_length'     => 40,
			'max_item_limit' => 3,
			'sanitize_input' => true,
		) );

		$select_nontechnical_domain = $validate->set_rules( array (
			'name'           => 'select_nontechnical_domain',
			'placeholder'    => 'Training domain - Non Technical',
			'type'           => 'custom',
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'in_list'        => $select_nontechnical_domain_array,
		) );

		$select_other_nontechnical_domain = $validate->set_rules( array (
			'name'           => 'select_other_nontechnical_domain',
			'placeholder'    => 'Others (Training domain - Non Technical)',
			'type'           => 'custom',
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'trim'           => true,
			'min_length'     => 2,
			'max_length'     => 40,
			'max_item_limit' => 3,
			'sanitize_input' => true,
		) );

		/*Select at least 1 Training domains field(s)*/
		$validate->group_set_rules( array (
			'group_placeholder' => 'Training domains',
			'group_name'        => 'training_domains',
			'items'             => array (
				'select_technical_domain',
				'select_other_technical_domain',
				'select_nontechnical_domain',
				'select_other_nontechnical_domain'
			),
			'min_item_selected' => '1',
		) );

		$select_average_training_experience = $validate->set_rules( array (
			'name'           => 'select_average_training_experience',
			'placeholder'    => 'Training Experience',
			'type'           => 'custom',
			'trim'           => true,
			'numeric'        => true,
			'required'       => true,
			'greater_than'   => 0,
			'in_list'        => $select_average_training_experience_inlist_array,
			'sanitize_input' => true,
		) );

		$select_training_frequency = $validate->set_rules( array (
			'name'           => 'select_training_frequency',
			'placeholder'    => 'Training frequency',
			'type'           => 'custom',
			'trim'           => true,
			'required'       => true,
			'in_list'        => array ( 'fulltime' => "Full Time", 'parttime' => "Part Time" ),
			'sanitize_input' => true,
		) );

		$select_training_level = $validate->set_rules( array (
			'name'           => 'select_training_level',
			'placeholder'    => 'Hierarchy of professionals trained',
			'type'           => 'custom',
			'trim'           => true,
			'required'       => true,
			'in_list'        => array (
				'beginner'     => "Beginner",
				'intermediate' => "Intermediate",
				'advanced'     => "Advanced",
			),
			'sanitize_input' => true,
		) );

		$select_keywords = $validate->set_rules( array (
			'name'           => 'select_keywords',
			'placeholder'    => 'Keywords to describe your training',
			'type'           => 'custom',
			'trim'           => true,
			'required'       => true,
			'allow_array'    => true,
			'is_array'       => true,
			'unique_items'   => true,
			'min_length'     => 2,
			'max_length'     => 20,
			'max_item_limit' => 6,
			'sanitize_input' => true,
		) );

		$txt_training_experiences_of_profile = $validate->set_rules( array (
			'name'           => 'txt_training_experiences_of_profile',
			'placeholder'    => 'Past training experiences',
			'type'           => 'custom',
			'trim'           => true,
			'required'       => true,
			'min_length'     => 2,
			'max_length'     => 500,
			'sanitize_input' => true,
		) );

		$trainer_references_ClientReference_1_contact_no = $validate->set_rules( array (
			'name'           => 'trainer_references_ClientReference_1_contact_no',
			'placeholder'    => 'Client Reference - 1 Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$trainer_references_ClientReference_1_contact_name = $validate->set_rules( array (
			'name'                 => 'trainer_references_ClientReference_1_contact_name',
			'placeholder'          => 'Client Reference - 1 Contact Name',
			'type'                 => 'custom',
			'alpha_numeric_spaces' => true,
			'trim'                 => true,
			'min_length'           => 2,
			'max_length'           => 25,
			'sanitize_input'       => true,
		) );

		$trainer_references_ClientReference_2_contact_no = $validate->set_rules( array (
			'name'           => 'trainer_references_ClientReference_2_contact_no',
			'placeholder'    => 'Client Reference - 2 Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$trainer_references_ClientReference_2_contact_name = $validate->set_rules( array (
			'name'                 => 'trainer_references_ClientReference_2_contact_name',
			'placeholder'          => 'Client Reference - 2 Contact Name',
			'type'                 => 'custom',
			'alpha_numeric_spaces' => true,
			'trim'                 => true,
			'min_length'           => 2,
			'max_length'           => 25,
			'sanitize_input'       => true,
		) );

		$trainer_references_PeerColleagueReference_1_contact_no = $validate->set_rules( array (
			'name'           => 'trainer_references_PeerColleagueReference_1_contact_no',
			'placeholder'    => 'Peer/Colleague Reference - 1 Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$trainer_references_PeerColleagueReference_1_contact_name = $validate->set_rules( array (
			'name'                 => 'trainer_references_PeerColleagueReference_1_contact_name',
			'placeholder'          => 'Peer/Colleague Reference - 1 Contact Name',
			'type'                 => 'custom',
			'alpha_numeric_spaces' => true,
			'trim'                 => true,
			'min_length'           => 2,
			'max_length'           => 25,
			'sanitize_input'       => true,
		) );

		$trainer_references_PeerColleagueReference_2_contact_no = $validate->set_rules( array (
			'name'           => 'trainer_references_PeerColleagueReference_2_contact_no',
			'placeholder'    => 'Peer/Colleague Reference - 2 Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$trainer_references_PeerColleagueReference_2_contact_name = $validate->set_rules( array (
			'name'                 => 'trainer_references_PeerColleagueReference_2_contact_name',
			'placeholder'          => 'Peer/Colleague Reference - 2 Contact Name',
			'type'                 => 'custom',
			'alpha_numeric_spaces' => true,
			'trim'                 => true,
			'min_length'           => 2,
			'max_length'           => 25,
			'sanitize_input'       => true,
		) );

		$trainer_references_PersonalReference_1_contact_no = $validate->set_rules( array (
			'name'           => 'trainer_references_PersonalReference_1_contact_no',
			'placeholder'    => 'Personal Reference 1 Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$trainer_references_PersonalReference_1_contact_name = $validate->set_rules( array (
			'name'                 => 'trainer_references_PersonalReference_1_contact_name',
			'placeholder'          => 'Personal Reference 1 Contact Name',
			'type'                 => 'custom',
			'alpha_numeric_spaces' => true,
			'trim'                 => true,
			'min_length'           => 2,
			'max_length'           => 25,
			'sanitize_input'       => true,
		) );

		$trainer_references_PersonalReference_2_contact_no = $validate->set_rules( array (
			'name'           => 'trainer_references_PersonalReference_2_contact_no',
			'placeholder'    => 'Personal Reference 2 Contact No.',
			'type'           => 'custom',
			'trim'           => true,
			'min_length'     => 10,
			'max_length'     => 15,
			'valid_phone'    => true,
			'sanitize_input' => true,
		) );

		$trainer_references_PersonalReference_2_contact_name = $validate->set_rules( array (
			'name'                 => 'trainer_references_PersonalReference_2_contact_name',
			'placeholder'          => 'Personal Reference 2 Contact Name',
			'type'                 => 'custom',
			'alpha_numeric_spaces' => true,
			'trim'                 => true,
			'min_length'           => 2,
			'max_length'           => 25,
			'sanitize_input'       => true,
		) );

		if ( $validate->run() == false ) {
			$return_message['error_type'] = $validate->error_field_id;
			$return_message['message']    = $validate->show_error();

			echo json_encode( $return_message );
			die();
		}

		/*Validation ends*/

		/**
		 * Create Profile Slugs
		 */

		$raw_slug     = $users_email . $trainer_user_id;
		$refined_slug = md5( $raw_slug );

		/**
		 *update wp_users table
		 */

		update_user_meta( $trainer_user_id, 'first_name', $txt_trainer_first_name );
		update_user_meta( $trainer_user_id, 'last_name', $txt_trainer_last_name );
		wp_update_user( array (
			'ID'         => $trainer_user_id,
			'user_email' => $txt_email_id,
		) );

		$user_model = new ModelUserInfo();
		$user_data  = array (
			'mobile_no'              => $txt_mobile_no,
			'alternative_contact_no' => $txt_alternate_contact_no,
			'date_of_birth'          => $date_of_birth,
			'gender'                 => $select_gender,
			'current_address'        => $txt_address_of_correspondance,
		);
		$user_model->update( $user_data, $trainer_user_info_id, array ( 'mobile_no' => $txt_mobile_no ) );

		/* start qualification */
		$qualification_array = array ();
		if ( is_numeric( $select_graduation ) ) {
			$qualification_array[] = $select_graduation;
		}

		if ( is_numeric( $select_postgraduation ) ) {
			$qualification_array[] = $select_postgraduation;
		}

		if ( isset( $select_professional_qualification ) ) {
			foreach ( $select_professional_qualification as $value ) {
				if ( is_numeric( $value ) ) {
					$qualification_array[] = $value;
				}
			}
		}
		$wpdb->get_results( "delete from pdg_trainer_qualification where trainer_id = $trainer_id" );
		foreach ( $qualification_array as $qualification ) {
			if ( is_numeric( $qualification ) ) {
				$insert_array = array (
					'trainer_id'       => $trainer_id,
					'qualification_id' => $qualification
				);
				$wpdb->insert( 'pdg_trainer_qualification', $insert_array, array ( '%d', '%d' ) );
			}
		}
		/**end qualification**/

		/*Start Companies served*/
		$new_companies_served_ids     = array ();
		$companies_served_list_string = implode( "', '", $select_companies_served );
		$companies_served             = $wpdb->get_results( "SELECT company_id, company_name
													FROM
														pdg_companies_logos
													WHERE
														deleted = 'no'
													AND company_name IN ('" . $companies_served_list_string . "')
													ORDER BY
														FIELD(
															company_name, '" . $companies_served_list_string . "');
													", ARRAY_A );
		$wpdb->get_results( "delete from pdg_trainer_company_served where trainer_id =" . $trainer_id );
		foreach ( $companies_served as $companies_served_item ) {
			$company_served_name = $companies_served_item['company_name'];
			$company_served_id   = $companies_served_item['company_id'];
			if ( in_array( strtolower( $company_served_name ), array_map( 'strtolower', $select_companies_served ) ) ) {
				$new_companies_served_ids [ $company_served_id ] = $company_served_name;

				$insert_array = array (
					'trainer_id' => $trainer_id,
					'company_id' => $company_served_id
				);
				$wpdb->insert( 'pdg_trainer_company_served', $insert_array, array ( '%d', '%d' ) );
			}

		}
		$old_companies_served_names = array_values( array_diff( $select_companies_served, $new_companies_served_ids ) );
		if ( is_array( $old_companies_served_names ) && ! empty( $old_companies_served_names ) ) {
			foreach ( $old_companies_served_names as $old_companies_served_item ) {
				$insert_array = array (
					'company_name' => $old_companies_served_item
				);
				$wpdb->insert( 'pdg_companies_logos', $insert_array, array ( '%s' ) );
				$insert_array = array (
					'trainer_id' => $trainer_id,
					'company_id' => $wpdb->insert_id
				);
				$wpdb->insert( 'pdg_trainer_company_served', $insert_array, array ( '%d', '%d' ) );
			}
		}

		/*End Companies served*/

		/*Start user videos*/
		$wpdb->get_results( "delete from pdg_user_video where user_id =" . $trainer_user_id );
		foreach ( $select_user_video as $select_user_video_item ) {
			$insert_array = array (
				'user_id'   => $trainer_user_id,
				'video_url' => $select_user_video_item,
			);
			$wpdb->insert( 'pdg_user_video', $insert_array, array ( '%d', '%s', ) );
		}
		/*End training domain*/

		/* start training domain */
		$training_domain_array = array_merge( (array) $select_technical_domain, (array) $select_nontechnical_domain );
		$wpdb->get_results( "delete from pdg_trainer_domain_list where trainer_id = $trainer_id" );
		foreach ( $training_domain_array as $training_domain_item ) {
			if ( is_numeric( $training_domain_item ) ) {
				$insert_array = array (
					'trainer_id' => $trainer_id,
					'domain_id'  => $training_domain_item
				);
				$wpdb->insert( 'pdg_trainer_domain_list', $insert_array, array ( '%d', '%d' ) );
			}
		}
		/**end training domain**/

		/*Starts Reference*/
		$wpdb->get_results( "delete from pdg_trainer_references where trainer_id = $trainer_id" );
		$insert_array = array (
			'trainer_id'        => $trainer_id,
			'reference_type_id' => 1,
			'contact_no'        => $trainer_references_ClientReference_1_contact_no,
			'contact_name'      => $trainer_references_ClientReference_1_contact_name,
		);
		$wpdb->insert( 'pdg_trainer_references', $insert_array, array ( '%d', '%d', '%s', '%s' ) );


		$insert_array = array (
			'trainer_id'        => $trainer_id,
			'reference_type_id' => 2,
			'contact_no'        => $trainer_references_ClientReference_2_contact_no,
			'contact_name'      => $trainer_references_ClientReference_2_contact_name,
		);
		$wpdb->insert( 'pdg_trainer_references', $insert_array, array ( '%d', '%d', '%s', '%s' ) );

		$insert_array = array (
			'trainer_id'        => $trainer_id,
			'reference_type_id' => 3,
			'contact_no'        => $trainer_references_PeerColleagueReference_1_contact_no,
			'contact_name'      => $trainer_references_PeerColleagueReference_1_contact_name,
		);
		$wpdb->insert( 'pdg_trainer_references', $insert_array, array ( '%d', '%d', '%s', '%s' ) );

		$insert_array = array (
			'trainer_id'        => $trainer_id,
			'reference_type_id' => 4,
			'contact_no'        => $trainer_references_PeerColleagueReference_2_contact_no,
			'contact_name'      => $trainer_references_PeerColleagueReference_2_contact_name,
		);
		$wpdb->insert( 'pdg_trainer_references', $insert_array, array ( '%d', '%d', '%s', '%s' ) );

		$insert_array = array (
			'trainer_id'        => $trainer_id,
			'reference_type_id' => 5,
			'contact_no'        => $trainer_references_PersonalReference_1_contact_no,
			'contact_name'      => $trainer_references_PersonalReference_1_contact_name,
		);
		$wpdb->insert( 'pdg_trainer_references', $insert_array, array ( '%d', '%d', '%s', '%s' ) );

		$insert_array = array (
			'trainer_id'        => $trainer_id,
			'reference_type_id' => 6,
			'contact_no'        => $trainer_references_PersonalReference_2_contact_no,
			'contact_name'      => $trainer_references_PersonalReference_2_contact_name,
		);
		$wpdb->insert( 'pdg_trainer_references', $insert_array, array ( '%d', '%d', '%s', '%s' ) );
		/*Ends Reference*/

		/**
		 * Update pdg_trainer table
		 * */
		$user_model = new ModelTrainer();
		$user_data  = array (
			'profile_slug'               => $refined_slug,
			'other_qualification'        => $txt_other_qualification,
			'trainer_xp_comments'        => $txt_training_experiences_of_profile,
			'trainer_heading'            => $txt_heading_of_profile,
			'other_domains_technical'    => implode( ",", $select_other_technical_domain ),
			'other_domains_nontechnical' => implode( ",", $select_other_nontechnical_domain ),
			'total_xp_id'                => $select_average_training_experience,
			'trainer_frequency'          => $select_training_frequency,
			'training_level'             => $select_training_level,
			'trainer_keywords'           => implode( ",", $select_keywords ),
		);

		$user_model->update( $user_data, $trainer_id );

		$return_message['error']   = false;
		$return_message['message'] = 'Your profile was successfully updated!';
		echo json_encode( $return_message );
		die();

	}

	public static function fn_save_trainer_profile_picture_ajax() {
		global $wpdb;
		$trainer_data   = array ();
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => null
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security_profile_pic';
			$return_message['message']    = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( isset( $_POST['trainer_intro_data'] ) ) {
			$trainer_data = json_decode( stripslashes( $_POST['trainer_intro_data'] ), true );
		} else {
			$return_message['error_type'] = 'trainer_intro_data_profile_pic';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$validate        = new ValidatorPedagoge( $trainer_data );
		$trainer_user_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_insti_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'numeric'        => true,
			'trim'           => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_user_info_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_info_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$check_trainer_id = $wpdb->get_var( "SELECT count(*) FROM `pdg_trainer` WHERE trainer_id = " . $trainer_id . " && user_id = " . $trainer_user_id );
		if ( ! $check_trainer_id ) {
			$return_message['error_type'] = 'trainer_id_unmatched_profile_picture';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$trainer_edit_secret_key = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_profile_edit_secret_key',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user     = get_user_by( 'id', $trainer_user_id );
		$users_email = $wp_user->user_email;
		require_once( ABSPATH . 'wp-includes/class-phpass.php' );
		$wp_hasher = new PasswordHash( 8, true );
		if ( ! $wp_hasher->CheckPassword( $users_email, $trainer_edit_secret_key ) ) {
			$return_message['error_type'] = 'secret_unmatched_profile_pic';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$file_upload_profile_picture = $validate->set_rules( array (
			'name'           => 'file_upload_profile_picture',
			'placeholder'    => 'Profile Picture',
			'type'           => 'files',
			'allow_array'    => true,
			'is_array'       => true,
			'required'       => true,
			'file_extension' => array ( 'jpg', 'jpeg', 'png', 'bmp' ),
			'file_max_size'  => '2000000',
		) );

		if ( $validate->run() == false ) {
			$return_message['error_type'] = $validate->error_field_id;
			$return_message['message']    = $validate->show_error();

			echo json_encode( $return_message );
			die();
		}

		$profile_dir = PEDAGOGE_PLUGIN_DIR . 'storage/uploads/images/' . $trainer_user_id;

		if ( ! is_dir( $profile_dir ) ) {
			mkdir( $profile_dir );
		}
		if ( $file_upload_profile_picture['error'] === UPLOAD_ERR_OK ) {
			$profile_image = $profile_dir . '/profile.jpg';

			if ( file_exists( $profile_image ) ) {
				unlink( $profile_image );
			}
			if ( ! move_uploaded_file( $file_upload_profile_picture["tmp_name"], $profile_image ) ) {
				$return_message['error_type'] = 'file_upload_error_profile_picture';
				$return_message['message']    = 'File upload error! Please try again.';

				echo json_encode( $return_message );
				die();
			}
		}
		$url                        = PEDAGOGE_PLUGIN_URL . '/storage/uploads/images/' . $trainer_user_id . '/profile.jpg';
		$return_message['error']    = false;
		$return_message['file_url'] = $url;
		$return_message['message']  = 'Profile picture uploaded successfully!';

		echo json_encode( $return_message );
		die();
	}

	public static function fn_save_trainer_resume_ajax() {
		global $wpdb;
		$trainer_data   = array ();
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => null
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security_resume';
			$return_message['message']    = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( isset( $_POST['trainer_intro_data'] ) ) {
			$trainer_data = json_decode( stripslashes( $_POST['trainer_intro_data'] ), true );
		} else {
			$return_message['error_type'] = 'trainer_intro_data_resume';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$validate        = new ValidatorPedagoge( $trainer_data );
		$trainer_user_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_insti_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'numeric'        => true,
			'trim'           => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_user_info_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_info_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$check_trainer_id = $wpdb->get_var( "SELECT count(*) FROM `pdg_trainer` WHERE trainer_id = " . $trainer_id . " && user_id = " . $trainer_user_id );
		if ( ! $check_trainer_id ) {
			$return_message['error_type'] = 'trainer_id_unmatched_resume';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$trainer_edit_secret_key = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_profile_edit_secret_key',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user     = get_user_by( 'id', $trainer_user_id );
		$users_email = $wp_user->user_email;
		require_once( ABSPATH . 'wp-includes/class-phpass.php' );
		$wp_hasher = new PasswordHash( 8, true );
		if ( ! $wp_hasher->CheckPassword( $users_email, $trainer_edit_secret_key ) ) {
			$return_message['error_type'] = 'secret_unmatched_resume';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$file_upload_resume = $validate->set_rules( array (
			'name'           => 'file_upload_resume',
			'placeholder'    => 'Resume',
			'type'           => 'files',
			'allow_array'    => true,
			'is_array'       => true,
			'required'       => true,
			'file_extension' => array ( 'doc', 'docx', 'pdf' ),
			'file_max_size'  => '10000000',
		) );

		if ( $validate->run() == false ) {
			$return_message['error_type'] = $validate->error_field_id;
			$return_message['message']    = $validate->show_error();

			echo json_encode( $return_message );
			die();
		}

		$raw_slug     = $users_email . $trainer_user_id;
		$refined_slug = md5( $raw_slug );

		$resume_dir = PEDAGOGE_PLUGIN_DIR . 'storage/uploads/resume/' . $refined_slug;

		if ( ! is_dir( $resume_dir ) ) {
			mkdir( $resume_dir );
			$file = fopen( $resume_dir . "/index.php", "a" );
			fclose( $file );
		}
		$file_path = '';
		if ( $file_upload_resume['error'] === UPLOAD_ERR_OK ) {
			$file_path   = "Resume-Trainer-" . $trainer_id . "-" . substr( sha1( $refined_slug . $trainer_id ), 0, 5 ) . "." . end( explode( ".", $file_upload_resume['name'] ) );
			$resume_path = $resume_dir . "/" . $file_path;
			if ( file_exists( $resume_path ) ) {
				unlink( $resume_path );
			}
			if ( ! move_uploaded_file( $file_upload_resume["tmp_name"], $resume_path ) ) {
				$return_message['error_type'] = 'file_upload_error_resume';
				$return_message['message']    = 'File upload error. Please try again.';

				echo json_encode( $return_message );
				die();
			}
		}

		$user_model = new ModelTrainer();
		$user_data  = array (
			'cv_path' => $refined_slug . "/" . $file_path,
		);
		$user_model->update( $user_data, $trainer_id );
		$url = PEDAGOGE_PLUGIN_URL . '/storage/uploads/resume/' . $refined_slug . '/' . $file_path;

		$return_message['error']    = false;
		$return_message['file_url'] = $url;
		$return_message['message']  = 'Resume uploaded successfully!';
		echo json_encode( $return_message );
		die();
	}

	public static function fn_save_trainer_sample_course_material_ajax() {
		global $wpdb;
		$trainer_data   = array ();
		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => '',
			'data'       => null
		);

		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security_sample_course_material';
			$return_message['message']    = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		if ( isset( $_POST['trainer_intro_data'] ) ) {
			$trainer_data = json_decode( stripslashes( $_POST['trainer_intro_data'] ), true );
		} else {
			$return_message['error_type'] = 'trainer_intro_data_sample_course_material';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$validate        = new ValidatorPedagoge( $trainer_data );
		$trainer_user_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_insti_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'numeric'        => true,
			'trim'           => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$trainer_user_info_id = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_user_info_var',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'numeric'        => true,
			'greater_than'   => 0,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		$check_trainer_id = $wpdb->get_var( "SELECT count(*) FROM `pdg_trainer` WHERE trainer_id = " . $trainer_id . " && user_id = " . $trainer_user_id );
		if ( ! $check_trainer_id ) {
			$return_message['error_type'] = 'trainer_id_unmatched_sample_course_materialture';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$trainer_edit_secret_key = $validate->set_rules( array (
			'name'           => 'hidden_dynamic_profile_edit_secret_key',
			'placeholder'    => 'Form data',
			'type'           => 'custom',
			'required'       => true,
			'trim'           => true,
			'sanitize_input' => true,
			'custom_error'   => 'Invalid form data! Please try again.'
		) );

		/**
		 * Check if the secret key matches with user's email address
		 * 1. load user's email address via user id
		 */
		$wp_user     = get_user_by( 'id', $trainer_user_id );
		$users_email = $wp_user->user_email;
		require_once( ABSPATH . 'wp-includes/class-phpass.php' );
		$wp_hasher = new PasswordHash( 8, true );
		if ( ! $wp_hasher->CheckPassword( $users_email, $trainer_edit_secret_key ) ) {
			$return_message['error_type'] = 'secret_unmatched_sample_course_material';
			$return_message['message']    = 'Invalid form data! Please try again.';

			echo json_encode( $return_message );
			die();
		}

		$file_upload_sample_course_material = $validate->set_rules( array (
			'name'           => 'file_upload_sample_course_material',
			'placeholder'    => 'Sample course material',
			'type'           => 'files',
			'allow_array'    => true,
			'is_array'       => true,
			'required'       => true,
			'file_extension' => array ( 'ppt', 'pptx', 'doc', 'docx', 'pdf' ),
			'file_max_size'  => '15000000',
		) );

		if ( $validate->run() == false ) {
			$return_message['error_type'] = $validate->error_field_id;
			$return_message['message']    = $validate->show_error();

			echo json_encode( $return_message );
			die();
		}

		$raw_slug     = $users_email . $trainer_user_id;
		$refined_slug = md5( $raw_slug );

		$sample_course_material_dir = PEDAGOGE_PLUGIN_DIR . 'storage/uploads/sample_course_material/' . $refined_slug;

		if ( ! is_dir( $sample_course_material_dir ) ) {
			mkdir( $sample_course_material_dir );
			$file = fopen( $sample_course_material_dir . "/index.php", "a" );
			fclose( $file );
		}

		$file_path = '';
		if ( $file_upload_sample_course_material['error'] === UPLOAD_ERR_OK ) {
			$file_path                   = "Sample_Course_Material-Trainer-" . $trainer_id . "-" . substr( sha1( $refined_slug . $trainer_id ), 0, 5 ) . "." . end( explode( ".", $file_upload_sample_course_material['name'] ) );
			$sample_course_material_path = $sample_course_material_dir . "/" . $file_path;
			if ( file_exists( $sample_course_material_path ) ) {
				unlink( $sample_course_material_path );
			}
			if ( ! move_uploaded_file( $file_upload_sample_course_material["tmp_name"], $sample_course_material_path ) ) {
				$return_message['error_type'] = 'file_upload_error_sample_course_material';
				$return_message['message']    = 'File upload error. Please try again.';

				echo json_encode( $return_message );
				die();
			}
		}
		$user_model = new ModelTrainer();
		$user_data  = array (
			'sample_course_material_path' => $refined_slug . "/" . $file_path,
		);

		$user_model->update( $user_data, $trainer_id );

		$url                        = PEDAGOGE_PLUGIN_URL . '/storage/uploads/sample_course_material/' . $refined_slug . '/' . $file_path;
		$return_message['error']    = false;
		$return_message['file_url'] = $url;
		$return_message['message']  = 'Sample course material uploaded successfully!';

		echo json_encode( $return_message );
		die();
	}


	public static function fn_random_string( $length ) {
		$key  = '';
		$keys = array_merge( range( 0, 9 ), range( 'a', 'z' ) );

		for ( $i = 0; $i < $length; $i ++ ) {
			$key .= $keys[ array_rand( $keys ) ];
		}

		return $key;
	}

	public static function img_crop( $src, $dst, $data ) {
		if ( ! empty( $src ) && ! empty( $dst ) && ! empty( $data ) ) {

			$src_img = imagecreatefromjpeg( $src );

			if ( ! $src_img ) {
				return false;
			}

			$size   = getimagesize( $src );
			$size_w = $size[0]; // natural width
			$size_h = $size[1]; // natural height

			$src_img_w = $size_w;
			$src_img_h = $size_h;

			$degrees = $data->rotate;

			// Rotate the source image
			if ( is_numeric( $degrees ) && $degrees != 0 ) {
				// PHP's degrees is opposite to CSS's degrees
				$new_img = imagerotate( $src_img, - $degrees, imagecolorallocatealpha( $src_img, 0, 0, 0, 127 ) );

				imagedestroy( $src_img );
				$src_img = $new_img;

				$deg = abs( $degrees ) % 180;
				$arc = ( $deg > 90 ? ( 180 - $deg ) : $deg ) * M_PI / 180;

				$src_img_w = $size_w * cos( $arc ) + $size_h * sin( $arc );
				$src_img_h = $size_w * sin( $arc ) + $size_h * cos( $arc );

				// Fix rotated image miss 1px issue when degrees < 0
				$src_img_w -= 1;
				$src_img_h -= 1;
			}

			$tmp_img_w = $data->width;
			$tmp_img_h = $data->height;
			$dst_img_w = 220;
			$dst_img_h = 220;

			$src_x = $data->x;
			$src_y = $data->y;

			if ( $src_x <= - $tmp_img_w || $src_x > $src_img_w ) {
				$src_x = $src_w = $dst_x = $dst_w = 0;
			} else if ( $src_x <= 0 ) {
				$dst_x = - $src_x;
				$src_x = 0;
				$src_w = $dst_w = min( $src_img_w, $tmp_img_w + $src_x );
			} else if ( $src_x <= $src_img_w ) {
				$dst_x = 0;
				$src_w = $dst_w = min( $tmp_img_w, $src_img_w - $src_x );
			}

			if ( $src_w <= 0 || $src_y <= - $tmp_img_h || $src_y > $src_img_h ) {
				$src_y = $src_h = $dst_y = $dst_h = 0;
			} else if ( $src_y <= 0 ) {
				$dst_y = - $src_y;
				$src_y = 0;
				$src_h = $dst_h = min( $src_img_h, $tmp_img_h + $src_y );
			} else if ( $src_y <= $src_img_h ) {
				$dst_y = 0;
				$src_h = $dst_h = min( $tmp_img_h, $src_img_h - $src_y );
			}

			// Scale to destination position and size
			$ratio = $tmp_img_w / $dst_img_w;
			$dst_x /= $ratio;
			$dst_y /= $ratio;
			$dst_w /= $ratio;
			$dst_h /= $ratio;

			$dst_img = imagecreatetruecolor( $dst_img_w, $dst_img_h );

			// Add transparent background to destination image
			imagefill( $dst_img, 0, 0, imagecolorallocatealpha( $dst_img, 0, 0, 0, 127 ) );
			imagesavealpha( $dst_img, true );

			$result = imagecopyresampled( $dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h );

			if ( $result ) {
				if ( ! imagejpeg( $dst_img, $dst ) ) {
					//$this -> msg = "Failed to save the cropped image file";
					imagedestroy( $src_img );
					imagedestroy( $dst_img );

					return false;
				}
			} else {
				imagedestroy( $src_img );
				imagedestroy( $dst_img );

				return false;
				//$this -> msg = "Failed to crop the image file";
			}

			imagedestroy( $src_img );
			imagedestroy( $dst_img );

			return true;
		} else {
			return false;
		}
	}

	private function callback_array_unique_ci( $param ) {
		return array_intersect_key( $param, array_unique( array_map( "strtolower", $param ) ) );
	}
}