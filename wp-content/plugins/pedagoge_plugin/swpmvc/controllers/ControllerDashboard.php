<?php 
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Controller Class to generate Dashboard pages for the route /dashboad - Pedagoge Control Panel for Admin. Not to be confused with Wordpress Admin Control Panel.
 * 
 * @author Niraj
 */
class ControllerDashboard extends ControllerMaster implements ControllerMasterInterface {
	
	public $menu_list = '';
	public function __construct() {
		
	}


//////////////////code by sudipta////////////////////

public function emailsubmit(){
	echo "<pre>"; print_r($_POST); echo "</pre>";
	$image_file = $_POST['imageFile'];
	echo "<pre>"; print_r($_FILES); echo "</pre>";
	/*echo  $val=$_POST['val'];
	echo  $email_subject=$_POST['email_subject'];
	echo  $cc=$_POST['cc'];
	echo  $bcc=$_POST['bcc'];
	//echo  $email_subject=$_POST['email_subject'];
	echo  $templates=$_POST['templates'];
	echo  $jodit_editor=$_POST['jodit_editor'];
	echo  $ename=$_POST['ename'];
	echo 'success';*/
	die();
}

public function fileupload(){
$file = $_FILES['file_data'];
print_r($file);	
	die();	
	}



public function emailtempfetch(){
	 $val=$_POST['val'];
	$query_model = new ModelQuery();
	$results = $query_model->get_emailtemplates($val);
	foreach($results AS $res){
	 echo $res->text; 
	 	}
	
	//$int_text='Interested';	
	//echo 'success';
	die();	
	}
//////////////////////code by sudipta///////////////////
	
	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Admin Control Panel : Dashboard';
		$this->fn_load_scripts();
		$this->menu_list = array(
			'dashboard' => '',
			'queries' => '',
			'view_query' => '',
			'view_log' => '',
			'teacher' => '',
			'institute' => '',
			'workshop' => '',
			//'student' => '',
			//'guardian' => '',
			'reviews' => '',
			'course' => '',
			//'entities' => '',
			//'users' => '',
			'locality' => '',
			'impex' => '',
			'reports' => '',
			'referral' => '',
			'seo' => '',
			'settings' => '',
			'email' => '',
			'manage_teacher' => '',
			'manage_institute' => '',
		);
		$this->fn_register_common_variables();
	}
	
	private function fn_load_scripts() {
		$control_panel_version = '0.3';
		
		$this->common_css = array();	
		$this->css_assets = array();
		$this->app_css = array();
		$this->google_fonts = array();
		$this->header_js = array();
		$this->footer_js = array();
		$this->footer_common_js = array();
		$this->app_js = array();
		
		$this->common_css['bootstrap'] = BOWER_ROOT_URL.'/bootstrap/dist/css/bootstrap.min.css?ver='.$this->fixed_version;
		$this->common_css['plugins'] = PEDAGOGE_CP_ASSETS_URL.'/css/dt_removed_plugins.css?ver='.$this->fixed_version;
		$this->common_css['main'] = PEDAGOGE_CP_ASSETS_URL.'/css/main.css?ver='.$this->fixed_version;
		$this->common_css['themes'] = PEDAGOGE_CP_ASSETS_URL.'/css/themes.css?ver='.$this->fixed_version;
		$this->header_js['jquery'] = PEDAGOGE_CP_ASSETS_URL.'/js/vendor/jquery-1.12.0.min.js?ver='.$this->fixed_version;
		$this->header_js['respond'] = PEDAGOGE_CP_ASSETS_URL.'/js/vendor/modernizr-respond.min.js?ver='.$this->fixed_version;
		//$this->footer_common_js['jquery'] = PEDAGOGE_CP_ASSETS_URL.'/js/vendor/jquery-1.12.0.min.js?ver='.$this->fixed_version;
		$this->footer_common_js['bootstrap'] = PEDAGOGE_CP_ASSETS_URL.'/js/vendor/bootstrap.min.js?ver='.$this->fixed_version;
		$page_url = @$_GET['page'];
		if($page_url!='queries'){
		$this->footer_common_js['uniform'] = BOWER_ROOT_URL."/jquery.uniform/jquery.uniform.min.js?ver=".$this->fixed_version;
		}
		$this->footer_js['plugins'] = PEDAGOGE_CP_ASSETS_URL.'/js/dt_removed_plugins.js?ver='.$this->fixed_version;
		$this->footer_js['appjs'] = PEDAGOGE_CP_ASSETS_URL.'/js/app.js?ver='.$this->fixed_version;
		if($page_url!='queries'){
		$this->app_js['pedagoge_common'] = PEDAGOGE_ASSETS_URL.'/js/pedagoge_common.js?ver='.$control_panel_version;
		$this->app_js['cp_common'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_common.js?ver='.$control_panel_version;
		}
		 		
	}
	
	public function fn_get_header() {
		$this->fn_load_cp_page_content();
		return $this->load_view('controlpanel/header');
	}
	
	public function fn_get_content() {
		
		$is_current_user_has_cp_acess = current_user_can('manage_options') || current_user_can('pdg_cap_cp_access'); 
		if(!$is_current_user_has_cp_acess) {
			return $this->load_view('controlpanel/unauthorized_acess');
		}				
		return $this->fn_load_cp_page_content(true);
	}
	
	public function fn_get_footer() {
		return $this->load_view('controlpanel/footer');
	}

	public function fn_load_cp_page_content($return_content=false) {
		$page_url = '';
		$version = '0.6';
		$str_content = '';
		$page_permission = FALSE;
		if(isset($_GET['page'])) {
			
			$page_url = $_GET['page'];
			
			if(array_key_exists($page_url, $this->menu_list)) {
				
				$this->menu_list[$page_url]=' active ';
				$page_permission = $this->fn_cp_access_permission($page_url);
				switch($page_url) {
					case 'queries':
						$this->title = 'Admin Control Panel : Queries';
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;	
						$this->css_assets['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery-ui-1.9.2.custom.min.css';
						$this->css_assets['alertify_core_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.core.css';
						$this->css_assets['alertify_def_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.default.css';
						$this->css_assets['multi_select_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/fSelect.css';
						$this->css_assets['tagit_css1'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery.tagit.css';
						$this->css_assets['tagit_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/tagit.ui-zendesk.css';
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						$this->css_assets['stylebord'] = PEDAGOGE_CP_ASSETS_URL.'/css/styleboard.css';
						//$this->css_assets['sb-admin'] = PEDAGOGE_CP_ASSETS_URL.'/css/sb-admin.css';
						
						//js-----------------
						$this->footer_js['jodit'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jodit.min.js';
						$this->footer_js['tagit'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/tag-it.min.js';
						$this->footer_js['style'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/style.js';
						//$this->footer_js['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/datepicker.js';
						$this->footer_js['scrool_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery.slimscroll.min.js';
						$this->footer_js['alertify_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/alertify.min.js';
						$this->footer_js['multi_select_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/fSelect.js';
						$this->footer_js['ui_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery-ui.js';
						$this->footer_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/queries.js';
						
						$this->header_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->header_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						//$this->app_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_teachers.js?ver='.$teachers_version;
						if($return_content) {
							//if(!$page_permission) {
								//$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							//} else {
								//Get Current user role----------------
								$user_id = get_current_user_id();
								$user = new WP_User( $user_id );
								$role='';//administrator
								if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
								    foreach ( $user->roles as $role );	
								}
								//-------------------------------------
								//echo $role;
								$query_model = new ModelQuery();
								$queries = $query_model->get_queries($user_id,$role);
								$template_vars['queries'] = $queries;
								$str_content =  $this->load_view('controlpanel/queries/index',$template_vars);	
							//}
						}
						break;
					    case 'view_query':
						$this->title = 'Admin Control Panel : View Query';
						//$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;	
						$this->css_assets['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery-ui-1.9.2.custom.min.css';
						$this->css_assets['alertify_core_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.core.css';
						$this->css_assets['alertify_def_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.default.css';
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						$this->css_assets['stylebord'] = PEDAGOGE_CP_ASSETS_URL.'/css/styleboard.css';
						//$this->css_assets['sb-admin'] = PEDAGOGE_CP_ASSETS_URL.'/css/sb-admin.css';
						
						//js-----------------
						$this->footer_js['jodit'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jodit.min.js';
						$this->footer_js['style'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/style.js';
						$this->footer_js['scrool_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery.slimscroll.min.js';
						$this->footer_js['alertify_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/alertify.min.js';
						$this->footer_js['ui_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery-ui.js';
						$this->footer_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/queries.js';
						
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						if($return_content) {
							//if(!$page_permission) {
								//$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							//} else {
								//Get Current user role----------------
								$user_id = get_current_user_id();
								$user = new WP_User( $user_id );
								$role='';//administrator
								if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
								    foreach ( $user->roles as $role );	
								}
								//-------------------------------------
								//echo $role;
								$query_model = new ModelQuery();
								$queries = $query_model->get_query_details_view($_GET['query_id']);
								$template_vars['queries'] = $queries;
								$template_vars['role'] = $role;
								$template_vars['current_user_id'] = $user_id;
								$str_content =  $this->load_view('controlpanel/queries/view_query',$template_vars);	
							//}
						}
						break;
					 case 'manage_teacher':
						$this->title = 'Admin Control Panel : Manage Teacher';
						//$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;	
						$this->css_assets['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery-ui-1.9.2.custom.min.css';
						$this->css_assets['alertify_core_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.core.css';
						$this->css_assets['alertify_def_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.default.css';
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						$this->css_assets['auto_search_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/bootstrap-select.css';
						$this->css_assets['stylebord'] = PEDAGOGE_CP_ASSETS_URL.'/css/styleboard.css';
                                                $this->footer_js['auto_search_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/bootstrap-select.js';
						
						//js-----------------
						//$this->footer_js['jodit'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jodit.min.js';
						$this->footer_js['style'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/style.js';
						$this->footer_js['scrool_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery.slimscroll.min.js';
						$this->footer_js['alertify_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/alertify.min.js';
						$this->footer_js['ui_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery-ui.js';
						$this->footer_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/queries.js';
						
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						if($return_content) {
							//if(!$page_permission) {
								//$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							//} else {
								//Get Current user role----------------
								$user_id = get_current_user_id();
								$user = new WP_User( $user_id );
								$role='';//administrator
								if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
								    foreach ( $user->roles as $role );	
								}
								//-------------------------------------
								//echo $role;
								$query_model = new ModelQuery();
								$queries = $query_model->get_query_details_view($_GET['query_id']);
								$template_vars['queries'] = $queries;
								$template_vars['role'] = $role;
								$template_vars['current_user_id'] = $user_id;
								$str_content =  $this->load_view('controlpanel/landing/manage_teacher',$template_vars);	
							//}
						}
						break;
					case 'manage_institute':
						$this->title = 'Admin Control Panel : Manage Institute';
						//$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;	
						$this->css_assets['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery-ui-1.9.2.custom.min.css';
						$this->css_assets['alertify_core_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.core.css';
						$this->css_assets['alertify_def_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.default.css';
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						$this->css_assets['auto_search_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/bootstrap-select.css';
						$this->css_assets['stylebord'] = PEDAGOGE_CP_ASSETS_URL.'/css/styleboard.css';
						//$this->css_assets['sb-admin'] = PEDAGOGE_CP_ASSETS_URL.'/css/sb-admin.css';
						
						//js-----------------
						$this->footer_js['auto_search_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/bootstrap-select.js';
						$this->footer_js['style'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/style.js';
						$this->footer_js['scrool_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery.slimscroll.min.js';
						$this->footer_js['alertify_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/alertify.min.js';
						$this->footer_js['ui_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery-ui.js';
						$this->footer_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/queries.js';
						
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						if($return_content) {
							//if(!$page_permission) {
								//$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							//} else {
								//Get Current user role----------------
								$user_id = get_current_user_id();
								$user = new WP_User( $user_id );
								$role='';//administrator
								if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
								    foreach ( $user->roles as $role );	
								}
								//-------------------------------------
								//echo $role;
								$query_model = new ModelQuery();
								$queries = $query_model->get_query_details_view($_GET['query_id']);
								$template_vars['queries'] = $queries;
								$template_vars['role'] = $role;
								$template_vars['current_user_id'] = $user_id;
								$str_content =  $this->load_view('controlpanel/landing/manage_institute',$template_vars);	
							//}
						}
						break;
						case 'referral':
						$this->title = 'Admin Control Panel : View Referral';
						//$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;	
						$this->css_assets['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery-ui-1.9.2.custom.min.css';
						$this->css_assets['alertify_core_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.core.css';
						$this->css_assets['alertify_def_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.default.css';
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						$this->css_assets['stylebord'] = PEDAGOGE_CP_ASSETS_URL.'/css/styleboard.css';
						//$this->css_assets['sb-admin'] = PEDAGOGE_CP_ASSETS_URL.'/css/sb-admin.css';
						
						//js-----------------
						$this->footer_js['jodit'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jodit.min.js';
						$this->footer_js['style'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/style.js';
						$this->footer_js['scrool_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery.slimscroll.min.js';
						$this->footer_js['alertify_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/alertify.min.js';
						$this->footer_js['ui_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery-ui.js';
						$this->footer_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/refeer.js';
						
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						if($return_content) {
							//if(!$page_permission) {
								//$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							//} else {
								//Get Current user role----------------
								$user_id = get_current_user_id();
								$user = new WP_User( $user_id );
								$role='';//administrator
								if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
								    foreach ( $user->roles as $role );	
								}
								//-------------------------------------
								//echo $role;
								//$query_model = new ModelQuery();
								//$queries = $query_model->get_query_details_view($_GET['query_id']);
								//$template_vars['queries'] = $queries;
								$template_vars['role'] = $role;
								$template_vars['current_user_id'] = $user_id;
								$str_content =  $this->load_view('controlpanel/referral/referral',$template_vars);	
							//}
						}
						break;
					    case 'view_log':
						$this->title = 'Admin Control Panel : View Log';
						//$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;	
						$this->css_assets['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/css/jquery-ui-1.9.2.custom.min.css';
						$this->css_assets['jodit_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/jodit.min.css';
						$this->css_assets['alertify_core_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.core.css';
						$this->css_assets['alertify_def_css'] = PEDAGOGE_CP_ASSETS_URL.'/css/alertify.default.css';
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						$this->css_assets['stylebord'] = PEDAGOGE_CP_ASSETS_URL.'/css/styleboard.css';
						//$this->css_assets['sb-admin'] = PEDAGOGE_CP_ASSETS_URL.'/css/sb-admin.css';
						
						//js-----------------
						$this->footer_js['jodit'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jodit.min.js';
						$this->footer_js['style'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/style.js';
						//$this->footer_js['datepicker'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/datepicker.js';
						$this->footer_js['scrool_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery.slimscroll.min.js';
						$this->footer_js['alertify_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/alertify.min.js';
						$this->footer_js['ui_js'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/jquery-ui.js';
						$this->footer_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/queries/queries.js';
						
						//$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						//$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						//$this->app_js['queries'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_teachers.js?ver='.$teachers_version;
						if($return_content) {
							//if(!$page_permission) {
								//$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							//} else {
								//Get Current user role----------------
								$user_id = get_current_user_id();
								$user = new WP_User( $user_id );
								$role='';//administrator
								if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
								    foreach ( $user->roles as $role );	
								}
								//-------------------------------------
								//echo $role;
								$query_model = new ModelQuery();
								$queries = $query_model->get_queries($user_id,$role);
								$template_vars['queries'] = $queries;



								///////////////////////changed by sudipta//////////////////////////////////////////////



								$template_vars['ids'] = $_REQUEST['query_id'];
								$template_vars['student_new'] = $query_model->get_studentemail($_REQUEST['query_id']);
								$template_vars['final_teacher'] = $query_model->get_finalteachercount($_REQUEST['query_id']);
								if($template_vars['final_teacher']>0)
								{
								$template_vars['teacher_new'] = $query_model->get_teacheremail($_REQUEST['query_id']);
									}
								$template_vars['template'] = $query_model->get_emailtemplate();



								///////////////////////changed by sudipta//////////////////////////////////////////////




								$str_content =  $this->load_view('controlpanel/queries/view_log',$template_vars);	
							//}
						}
						break;
					        case 'teacher':
						$teachers_version = '1.3';
						$this->title = 'Admin Control Panel : Teachers Data Management';
												
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;						
						//$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;						
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						
						$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						$this->app_js['teachers'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_teachers.js?ver='.$teachers_version;
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/teachers/index');	
							}
						}
						break;
					case 'institute':
						$institute_version = '1.3';
						$this->title = 'Admin Control Panel : Institute Data Management';
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;						
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						
						$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						$this->app_js['institutes'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_institutions.js?ver='.$institute_version;
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/institutions/index');
							}
						}
						break;
					case 'student':
						$this->title = 'Admin Control Panel : Student Data Management';
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/underconstruction');
							}
						}
						break;
					case 'guardian':
						$this->title = 'Admin Control Panel : Guardian Data Management';
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/underconstruction');	
							}
						}
						break;
					case 'reviews':
						$this->title = 'Admin Control Panel : Reviews Management';
						
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;
																		
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						$this->app_js['reviews'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_reviews.js?ver='.$version;
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/reviews/index');	
							}
						}
						break;
					case 'course':
						$course_version = '1.1';
						$this->title = 'Admin Control Panel : Subjects and Course Data Management';
						
						$this->css_assets['uniform'] = BOWER_ROOT_URL."/jquery.uniform/themes/default/css/uniform.default.min.css?ver=".$this->fixed_version;						
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;						
						//$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;
						
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						
						$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
						$this->footer_js['uniform'] = BOWER_ROOT_URL."/jquery.uniform/jquery.uniform.min.js?ver=".$this->fixed_version;												
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						$this->app_js['single_modal'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_single_table_modal.js?ver='.$version;
						
						$this->app_js['course'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_course.js?ver='.$course_version;
						
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/course');	
							}
						}
						break;
					case 'entities':
						$this->title = 'Admin Control Panel : Entitites Management';
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/underconstruction');
							}
						}
						break;
					case 'users':
						$this->title = 'Admin Control Panel : Users Management';
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/underconstruction');
							}
						}
						break;
					case 'impex':
						$this->title = 'Admin Control Panel : Import/Export';
						//$this->footer_js['excellentexport'] = BOWER_ROOT_URL."/excellentexport/excellentexport.js?ver=".$this->fixed_version;
						$this->app_js['impex'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_impex.js?ver='.$version;
												
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/impex');	
							}
						}
						break;
					case 'reports':
						$report_version = '1.2';
						$this->title = 'Admin Control Panel : Reports';
						$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->app_js['reports'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_reports.js?ver='.$report_version;
						
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/reports');	
							}
						}
						break;
					case 'seo':
						$seo_version = '0.1';
						$this->title = 'Admin Control Panel : SEO';
						$this->app_js['reports'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_seo.js?ver='.$seo_version;
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/seo');	
							}							
						}
						break;
					case 'locality':
						$locality_version = '0.2';
						$this->title = 'Admin Control Panel : Localities Management';
						
						$this->css_assets['uniform'] = BOWER_ROOT_URL."/jquery.uniform/themes/default/css/uniform.default.min.css?ver=".$this->fixed_version;
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;						
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						
						$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
						$this->footer_js['uniform'] = BOWER_ROOT_URL."/jquery.uniform/jquery.uniform.min.js?ver=".$this->fixed_version;
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						
						$this->app_js['locality'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_locality.js?ver='.$locality_version;
						$this->app_js['locality_list'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_locality_list.js?ver='.$locality_version;
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/locality/index');	
							}							
						}
						break;
						
					case 'settings':
						$settings_version = '0.3';
						$this->css_assets['uniform'] = BOWER_ROOT_URL."/jquery.uniform/themes/default/css/uniform.default.min.css?ver=".$this->fixed_version;
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;						
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
						//$this->app_css['settings'] = PEDAGOGE_CP_ASSETS_URL.'/css/cp_settings.css?ver='.$this->fixed_version;
						
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						
						$this->footer_js['uniform'] = BOWER_ROOT_URL."/jquery.uniform/jquery.uniform.min.js?ver=".$this->fixed_version;												
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
						$this->footer_js['ace'] = PEDAGOGE_ASSETS_URL."/plugins/ace/ace.js?ver=".$this->fixed_version;
						
						$this->app_js['settings'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_settings.js?ver='.$settings_version;
						$this->app_js['settings_reviews'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_settings_reviews.js?ver='.$settings_version;
						
						$this->title = 'Admin Control Panel : Settings';
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/settings/index');	
							}
						}
						break;
					case 'workshop':
						$workshop_version = '0.4';
						$this->css_assets['uniform'] = BOWER_ROOT_URL."/jquery.uniform/themes/default/css/uniform.default.min.css?ver=".$this->fixed_version;
						$this->css_assets['datatable'] = BOWER_ROOT_URL.'/datatables/media/css/dataTables.bootstrap.min.css?ver='.$this->fixed_version;						
						$this->app_css['pedagoge_common'] = $this->registered_css['pedagoge_common']."?ver=".$this->fixed_version;
												
						$this->footer_js['datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/jquery.dataTables.min.js?ver='.$this->fixed_version;
						$this->footer_js['bootstrap_datatable'] = PEDAGOGE_CP_ASSETS_URL.'/plugins/datatables/dataTables.bootstrap.min.js?ver='.$this->fixed_version;
						$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
						$this->footer_js['uniform'] = BOWER_ROOT_URL."/jquery.uniform/jquery.uniform.min.js?ver=".$this->fixed_version;												
						$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
						$this->footer_js['bootbox'] = BOWER_ROOT_URL."/bootbox.js/bootbox.js?ver=".$this->fixed_version;
												
						$this->app_js['workshop'] = PEDAGOGE_CP_ASSETS_URL.'/js/cp_workshop.js?ver='.$workshop_version;
												
						$this->title = 'Admin Control Panel : Workshop';
						if($return_content) {
							if(!$page_permission) {
								$str_content =  $this->load_view('controlpanel/unauthorized_acess');
							} else {
								$str_content =  $this->load_view('controlpanel/workshops/index');	
							}
						}
						break;
					case 'email':						
						$this->title = 'Admin Control Panel : Pedagoge Email';
						if($return_content) {							
							$str_content =  $this->load_view('controlpanel/email');
						}
						break;
					default:
						$this->title = 'Admin Control Panel : Page not found';
						if($return_content) {
							$str_content =  $this->load_view('controlpanel/404');
						}
						break;
				}
				
				
			} else {
				$this->title = 'Admin Control Panel : Page not Found';
				if($return_content) {
					$str_content =  $this->load_view('controlpanel/404');
				}
			}
		} else {
			//Dashboard
			$this->menu_list['dashboard']=' active ';
			$this->footer_js['index'] = PEDAGOGE_CP_ASSETS_URL.'/js/dashboard.js?ver='.$this->fixed_version;
			// $this->app_data['inline_js'][] = '
			// 	<script type="text/javascript">
			// 		$(function(){ Index.init(); });
			// 	</script>
			// ';
			
			if($return_content) {				
				$str_content = $this->load_view('controlpanel/index');
			}
		}
		
		if($return_content) {				
			return $str_content;
		}
	}

	private function fn_register_common_variables() {
		global $wpdb;
		$page_url = '';
		$current_user_email = $this->app_data['pdg_current_user']->user_email;
		if(strpos($current_user_email, 'pedagoge.com') >=0) {
			$this->app_data['current_user_email'] = $current_user_email;
			$this->app_data['pedagoge_email'] = 'yes';
		}
		if(isset($_GET['page'])) {
			
			$page_url = $_GET['page'];
			
			if(array_key_exists($page_url, $this->menu_list)) {
				switch($page_url) {
					case 'teacher':
										
						break;
					case 'institute':
						
						break;
					case 'student':
						
						break;
					case 'guardian':
						
						break;
					case 'reviews':
						
						break;
					case 'course':
						$this->app_data['course_type_data'] = PDGManageCache::fn_load_cache('pdg_course_type');
						$this->app_data['subject_type_data'] = PDGManageCache::fn_load_cache('pdg_subject_type');
						$this->app_data['subject_name_data'] = PDGManageCache::fn_load_cache('pdg_subject_name');
						$this->app_data['academic_board_data'] = PDGManageCache::fn_load_cache('pdg_academic_board');
						$this->app_data['course_age_category_data'] = PDGManageCache::fn_load_cache('pdg_course_age_category');
						break;
					case 'entities':
						
						break;
					case 'users':
						
						break;
					case 'locality':
						$this->app_data['locality_list_db'] = PDGManageCache::fn_load_cache('pdg_locality');
						$this->app_data['pdg_city'] = PDGManageCache::fn_load_cache('pdg_city');
						break;
						
					case 'impex':
						break;
						
					case 'seo':
						
						break;
					case 'reports':
						
						break;
					case 'settings':
						$this->app_data['email_types_data'] = $wpdb->get_results("select email_settings_id, email_label from pdg_email_settings");
						break;
					case 'workshop':
						//$this->app_data['email_types_data'] = $wpdb->get_results("select email_settings_id, email_label from pdg_email_settings");
						$this->app_data['pdg_locality'] = PDGManageCache::fn_load_cache('pdg_locality');
						$this->app_data['pdg_city'] = PDGManageCache::fn_load_cache('pdg_city');
						$this->app_data['pdg_workshop_categories'] = PDGManageCache::fn_load_cache('pdg_workshop_categories');
						break;
					case 'email':
						break;
					default:										
						break;
				}
				
				
			} else {
				
			}
		} else {
			//Dashboard
			/*No of likes
			no of reviews
			no of mails
			no of users*/
			$str_sql = "
				select
				(select count(ID) from wp_users) as total_users,
				(select count(recommendation_id) from pdg_recommendations) as total_recommendations,
				(select count(review_id) from pdg_reviews) as total_reviews,
				(select count(id) from pdg_contact_form_logger) as total_mails,
				(select count(institute_id) from  pdg_institutes where active = 'yes' and approved ='yes')as total_institutes,
				(select count(teacher_id) from pdg_teacher where active = 'yes' and approved ='yes')as total_teachers,
				(select count(guardian_id) from pdg_guardian) as total_guardians,
				(select count(student_id) from pdg_student) as total_students,
				(select count(locality_id) from pdg_locality where locality_id not in (select distinct locality_id from pdg_tutor_location)) as empty_localities,
			    (
			    	select count(subject_name_id) from pdg_subject_name 
			    	where subject_name_id not in (
			    		select distinct(pdg_course_category.subject_name_id) from pdg_batch_subject left join pdg_course_category on pdg_batch_subject.course_category_id = pdg_course_category.course_category_id
					)
			    ) as empty_subjects			    
			";
			
			$stats_data = $wpdb->get_results($str_sql);
			if(!empty($stats_data)) {
				$this->app_data['stats_data'] = $stats_data;
			}
		}
	}

	public static function fn_cp_impex_generator_ajax() {
		$return_message = array(
			'error' => TRUE,			
			'error_type' => '',
			'message' => 'Error message!',
			'data' => ''
		);
		
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', FALSE );
		if( !$is_valid_referer ) {
				
			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';
			
			echo json_encode($return_message );
			die();
		}
		
		$impex_type = $_POST['impex_type'];
		
		switch($impex_type) {
			case 'teacher_dump':
				$return_data = ControlPanelPedagogeImpex::fn_teacher_dump();
				echo json_encode($return_data);
				die();
				break;
			case 'institute_dump':
				$return_data = ControlPanelPedagogeImpex::fn_institute_dump();
				echo json_encode($return_data);
				die();
				break;
			default:
				$return_message['error_type'] = 'report_type_undefined';
				$return_message['message'] = 'Report type is undefined. Please select a report type!';
				break; 
		}
		
		echo json_encode($return_message);
		die();
	}

	private function fn_cp_access_permission($page_url) {
		$access_permission = false;
		
		if(current_user_can('manage_options')) {			
			return TRUE;
		}
		
		switch($page_url) {
			case 'queries':
				if(current_user_can('pdg_cap_cp_queries_access')) {			
					$access_permission = TRUE;
				}
				break;
			
			case 'teacher':
				if(current_user_can('pdg_cap_cp_teacher_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'institute':
				if(current_user_can('pdg_cap_cp_institute_access')) {			
					$access_permission = TRUE;
				}				
				break;
				
			case 'student':
				if(current_user_can('pdg_cap_cp_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'guardian':
				if(current_user_can('pdg_cap_cp_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'reviews':
				if(current_user_can('pdg_cap_cp_review_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'course':
				if(current_user_can('pdg_cap_cp_course_access')) {			
					$access_permission = TRUE;
				}					
				break;
				
			case 'entities':
				if(current_user_can('pdg_cap_cp_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'users':
				if(current_user_can('pdg_cap_cp_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'impex':
				if(current_user_can('pag_cap_cp_impex_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'seo':
				if(current_user_can('pdg_cap_cp_seo_access')) {			
					$access_permission = TRUE;
				}
				break;
			case 'locality':
				if(current_user_can('pdg_cap_cp_locality_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'reports':
				if(current_user_can('pdg_cap_cp_reports_acess')) {			
					$access_permission = TRUE;
				}
				break;
				
			case 'settings':
				if(current_user_can('pdg_cap_cp_settings_access')) {			
					$access_permission = TRUE;
				}
				break;
			case 'workshop':
				if(current_user_can('pdg_cap_cp_workshop_access')) {			
					$access_permission = TRUE;
				}
				break;
				
			default:
				$access_permission = FALSE;			
				break;
		}
		
		return $access_permission;
	}
	
	//Approved Refeerals-------------------------------------------------------
	public function approve_refeeral(){
	$ref_model = new ModelReferral();
	$reff_id=$_POST['reffer_id'];
	$reff_by=$_POST['reff_by'];
	$ref_details=$ref_model->get_ref_details_by_id($reff_id);
	$query_id=$ref_details[0]->fk_query_id;
	$ref_email=$ref_details[0]->email;
	$ref_phone=$ref_details[0]->phone;
	$ref_by_email=$ref_details[0]->user_email;
	$ref_by_phone=$ref_details[0]->phone_no;
	$by_teacher_name=$ref_details[0]->display_name;
	$ref_model->approve_refeeral($reff_id);
	
	//Send SMS----------------------------------------------------
	$query_url=home_url().'/viewquery/?query_id='.$query_id;
	$sms_text1=''.$by_teacher_name.' referred you as a teacher for a Query on Pedagoge. Note :: For new user please register as a teacher with the referred mail('.$ref_email.') ids only. View Query:- '.$query_url;
	//send sms--------------------------
	$api_key = '559E5AAED983CA';
	$from = 'PDAGOG';
	$sms_text = urlencode($sms_text1);
	
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, "http://www.sambsms.com/app/smsapi/index.php");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=7&type=text&contacts=".$ref_phone."&senderid=".$from."&msg=".$sms_text);
	$response = curl_exec($ch);
	curl_close($ch);
	
	$message='<!doctype html>
	<html>
	<head>
	<meta charset="utf-8">
	<title>Pedagoge</title>
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"> 
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
	</head>
	<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;">
			<section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;">
			<div style="width:100%;">
			<div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;">
			<img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;">                    
			</div>
			<div style="position:relative;background:#fff;">
			    <div style="margin:15px 0; text-align:center;">
				<h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Query Referral Approved:</h1>
				<p>'.$by_teacher_name.' referred you as a teacher for a Query on Pedagoge.</p>
				<p>View Query: <a href="'.$query_url.'" style="color:#13a89e;text-decoration:none;font-weight:bold;">'.$query_url.'</a></p>
				<p>Note :: For new user please register as a teacher with the referred mail('.$ref_email.') id only</p>
				<div style="margin:30px 0 15px">
					<ul style="list-style-type:none;text-align:center; padding:0;">
					<li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="Twtter" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li>
					<li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="FB" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li>
					<li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="Instagram" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></a></li>
				    </ul>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p>
				    <p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p>
				</div>
			    </div>
			</div>
		    </div>
		</section>
	</body>
	</html>';
	
	$from='noreply@pedagoge.com';
	$subject='Query referral';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	
	$status=mail($ref_email, $subject, $message, $headers);
	//----------------------------------------------------------
	echo "success";
	die();		
	}
	//-------------------------------------------------------------------------
	
	//UnApproved Refeerals-------------------------------------------------------
	public function unapprove_refeeral(){
	$ref_model = new ModelReferral();
	$reff_id=$_POST['reffer_id'];
	$ref_model->unapprove_refeeral($reff_id);
	echo "success";
	die();		
	}
	//-------------------------------------------------------------------------
}		
	


	