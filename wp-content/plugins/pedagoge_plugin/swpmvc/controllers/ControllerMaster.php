<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * This is the Master Controller Class. All Controller (Page generating) classes must inherit from this.
 * 
 * @author nirajkvinit
 */
class ControllerMaster implements ControllerMasterInterface {
	
	/**
	 * Protected Variable which holds title text for a displayed page.
	 * Any subclass can set their own title.
	 */
	protected $title;
	
	/**
	 * Protected Variable which Stores the current page URL - required for route based files or events management.
	 */
	protected $url;
	
	/**
	 * Protected Variable which stores the current page's slug - required for route based files or events management.
	 */
	protected $slug;
	
	/**
	 * Public Variable for setting up title.
	 * @todo review this again to see whether it is required or not.
	 */
	public $parent_title;

	/**
	 * Protected variable which contains a defining list of all the registered CSS files with the application.
	 */
	protected $registered_css;
	
	/**
	 * Protected variable which contains a defining list of all the registered JS files with the application.
	 */
	protected $registered_js;

	/**
	 * Includes CSS files which will be loaded on every pages and generally do not need versioning.
	 * These will be combined and compressed in production.
	 */
	protected $common_css;

	/**
	 * Library Css Files which will be loaded on specific pages, these too do not need versioning.
	 * These will be compressed but not combined in production.
	 */
	protected $css_assets;

	/**
	 * Custom CSS related to a page. These will have versioning system and will never be compressed or combined with any other files.
	 */
	protected $app_css;

	/**
	 * Google Fonts will not have any versioning system.
	 */
	protected $google_fonts;

	/**
	 * Includes JS Files which needs to be included in the header section of an HTML page.
	 */
	protected $header_js;

	/**
	 * Library JS File which will be loaded on specific paged. No versioning required. Individual file compression but no combining.
	 */
	protected $footer_js;

	/**
	 * Includes JS Files which will be loaded on all the pages. Do not need versioning. All will be combined and compressed.
	 */
	protected $footer_common_js;

	/**
	 * Includes Page specific JS Files. Will be versioned but never compressed or combined with any other files.
	 */
	protected $app_js;

	/**
	 * Protected variable which may contain class name for HTML tag for a page. Used in views.
	 */
	protected $html_class;
	
	/**
	 * Protected variable which may contain class name for body tag for a page. Used in views.
	 */
	protected $body_class;

	/**
	 * All Library files which will update rarely will have this common version.
	 */
	protected $fixed_version;


	/**
	 * Name of the current Controller Class. Need it for ajax controller which Identifies which class to find and connect to.
	 * This variable is set by the Controller Classes and exposed as javascript variable.
	 */
	protected $pedagoge_callback_class;
	
	/**
	 * Contains Visitors IP address which can be used to track the ip of a visitor.
	 */
	protected $visitor_ip_address;

	/**
	 * Pedagoge variables Container/Register. Views will get/extract data/variables from here.
	 */
	public $app_data;
	
	/**
	 * All subclass of ControllerMaster class will automatically execute this function.
	 * 
	 * @todo Review the code in MasterController
	 * .
	 * @author nirajkvinit
	 */
	public function __construct( )	{
		global $post, $wp_query;

		$this->pedagoge_callback_class = get_class($this);

		$pdg_current_page_url = $wp_query->virtual_page->getUrl();
		$pdg_parsed_current_url_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

		//@todo improve this to handle multiple endpoints
		$pdg_current_page_slug = str_replace( PEDAGOGE_ENDPOINT_URL, '', $pdg_parsed_current_url_path );

		$this->slug = $pdg_current_page_slug;
		$this->url = $pdg_current_page_url;
		$this->parent_title = 'Pedagoge ';
		$this->title = $this->parent_title;

		//For versioning all the common styles and scripts.
		$this->fixed_version = '0.50';

		//CSS Classes
		$this->html_class = '';
		$this->body_class = '';

		//Registered CSS and JS Universal Container
		$this->registered_css = array();
		$this->registered_js = array();

		//Divide all registered CSS and JS into their own containers.
		//CSS Containers
		$this->common_css = array();
		$this->css_assets = array();
		$this->app_css = array();
		$this->google_fonts = array();

		//JS Containers
		$this->header_js = array();
		$this->footer_js = array();
		$this->footer_common_js = array();
		$this->app_js = array();

		//Application Supports two menu system. Header and Main. 
		$this->header_menu = array();
		$this->main_menu = array();

		//Variables Container. Views will extract data/variables from here.
		$this->app_data = array();

		$this -> html_class = 'footer-sticky';

		//Register all the scripts and put them into their specific containers.
		$this->__register_scripts();

		//Acitivities Logging will be implemented later.
		//$this->activities_log_model = new STActivitiesLogModel();

		//Get Visitor's IP Address.
		$this->visitor_ip_address = pedagoge_get_visitor_ip();

		// Put variables to variables Registery $this->app_data
		$this->fn_register_common_variables();

	}

	/**
	 * Loads all the documented scripts.
	 * Extract CSS and JS to put them into their respective Registered Containers.
	 * 	Populates $registered_css array.
	 * 	Populates $registered_js array.
	 *  @author nirajkvinit
	 */
	private function __register_scripts() {
		// Load all the registered scripts
		$assets_loader = new PDGAssetsLoader();
		$assets = $assets_loader->fn_load_scripts();

		//Extract CSS Assets and put them into Registered CSS Container
		$this->registered_css = $assets[ 'css' ];

		//Extract JS Assets and put them into Registered JS Container
		$this->registered_js = $assets[ 'js' ];

		//Some cleanup
		unset($assets_loader);
		unset($assets);

		//Load all the common CSS and JS which will be required for all the pages.
		$this->fn_load_scripts();
	}

	/**
	 * Route Handler calls this function on a child object created from a Controller Class (Page Creator) manually,
	 * instead of its automatic constructor function __construct.
	 * Upon invocation child object calls its parent's automatic constructor function __construct for initial setup.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_construct_class() {
	}

	/**
	 * This function loads all the scripts required for every pages and distributes them into their respective containers.
	 * Other controllers will have their own version of this function to load scripts related to that specific page.
	 * @author nirajkvinit
	 */
	private function fn_load_scripts() {

		/**
		 * Load Common CSS and JS for a blank Page
		 */

		$fixed_version = $this->fixed_version;
		$common_css_version = '0.3';
		$common_js_version = '0.1';
		$custom_css_version = '0.4';
		$custom_js_version = '0.5';

		$registered_styles = $this->registered_css;
		$registered_scripts = $this->registered_js;

		/**
		 * Load CSS--------------------------------------------------------------
		 */
		//CSS Containers

        /** Load Google Fonts */
		$this->google_fonts['google_opensans'] = $registered_styles['google_opensans'];

		/** Load CSS Assets @todo Move the components in their containers */
		$this->css_assets['pace'] = $registered_styles['pace']."?ver=".$fixed_version;
		$this->css_assets['uniform'] = $registered_styles['uniform']."?ver=".$fixed_version;
		$this->css_assets['bootstrap'] = $registered_styles['bootstrap']."?ver=".$fixed_version;
		$this->css_assets['fontawesome'] = $registered_styles['fontawesome']."?ver=".$fixed_version;
		$this->css_assets['ankitesh-validationEngine'] = $registered_styles['ankitesh-validationEngine']."?ver=".$fixed_version;
		
		$this->app_css['pedagoge_common'] = $registered_styles['pedagoge_common']."?ver=".$custom_css_version;

		//JS Containers
		/**
		 * Load JS--------------------------------------------------------------
		 */
		/** Load header js **/
		$this->header_js['pace'] = $registered_scripts['pace']."?ver=".$fixed_version;
		$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$fixed_version;
		
		/** Load footer js **/
		$this->footer_js['jquery'] = $registered_scripts['jquery']."?ver=".$fixed_version;
		$this->footer_js['jquery'] = $registered_scripts['jquery']."?ver=".$fixed_version;
		$this->footer_js['jquery-ui'] = $registered_scripts['jquery-ui']."?ver=".$fixed_version;

		$this->footer_js['bootstrap'] = $registered_scripts['bootstrap']."?ver=".$fixed_version;
		$this->footer_js['uniform'] = $registered_scripts['uniform']."?ver=".$fixed_version;		
		$this->footer_js['validationengine'] = $registered_scripts['validationengine'].'?ver='.$this->fixed_version;
		$this->footer_js['validationengine-en'] = $registered_scripts['validationengine-en'].'?ver='.$this->fixed_version;
		
		/** Load Application common js **/
		$this->app_js['pedagoge_common'] = $registered_scripts['pedagoge_common']."?ver=".$custom_js_version;

		/**let other controllers load their own css/js files **/
	}

	/**
	 * Description: Themes - version 2 css/js files
	 * 
	 * @author Ganesh
	 */
	public function fn_themes_ver2_load_scripts() {
		unset( $this->google_fonts );
		unset( $this->css_assets );
		unset( $this->app_css );
		unset( $this->header_js );
		unset( $this->footer_js );
		unset( $this->app_js );

		$this->google_fonts['quicksand'] = 'https://fonts.googleapis.com/css?family=Quicksand:400,700';
		$this->google_fonts['material_icons'] = 'https://fonts.googleapis.com/icon?family=Material+Icons';
		$this->css_assets['bootstrap-style'] = 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css';
		$this->css_assets['bootstrap_material_design'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/libs/bootstrap-material-design.min.css?ver=' . $this->fixed_version;
		$this->css_assets['material_ripples'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/libs/ripples.min.css?ver=' . $this->fixed_version;
		$this->css_assets['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/global-all.css?ver=' . $this->fixed_version;
		$this->footer_js['ver2_homepage_plugins'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/ver2_pluginshome.min.js?ver=' . $this->fixed_version;
	}
	
	/**
	 * Loads the view for a page's header section. Child class can over ride this.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_get_header() {

		return $this->load_view('header');
	}
	
	/**
	 * Loads the view for a page's footer section. Child class can over ride this.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_get_footer() {

		return $this->load_view('footer');
	}
	
	/**
	 * Loads the view for a page's content section. Child class can over ride this.
	 * 
	 * @author nirajkvinit
	 */
	public function fn_get_content() {

		return $this->load_view('index');
	}
	
	/**
	 * This function combines all the return code from header/content/footer 
	 * section view to create the final view and returns the generated html code. 
	 * 
	 * @author nirajkvinit
	 */
	public function fn_display() {

		$header = $this->fn_get_header();
		$footer = $this->fn_get_footer();
		$content = $this->fn_get_content();

		return $header.$content.$footer;
	}

	/**
	 * All views are relative to the folder swpmvc/views/
	 * just name the file and not its extensions to parse and load php and html code.
	 */
	public function load_view($view, $template_vars = null) {

		$view_file_name = PEDAGOGE_PLUGIN_DIR.'views/'.$view.'.html.php';

		$str_return = '';

		if(file_exists($view_file_name)) {

			extract($this->app_data);

			if(isset($template_vars) && !empty($template_vars)) {
				extract($template_vars);
			}

			ob_start();
			include_once($view_file_name);
			$str_return = ob_get_clean();

		} else {
			$str_return = 'Error! Loading view '.$view_file_name;
		}

		return $str_return;
	}

	/**
	 * Function to register common variables to global application variables registry
	 */
	private function fn_register_common_variables() {
		$current_user = wp_get_current_user();
		//perhaps we don't need this variable. still, lets continue with this.
		$this->app_data['pdg_current_user'] = $current_user;
		$this->app_data['visitor_ip'] = $this->visitor_ip_address;
		$this->app_data['inline_js'] = '';
	}

} //Class ends here.
