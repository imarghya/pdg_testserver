<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerSeo extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}
	
	public function fn_construct_class() {
		parent::__construct();		
		$this->title = 'Pedagoge - Find Teacher and Institute at different locations';
		$this->body_class .= ' page-register login-alt page-header-fixed';		
		$this->fn_load_scripts();
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		//Load Styles and Scripts for Login and Register Page
		$seo_version = '0.1';
		
		unset($this->css_assets['waves']);
		unset($this->css_assets['ankitesh-validationEngine']);
		unset($this->css_assets['offcanvasmenueffects']);
		unset($this->css_assets['3d-bold-navigation']);
		unset($this->css_assets['ankitesh-search']);
		unset($this->css_assets['ankitesh-modern']);
		unset($this->css_assets['ankitesh-theme-green']);
		unset($this->css_assets['ankitesh-custom']);		
		unset($this->header_js['ankitesh-forms']);
		unset($this->footer_js['validationengine']);
		unset($this->footer_js['validationengine-en']);
		unset($this->footer_js['ankitesh-modern']);
		unset($this->footer_js['offcanvasmenueffects']);
		
		//$this->app_js['pedagoge_profile'] = PEDAGOGE_ASSETS_URL."/js/students_profile.js?ver=".$profile_version;
		
		$this->css_assets['proui_plugins'] = PEDAGOGE_PROUI_URL."/css/plugins.css?ver=".$this->fixed_version;
		$this->css_assets['proui_main'] = PEDAGOGE_PROUI_URL."/css/main.css?ver=".$this->fixed_version;
		$this->css_assets['proui_themes'] = PEDAGOGE_PROUI_URL."/css/themes.css?ver=".$this->fixed_version;
		$this->css_assets['select2'] = $this->registered_css['select2'].'?ver='.$this->fixed_version;
		$this->css_assets['custom'] = PEDAGOGE_PROUI_URL."/css/custom.css?ver=".$this->fixed_version;
		$this->css_assets['custom_backend'] = PEDAGOGE_PROUI_URL."/css/custom_backend.css?ver=".$this->fixed_version;
	    
	    $this->footer_js['proui_plugin'] = PEDAGOGE_PROUI_URL."/js/plugins.js?ver=".$this->fixed_version;
		$this->footer_js['app_js'] = PEDAGOGE_PROUI_URL."/js/app.js?ver=".$this->fixed_version;
		$this->footer_js['backstretch'] = PEDAGOGE_PROUI_URL."/js/jquery.backstretch.min.js?ver=".$this->fixed_version;
		$this->footer_js['select2'] = $this->registered_js['select2'].'?ver='.$this->fixed_version;
		
		$this->app_js['seo_urls'] = PEDAGOGE_ASSETS_URL.'/js/seo_urls.js?ver='.$seo_version;

	}

	private function fn_register_common_variables() {
		//$seo_urls = PDGManageCache::fn_load_cache('pdg_seo_urls');
		//$this->app_data['seo_urls'] = $seo_urls;
	}
	
	public function fn_get_header() {
		return $this->load_view('search/header');		
	}
	
	public function fn_get_footer() {
		return $this->load_view('search/footer');
	}
	
	public function fn_get_content() {
		global $wpdb;
		
		$str_seo_urls_list = ControlPanelSeo::fn_return_seo_urls_list();
		
		return '
			<br />
			<br /><br /><br /><br /><br /><br />
			<section class="site-content site-section">
                <div class="container">
                	<div class="well">
                		'.$str_seo_urls_list.'
                	</div>
				</div>
			</section>
			<br /><br /><br /><br /><br /><br />
		';
	}
	
	public static function fn_return_seo_urls_list() {
		global $wpdb;
		
		$str_return = '';
		//$str_sql = "select * from pdg_seo_urls";
	}
}