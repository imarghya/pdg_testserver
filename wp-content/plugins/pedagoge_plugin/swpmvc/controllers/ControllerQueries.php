<?php
session_start();
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerQueries extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {
		
	}

	public function assign_timezone(){
		$timezone_offset_minutes =  $_POST['timezone_offset_minutes'];
		//echo $timezone_offset_minutes;
		//Convert minutes to seconds
		$timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes*60, false);
		date_default_timezone_set($timezone_name);
		// Asia/Kolkata
		
		echo $timezone_name;
		die();
	}
	
	//Get quires data first time-----------------------------
	public function get_quires(){
	 echo "Its Working...";
	}
	//-------------------------------------------------------
	//Assign Sales Rep---------------------------------------
	public function assign_sales_rep(){
	$sales_rep_id = sanitize_user( $_POST['sales_rep_id'] );
	$query_ids = sanitize_user( $_POST['query_ids'] );
	$query_model = new ModelQuery();
	$res = $query_model->assign_sales_rep($sales_rep_id,$query_ids);
	die();
	}
	//-------------------------------------------------------
	//Change Query Status---------------------------------
	public function change_query_status(){
	$query_id = sanitize_user( $_POST['query_id'] );
	$status = sanitize_user( $_POST['status'] );
	$query_model = new ModelQuery();
	$res = $query_model->change_query_status($query_id,$status);
	die();
	}

	//Change FastPass Status---------------------------------
	public function change_fast_pass_status(){
	$query_id = sanitize_user( $_POST['query_id'] );
	$status = sanitize_user( $_POST['status'] );
	$query_model = new ModelQuery();
	$res = $query_model->change_fast_pass_status($query_id,$status);
	die();
	}
	//-------------------------------------------------------
	//Change Follow Up Date----------------------------------
	public function change_follow_up_date(){
	$query_id = sanitize_user( $_POST['query_id'] );
	$follow_up_date = sanitize_user( $_POST['follow_up_date'] );
	$query_model = new ModelQuery();
	$res = $query_model->change_follow_up_date($query_id,$follow_up_date);
	die();
	}
	//-------------------------------------------------------
	//Change FastPass Status---------------------------------
	public function change_review_status(){
	$query_id = sanitize_user( $_POST['query_id'] );
	$status = sanitize_user( $_POST['status'] );
	$query_model = new ModelQuery();
	$res = $query_model->change_review_status($query_id,$status);
	die();
	}
	//-------------------------------------------------------
	//Query Approve------------------------------------------
	public function query_approve(){
	$query_ids = sanitize_user( $_POST['query_ids'] );
	$query_model = new ModelQuery();
	$res = $query_model->query_approve($query_ids);
	//Send mail to user--------------------
	$query_id_arr=explode(",",$query_ids);
	foreach($query_id_arr AS $query_id){
	$student_details=$query_model->get_student_details_by_qid($query_id);
	$student_name=$student_details[0]->display_name;
	$student_email=$student_details[0]->user_email;

	/************** New Section For Student Notification ***************/
	$student_id = $student_details[0]->user_id;
	$type = 'approve';
	$student_notification = $query_model->student_notification($query_id,$student_id,$type);
	/************** New Section For Student Notification ***************/

	global $wpdb;

	$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$query_id;
	$res_subject = $wpdb->get_results($sql_subject);
	$subject_name = $res_subject[0]->subject_name;

	$mail_body = "Your Query ID : ".$query_id." for the subject : ".$subject_name." got approved";

	// Compose a HTML email message
	$message = '<!doctype html><html><head><meta charset="utf-8"><title>Pedagoge</title><link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"></head>';
	$message .= '<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;"><section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;"><div style="width:100%;"><div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;"></div>';
    $message .= '<div style="position:relative;background:#fff;"><div style="margin:15px 0; text-align:center;"><h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Query Approved</h1><p>'.$mail_body.'</p>';
    $message .= '<div style="margin:30px 0 15px"><ul style="list-style-type:none;text-align:center; padding:0;"><li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></a></li></ul><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p></div></div></div></div></section></body></html>';
	
	//Send mail-----------------------
	$from='noreply@pedagoge.com';
	$subject= $subject_name.' Query Approved';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	//$headers .= "Bcc: arghya.saha@techexactly.com\r\n";
	$email = $student_email;
	//$email = "arghya.saha@techexactly.com";
	$status=mail($email, $subject, $message, $headers);


	$template_vars['name'] = $student_name;
	$template_vars['query_id'] = $query_id;
	$template_vars['subject'] = 'Query Approved';
	$mail_data = array (
		'to_address'    => $student_email,
		'mail_type'     => 'query_approved',
		'template_vars' => $template_vars,
	);
	$pedagoge_mailer = new PedagogeMailer( $mail_data );
	$mail_process_completed = $pedagoge_mailer->sendmail();
	$mail_process_completed = null;
	}
	//-------------------------------------
	die();
	}
	//-------------------------------------------------------
	//Query Disapprove------------------------------------------
	public function query_disapprove(){
	$query_ids = sanitize_user( $_POST['query_ids'] );
	$message = sanitize_user( $_POST['message'] );
	$query_model = new ModelQuery();
	$res = $query_model->query_disapprove($query_ids,$message);

	/************** New Section For Student Notification ***************/
	$query_id_arr=explode(",",$query_ids);
	foreach($query_id_arr AS $query_id){
		$student_details=$query_model->get_student_details_by_qid($query_id);
		$student_id = $student_details[0]->user_id;
		$student_email=$student_details[0]->user_email;
		$type = 'disapprove';
		$student_notification = $query_model->student_notification($query_id,$student_id,$type);
	}
	/************** New Section For Student Notification ***************/

	global $wpdb;

	$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$query_id;
	$res_subject = $wpdb->get_results($sql_subject);
	$subject_name = $res_subject[0]->subject_name;

	$mail_body = "Your Query ID : ".$query_id." for the subject : ".$subject_name." got disapproved";

	// Compose a HTML email message
	$message = '<!doctype html><html><head><meta charset="utf-8"><title>Pedagoge</title><link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"></head>';
	$message .= '<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;"><section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;"><div style="width:100%;"><div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;"></div>';
    $message .= '<div style="position:relative;background:#fff;"><div style="margin:15px 0; text-align:center;"><h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Query Disapproved</h1><p>'.$mail_body.'</p><p> Reason : '.sanitize_user( $_POST['message'] ).'</p>';
    $message .= '<div style="margin:30px 0 15px"><ul style="list-style-type:none;text-align:center; padding:0;"><li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></a></li></ul><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p></div></div></div></div></section></body></html>';
	
	//Send mail-----------------------
	$from = 'noreply@pedagoge.com';
	$subject = $subject_name.' Query Disapproved';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	$headers .= "Bcc: arghya.techexactly@gmail.com\r\n";
	$email = $student_email;
	//$email = "arghya.saha@techexactly.com";
	$status=mail($email, $subject, $message, $headers);

	die();
	}
	//-------------------------------------------------------
	//Query Close------------------------------------------
	public function query_close(){
	$query_ids = sanitize_user( $_POST['query_ids'] );
	$message = sanitize_user( $_POST['message'] );
	$query_model = new ModelQuery();
	$res = $query_model->query_close($query_ids,$message);

	/************** New Section For Student Notification ***************/
	$query_id_arr1=explode(",",$query_ids);
	$query_id_arr=array_unique($query_id_arr1);
	foreach($query_id_arr AS $query_id){
		$student_details=$query_model->get_student_details_by_qid($query_id);
		$student_id = $student_details[0]->user_id;
		$type = 'close';
		$student_notification = $query_model->student_notification($query_id,$student_id,$type);
	}
	/************** New Section For Student Notification ***************/
	die();
	}
	//-------------------------------------------------------
	
	//Get Filter queries-------------------------------------
	public function get_filter_queries(){
	@$st_query_date=sanitize_user( $_POST['st_query_date'] );
	@$end_query_date=sanitize_user( $_POST['end_query_date'] );
	@$st_start_date=sanitize_user( $_POST['st_start_date'] );
	@$end_start_date=sanitize_user( $_POST['end_start_date'] );
	@$status=$_POST['qstatus'];
	@$flag_status=$_POST['flag_status'];
	@$header_query_status=$_POST['header_query_status'];
	@$lead_source = $_POST['lead_source'];
	//Get Current user role----------------
	$user_id = get_current_user_id();
	$user = new WP_User( $user_id );
	$role='';//administrator
	if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
	    foreach ( $user->roles as $role );	
	}
	//-------------------------------------
	$query_model = new ModelQuery();
	$results = $query_model->get_filter_queries($user_id,$role,$st_query_date,$end_query_date,$st_start_date,$end_start_date,$status,$header_query_status,$lead_source);
	//echo $results;
	//exit;
	?>
	<table class="table admin_query_table_1905" id="table_query">
               <thead>
                  <tr class="text-uppercase">
                               <th></th>
                               <th class="blank_space"></th>
                       <th class="admin_blue_background color_white cell_decorate">Id</th>
                       <th class="admin_blue_background color_white cell_decorate">Sales Rep</th>
                       <th class="admin_blue_background color_white cell_decorate">Fast Pass</th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                               Status 
                               <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                  <form>
                                   <ul class="flag_option_details text-uppercase">
                                       <li class="dropdown-item"><label for="status_option_1">Hot</label><input <?php if(in_array('HOT',$status)){echo "checked";} ?> class="fl_query_status" type="checkbox" name="query_status[]" value="HOT" id="status_option_1"></li>
                                       <li class="dropdown-item"><label for="status_option_2">Baked</label><input <?php if(in_array('BAKED',$status)){echo "checked";} ?> class="fl_query_status" type="checkbox" name="query_status[]" value="BAKED" id="status_option_2"></li>
                                       <li class="dropdown-item"><label for="status_option_3">pay_exp_dt</label><input <?php if(in_array('PAY_EXP_DT',$status)){echo "checked";} ?> class="fl_query_status" type="checkbox" name="query_status[]" value="PAY_EXP_DT" id="status_option_3"></li>
                                       <li class="dropdown-item"><label for="status_option_4">Pay_rec</label><input <?php if(in_array('PAY_REC',$status)){echo "checked";} ?> class="fl_query_status" type="checkbox" name="query_status[]" value="PAY_REC" id="status_option_4"></li>
                                       <li class="dropdown-item"><label for="status_option_5">paid_to</label><input <?php if(in_array('PAID_TO',$status)){echo "checked";} ?> class="fl_query_status" type="checkbox" name="query_status[]" value="PAID_TO" id="status_option_5"></li>
                                       <li class="dropdown-item"><label for="status_option_6">closed</label><input <?php if(in_array('CLOSED',$status)){echo "checked";} ?> class="fl_query_status" type="checkbox" name="query_status[]" value="CLOSED" id="status_option_6"></li>
                                   </ul>
                                  </form>
                             </div>
                           </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                           Flag 
                           <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <form method="post">
                                   <ul class="flag_option_details">
                                       <li class="dropdown-item"><label for="flag_option_1">Very Urgent</label><input <?php if(in_array('Very Urgent',$flag_status)){echo "checked";} ?> class="fl_flag_status" type="checkbox" name="flag_option[]" value="Very Urgent" id="flag_option_1"></li>
                                       <li class="dropdown-item"><label for="flag_option_2">Urgent</label><input <?php if(in_array('Urgent',$flag_status)){echo "checked";} ?> class="fl_flag_status" type="checkbox" name="flag_option[]" value="Urgent" id="flag_option_2"></li>
                                       <li class="dropdown-item"><label for="flag_option_3">Normal</label><input <?php if(in_array('Normal',$flag_status)){echo "checked";} ?> class="fl_flag_status" type="checkbox" name="flag_option[]" value="Normal" id="flag_option_3"></li>
                                       <li class="dropdown-item"><label for="flag_option_4">Expired</label><input <?php if(in_array('Expired',$flag_status)){echo "checked";} ?> class="fl_flag_status" type="checkbox" name="flag_option[]" value="Expired" id="flag_option_4"></li>
                                   </ul>
                                </form>
                             </div>
                           </div>
                       </th>
                       <th class="blank_space"></th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                       Query Dt 
                       <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                 <form method="post">
                                    <ul class="query_date_filter">
                                       <li class="dropdown-item"><label>Start Date : </label><input class="form-control date" id="query_st_date" name="query_st_date" placeholder="MM/DD/YYYY" type="text" value="<?php echo $st_query_date; ?>"/></li>
                                       <li class="dropdown-item"><label>End Date : </label><input class="form-control date" id="query_end_date" name="query_end_date" placeholder="MM/DD/YYYY" type="text" value="<?php echo $end_query_date; ?>"/></li>
                                       <li class="dropdown-item text-center clearfix"><button id="filter_query_date_btn" type="button" class="green">Filter</button><button type="button"  class="red fl_cancel">Cancel</button></li>
                                    </ul>
                                 </form>
                             </div>
                           </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">Lead Source
                       	<div class="dropdown">
                        	<button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                        	<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
	                           	<form>
	                            	<ul class="flag_option_details text-uppercase">
		                                <li class="dropdown-item"><label for="">Others</label><input <?php if(in_array('Others',$lead_source)){echo "checked";}?> class="fl_query_lead" type="checkbox" name="query_lead[]" value="Others" id="lead_option_1"></li>
		                                <li class="dropdown-item"><label for="">Website</label><input <?php if(in_array('Website',$lead_source)){echo "checked";}?> class="fl_query_lead" type="checkbox" name="query_lead[]" value="Website" id="lead_option_2"></li>
		                                <li class="dropdown-item"><label for="status_option_3">Flyers</label><input <?php if(in_array('Flyers',$lead_source)){echo "checked";}?> class="fl_query_lead" type="checkbox" name="query_lead[]" value="Flyers" id="lead_option_3"></li>
		                                <li class="dropdown-item"><label for="">Customer.Ref</label><input <?php if(in_array('Customer.Ref',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Customer.Ref" id="lead_option_4"></li>
		                                <li class="dropdown-item"><label for="">Teacher.Ref</label><input <?php if(in_array('Teacher.Ref',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Teacher.Ref" id="lead_option_5"></li>
		                                <li class="dropdown-item"><label for="">Exhibition</label><input <?php if(in_array('Exhibition',$lead_source)){echo "checked";}?> class="fl_query_lead" type="checkbox" name="query_lead[]" value="Exhibition" id="lead_option_6"></li>
		                                <li class="dropdown-item"><label for="">TRP</label><input <?php if(in_array('TRP',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="TRP" id="lead_option_7"></li>
		                                <li class="dropdown-item"><label for="">SMS</label><input <?php if(in_array('SMS',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="SMS" id="lead_option_8"></li>
		                                <li class="dropdown-item"><label for="">Social Media</label><input <?php if(in_array('Social Media',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Social Media" id="lead_option_9"></li>
		                                <li class="dropdown-item"><label for="">Old Requirement</label><input <?php if(in_array('Old Requirement',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Old Requirement" id="lead_option_10"></li>
		                                <li class="dropdown-item"><label for="">Facebook Groups</label><input <?php if(in_array('Facebook Groups',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Facebook Groups" id="lead_option_11"></li>
		                                <li class="dropdown-item"><label for="">Returning Cust</label><input <?php if(in_array('Returning Cust',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Returning Cust" id="lead_option_12"></li>
		                                <li class="dropdown-item"><label for="">Posters</label><input <?php if(in_array('Posters',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Posters" id="lead_option_13"></li>
		                                <li class="dropdown-item"><label for="">Employee Ref</label><input <?php if(in_array('Employee Ref',$lead_source)){echo "checked";}?>  class="fl_query_lead" type="checkbox" name="query_lead[]" value="Employee Ref" id="lead_option_14"></li>
	                            	</ul>
	                            </form>
                        	</div>
                        </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate">Name</th>
                       <th class="admin_blue_background color_white cell_decorate">Contact No</th>
                       <th class="admin_blue_background color_white cell_decorate">Class/Age</th>
                       <th class="admin_blue_background color_white cell_decorate">Category</th>
                       <th class="admin_blue_background color_white cell_decorate subject_line_break">Subject</th>
                       <th class="admin_blue_background color_white cell_decorate">Locality</th>
                       <th class="admin_blue_background color_white cell_decorate admin_head_filter admin_flag_option">
                               Follow up dt 
                               <div class="dropdown">
                             <button type="button" class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter" aria-hidden="true"></i></button>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                 <form method="post">
                                               <ul class="query_date_filter">
                                               <li class="dropdown-item"><label>Start Date : </label><input class="form-control date" id="follow_up_st_date" name="follow_up_st_date" placeholder="MM/DD/YYYY" type="text" value="<?php echo $st_start_date; ?>" /></li>
                                           <li class="dropdown-item"><label>End Date : </label><input class="form-control date" id="follow_up_end_date" name="follow_up_end_date" placeholder="MM/DD/YYYY" type="text" value="<?php echo $end_start_date; ?>" /></li>
                                           <li class="dropdown-item text-center clearfix"><button id="filter_followup_date_btn" type="button" class="green">Filter</button><button  type="button"  class="red fl_cancel">Cancel</button></li>
                                       </ul>
                                 </form>
                             </div>
                           </div>
                       </th>
                       <th class="admin_blue_background color_white cell_decorate">Details</th>
                       <th class="admin_blue_background color_white cell_decorate">Roc</th>
                       <th class="admin_blue_background color_white cell_decorate">Review</th>
                  </tr>
               </thead>
               <tbody id="queru_list">
               <input type="text" id="query_selected_ids" value="0" style="display: none;" />
	<?php
	$sales_rep_arr=array();
	foreach($results AS $query){
		if($query->sales_rep_name!=''){
		$sales_rep_arr[]=$query->sales_rep_name;
		}
	}
	
	foreach($results AS $query){
	$fast_pass=$query->fast_pass;        
	$lead_source='Flyers';
	if($query->lead_source==0){
	 $lead_source='';  
	}
	
	$counts = array_count_values($sales_rep_arr);
        //echo $counts[$query->sales_rep_name];
	
	//For flag status---------------------------------
	$flag='';
	$system_date=time();
	$query_stsrt_date=strtotime($query->start_date);
	$datediff = $query_stsrt_date - $system_date;
	$days= floor($datediff / (60 * 60 * 24));
	if($days <=3 && $query_stsrt_date > $system_date ){
	 $flag='Very Urgent';  
	}
	elseif($days > 3 && $days <= 7 && $query_stsrt_date > $system_date){
	 $flag='Urgent';   
	}
	elseif($days > 7 && $query_stsrt_date > $system_date){
	 $flag='Normal';   
	}
	else{
	 $flag='Expired';     
	}
	//------------------------------------------------
	//For Review Status-------------------------------
	$is_review=$query->is_review;
	//------------------------------------------------
	 //Get Locality------------------------------------
	$query_model = new ModelQuery();
	$res = $query_model->get_q_locality($query->localities);
	//$locality=implode(",",$res);
	$locality=$res[0];
        //------------------------------------------------
	$standard_age='';
	if($query->standard!='' && $query->age!=0){
	 $standard_age= $query->standard.'/'.$query->age.' Years';
	}
	elseif($query->standard!='' && $query->age==0){
	 $standard_age= $query->standard;  
	}
	else{
	  $standard_age= $query->age.' Years';   
	}
	if($flag_status!='' && in_array($flag,$flag_status)){
	?>
	 <tr id="query_tr_<?php echo $query->id; ?>" data-ww="<?php if($counts[$query->sales_rep_name]>0){echo $counts[$query->sales_rep_name].' Queries Assigned To '.$query->sales_rep_name;} ?>" class="wenk-align--center wenk-length--large query_tr" data-wenk="">
		<td class="cell_decorate squere_width"><input type="checkbox" class="query_id" id="query<?php echo $query->id; ?>" name="query_id[]" value="<?php echo $query->id; ?>" data-status="<?= $query->query_status;?>" data-hstatus="<?= $header_query_status;?>" /><label for="query<?php echo $query->id; ?>"></label></td>
		<td class="blank_space"></td>
		<td class="text-uppercase text-center cell_decorate"><?php echo $query->id; ?></td>
		<td class="text-uppercase text-center cell_decorate" id="sales_rep_name_<?php echo $query->id; ?>"><?php echo $query->sales_rep_name; ?></td>
		<td class="text-uppercase text-center cell_decorate fast_pass_select">
		 <select class="fast_pass" query-id="<?php echo $query->id; ?>">
		    <option <?php if($fast_pass=='2'){echo "selected";} ?> value="1" class="it"></option>
		    <option <?php if($fast_pass==1){echo "selected";} ?> value="1" class="it">Yes</option>
		    <option <?php if($fast_pass==0){echo "selected";} ?> value="0" class="it">No</option>
		 </select>
		</td>
		<td class="text-uppercase text-center cell_decorate fast_pass_select">
			<?php //$query->query_status;?>
                        <?php
						if($user->roles[0] == "administrator")
						{
						?>
                        <select class="query_ststus" query-id="<?php echo $query->id; ?>">
                           <option <?php if($query->query_status==''){echo "selected";} ?> value="" class="it"></option>
                           <option <?php if($query->query_status=='HOT'){echo "selected";} ?> value="HOT" class="it">Hot</option>
                           <option <?php if($query->query_status=='BAKED'){echo "selected";} ?> value="BAKED" class="it">Baked</option>
                           <option <?php if($query->query_status=='PAY_EXP_DT'){echo "selected";} ?> value="PAY_EXP_DT" class="it">Pay_exp_dt</option>
                           <option <?php if($query->query_status=='PAY_REC'){echo "selected";} ?> value="PAY_REC" class="it">Pay_rec</option>
                           <option <?php if($query->query_status=='PAID_TO'){echo "selected";} ?> value="PAID_TO" class="it">Paid_to</option>
                           <option <?php if($query->query_status=='CLOSED'){echo "selected";} ?> value="CLOSED" class="it">Closed</option>
                        </select>
                        <?php
                        }
                        else
                        {
                        	echo $query->query_status;
                        }
                        ?>
                </td>
		<td class="text-center cell_decorate"><?php echo $flag; ?><?php //echo $days; ?></td>
		<td class="blank_space"></td>
		<td class="text-uppercase text-center cell_decorate">
		<?php
		//echo $query->querydate;
		$utctime = strtotime('+7 hour',strtotime($query->querydate .' UTC'));
        $dateInLocal = date("Y-m-d H:i:s", $utctime);
        echo $query_date = date("Y-m-d H:i:s A",strtotime($dateInLocal));
		?><!--<input class="form-control" name="date" placeholder="07/28/2017" type="text" value="<?php echo $query->start_date; ?>"/>--></td>
		<td class="text-center cell_decorate">
		
						<?php /*?><?php echo $lead_source; ?><?php */?>
                        
                        <select class="lead_source" query-id="<?php echo $query->id; ?>">
                           <option <?php if($query->lead_source=='Others'){echo "selected";} ?> value="Others">Others</option>
                           <option <?php if($query->lead_source=='Website'){echo "selected";} ?> value="Website">Website</option>
                           <option <?php if($query->lead_source=='Flyers'){echo "selected";} ?> value="Flyers">Flyers</option>
                           <option <?php if($query->lead_source=='Customer.Ref'){echo "selected";} ?> value="Customer.Ref">Customer.Ref</option>
                           <option <?php if($query->lead_source=='Teacher.Ref'){echo "selected";} ?> value="Teacher.Ref">Teacher.Ref</option>
                           <option <?php if($query->lead_source=='Exhibition'){echo "selected";} ?> value="Exhibition">Exhibition</option>
                           <option <?php if($query->lead_source=='TRP'){echo "selected";} ?> value="TRP">TRP</option>
                           <option <?php if($query->lead_source=='SMS'){echo "selected";} ?> value="SMS">SMS</option>
                           <option <?php if($query->lead_source=='Social Media'){echo "selected";} ?> value="Social Media">Social Media</option>
                           <option <?php if($query->lead_source=='Old Requirement'){echo "selected";} ?> value="Old Requirement">Old Requirement</option>
                           <option <?php if($query->lead_source=='Facebook Groups'){echo "selected";} ?> value="Facebook Groups">Facebook Groups</option>
                           <option <?php if($query->lead_source=='Returning Cust'){echo "selected";} ?> value="Returning Cust">Returning Cust</option>
                           <option <?php if($query->lead_source=='Posters'){echo "selected";} ?> value="Posters">Posters</option>
                           <option <?php if($query->lead_source=='Employee Ref'){echo "selected";} ?> value="Employee Ref">Employee Ref</option>
                        </select>
                        
                        </td>
		<td class="text-center cell_decorate"><?php echo $query->student_name; ?></td>
		<td class="text-center cell_decorate"><?= ($query->contact_no != ""?$query->contact_no:$query->mobile_no);?></td>
		<td class="text-center cell_decorate"><?php echo $standard_age; ?></td>
		<td class="text-center cell_decorate"><?php echo $query->course_type; ?></td>
		<td class="text-uppercase text-center cell_decorate subject_line_break"><?php echo $query->subject_name; ?></td>
		<td class="text-uppercase text-center cell_decorate"><?php echo $locality; ?></td>
		<td class="text-uppercase text-center cell_decorate follow_up_date_td"><input class="form-control follow_up_date" query-id="<?php echo $query->id; ?>" sales-rep="<?php echo $query->current_sales_rep_id; ?>" current-date="<?php echo $query->follow_up_date; ?>" value="<?php echo $query->follow_up_date; ?>" type="text"/></td>
		<td class="text-center cell_decorate"><a href="<?php echo home_url('/dashboard').'?page=view_query&query_id='.$query->id; ?>" class="admin_blue_background color_white border_radius_12">View</a></td>
		<td class="text-center cell_decorate"><?php //if($query->roc==''){ ?>
			<?php
			if($user->roles[0] == "administrator")
			{
			?>
			<input type="text" q-id="<?php echo $query->id; ?>" value="<?php echo $query->roc; ?>" readonly class="assign roc_assign" data-toggle="modal" data-target="#roc">
			<?php
			}
			else
			{
			?>
			<input type="text" q-id="<?php echo $query->id; ?>" value="<?php echo $query->roc; ?>" readonly class="assign roc_assign">
			<?php	
			}
			?>
			<?php //}else{echo $query->roc; } ?></td>
		<td class="text-uppercase text-center cell_decorate fast_pass_select">
		 <select class="review" query-id="<?php echo $query->id; ?>">
		    <option <?php if($is_review==0){echo "selected";} ?> value="0" class="it">No</option>
		    <option <?php if($is_review==1){echo "selected";} ?> value="1" class="it">Yes</option></select>
		</td>
	   </tr>
	<?php
	}
	if($flag_status==''){
	?>
		<tr id="query_tr_<?php echo $query->id; ?>" data-ww="<?php if($counts[$query->sales_rep_name]>0){echo $counts[$query->sales_rep_name].' Queries Assigned To '.$query->sales_rep_name;} ?>" class="wenk-align--center wenk-length--large query_tr" data-wenk="">
		<td class="cell_decorate squere_width"><input type="checkbox" class="query_id" id="query<?php echo $query->id; ?>" name="query_id[]" value="<?php echo $query->id; ?>" data-status="<?= $query->query_status;?>" data-hstatus="<?= $header_query_status;?>" /><label for="query<?php echo $query->id; ?>"></label></td>
		<td class="blank_space"></td>
		<td class="text-uppercase text-center cell_decorate"><?php echo $query->id; ?></td>
		<td class="text-uppercase text-center cell_decorate" id="sales_rep_name_<?php echo $query->id; ?>"><?php echo $query->sales_rep_name; ?></td>
		<td class="text-uppercase text-center cell_decorate fast_pass_select">
		 <select class="fast_pass" query-id="<?php echo $query->id; ?>">
		    <option <?php if($fast_pass=='2'){echo "selected";} ?> value="1" class="it"></option>
		    <option <?php if($fast_pass==1){echo "selected";} ?> value="1" class="it">Yes</option>
		    <option <?php if($fast_pass==0){echo "selected";} ?> value="0" class="it">No</option>
		 </select>
		</td>
		<td class="text-uppercase text-center cell_decorate fast_pass_select">
			<?php //$query->query_status;?>
						<?php
						if($user->roles[0] == "administrator")
						{
						?>
                        <select class="query_ststus" query-id="<?php echo $query->id; ?>">
                           <option <?php if($query->query_status==''){echo "selected";} ?> value="" class="it"></option>
                           <option <?php if($query->query_status=='HOT'){echo "selected";} ?> value="HOT" class="it">Hot</option>
                           <option <?php if($query->query_status=='BAKED'){echo "selected";} ?> value="BAKED" class="it">Baked</option>
                           <option <?php if($query->query_status=='PAY_EXP_DT'){echo "selected";} ?> value="PAY_EXP_DT" class="it">Pay_exp_dt</option>
                           <option <?php if($query->query_status=='PAY_REC'){echo "selected";} ?> value="PAY_REC" class="it">Pay_rec</option>
                           <option <?php if($query->query_status=='PAID_TO'){echo "selected";} ?> value="PAID_TO" class="it">Paid_to</option>
                           <option <?php if($query->query_status=='CLOSED'){echo "selected";} ?> value="CLOSED" class="it">Closed</option>
                        </select>
                        <?php
                    	}
                    	else
                    	{
                    		echo $query->query_status;
                    	}
                        ?>
                </td>
		<td class="text-center cell_decorate"><?php echo $flag; ?><?php //echo $days; ?></td>
		<td class="blank_space"></td>
		<td class="text-uppercase text-center cell_decorate">
		<?php
		//echo $query->querydate;
		$utctime = strtotime('+7 hour',strtotime($query->querydate .' UTC'));
            	$dateInLocal = date("Y-m-d H:i:s", $utctime);
            	echo $query_date = date("Y-m-d H:i:s A",strtotime($dateInLocal));
		?><!--<input class="form-control" name="date" placeholder="07/28/2017" type="text" value="<?php echo $query->start_date; ?>"/>--></td>
		<td class="text-center cell_decorate">
		
						<?php /*?><?php echo $lead_source; ?><?php */?>
                        
                        <select class="lead_source" query-id="<?php echo $query->id; ?>">
                           <option <?php if($query->lead_source=='Others'){echo "selected";} ?> value="Others">Others</option>
                           <option <?php if($query->lead_source=='Website'){echo "selected";} ?> value="Website">Website</option>
                           <option <?php if($query->lead_source=='Flyers'){echo "selected";} ?> value="Flyers">Flyers</option>
                           <option <?php if($query->lead_source=='Customer.Ref'){echo "selected";} ?> value="Customer.Ref">Customer.Ref</option>
                           <option <?php if($query->lead_source=='Teacher.Ref'){echo "selected";} ?> value="Teacher.Ref">Teacher.Ref</option>
                           <option <?php if($query->lead_source=='Exhibition'){echo "selected";} ?> value="Exhibition">Exhibition</option>
                           <option <?php if($query->lead_source=='TRP'){echo "selected";} ?> value="TRP">TRP</option>
                           <option <?php if($query->lead_source=='SMS'){echo "selected";} ?> value="SMS">SMS</option>
                           <option <?php if($query->lead_source=='Social Media'){echo "selected";} ?> value="Social Media">Social Media</option>
                           <option <?php if($query->lead_source=='Old Requirement'){echo "selected";} ?> value="Old Requirement">Old Requirement</option>
                           <option <?php if($query->lead_source=='Facebook Groups'){echo "selected";} ?> value="Facebook Groups">Facebook Groups</option>
                           <option <?php if($query->lead_source=='Returning Cust'){echo "selected";} ?> value="Returning Cust">Returning Cust</option>
                           <option <?php if($query->lead_source=='Posters'){echo "selected";} ?> value="Posters">Posters</option>
                           <option <?php if($query->lead_source=='Employee Ref'){echo "selected";} ?> value="Employee Ref">Employee Ref</option>
                        </select>
                        
                        </td>
		<td class="text-center cell_decorate"><?php echo $query->student_name; ?></td>
		<td class="text-center cell_decorate"><?= ($query->contact_no != ""?$query->contact_no:$query->mobile_no);?></td>
		<td class="text-center cell_decorate"><?php echo $standard_age; ?></td>
		<td class="text-center cell_decorate"><?php echo $query->course_type; ?></td>
		<td class="text-uppercase text-center cell_decorate subject_line_break"><?php echo $query->subject_name; ?></td>
		<td class="text-uppercase text-center cell_decorate"><?php echo $locality; ?></td>
		<td class="text-uppercase text-center cell_decorate follow_up_date_td"><input class="form-control follow_up_date" query-id="<?php echo $query->id; ?>" sales-rep="<?php echo $query->current_sales_rep_id; ?>" current-date="<?php echo $query->follow_up_date; ?>" value="<?php echo $query->follow_up_date; ?>" type="text"/></td>
		<td class="text-center cell_decorate"><a href="<?php echo home_url('/dashboard').'?page=view_query&query_id='.$query->id; ?>" class="admin_blue_background color_white border_radius_12">View</a></td>
		<td class="text-center cell_decorate"><?php //if($query->roc==''){ ?>
			<?php
			if($user->roles[0] == "administrator")
			{
			?>
			<input type="text" q-id="<?php echo $query->id; ?>" value="<?php echo $query->roc; ?>" class="assign roc_assign" data-toggle="modal" data-target="#roc">
			<?php
			}
			else
			{
			?>
			<input type="text" q-id="<?php echo $query->id; ?>" value="<?php echo $query->roc; ?>" class="assign roc_assign">
			<?php	
			}
			?>
			<?php //}else{echo $query->roc; } ?></td>
		<td class="text-uppercase text-center cell_decorate fast_pass_select">
		 <select class="review" query-id="<?php echo $query->id; ?>">
		    <option <?php if($is_review==0){echo "selected";} ?> value="0" class="it">No</option>
		    <option <?php if($is_review==1){echo "selected";} ?> value="1" class="it">Yes</option></select>
		</td>
	   </tr>

	<?php
	}
	}
	?>
	</tbody>
            </table>
	<?php
	echo "<script>
		$(document).ready(function(){
			var table=$('#table_query').DataTable({'ordering': false,'sDom':'ltipr'});
			$('#srch-term').on('keyup',function(){table.search($(this).val()).draw();
			});
			$('.admin_search_btn').on('click',function(){table.search($('#srch-term').val()).draw();
			});
		});
		</script>";
	die();
	}
	//-------------------------------------------------------
	//Get Query Status---------------------------------------
	public function get_query_status_byid(){
	$query_id=$_POST["query_id"];
	$query_model = new ModelQuery();
	$results = $query_model->get_query_status_byid($query_id);
	echo $results;
	die();
	}
	//-------------------------------------------------------
	//Get Subject Typeid list--------------------------------
	public function get_standard_list(){
	$subject_name_id=$_POST['subject_id'];
	$query_model = new ModelQuery();
	$results = $query_model->get_standard_list($subject_name_id);
	foreach($results AS $res){
	?>
	<option value="<?php echo $res->subject_type; ?>"><?php echo $res->subject_type; ?></option>
	<?php
	}
	die();
	}
	//-------------------------------------------------------
	//Get Query Details For Clone----------------------------
	public function get_clone_query_details(){
	$query_id=$_POST["query_id"];
	$query_model = new ModelQuery();
	$results = $query_model->get_query_details($query_id);
	foreach($results AS $res){
	$course_type_id_arr=$query_model->course_type_id_arr($res->subject);
	?>
	<script>$('#clone_locality').fSelect();</script>
	<div class="col-sm-12">
	   <input type="hidden" id="clone_student_id" value="<?php echo $res->student_id; ?>" name="student_id"/>
	   <ul class="profile_form">
		  <li class="full_width"><h2>Query Details</h2></li>
		  <li class="triangel_decoratiton">
		      <span>Subject : </span>
		      <select required name="subject" id="clone_subject" class="field-select" disabled="disabled">
			<?php
			$subject_list=$query_model->get_subject_list();
			foreach($subject_list AS $sub){
			?>
			<option <?php if($sub->subject_name_id==$res->subject){echo "selected";} ?> value="<?php echo $sub->subject_name_id; ?>"><?php echo $sub->subject_name; ?></option>
			<?php } ?>
		      </select>
		  </li>
		  <?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
			 <li>
			<span>Age : </span>
			 <input placeholder="Age" required name="age" id="clone_age" value="<?php echo $res->age; ?>" pattern="\d*" class="field-long" type="text">
			 </li>
		<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
		  <li class="triangel_decoratiton">
		      <span>School/college : </span>
		      <select required name="school" class="field-select" id="clone_school">
			<optgroup label="SCHOOL">
				<?php
				 $schools=$query_model->get_school_list();     
				 foreach($schools as $school){
				?>
				<option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?= $school->school_id ?>" data-type="school"><?= $school->school_name ?></option>
				<?php
				}
				?>
				</optgroup>
				<optgroup label="COLLEGE">
				<?php
				 $colleges=$query_model->get_college_list();     
				 foreach($colleges as $college){
				?>
				<option <?php if($college->college_id==$res->college){echo "selected";} ?> value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
				<?php
				}
				?>
				</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Board/University : </span>
		      <select required name="board" class="field-select" id="clone_board">
			<optgroup label="BOARD">
			<?php
			 $boards=$query_model->get_board_list();
			 foreach($boards as $board){
			?>
			<option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?= $board->academic_board_id ?>" data-type="board"><?= $board->academic_board ?></option>
			<?php
			 }
			?>
			</optgroup>
			<optgroup label="UNIVERSITY">
			<?php
			$university = $query_model->get_university_list();
			foreach($university as $u){
			?>
				<option <?php if($u->id==$res->university){echo "selected";} ?> value="<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
			<?php
			}
			?>
			</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Standard/Yeary : </span>
		      <select id="clone_standard" name="clone_standard" class="field-select">
		      <?php
		      //$std_list=$query_model->get_standard_list($res->subject);
		      $a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			  $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
		      //foreach($std_list AS $std){
		      ?>
		     <optgroup label="SCHOOL">
				 <?php
				 foreach($a_school_standards as $s){
				 ?>
				 <option <?php if($res->standard==$s){echo "selected";} ?> value="<?= $s ?>"><?= $s; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
			<optgroup label="COLLEGE">
				<?php
				 foreach($a_college_standards as $c){
				 ?>
				 <option <?php if($res->standard==$c){echo "selected";} ?> value="<?= $c ?>"><?= $c; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
		      </select>
		  </li>
		   <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
		   <li>
			<span>Age : </span>
			 <input placeholder="Age" required name="age" id="clone_age" value="<?php echo $res->age; ?>" pattern="\d*" class="field-long" type="text">
		   </li>
		   <li class="triangel_decoratiton">
		      <span>School/college : </span>
		      <select required name="school" class="field-select" id="clone_school">
			<optgroup label="SCHOOL">
				<?php
				 $schools=$query_model->get_school_list();     
				 foreach($schools as $school){
				?>
				<option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?= $school->school_id ?>" data-type="school"><?= $school->school_name ?></option>
				<?php
				}
				?>
				</optgroup>
				<optgroup label="COLLEGE">
				<?php
				 $colleges=$query_model->get_college_list();     
				 foreach($colleges as $college){
				?>
				<option <?php if($college->college_id==$res->college){echo "selected";} ?> value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
				<?php
				}
				?>
				</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Board/University : </span>
		      <select required name="board" class="field-select" id="clone_board">
			<optgroup label="BOARD">
				<?php
				 $boards=$query_model->get_board_list();
				 foreach($boards as $board){
				?>
				<option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?= $board->academic_board_id ?>" data-type="board"><?= $board->academic_board ?></option>
				<?php
				 }
				?>
				</optgroup>
				<optgroup label="UNIVERSITY">
				<?php
				$university = $query_model->get_university_list();
				foreach($university as $u){
				?>
					<option <?php if($u->id==$res->university){echo "selected";} ?> value="<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
				<?php
				}
				?>
				</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Standard/Yeary : </span>
		      <select id="clone_standard" name="clone_standard" class="field-select">
		      <?php
		      //$std_list=$query_model->get_standard_list($res->subject);
		      //foreach($std_list AS $std){
		      $a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			  $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
		      ?>
		      <optgroup label="SCHOOL">
				 <?php
				 foreach($a_school_standards as $s){
				 ?>
				 <option <?php if($res->standard==$s){echo "selected";} ?> value="<?= $s ?>"><?= $s; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
			<optgroup label="COLLEGE">
				<?php
				 foreach($a_college_standards as $c){
				 ?>
				 <option <?php if($res->standard==$c){echo "selected";} ?> value="<?= $c ?>"><?= $c; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
		      </select>
		  </li>
		   <?php } ?>
		  
		  <li>
		      <span>Preferred Option : </span>
		      <input type="checkbox" <?php if($res->myhome==1){echo "checked";} ?> name="my_home" value="1" id="clone_my_home" class="no_margin_left"><label for="clone_my_home">My Home</label>
		      <input type="checkbox" <?php if($res->tutorhome==1){echo "checked";} ?> name="t_home" value="1" id="clone_t_home"><label for="clone_t_home">Tutor's Home</label>
		      <input type="checkbox" <?php if($res->institute==1){echo "checked";} ?> name="institute" value="1" id="clone_institute"><label for="clone_institute">Institute</label>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Locality : </span>
		      <select name="clone_locality[]" class="field-select" id="clone_locality" multiple>
		      <?php
		      $locs=explode(",",$res->localities);
		      $locality_list=$query_model->get_locality_list();
		      foreach($locality_list AS $loc){
		      ?>
		      <option <?php if(in_array($loc->locality_id,$locs)){echo "selected";} ?> value="<?php echo $loc->locality_id; ?>"><?php echo $loc->locality; ?></option>
		      <?php } ?>
		      </select>
		  </li>
		  <li><span>Start Date :</span> <input class="field-long" id="clone_query_date" name="clone_query_date" value="<?php echo $res->start_date; ?>" type="text"></li>
		  <li><span>More Info :</span> <textarea id="clone_requirement" name="requirement" class="field-long"><?php echo $res->requirement; ?></textarea></li>
		  <li class="full_width"><h2>Contact Details</h2></li>
		  <li><span>Name :</span> <input id="clone_student_name" class="field-long" required name="student_name" type="text" readonly value="<?php echo $res->student_name; ?>" /></li>
		  <li><span>Phone Number :</span> <input id="clone_phone_number" class="field-long" required  name="phone_number" readonly type="text" value="<?php echo $res->mobile_no; ?>" /></li>
		  <li><span>Email :</span> <input id="clone_user_email" class="field-long" required name="user_email" type="email" readonly value="<?php echo $res->user_email; ?>" /></li>
		  <li class="text-center">
		      <button type="submit" class="blue_button">Submit</button>
		  </li>
	      </ul>
	</div>
	<?php
	}
	die();
	}
	//-------------------------------------------------------
	//Submit Clone Query-------------------------------------
	public function submit_clone_query(){
	//$data=urldecode($_POST['data']);
	$clone_student_id=$_POST['clone_student_id'];
	$clone_subject=$_POST['clone_subject'];
	$clone_school=$_POST['clone_school'];
	$clone_board=$_POST['clone_board'];
	$clone_school_type = $_POST['clone_school_type'];
	$clone_board_type = $_POST['clone_board_type'];
	$clone_standard=$_POST['clone_standard'];
	$age=$_POST['age'];
	@$clone_my_home=$_POST['clone_my_home'];
	@$clone_t_home=$_POST['clone_t_home'];
	@$clone_institute=$_POST['clone_institute'];
	$clone_locality=implode(",",$_POST['clone_locality']);
	$clone_query_date=$_POST['clone_query_date'];
	$clone_requirement=$_POST['clone_requirement'];
	$clone_student_name=$_POST['clone_student_name'];
	$clone_phone_number=$_POST['clone_phone_number'];
	$clone_user_email=$_POST['clone_user_email'];
	//echo $clone_user_email;
	$query_model = new ModelQuery();
	$query_id = $query_model->submit_clone_query($clone_student_id,$clone_subject,$clone_school,$clone_board,$clone_standard,$age,$clone_my_home,$clone_t_home,
						    $clone_institute,$clone_locality,$clone_query_date,$clone_requirement,$clone_student_name,$clone_phone_number,
						    $clone_user_email,$clone_school_type,$clone_board_type);
	
	die();
	}
	//-------------------------------------------------------
	//Get Query Details For Edit----------------------------
	public function get_edit_query_details(){
	$query_id=$_POST["query_id"];
	$query_model = new ModelQuery();
	$results = $query_model->get_query_details($query_id);
	foreach($results AS $res){
	$course_type_id_arr=$query_model->course_type_id_arr($res->subject);
	//print_r($course_type_id_arr);
	?>
	<!--<script>$('#edit_locality').fSelect();</script>-->
	<div class="col-sm-12">
	   <input type="hidden" id="edit_student_id" value="<?php echo $res->student_id; ?>" name="student_id"/>
	   <ul class="profile_form">
		  <li class="full_width"><h2>Edit Query Details</h2></li>
		  <li class="triangel_decoratiton">
		      <span>Subject : </span>
		      <select required name="subject" id="edit_subject" class="field-select" disabled="disabled">
			<?php
			$subject_list=$query_model->get_subject_list();
			foreach($subject_list AS $sub){
			?>
			<option <?php if($sub->subject_name_id==$res->subject){echo "selected";} ?> value="<?php echo $sub->subject_name_id; ?>"><?php echo $sub->subject_name; ?></option>
			<?php } ?>
		      </select>
		  </li>
		 <?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
			 <li>
			<span>Age : </span>
			 <input placeholder="Age" required name="age" id="edit_age" pattern="\d*" value="<?php echo $res->age; ?>" class="field-long" type="text">
			 </li>
		<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
		  <li class="triangel_decoratiton">
		      <span>School/college : </span>
		      <select required name="school" class="field-select" id="edit_school">
		      	<optgroup label="SCHOOL">
				<?php
				 $schools=$query_model->get_school_list();     
				 foreach($schools as $school){
				?>
				<option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?= $school->school_id ?>" data-type="school"><?= $school->school_name ?></option>
				<?php
				}
				?>
				</optgroup>
				<optgroup label="COLLEGE">
				<?php
				 $colleges=$query_model->get_college_list();     
				 foreach($colleges as $college){
				?>
				<option <?php if($college->college_id==$res->college){echo "selected";} ?> value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
				<?php
				}
				?>
				</optgroup>
			<?php
			//$school_list=$query_model->get_school_list();
			//foreach($school_list AS $school){
			?>
		       <!--<option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?php echo $school->school_id; ?>"><?php echo $school->school_name; ?></option>-->
		      <?php //} ?>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Board/University : </span>
		      <select required name="board" class="field-select" id="edit_board">
		      	<optgroup label="BOARD">
			<?php
			 $boards=$query_model->get_board_list();
			 foreach($boards as $board){
			?>
			<option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?= $board->academic_board_id ?>" data-type="board"><?= $board->academic_board ?></option>
			<?php
			 }
			?>
			</optgroup>
			<optgroup label="UNIVERSITY">
			<?php
			$university = $query_model->get_university_list();
			foreach($university as $u){
			?>
				<option <?php if($u->id==$res->university){echo "selected";} ?> value="<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
			<?php
			}
			?>
			</optgroup>
			<?php
			//$board_list=$query_model->get_board_list();
			//foreach($board_list AS $board){
			?>
		       <!--<option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?php echo $board->academic_board_id; ?>"><?php echo $board->academic_board; ?></option>-->
		      <?php //} ?>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Standard/Yeary : </span>
		      <select id="edit_standard" name="edit_standard" class="field-select">
		      <?php
		      $a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			  $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
		      //$std_list=$query_model->get_standard_list($res->subject);
		      //foreach($std_list AS $std){
		      ?>
		      <optgroup label="SCHOOL">
				 <?php
				 foreach($a_school_standards as $s){
				 ?>
				 <option <?php if($res->standard==$s){echo "selected";} ?> value="<?= $s ?>"><?= $s; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
			<optgroup label="COLLEGE">
				<?php
				 foreach($a_college_standards as $c){
				 ?>
				 <option <?php if($res->standard==$c){echo "selected";} ?> value="<?= $c ?>"><?= $c; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
		      <!--<option <?php if($res->standard==$std->subject_type){echo "selected";} ?> value="<?php echo $std->subject_type; ?>"><?php echo $std->subject_type; ?></option>-->
		      <?php //} ?>
		      </select>
		  </li>
		  <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
		   <li>
			<span>Age : </span>
			 <input placeholder="Age"  required name="age" id="edit_age" pattern="\d*" value="<?php echo $res->age; ?>" class="field-long" type="text">
		   </li>
		  <li class="triangel_decoratiton">
		      <span>School/college : </span>
		      <select required name="school" class="field-select" id="edit_school">
			<optgroup label="SCHOOL">
				<?php
				 $schools=$query_model->get_school_list();     
				 foreach($schools as $school){
				?>
				<option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?= $school->school_id ?>" data-type="school"><?= $school->school_name ?></option>
				<?php
				}
				?>
				</optgroup>
				<optgroup label="COLLEGE">
				<?php
				 $colleges=$query_model->get_college_list();     
				 foreach($colleges as $college){
				?>
				<option <?php if($college->college_id==$res->college){echo "selected";} ?> value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
				<?php
				}
				?>
				</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Board/University : </span>
		      <select required name="board" class="field-select" id="edit_board">
			<optgroup label="BOARD">
				<?php
				 $boards=$query_model->get_board_list();
				 foreach($boards as $board){
				?>
				<option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?= $board->academic_board_id ?>" data-type="board"><?= $board->academic_board ?></option>
				<?php
				 }
				?>
				</optgroup>
				<optgroup label="UNIVERSITY">
				<?php
				$university = $query_model->get_university_list();
				foreach($university as $u){
				?>
					<option <?php if($u->id==$res->university){echo "selected";} ?> value="<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
				<?php
				}
				?>
				</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Standard/Yeary : </span>
		      <select id="edit_standard" name="edit_standard" class="field-select">
		      <?php
		      //$std_list=$query_model->get_standard_list($res->subject);
		      //foreach($std_list AS $std){
		      $a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			  $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
		      ?>
		      <optgroup label="SCHOOL">
				 <?php
				 foreach($a_school_standards as $s){
				 ?>
				 <option <?php if($res->standard==$s){echo "selected";} ?> value="<?= $s ?>"><?= $s; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
			<optgroup label="COLLEGE">
				<?php
				 foreach($a_college_standards as $c){
				 ?>
				 <option <?php if($res->standard==$c){echo "selected";} ?> value="<?= $c ?>"><?= $c; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
		      <!--<option <?php if($res->standard==$std->subject_type){echo "selected";} ?> value="<?php echo $std->subject_type; ?>"><?php echo $std->subject_type; ?></option>-->
		      <?php //} ?>
		      </select>
		  </li>
		  <?php } ?>
		  
		  
		  <li>
		      <span>Preferred Option : </span>
		      <input type="checkbox" <?php if($res->myhome==1){echo "checked";} ?> name="my_home" value="1" id="edit_my_home" class="no_margin_left"><label for="clone_my_home">My Home</label>
		      <input type="checkbox" <?php if($res->tutorhome==1){echo "checked";} ?> name="t_home" value="1" id="edit_t_home"><label for="clone_t_home">Tutor's Home</label>
		      <input type="checkbox" <?php if($res->institute==1){echo "checked";} ?> name="institute" value="1" id="edit_institute"><label for="clone_institute">Institute</label>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Locality : </span>
		      <select style="display: none;" name="clone_locality[]" class="field-select" id="edit_locality" multiple>
		      <?php
		      $locs=explode(",",$res->localities);
		      $locality_list=$query_model->get_locality_list();
		      foreach($locality_list AS $loc){
		      ?>
		      <option  <?php if(in_array($loc->locality_id,$locs)){echo "selected"; $lc[]=$loc->locality;} ?> value="<?php echo $loc->locality_id; ?>"><?php echo $loc->locality; ?></option>
		      <?php } ?>
		      </select>
		      <?php echo implode(",",$lc); ?>
		  </li>
		  <li><span>Start Date :</span> <input class="field-long" required id="edit_query_date" name="clone_query_date" value="<?php echo $res->start_date; ?>" type="text"></li>
		  <li><span>More Info :</span> <textarea id="edit_requirement" name="requirement" class="field-long"><?php echo $res->requirement; ?></textarea></li>
		  <li class="full_width"><h2>Contact Details</h2></li>
		  <li><span>Name :</span> <input id="edit_student_name" class="field-long" required name="student_name" type="text" readonly value="<?php echo $res->student_name; ?>" /></li>
		  <li><span>Phone Number :</span> <input id="edit_phone_number" class="field-long" required  name="phone_number" readonly type="text" value="<?php echo $res->mobile_no; ?>" /></li>
		  <li><span>Email :</span> <input id="edit_user_email" class="field-long" required name="user_email" type="email" readonly value="<?php echo $res->user_email; ?>" /></li>
		  <li class="text-center">
		      <button type="submit" class="blue_button">Submit</button>
		  </li>
	      </ul>
	</div>
	<?php
	}
	die();
	}
	//-------------------------------------------------------
	//Submit Edit Query-------------------------------------
	public function submit_edit_query(){
	//$data=urldecode($_POST['data']);
	$query_id=$_POST['query_id'];
	$clone_student_id=$_POST['clone_student_id'];
	$clone_subject=$_POST['clone_subject'];
	$clone_school=$_POST['clone_school'];
	$clone_board=$_POST['clone_board'];
	$clone_school_type = $_POST['clone_school_type'];
	$clone_board_type = $_POST['clone_board_type'];
	$clone_standard=$_POST['clone_standard'];
	$age=$_POST['age'];
	@$clone_my_home=$_POST['clone_my_home'];
	@$clone_t_home=$_POST['clone_t_home'];
	@$clone_institute=$_POST['clone_institute'];
	$clone_locality=implode(",",$_POST['clone_locality']);
	$clone_query_date=$_POST['clone_query_date'];
	$clone_requirement=$_POST['clone_requirement'];
	$clone_student_name=$_POST['clone_student_name'];
	$clone_phone_number=$_POST['clone_phone_number'];
	$clone_user_email=$_POST['clone_user_email'];
	//echo $clone_user_email;
	$query_model = new ModelQuery();
	$query_id = $query_model->submit_edit_query($query_id,$clone_student_id,$clone_subject,$clone_school,$clone_board,$clone_standard,$age,$clone_my_home,$clone_t_home,
						    $clone_institute,$clone_locality,$clone_query_date,$clone_requirement,$clone_student_name,$clone_phone_number,
						    $clone_user_email,$clone_school_type,$clone_board_type);
	
	die();
	}
	//---------------------------------------------------------------------------------------------------------------------
	
	//********************************************************New Query*********************************************************
	//For get new query subject from------------------------
	public function get_subject_from(){
	 $x=1;
	 $i=1;
	 $subject_name=array();
	 $subjects=$_POST['subjects'];
	 $selected_sub=explode(",",$subjects);
		$str="";
		$num_sub=0;
		foreach($selected_sub as $selsub) {
			$str.="'".$selsub."',";
				$num_sub++;
		}
	  $str=rtrim($str,",");
		
	 $query_model = new ModelQuery();
	 $result=$query_model->get_subject_with_type($str);
	 //print_r($result);
	 //exit;
	 foreach($result AS $res){ $subject_name[$i]=$res->subject_name; $i++;}
	 foreach($result AS $res){
	 $course_type_id_arr=$query_model->course_type_id_arr($res->subject_name_id);
	 //print_r($course_type_id_arr);
	 ?>
	  <aside class="model_wrapper">
	       <div class="modal fade" class="modal hide" data-backdrop="static" data-keyboard="false"  id="subjectSelect<?= $x; ?>" role="dialog">
	       <div class="modal-dialog modal-dialog-next">
	       <!-- Modal content-->
	       <div class="modal-content blue_border">
	       <div class="modal-header">
	       <button type="button" class="close" data-dismiss="modal">&times;</button>
	       <ul class="acadamic_subject_form">
	       <li>
	       <?php
	       if($x==1){
	       ?>
		<li><a href="#"  data-toggle="modal" data-target="#"><i ></i></a></li>
	       <?php										
	       }else{
	       ?>
		<li><a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelect<?= $x-1; ?>" data-dismiss="modal">
		
		<i class="fa fa-caret-left" aria-hidden="true"></i>
        <div id="selsubjectleft<?= $x; ?>"><?php echo $subject_name[$x-1]; ?></div>
	       </a>
	       </li>
	       <?php
	       }	
	       ?>
	       <li class="active_subject"><a href="#"><div id="mysubject"><div id="selsubject<?= $x; ?>"><?php echo $subject_name[$x]; ?></div></div></a></li>
	       <?php if(count($result) >1 && count($result)!=$x ){ ?>
	       <li>
	       <a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelect<?= $x+1 ?>" data-dismiss="modal" id="next<?= $x ?>">
	       <div id="selsubjectright<?= $x ?>"><?php echo $subject_name[$x+1]; ?></div>
	       <i class="fa fa-caret-right" aria-hidden="true"></i>
	       </a>
	       </li>
	       <?php } ?>
	       </ul>
	       </div>
		<form id="frm_submit_new<?= $x; ?>" method="post" class="subject_form" idd="<?= $x; ?>" subject="<?php echo $res->subject_name_id; ?>" totalx="<?php echo count($result); ?>">
			<div class="modal-body no_padding_top_bottom">
			<div class="row">
			
			<?php
			if($x==1){}else{
			?>
			<div class="col-sm-12 subject_type no_full_width">
			<label for="sameAs">Same as Previous</label> <input class="non_grid_input same_as_prv" idd="<?php echo $x-1; ?>" type="checkbox" name="sameAs">
			</div>
			<?php
			}
			?>
			<?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
			 <div class="col-sm-12 subject_type">
			 <input placeholder="Age"  value="" required name="age" id="age_<?php echo $x; ?>" pattern="\d*" class="validate[required,custom[integer],max[100]] text-input non_grid_input black_border non_grid_input black_border" type="text">
			 </div>
			 <?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
			 
			<div class="col-sm-12 subject_type">
			<select  name="school" id="school_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]" >
			<option value="">SCHOOL/COLLEGE</option>
			<optgroup label="SCHOOL">
			<?php
			 $schools=$query_model->get_school_list();     
			 foreach($schools as $r){
			?>
			<option value="<?= $r->school_id ?>" data-type="school"><?= $r->school_name ?></option>
			<?php
			}
			?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
			$colleges = $query_model->get_college_list();
			foreach($colleges as $college){
			?>
				<option value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="board" id="board_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">BOARD/UNIVERSITY</option>
			<optgroup label="BOARD">
			<?php
			 $boards=$query_model->get_board_list();      
			 foreach($boards as $r){
			?>
			<option value="<?= $r->academic_board_id ?>" data-type="board"><?= $r->academic_board ?></option>
			<?php
			 }
			?>
			</optgroup>
			<optgroup label="UNIVERSITY">
			<?php
			$university = $query_model->get_university_list();
			foreach($university as $u){
			?>
				<option value="<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="standard" id="standard_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">STANDARD/YEAR</option>
			<?php
			 //$standards=$query_model->get_standard_list($res->subject_name_id);  
			 //foreach($standards as $r){
			$a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			 $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
			?>
			<optgroup label="SCHOOL">
			 <?php
			 foreach($a_school_standards as $s){
			 ?>
			 <option value="<?= $s ?>"><?= $s ?></option>
			 <?php
			 }
			 ?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
			 foreach($a_college_standards as $c){
			 ?>
			 <option value="<?= $c ?>"><?= $c ?></option>
			 <?php
			 }
			 ?>
			</optgroup>
			<?php
			 //}
			?>
			</select>
			</div>
			<?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
			<div class="col-sm-12 subject_type">
			 <input placeholder="Age"  value="" required name="age" id="age_<?php echo $x; ?>" pattern="\d*" class="validate[required,custom[integer],max[100]] text-input non_grid_input black_border non_grid_input black_border" type="text">
			 </div>
			
			<div class="col-sm-12 subject_type">
			<select  name="school" id="school_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]" >
			<option value="">SCHOOL/COLLEGE</option>
			<optgroup label="SCHOOL">
			<?php
			 $schools=$query_model->get_school_list();     
			 foreach($schools as $r){
			?>
			<option value="<?= $r->school_id ?>"><?= $r->school_name ?></option>
			<?php
			}
			?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
			$colleges = $query_model->get_college_list();
			foreach($colleges as $college){
			?>
				<option value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="board" id="board_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">BOARD/UNIVERSITY</option>
			<optgroup label="BOARD">
			<?php
			 $boards=$query_model->get_board_list();      
			 foreach($boards as $r){
			?>
			<option value="<?= $r->academic_board_id ?>"><?= $r->academic_board ?></option>
			<?php
			 }
			?>
			</optgroup>
			<optgroup label="UNIVERSITY">
			<?php
			$university = $query_model->get_university_list();
			foreach($university as $u){
			?>
				<option value="<?= $u->id ?>" data-type="university"><?= $u->university_name; ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			<select name="standard" id="standard_<?php echo $x; ?>" required class="non_grid_input black_border form-control validate[required]">
			<option value="">STANDARD/YEAR</option>
			<?php
			$a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			$a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
			
			?>
			<optgroup label="SCHOOL">
			<?php
			foreach($a_school_standards as $s){
			?>
			<option value="<?= $s ?>"><?= $s; ?></option>
			<?php
			}
			?>
			</optgroup>
			<optgroup label="COLLEGE">
			<?php
			foreach($a_college_standards as $c){
			?>
			<option value="<?= $c ?>"><?= $c; ?></option>
			<?php
			}
			?>
			</optgroup>
			</select>
			</div>
			<?php } ?>
			<div class="col-sm-12 subject_type">
			<p class="text-center option_heading">Select your preferred option</p>
			<ul class="place_option">
			<input id="prefer_location_<?php echo $x; ?>" type="text" name="" value="" required style="position:absolute; border: 0; opacity: 0; background: #f1f1f2;" />
			<li><input class="prefer_location" type="checkbox" name="myhome" value="1" id="myhome_<?php echo $x; ?>" data-tt="<?= $x;?>"><label for="myhome_<?php echo $x; ?>">My Home</label></li>
			<li><input type="checkbox" class="prefer_location" name="tutorhome" value="1" id="tutorhome_<?php echo $x; ?>" data-tt="<?= $x;?>"><label for="tutorhome_<?php echo $x; ?>">Tutor's Home</label></li>
			<li><input class="prefer_location" type="checkbox" name="institute" value="1" id="institute_<?php echo $x; ?>" data-tt="<?= $x;?>"><label for="institute_<?php echo $x; ?>">Institute</label></li>
			</ul>
			<!--<input type="hidden" name="localities" id="localities">-->
			<div class="col-sm-12 no_padding subject_type angle">
			<input id="hid_localities_<?php echo $x; ?>" type="text" name="" value="" required style="position:absolute; border: 0; background: #f1f1f2; opacity: 0;">
			<select name="localities[]" id="localities_<?php echo $x; ?>" class="non_grid_input black_border form-control admin_mlc" multiple>
			<option value="">MENTION LOCALITIES</option>
			<?php
			 $localities=$query_model->get_locality_list();     
			 foreach($localities as $r){
			?>
			<option value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
			<?php
			 }
			?>
			</select>
			</div>
			<input required value="" class="mentions_localitites black_border validate[required] new_query_start_date" id="start_date_<?php echo $x; ?>" name="start_date"  placeholder="When do you want the classes to begin" type="text" />
			<textarea class="validate[required] text-input mentions_localitites black_border min_height_130" id="requirement_<?php echo $x; ?>" name="requirement" placeholder="Please mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching."></textarea>
			</div>
			</div>
			</div>
			<div class="modal-footer no_padding_top_bottom">
			 <?php //if(count($result)==$x){ ?>
			 <!--<button type="submit" idd="<?= $x; ?>" subject="<?php echo $res->subject_name_id; ?>" data-toggle="modal" data-target="#nameaddress" data-dismiss="modal" class="data_submit qdata_submit blue_background color_white">Submit</button>-->
			 <?php //}else{ ?>
			 <button type="submit" idd="<?= $x; ?>" subject="<?php echo $res->subject_name_id; ?>"  class="data_submit qdata_submit blue_background color_white">Submit</button>
			 <?php //} ?>
			</div>
		</form>
	       </div>
	       </div>      
	       </div>
	       </aside>
	 <?php
	 $x++;	
	 }
	 ?>
	<aside class="model_wrapper">
	<div class="modal fade" id="nameaddress" name="nameaddress" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-dialog-next">
	<!-- Modal content-->
	<div class="modal-content blue_border">
	<form method="post" id="frmnameemail">
	<input type="hidden" id="x" value="<?= $x-1; ?>">
	<div class="modal-body no_padding_top_bottom">
	<div class="row">
	<div class="col-sm-12 subject_type address_type">
	<input placeholder="Name" required id="studname" name="studname" class="validate[required] text-input non_grid_input black_border" type="text"  value="" />
	<input placeholder="Phone Number" pattern="[0-9]{10}" required id="phonenum" name="phonenum" class="validate[required,custom[phone]] text-input non_grid_input black_border" type="text" value="" />
	<input placeholder="Email Id" required  id="emailid" name="emailid" class="validate[required,custom[email]] text-input non_grid_input black_border" type="email" value="" />
	</div>
	</div>
	</div>
	<div class="modal-footer no_padding_top_bottom">
	<button type="button"  data-toggle="modal" data-target="#subjectSelect<?php echo $x-1; ?>" data-dismiss="modal"  class="data_submit blue_background color_white">Go Back</button>
	<button type="submit" id="submit_all_new_query" class="data_submit blue_background color_white">Submit</button>
	</div>
	</form>
	</div>
	</div>
	</div>
	</aside>
	 <?php
	 die();
	}
	//------------------------------------------------------
	//*********************************************************************************************************************
	//For Submit Query subject wise data--------------------
	public function submit_q_subject_data(){
	$x=$_POST["x"];
	$subject=$_POST["subject"];
	$_SESSION['subject'.$x]=$subject;
	
	$age=@$_POST["age"];
	$_SESSION['age'.$x]=$age;
	
	$school=$_POST["school"];
	$_SESSION['school'.$x]=$school;
	
	$school_type = $_POST['school_type'];
	$_SESSION['school_type'.$x] = $school_type;

	$board=$_POST["board"];
	$_SESSION['board'.$x]=$board;
	
	$board_type = $_POST["board_type"];
	$_SESSION['board_type'.$x] = $board_type;

	$standard=$_POST["standard"];
	$_SESSION['standard'.$x]=$standard;
	
	$myhome=$_POST["myhome"];
	$_SESSION['myhome'.$x]=$myhome;
	
	$tutorhome=$_POST["tutorhome"];
	$_SESSION['tutorhome'.$x]=$tutorhome;
	
	$institute=$_POST["institute"];
	$_SESSION['institute'.$x]=$institute;
	
	$localities=$_POST["localities"];
	$_SESSION['localities'.$x]=implode(",",$localities);
	
	$start_date=$_POST["start_date"];
	$_SESSION['start_date'.$x]=$start_date;
	
	$requirement=$_POST["requirement"];
	$_SESSION['requirement'.$x]=$requirement;
	
	//echo $_SESSION['school'.$x];
	//echo $x.'|'.$subject.'|'.$age.'|'.$school.'|'.$board.'|'.$standard.'|'.$myhome.'|'.$tutorhome.'|'.$institute.'|'.$localities.'|'.$start_date.'|'.$requirement;
	die();
	}
	//------------------------------------------------------
	//For get subject session data--------------------------
	public function get_q_subject_data(){
	$x=$_POST['x'];
	//echo $x;
	//             0                       1                        2                           3                           4                             5                           6                               7                              8                              9
	echo $_SESSION['age'.$x].'|'.$_SESSION['school'.$x].'|'.$_SESSION['board'.$x].'|'.$_SESSION['standard'.$x].'|'.$_SESSION['myhome'.$x].'|'.$_SESSION['tutorhome'.$x].'|'.$_SESSION['institute'.$x].'|'.$_SESSION['localities'.$x].'|'.$_SESSION['start_date'.$x].'|'.$_SESSION['requirement'.$x];
	die();
	}
	//------------------------------------------------------
	//For submit new query data-----------------------------
	public function submit_new_query_data(){
	$x=$_POST['x'];
	$name=$_POST['name'];
	$email=$_POST['email'];
	$phone=$_POST['phone'];
	$query_model = new ModelQuery();
	$result=$query_model->submit_new_query_data($x,$name,$email,$phone);
	echo $result;
	die();
	}
	//------------------------------------------------------
	
	//Get Query Details For Edit----------------------------
	public function get_edit_past_query_details(){
	$query_id=$_POST["query_id"];
	$query_model = new ModelQuery();
	$results = $query_model->get_query_details($query_id);
	foreach($results AS $res){
	$course_type_id_arr=$query_model->course_type_id_arr($res->subject);
	//print_r($course_type_id_arr);
	?>
	<!--<script>$('#edit_locality').fSelect();</script>-->
	<div class="col-sm-12">
	   <input type="hidden" id="edit_student_id" value="<?php echo $res->student_id; ?>" name="student_id"/>
	   <input type="hidden" id="query_id" value="<?php echo $query_id; ?>" name="query_id"/>
	   <ul class="profile_form">
		  <li class="full_width"><h2>Edit Query Details</h2></li>
		  <li class="triangel_decoratiton">
		      <span>Subject : </span>
		      <select required name="subject" id="edit_subject" class="field-select" disabled="disabled">
			<?php
			$subject_list=$query_model->get_subject_list();
			foreach($subject_list AS $sub){
			?>
			<option <?php if($sub->subject_name_id==$res->subject){echo "selected";} ?> value="<?php echo $sub->subject_name_id; ?>"><?php echo $sub->subject_name; ?></option>
			<?php } ?>
		      </select>
		  </li>
		 <?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
			 <li>
			<span>Age : </span>
			 <input placeholder="Age" required name="age" id="edit_age" pattern="\d*" value="<?php echo $res->age; ?>" class="field-long" type="text">
			 </li>
		<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
		  <li class="triangel_decoratiton">
		      <span>School/college : </span>
		      <select required name="school" class="field-select" id="edit_school">
			<optgroup label="SCHOOL">
			<?php
			$school_list=$query_model->get_school_list();
			foreach($school_list AS $school){
			?>
		       <option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?php echo $school->school_id;?>" data-type="school"><?php echo $school->school_name; ?></option>
		      <?php } ?>
		  		</optgroup>
		      <optgroup label="COLLEGE">
			<?php
				$colleges = $query_model->get_college_list();
				foreach($colleges as $college){
			?>
				<option <?php if($college->college_id==$res->school){echo "selected";} ?> value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
			<?php
			}
			?>
			</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Board/University : </span>
		      <select required name="board" class="field-select" id="edit_board">
			<optgroup label="SCHOOL">
			<?php
			$board_list=$query_model->get_board_list();
			foreach($board_list AS $board){
			?>
		       <option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?php echo $board->academic_board_id; ?>" data-type="board"><?php echo $board->academic_board; ?></option>
		      <?php } ?>
		  		</optgroup>
		  		<optgroup label="COLLEGE">
		  		<?php
					$university = $query_model->get_university_list();
					foreach($university as $u){
				?>
					<option <?php if($u->id==$res->board){echo "selected";} ?> value="<?= $u->id ?>" data-type="university"><?= $u->university_name ?></option>
				<?php
				}
				?>
		  		</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Standard/Yeary : </span>
		      <select id="edit_standard" name="edit_standard" class="field-select">
		      <?php
		      //$std_list=$query_model->get_standard_list($res->subject);
		      $a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			  $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
		      //foreach($std_list AS $std){
		      ?>
		     <optgroup label="SCHOOL">
				 <?php
				 foreach($a_school_standards as $s){
				 ?>
				 <option <?php if($res->standard==$s){echo "selected";} ?> value="<?= $s ?>"><?= $s; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
			<optgroup label="COLLEGE">
				<?php
				 foreach($a_college_standards as $c){
				 ?>
				 <option <?php if($res->standard==$c){echo "selected";} ?> value="<?= $c ?>"><?= $c; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
		      </select>
		  </li>
		  <?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
		   <li>
			<span>Age : </span>
			 <input placeholder="Age"  required name="age" id="edit_age" pattern="\d*" value="<?php echo $res->age; ?>" class="field-long" type="text">
		   </li>
		  <li class="triangel_decoratiton">
		      <span>School/college : </span>
		      <select required name="school" class="field-select" id="edit_school">
			<optgroup label="SCHOOL">
			<?php
			$school_list=$query_model->get_school_list();
			foreach($school_list AS $school){
			?>
		       <option <?php if($school->school_id==$res->school){echo "selected";} ?> value="<?php echo $school->school_id;?>" data-type="school"><?php echo $school->school_name; ?></option>
		      <?php } ?>
		  		</optgroup>
		      <optgroup label="COLLEGE">
			<?php
				$colleges = $query_model->get_college_list();
				foreach($colleges as $college){
			?>
				<option <?php if($college->college_id==$res->school){echo "selected";} ?> value="<?= $college->college_id ?>" data-type="college"><?= $college->college_name ?></option>
			<?php
			}
			?>
			</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Board/University : </span>
		      <select required name="board" class="field-select" id="edit_board">
			<optgroup label="SCHOOL">
			<?php
			$board_list=$query_model->get_board_list();
			foreach($board_list AS $board){
			?>
		       <option <?php if($board->academic_board_id==$res->board){echo "selected";} ?> value="<?php echo $board->academic_board_id; ?>" data-type="board"><?php echo $board->academic_board; ?></option>
		      <?php } ?>
		  		</optgroup>
		  		<optgroup label="COLLEGE">
		  		<?php
					$university = $query_model->get_university_list();
					foreach($university as $u){
				?>
					<option <?php if($u->id==$res->board){echo "selected";} ?> value="<?= $u->id ?>" data-type="university"><?= $u->university_name ?></option>
				<?php
				}
				?>
		  		</optgroup>
		      </select>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Standard/Yeary : </span>
		      <select id="edit_standard" name="edit_standard" class="field-select">
		      <?php
		      //$std_list=$query_model->get_standard_list($res->subject);
		      $a_school_standards = array('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6','Class 7','Class 8','Class 9','Class 10','Class 11','Class 12');
			  $a_college_standards = array('Year 1','Year 2','Year 3','Year 4','Year 5','Year 6','Year 7','Year 8','Year 9');
		      //foreach($std_list AS $std){
		      ?>
		     <optgroup label="SCHOOL">
				 <?php
				 foreach($a_school_standards as $s){
				 ?>
				 <option <?php if($res->standard==$s){echo "selected";} ?> value="<?= $s ?>"><?= $s; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
			<optgroup label="COLLEGE">
				<?php
				 foreach($a_college_standards as $c){
				 ?>
				 <option <?php if($res->standard==$c){echo "selected";} ?> value="<?= $c ?>"><?= $c; ?></option>
				 <?php
				 }
				 ?>
			</optgroup>
		      </select>
		  </li>
		  <?php } ?>
		  
		  
		  <li>
		      <span>Preferred Option : </span>
		      <input type="checkbox" <?php if($res->myhome==1){echo "checked";} ?> name="my_home" value="1" id="edit_my_home" class="no_margin_left"><label for="clone_my_home">My Home</label>
		      <input type="checkbox" <?php if($res->tutorhome==1){echo "checked";} ?> name="t_home" value="1" id="edit_t_home"><label for="clone_t_home">Tutors Home</label>
		      <input type="checkbox" <?php if($res->institute==1){echo "checked";} ?> name="institute" value="1" id="edit_institute"><label for="clone_institute">Institute</label>
		  </li>
		  <li class="triangel_decoratiton">
		      <span>Locality : </span>
		      <select style="display: none;" name="clone_locality[]" disabled class="field-select" id="edit_locality" multiple>
		      <?php
		      $locs=explode(",",$res->localities);
		      $locality_list=$query_model->get_locality_list();
		      foreach($locality_list AS $loc){
		      ?>
		      <option <?php if(in_array($loc->locality_id,$locs)){echo "selected"; $ls[]=$loc->locality;} ?> value="<?php echo $loc->locality_id; ?>"><?php echo $loc->locality; ?></option>
		      <?php } ?>
		      </select>
		      <?php echo implode(",",$ls); ?>
		  </li>
		  <li><span>Start Date :</span> <input class="field-long" required id="edit_query_date" name="clone_query_date" value="<?php echo $res->start_date; ?>" type="text"></li>
		  <li><span>More Info :</span> <textarea id="edit_requirement" name="requirement" class="field-long"><?php echo $res->requirement; ?></textarea></li>
		  <li class="full_width"><h2>Contact Details</h2></li>
		  <li><span>Name :</span> <input id="edit_student_name" class="field-long" required name="student_name" type="text" readonly value="<?php echo $res->student_name; ?>" /></li>
		  <li><span>Phone Number :</span> <input id="edit_phone_number" class="field-long" required  name="phone_number" readonly type="text" value="<?php echo $res->mobile_no; ?>" /></li>
		  <li><span>Email :</span> <input id="edit_user_email" class="field-long" required name="user_email" type="email" readonly value="<?php echo $res->user_email; ?>" /></li>
		  <li class="text-center">
		      <button type="submit" class="blue_button">Submit</button>
		  </li>
	      </ul>
	</div>
	<?php
	}
	die();
	}
	//-----------------------------------------------------------------------------------
	//Get Teacher Institute Responses----------------------------------------------------
	public function get_teacher_responses(){
	$query_model = new ModelQuery();
	$intid=$_POST["intid"];
	$invid=$_POST["invid"];
	$teacher_inst_id=$_POST['ti_id'];
	$type=$_POST['type'];
	$role=$_POST['role'];
	$profile_slug=$query_model->get_teacher_profile_slug($teacher_inst_id);
	if($intid!=0){
	$respons=$query_model->get_teacher_responses_details($intid);
	//echo "<pre>";print_r($respons);echo "</pre>";

	$respons = json_decode(json_encode($respons), true);
    uasort($respons, 'sort_by_time');
    function sort_by_time($a, $b)
    {
      return strtotime($b['internal_rating_date_time']) - strtotime($a['internal_rating_date_time']);
    }

    $_data = array();
    foreach ($respons as $v) {
      if (isset($_data[$v['teacherinst_id']])) {
        // found duplicate
        continue;
      }
      // remember unique item
      $_data[$v['teacherinst_id']] = $v;
    }
    //if you need a zero-based array, otheriwse work with $_data
    $respons = array_values($_data);
    $respons =  json_decode(json_encode($respons));
	
		foreach($respons AS $res){
		$ava_date=$res->available_from;
		$dd=date("d",strtotime($ava_date));
		$mm=date("m",strtotime($ava_date));
		$yy=date("y",strtotime($ava_date));
		?>
		<div class="modal-body no_padding_top_bottom">
		   <div class="row">
		      <div class="col-sm-12 subject_type fees">
			 <ul class="student_fees">
			    <li><input type="text" name="min_fees" id="min_fees" value="<?php echo $res->our_fees; ?>" readonly></li>
			    <li><input type="text" name="max_fees" id="max_fees" value="<?php echo $res->teacher_inst_fees; ?>" readonly></li>
			 </ul>
			 <h4 class="modal-title text-center padding10">Available From</h4>
			 <ul class="dd_mm_yy text-center">
			    <li>
			       <input type="text" name="DD" value="<?php echo $dd; ?>" readonly>
			    </li>
			    <li>
			       <input type="text" name="MM" value="<?php echo $mm; ?>" readonly>
			    </li>
			    <li>
			       <input type="text" name="YY" value="<?php echo $yy; ?>" readonly>
			    </li>
			 </ul>
			 <ul class="fees_address_details">
			    <li><input type="text" id="fees_mobile_number" name="fees_mobile_number" value="<?php echo $res->mobile; ?>" readonly> </li>
			    <li><input type="email" id="fees_email_id" name="fees_email_id" value="<?php echo $res->email; ?>" readonly> </li>
			    <li><textarea id="fees_message" name="fees_message" class="min_height_130" readonly><?php echo $res->message; ?></textarea> </li>
			 </ul>
			 <?php if($role=='administrator'){ ?>
			 <input type="hidden" id="queryid" value="<?php echo $res->fk_query_id;?>">
			 <div class="approve_unapprove no_padding_top_bottom">
			    <input value="<?php if($res->approved==1){echo 'Approved';}else{echo "Approve";}?>" ttype="t" id="<?php if($res->approved==1){echo 'Approved';}else{echo "respons_approve";}?>" idd="<?php echo $intid; ?>" q_id="<?php echo $res->fk_query_id;?>" class="name_address <?php if($res->approved==1){echo 'red_background';}else{echo "blue_background";}?> pull-left color_white" teacher_id="<?php echo $res->teacherinst_id;?>" student_id="<?php echo $res->fk_student_id;?>" type="button">
			   <?php //if($res->approved==1){?>
			    <input value="Unapprove" ttype="t" role="<?php echo $role; ?>" id="respons_disapprove" idd="<?php echo $intid; ?>" teacher_id="<?php echo $res->teacherinst_id;?>" student_id="<?php echo $res->fk_student_id;?>" class="name_address blue_background pull-right color_white" type="button">
			   <?php //} ?>
			 </div>
			 <?php } ?>
			 <div class="clearfix"></div>
			 <h4 class="modal-title text-center padding10">Internal Rating</h4>
			 <ul class="fees_rating">
			    <li>
			       <p>Punctuality</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($res->punctuality_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Responsiveness</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($res->responsiveness_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Behaviour</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($res->behaviour_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Comm Skills</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($res->communication_skills_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			 </ul>
             
             <a href="<?= home_url(); ?>/teacher/<?php echo $profile_slug; ?>" target="_blank" class="admin_update_rating">Update Rating</a>
             
		      </div>
		   </div>
		</div>
		<?php
		}
	}
	else{
	$rating=$query_model->get_teacher_rating($teacher_inst_id);
	?>
	<div class="modal-body no_padding_top_bottom">
	   <div class="row">
	      <div class="col-sm-12 subject_type fees">
		    <div class="approve_unapprove no_padding_top_bottom">
			<input type="hidden" id="fees_email_id" name="fees_email_id" value="<?php echo $res->email; ?>">
		    <?php if($role=='administrator'){ ?>
		    <input value="Approve" id="respons_approve" ttype="s" idd="<?php echo $invid; ?>" class="name_address blue_background pull-left color_white" type="button">
		    <input value="Unapprove" id="respons_disapprove" ttype="s" idd="<?php echo $invid; ?>" class="name_address blue_background pull-right color_white" type="button">
		    <?php } ?>
		    </div>
		 <div class="clearfix"></div>
		 <h4 class="modal-title text-center padding10">Internal Rating</h4>
			 <ul class="fees_rating">
			    <li>
			       <p>Punctuality</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->punctuality_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Responsiveness</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->responsiveness_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Behaviour</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->behaviour_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Comm Skills</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->communication_skills_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			 </ul>
	      </div>
	   </div>
	</div>

	<?php
	//}
	}
	die();	
	}
	//-----------------------------------------------------------------------------------
	//Respnses Approve-------------------------------------------------------------------
	public function respons_approve(){
	$id=$_POST['id'];
	$type=$_POST['type'];
	$email=$_POST['email'];
	$query_id = $_POST['query_id'];
	$teacher_id = $_POST['teacher_id'];
	$student_id = $_POST['student_id'];
	$query_model = new ModelQuery();
	$res=$query_model->respons_approve($id,$type);

	$teacher_notification = $query_model->create_notification_approve($query_id,$teacher_id);
	//$message="Your response has been approved";
	
	/************** New Section For Student Notification ***************/
	//$student_details=$query_model->get_student_details_by_qid($query_id);
	//$student_id = $student_details[0]->user_id;
	$type = 'show_interest';
	$student_notification = $query_model->student_notification($query_id,$student_id,$type);

	/************** New Section For Student Notification ***************/

	$query_link = home_url().'/viewquery/?query_id='.$query_id .'&type=interest';

	// Compose a HTML email message
	$message = '<!doctype html><html><head><meta charset="utf-8"><title>Pedagoge</title><link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"></head>';
	$message .= '<body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;"><section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;"><div style="padding:0 20px; width:100%; margin-left:-20px; margin-right:-20px;"><div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;"></div>';
    $message .= '<div style="position:relative;background:#fff;"><div style="margin:15px 0; text-align:center;"><h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Interest response approved</h1><p>Your Show Interest response has been approved against the Query </p><p>link of the Query: <a href="'.$query_link.'" style="color:#13a89e;text-decoration:none;font-weight:bold;">'.$query_link.'</a></p>';
    $message .= '<div style="margin:30px 0 15px"><ul style="list-style-type:none;text-align:center; padding:0;"><li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></a></li></ul><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p></div></div></div></div></section></body></html>';
	
	//Send mail-----------------------
	$from='noreply@pedagoge.com';
	$subject='Query Response Approved';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	//$headers .= "Bcc: arghya.saha@techexactly.com\r\n";
	
	//$query_link = home_url().'/viewquery/?query_id='.$id.'&type=interest';
	$status=mail($email, $subject, $message, $headers);
	//--------------------------------
	echo "success";
	die();	
	}
	
	//-----------------------------------------------------------------------------------
	//Respnses Disapprove Teacher Query Response-------------------------------------------------------------------
	public function respons_disapprove_teacher(){
		
		$query_id = $_POST['query_id'];
		$role = $_POST['role'];
		//exit();
		//echo $query_id." -- ".$role; exit();
		//echo $query_id."  ".$student_id;
		//$teacher_id = $_POST['teacher_id'];
		$query_model = new ModelQuery();
        
        $teacher_responses=$query_model->get_teacher_responses($query_id,$role);
         //echo "<pre>"; print_r($teacher_responses); echo "</pre>";
         foreach($teacher_responses AS $tech_resp){
         $external_avg_rat=$query_model->get_ratings($tech_resp->teacherinst_id);
          // echo $external_avg_rat['avarage_rating'];
         ?>
         <li id="respid_<?php echo $tech_resp->int_it; ?>" class="active">
            <ul>
               <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
               <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
               <li>
                  <p><?php echo $tech_resp->teacherinst_id; ?></p>
               </li>
               <li><a href="#" role="<?php echo $role; ?>" invt="0" intid="<?php echo $tech_resp->int_it; ?>" type="t" ti-id="<?php echo $tech_resp->teacherinst_id; ?>" class="border_radius_12 blue_background color_white fees_modal">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo $tech_resp->average_rating; ?>)</span></a></li>
            </ul>
         </li>
         <?php } ?>
         <!-- End Tracher List -->
         <!-- Start Student List -->
         <?php
         $student_responses=$query_model->get_student_responses($query_id,$role);
         foreach($student_responses AS $tech_resp){
         $external_avg_rat=$query_model->get_ratings($tech_resp->teacherinstid);
         ?>
         <li id="respid_<?php echo $tech_resp->id; ?>">
            <ul>
               <li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
               <!--<li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
               <li>
                  <p><?php echo $tech_resp->teacherinstid; ?></p>
               </li>
               <li><a href="#" invt="<?php echo $tech_resp->id; ?>" intid="0" type="s" ti-id="<?php echo $tech_resp->teacherinstid; ?>" class="border_radius_12 blue_background color_white fees_modal">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo $tech_resp->average_rating; ?>)</span></a></li>
            </ul>
         </li>
         <?php
         }
        die();
	}
	
	
	//-----------------------------------------------------------------------------------
	//Respnses Disapprove Match Teacher-------------------------------------------------------------------
	public function respons_disapprove_match_teacher(){
		$query_id = $_POST['query_id'];
		$student_id = $_POST['student_id'];
		//echo $query_id."  ".$student_id;
		//$teacher_id = $_POST['teacher_id'];
		$query_model = new ModelQuery();
		$matched=$query_model->get_matched_teacher_ids($query_id,$student_id);
        foreach($matched as $res){
        	$external_avg_rat=$query_model->get_ratings($res->teacherinst_id);
            $internal_avg_rat=$query_model->get_teacher_rating($res->teacherinst_id);
            ?>
            <li>
            	<ul>
                	<li class="blue_background color_white pull-left hidden"><input type="checkbox" name="query1" id="query1"></li>
	                <li><p><?php echo $res->teacherinst_id; ?></p></li>
                	<li><a href="#" type="t" ti-id="<?php echo $res->teacherinst_id; ?>" class="get_rating border_radius_12 blue_background color_white">(<?php echo $external_avg_rat['avarage_rating']; ?>)/(<?php echo @$internal_avg_rat[0]->average_rating; ?>)</span></a></li>
             	</ul>
          	</li>
            <?php
        }
        die();
	}
	
	//-----------------------------------------------------------------------------------
	//Respnses Disapprove-------------------------------------------------------------------
	public function respons_disapprove(){
	$id=$_POST['id'];
	$type=$_POST['type'];
	$email=$_POST['email'];
	$message=$_POST['message'];
	$query_id = $_POST['query_id'];
	$student_id = $_POST['student_id'];
	$teacher_id = $_POST['teacher_id'];
	$query_model = new ModelQuery();
	
	$res_delete = $query_model->remove_disapprove_teacher($query_id,$student_id,$teacher_id);
	$res_teacher_delete = $query_model->remove_disapprove_teacher_response($query_id,$student_id,$teacher_id);
	$teacher_notification = $query_model->create_notification_disapprove($query_id,$teacher_id);
	$res=$query_model->respons_disapprove($id,$type,$message);
	//Send mail-----------------------
	$from='noreply@pedagoge.com';
	$subject='Query Response Disapproved.';
	$headers  = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
	$headers .= "From: ". $from. "\r\n";
	$headers .= "Reply-To: ". $from. "\r\n";
	$headers .= "X-Mailer: PHP/" . phpversion();
	$headers .= "X-Priority: 1" . "\r\n";
	//$headers .= "Bcc: arghya.saha@techexactly.com\r\n";
	
	$query_link = home_url().'/viewquery/?query_id='.$query_id .'&type=disapprove';

	// Compose a HTML email message
	$mail_message = '<!doctype html><html><head><meta charset="utf-8"><title>Pedagoge</title><link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet"></head><body style="padding:0;margin:0; background-color:#f2f2f2; font-family:Arial, Helvetica, sans-serif;"><section style="display:block; width:730px; display:table; margin:0 auto; background-color:#fff;border:1px solid #ddd;"><div style="padding:0 20px; width:100%; margin-left:-20px; margin-right:-20px;"><div style="background-color:#0c6f82; padding:15px 0 80px; text-align:center;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/logo.png" alt="Pedagoge" style="border:0;vertical-align:middle;max-width:100%;"></div>';
                
    $mail_message .= '<div style="position:relative;background:#fff;"><div style="margin:15px 0; text-align:center;"><h1 style="font-family: "EB Garamond", serif; font-weight:normal;color:#0c6f82;font-size:32px; text-align:center; margin:0;">Interest response Disapproved</h1><p>Your Show Interest response has been disapproved for the Query</p><p>Reason of disapproval: <span style="margin-left:5px;display:inline-block;">'.$message.'</span></p><p>Query Link: <a href="'.$query_link.'" style="color:#13a89e;text-decoration:none;font-weight:bold;">'.$query_link.'</a></p>';
    
    $mail_message .= '<div style="margin:30px 0 15px"><ul style="list-style-type:none;text-align:center; padding:0;"><li style="display:inline-block;margin-right:5px;"><a href="https://twitter.com/PedagogeBaba" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/tw_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 18px; height: 15px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.facebook.com/pedagoge0/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/fb_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 10px; height: 15px; margin-top: -2px;"></a></li><li style="display:inline-block;margin-right:5px;"><a href="https://www.instagram.com/pedagogebaba/" target="_blank" style="width:30px;height:30px;color:#000;font-size:18px;line-height:30px;text-align:center;border:2px solid #000;border-radius:100%; display:inline-block;"><img src="'.home_url().'/wp-content/themes/pedagoge-pedagoge_theme/assets/ver2/desktop/images/mail_template/insta_mail.png" alt="" style="display: inline-block; vertical-align: middle; width: 15px; height: 15px; margin-top: -2px;"></a></li></ul><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Copyright &copy; 2017 Trencher Online Services, All rights reserved.</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Our mailing address is:<span style="display:block;">hello@pedagoge.com</span></p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">Want to change how you receive these emails?</p><p style="font-size:12px; font-weight:bold; letter-spacing:0.5px;">You can update your preferences or unsubscribe from this list.</p></div></div></div></div></section></body></html>';

    $status=mail($email, $subject, $mail_message, $headers);
	
	//$status=mail($email, $subject, $message, $headers);
	//--------------------------------
	
	echo "success";
	die();	
	}
	//-----------------------------------------------------------------------------------
	//get_rating_popup-------------------------------------------------------------------
	public function get_rating_popup(){
	$query_model = new ModelQuery();
	$teacher_inst_id=$_POST['ti_id'];
	$rating=$query_model->get_teacher_rating($teacher_inst_id);
	?>
	<div class="modal-body no_padding_top_bottom">
	   <div class="row">
	      <div class="col-sm-12 subject_type fees">
		 <div class="clearfix"></div>
		 <h4 class="modal-title text-center padding10">Internal Rating</h4>
			 <ul class="fees_rating">
			    <li>
			       <p>Punctuality</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->punctuality_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Responsiveness</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->responsiveness_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Behaviour</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->behaviour_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			    <li>
			       <p>Comm Skills</p>
			       <div class="text-right">
				<?php
				$pu_rate=$query_model->pdg_avg_rating_to_stars($rating[0]->communication_skills_rating);
				foreach($pu_rate AS $star){
				?>
				  <img style="width: 15px;" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/search/<?php echo $star['show_stars'];?>.png">
				<?php } ?>
			       </div>
			    </li>
			 </ul>
	      </div>
	   </div>
	</div>

	<?php
	die();		
	}
	//-----------------------------------------------------------------------------------
	//Finalazie Teacher------------------------------------------------------------------
	public function finalazie_teacher(){
	$query_model = new ModelQuery();
	$sales_rep_id = get_current_user_id();
	$query_id=$_POST['query_id'];
	$teacher_id=$_POST['teacher_id'];
	$student_id=$_POST['student_id'];
	$plan='';
	$user = new WP_User( $teacher_id );
	$role='';
	if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
	    foreach ( $user->roles as $role );	
	}
	
	$exist=$query_model->teacher_exist($teacher_id);
	if($role=='teacher' ||  $role=='institution' && $exist==1){
	$res=$query_model->finalazie_teacher($query_id,$student_id,$teacher_id,$sales_rep_id);

	//$log = "The Query (ID - ".$query_id.") status has been changed from HOT to BAKED";
	$log = "The status of the Query was changed from <span class='f_status'>HOT</span> to <span class='t_status'>BAKED</span>";
	$is_view = '0';
	$member_id = '';
	$activity_result = $query_model->add_activity_log($query_id,$member_id,$log,$is_view);
	
		if($role=='teacher'){
		$profile_slug=$query_model->get_teacher_profile_slug($teacher_id);
		$plan=$query_model->get_teacher_plan($teacher_id);
		?>
		<div class="h1_heading overflow_hidden">
		<h5 class="admin_menu_background top_radius">Teacher ID - <?php echo $teacher_id; ?></h5>
		<p class="plan_ab_otp">
			<strong>Cycle:</strong>
			<select t-type="<?php echo $role; ?>" ti-id="<?php echo $teacher_id; ?>" class="blue_border border_radius_12 teacher_plan">
			 <option <?php if($plan=='Plan A'){echo "selected";} ?> value="Plan A">Plan A</option>
			 <option <?php if($plan=='Plan B'){echo "selected";} ?> value="Plan B">Plan B</option>
			 <option <?php if($plan=='Plan C'){echo "selected";} ?> value="Plan C">Plan C</option>
			 <option <?php if($plan=='Plan D'){echo "selected";} ?> value="Plan D">Plan D</option>
			 <option <?php if($plan=='Plan OTP'){echo "selected";} ?> value="Plan OTP">Plan OTP</option>
		       </select> 
		   </p>
		<ul class="finalized_teacher_profile_details">
		    <li><a href="<?= home_url(); ?>/teacher/<?php echo $profile_slug; ?>" target="_blank" class="admin_menu_background color_white text-center border_radius_12">View Profile</a></li>
		    <li><a href="#" id="change_final_teacher" class="admin_menu_background color_white text-center border_radius_12">Change</a></li>
		</ul>
	        </div>
		<?php
		}
		else{
		$profile_slug=$query_model->get_institute_profile_slug($teacher_id);
		$plan='';
		?>
		<div class="h1_heading overflow_hidden">
		<h5 class="admin_menu_background top_radius">Teacher ID - <?php echo $teacher_id; ?></h5>
		<ul class="finalized_teacher_profile_details">
			<li><a href="<?= home_url(); ?>/institute/<?php echo $profile_slug; ?>" target="_blank" class="admin_menu_background color_white text-center border_radius_12">View Profile</a></li>
		    <li><a href="#" id="change_final_teacher" class="admin_menu_background color_white text-center border_radius_12">Change</a></li>
		</ul>
	        </div>
		<?php
		}
	}
	else{
	echo "NotExist";	
	}
	
	die();		
	}
	//-----------------------------------------------------------------------------------
	//Update Inbound Payment-------------------------------------------------------------
	public function update_inbound_payment(){
	
	$user_id = get_current_user_id();
	$user = new WP_User( $user_id );
	$user_role = $user->roles[0];
	//echo $user_role; die();

	$query_model = new ModelQuery();
	//$sales_rep_id = get_current_user_id();
	$query_id=$_POST['query_id'];
	$sales_rep_id = $query_model->get_current_sales_rep_id($query_id);
	$teacher_id=$_POST['teacher_id'];
	$student_id=$_POST['student_id'];
	$inbound_payment_type=$_POST['inbound_payment_type'];
	$payment_type_details=$_POST['payment_type_details'];
	$inbound_payment_fee=$_POST['inbound_payment_fee'];
	$exp_date_payment=date('Y-m-d',strtotime($_POST['exp_date_payment']));
	$fees=$_POST['fees'];
	$inbound_payment_cycle=$_POST['inbound_payment_cycle'];
	$vendor_invoice_number=@$_POST['vendor_invoice_number'];
	$cash_trans=$_POST['cash_trans'];
	$res=$query_model->update_inbound_payment($query_id,$student_id,$teacher_id,$sales_rep_id,$inbound_payment_type,$payment_type_details,$inbound_payment_fee,$exp_date_payment,$fees,$inbound_payment_cycle,$vendor_invoice_number,$cash_trans);

	if($user_role == "sales"){

		$p_update = '1';
		$p_u_notification = 'Payment details updated against Query ID - '.$query_id;
		$p_r_notification = '';
		$p_t_notification = '';
		$p_received = '0';
		$p_transfer = '0';

		$member_id = '';
		$is_view = '0';	
		//$log = "The Query (ID - ".$query_id.") status has been changed from BAKED to PAY_EXP_DT";
		$log = "TThe status of the Query was changed from <span class='f_status'>BAKED</span> to <span class='t_status'>PAY_EXP_DT</span>";
		//$res_notification = $query_model->payment_inbound_notification($user_role,$query_id,$sales_rep_id,$p_u_notification,$p_r_notification,$p_t_notification,$p_update,$p_received,$p_transfer);
	}
	else if($user_role == "finance"){

		$p_received = '1';
		$p_r_notification = 'Payment received against Query ID - '.$query_id;

		$p_u_notification = '';
		$p_t_notification = '';
		$p_update = '';
		$p_transfer = '';

		$member_id = '';
		$is_view = '0';	
		//$log = "The Query (ID - ".$query_id.") status has been changed from PAY_EXP_DT to PAY_REC";
		$log = "The status of the Query was changed from <span class='f_status'>PAY_EXP_DT</span> to <span class='t_status'>PAY_REC</span>";
		//$res_notification = $query_model->payment_inbound_notification($user_role,$query_id,$sales_rep_id,$p_u_notification,$p_r_notification,$p_t_notification,$p_update,$p_received,$p_transfer);
	}
	if($user_role == "administrator"){
	 	if($cash_trans == '1')
	 	{
	 		$p_update = '1';
	 		$p_received = '1';
	 		$p_u_notification = 'Payment details updated against Query ID - '.$query_id;
	 		$p_r_notification = 'Payment received against Query ID - '.$query_id;

			//$log = "The Query (ID - ".$query_id.") status has been changed from BAKED to PAY_REC";
			$log = "The status of the Query was changed from <span class='f_status'>BAKED</span> to <span class='t_status'>PAY_REC</span>";

	 	}
	 	else
	 	{
	 		$p_update = '1';
	 		$p_received = '0';
	 		//$log = "The Query (ID - ".$query_id.") status has been changed from BAKED to PAY_EXP_DT";
	 		$log = "The status of the Query was changed from <span class='f_status'>BAKED</span> to <span class='t_status'>PAY_EXP_DT</span>";
	 		$p_u_notification = 'Payment details updated against Query ID - '.$query_id;
	 		$p_r_notification = '';
	 	}
	 	$member_id = '';
	 	$is_view = '0';
	 	$p_transfer = '0';
	 	$p_t_notification = '';
	 }


	$res_notification = $query_model->payment_inbound_notification($user_role,$query_id,$sales_rep_id,$p_u_notification,$p_r_notification,$p_t_notification,$p_update,$p_received,$p_transfer);

	/******************* Activity Log For Status Change *********************/
	
	$activity_result = $query_model->add_activity_log($query_id,$member_id,$log,$is_view);
	/******************* End of Activity Log For Status Change *********************/

	echo date('Y M d h:i a',strtotime($res));
	die();
	}
	//-----------------------------------------------------------------------------------
	//Update Outbound Payment------------------------------------------------------------
	public function update_outbound_payment(){
	$query_model = new ModelQuery();
	$finance_rep_id = get_current_user_id();
	$query_id=$_POST['query_id'];
	$teacher_id=$_POST['teacher_id'];
	$student_id=$_POST['student_id'];
	$outbound_payment_type=$_POST['outbound_payment_type'];
	$payment_type_details=$_POST['payment_type_details'];
	@$distri_name=$_POST['distri_name'];
	@$paytm_mob_no=$_POST['paytm_mob_no'];
	@$bank_count=$_POST['bank_count'];
	@$cash_receive=$_POST['cash_receive'];
	@$customer_invoice_number=$_POST['customer_invoice_number'];
	$our_share=$_POST['our_share'];
	$teacher_share=$_POST['teacher_share'];
	$gst=$_POST['gst'];
	
	$bank_data=urldecode($_POST['bank_data']);
	
	$res=$query_model->update_outbound_payment($query_id,$teacher_id,$student_id,$finance_rep_id,$outbound_payment_type,$payment_type_details,$distri_name,$paytm_mob_no,$bank_count,$cash_receive,$customer_invoice_number,$our_share,$teacher_share,$bank_data,$gst);

	$p_transfer = '1';
	$p_t_notification = 'Payment transferred against Query ID - '.$query_id;
	$res_notification = $query_model->payment_outbound_notification($query_id,$p_t_notification,$p_transfer);

	/******************* Activity Log For Status Change *********************/
	$member_id = '';
	$is_view = '0';	
	//$log = "The Query (ID - ".$query_id.") status has been change from PAY_REC to PAID_TO";
	$log = "The status of the Query was changed from <span class='f_status'>PAY_REC</span> to <span class='t_status'>PAID_TO</span>";
	$activity_result = $query_model->add_activity_log($query_id,$member_id,$log,$is_view);
	/******************* End of Activity Log For Status Change *********************/

	echo date('Y M d h:i a',strtotime($res));
	die();	
	}
	//-----------------------------------------------------------------------------------
	//Update query plan------------------------------------------------------------------
	public function update_query_plan(){
	$query_model = new ModelQuery();
	$query_id=$_POST['query_id'];
	$query_plan=$_POST['query_plan'];
	echo $res=$query_model->update_query_plan($query_id,$query_plan);
	die();	
	}
	//-----------------------------------------------------------------------------------
	//Update query fast pass status------------------------------------------------------
	public function update_query_fast_pass(){
	$query_model = new ModelQuery();
	$query_id=$_POST['query_id'];
	$fast_pass=$_POST['fast_pass'];
	echo $res=$query_model->update_query_fast_pass($query_id,$fast_pass);
	die();	
	}
	//-----------------------------------------------------------------------------------
	//Update teacher plan----------------------------------------------------------------
	public function update_teacher_ins_plan(){
	$query_model = new ModelQuery();
	$t_type=$_POST['t_type'];
	$ti_id=$_POST['ti_id'];
	$teacher_plan=$_POST['teacher_plan'];
	$res=$query_model->update_teacher_ins_plan($t_type,$ti_id,$teacher_plan);
	echo 'success';
	die();		
	}
	//-----------------------------------------------------------------------------------

	/***************** SMS Code By Arghya*******************/

	public function get_sms_template_text(){
		$query_model = new ModelQuery();
		$template_id = $_POST['t_id'];
		$result=$query_model->get_sms_template_text($template_id);
		echo $result[0]->template_text;
		die();
	}

	public function send_sms_to_member(){
		$phone = $_POST['member_phone'];
		$message = $_POST['message'];
		$member_id = $_POST['member_id'];
		//send sms--------------------------
		$api_key = '559E5AAED983CA';
		$from = 'PDAGOG';
		$sms_text = urlencode($message);
		//$student_phone='9674775108';
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, "http://www.sambsms.com/app/smsapi/index.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=7&type=text&contacts=".$phone."&senderid=".$from."&msg=".$sms_text);
		$response = curl_exec($ch);
		curl_close($ch);

		/*******************06/01/2018**********************/

		$query_id = $_POST['query_id'];
		$log = $_POST['log'];
		$is_view = '1';
		$query_model = new ModelQuery();
		$result_activity = $query_model->add_activity_log($query_id,$member_id,$log,$is_view,$message);

		/*******************06/01/2018**********************/
		echo 'success';
		die();
	}

	public function get_activity_logs_by_id(){
		$id = $_POST['id'];
		$query_model = new ModelQuery();
		//$query_id=$_POST['query_id'];
		//$note=$_POST['note'];
		$res=$query_model->get_activity_logs_by_id($id);
		echo $res[0]->activity_log;
		die();	
	}
	
	public function get_activity_content_by_id(){
		$id = $_POST['id'];
		$query_model = new ModelQuery();
		//$query_id=$_POST['query_id'];
		//$note=$_POST['note'];
		$res=$query_model->get_activity_logs_by_id($id);
		echo $res[0]->activity_content;
		die();	
	}

	//-----------------------------------------------------------------------------------
	//Add Note in Activy Log------------------------------------------------------
	public function add_activity_note(){
	$query_model = new ModelQuery();
	$query_id=$_POST['query_id'];
	$note=$_POST['note'];
	$res=$query_model->add_activity_note($query_id,$note);
	echo "success";
	die();	
	}
	//-----------------------------------------------------------------------------------
	//Update Note in Activy Log------------------------------------------------------
	public function update_activity_note(){
	$query_model = new ModelQuery();
	$query_id=$_POST['query_id'];
	$note_id = $_POST['note_id'];
	$note=$_POST['note'];
	$res=$query_model->update_activity_note($note_id,$note);
	echo "success";
	die();	
	}



	public function get_admin_ajax_notification(){
  $query_model = new ModelQuery();
  //$teacher_id=$_SESSION['member_id'];
  $a_notification = $query_model->get_all_admin_notification();
      usort($a_notification, function ($item1, $item2) {
          if ($item1['created_at'] == $item2['created_at']) return 0;
          return $item1['created_at'] > $item2['created_at'] ? -1 : 1;
      });
  
  if(count($a_notification)>0){ foreach($a_notification AS $noti){ 
  ?>
        <li id="noti<?php echo $noti->id; ?>">
            <div class="bell_icon"><i class="fa fa-bell-o" aria-hidden="true"></i></div>
            <div class="notification_details_para">
                <p><?php echo $noti['notification']; ?></p>
            </div>
            <!--<div class="navigation_delate">
                <button class="delete_noti" idd="<?php echo $noti->id; ?>" type="button">X</button>
            </div>-->
          </li>
      <?php } }else{?>
          <li>
              <div class="bell_icon"><i class="fa fa-bell-o" aria-hidden="true"></i></div>
              <div class="notification_details_para">
                  <p>You have no notification to show!</p>
              </div>
              <div class="navigation_delate">
                  <!--<button type="button">X</button>-->
              </div>
          </li>
      <?php 
      }
      die();
	}

	public function get_admin_ajax_notification_count(){
    $query_model = new ModelQuery();
    echo $notification_count=$query_model->get_admin_count_notification();
    die();
	}

	public function get_finance_ajax_notification_count(){
		$query_model = new ModelQuery();
		
		$a_payment_finance_notification = $query_model->payment_update_count_notification();
		echo $notification_count=$a_payment_finance_notification['count'];
    	die();
	}

	public function get_sales_ajax_notification_count(){
		$query_model = new ModelQuery();
		$user_id = get_current_user_id();
		
		$a_receive = $query_model->payment_received_count_notification($user_id);
		$a_transfer = $query_model->payment_transfer_count_notification($user_id);
		$notification_count = $a_receive['count'] + $a_transfer['count'];
		
		echo $notification_count.":".$a_receive['count'].":".$a_transfer['count'];
    	die();
	}

	/******************** Read Payment Notification By Sales **************************/
	public function read_receive_notification_by_sales(){
		$query_model = new ModelQuery();
		$user_id = get_current_user_id();

		$query_model->read_receive_notification_by_sales($user_id);
		die();
	}

	public function read_transfer_notification_by_sales(){
		$query_model = new ModelQuery();
		$user_id = get_current_user_id();

		$query_model->read_transfer_notification_by_sales($user_id);
		die();
	}
	/******************** End Of Read Payment Notification By Sales **************************/

	/******************** Read Payment Notification By Finance **************************/

	public function read_update_notification_by_finance(){
		$query_model = new ModelQuery();
		$user_id = get_current_user_id();

		$query_model->read_update_notification_by_finance();
		die();
	}

	public function read_notification_by_admin(){
		$query_model = new ModelQuery();
		$query_model->read_notification_by_admin();
		die();
	}

	public function change_lead_source(){
		$query_model = new ModelQuery();
		//$user_id = get_current_user_id();
		$query_id = $_POST['query_id'];
		$lead = $_POST['lead'];
		$query_model->change_lead_source($query_id,$lead);
		die();
	}
	
	
	//Get Teacher landing query--------------------------------------
	public function get_land_query(){
	$query_model = new ModelQuery();
	$query_id=$_POST['query_id'];
	$querys=$query_model->get_query_details_view($query_id);
	$localities = $query_model->get_q_locality($querys[0]->localities);
	$course_type_id_arr=$query_model->course_type_id_arr($querys[0]->subject_name_id);
	foreach($querys AS $query){
	?>
	        <div class="modal-content student_searching_details_model">
                    <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     	<h2><?php echo $query->subject_name; ?> Home-Tutor In</h2>
                    </div>
                    <form action="" method="post">
                      <div class="modal-body">
                         <div class="row">
                         	<div class="col-xs-12 col-md-6 col-sm-6">
                            	  <?php if(in_array(2,$course_type_id_arr) && !in_array(1,$course_type_id_arr)){ ?>
				<p>Age : <span><?php echo $query->age; ?> Years</span></p>
				<?php }elseif(in_array(1,$course_type_id_arr) && !in_array(2,$course_type_id_arr)){ ?>
				<p>Year/Standard: <span><?php echo $query->standard; ?></span></p>
				<p>Board/University : <span><?php echo $query->academic_board; ?></span></p>
				<?php }elseif(in_array(1,$course_type_id_arr) && in_array(2,$course_type_id_arr)){ ?>
				<p>Year/Standard: <span><?php echo $query->standard; ?></span></p>
				<p>Board/University : <span><?php echo $query->academic_board; ?></span></p>
				 <p>Age : <span><?php echo $query->age; ?> Years</span></p>
                           <?php } ?>
                           		<!--<p>Year/Standard: <strong>Class 2</strong></p>
                                <p>Board/University: <strong>&nbsp;</strong></p>-->
                                <div class="student_search_for_locality">
                                	<p>Locality Type:</p>
                                    <ul class="area">
					 <?php if($query->myhome==1){?>
					<li><button type="button">My Home</button></li>
					<?php } ?>
					<?php if($query->tutorhome==1){  ?>
					<li><button type="button">Tutor's Home</button></li>
					<?php } ?>
					<?php if($query->institute==1){ ?>
					<li><button type="button">Institue</button></li>
					<?php } ?>
                                  	</ul>
                                </div>
                                <div class="student_search_for_locality">
                                	<p>Preferred Localities:</p>
                                    <ul class="area">
					 <?php foreach($localities AS $loc){ ?>
                                        <li><button type="button"><?php echo $loc; ?></button></li>
                                       	<?php } ?>
                                  	</ul>
                                </div>
                                <p>Date of commencement:  <strong><?php echo date_format(date_create($query->start_date), "d/m/Y"); ?></strong></p>
                            </div>
                            <div class="col-xs-12 col-md-3 col-sm-3 student_search_for_preference">
                            	<h5>Preferences: </h5>
                                <p class="preference">
                                	<?php echo trim($query->requirement); ?>
                                </p>
                            </div>
                            <div class="col-xs-12 col-md-3 col-sm-3">
                            	<ul class="side_right_menu text-uppercase text-center">
                                     <li><a href="javascript:void(0);" class="collapse_query">Collapse</a></li>
                                     <!--<li><a href="<?php echo home_url().'/login/?nexturl='; ?>">Save this</a></li>-->
                                     <li><a href="javascript:void(0);" class="sinterest">Show interest</a></li>
                                     <!--<li><a href="<?php echo home_url().'/login/?nexturl='; ?>">Refer someone</a></li>-->
                                </ul>
                            </div>
                         </div>
                     </div>
                     <!--<div class="modal-footer margin_top_10 margin_bottom_10">
                                      
                 	</div>-->
                  </form>
             </div>
		<?php } ?>
             <script>
		$(function(){
					$('.preference').slimScroll({
						height: '180px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2a3543',
						opacity: 1,
						wheelStep: 50,
						distance: '0',
						wheelStep: 50
					});
				});
		$(document).on("click",".collapse_query",function(){
			$("#student_searching_for").modal("hide");
		})
	     </script>
	<?php
	die();	
	}
	//---------------------------------------------------------------

	public function add_email_subscriber(){
		$query_model = new ModelQuery();
		$email_id=$_POST['email_id'];
		$query=$query_model->add_email_subscriber($email_id);
		die();
	}
}