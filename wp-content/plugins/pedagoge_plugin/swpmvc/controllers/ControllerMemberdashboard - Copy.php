<?php
session_start();
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerMemberdashboard extends ControllerMaster implements ControllerMasterInterface {
	
	
	public function __construct() {
		// nothing to do here.
		// Only master controller gets autoconstructed.		 
	}
	
	public function fn_construct_class() {
		//Initialize Master Controller
		parent::__construct(); 
		
		//Page Title
		$this->title = 'Settings Page:';
		
		//You can load your custom assets (css/js) specific to this page. 
		//Master page has already loaded all the required scripts for a blank page.
		$this->fn_load_scripts(); 
		
		//Set this variable to define body class of your page.
		$this->body_class .= ' page-register login-alt page-header-fixed';
		
		//Register variables required in the global scope. 
		//Variables are stored in $this->app_data array variable.
		$this->fn_register_common_variables();	

			
	}
	
	private function fn_load_scripts() {
		//Load Styles and Scripts for Settings Page and store in 
		// You need to use defined handle of an asset for identification and loading from the registry.		
		//$this->header_js['modernizr'] = $registered_scripts['modernizr']."?ver=".$this->fixed_version;
		
		// You may skip the registry and directly load an asset by its url.
		//$this->app_js['pedagoge_settings'] = PEDAGOGE_ASSETS_URL."/js/pedagoge_settings.js?ver=0.1";
		
	}
	
	//You need to load the header. You may define your own header, in this example an existing header is being resued.
	public function fn_get_header() {
		return $this->load_view('memberdashboard/header');		
	}
	
	//You need to load the footer. You may define your own footer, in this example an existing footer is being resued.
	public function fn_get_footer() {
		return $this->load_view('memberdashboard/footer');
	}
	
	//Load the relevant content section of the page.
	public function fn_get_content() {
				
		//Template vars will be available to the template file.
		$template_vars = array(); 
		
		//You may include as many variable in the array as you want.
		//$template_vars['example_string'] = 'Hello World!';
		//$template_vars['example_number'] = 4;
		
		//$content = $this->load_view('settings/index', $template_vars);		
		$content = $this->load_view('memberdashboard/index');	
		return $content;
	}
	
	/**
	 * Function to register common variables to global application variables registry 
	 */	
	private function fn_register_common_variables() {
		//Available for the page at global scope.
		$this->app_data['global_example_string'] = 'example string';
		
		//If you wish to include a dynamic inline javascript code then use the variable 'inline_js' available in the $app_data variable.
		// This variable is a string, so append your script string to it. e.g.
		$this->app_data['inline_js'] .= '<script type="text/javascript">console.log("Testing");</script>';
	}
	
	// Example of Ajax request processing
	
	public static function filter_data_ajax() {
		
		$return_message = array ();
		
		$selected_sub=$_POST['selected_sub'];
		$selected_sub=explode(",",$selected_sub);
		$str="";
		$num_sub=0;
		foreach($selected_sub as $selsub) {
			$str.="'".$selsub."',";
				$num_sub++;
		}
		
		
		
		$str=rtrim($str,",");
		
		global $wpdb;
		
		
		
		$res = $wpdb->get_results( "SELECT distinct course_type_id from pdg_course_category where subject_name_id in (select subject_name_id from pdg_subject_name where subject_name in ($str))" );
		foreach ( $res as $sub ) $subselect.=$sub->course_type_id.',';
		$return_message['selsub'] = $subselect;
		$return_message['selectedsub'] = $selected_sub;
		$return_message['userid']= $_SESSION['member_id'];
		
		$return_message['numsub'] = $num_sub;
		
		
		
		echo json_encode($return_message);
		die();
		
	}
	
	public static function subject_select_ajax(){
		
		$_SESSION['selectedsub']=$_POST['selectedsub'];
		
		die();
	}
	
	public static function getsamedetails_ajax(){
		
		global $wpdb;
		$userid=$_SESSION['member_id'];
		$str='';
		$sql="SELECT * FROM pdg_query WHERE student_id=$userid order by id DESC limit 1";
		
		$ret = $wpdb->get_results( $sql );
		$myformname=$_POST['data'];
		
		//echo $res[0];
		
		////echo json_encode($res);
			
		//echo json_encode($ret);
		foreach($ret[0] AS  $key => $val){
		$str=$str.$val.'|';	
		}
		$str.=$myformname;
		echo $str;
		die(); 
		
	}
	
	
	
	public static function submit_data_ajax(){
		
		global $wpdb;
		
		$retval = $_POST['data'];
		
		//echo json_encode($return_message);
		//echo $_POST['school'];
		/*$retval=json_decode(json_encode($_POST),true);
	

	
	

		$retval=json_decode(json_encode($retval['data']),true);
		
		echo $retval;*/
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		
		

	//echo $data['school'];
		
		
		$userid=$_SESSION['member_id'];
		
		$subject=$data['mysubject'];
		$school=$data['school'];
		

		$board=$data['board'];
		$standard=$data['standard'];
		
		
		if (isset($data['myhome']))
		$myhome = 1;
		else
		$myhome = 0;
		
		
		
		if (isset($data['tutorhome']))
		$tutorhome = 1;
		else
		$tutorhome = 0;
		
		
		
		if (isset($data['institute']))
		

		$institute = 1;
		

		else
		$institute = 0;
		
		
		
		$localities=$data['localities'];
		$start_date=date("Y-m-d", strtotime($data['start_date']));
		

		$requirement=trim($data['requirement']);
		
		$sql="select * from pdg_query where student_id=$userid and subject='$subject' order by id DESC limit 1";
		
		$recs=$wpdb->get_results($sql);
		
		foreach ( $recs as $rec ) $myid=$rec->id;
		
		$rowcount = $wpdb->num_rows;
		
		
		
		if($rowcount>0){
			
			$sql="update pdg_query set school='$school', board='$board', standard='$standard', 

	myhome=$myhome, tutorhome=$tutorhome, institute=$institute, 

	localities='$localities', start_date='$start_date', requirement='$requirement' where student_id=$userid and subject='$subject' and id=$myid";
	
		
			
			
		}else{
			
			
			$sql="select subject_name_id from pdg_subject_name where subject_name='$subject'";
			$recs=$wpdb->get_results($sql);
		
			foreach ( $recs as $rec ) $subject_id=$rec->subject_name_id;
		
		
			
		
		$sql="INSERT INTO pdg_query(student_id, subject, school, board, standard, 

	myhome, tutorhome, institute, 

	localities, start_date, requirement) VALUES ($userid,'$subject_id','$school','$board','$standard',$myhome,$tutorhome,

	$institute,'$localities','$start_date','$requirement')";
	
		}
		
		
		
		$wpdb->query($sql);
			
		//wp_reset_query();	

//------------------------------find email name mobile----------------------------------------------
		
		
		$str='';
		$sql="select a.display_name, a.user_email, b.mobile_no from wp_users a inner join pdg_user_info b on a.id=b.user_id and a.id=$userid";
		
		$ret = $wpdb->get_results( $sql );
			
		foreach($ret[0] AS  $key => $val){
			$str=$str.$val.'|';	
		}
		$str.=$userid;
		
		echo $str;			
			
			
			
		die();

	
		
	}
	
	public static function submit_data_ajaxna(){
		
		global $wpdb;
		
		$retval = $_POST['data'];
		
		//echo json_encode($return_message);
		//echo $_POST['school'];
		/*$retval=json_decode(json_encode($_POST),true);
	

	
	

		$retval=json_decode(json_encode($retval['data']),true);
		
		echo $retval;*/
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		
		

	//echo $data['school'];
		
		
		$userid=$_SESSION['member_id'];
		
		$subject=$data['mysubject'];
		
		$age=$data['age'];
		
		if (isset($data['myhome']))
		$myhome = 1;
		else
		$myhome = 0;
		
		
		
		if (isset($data['tutorhome']))
		$tutorhome = 1;
		else
		$tutorhome = 0;
		
		
		
		if (isset($data['institute']))
		

		$institute = 1;
		

		else
		$institute = 0;
		
		
		
		$localities=$data['localities'];
		$start_date=date("Y-m-d", strtotime($data['start_date']));
		

		$requirement=trim($data['requirement']);
		
		$sql="select subject_name_id from pdg_subject_name where subject_name='$subject'";
			$recs=$wpdb->get_results($sql);
		
			foreach ( $recs as $rec ) $subject_id=$rec->subject_name_id;
		
		$sql="INSERT INTO pdg_query(student_id, subject,  

	myhome, tutorhome, institute, 

	localities, start_date, requirement,age) VALUES ($userid,'$subject_id',$myhome,$tutorhome,

	$institute,'$localities','$start_date','$requirement',$age)";
		
		$wpdb->query($sql);
			
		//wp_reset_query();	

			
		//------------------------------find email name mobile----------------------------------------------
		
		
		$str='';
		$sql="select a.display_name, a.user_email, b.mobile_no from wp_users a inner join pdg_user_info b on a.id=b.user_id and a.id=$userid";
		
		$ret = $wpdb->get_results( $sql );
			
		foreach($ret[0] AS  $key => $val){
			$str=$str.$val.'|';	
		}
		$str.=$userid;
		
		echo $str;	
			
		die();

	
		
	}
	
	public static function submit_data_ajaxgetsubtypenaboth(){
		
		global $wpdb;
		
		$retval=$_POST['data'];
		
		$sql="SELECT distinct course_type_id from pdg_course_category where subject_name_id in (select subject_name_id from pdg_subject_name where subject_name in ('$retval'))";
		
		
		
		$res = $wpdb->get_results( $sql );
		foreach ( $res as $sub ) $subselectid.=$sub->course_type_id;

		
		echo trim($subselectid);
		
		
		die;
		
	}
	
	
	public static function submit_data_ajaxnaboth(){
		
		global $wpdb;
		
		$retval = $_POST['data'];
		
		//echo json_encode($return_message);
		//echo $_POST['school'];
		/*$retval=json_decode(json_encode($_POST),true);
	

	
	

		$retval=json_decode(json_encode($retval['data']),true);
		
		echo $retval;*/
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		
		

	//echo $data['school'];
		
		
		$userid=$_SESSION['member_id'];
		
		$subject=$data['mysubject'];
		$school=$data['school'];
		

		$board=$data['board'];
		$standard=$data['standard'];
		
		
		if (isset($data['myhome']))
		$myhome = 1;
		else
		$myhome = 0;
		
		
		
		if (isset($data['tutorhome']))
		$tutorhome = 1;
		else
		$tutorhome = 0;
		
		
		
		if (isset($data['institute']))
		

		$institute = 1;
		

		else
		$institute = 0;
		
		
		
		$localities=$data['localities'];
		$start_date=date("Y-m-d", strtotime($data['start_date']));
		

		$requirement=trim($data['requirement']);
		$age=$data['age'];
		
		$sql="select subject_name_id from pdg_subject_name where subject_name='$subject'";
			$recs=$wpdb->get_results($sql);
		
			foreach ( $recs as $rec ) $subject_id=$rec->subject_name_id;
		
		
		$sql="INSERT INTO pdg_query(student_id, subject, school, board, standard, 

	myhome, tutorhome, institute, 

	localities, start_date, requirement, age) VALUES ($userid,'$subject_id','$school','$board','$standard',$myhome,$tutorhome,

	$institute,'$localities','$start_date','$requirement','$age')";
	
		
		
		$wpdb->query($sql);
			
		//wp_reset_query();	


//------------------------------find email name mobile----------------------------------------------
		
		
		$str='';
		$sql="select a.display_name, a.user_email, b.mobile_no from wp_users a inner join pdg_user_info b on a.id=b.user_id and a.id=$userid";
		
		$ret = $wpdb->get_results( $sql );
			
		foreach($ret[0] AS  $key => $val){
			$str=$str.$val.'|';	
		}
		$str.=$userid;
		
		echo $str;			
			
			
			
		die();

	
		
	}
	
	public static function submit_data_ajaxaftersubmitmodal(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		$studname= $data['studname'];
		$email=$data['emailid'];
		$phone=$data['phonenum'];
		$userid=$data['myuserid'];
		$sql="update wp_users set display_name='$studname', user_email='$email' where id=$userid";
		$wpdb->query($sql);
		$sql="update pdg_user_info set mobile_no='$phone' where user_id=$userid";
		$wpdb->query($sql);
		echo $sql;
		
		
		die();
		
	}
	
	public static function update_data_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		
		

	//echo $data['school'];
		
		
		$userid=$_SESSION['member_id'];
		
		$subject=$data['mysubject'];
		$school=$data['school'];
		

		$board=$data['board'];
		$standard=$data['standard'];
		
		
		if (isset($data['myhome']))
		$myhome = 1;
		else
		$myhome = 0;
		
		
		
		if (isset($data['tutorhome']))
		$tutorhome = 1;
		else
		$tutorhome = 0;
		
		
		
		if (isset($data['institute']))
		

		$institute = 1;
		

		else
		$institute = 0;
		
		
		
		$localities=$data['localities'];
		$start_date=date("Y-m-d", strtotime($data['start_date']));
		

		$requirement=trim($data['requirement']);
		
		$age=$data['age'];
		
		$myid=$data['mysubid'];
		
		
			
			$sql="update pdg_query set school='$school', board='$board', standard='$standard', 

	myhome=$myhome, tutorhome=$tutorhome, institute=$institute, 

	localities='$localities', start_date='$start_date', requirement='$requirement', age='$age' where id=$myid";
	
		
	
		
		$wpdb->query($sql);
		
		//echo $sql;
		
		die();
		
	}
	
	public static function update_archive_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$subid= $data['mysubid'];
		
		$sql="update pdg_query set archived='1' where id=$subid";
		
		$wpdb->query($sql);
		
		
		
		die();
		
	}
	
	public static function delete_unarchive_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$subid= $data['mysubid'];
		
		$sql="delete from pdg_query where id=$subid";
		
		$wpdb->query($sql);
		
		
		
		die();
		
	}
	public static function modal_editsubjectselect_ajax(){
		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$subid= $data['mysubid'];
		
		
		
		?>
			<div class="modal fade" id="editsubjectselectarchive" role="dialog">
                                <div class="modal-dialog modal-dialog-next">
		<div class="modal-content blue_border">
                                    <div class="modal-header">
                                      <ul class="acadamic_subject_form">
									  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      	<li>&nbsp;</li>	
										
										<?php
											global $wpdb;
											$sql="select * from pdg_query where id='$subid'";
											
											$recsub=$wpdb->get_results($sql);		
											foreach ( $recsub as $recsb ) $subject_id=$recsb->subject;
											
											$sql="select subject_name from pdg_subject_name where 

subject_name_id='$subject_id'";
											$recsub=$wpdb->get_results($sql);		
											foreach ( $recsub as $recsb ) $subject_name=$recsb->subject_name;
											
										 ?>



										
                                      	<li class="active_subject"><a href="#"><?= $subject_name; ?></a></li>
                                        <li><!--<a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelectNext">History <i 

class="fa fa-caret-right" aria-hidden="true"></i></a>--></li>
                                      </ul>
                                    </div>
                                    <form action="" method="post" id="frmeditsubjectselectarchive">
									
									<input type="hidden" value="<?= $subject_id ?>" id="subject_id" name="subject_id">
									<input type="hidden" value="<?= $subid ?>" id="sub_id" name="sub_id">
										<?php
							
										global $wpdb;
										
										$res = $wpdb->get_results( "SELECT distinct course_type_id from pdg_course_category 

where subject_name_id ='$subject_id'" ); 
										
										
										
										$course_type=$res[0]->course_type_id;
										
										
										
										
										
										$sql="select * from pdg_query where id='$subid'" ;
										$recs=$wpdb->get_results($sql);						
									
										
										
										foreach ( $recs as $rec ) {
							
							?>
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
									<input type="hidden" value="<?= $rec->id ?>" id="subid" name="subid">
									<?php 
									
									
									
									if($course_type==2){ ?>
									
									 <div class="col-sm-12 subject_type">
                                     	<input placeholder="Age" name="age" id="age" class="non_grid_input black_border non_grid_input black_border" type="text" 

value="<?= $rec->age ?>">
                                     </div>
									
									<?php }else if($course_type==1){ ?>
									
                                     <div class="col-sm-12 subject_type">
									 
                                     
										
										<select name="school" id="school" class="non_grid_input black_border form-control" >
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_schools ORDER BY school_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
												$selected=($rec->school == $r->school_id)? "selected='selected'" : "";
										?>
										<option <?= $selected ?> value="<?= $r->school_id ?>"><?= $r->school_name ?></option>
										<?php
											}
										?>
										</select>
										
										<select name="board" id="board" class="non_grid_input black_border form-control">
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_academic_board ORDER BY academic_board_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
												$selected=($rec->board == $r->academic_board_id)? "selected='selected'" 

: "";
										?>
										<option <?= $selected ?> value="<?= $r->academic_board_id ?>"><?= $r->academic_board ?></option>
										<?php
											}
										?>
										</select>
										
										
										
                                        <!--<input name="standard" id="standard" value="<?= $rec->standard ?>" placeholder="Standard/year" class="non_grid_input 

black_border"   type="text">-->


									<select name="standard" id="standard" class="non_grid_input black_border form-control">
										<option value="Select Standard">STANDARD/YEAR</option>
										<?php
											global $wpdb;
											$SQL = "SELECT subject_type FROM pdg_subject_type ORDER BY subject_type_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
												$selected=($rec->standard == $r->subject_type)? "selected='selected'" : "";
										?>
										<option <?= $selected ?> value="<?= $r->subject_type ?>"><?= $r->subject_type ?></option>
										<?php
											}
										?>
										</select>









                                     </div>
									 
									<?php }else{ ?>
									
										<div class="col-sm-12 subject_type" style="display:none" >
									 <input type="hidden" value="<?= $rec->id ?>" id="subid" name="subid">
                                     	<input name="school" id="school"  value="" placeholder="School/college" class="non_grid_input black_border"   type="text">
                                        <input name="board" id="board" value="" placeholder="Board/university" class="non_grid_input black_border"   type="text">
                                        <input name="standard" id="standard" value="" placeholder="Standard/year" class="non_grid_input black_border"   type="text">
                                     </div>
									
									
									<?php } ?>
                                     
                                     <div class="col-sm-12 subject_type">
                                     	<p class="text-center option_heading">Select your preferred option</p>
                                        <ul class="place_option">
                                        	<li><input <?php echo $rec->myhome? 'checked' : '' ?> type="checkbox" name="myhome" id="myhome"><label for="MyHome">My 

Home</label></li>
                                            <li><input <?php echo $rec->tutorhome? 'checked' : '' ?> type="checkbox" name="tutorhome" id="tutorhome"><label 

for="TutorHome">Tutor's Home</label></li>
                                            <li><input  <?php echo $rec->institute? 'checked' : '' ?> type="checkbox" name="institute" id="institute"><label 

for="Institute">Institute</label></li>
                                        </ul>
                                       
									   <input type="hidden" name="localities" id="localities" value="<?= $rec->localities ?>">
										<select onclick="funcsel()" id="localitiesz" class="non_grid_input black_border form-control" multiple>
										<?php
											//global $wpdb;
											$SQL = "SELECT * FROM pdg_locality ORDER BY locality_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<?php 
										$selitems= explode(",",$rec->localities);
										
										$isSelected = in_array($r->locality_id,$selitems) ? "selected='selected'" : "";
										
										//$selected=($rec->localities == $r->locality_id)? "selected='selected'" : "";
										?>
										<option <?= $isSelected ?> value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
										<?php
											}
										?>
										</select>





                                        <input placeholder="When do you want the classes to begin" class="non_grid_input black_border no_upper" type="text" value="<?= 

$rec->start_date ?>" id="start_date" name="start_date">
                                        <textarea class="mentions_localitites black_border min_height_130" id="requirement" name="requirement" placeholder="Please 

mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching.">
										<?= $rec->requirement ?>
										</textarea>
                                     </div>
                                     
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <button id="cmdeditsubjectselectarchive" name="cmdeditsubjectselectarchive"  class="data_submit blue_background color_white" onclick="f(event)" >Submit</button>
                                    </div>
									<?php
										}
									?>
                                    </form>
                                  </div>
								    </div>
                          </div>
		<?php
		
		
		die();
		
	}
	
	public static function update_archivedata_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		
		$subid= $data['sub_id'];
		
		
		
		$subjectid=$data['subject_id'];
		$school=$data['school'];
		

		$board=$data['board'];
		$standard=$data['standard'];
		
		
		if (isset($data['myhome']))
		$myhome = 1;
		else
		$myhome = 0;
		
		
		
		if (isset($data['tutorhome']))
		$tutorhome = 1;
		else
		$tutorhome = 0;
		
		
		
		if (isset($data['institute']))
		

		$institute = 1;
		

		else
		$institute = 0;
		
		
		
		$localities=$data['localities'];
		$start_date=date("Y-m-d", strtotime($data['start_date']));
		

		$requirement=trim($data['requirement']);
		
		$age=$data['age'];
		
	
		
		
		$sql="delete from pdg_query where id='$subid'";
		
		$wpdb->query($sql);
		
			
			$sql="insert into pdg_query(student_id,subject, school, board, standard, 

	myhome, tutorhome, institute, 

	localities, start_date, requirement, age,approved) values('$userid','$subjectid','$school','$board','$standard',$myhome,$tutorhome,$institute,'$localities','$start_date','$requirement','$age','0')";
	
		
		$wpdb->query($sql);
		
		
		
		die();
		
	}
	
	public static function shortlist_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$teacher_id=$data['teacherid'];
		$type=$data['type'];
		$queryid=$data['query_id'];
		
		//echo "userid".$userid."teacherid".$teacher_id."type".$type."queryid".$queryid;
		
		$sql="insert into pdg_shortlist(queryid, studentid,teacherinstid, type) values($queryid,$userid,$teacher_id,'$type')";
	
		
		$wpdb->query($sql);
		
		//echo $sql;
		
		die();
	}
	
	public static function shortlistremove_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$teacherinst_id=$data['teacherinstid']; 
		
		$sql="delete from pdg_shortlist where teacherinstid=$teacherinst_id";		
				
		$wpdb->query($sql);	
		
		die();
		
	}
	
	public static function inviteremove_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$teacherinst_id=$data['teacherinstid']; 
		
		$sql="update pdg_invite set status=0 where studentid=$userid and teacherinstid=$teacherinst_id";		
				
		$wpdb->query($sql);	
		
		die();
		
	}
	
	public static function invite_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$teacher_id=$data['teacherid'];
		$type=$data['tutortype'];
		$queryid=$data['query_id'];
		
		$linktext=$data['linktext'];
		
		//echo "userid".$userid."teacherid".$teacher_id."type".$type."queryid".$queryid;
		
		if(trim($linktext)=="Invite"){
			
			$sql="select * from pdg_invite where queryid=$queryid and studentid=$userid and teacherinstid=$teacher_id";
			
			$wpdb->query($sql);
			
			if($wpdb->num_rows > 0){
				
			$sql="update pdg_invite set status=1 where queryid=$queryid and studentid=$userid and teacherinstid=$teacher_id";
			
			$wpdb->query($sql);
			
			echo $status="1";
			
			}else{
		
			$sql="insert into pdg_invite(queryid, studentid,teacherinstid, type,status) values($queryid,$userid,$teacher_id,'$type',1)";
			
			$wpdb->query($sql);
			
			echo $status="1";
			
			}
		
		}else if(trim($linktext)=="Uninvite"){
		
		$sql="update pdg_invite set status=0 where teacherinstid=$teacher_id and queryid=$queryid";
		
		$wpdb->query($sql);
		echo $status="0";
		}
		die();
		
	}
	
	public static function savequery_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$queryid=$data['queryid'];
		$tutortype=$data['tutortype'];
		$linktext=trim($data['linktext']);
		
		if(trim($linktext)=="Save"){
			
			$sql="select * from pdg_saved where queryid=$queryid and teacherinstid=$userid";
			
			$wpdb->query($sql);
			
			if($wpdb->num_rows > 0){
				
			$sql="update pdg_saved set status=1 where queryid=$queryid and teacherinstid=$userid";
			
			$wpdb->query($sql);
			
			echo $status="1";
			
			}else{
		
			$sql="insert into pdg_saved(queryid,teacherinstid, tutortype,status) values($queryid,$userid,'$tutortype',1)";
			
			$wpdb->query($sql);
			
			echo $status="1";
			
			}
		
		}else if(trim($linktext)=="Unsave"){
		
		$sql="update pdg_saved set status=0 where teacherinstid=$userid and queryid=$queryid";
		
		$wpdb->query($sql);
		echo $status="0";
		}
		
		die();
		
	}
	public function showquerypanel_ajax(){
		
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$subid=$data['query_id'];
							
									
										
										
										
		$sql="select * from pdg_query where student_id=$userid and id=$subid and approved=1 and archived=0" ;
										
		//echo $sql;
		
		$recs=$wpdb->get_results($sql);
										
		$studeditsubject=$recs[0]->subject;
										
		//foreach ( $recs as $rec ) {
										
										
									
        ?>
		
		<div class="row">
				 <div class="col-xs-12 blue_background top_radius h1_heading">
				 
				 <?php
					
					$sql="select subject_name from pdg_subject_name where subject_name_id='$studeditsubject'";
					
					$recsub=$wpdb->get_results($sql);		
					
					
						$subject_namep=$recsub[0]->subject_name;
						
						$subject_name="'".$subject_namep."'";
				 ?>
				 
				 
					<h2 class="color_white">Need a <span>Home Tutor</span> for <span><?= $subject_namep ?></span></h2>
					<input type="hidden" id="queryid" value="<?= $subid ?>">
				 </div>
        </div>
		
		<div class="row margin_top_10">
                                         <div class="col-md-6 col-sm-6 col-xs-12 localities">
										 <?php 
											$standard= $recs[0]->standard;  
										?>
                                            	<p>Year/Standard: <b><?= $recs[0]->standard ?></b></p>
											<?php	
												function convertboardid($str){
								global $wpdb;
								$sql="select academic_board from pdg_academic_board 

where academic_board_id='$str'";
								$rec=$wpdb->get_results($sql);
								return $rec[0]->academic_board;
							}
											
									?>			
												
												
												<p  id="activequeryboard">Board/University:<b><?= convertboardid($recs[0]->board) ?></b></p>
												 <div class="preferred_location"  id="activequerylocality">
                                                    <p>Locality Type:</p> 
                                                    <ul class="area">
													<?php
														$teachplace="";
														if($recs[0]->myhome)$teachplace="Student's Location";
														
														else if($recs[0]->tutorhome)$teachplace="Own Location";
														
														else if($recs[0]->institute) $teachplace="Both own location and student's location";
														
														$teachplace="'".$teachplace."'";
													
													
													?>
													
													<?php if($recs[0]->myhome){  ?>
                                                        <li>
														
														<a 

href="#" class="blue_background">My Home</a>
														
														
													</li><?php }else{ ?><li></li><?php } ?>
													
													<?php if($recs[0]->tutorhome){ ?>
                                                        <li>
														
														<a 

href="#" class="blue_background">Tutor's Home</a>
														
													</li><?php }else{ ?><li></li><?php } ?>
													
													<?php if($recs[0]->institute){ ?>
                                                        <li>
														
														<a 

href="#" class="blue_background">Institute</a>
														
													</li><?php }else{ ?><li></li><?php } ?>
														
                                                    </ul>
                                                </div>
												
                                                <div class="preferred_location">
												<?php
												
													function getlocality($id){
														global $wpdb;
														$sql="select locality from pdg_locality where locality_id='$id'";
														$recloc=$wpdb->get_results($sql);
														foreach ( $recloc as $loc ) return $loc->locality;
														
														
														
													}
												
												?>
                                                    <p>Preferred Localities:</p> 
                                                    <ul class="area">
													<?php
														$localities=explode(",",$recs[0]->localities);
														$preflocs="";
														foreach($localities as $loc){

															
															$preflocs.= "'".getlocality($loc)."',";
															

															
													?>
                                                        <li><a href="#" class="blue_background"><?= getlocality($loc) ?></a></li>
                                                       
													<?php
														}
														$preflocs=rtrim($preflocs,",");
													?>
                                                    </ul>
                                                </div>
												<div class="preferred_location"  id="activequerydoc">
                                                    <p class="width_100">Date of commencement: <strong><?= date_format(date_create($recs[0]->start_date), "d/m/Y") ?></strong></p> 
                                                </div>
												
                                         </div>
										 
										 <div class="col-md-3 col-sm-3 col-xs-12 margin_bottom_20 preference" >
											<div  id="activequerypreferences">
                                         	<h5>Preferences: </h5>
                                            <p class="text-justify">
											<?= trim($recs[0]->requirement) ?>
											</p>
											</div>
                                         </div>
										 
										 
										 
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                            <ul class="pull-right side_right_menu side_right_menu_media text-center">
                                            	<!--<li><a href="#" class="blue_border blue_color" id="activequeryexpand">Expand</a></li>
												<li><a href="#" class="blue_border blue_color"  id="activequerycollapse">Collapse</a></li>-->
                                                <li><a href="#" class="blue_border blue_color" onclick="activequeryedit(event,idx=<?= $subid ?>)" data-toggle="modal" >Edit</a></li>
                                                <li><a  href="#" class="blue_border blue_color" id="activequeryarchive" data-text="<?= $subid  ?>">Archive</a></li>
												
                                                <!--<li><a href="#" class="blue_border blue_color"  id="activequerysavethis">Save this</a></li>
                                                <li><a href="#" class="blue_border blue_color"  id="activequeryshowinterest">Show interest</a></li>
                                                <li><a href="#" class="blue_border blue_color"  id="activequeryrefersomeone">Refer someone</a></li>-->
                                            </ul>
                                         </div>
        </div>
		
		
		<?php
		
		
		
		die();
	}
	
	
	public function editquerymodal_ajax(){
	
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$userid=$_SESSION['member_id'];
		$subid=$data['query_id'];
		
		
		$sql="select * from pdg_query where student_id=$userid and id=$subid and approved=1 and archived=0" ;
										
										//echo $sql;
		
										$recs=$wpdb->get_results($sql);
										
										$studeditsubject=$recs[0]->subject;
		
		?>
		
					   
					   <div class="modal fade" id="editsubjectselect" role="dialog">
                                <div class="modal-dialog modal-dialog-next">
		<div class="modal-content blue_border">
                                    <div class="modal-header">
									<ul class="acadamic_subject_form">
									  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      	<li>&nbsp;</li>	
										
										<?php
											global $wpdb;
											$sql="select subject_name from pdg_subject_name where subject_name_id='$studeditsubject'";
											$recsub=$wpdb->get_results($sql);		
											foreach ( $recsub as $recsb ) $subject_name=$recsb->subject_name;
											
										 ?>



										
                                      	<li class="active_subject"><a href="#"><?= $subject_name; ?></a></li>
                                        <li><!--<a href="#" class="blue_background color_white" data-toggle="modal" data-target="#subjectSelectNext">History <i class="fa fa-caret-right" aria-hidden="true"></i></a>--></li>
                                      </ul>
									
									</div>
									
									<form action="" method="post" id="frmeditsubject" name="frmeditsubject">
										<?php
							
										global $wpdb;
										
										$res = $wpdb->get_results( "SELECT distinct course_type_id from pdg_course_category where subject_name_id ='$studeditsubject'" ); 
										
										
										
										$course_type=$res[0]->course_type_id;
										
										
										
										
										
										$sql="select * from pdg_query where id='$subid'" ;
										$recs=$wpdb->get_results($sql);						
									
										
										
										foreach ( $recs as $rec ) {
							
							?>
                                    <div class="modal-body no_padding_top_bottom">
                                    <div class="row">
									<input type="hidden" value="<?= $rec->id ?>" id="subid" name="subid">
									<?php 
									
									//course_type = 2 non academic
									
									if($course_type==2){ ?>
									
									 <div class="col-sm-12 subject_type">
                                     	<input placeholder="Age" name="age" id="age" class="non_grid_input black_border non_grid_input black_border" type="text" value="<?= $rec->age ?>">
                                     </div>
									
									
									<!-- course type 1 academic  -------------------------------------->
									
									
									
									<?php }else if($course_type==1){ ?>
									
                                     <div class="col-sm-12 subject_type">
									 
                                     	<!--<input name="school" id="school"  value="<?= $rec->school ?>" placeholder="School/college" class="non_grid_input black_border"   type="text">
                                        <input name="board" id="board" value="<?= $rec->board ?>" placeholder="Board/university" class="non_grid_input black_border"   type="text">
										-->
										
										<select name="school" id="school" class="non_grid_input black_border form-control" >
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_schools ORDER BY school_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
												$selected=($rec->school == $r->school_id)? "selected='selected'" : "";
										?>
										<option <?= $selected ?> value="<?= $r->school_id ?>"><?= $r->school_name ?></option>
										<?php
											}
										?>
										</select>
										
										<select name="board" id="board" class="non_grid_input black_border form-control">
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_academic_board ORDER BY academic_board_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
												$selected=($rec->board == $r->academic_board_id)? "selected='selected'" : "";
										?>
										<option <?= $selected ?> value="<?= $r->academic_board_id ?>"><?= $r->academic_board ?></option>
										<?php
											}
										?>
										</select>
										
										
										
                                        <!--<input name="standard" id="standard" value="<?= $rec->standard ?>" placeholder="Standard/year" class="non_grid_input black_border"   type="text">-->
										
										
										<select name="standard" id="standard" class="non_grid_input black_border form-control">
										<option value="Select Standard">STANDARD/YEAR</option>
										<?php
											global $wpdb;
											$SQL = "SELECT subject_type FROM pdg_subject_type ORDER BY subject_type_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
												$selected=($rec->standard == $r->subject_type)? "selected='selected'" : "";
										?>
										<option <?= $selected ?> value="<?= $r->subject_type ?>"><?= $r->subject_type ?></option>
										<?php
											}
										?>
										</select>
										
                                     </div>
									 
									<?php }else{ ?>
									
										<div class="col-sm-12 subject_type" style="display:none" >
									 <input type="hidden" value="<?= $rec->id ?>" id="subid" name="subid">
                                     	<input name="school" id="school"  value="" placeholder="School/college" class="non_grid_input black_border"   type="text">
                                        <input name="board" id="board" value="" placeholder="Board/university" class="non_grid_input black_border"   type="text">
                                        <input name="standard" id="standard" value="" placeholder="Standard/year" class="non_grid_input black_border"   type="text">
                                     </div>
									
									
									<?php } ?>
                                     
                                     <div class="col-sm-12 subject_type">
                                     	<p class="text-center option_heading">Select your preferred option</p>
                                        <ul class="place_option">
                                        	<li><input <?php echo $rec->myhome? 'checked' : '' ?> type="checkbox" name="myhome" id="myhome"><label for="MyHome">My Home</label></li>
                                            <li><input <?php echo $rec->tutorhome? 'checked' : '' ?> type="checkbox" name="tutorhome" id="tutorhome"><label for="TutorHome">Tutor's Home</label></li>
                                            <li><input  <?php echo $rec->institute? 'checked' : '' ?> type="checkbox" name="institute" id="institute"><label for="Institute">Institute</label></li>
                                        </ul>
                                        
										
										
										<input type="hidden" name="localities" id="localities" value="<?= $rec->localities ?>">
										<select  id="localitiesedit" name="localitiesedit" class="non_grid_input black_border form-control" multiple onclick="localitiesclick()">
										<?php
											global $wpdb;
											$SQL = "SELECT * FROM pdg_locality ORDER BY locality_id";
											$rows = $wpdb->get_results( $SQL );
											foreach($rows as $r){
										?>
										<?php 
										$selitems= explode(",",$rec->localities);
										
										$isSelected = in_array($r->locality_id,$selitems) ? "selected='selected'" : "";
										
										//$selected=($rec->localities == $r->locality_id)? "selected='selected'" : "";
										?>
										<option <?= $isSelected ?>  value="<?= $r->locality_id ?>"><?= $r->locality ?></option>
										<?php
											}
										?>
										</select>
										
										
										
										
                                        <input placeholder="When do you want the classes to begin" class="non_grid_input black_border no_upper" type="text" value="<?= $rec->start_date ?>" id="start_date" name="start_date">
                                        <textarea class="mentions_localitites black_border min_height_130" id="requirement" name="requirement" placeholder="Please mention here, If you have any special requirement from the teacher. eg:  Need a female home tutor with minimum 2 years of experience in teaching.">
										<?= $rec->requirement ?>
										</textarea>
                                     </div>
                                     
                                     </div>
                                    </div>
                                    <div class="modal-footer no_padding_top_bottom">
                                      <button id="cmdeditsubjectselect"   class="data_submit blue_background color_white" onclick="submitqueryeditval(event)" >Submit</button>
                                    </div>
									<?php
										}
									?>
                                    </form>
									
									
									</div>
								</div>
					 </div>
		
		
		<?php
		die;
		
	}
	
	function recommendedtab_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
	$queryid=$data['query_id'];
	
	$sql="select * from pdg_query where id=".$queryid ;
	$recs=$wpdb->get_results($sql);					

	
	$subject_id=$recs[0]->subject;
	
	$standard=$recs[0]->standard;
	
	$myhome=$recs[0]->myhome;
	$tutorhome=$recs[0]->tutorhome;
	$institute=$recs[0]->institute;
	
	if($myhome==1) $prefloc="Student\'s Location";
	if($tutorhome==1) $prefloc="Own Location";
	if($myhome==1 && $tutorhome==1) $prefloc="Both own location and student\'s location";
	if($institute==1 ) $prefloc="Institute";
	$sql="select subject_name from pdg_subject_name where subject_name_id=".$subject_id;				
	$recsub=$wpdb->get_results($sql);				
	$subject_name=$recsub[0]->subject_name;					
	?>
	
	<!------------------------------------------------private tutor------------------------------------->
	<div class="row" id="privatetutor">
		<div class="col-xs-12 author_review">
															
																																															<?php
																	
																		
	

/*$sql="SELECT * FROM user_data_teacher WHERE  subject_taught1 = '$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = '$subject_name' or subject_taught4 = '$subject_name'  ORDER BY id ASC";*/

/*$sql="SELECT * FROM user_data_teacher WHERE  subject_taught1 = '$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = '$subject_name' or subject_taught4 = '$subject_name' and classofteaching='$standard' and hometutor='$prefloc'

UNION

SELECT * FROM user_data_teacher WHERE  subject_taught1 = '$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = '$subject_name' or subject_taught4 = '$subject_name' and classofteaching='$standard'

UNION

SELECT * FROM user_data_teacher WHERE  subject_taught1 = '$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = '$subject_name' or subject_taught4 = '$subject_name'";*/


$sql="SELECT * FROM user_data_teacher  WHERE subject_taught1 = '$subject_name' 
UNION 
SELECT * FROM user_data_teacher WHERE subject_taught1 = 
'$subject_name' or subject_taught2 = '$subject_name'  
UNION 
SELECT * FROM user_data_teacher WHERE subject_taught1 = 
'$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = '$subject_name' 
UNION
SELECT * FROM user_data_teacher WHERE subject_taught1 = 
'$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = 
'$subject_name' or subject_taught4 = '$subject_name' 
UNION
SELECT * FROM user_data_teacher WHERE subject_taught1 = 
'$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = 
'$subject_name' or subject_taught4 = 
'$subject_name' and classofteaching like '%$standard%'
UNION
SELECT * FROM user_data_teacher WHERE subject_taught1 = 
'$subject_name' or subject_taught2 = '$subject_name' or subject_taught3 = 
'$subject_name' or subject_taught4 = 
'$subject_name' and classofteaching like '%$standard%' and hometutor = '$prefloc'";


	$recs=$wpdb->get_results($sql);
	//print_r($recs);
	foreach ( $recs as $rec ) {
		
	$sqlsl="select teacherinstid from pdg_shortlist where studentid='".$userid."' and teacherinstid='".$rec->id."'";
	
	$recsl=$wpdb->get_results($sqlsl);

	//echo $recsl[0]->teacherinstid ."/".$rec->id;
	
	if($recsl[0]->teacherinstid!=$rec->id){
	
?>
															
                                                            	<ul class="author_details">
                                                                <!-- Author Box Start -->
																

                                                                	<li class="author_box">
                                                                    	<div class="author_image">
                                                                        	<div class="image_holder">
                                                                        		
										<img src="<?php
											//echo get_avatar($rec->new_user_id , 88 );
											
											echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $rec->new_user_id ."/profile.jpg"; ?>" width="88" height="88">
										
																				
                                                                            </div>
																			
                                                                        </div>
                                                                        <div class="author_rating pull-right">
                                                                        	<ul class="text-right">
                                                                            	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>
                                                                                <li>28like(s)</li>
                                                                                <!--<li class="blue_button"><a href="#">View Profile</a></li>-->
																				<?php
																				$sqlp="SELECT profile_slug FROM pdg_teacher where user_id=$rec->new_user_id";
																				
																				//echo $sqlp;

																	//echo $sql;


																	$recsp=$wpdb->get_results($sqlp);
																	
																	//foreach ( $recsp as $recp ) {
																				?>
																				<li class="blue_button"><a href="<?= home_url(); ?>/teacher/<?= $recsp[0]->profile_slug ?>" target="_blank">View Profile</a></li>
																				
																	<?php //} ?>	
																				
																				
                                                                                <li class="blue_button"><a href="#" class="shortlist" data-text="privatetutor" id="<?= $rec->id ?>" data-id="<?= $subid ?>" >Short List</a></li>
                                                                                <li class="blue_button"><a href="#" class="invite" data-text="privatetutor" id="<?= $rec->id ?>" data-id="<?= $subid ?>">
																				
																				<?php
																				$tid=$rec->id;
																				$sqlx="select status from pdg_invite where teacherinstid=$tid";
																				
																				

																				$recsx=$wpdb->get_results($sqlx);
																				
																			
																				$status= $recsx[0]->status;
																				
																				echo $status==0? 'Invite':'Uninvite';
																				
																				
																				?>
																				
																				</a></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="author_address">
                                                                        	<ul>
                                                                            	<li>Name : <strong><?= $rec->name; ?></strong></li>
																				<?php
																				
																				$alllocality='';
																				
																				if($rec->locality!="NULL") $alllocality.=$rec->locality.", ";
																				if($rec->locality1!="NULL") $alllocality.=$rec->locality1.", ";
																				if($rec->locality2!="NULL") $alllocality.=$rec->locality2.", ";
																				if($rec->locality3!="NULL") $alllocality.=$rec->locality3.", ";
																				
																				$alllocality=rtrim($alllocality,", ");
																				//echo $alllocality;
																				?>
                                                                                <li>Location : <strong><?= $alllocality;  ?></strong></li>
																				<?php
																				
																				$subjects='';
																				
																				if($rec->subject_taught1!="NULL") $subjects.=$rec->subject_taught1.", ";
																				if($rec->subject_taught2!="NULL") $subjects.=$rec->subject_taught2.", ";
																				if($rec->subject_taught3!="NULL") $subjects.=$rec->subject_taught3.", ";
																				if($rec->subject_taught4!="NULL") $subjects.=$rec->subject_taught4.", ";
																				
																				$subjects=rtrim($subjects,", ");
																				
																				
																				
																				?>
                                                                                <li>Subject : <strong><?= $subjects; ?></strong></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="author_experience">
                                                                        	<ul>
                                                                            	<li>Experience (Years): <strong><?= $rec->workexperience; ?> </strong></li>
                                                                                <li>Type of location: <strong><?= $rec->hometutor ?></strong></li>
                                                                                <li>Starting from : <strong><?= $rec->feeslower; ?></strong></li>
                                                                            </ul>
                                                                        </div>
                                                                    </li>								
								<?php
								}else{}
								
								}
								?>
								<!-- Author Box Start -->   
                                                                </ul>
																
															
																
                                                            </div>
                                                        </div>
	
	
	<!--------------------------------------end private tutor------------------------------------>
														
	<!---------------------------------------------------institute---------------------------------------------------------->
         <div class="row" id="institutions">
		<div class="col-xs-12 author_review">													
		<?php
		 $sql="SELECT a.*,b.id as userid FROM user_data_institution a INNER JOIN wp_users b ON a.emailinst = b.user_email WHERE 
		 a.coursename1 ='$subject_name' or a.coursename2 ='$subject_name' or a.coursename3 = '$subject_name' or a.coursename4 ='$subject_name' or a.coursename5 ='$subject_name'";
                 $recs=$wpdb->get_results($sql);
			foreach ( $recs as $rec ) {
			//echo $rec->id;
			$sqlsl="select teacherinstid from pdg_shortlist where studentid='".$userid."'";
			
			$recsl=$wpdb->get_results($sqlsl);

			//echo $recsl[0]->teacherinstid ."/".$rec->id;
			
			if($recsl[0]->teacherinstid!=$rec->id){
		?>													
																
															
															
                                                            	<ul class="author_details">
                                                                <!-- Author Box Start -->
																

                                                                	<li class="author_box">
                                                                    	<div class="author_image">
                                                                        	<div class="image_holder">
                                                                        		<img src="<?php
																					
																					
																					echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $rec->userid ."/profile.jpg";
																				?>" width="88" height="88">
																				
																				
																				
																				
																				
                                                                            </div>
																			
                                                                        </div>
                                                                        <div class="author_rating pull-right">
                                                                        	<ul class="text-right">
                                                                            	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>
                                                                                <li>28like(s)</li>
                                                                                <!--<li class="blue_button"><a href="#">View Profile</a></li>-->
																				
																					<?php
						$sqlin="SELECT profile_slug FROM pdg_institutes where primary_contact_no='$rec->contactinst'";
																				
																				//echo $sqlin;

																	//echo $sql;


																	$recsin=$wpdb->get_results($sqlin);
																	foreach ( $recsin as $recin ) {
																				?>
																				
																				<li class="blue_button"><a href="<?= home_url(); ?>/institute/<?= $recin->profile_slug ?>" target="_blank">View Profile</a></li>
																				
																				<?php
																				}
																	
																				?>
																				
																				
                                                                                <li class="blue_button"><a href="#" class="shortlist" data-text="institute" id="<?= $rec->id ?>">Short List</a></li>
                                                                                
																				<li class="blue_button"><a href="#" class="invite" data-text="institute" id="<?= $rec->id ?>" data-id="<?= $subid ?>">
																				
																				<?php
																				$tid=$rec->id;
																				$sqlx="select status from pdg_invite where teacherinstid=$tid";
																				
																				

																				$recsx=$wpdb->get_results($sqlx);
																				
																				$status= $recsx[0]->status;
																				echo $status==0? 'Invite':'Uninvite';
																				//echo $status;
																				
																				?>
																				
																				
																				</a></li>
																				
																				
																				
                                                                            </ul>
                                                                        </div>
                                                                        <div class="author_address">
                                                                        	<ul>
                                                                            	<li>Name : <strong><?= $rec->namecontact; ?></strong></li>
																				<?php
																				
																				$alllocality='';
																				
																				if($rec->learninglocality1!="NULL") $alllocality.=$rec->learninglocality1.", ";
																				if($rec->learninglocality2!="NULL") $alllocality.=$rec->learninglocality2.", ";
																				if($rec->learninglocality3!="NULL") $alllocality.=$rec->learninglocality3.", ";
																				if($rec->learninglocality4!="NULL") $alllocality.=$rec->learninglocality4.", ";
																				if($rec->learninglocality5!="NULL") $alllocality.=$rec->learninglocality5.", ";
																				
																				$alllocality=rtrim($alllocality,", ");
																				
																				?>
                                                                                <li>Location : <strong><?= $alllocality;  ?></strong></li>
																				<?php
																				
																				$subjects='';
																				
																				if($rec->coursename1!="NULL") $subjects.=$rec->coursename1.", ";
																				if($rec->coursename2!="NULL") $subjects.=$rec->coursename2.", ";
																				if($rec->coursename3!="NULL") $subjects.=$rec->coursename3.", ";
																				if($rec->coursename4!="NULL") $subjects.=$rec->coursename4.", ";
																				if($rec->coursename5!="NULL") $subjects.=$rec->coursename5.", ";
																				
																				
																				$subjects=rtrim($subjects,", ");
																				
																				
																				
																				?>
                                                                                <li>Subject : <strong><?= $subjects; ?></strong></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="author_experience">
                                                                        	<ul>
                                                                            	<li>Experience (Years): <strong><?= $rec->yearsofexsistance; ?> </strong></li>
                                                                                <li>Type of location: <strong><?= $rec->institutionlearninglocatiom ?></strong></li>
                                                                                <li>Starting from : <strong><?= $rec->coursefeesinstitution1; ?></strong></li>
                                                                            </ul>
                                                                        </div>
                                                                    </li>
																	
																	<?php
																	
																	}else{}
																	
																	}
																	
																	?>
																	
                                                                    <!-- Author Box Start -->
                                                                    
                                                                </ul>
                                                            </div>
														
												</div>
														
<!------------------------------------------------invite---------------------------------------------------------->


														<div class="row" id="invited" >
                                                        	<div class="col-xs-12 author_review">
															
																					

																					

					<?php
																	
																	

								global $wpdb;	
																	



$sql="SELECT * FROM user_data_teacher WHERE id in (select teacherinstid from pdg_invite where status=1) and  (subject_taught1='$subject_name' or subject_taught2='$subject_name' or subject_taught3='$subject_name' or subject_taught4='$subject_name') ORDER BY id ASC";

																	//echo $sql;


																	$recs=$wpdb->get_results($sql);
																	foreach ( $recs as $rec ) {
																	
																	//echo $rec->id;
																
																?>					

                                                            	<ul class="author_details">
                                                                <!-- Author Box Start -->
																

                                                                	<li class="author_box">
                                                                    	<div class="author_image">
                                                                        	<div class="image_holder">
                                                                        		<img src="<?php
																					
																					
																					echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $rec->new_user_id ."/profile.jpg"
																				?>" width="88" height="88">
                                                                            </div>
                                                                        </div>
                                                                        <div class="author_rating pull-right">
                                                                        	<ul class="text-right">
                                                                            	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>
                                                                                <li>28like(s)</li>
                                                                                <li class="blue_button"><a href="#">View Profile</a></li>
																				
                                                                                <li class="blue_button"><a href="#" class="shortlist" data-text="institute" id="<?= $rec->id ?>">Short List</a></li>
                                                                                
																				<li class="blue_button"><a href="#" class="invite" data-text="institute" id="<?= $rec->id ?>" data-id="<?= $subid ?>">
																				
																				<?php
																				$tid=$rec->id;
																				$sqlx="select status from pdg_invite where teacherinstid=$tid";
																				
																				

																				$recsx=$wpdb->get_results($sqlx);
																				
																			
																				
																				$status=$recsx[0]->status;
																				
																				echo $status==0? 'Invite':'Uninvite';
																				//echo $status;
																				
																				?>
																				
																				
																				</a></li>
																				
																				
																				
                                                                            </ul>
                                                                        </div>
                                                                        <div class="author_address">
                                                                        	<ul>
                                                                            	<li>Name : <strong><?= $rec->name; ?></strong></li>
																				<?php
																				
																				$alllocality='';
																				
																				if($rec->locality!="NULL") $alllocality.=$rec->locality.", ";
																				if($rec->locality1!="NULL") $alllocality.=$rec->locality1.", ";
																				if($rec->locality2!="NULL") $alllocality.=$rec->locality2.", ";
																				if($rec->locality3!="NULL") $alllocality.=$rec->locality3.", ";
																				
																				
																				$alllocality=rtrim($alllocality,", ");
																				
																				?>
                                                                                <li>Location : <strong><?= $alllocality;  ?></strong></li>
																				<?php
																				
																				$subjects='';
																				
																				if($rec->subject_taught1!="NULL") $subjects.=$rec->subject_taught1.", ";
																				if($rec->subject_taught2!="NULL") $subjects.=$rec->subject_taught2.", ";
																				if($rec->subject_taught3!="NULL") $subjects.=$rec->subject_taught3.", ";
																				if($rec->subject_taught4!="NULL") $subjects.=$rec->subject_taught4.", ";
																				
																				
																				
																				$subjects=rtrim($subjects,", ");
																				
																				
																				
																				?>
                                                                                <li>Subject : <strong><?= $subjects; ?></strong></li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="author_experience">
                                                                        	<ul>
                                                                            	<li>Experience (Years): <strong><?= $rec->workexprience; ?> </strong></li>
                                                                                <li>Type of location: <strong><?= $rec->hometutor ?></strong></li>
                                                                                <li>Starting from : <strong><?= $rec->feeslower; ?></strong></li>
                                                                            </ul>
                                                                        </div>
                                                                    </li>
																	
																	<?php
																	
																	}
																	
																	?>
																	
                                                                    <!-- Author Box Start -->
                                                                    
                                                                </ul>
																
                                                            </div>
														
                                                        </div>


<!------------------------invite end------------------------------------------------------------------------------>													

		
	
	
	<?php
	
	die;	
		
	}
	
	function shortlistedtab_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
	

		$subid=$data['query_id'];
		$userid=$_SESSION['member_id'];
		
	
											
					//$sql="SELECT * FROM pdg_shortlist where studentid=$userid";


?>	

	
<!------------------------------------------------shortlist hometutor--------------------------------------------->						

								

													

	

                                            <div class="row" id="privatetutor">

                                                 <div class="col-xs-12 author_review">

                                                            	<ul class="author_details">

                                             

                   <!-- Author Box Start -->

																

																<?php

								global $wpdb;											
								
								
								

				

	$sql="select 

a1.id,a1.new_user_id,a1.name, a1.locality, a1.locality1, a1.locality2, a1.locality3,a1.subject_taught1, a1.subject_taught2, 



a1.subject_taught3, a1.subject_taught4, 

a1.workexperience, a1.hometutor, a1.feeslower from user_data_teacher a1 inner 



join pdg_shortlist a2 on a1.id= a2.teacherinstid and a2.studentid=$userid and 

a2.queryid=$subid";



//echo $sql;
																$recs=$wpdb->get_results($sql);

		

															foreach ( $recs as $rec ) {

			
		?>

													

                                                

                	 <li class="author_box">

                                                                    	<div class="author_image">

                      

                                                  	<div class="image_holder">

<img src="<?php echo plugins_url().'/pedagoge_plugin/storage/uploads/images/'. $rec->new_user_id .'/profile.jpg'																?>" width="88" height="88">

																				

                                                                            </div>

                                                                        </div>

        

                                                                <div class="author_rating pull-right">

                                                                  

      	<ul class="text-right">

                                                                            	<li class="rating"><a href="#"><i class="fa fa-star" 

aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i 

class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                     

                                           <li>28like(s)</li>

                                                                                <!--<li 

class="blue_button"><a href="#">View Profile</a></li>-->

														

						

															

					

																

				<?php

																	

			$sqlp="SELECT profile_slug FROM pdg_teacher where user_id=$rec->new_user_id";

									

				

													

				$recsp=$wpdb->get_results($sqlp);

													

				foreach ( $recsp as $recp ) {

														

						?>

															

					<li class="blue_button"><a href="<?= home_url(); ?>/teacher/<?= $recp->profile_slug ?>" target="_blank">View Profile</a></li>

	

																			

		

															<?php } ?>	

				

												
									

											

                                                                                

<li class="blue_button"><a href="#" class="removehometutorsl" id="<?= $rec->id ?>">Remove</a></li>

                                                                      

          <!--<li class="blue_button"><a href="#" class="invite" data-text="privatetutor" id="<?= $rec->id ?>" data-id="<?= $subid ?>">Invite:</a></li>-->

		

																		

			

																	<li class="blue_button"><a 

href="#" class="invite" data-text="privatetutor" id="<?= $rec->id ?>" data-id="<?= $subid ?>">

										

										

											

									<?php

												

								$tid=$rec->id;

												

								$sqlx="select status from pdg_invite where teacherinstid=$tid";

						

														

							

													



								

												$recsx=$wpdb->get_results($sqlx);

					

														
							

													$status=$recsx[0]->status;

					

															echo $status==0? 'Invite':'Uninvite';

		

																	

				

																

					

															?>

						

														

							

													

								

												</a></li>

								

										

                                                                            </ul>

       

                                                                 </div>

                                                                        <div 

class="author_address">

                                                                        	<ul>

                                                                    

        	<li>Name : <strong><?= $rec->name ?></strong></li>

                                                                                <li>Location : 

<strong><?= $rec->locality.', '.$rec->locality1.', '.$rec->locality2.', '.$rec->locality3 ?></strong></li>

                                                                 

               <li>Subject : <strong><?= $rec->subject_taught1.', '.$rec->subject_taught2.', '.$rec->subject_taught3.', '.$rec->subject_taught4 ?></strong></li>

           

                                                                 </ul>

                                                                        </div>

                    

                                                    <div class="author_experience">

                                                                        	<ul>

    

                                                                        	<li>Experience (Years): <strong><?= $rec->workexperience ?></strong></li>

               

                                                                 <li>Type of location: <strong><?= $rec->hometutor ?></strong></li>

                                     

                                           <li>Starting from : <strong><?= $rec->feeslower ?></strong></li>

                                                             

               </ul>

                                                                        </div>

                                                                    

</li>

                                                                    <!-- Author Box Start -->

                                                                   

<?php

																	}

				

													

								

									?>

												

				</ul>

															</div>

		

										</div>									

							

														

		

														

<!------------------------------------------------shortlist institute--------------------------------------------->



							

							<div class="row" id="institutions">

                                                        	<div 

class="col-xs-12 author_review">

                                                            	<ul class="author_details">



<?php

					

												

									

								global $wpdb;											

	

																

					

											
										

							

	$sql="select a1.id,a1.contactinst,a1.namecontact, a1.learninglocality1, a1.learninglocality2, 

a1.learninglocality3, a1.learninglocality4, 



a1.learninglocality5, a1.coursename1, a1.coursename2, 



a1.coursename3, a1.coursename4, a1.coursename5, a1.yearsofexsistance, 

a1.institutionlearninglocatiom, 



a1.coursefeesinstitution1, a1.emailinst from user_data_institution a1 inner 



join pdg_shortlist a2 on a1.id= a2.teacherinstid and 

a2.studentid=$userid and a2.queryid=$subid";



																

	//echo $sql;





																	$recs=$wpdb->get_results($sql);

																	foreach ( $recs 

as $rec ) {

																	

			

														//echo $rec->id;

					

											

										

						?>

													

                

                                                	 <li class="author_box">

                                                                    	<div 

class="author_image">

                                                                        	<div class="image_holder">

						

													

								

											<?php

										

									global $wpdb;

	$sqlimg="select id from wp_users where user_email='$rec->emailinst'";

	

	

	$recimg=$wpdb->get_results($sqlimg);

																

			

																		

	

																			?>

 <img src="<?php echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $recimg[0]->id ."/profile.jpg";

								

												?>" width="88" height="88">

						

														

							

													

                                                                

            </div>

																			

                                                                        </div>

                                                                        <div 

class="author_rating pull-right">

                                                                        	<ul class="text-right">

                                 

                                           	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" 

aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i 

class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                                                                <li>28like(s)

</li>

                                                                                <!--<li class="blue_button"><a href="#">View Profile</a></li>-->

			

																	

				

																<?php

					

															$sqlin="SELECT profile_slug FROM pdg_institutes 

where primary_contact_no='$rec->contactinst'";

																

	




																	$recsin=$wpdb->get_results($sqlin);

																	foreach ( $recsin as $recin ) {

																			

	?>

																				

																				<li 

class="blue_button"><a href="<?= home_url(); ?>/institute/<?= $recin->profile_slug ?>" target="_blank">View Profile</a></li>

						

														

							

													<?php

								

												}

									

								

													

							?>

														

						

                                                                                <li class="blue_button"><li 

class="blue_button"><a id="<?= $rec->id ?>" href="#" class="removehometutorsl">Remove</a></li>

                                                                          

     

	
   <li class="blue_button"><a href="#" class="invite" data-text="institute" id="<?= $rec->id ?>" data-id="<?= $subid ?>">

						

														

							

													<?php

								

												$tid=$rec->id;

								

												$sqlx="select status from pdg_invite where teacherinstid=$tid";

		

																		

			

																	



				

																$recsx=$wpdb->get_results($sqlx);


	

																			$status=$recsx

[0]->status;

																				

echo $status==0? 'Invite':'Uninvite';

																	

			//echo $status;

																	

			

																		

		?>

																			

	

																				

																				

</a></li>

																			 

       

                                                                     </ul>

                                                                        </div>

                

                                                        <div class="author_address">

                                                                        	<ul>

    

                                                                        	<li>Name : <strong><?= $rec->namecontact ?></strong></li>

                               

                                                 <li>Location : <strong><?= $rec->learninglocality1.', '.$rec->learninglocality2.', '.$rec->learninglocality3.', '.$rec->learninglocality4 ?></strong></li>

                                                                                <li>Subject : <strong><?= $rec->coursename1.','.

$rec->coursename2.', '.$rec->coursename3.', '.$rec->coursename4.', '.$rec->coursename5 ?></strong></li>

                                                                    

        </ul>

                                                                        </div>

                                                                        <div 

class="author_experience">

                                                                        	<ul>

                                                            

                	<li>Experience (Years): <strong><?= $rec->yearsofexsistance ?></strong></li>

                                                                    

            <li>Type of location: <strong><?= $rec->institutionlearninglocatiom ?></strong></li>

                                                                        

        <li>Starting from : <strong><?= $rec->coursefeesinstitution1 ?></strong></li>

                                                                            </ul>

  

                                                                      </div>

                                                                    </li>

                   

                                                 <!-- Author Box Start -->

                                                                   <?php

			

														}

							

										

											

						?>

                                                                </ul>

                                                 

           </div>

                        </div>

                                                   
<!----------------------------------shortlist invite------------------------------------------->

							<!--						

							<div class="row" id="invited" >

                                                        	<div class="col-xs-12 author_review">

	-->
					<?php
/*
					global $wpdb;			


						

$sql="select a1.id,a1.phone_no,a1.name,a1.new_user_id, a1.locality, a1.locality1, a1.locality2, a1.locality3,a1.subject_taught1, a1.subject_taught2, a1.subject_taught3, a1.subject_taught4, a1.workexperience, a1.hometutor, a1.feeslower from user_data_teacher a1 inner join pdg_invite a2 on a1.id= a2.teacherinstid and a2.status=1 and a2.studentid=$userid and a2.queryid=$subid";




																	//echo $sql;





		

															$recs=$wpdb->get_results($sql);

			

														foreach ( $recs as $rec ) {

				

													

								

									//echo $rec->id;

										

						
*/
															

	?>					

 

                                    	<!--<ul class="author_details">-->

                                                                <!-- Author Box Start -->

		

														



                                                        

        	<!--<li class="author_box">

                                                                    	<div class="author_image">

                              

                                          	<div class="image_holder">

                      

 <img src="<?php echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $rec->new_user_id ."/profile.jpg";

								

												?>" width="88" height="88">                                              		




                                              

                              </div>

                                                                        </div>

                                                      

                  <div class="author_rating pull-right">

                                                                        	<ul class="text-right">

         

                                                                   	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a 



href="#"><i 

class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-



hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                                             

                   <li>28like(s)</li>

                                                                                <!--<li class="blue_button"><a href="#">View 

Profile</a></li>-->


<?php

			/*														

			$sqlp="SELECT profile_slug FROM pdg_teacher where user_id=$rec->new_user_id";

									

				

													

				$recsp=$wpdb->get_results($sqlp);

													

				foreach ( $recsp as $recp ) {

														
*/
						?>

															

					<!--<li class="blue_button"><a href="<?= home_url(); ?>/teacher/<?= $recp->profile_slug ?>" target="_blank">View Profile</a></li>-->

	

																			

		

															<?php //} ?>	

		
  <!--                                                                              
 <li class="blue_button"><li 

class="blue_button"><a id="<?= $rec->id ?>" href="#" class="removehometutorsl">Remove</a></li>
																				
	
<li class="blue_button"><a href="#" class="invite" data-text="institute" id="<?= $rec->id ?>" data-id="<?= $subid ?>">-->

													<?php

								/*

												$tid=$rec->id;

								

												$sqlx="select status from pdg_invite where teacherinstid=$tid";

																$recsx=$wpdb->get_results($sqlx);


																			$status=$recsx

[0]->status;

																				

echo $status==0? 'Invite':'Uninvite';
*/

		?>
<!--
</a></li>
                                 </ul>

                                                                        </div>

                

                                                        <div class="author_address">

                                                                        	<ul>

    

                                                                        	<li>Name : <strong><?= $rec->name; ?></strong></li>-->

					

															<?php

						

														

							
/*
													



$alllocality='';

						

														

							

													if



($rec->locality!="NULL") $alllocality.=$rec->locality.", ";

	

																			if



($rec->locality1!="NULL") $alllocality.=$rec->locality1.", ";

															

					if



($rec->locality2!="NULL") $alllocality.=$rec->locality2.", ";

									

											if



($rec->locality3!="NULL") $alllocality.=$rec->locality3.", ";

			

																	

				

																



$alllocality=rtrim($alllocality,", ");

	

																			
*/
		

																		?>

                      

                                                          <!--<li>Location : <strong><?= $alllocality;  ?></strong></li>-->

							

													<?php

								

												
/*
									

											



$subjects='';

									

											

										

										if



($rec->subject_taught1!="NULL") $subjects.=$rec->subject_taught1.", ";

			

																	if



($rec->subject_taught2!="NULL") $subjects.=$rec->subject_taught2.", ";

																

				if



($rec->subject_taught3!="NULL") $subjects.=$rec->subject_taught3.", ";

									

											if



($rec->subject_taught4!="NULL") $subjects.=$rec->subject_taught4.", ";



$subjects=rtrim($subjects,", ");

*/
												?>

                                      

                                          <!--<li>Subject : <strong><?= $subjects; ?></strong></li>

                                                                         

   </ul>

                                                                        </div>

                                                                        <div 

class="author_experience">

                                                                        	<ul>

                                                            

                	<li>Experience (Years): <strong><?= $rec->workexperience; ?> </strong></li>

                                                                     

           <li>Type of location: <strong><?= $rec->hometutor ?></strong></li>

                                                                                

<li>Starting from : <strong><?= $rec->feeslower; ?></strong></li>

                                                                            </ul>

                      

                                                  </div>

                                                                    </li>-->

					

												

									

								<?php

													

				

																	

//}

																	

				

													?>

								

									

                                                                    <!-- Author Box Start -->

   

                                                                 

                                                               <!-- </ul>

                                                            </div>

															

		

												

                        </div>-->




<!----------------------------------------------shortlist invite end------------------------------------------->
					

								
							

	<?php	
		die;
	}
	
	
	function interestedtab_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
	

		$subid=$data['query_id'];
		$userid=$_SESSION['member_id'];
		
	
											
					

?>	


                                                        <div class="row">

                                                        	<div class="col-xs-12 author_review">

                                                            	<ul class="author_details">

                                                                

                                                                	<ul class="new_author_details pull-left green_background margin_top_10"><!-- Nested Ul -->

                                                                    	<h2 class="text-center color_white">New Interests</h2>
																		
																		<?php
																		
				$sql="SELECT * FROM pdg_invite where studentid=$userid and queryid=$subid and status=1 order by id desc LIMIT 3";

				$recs=$wpdb->get_results($sql);
					
				foreach($recs as $rec){
					
				
					$tid=$rec->teacherinstid;
					
					
					$sqlx="SELECT * FROM user_data_teacher WHERE id=$tid";					
					$recx=$wpdb->get_results($sqlx);
					
					$sqlp="SELECT profile_slug FROM pdg_teacher where user_id=".$recx[0]->new_user_id;
					
					$recsy=$wpdb->get_results($sqlp);
					
					
														
																		?>
                                                                        <!-- Author Box Start -->
																		
                                                                        <li class="author_box">

                                                                            <div class="author_image">

                                                                                <div class="image_holder">

                                                                                   <!--<img src="<?= home_url() ?>/wp-

content/plugins/pedagoge_plugin/assets/images/author.jpg" alt="People">-->


<img src="<?php echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $recx[0]->new_user_id ."/profile.jpg" ?>" width="88" height="88">
	



                                                                                </div>

                                                                            </div>

                                                                            <div class="author_rating pull-right">

                                                                                <ul class="text-right">

                                                                                    <li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a 

href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-

hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                                                                    <li>28like(s)</li>
																					
																				

                                                                                    
			<li class="blue_button"><a href="<?= home_url(); ?>/teacher/<?= $recsy[0]->profile_slug ?>" target="_blank">View Profile</a></li>
																				

                                                                                    <li class="blue_button"><a href="#" class="ignoreinterest" id="<?= $tid ?>" data-id="<?= $subid ?>" >Ignore</a></li>

                                                                                    <li class="blue_button"><a class="alert_intro" href="#">Get Intro</a></li>

                                                                                </ul>

                                                                            </div>

                                                                            <div class="author_address">

                                                                                <ul>

                                                                                    <li>Name : <strong><?= $recx[0]->name; ?> </strong></li>
																					
																					<?php
																				
																				$alllocality='';
																				
																				if($recx[0]->locality!="NULL") $alllocality.=$recx[0]->locality.", ";
																				if($recx[0]->locality1!="NULL") $alllocality.=$recx[0]->locality1.", ";
																				if($recx[0]->locality2!="NULL") $alllocality.=$recx[0]->locality2.", ";
																				if($recx[0]->locality3!="NULL") $alllocality.=$recx[0]->locality3.", ";
																				
																				$alllocality=rtrim($alllocality,", ");
																				//echo $alllocality;
																				?>

                                                                                    <li>Location : <strong><?= $alllocality ?></strong></li>
																					
																					<?php
																				
																				$subjects='';
																				
																				if($recx[0]->subject_taught1!="NULL") $subjects.=$recx[0]->subject_taught1.", ";
																				if($recx[0]->subject_taught2!="NULL") $subjects.=$recx[0]->subject_taught2.", ";
																				if($recx[0]->subject_taught3!="NULL") $subjects.=$recx[0]->subject_taught3.", ";
																				if($recx[0]->subject_taught4!="NULL") $subjects.=$recx[0]->subject_taught4.", ";
																				
																				$subjects=rtrim($subjects,", ");
																				
																				
																				
																				?>

                                                                                    <li>Subject : <strong><?= $subjects ?></strong></li>

                                                                                </ul>

                                                                            </div>

                                                                            <div class="author_experience">

                                                                                <ul>

                                                                                    <li>Experience : <strong><?= $recx[0]->workexperience; ?></strong></li>

                                                                                    <li>Type of location: <strong><?= $recx[0]->hometutor ?></strong></li>

                                                                                    <li>Starting from : <strong><?= $recx[0]->feeslower; ?></strong></li>

                                                                                </ul>

                                                                            </div>

                                                                        </li>
																		
																		<?php
																		
				}
				
				?>

                                                                      
                                                                    	</ul><!-- Nested Ul -->

                                                                    

                                                                    <!-- Author Box Start -->

<!--This is for Ad Banner-->                                   	<!--<li class="author_box text-center">

                                                                    	<div class="middle_ad text-uppercase"><h1>Space for ad banner</h1></div>

                                                                    </li>-->

                                                                  
                                                                 

                                                                    <!-- Author Box Start -->
																	
																	

                                                                </ul>

                                                            </div>

                                                        </div>
														
														
<!--------------------------------------teacher--------------------------------------->




<div class="row" id="privatetutor">

                                                        	<div class="col-xs-12 author_review">

                                                            	<ul class="author_details">

                                                                

                                                                	<ul class="new_author_details pull-left"><!-- Nested Ul -->

<?php
																		
				
$sql="select * from pdg_invite join (select id from pdg_invite order by id limit 3 ) tlim on pdg_invite.id = tlim.id and pdg_invite.queryid=$subid and pdg_invite.studentid=$userid and 

pdg_invite.status=1 and pdg_invite.type='privatetutor'";

				$recs=$wpdb->get_results($sql);
					
				foreach($recs as $rec){
					
				
					$tid=$rec->teacherinstid;
					
					
					$sqlx="SELECT * FROM user_data_teacher WHERE id=$tid";					
					$recx=$wpdb->get_results($sqlx);
					
					$sqlp="SELECT profile_slug FROM pdg_teacher where user_id=".$recx[0]->new_user_id;
					
					$recsy=$wpdb->get_results($sqlp);
					
					

					
					
					
					
														
														
																		?>
                                                                        <!-- Author Box Start -->
																		
                                                                        <li class="author_box">

                                                                            <div class="author_image">

                                                                                <div class="image_holder">

                                                                                   <!--<img src="<?= home_url() ?>/wp-

content/plugins/pedagoge_plugin/assets/images/author.jpg" alt="People">-->


<img src="<?php echo plugins_url()."/pedagoge_plugin/storage/uploads/images/". $recx[0]->new_user_id ."/profile.jpg" ?>" width="88" height="88">
	



                                                                                </div>

                                                                            </div>

                                                                            <div class="author_rating pull-right">

                                                                                <ul class="text-right">

                                                                                    <li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a 

href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-

hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                                                                    <li>28like(s)</li>
																					
																				

                                                                                    
			<li class="blue_button"><a href="<?= home_url(); ?>/teacher/<?= $recsy[0]->profile_slug ?>" target="_blank">View Profile</a></li>
																				

                                                                                    <li class="blue_button"><a href="#" class="ignoreinterest" id="<?= $tid ?>" data-id="<?= $subid ?>" >Ignore</a></li>

                                                                                    <li class="blue_button"><a class="alert_intro" href="#">Get Intro</a></li>

                                                                                </ul>

                                                                            </div>

                                                                            <div class="author_address">

                                                                                <ul>

                                                                                    <li>Name : <strong><?= $recx[0]->name; ?> </strong></li>
																					
																					<?php
																				
																				$alllocality='';
																				
																				if($recx[0]->locality!="NULL") $alllocality.=$recx[0]->locality.", ";
																				if($recx[0]->locality1!="NULL") $alllocality.=$recx[0]->locality1.", ";
																				if($recx[0]->locality2!="NULL") $alllocality.=$recx[0]->locality2.", ";
																				if($recx[0]->locality3!="NULL") $alllocality.=$recx[0]->locality3.", ";
																				
																				$alllocality=rtrim($alllocality,", ");
																				//echo $alllocality;
																				?>

                                                                                    <li>Location : <strong><?= $alllocality ?></strong></li>
																					
																					<?php
																				
																				$subjects='';
																				
																				if($recx[0]->subject_taught1!="NULL") $subjects.=$recx[0]->subject_taught1.", ";
																				if($recx[0]->subject_taught2!="NULL") $subjects.=$recx[0]->subject_taught2.", ";
																				if($recx[0]->subject_taught3!="NULL") $subjects.=$recx[0]->subject_taught3.", ";
																				if($recx[0]->subject_taught4!="NULL") $subjects.=$recx[0]->subject_taught4.", ";
																				
																				$subjects=rtrim($subjects,", ");
																				
																				
																				
																				?>

                                                                                    <li>Subject : <strong><?= $subjects ?></strong></li>

                                                                                </ul>

                                                                            </div>

                                                                            <div class="author_experience">

                                                                                <ul>

                                                                                    <li>Experience : <strong><?= $recx[0]->workexperience; ?></strong></li>

                                                                                    <li>Type of location: <strong><?= $recx[0]->hometutor ?></strong></li>

                                                                                    <li>Starting from : <strong><?= $recx[0]->feeslower; ?></strong></li>

                                                                                </ul>

                                                                            </div>

                                                                        </li>
																		
																		<?php
																		
				}
				
				?>

                                                                      
                                                                    	</ul><!-- Nested Ul -->

                                                                    

                                                                    <!-- Author Box Start -->

<!--This is for Ad Banner-->                                   	<!--<li class="author_box text-center">

                                                                    	<div class="middle_ad text-uppercase"><h1>Space for ad banner</h1></div>

                                                                    </li>-->

                                                                  
                                                                 

                                                                    <!-- Author Box Start -->
																	
																	

                                                                </ul>

                                                            </div>

                                                        </div>
														
<!------------------teacher ends-------------------------------------------------------------------->														



														


	<?php	
		die;
	}
	
	function matchedtab_ajax(){
		
		?>
		
		<!--<div class="tab-pane fade" id="tab4default"><!-- Tab Five Start -->

                                                    	

                                                        

                                                        <div class="row">

                                                        	<div class="col-xs-12 author_review">

                                                            	<ul class="author_details">

                                                                <!-- Author Box Start -->

                                                                	<li class="author_box">

                                                                    	<div class="author_image">

                                                                        	<div class="image_holder">

                                                                        		<img src="<?= home_url() ?>/wp-content/plugins/pedagoge_plugin/assets/images/author.jpg" alt="People">

                                                                            </div>

                                                                        </div>

                                                                        <div class="author_rating pull-right">

                                                                        	<ul class="text-right">

                                                                            	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                                                                <li>28like(s)</li>

                                                                                <li class="blue_button"><a href="#">View Profile</a></li>

                                                                                <li class="blue_button"><a href="#">Unmatch</a></li>

                                                                            </ul>

                                                                        </div>

                                                                        <div class="author_address">

                                                                        	<ul>

                                                                            	<li>Name : <strong> Ra's Al Ghul</strong></li>

                                                                                <li>Location : <strong> China</strong></li>

                                                                                <li>Subject : <strong> Subject</strong></li>

                                                                            </ul>

                                                                        </div>

                                                                        <div class="author_experience">

                                                                        	<ul>

                                                                            	<li>Experience : <strong> 600+ years</strong></li>

                                                                                <li>Type of location: <strong> Own</strong></li>

                                                                                <li>Starting from ($) : <strong> 2500</strong></li>

                                                                            </ul>

                                                                        </div>

                                                                    </li>

                                                                    <!-- Author Box Start -->

                                                                    

                                                                    <!-- Author Box Start -->

                                                                    <!--<li class="author_box text-center">

                                                                    	<div class="middle_ad text-uppercase"><h1>Space for ad banner</h1></div>

                                                                    </li>-->

                                                                    <!-- Author Box Start -->

                                                                    <li class="author_box">

                                                                    	<div class="author_image">

                                                                        	<div class="image_holder">

                                                                        		<img src="<?= home_url() ?>/wp-content/plugins/pedagoge_plugin/assets/images/author.jpg" alt="People">

                                                                            </div>

                                                                        </div>

                                                                        <div class="author_rating pull-right">

                                                                        	<ul class="text-right">

                                                                            	<li class="rating"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a> <span>(192)</span></li>

                                                                                <li>28like(s)</li>

                                                                                <li class="blue_button"><a href="#">View Profile</a></li>

                                                                                <li class="blue_button"><a href="#">Unmatch</a></li>

                                                                            </ul>

                                                                        </div>

                                                                        <div class="author_address">

                                                                        	<ul>

                                                                            	<li>Name : <strong> Ra's Al Ghul</strong></li>

                                                                                <li>Location : <strong> China</strong></li>

                                                                                <li>Subject : <strong> Subject</strong></li>

                                                                            </ul>

                                                                        </div>

                                                                        <div class="author_experience">

                                                                        	<ul>

                                                                            	<li>Experience : <strong> 600+ years</strong></li>

                                                                                <li>Type of location: <strong> Own</strong></li>

                                                                                <li>Starting from ($) : <strong> 2500</strong></li>

                                                                            </ul>

                                                                        </div>

                                                                    </li>

                                                                    

                                                                </ul>

                                                            </div>

                                                        </div>

                                                        

                                                    <!--</div> <!-- Tab Five End -->
		
		
		<?php
		
		die;
		
	}
	
	
	
	
	function alert_intro(){
		?>
		
		<aside class="model_wrapper">

                                              

                       		<div class="modal fade" id="alertintro" name="alertintro" role="dialog" class="modal hide" data-backdrop="static" data-keyboard="false" data-dismiss="modal">

                                <div class="modal-dialog modal-dialog-next">

                                 <!-- Modal content-->

                                  <div class="modal-content blue_border">                                 

                                    

                                    <input type="hidden" id="temp">

                                    <div class="modal-footer no_padding_top_bottom">

                                   <button type="button" class="close" data-dismiss="modal">&times;</button>

								   <span class="alert_banner">Thank you for showing interest. Our representative will call you within 72 hours and get connected with teacher/institution</span>

								



<button id="btncancel" data-dismiss="modal"  



class="data_submit blue_background color_white">OK</button>

									 

									 

									 

                                    </div>

                                   

                                  </div>

                               </div>

                          </div>



                       </aside>	
		
		<?php
		die;
	}
	
	public static function update_invite_ajax(){
		
		global $wpdb;		
		$retval= $_POST['data'];
		
		$data=array();
		$a=explode("&",$retval);
	

		foreach($a as $val){
			$b=explode("=",$val);
			$data[$b[0]]=urldecode($b[1]);		
		}
		
		$queryid= $data['queryid'];
		
		$tid= $data['tid'];
		
		$sql="update pdg_invite set status='0' where queryid=$queryid and teacherinstid=$tid";
		
		$wpdb->query($sql);
		
		
		
		die();
		
	}
	
	
}
		
	?>
