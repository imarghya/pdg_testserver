<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class ControllerInstitute extends ControllerMaster implements ControllerMasterInterface {
	public function __construct() {

	}

	public function fn_construct_class() {
		parent::__construct();
		$this->title = 'Institute\'s profile ';
		$this->body_class .= ' page-register login-alt page-header-fixed';
		$this->fn_load_scripts();
		$this->fn_register_common_variables();
	}

	public function fn_load_scripts() {
		$file_version = '0.1';

		parent::fn_themes_ver2_load_scripts();
		$this->app_js['ver2_global_all'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['validationEngine_en'] = PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['rateyo'] = 'https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js';
		$this->app_js['addthis'] = 'https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-585b9d7903b68dc8';

		$this->app_js['ver2_social_login'] = PEDAGOGE_ASSETS_URL . '/js/social_login.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['ver2_modals_callback'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/modals_callback.js?ver=' . $this->fixed_version . "." . $file_version;

		$this->app_js['ver2_search_common_ui'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/search-common-ui.js?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_js['ver2_profile'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/profile.js?ver=' . $this->fixed_version . "." . $file_version;
		
		$this->css_assets['fontawesome'] = $this->registered_css['fontawesome']."?ver=".$this->fixed_version;
		$this->footer_js['notify'] = BOWER_ROOT_URL."/notifyjs/dist/notify.js?ver=".$this->fixed_version;
		
		$this->app_css['ver2_profile'] = PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/profile.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['validationEngine'] = PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css?ver=' . $this->fixed_version . "." . $file_version;
		$this->app_css['rateyo'] = 'https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css';
	}

	private function fn_register_common_variables() {
		global $wpdb;
		$current_user_db_info = '';

		$current_user_id = $this->app_data['pdg_current_user']->ID;

		if ( $current_user_id > 0 ) {
			$str_sql = "select * from view_pdg_user_info where user_id=$current_user_id";
			$current_user_db_info = $wpdb->get_results( $str_sql );

			if ( ! empty( $current_user_db_info ) ) {
				$this->app_data['current_user_db_info'] = $current_user_db_info;
			}
		}

		$str_dynamic_js = '
			<script type="text/javascript">
				var $recommended_icon = "' . PEDAGOGE_PROUI_URL . '/img/icons/recommended.png";
				var $unrecommended_icon = "' . PEDAGOGE_PROUI_URL . '/img/icons/rec.png";
			</script>
		';

		$this->app_data['dynamic_js'] = $str_dynamic_js;
	}

	public function fn_get_header() {
		global $wpdb;
		/**
		 * Issue no 65
		 * Updated for SEO Title of Institute
		 */
		$profile_slug = sanitize_text_field( $this->app_data['pdg_profile_slug'] );
		$found_institute_data = $wpdb->get_results( "select * from view_pdg_institutes where profile_slug = '$profile_slug'" );

		$this->app_data['found_institute_data'] = $found_institute_data;

		$institute_name = '';
		$seo_meta = '';
		foreach ( $found_institute_data as $institute_data ) {
			$institute_name = $institute_data->institute_name;
			$seo_meta = $institute_data->about_coaching;
		}
		if ( ! empty( $seo_meta ) ) {
			$this->app_data['seo_meta'] = $seo_meta;
		}

		if ( ! empty( $institute_name ) ) {
			$this->title = 'Pedagoge Institute\'s profile : ' . $institute_name;
		}

		return $this->load_view( 'ver2/header' );
	}

	public function fn_get_footer() {
		return $this->load_view( 'ver2/footer' );
	}


	public function fn_get_content() {
		global $wpdb;
		$str_content = '
			<br />
			<br /><br /><br /><br /><br /><br />
			<section class="site-content site-section">
                <div class="container">
		
					<div class="alert alert-warning">
						<h2>Sorry, Profile information is not available!</h2>
						<p><strong>Institute may have deactivated the profile or the profile was not approved. Please visit this profile later.</strong></p>
					</div>
				</div>
			</section>
			<br /><br /><br /><br /><br /><br />
		';

		$is_current_user_admin = current_user_can( 'manage_options' ) || current_user_can( 'view_teacher_institute_personal_info' );
		$current_user_id = $this->app_data['pdg_current_user']->ID;
		$profile_slug = sanitize_text_field( $this->app_data['pdg_profile_slug'] );

		$found_institute_id = $institute_id = '';
		$institute_user_id = '';
		$is_active = '';
		$is_approved = '';

		//$found_institute_data = $wpdb->get_results("select * from view_pdg_institutes where profile_slug = '$profile_slug'");
		$found_institute_data = $this->app_data['found_institute_data'];
		if ( empty( $found_institute_data ) ) {
			return $str_content;
		}
		foreach ( $found_institute_data as $found_institute ) {
			$found_institute_id = $found_institute->institute_id;
			$institute_user_id = $found_institute->user_id;
			$is_active = $found_institute->active;
			$is_approved = $found_institute->approved;
		}

		if ( ! $is_current_user_admin ) {
			if ( $is_active == 'no' || $is_approved == 'no' ) {
				return $str_content;
			}
		}

		$this->app_data['pdg_profile_id'] = $institute_id = $found_institute_id; //set it so that old code can access it.

		$institute_size_of_faculty = '';

		foreach ( $found_institute_data as $institute_info ) {
			$institute_size_of_faculty = $institute_info->size_of_faculty;
		}

		$institute_faculty_model = new ModelMaster( "pdg_size_of_faculty" );
		$institute_faculty_model->set_primary_key( 'size_of_faculty_id' );
		$institute_faculty_data = $institute_faculty_model->where( array (), 'size_of_faculty', $institute_size_of_faculty )->find();

		$institute_achievments_data = $wpdb->get_results( "select * from pdg_teaching_achievements where user_id=$institute_user_id and deleted='no'" );

		$tuition_batch_data = $wpdb->get_results( "select * from view_pdg_tuition_batch where tutor_institute_user_id=$institute_user_id" );

		$batches_id_array = array ();
		$teacher_about_the_course = '';
		foreach ( $tuition_batch_data as $tuition_batch ) {
			$batches_id_array[] = $tuition_batch->tuition_batch_id;
			$teacher_about_the_course = $tuition_batch->about_course;
		}

		$tutor_locality_data = '';
		$tutor_batch_fees_data = '';
		$tuition_batch_subject_data = '';
		$tuition_batch_class_timing_data = '';
		if ( count( $batches_id_array ) > 0 ) {
			$str_locality_sql = "
				select * from view_pdg_tutor_location where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")
			";


			$tutor_locality_data = $wpdb->get_results( $str_locality_sql );

			$str_batch_fees_sql = "
				select * from view_pdg_batch_fees  where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")
			";

			$tutor_batch_fees_data = $wpdb->get_results( $str_batch_fees_sql );

			$str_batch_subjects_sql = "
				select * from view_pdg_batch_subject where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")
			";

			$tuition_batch_subject_data = $wpdb->get_results( $str_batch_subjects_sql );

			$str_batch_timing_sql = "
				select * from view_pdg_batch_class_timing where tuition_batch_id in (" . implode( ',', $batches_id_array ) . ")
			";

			$tuition_batch_class_timing_data = $wpdb->get_results( $str_batch_timing_sql );

		}

		$recommended_data = '';

		$recommendations_sql = "select * from pdg_recommendations where recommended_to=$institute_user_id";
		$recommended_data = $wpdb->get_results( $recommendations_sql );

		$rating_sql = "select * from pdg_reviews where tutor_institute_user_id=$institute_user_id";
		$rating_data = $wpdb->get_results( $rating_sql );

		$this->app_data['institute_data'] = $found_institute_data;
		$this->app_data['achievement_data'] = $institute_achievments_data;
		$this->app_data['tutor_locality_data'] = $tutor_locality_data;
		$this->app_data['tuition_batch_data'] = $tuition_batch_data;

		$this->app_data['tutor_batch_fees_data'] = $tutor_batch_fees_data;
		$this->app_data['tuition_batch_subject_data'] = $tuition_batch_subject_data;
		$this->app_data['tuition_batch_class_timing_data'] = $tuition_batch_class_timing_data;
		$this->app_data['recommendations_data'] = $recommended_data;
		$this->app_data['rating_data'] = $rating_data;

		/**
		 * If found institute user id and current userid is the same or admin then load profile with personal data
		 * if found institute's user id does not match then load public profile
		 */

		if ( $is_current_user_admin || ( $current_user_id == $institute_user_id ) ) {
			// load public profile with personal data

			$permanent_address = '';
			$permanent_city = '';
			$current_address = '';
			$current_city = '';
			$mobile = '';
			$email = '';
			$contact_person_name = '';

			$institute_extra_sql = "
				select 
					pdg_user_info.*,
					p1.city_name as current_city, 
					p2.city_name as permanent_city, 
					view_pdg_institutes.user_email,
					view_pdg_institutes.contact_person_name 
				from pdg_user_info 
				left join pdg_city p1 on p1.city_id = pdg_user_info.current_address_city 
				left join pdg_city p2 on p2.city_id = pdg_user_info.permanent_address_city 
				left join view_pdg_institutes on pdg_user_info.user_id = view_pdg_institutes.user_id 
				where 
					pdg_user_info.user_id= $institute_user_id
				";

			$institute_personal_data = $wpdb->get_results( $institute_extra_sql );

			foreach ( $institute_personal_data as $institute_info ) {
				$permanent_address = $institute_info->permanent_address;
				$permanent_city = $institute_info->permanent_city;
				$current_address = $institute_info->current_address;
				$current_city = $institute_info->current_city;
				$mobile = $institute_info->mobile_no;
				$email = $institute_info->user_email;
				$contact_person_name = $institute_info->contact_person_name;
			}
			$this->app_data['private_profile'] = true;
			$this->app_data['mobile_no'] = $mobile;
			$this->app_data['user_email'] = $email;

			$this->app_data['contact_person_name'] = $contact_person_name;
			if ( empty( $contact_person_name ) ) {
				$this->app_data['contact_person_name'] = "N/A";
			}

			$this->app_data['permanent_address'] = $permanent_address;
			if ( empty( $permanent_address ) ) {
				$this->app_data['permanent_address'] = "N/A";
			}

			$this->app_data['current_address'] = $current_address;
			if ( empty( $current_address ) ) {
				$this->app_data['current_address'] = "N/A";
			}

			$this->app_data['current_city'] = $current_city;
			if ( empty( $current_city ) ) {
				$this->app_data['current_city'] = "N/A";
			}

			$this->app_data['permanent_city'] = $permanent_city;
			if ( empty( $permanent_city ) ) {
				$this->app_data['permanent_city'] = "N/A";
			}
			/**
			 * Correspondence Address City
			 */
			$correspondence_address_city_name = 'N/A';
			$correspondence_address_city_id = '';
			foreach($this->app_data['institute_data'] as $institute_info) {
				$correspondence_address_city_id = $institute_info->city_id;
			}
			if(is_numeric($correspondence_address_city_id)) {
				$correspondence_address_city_name = $wpdb->get_var("select city_name from pdg_city where city_id = $correspondence_address_city_id");
			}
			$this->app_data['address_city'] = $correspondence_address_city_name
			;
			$str_content = $this->load_view( 'public/ver2/institute/index' );

		} else {
			// load public profile
			$str_content = $this->load_view( 'public/ver2/institute/index' );
		}

		return $str_content;

	}

	/**code by pallav**/

	public static function show_interest_ajax() {

		global $wpdb;

		$return_message = array (
			'error'      => true,
			'error_type' => '',
			'message'    => ''
		);
		$is_valid_referer = check_ajax_referer( 'pedagoge', 'nonce', false );
		if ( ! $is_valid_referer ) {

			$return_message['error_type'] = 'security';
			$return_message['message'] = 'Session has expired. Please reload the page.';

			echo json_encode( $return_message );
			die();
		}

		$txt_modal_phone = sanitize_text_field( $_POST['modal_phone'] );
		$txt_modal_user_name = sanitize_text_field( $_POST['modal_user_name'] );
		$txt_modal_locality = sanitize_text_field( $_POST['modal_locality'] );
		$txt_modal_request = sanitize_text_field( $_POST['modal_request'] );

		$txt_modal_user_id = sanitize_text_field( $_POST['modal_user_id'] );


		$str_sql_email = "select `user_email` from `view_pdg_institutes` where user_id = '$txt_modal_user_id'";
		$str_sql_name = "select `institute_name` from `view_pdg_institutes` where user_id = '$txt_modal_user_id'";
		$txt_modal_email = $wpdb->get_row( $str_sql_email );
		$txt_modal_name = $wpdb->get_row( $str_sql_name );
		$txt_email = '';
		$txt_name = '';

		foreach ( $txt_modal_email as $txt_email_value ) {
			$txt_email = $txt_email_value;
		}

		foreach ( $txt_modal_name as $txt_name_value ) {
			$txt_name = $txt_name_value;
		}

		if ( empty( $txt_modal_phone ) || empty( $txt_modal_locality ) ) {

			$return_message['message'] = '<h3>Please fill the form correctly and try again!</h3>';
			echo json_encode( $return_message );
			die();
		}

		/***insert in show interest****/

		$insert_array = array (
			'pdg_show_interest_id'                      => '',
			'pdg_show_interest_teacher_institute_name'  => $txt_name,
			'pdg_show_interest_teacher_institute_email' => $txt_email,
			'pdg_show_interest_user_name'               => $txt_modal_user_name,
			'pdg_show_interest_user_phone'              => $txt_modal_phone,
			'pdg_show_interest_user_locality'           => $txt_modal_locality,
			'pdg_show_interest_user_request'            => $txt_modal_request,
			'deleted'                                   => 'no'
		);
		$wpdb->insert( 'pdg_show_interest', $insert_array, array ( '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s' ) );

		/***end of insert in showinterest***/

		$template_vars['modal_user_name'] = $txt_modal_user_name;
		$template_vars['modal_phone'] = $txt_modal_phone;
		$template_vars['modal_locality'] = $txt_modal_locality;
		$template_vars['modal_request'] = $txt_modal_request;
		$template_vars['modal_email'] = $txt_email;
		$template_vars['modal_name'] = $txt_name;

		$show_interest_header = PDGViewLoader::load_view( 'email_templates/email_header' );
		$show_interest_content = PDGViewLoader::load_view( 'email_templates/show_interest', $template_vars );
		$show_interest_footer = PDGViewLoader::load_view( 'email_templates/email_footer' );
		$show_interest_content = $show_interest_header . $show_interest_content . $show_interest_footer;


		$headers[] = 'From: Pedagoge Noreply <noreply@pedagoge.com>';
		$headers[] = 'Reply-To: Pedagoge Helpdesk <ask@pedagoge.com>';

		add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html"; ' ) );
		wp_mail( 'showinterest@pedagoge.com', 'Pedagoge User Interest Mail', $show_interest_content, $headers );

		$return_message['error'] = false;
		$return_message['message'] = '<h3>Your message was sent successfully!</h3>';


		echo json_encode( $return_message );
		die();

	}

}