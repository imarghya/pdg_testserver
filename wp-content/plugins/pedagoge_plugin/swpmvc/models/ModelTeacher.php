<?php
class ModelTeacher extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_teacher';
		$this->set_primary_key('teacher_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['teacher_id']['value'] = null;
		$columns_array['teacher_id']['type'] = '%d';
		
		$columns_array['profile_slug']['value'] = null;
		$columns_array['profile_slug']['type'] = '%s';
		
		$columns_array['user_id']['value'] = null;
		$columns_array['user_id']['type'] = '%d';
		
		$columns_array['other_communication_mode']['value'] = null;
		$columns_array['other_communication_mode']['type'] = '%s';
		
		$columns_array['other_qualification']['value'] = null;
		$columns_array['other_qualification']['type'] = '%s';
		
		$columns_array['operation_hours_from']['value'] = null;
		$columns_array['operation_hours_from']['type'] = '%s';
		
		$columns_array['operation_hours_to']['value'] = null;
		$columns_array['operation_hours_to']['type'] = '%s';
		
		$columns_array['teaching_xp_id']['value'] = null;
		$columns_array['teaching_xp_id']['type'] = '%d';
		
		$columns_array['present_place_of_work']['value'] = null;
		$columns_array['present_place_of_work']['type'] = '%s';
		
		$columns_array['profile_heading']['value'] = null;
		$columns_array['profile_heading']['type'] = '%s';
		
		$columns_array['about_coaching']['value'] = null;
		$columns_array['about_coaching']['type'] = '%s';
		
		$columns_array['students_konws_by']['value'] = null;
		$columns_array['students_konws_by']['type'] = '%s';
		
		$columns_array['installment_allowed']['value'] = null;
		$columns_array['installment_allowed']['type'] = '%s';
		
		$columns_array['about_fees_structure']['value'] = null;
		$columns_array['about_fees_structure']['type'] = '%s';
		
		$columns_array['demo_allowed']['value'] = null;
		$columns_array['demo_allowed']['type'] = '%s';
		
		$columns_array['no_of_demo_classes']['value'] = null;
		$columns_array['no_of_demo_classes']['type'] = '%d';	
		
		$columns_array['price_per_demo_class']['value'] = 'no';
		$columns_array['price_per_demo_class']['type'] = '%f';
		
		$columns_array['profile_picture_url']['value'] = null;
		$columns_array['profile_picture_url']['type'] = '%s';
		
		$columns_array['video_url']['value'] = null;
		$columns_array['video_url']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];;
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	//Get State---------------------------------------------------------
	public function get_state(){
	global $wpdb;
	$sql="SELECT * FROM pdg_state";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//------------------------------------------------------------------
	//Get user email by id----------------------------------------------
	public function get_user_email_byid($user_id){
	global $wpdb;
	$sql="SELECT user_email FROM wp_users WHERE ID='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->user_email;
	}
	//------------------------------------------------------------------
	//Get user_data_teacher---------------------------------------------
	public function get_user_data_teacher($user_id){
	global $wpdb;
	$res=array();
	$sql="SELECT a.*,b.verified,b.profile_slug,b.approved,c.date_of_birth,c.current_address,d.subjects AS te_subj,d.localities AS te_loc,d.course_age AS t_course_age,d.subject_type
	FROM user_data_teacher AS a
	LEFT OUTER JOIN pdg_teacher AS b ON a.new_user_id=b.user_id
	LEFT OUTER JOIN pdg_user_info AS c ON a.new_user_id=c.user_id
	LEFT OUTER JOIN view_pdg_teacher_extented AS d ON a.new_user_id=d.user_id
	WHERE a.new_user_id='".$user_id."'";
	$res=$wpdb->get_results($sql);
	if(empty($res)){
	$sql1="SELECT d.approved,d.localities AS locality1,d.about_coaching AS aboutcoaching,d.gender,CONCAT(d.first_name, ' ', d.last_name) AS name, d.nickname as nick_name,d.subjects AS te_subj,d.localities AS te_loc,d.course_age AS t_course_age,d.subject_type FROM view_pdg_teacher_extented AS d WHERE d.user_id='".$user_id."'";
	$res=$wpdb->get_results($sql1);
	}
	return $res;
	}
	//------------------------------------------------------------------
	//Get Teacher experience id-----------------------------------------
	public function get_teacher_expid($to_exp){
	global $wpdb;
	$sql="SELECT teaching_xp_id FROM pdg_teaching_xp WHERE teaching_xp='".$to_exp."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->teaching_xp_id;
	}
	//------------------------------------------------------------------
	//Get Locality list-------------------------------------------------
	public function get_locality_list(){
	global $wpdb;
	$sql="SELECT locality_id,locality FROM pdg_locality WHERE deleted='no' AND locality!='' GROUP BY locality";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get College list-------------------------------------------------
	public function get_college_list(){
	global $wpdb;
	$sql="SELECT college_id,college_name FROM pdg_college WHERE deleted='no' AND college_name!='' GROUP BY college_name";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get Ajax localities----------------------------------------------
	public function get_ajax_locality($search_str){
	global $wpdb;
	if($search_str==''){
	$sql="SELECT locality,locality_id FROM pdg_locality";	
	}
	else{
	$sql="SELECT locality,locality_id FROM pdg_locality WHERE locality LIKE '%".trim($search_str)."%'";	
	}
	
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get subject List-------------------------------------------------
	public function get_subject_list(){
	global $wpdb;
	$subjects = $wpdb->get_results( "select subject_name from pdg_subject_name ORDER BY subject_name_id ASC" );
	return $subjects;
	}
	//-----------------------------------------------------------------
	//Get school List-------------------------------------------------
	public function get_school_list(){
	global $wpdb;
	$schools = $wpdb->get_results( "select * from pdg_schools ORDER BY school_id ASC" );
	return $schools;
	}
	//-----------------------------------------------------------------
	//Get graduation degree--------------------------------------------
	public function get_gradution_deg($type){
	global $wpdb;
	$grd_deg = $wpdb->get_results( "select * from pdg_qualification WHERE qualification_type='".$type."' ORDER BY qualification_id ASC" );
	return $grd_deg;
	}
	//-----------------------------------------------------------------
	//Get descipilne--------------------------------------------
	public function get_descipilne(){
	global $wpdb;
	$desc = $wpdb->get_results( "select * from pdg_discipline" );
	return $desc;
	}
	//-----------------------------------------------------------------
	//Get graduation degree--------------------------------------------
	public function get_other_course(){
	global $wpdb;
	$grd_deg = $wpdb->get_results( "select * from pdg_qualification WHERE qualification_type='professional_course' ORDER BY CASE WHEN qualification = 'Others' THEN 2 ELSE 1 END, qualification" );
	return $grd_deg;
	}
	//-----------------------------------------------------------------
	//Get ajax othter degree--------------------------------------------
	public function get_ajax_other_qualification($search_str){
	global $wpdb;
	$qstr='';
	if($search_str!=''){
	$qstr="AND qualification LIKE '".$search_str."%'";	
	}
	$grd_deg = $wpdb->get_results( "select * from pdg_qualification WHERE qualification_type='professional_course' $qstr ORDER BY qualification_id ASC" );
	return $grd_deg;
	}
	//-----------------------------------------------------------------
	//Get ajax past school list--------------------------------------------
	public function get_ajax_past_school_list($search_str){
	global $wpdb;
	$qstr='';
	if($search_str!=''){
	$qstr="WHERE school_name LIKE '".$search_str."%'";	
	}
	$grd_deg = $wpdb->get_results( "select * from pdg_schools $qstr" );
	return $grd_deg;
	}
	//-----------------------------------------------------------------
	//Insert Or update user_data_teacher-------------------------------
	public function update_user_data_teacher($user_id,$email_id,$profile_heading,$name,$first_name,$last_name,$nick_name,$gender,$name_of_coaching,$about_me,$contact_no,$alt_contact_no,
	$experience,$subjects,$city,$street_no,$localicity,$landmark,$pincode,$state,$dob,$current_cv,$fees,$fees_lower,$gstin,$education,$pan,
	$graduation_det,$post_graduation_det,$other_quali_det,$prefloc,$online,$student_location,$teacher_location,$languages,$hire,$workshop,
	$past_taught_school,$achivments,$gallery_imgs,$gallery_img_hidden,$videos,$teaching_category,$classes,$collegs,$age,$teacher_reg_classorage,$class_of_teach,$college_of_teach,$plan){
	global $wpdb;
	//update pdg_user_info table------------------------------------
	$sql_user_info="UPDATE pdg_user_info SET mobile_no='".$contact_no."',alternative_contact_no='".$alt_contact_no."',date_of_birth='".$dob."',gender='".$gender."' WHERE user_id='".$user_id."'";
	$wpdb->get_results($sql_user_info);
	//--------------------------------------------------------------
	//Update pdg_teacher--------------------------------------------
	//Get teacher exp id--------------------------------------------
	$sql_sel_exp="SELECT teaching_xp_id FROM  pdg_teaching_xp WHERE teaching_xp='".$experience."'";
	$res_exp=$wpdb->get_results($sql_sel_exp);
	$exp_id=$res_exp[0]->teaching_xp_id;
	$raw_slug = $email_id.$user_id;
	$refined_slug = md5($raw_slug);
	$sql_pdg_teacher="UPDATE pdg_teacher SET profile_heading='".$profile_heading."',profile_slug='".$refined_slug."',active='yes',verified='yes',teaching_xp_id='".$exp_id."' WHERE user_id='".$user_id."'";
	$wpdb->get_results($sql_pdg_teacher);
	//--------------------------------------------------------------
	 $sql_chk="SELECT new_user_id FROM user_data_teacher WHERE new_user_id='".$user_id."'";
	 $res = $wpdb->get_results($sql_chk);
	 $subj=explode(",",$subjects);
	 $address=$street_no.','.$localicity.','.$landmark.','.$pincode.','.$state;
	 
	 //$st_loc=explode(",",$student_location);
	 $te_loc=explode(",",$teacher_location);
	 if(empty($res)){
	 $sql="INSERT INTO user_data_teacher SET new_user_id='".$user_id."',name='".$name."',nick_name='".$nick_name."',gender='".$gender."',nameofchoaching='".$name_of_coaching."',aboutcoaching='".$about_me."',
	 phone_no='".$contact_no."',altcontactnumber='".$alt_contact_no."',experience='".$experience."',subject_taught1='".@$subj[0]."',subject_taught2='".@$subj[1]."',subject_taught3='".@$subj[2]."',subject_taught4='".@$subj[3]."',subject_taught5='".@$subj[4]."',subject_taught6='".@$subj[5]."',subject_taught7='".@$subj[6]."',subject_taught8='".@$subj[7]."',
	 city='".$city."',locality='".$localicity."',landmark='".$landmark."',zipcode='".$pincode."',state='".$state."',street_no='".$street_no."',address='".$address."',current_cv='".$current_cv."',subjects='".$subjects."',
	 feesupper='".$fees."',feeslower='".$fees_lower."',gstin='".$gstin."',education='".$education."',pan='".$pan."',graduation='".$graduation_det."',post_graduation='".$post_graduation_det."',other_qualification='".$other_quali_det."',
	 hometutor='".$prefloc."',online='".$online."',locality1='".@$te_loc[0]."',locality2='".@$te_loc[1]."',locality3='".@$te_loc[2]."',language='".$languages."',
	 interest_in_hired='".$hire."',interest_in_cw='".$workshop."',past_schools='".$past_taught_school."',categoryofteaching='".$teaching_category."',classofteaching='".$class_of_teach."',classofteaching_college='".$college_of_teach."',classofteaching_age='".$age."',class_age='".$teacher_reg_classorage."',plan='".$plan."',is_new='1'";

	 /************** New Section For INSERT INTO Master Table **************/
	$sql_user_role="SELECT user_role_display FROM pdg_user_info INNER JOIN pdg_user_role ON pdg_user_info.user_role_id = pdg_user_role.user_role_id WHERE user_id='".$user_id."'";
	$result_user_role = $wpdb->get_results($sql_user_role);
	$user_role = strtolower($result_user_role[0]->user_role_display);
	//$refined_slug
	
	$sql_master = "INSERT INTO pdg_teacher_institute_master SET user_id='".$user_id."',teacher_institute_name='".$name."',user_type='".$user_role."',profile_slug='".$refined_slug."',gender='".$gender."',localities='".$teacher_location."',subjects='".$subjects."',fees='".$fees_lower."',experience='".$experience."',standard='".$class_of_teach."',location_type='".$prefloc."'";
	
	 }
	 else{
	 $sql="UPDATE user_data_teacher SET name='".$name."',nick_name='".$nick_name."',gender='".$gender."',nameofchoaching='".$name_of_coaching."',aboutcoaching='".$about_me."',
	 phone_no='".$contact_no."',altcontactnumber='".$alt_contact_no."',experience='".$experience."',subject_taught1='".$subj[0]."',subject_taught2='".$subj[1]."',subject_taught3='".$subj[2]."',subject_taught4='".$subj[3]."',subject_taught5='".@$subj[4]."',subject_taught6='".@$subj[5]."',subject_taught7='".@$subj[6]."',subject_taught8='".@$subj[7]."',
	 city='".$city."',locality='".$localicity."',landmark='".$landmark."',zipcode='".$pincode."',state='".$state."',street_no='".$street_no."',address='".$address."',current_cv='".$current_cv."',subjects='".$subjects."',
	 feesupper='".$fees."',feeslower='".$fees_lower."',gstin='".$gstin."',education='".$education."',pan='".$pan."',graduation='".$graduation_det."',post_graduation='".$post_graduation_det."',other_qualification='".$other_quali_det."',
	 hometutor='".$prefloc."',online='".$online."',locality1='".@$te_loc[0]."',locality2='".@$te_loc[1]."',locality3='".@$te_loc[2]."',language='".$languages."',
	 interest_in_hired='".$hire."',interest_in_cw='".$workshop."',past_schools='".$past_taught_school."',categoryofteaching='".$teaching_category."',classofteaching='".$class_of_teach."',classofteaching_college='".$college_of_teach."',
	 classofteaching_age='".$age."',class_age='".$teacher_reg_classorage."',plan='".$plan."',is_new='1' WHERE new_user_id='".$user_id."'";
	
	 /************** New Section For INSERT INTO Master Table **************/
	 $sql_master = "UPDATE pdg_teacher_institute_master SET teacher_institute_name='".$name."',profile_slug='".$refined_slug."',gender='".$gender."',localities='".$teacher_location."',subjects='".$subjects."',fees='".$fees_lower."',experience='".$experience."',standard='".$class_of_teach."',location_type='".$prefloc."' WHERE user_id='".$user_id."'";

	 }
	  $wpdb->show_errors();
	  $wpdb->get_results($sql);
	  
	  $wpdb->get_results($sql_master);
	  
	  //Update wp_user_metatable----------------------------------------------------------------------------
	  update_user_meta( $user_id, 'first_name', $first_name );
	  update_user_meta( $user_id, 'last_name', $last_name );
	  //update_user_meta( $user_id, 'nickname', $nick_name );
	  
	  $wp_user_sql="UPDATE wp_users SET display_name='".$name."' WHERE ID='".$user_id."'";
	  $wpdb->get_results($wp_user_sql);
	  //-------------------------------------------------------------------------------------------------
	  
	  //delete----------
	   $sql_loc_del="DELETE FROM pdg_search_tutor_location  WHERE tutor_institute_user_id='".$user_id."'";
	    $wpdb->get_results($sql_loc_del);
	  //Insert into view_pdg_tutor_location
	  $loc_type='';
	  if($prefloc=='Own Location'){
	  $loc_type="own";	
	  }
	  elseif($prefloc=='Online'){
	  $loc_type="online";	
	  }
	  else{
	  $loc_type="student";		
	  }
	  $sql_loc_view="INSERT INTO pdg_search_tutor_location SET locality='".$teacher_location."',tutor_institute_user_id='".$user_id."',user_role_name='teacher',tuition_location_type='".$loc_type."'";
	  $wpdb->get_results($sql_loc_view);
	  
	  //update search subject------------------------------
	   //delete----------
	    $sql_sub_del="DELETE FROM pdg_search_subject  WHERE tutor_institute_user_id='".$user_id."'";
	    $wpdb->get_results($sql_sub_del);
	    foreach($subj AS $sub_name){
	    $sql_sub_in="INSERT INTO pdg_search_subject SET tutor_institute_user_id='".$user_id."',subject_name='".$sub_name."'";
	    $wpdb->get_results($sql_sub_in);
	    }
	  //--------------------------------------------------
	  
	  //update teacher achivements-------------------------------
	  $sql_del_ach="DELETE FROM pdg_teaching_achievements WHERE user_id='".$user_id."'";
	  $wpdb->get_results($sql_del_ach);
	  foreach($achivments AS $ach){
		if($ach!=''){
		$sql_ach="INSERT INTO pdg_teaching_achievements SET achievement='".$ach."',user_id='".$user_id."'";
		$wpdb->get_results($sql_ach);
		}
	  }
	  //---------------------------------------------------------
	  //Update image gallery-------------------------------------
	  $del_img="DELETE FROM pdg_user_images WHERE user_id='".$user_id."'";
	  $wpdb->get_results($del_img);
	  
	  if (!file_exists('wp-content/plugins/pedagoge_plugin/storage/uploads/images/'.$user_id.'/gallery')) {
            mkdir('wp-content/plugins/pedagoge_plugin/storage/uploads/images/'.$user_id.'/gallery', 0777, true);
          }
	 // print_r($gallery_imgs);
	  if(!empty($gallery_imgs)){
		foreach($gallery_imgs["tmp_name"] as $key=>$tmp_name){
		if($gallery_imgs["name"][$key]!=''){	
		$targetPath1 = "wp-content/plugins/pedagoge_plugin/storage/uploads/images/".$user_id."/gallery/".$gallery_imgs['name'][$key];
		move_uploaded_file($tmp_name,$targetPath1);
		$sql_img="INSERT INTO pdg_user_images SET user_id='".$user_id."',image='".$gallery_imgs['name'][$key]."'";
		$wpdb->get_results($sql_img);
		}
		}
	  }
	  if(!empty($gallery_img_hidden)){
	  foreach($gallery_img_hidden AS $img){
		if($img!=''){
		$sql_img1="INSERT INTO pdg_user_images SET user_id='".$user_id."',image='".$img."'";
		$wpdb->get_results($sql_img1);
		}
		}	
	  }
	  //---------------------------------------------------------
	  //Upload videos--------------------------------------------
	  $sql_del="DELETE FROM pdg_user_video WHERE user_id='".$user_id."'";
	  $wpdb->get_results($sql_del);
	  foreach($videos as $video){
	   if($video!=''){
	   $sql_in="INSERT INTO pdg_user_video SET video_url='".$video."',user_id='".$user_id."'";
	   $wpdb->get_results($sql_in);
	   }
	  }
	  //---------------------------------------------------------
	}
	public function update_teacher_education($user_id,$degree,$year_of_comp,$colleges,$post_degree,$post_year_of_comp,$post_colleges,$other_degrees,$other_certi,$other_certi_hidden){
	global $wpdb;
	$sql_del="DELETE FROM pdg_teacher_qualification WHERE teacher_id='".$user_id."'";
	$wpdb->get_results($sql_del);
	//For graduation---------------
	foreach($degree as $key=>$deg_id){
	if($deg_id!=0){	
	//$discip_id=$discip[$key];
	$year_comp=$year_of_comp[$key];
	$college=$colleges[$key];
	//$graduation_certi_name=$graduation_certi['name'][$key];
	//if($graduation_certi_name!=''){
	//$sourcePath1 = $graduation_certi['tmp_name'][$key];
	//$targetPath1 = "wp-content/plugins/pedagoge_plugin/storage/uploads/images/".$user_id."/".$graduation_certi['name'][$key];
	//move_uploaded_file($sourcePath1,$targetPath1);
	//}
	//else{
	//$graduation_certi_name=	$graduation_certi_hidden[$key];
	//}
	
	$sql_in_edu1="INSERT INTO pdg_teacher_qualification SET teacher_id='".$user_id."',qualification_id='".$deg_id."',comp_year='".$year_comp."',college='".$college."',type=1";
	$wpdb->get_results($sql_in_edu1);
	}
	}
	//--------------------------
	//For post Graduation-------
	foreach($post_degree as $key=>$post_deg_id){
	if($post_deg_id!=0){
	//$post_discip_id=$post_discip[$key];
	$post_year_comp=$post_year_of_comp[$key];
	$post_college=$post_colleges[$key];
	//$post_graduation_certi_name=$post_graduation_certi['name'][$key];
	//if($post_graduation_certi_name!=''){
	//$sourcePath = $post_graduation_certi['tmp_name'][$key];
	//$targetPath = "wp-content/plugins/pedagoge_plugin/storage/uploads/images/".$user_id."/".$post_graduation_certi['name'][$key];
	//move_uploaded_file($sourcePath,$targetPath);
	//}
	//else{
	//$post_graduation_certi_name=$post_graduation_certi_hidden[$key];	
	//}
	$sql_in_edu2="INSERT INTO pdg_teacher_qualification SET teacher_id='".$user_id."',qualification_id='".$post_deg_id."',comp_year='".$post_year_comp."',college='".$post_college."',type=2";
	$wpdb->get_results($sql_in_edu2);
	}
	}
	//--------------------------
	//For other degree----------
	foreach($other_degrees as $key=>$oth_deg_id){
	if(isset($other_degrees[$key])){
	//$other_certi_hidden
	$other_certi_name=$other_certi['name'][$key];
	if($other_certi_name!=''){
	$sourcePath2 = $other_certi['tmp_name'][$key];
	$targetPath2 = "wp-content/plugins/pedagoge_plugin/storage/uploads/images/".$user_id."/".$other_certi['name'][$key];
	move_uploaded_file($sourcePath2,$targetPath2);
	}
	else{
	$other_certi_name=$other_certi_hidden[$key];	
	}
	$sql_in_edu3="INSERT INTO pdg_teacher_qualification SET teacher_id='".$user_id."',qualification_id='".$oth_deg_id."',certificate='".$other_certi_name."',type=3";
	$wpdb->get_results($sql_in_edu3);
	}
	}
	//--------------------------
	}
	//-----------------------------------------------------------------
	//Get teacher education details------------------------------------
	public function get_education_details($user_id,$type){
	global $wpdb;
	$sql="SELECT * FROM pdg_teacher_qualification WHERE teacher_id='".$user_id."' AND type='".$type."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get Teacher Achivements------------------------------------------
	public function get_teacher_achivements($user_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_teaching_achievements WHERE user_id='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get Gallery Images---------------------------------------
	public function get_gallery_images($user_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_user_images WHERE user_id='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get Gallery Videos---------------------------------------
	public function get_gallery_videos($user_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_user_video WHERE user_id='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------
	//Get subject with subject type for new query-------------------
	public function get_subject_name_id($subject_name){
	global $wpdb;
	$sql="SELECT subject_name_id from pdg_subject_name where subject_name='".$subject_name."'";
	$res = $wpdb->get_results($sql);
	return $res[0]->subject_name_id;
	}
	//--------------------------------------------------------------
	//For Get course type id----------------------------------------
	public function course_type_id_arr($subject_name_id){
	global $wpdb;
	$tt=array();
	$sql="SELECT distinct course_type_id from pdg_course_category
	where subject_name_id in ('$subject_name_id') AND course_type_id !='NULL'";
	$res = $wpdb->get_results($sql);
	foreach($res AS $rr){
	$tt[]=	$rr->course_type_id;
	}
	return $tt;
	}
	//--------------------------------------------------------------
	//Get user id by slug-------------------------------------------
	public function get_user_id_byslug($profile_slug){
	global $wpdb;
	$sql="SELECT user_id FROM pdg_teacher WHERE profile_slug='".$profile_slug."'";
	$res = $wpdb->get_results($sql);
	return $res[0]->user_id;
	}
	//--------------------------------------------------------------
	//Get Slug By user id-------------------------------------------
	public function get_teacher_slug($user_id){
	global $wpdb;
	$sql="SELECT profile_slug FROM pdg_teacher WHERE user_id='".$user_id."'";
	$res = $wpdb->get_results($sql);
	return $res[0]->profile_slug;
	}
	//--------------------------------------------------------------
	//Get Teacher Qualification-------------------------------------
	public function get_teacher_qualification($user_id){
	global $wpdb;
	$sql="SELECT b.qualification FROM pdg_teacher_qualification AS a LEFT OUTER JOIN pdg_qualification AS b ON a.qualification_id=b.qualification_id WHERE a.teacher_id='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get teacher recomendation-------------------------------------
	public function get_teacher_recomen($user_id){
	global $wpdb;
	$sql="SELECT COUNT(*) AS count_reco FROM pdg_recommendations WHERE recommended_to='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->count_reco;
	}
	//--------------------------------------------------------------
	//Get Student Review--------------------------------------------
	public function get_student_review($user_id){
	global $wpdb;
	$sql="SELECT a.*,b.display_name FROM pdg_reviews AS a LEFT OUTER JOIN wp_users AS b ON a.reviewer_user_id=b.ID WHERE a.tutor_institute_user_id='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get teacher internal rating-----------------------------------
	public function get_teacher_internal_rat($user_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_teacher_internal_rating WHERE user_id='".$user_id."' ORDER BY internal_rating_date_time DESC LIMIT 1";
	$res=$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get City------------------------------------------------------
	public function get_city(){
	global $wpdb;
	$sql="SELECT * FROM pdg_city";
	$res=$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get user Name and Nick name-----------------------------------
	public function get_user_name_nick($user_id){
	global $wpdb;
	$sql="SELECT a.display_name,a.user_nicename,a.user_email,b.mobile_no FROM wp_users AS a LEFT OUTER JOIN pdg_user_info AS b ON a.ID=b.user_id WHERE a.ID='".$user_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get Languages-------------------------------------------------
	public function get_languages(){
	global $wpdb;
	$sql="SELECT * FROM pdg_laguages";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	
	//Teacher Dashboard---------------------------------------------
	//Get Subject id by subject name---------------
	public function get_subject_id($subject_name){
	global $wpdb;
	$sql="SELECT subject_name_id FROM pdg_subject_name WHERE subject_name='".$subject_name."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->subject_name_id;
	}
	//---------------------------------------------
	//Get locality id by name----------------------
	public function get_locality_id($locality){
	global $wpdb;
	$sql="SELECT locality_id FROM pdg_locality WHERE locality='".$locality."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->locality_id;
	}
	//---------------------------------------------
	//Get discoverd queries------------------------
	public function get_discovered_queries($teacher_id,$subject1,$subject2,$subject3,$locality1,$locality2,$locality3,$myhome,$tutorhome,$institute){
	global $wpdb;
	$locality_arr=array();
	if($locality1!=''){
	$locality_arr[]=$locality1;	
	}
	if($locality2!=''){
	$locality_arr[]=$locality2;	
	}
	if($locality3!=''){
	$locality_arr[]=$locality3;	
	}
	$locality=implode(",",$locality_arr);
	$loc_qstr='';
	if(!empty($locality)){
	$loc_qstr="AND a.localities IN ($locality) ";	
	}
	$sql="SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE (a.subject='".$subject1."' OR a.subject='".$subject2."' OR a.subject='".$subject3."') $loc_qstr AND (myhome='".$myhome."' OR tutorhome='".$tutorhome."' OR institute='".$institute."')
	AND a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."')
	UNION
	SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE (a.subject='".$subject1."' OR a.subject='".$subject2."' OR a.subject='".$subject3."') $loc_qstr
	AND a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."')
	UNION
	SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE (a.subject='".$subject1."' OR a.subject='".$subject2."' OR a.subject='".$subject3."') AND (myhome='".$myhome."' OR tutorhome='".$tutorhome."' OR institute='".$institute."')
	AND a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."')
	UNION
	SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE (a.subject='".$subject1."' OR a.subject='".$subject2."' OR a.subject='".$subject3."')
	AND a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."')
	UNION
	SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."') $loc_qstr
	UNION
	SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE (myhome='".$myhome."' OR tutorhome='".$tutorhome."' OR institute='".$institute."')
	AND a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."')
	UNION
	SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."')
	";
	
	//echo $sql;
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	//Chk in interest------------------------------
	public function chk_int($query_id,$teacher_id){
	global $wpdb;
	$sql="SELECT COUNT(*) AS count FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND teacherinst_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->count;
	}
	//---------------------------------------------
	//Save Query-----------------------------------
	public function save_query($query_id,$teacher_id){
	global $wpdb;
	$sql="INSERT INTO pdg_saved SET teacherinstid='".$teacher_id."',queryid='".$query_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//Save Query-----------------------------------
	public function unsave_query($query_id,$teacher_id){
	global $wpdb;
	$sql="DELETE FROM pdg_saved WHERE teacherinstid='".$teacher_id."' AND queryid='".$query_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//Show Interest--------------------------------
	public function show_intersted($teacher_id,$query_id,$student_id,$min_fees,$max_fees,$fees_date,$mobile_number,$email_id,$message){
	global $wpdb;
	$sql="INSERT INTO pdg_interest_inyou SET fk_query_id='".$query_id."',fk_student_id='".$student_id."',teacherinst_id='".$teacher_id."',
	our_fees='".$min_fees."',teacher_inst_fees='".$max_fees."',available_from='".$fees_date."',mobile='".$mobile_number."',email='".$email_id."',message='".$message."'";
	$res=$wpdb->get_results($sql);
	$created_at=date('Y-m-d h:i:s a');
	$noti_text = "Teacher (".$teacher_id.") interest response received against the Query ID - ".$query_id;
	$sql_noti = "INSERT INTO pdg_admin_notification SET fk_query_id='".$query_id."',member_id='".$teacher_id."',notification='".$noti_text."',created_at='".$created_at."'";
	$res_admin_notification = $wpdb->get_results($sql_noti);

	}
	//---------------------------------------------
	//Reffer Teacher-------------------------------
	public function reffer_teacher($query_id,$teacher_by_id,$teacher_name,$mobile_number,$email_id){
		global $wpdb;
		$sql_chk="SELECT email FROM pdg_query_reffer WHERE email='".$email_id."' AND fk_query_id='".$query_id."'";
		$res_chk=$wpdb->get_results($sql_chk);
		if(!empty($res_chk)){
			return 'reffer_exist';	
		}
		else{
			$sql_chk1="SELECT ID,user_email FROM wp_users WHERE user_email='".$email_id."'";
			$res_chk1=$wpdb->get_results($sql_chk1);
			$reffer_to_id=$res_chk1[0]->ID;
			if($reffer_to_id==$teacher_by_id){
				return 'same';	
			}
			else{
				$sql="INSERT INTO pdg_query_reffer SET fk_query_id='".$query_id."',reffer_by_id='".$teacher_by_id."',name='".$teacher_name."',email='".$email_id."',phone='".$mobile_number."'";	
				$res=$wpdb->get_results($sql);
				return 'success';	
			}

			$created_at=date('Y-m-d h:i:s a');
			$noti_text = "A referral request received from Teacher (".$teacher_by_id.") for ".$teacher_name." against the Query ID - ".$query_id;
			$sql_noti = "INSERT INTO pdg_admin_notification SET fk_query_id='".$query_id."',member_id='".$teacher_id."',notification='".$noti_text."',created_at='".$created_at."'";
			$res_admin_notification = $wpdb->get_results($sql_noti);
		}
	}
	//---------------------------------------------
	//Get filtered discover queries----------------
	public function get_discover_filter_query($localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$teacher_id = $_SESSION['member_id'];
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	     $loc=implode(",",$loc_arr);
		if($loc!=''){
			$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND institute='".$place3."'";		
	}
	
	$sql="SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE a.query_status != 'CLOSED' AND a.approved = 1 AND a.id NOT IN(SELECT queryid FROM pdg_saved WHERE teacherinstid='".$teacher_id."') AND $qstr";
	//echo $sql;
	$res=$wpdb->get_results($sql);
	return $res;
	//return implode(",",$loc_arr);
	}
	//---------------------------------------------
	//--------------------------------------------------------------
	//For Saved Tab-------------------------------------------------
	public function get_saved_queries($teacher_id){
	global $wpdb;
	$sql="SELECT a.queryid,b.*,c.subject_name,d.academic_board,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_saved AS a LEFT OUTER JOIN pdg_query AS b ON a.queryid=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id 
	WHERE a.teacherinstid='".$teacher_id."'";
	//echo $sql;
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Email Verification--------------------------------------------
	public function email_verification($teacher_id){
	global $wpdb;
	$sql_chk="SELECT is_email_verified FROM user_data_teacher WHERE new_user_id='".$teacher_id."'";
	$res_chk=$wpdb->get_results($sql_chk);
	if(empty($res_chk)){
	return 1;	
	}
	elseif($res_chk[0]->is_email_verified==1){
	return 2;		
	}
	else{
	$sql="UPDATE user_data_teacher SET is_email_verified=1 WHERE new_user_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	return 3;
	}
	}
	//--------------------------------------------------------------
	//Phone Verification--------------------------------------------
	public function phone_verification($teacher_id){
	global $wpdb;
	$sql_chk="SELECT is_phone_verified FROM user_data_teacher WHERE new_user_id='".$teacher_id."'";
	$res_chk=$wpdb->get_results($sql_chk);
	if(empty($res_chk)){
	return 'No teacher found!!';	
	}
	elseif($res_chk[0]->is_phone_verified==1){
	return 'Phone already verified.';		
	}
	else{
	$sql="UPDATE user_data_teacher SET is_phone_verified=1 WHERE new_user_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	return 'Phone verification done.';
	}
	}
	//--------------------------------------------------------------
	//Get filtered saved queries----------------
	public function get_saved_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 if($subb!=''){
	  $qstr=$qstr." AND subject IN($subb)";		
	 }
	 
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND institute='".$place3."'";		
	}
	
	$sql="SELECT *,b.subject_name,c.academic_board,(SELECT querydate FROM pdg_query WHERE a.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR a.query_status='CLOSED' AND id=a.id LIMIT 1) AS past FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON a.subject=b.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	LEFT OUTER JOIN pdg_saved AS d ON a.id=d.queryid
	WHERE  d.teacherinstid='".$teacher_id."' AND $qstr";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-------------------------------------------------------------------
	//For Interested Tab-------------------------------------------------
	public function get_interest_inyou_queries($teacher_id){
	global $wpdb;
	$sql="SELECT b.*,c.subject_name,d.academic_board,a.fk_student_id,(SELECT m_id FROM pdg_matched_query WHERE teacherinst_id='".$teacher_id."' AND fk_query_id=b.id) AS matched,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_interest_inyou AS a
	LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id WHERE a.teacherinst_id='".$teacher_id."' AND b.id NOT IN(SELECT fk_query_id FROM pdg_teachins_arch_query WHERE fk_query_id=b.id AND teacherinst_id='".$teacher_id."')
	 GROUP BY b.id";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------------------------------
	//For Interest inyou tab(invite)---------------------------------------
	public function get_interest_inv_queries($teacher_id){
	global $wpdb;
	$sql="SELECT b.*,c.subject_name,d.academic_board,a.studentid,(SELECT m_id FROM pdg_matched_query WHERE teacherinst_id='".$teacher_id."' AND fk_query_id=b.id) AS matched,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_invite AS a LEFT OUTER JOIN pdg_query AS b ON a.queryid=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id WHERE a.teacherinstid='".$teacher_id."' AND b.id NOT IN(SELECT fk_query_id FROM pdg_teachins_arch_query WHERE fk_query_id=b.id AND teacherinst_id='".$teacher_id."') AND a.approved=1";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------------------------------
	//For uninterest-------------------------------------------------------
	public function un_intersted($teacher_id,$student_id,$query_id){
	global $wpdb;
	$sql="DELETE FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND teacherinst_id='".$teacher_id."' AND fk_student_id='".$student_id."'";
	$res=$wpdb->get_results($sql);

	$s_match_delete = "DELETE FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id ='".$teacher_id."'";
	$res_match_delete=$wpdb->get_results($s_match_delete);

	}
	//---------------------------------------------------------------------
	//For uninterest-------------------------------------------------------
	public function ignore_response($teacher_id,$student_id,$query_id){
	global $wpdb;
	$sql="DELETE FROM pdg_invite WHERE queryid='".$query_id."' AND teacherinstid='".$teacher_id."' AND studentid='".$student_id."'";
	$res=$wpdb->get_results($sql);

	/************* Ignore Response For Match Query **************/
	$s_match_delete = "DELETE FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id ='".$teacher_id."'";
	$res_match_delete=$wpdb->get_results($s_match_delete);

	/************* Ignore Response For Interested In You Query **************/
$s_interest_delete="DELETE FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id='".$teacher_id."'";
	$res_interest_delete=$wpdb->get_results($s_interest_delete);

	}
	//---------------------------------------------------------------------
	//---------------------------------------------------------------------
	public function get_interested_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND b.subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND b.myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND b.tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND b.institute='".$place3."'";		
	}
	
	$sql="SELECT b.*,c.subject_name,d.academic_board,a.fk_student_id,(SELECT m_id FROM pdg_matched_query WHERE teacherinst_id='".$teacher_id."' AND fk_query_id=b.id) AS matched,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_interest_inyou AS a LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id WHERE a.teacherinst_id='".$teacher_id."' AND b.id NOT IN(SELECT fk_query_id FROM pdg_teachins_arch_query WHERE fk_query_id=b.id AND teacherinst_id='".$teacher_id."') AND $qstr";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-------------------------------------------------------------------
        //Get Interest in you------------------------------------------------
	public function get_interested_in_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND b.subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND b.myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND b.tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND b.institute='".$place3."'";		
	}
	
	$sql="SELECT b.*,c.subject_name,d.academic_board,a.studentid,(SELECT m_id FROM pdg_matched_query WHERE teacherinst_id='".$teacher_id."' AND fk_query_id=b.id) AS matched,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_invite AS a LEFT OUTER JOIN pdg_query AS b ON a.queryid=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id WHERE a.teacherinstid='".$teacher_id."' AND b.id NOT IN(SELECT fk_query_id FROM pdg_teachins_arch_query WHERE fk_query_id=b.id AND teacherinst_id='".$teacher_id."') AND $qstr";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	//For archive query----------------------------------------------------
	public function query_archive($teacher_id,$query_id){
	global $wpdb;
	$sql="INSERT INTO pdg_teachins_arch_query SET fk_query_id='".$query_id."',teacherinst_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);

	/**************** Query Archive *****************/
	$sql_update_query="UPDATE pdg_query SET archived='1' WHERE id='".$query_id."'";
	$res_update_query=$wpdb->get_results($sql_update_query);
	}
	//---------------------------------------------------------------------
	//For teacher unarchive query------------------------------------------
	public function query_unarchive($teacher_id,$query_id){
	global $wpdb;
	$sql="DELETE FROM pdg_teachins_arch_query WHERE fk_query_id='".$query_id."' AND teacherinst_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------------------------------
	//For Recomended teacher by student------------------------------------
	public function recomended_teacher($idd,$teacher_id,$student_id){
	global $wpdb;
	if($idd==0){
	$sql_chk="SELECT recommendation_id FROM pdg_recommendations WHERE recommended_by='".$student_id."' AND recommended_to='".$teacher_id."'";
	$res_chk=$wpdb->get_results($sql_chk);
	if(empty($res_chk)){
	 $sql="INSERT INTO pdg_recommendations SET recommended_by='".$student_id."',recommended_to='".$teacher_id."'";
	 $wpdb->get_results($sql);
	}
	}
	else{
	$sql="DELETE FROM pdg_recommendations WHERE recommended_by='".$student_id."' AND recommended_to='".$teacher_id."'";
	$wpdb->get_results($sql);
	}
	}
	//---------------------------------------------------------------------
	//Get Referrer By You--------------------------------------------------
	public function get_referr_by_you($teacher_id){
	global $wpdb;
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board FROM pdg_query_reffer AS a
	LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	WHERE a.reffer_by_id='".$teacher_id."' AND a.is_approve=1 ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------------------------------
	//Get Refeerals By you--------------------------------------------------
	public function get_referrals_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',b.localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',b.localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND b.subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND b.myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND b.tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND b.institute='".$place3."'";		
	}
	
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board FROM pdg_query_reffer AS a LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	WHERE a.reffer_by_id='".$teacher_id."' AND a.is_approve=1 AND $qstr ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	//Get Referrer In You--------------------------------------------------
	public function get_referr_in_you($user_email){
	global $wpdb;
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board,e.user_email,e.display_name,f.mobile_no FROM pdg_query_reffer AS a
	LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	LEFT OUTER JOIN wp_users AS e ON a.reffer_by_id=e.ID
	LEFT OUTER JOIN pdg_user_info AS f ON a.reffer_by_id=f.user_id
	WHERE a.email='".$user_email."' AND a.is_approve=1 ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------------------------------
	//Get Refeerals in you--------------------------------------------------
	public function get_referrals_inyou_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$user_email=$this->get_user_email_byid($teacher_id);
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',b.localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',b.localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND b.subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND b.myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND b.tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND b.institute='".$place3."'";		
	}
	
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board,e.display_name,f.mobile_no FROM pdg_query_reffer AS a
	LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	LEFT OUTER JOIN wp_users AS e ON a.reffer_by_id=e.ID
	LEFT OUTER JOIN pdg_user_info AS f ON a.reffer_by_id=f.user_id
	WHERE a.email='".$user_email."' AND a.is_approve=1 AND $qstr ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	//Get Teacher Archive Queries-----------------------------------------
	public function teacher_arch_queries($teacher_id){
	global $wpdb;
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board FROM pdg_teachins_arch_query AS a LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	WHERE a.teacherinst_id='".$teacher_id."' ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	//Get Archived Filter Queries--------------------------------------------------
	public function get_archived_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',b.localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',b.localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND b.subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND b.myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND b.tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND b.institute='".$place3."'";		
	}
	
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board FROM pdg_teachins_arch_query AS a LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	WHERE a.teacherinst_id='".$teacher_id."' AND  $qstr ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	
	//For Matched Tab-------------------------------------------------
	public function get_matched_queries($teacher_id){
	global $wpdb;
	$sql="SELECT a.fk_query_id,b.*,c.subject_name,d.academic_board,a.fk_student_id,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_matched_query AS a LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id WHERE a.teacherinst_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	
	//Get Matched Filter Queries--------------------------------------------------
	public function get_match_filter_query($teacher_id,$localitys,$subjects,$place1,$place2,$place3){
	global $wpdb;
	$qstr=1;
	$loc_arr=array();
	$loc_ar=explode(",",$localitys);
	if($localitys!=''){
		foreach($loc_ar AS $loc){
		$loc_arr[]=$this->get_locality_id($loc);	
		}
	        $loc=implode(",",$loc_arr);
		if($loc!=''){
		$i=1;
		$qstr=$qstr." AND ";
		foreach($loc_arr AS $lcc){
		if($i==1){
		$qstr=$qstr." find_in_set('".$lcc."',b.localities) <> 0";
		}
		else{
		$qstr=$qstr." OR find_in_set('".$lcc."',b.localities) <> 0";	
		}
		$i++;
		}
		}
	}
	$sub_arr=array();
	$sub_ar=explode(",",$subjects);
	if($subjects!=''){
	foreach($sub_ar AS $sub){
	 $sub_arr[]=$this->get_subject_name_id($sub);	
	 }
	 if(count($sub_arr)>0){
	 $subb=implode(",",$sub_arr);
	 $qstr=$qstr." AND b.subject IN($subb)";	
	 }
	}
	if($place1!=0){
	$qstr=$qstr." AND b.myhome='".$place1."'";		
	}
	if($place2!=0){
	$qstr=$qstr." AND b.tutorhome='".$place2."'";		
	}
	if($place3!=0){
	$qstr=$qstr." AND b.institute='".$place3."'";		
	}
	
	$sql="SELECT a.fk_query_id,b.*,c.subject_name,d.academic_board,a.fk_student_id,(SELECT querydate FROM pdg_query WHERE b.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR b.query_status='CLOSED' AND id=b.id LIMIT 1) AS past FROM pdg_matched_query AS a LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id WHERE a.teacherinst_id='".$teacher_id."' AND $qstr";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	//For teacher unmatch query------------------------------------------
	public function query_unmatch($teacher_id,$query_id){
	global $wpdb;
	$sql="DELETE FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND teacherinst_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------------------------------
	//Get Student Email And Phone Number-----------------------------------
	public function get_student_email_ph($student_id){
	global $wpdb;
	$sql="SELECT a.user_email,b.mobile_no,a.display_name FROM wp_users AS a
	LEFT OUTER JOIN pdg_user_info AS b ON a.ID=b.user_id
	WHERE a.ID='".$student_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------------------------------
	
	//Get All teacher notification----------------------------------------
	public function get_all_notification($teacher_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_teacher_notification WHERE teacher_id='".$teacher_id."' ORDER BY id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------------
	//Count Active notification-------------------------------------------
	public function get_count_notification($teacher_id){
	global $wpdb;
	$sql="SELECT COUNT(*) AS count FROM pdg_teacher_notification WHERE teacher_id='".$teacher_id."' AND is_view=0";
	$res=$wpdb->get_results($sql);
	return $res[0]->count;
	}
	//--------------------------------------------------------------------
	//Delete Notification-------------------------------------------------
	public function delete_notification($noti_id){
	global $wpdb;
	$sql="DELETE FROM pdg_teacher_notification WHERE id='".$noti_id."'";
	$wpdb->get_results($sql);
	}
	//--------------------------------------------------------------------
	//View notification---------------------------------------------------
	public function view_notification($teacher_id){
	global $wpdb;
	$sql="UPDATE pdg_teacher_notification SET is_view=1 WHERE teacher_id='".$teacher_id."' AND is_view=0";
	$wpdb->get_results($sql);
	}
	//--------------------------------------------------------------------

	//Save Query View-----------------------------------
	public function save_query_view($query_id,$user_id,$user_role){
		global $wpdb;

		$chk_sql = "SELECT * FROM pdg_query_view WHERE query_id='".$query_id."'AND user_id='".$user_id."' AND user_type='".$user_role."'";

		$resul_chk = $wpdb->get_results($chk_sql);

		if(empty($resul_chk))
		{
			$sql="INSERT INTO pdg_query_view SET query_id='".$query_id."',user_id='".$user_id."',user_type='".$user_role."'";
			$res=$wpdb->get_results($sql);	
		}
	}

	/****************GET UNIVERSITY AND BOARD NAME*********************/
	public function get_university_name($id){
		
		global $wpdb;

		$sql = "SELECT university_name FROM pdg_university WHERE id='".$id."'";

		$result = $wpdb->get_results($sql);

		return $result[0]->university_name;
	}

	public function get_board_name($id){
		
		global $wpdb;

		$sql = "SELECT academic_board FROM pdg_academic_board WHERE academic_board_id='".$id."'";

		$result = $wpdb->get_results($sql);

		return $result[0]->academic_board;

	}
	/****************END OF GET UNIVERSITY AND BOARD NAME*********************/
}
