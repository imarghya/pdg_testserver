<?php

class ModelTRP extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_trp';
		$this->set_primary_key('trp_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['trp_id']['value'] = null;
		$columns_array['trp_id']['type'] = '%d';

		$columns_array['trp_teacher_institute_name']['value'] = null;
		$columns_array['trp_teacher_institute_name']['type'] = '%s';
		
		$columns_array['trp_teacher_institute_phone']['value'] = null;
		$columns_array['trp_teacher_institute_phone']['type'] = '%s';
		
		$columns_array['trp_teacher_institute_email']['value'] = null;
		$columns_array['trp_teacher_institute_email']['type'] = '%s';

		$columns_array['trp_teacher_institute_subjects_taught']['value'] = null;
		$columns_array['trp_teacher_institute_subjects_taught']['type'] = '%s';
		
		
		
		$columns_array['trp_teacher_institute_locality']['value'] = null;
		$columns_array['trp_teacher_institute_locality']['type'] = '%s';
		
		$columns_array['trp_user_name']['value'] = null;
		$columns_array['trp_user_name']['type'] = '%s';
		
		$columns_array['trp_user_phone_no']['value'] = null;
		$columns_array['trp_user_phone_no']['type'] = '%s';
		
		$columns_array['trp_user_email']['value'] = null;
		$columns_array['trp_user_email']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];;
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
}
