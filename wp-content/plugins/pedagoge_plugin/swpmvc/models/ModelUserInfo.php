<?php

class ModelUserInfo extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_user_info';
		$this->set_primary_key('personal_info_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['personal_info_id']['value'] = null;
		$columns_array['personal_info_id']['type'] = '%d';
		
		$columns_array['user_id']['value'] = null;
		$columns_array['user_id']['type'] = '%s';
		
		$columns_array['user_role_id']['value'] = null;
		$columns_array['user_role_id']['type'] = '%d';
		
		$columns_array['mobile_no']['value'] = null;
		$columns_array['mobile_no']['type'] = '%s';
		
		$columns_array['alternative_contact_no']['value'] = null;
		$columns_array['alternative_contact_no']['type'] = '%s';
		
		$columns_array['current_address']['value'] = null;
		$columns_array['current_address']['type'] = '%s';
		
		$columns_array['current_address_city']['value'] = null;
		$columns_array['current_address_city']['type'] = '%d';
		
		$columns_array['permanent_address']['value'] = null;
		$columns_array['permanent_address']['type'] = '%s';
		
		$columns_array['permanent_address_city']['value'] = null;
		$columns_array['permanent_address_city']['type'] = '%d';
		
		$columns_array['date_of_birth']['value'] = null;
		$columns_array['date_of_birth']['type'] = '%s';
		
		$columns_array['gender']['value'] = null;
		$columns_array['gender']['type'] = '%s';
		
		$columns_array['profile_picture']['value'] = null;
		$columns_array['profile_picture']['type'] = '%s';
		
		$columns_array['user_status_id']['value'] = null;
		$columns_array['user_status_id']['type'] = '%d';
		
		$columns_array['activation_key']['value'] = null;
		$columns_array['activation_key']['type'] = '%s';
		
		$columns_array['profile_activated']['value'] = null;
		$columns_array['profile_activated']['type'] = '%s';
		
		$columns_array['registration_reference']['value'] = null;
		$columns_array['registration_reference']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];;
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
}
