<?php

class ModelSubjects extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_subject_name';
		$this->set_primary_key('subject_name_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['subject_name_id']['value'] = null;
		$columns_array['subject_name_id']['type'] = '%d';
		
		$columns_array['subject_name']['value'] = null;
		$columns_array['subject_name']['type'] = '%s';
		
		

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function subject_name_info_by_id($subject_name_id) {
		$return_var = null;
		
		if(!empty($subject_name_id) && is_numeric($subject_name_id)) {
			$return_var = $this->find_one($subject_name_id);
		}
		return $return_var;
	}
	
	public function subject_name_info_by_name($subject_name) {
		
		$return_data=null;
		if(!empty($subject_name)) {
			
			$columns_array = $this->get_columns();
			$columns_array['subject_name'] = $subject_name;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
	
	
	/***Start of save function***/
	
	
	
	public function fn_save_subject_name( $subject_data_array ) {
		global $wpdb;
		
		$return_val = null;
		
		if( is_array( $subject_data_array ) ) {
			$subject_id = $subject_data_array['subject_id'];
			$subject_name = $subject_data_array['subject_name'];
			
			
			$data_array = array(
				'subject_name' => $subject_name,
				
			);
			$data_format_array = array(
				'%s',
				
			);
			if(empty($subject_id)) {
				//insert
				
				$result = $wpdb->insert($this->table, $data_array, $data_format_array);
				if(FALSE === $result) {
					$return_val = null;
				} else {
					$return_val = $wpdb->insert_id;
					
					//Running Counter//
					$running_counter_array = array(
					'subject_name_id' => $return_val,
					'counter_type' => "subject"
					);
					$running_counter_format_array = array(
						'%d',
						'%s'
					);	
					$wpdb->insert('pdg_subject_name', $running_counter_array, $running_counter_format_array);
					
					/*$activities_message_array = $this->fn_activity_log_template_array('Academic Session');
					$activities_message_array['activity_action'] = 'created';
					$activities_message_array['activity_message'] = 'A new Academic Session was created with record id '.$return_val;
					$this->fn_save_activity($activities_message_array);*/
				}
				
			} else {
				//update
				
				$where_data_array = array(
					'subject_name_id' => $subject_name_id
				);
				$where_data_format_array = array(
					'%d'
				);
				
				$result = $wpdb->update( $this->table, $data_array, $where_data_array, $data_format_array, $where_data_format_array );
				if(FALSE === $result) {
					$return_val = null;
				} else {
					$return_val = $subject_name_id;
					
					/*$activities_message_array = $this->fn_activity_log_template_array('Academic Session');
					$activities_message_array['activity_action'] = 'updated';
					$activities_message_array['activity_message'] = 'Academic Session with record id '.$return_val.' was updated.';
					$this->fn_save_activity($activities_message_array);*/
				}
			}
		}
		return $return_val;
	}
	/***End of save function****/
	
	
}

