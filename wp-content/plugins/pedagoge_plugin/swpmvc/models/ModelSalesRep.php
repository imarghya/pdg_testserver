<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Academic Classes Model
 */
class ModelSalesRep extends ModelMaster {	
			
	public function get_sales_rep(){
	global $wpdb;
	$sql="SELECT wp_users.ID, wp_users.user_nicename,wp_users.display_name
		FROM wp_users INNER JOIN wp_usermeta 
		ON wp_users.ID = wp_usermeta.user_id 
		WHERE wp_usermeta.meta_key = 'wp_capabilities' 
		AND wp_usermeta.meta_value LIKE '%sales%' 
		ORDER BY wp_users.user_nicename";
		
	$found_sales_rep = $wpdb->get_results($sql);
	return $found_sales_rep;
	}
}
