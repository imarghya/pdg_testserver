<?php

class ModelPublicInstitute extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_institutes';
		$this->set_primary_key('institute_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['institute_id']['value'] = null;
		$columns_array['institute_id']['type'] = '%d';
		
		$columns_array['institute_name']['value'] = null;
		$columns_array['institute_name']['type'] = '%s';
		
		$columns_array['profile_slug']['value'] = null;
		$columns_array['profile_slug']['type'] = '%s';
		
		$columns_array['user_id']['value'] = null;
		$columns_array['user_id']['type'] = '%d';
		
		$columns_array['correspondance_address']['value'] = null;
		$columns_array['correspondance_address']['type'] = '%s';
		
		$columns_array['city_id']['value'] = null;
		$columns_array['city_id']['type'] = '%d';
		
		
		$columns_array['mobile_no']['value'] = null;
		$columns_array['mobile_no']['type'] = '%s';
		
		$columns_array['primary_contact_no']['value'] = null;
		$columns_array['primary_contact_no']['type'] = '%s';
		
		$columns_array['alternate_contact_no']['value'] = null;
		$columns_array['alternate_contact_no']['type'] = '%s';
		
		$columns_array['size_of_faculty']['value'] = null;
		$columns_array['size_of_faculty']['type'] = '%d';
		
		$columns_array['operation_hours_from']['value'] = null;
		$columns_array['operation_hours_from']['type'] = '%s';
		
		$columns_array['operation_hours_to']['value'] = null;
		$columns_array['operation_hours_to']['type'] = '%s';
		
		$columns_array['website_url']['value'] = null;
		$columns_array['website_url']['type'] = '%s';
		
		$columns_array['profile_heading']['value'] = null;
		$columns_array['profile_heading']['type'] = '%s';
		
		$columns_array['profile_picture_url']['value'] = null;
		$columns_array['profile_picture_url']['type'] = '%s';
		
		$columns_array['class_picture_url']['value'] = null;
		$columns_array['class_picture_url']['type'] = '%d';
		
		$columns_array['video_url']['value'] = null;
		$columns_array['video_url']['type'] = '%s';
		
		$columns_array['achievements']['value'] = null;
		$columns_array['achievements']['type'] = '%s';

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function institute_info_by_id($institute_id) {
		$return_var = null;
		
		if(!empty($institute_id) && is_numeric($institute_id)) {
			$return_var = $this->find_one($institute_id);
		}
		return $return_var;
	}
	
	public function institute_info_by_name($institute_name) {
		
		$return_data=null;
		
		if(!empty($institute_name)) {
			
			$columns_array = $this->get_columns();
			$columns_array['institute_name'] = $institute_name;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
	
}
