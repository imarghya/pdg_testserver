<?php
class ModelAcademicBoard extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_academic_board';
		$this->set_primary_key('academic_board_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['academic_board_id']['value'] = null;
		$columns_array['academic_board_id']['type'] = '%d';
		
		$columns_array['academic_board']['value'] = null;
		$columns_array['academic_board']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function academic_board_info_by_id($academic_board_id) {
		$return_var = null;
		
		if(!empty($academic_board_id) && is_numeric($academic_board_id)) {
			$return_var = $this->find_one($academic_board_id);
		}
		return $return_var;
	}
	
	public function academic_board_info_by_type($academic_board) {
		
		$return_data=null;
		if(!empty($academic_board)) {
			
			$columns_array = $this->get_columns();
			$columns_array['academic_board'] = $academic_board;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
}
?>