<?php
class ModelCourseAgeCategory extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_course_age_category';
		$this->set_primary_key('course_age_category_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['course_age_category_id']['value'] = null;
		$columns_array['course_age_category_id']['type'] = '%d';
		
		$columns_array['course_age']['value'] = null;
		$columns_array['course_age']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function course_age_category_info_by_id($course_age_category_id) {
		$return_var = null;
		
		if(!empty($course_age_category_id) && is_numeric($course_age_category_id)) {
			$return_var = $this->find_one($course_age_category_id);
		}
		return $return_var;
	}
	
	public function course_age_category_info_by_name($course_age) {
		
		$return_data=null;
		if(!empty($course_age)) {
			
			$columns_array = $this->get_columns();
			$columns_array['course_age'] = $course_age;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
}
?>