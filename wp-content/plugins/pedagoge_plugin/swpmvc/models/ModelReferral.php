<?php
class ModelReferral extends ModelMaster {
		
	public function __construct() {
		//$this->table_name = 'pdg_teacher';
		//$this->set_primary_key('teacher_id');
		//$this->columns = $this->get_columns();
		//$this->columns_datatype = $this->get_columns_type();		
	}
	
	//Get Refeerals-----------------------------------------------
	public function get_referrals(){
	global $wpdb;
	$sql="SELECT a.*,b.*,c.subject_name,d.academic_board,e.display_name,a.id AS ref_id FROM pdg_query_reffer AS a
	LEFT OUTER JOIN pdg_query AS b ON a.fk_query_id=b.id
	LEFT OUTER JOIN pdg_subject_name AS c ON b.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON b.board=d.academic_board_id
	LEFT OUTER JOIN wp_users AS e ON a.reffer_by_id=e.ID ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//------------------------------------------------------------
	//Get Refeeral Details By Ref id------------------------------
	public function get_ref_details_by_id($reff_id){
	global $wpdb;
	$sql="SELECT a.*,b.user_email,c.phone_no,b.display_name FROM pdg_query_reffer AS a
	LEFT OUTER JOIN wp_users AS b ON a.reffer_by_id=b.ID
	LEFT OUTER JOIN user_data_teacher AS c ON a.reffer_by_id=c.new_user_id
	WHERE a.id='".$reff_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//------------------------------------------------------------
	//Approve Refeerals*------------------------------------------
	public function approve_refeeral($reff_id){
	global $wpdb;
	$sql="UPDATE pdg_query_reffer SET is_approve=1 WHERE id='".$reff_id."'";
	$res=$wpdb->get_results($sql);
	}
	//------------------------------------------------------------
	
	//Approve Refeerals*------------------------------------------
	public function unapprove_refeeral($reff_id){
	global $wpdb;
	$sql="UPDATE pdg_query_reffer SET is_approve=0 WHERE id='".$reff_id."'";
	$res=$wpdb->get_results($sql);
	}
	//------------------------------------------------------------
}
