<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Pedagoge Master Model
 * @todo extend this for better management. Implement function overloading.
 */
class ModelMaster extends PDGQuery {
		
	public $columns = null;
	public $columns_datatype = null;
		
	public function __construct($table_name = null) {
		if(!empty($table_name)) {
			$this->table_name = $table_name;
		}
	}
	
	/**
     * Set the columns for the table with column name and their types
     *
     * @param  array $fields (with field types)
     */
    public function set_columns(array $columns_array) {
        $this->columns = $columns_array;
		return $this;
    }
	
	/**
     * Set the columns for the table with column name and default/null values
     *
     * @param  array $fields (with values)
     */
    public function set_columns_datatype(array $columns_array) {
        $this->columns_datatype = $columns_array;
		return $this;
    }
	
	/**
	 * <deprecated> This function will not return deleted records.
	 *  
	 * @return associative array of row objects or empty resultset
	 */
	public function get_records($table_name = null) {
		
		if(!empty($table_name)) {
			$this->table_name = $table_name;
		}		
		
		if(!is_array($this->columns_array)) {
			$this->columns_array = array();	
		}
		
		$this->columns_array['deleted'] = 'no';
		
		$data = $this->where($this->columns_array)->find();
		
		return $data;
	}

	/**
	 * <deprecated> Direct usage of this function will return all row objects including deleted records.
	 * 
	 * Deprecated function. Do not Use it. Use ORM's Functions.
	 * 
	 * @param $table_name (String) - Table/View Name.
	 * 
	 * @param $columns_array (Array) - Array of Column Names of a table.
	 * 
	 * @return associative array of row objects or empty resultset
	 */
	public function get_records_by_table_name( $table_name, $columns_array = null ) {
		global $wpdb;
		
		$this->table_name = $table_name;
		$data = '';
		if(!empty($columns_array)) {
			$data = $this->where($columns_array)->find();			
		} else {
			$this->all();
		}		
		return $data;
	}
	
	
	public function find_one_by($property, $value) {
        global $wpdb;
		
		// Escape the property
		$property = esc_sql($property);
		
        // Escape the value
        $value = esc_sql($value);

        // Get the table name
        $table = $this->table_name;
		
		//$query = $this->where(array($property=>$value));
		$str_query = "SELECT * FROM `{$table}` WHERE `{$property}` = '{$value}'";
		
        // Get the item
        $obj = $wpdb->get_row($str_query);

        // Return false if no item was found, or a new model
        return $obj;
    }
	
	/**
     * Find a specific model by it's unique ID.
     *
     * @param  integer $id
     * @return false|self
     */
    public function find_one($id) {
    	
		if(empty($this->primary_key)) {
			
			return FALSE;
		}
		
    	$id = esc_sql($id);
    	return $this->find_one_by($this->primary_key, $id);
    }
	
    public function all() {
        global $wpdb;

        // Get the table name
        $table = $this->table_name;

        // Get the items
        $results = $wpdb->get_results("SELECT * FROM $table where deleted='no'");

        return $results;
    }
	
	public function purge( $id ) {
		global $wpdb;
		
		$return_vars = array(
			'error' => FALSE,
			'error_desc' => '',
			'result' => ''
		);
		
		if(!is_numeric($id)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'ID Field is not a number.';
			return $return_vars;
		}
		
		if(empty($this->primary_key)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Primary key is not set for the Model';
			return $return_vars;
		}
		
		$where_array = array(
			$this->primary_key => $id
		);
		
		$where_format_array = array(
			'%d'
		);
				
		$result = $wpdb->delete($this->table_name, $where_array, $where_format_array);
				
		if(FALSE === $result) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Database Error! Record with ID '.$id.' was not purged from the database.';
			return $return_vars;
		} else {
			$return_vars['error'] = FALSE;
			$return_vars['result'] = 'Record with ID '.$id.' was successfully purged from the database.';
			return $return_vars;
		}		
	}
	
	public function delete( $id ) {
		global $wpdb;		
		
		$return_vars = array(
			'error' => FALSE,
			'error_desc' => '',
			'result' => ''
		);
		
		if(!is_numeric($id)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'ID Field is not a number.';
			return $return_vars;
		}
		
		if(empty($this->primary_key)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Primary key is not set for the Model';
			return $return_vars;
		}
		
		$data_array = array(
			'deleted' => 'yes'
		);
		$data_format_array = array(
			'%s'
		);
		
		$where_data_array = array(
			$this->primary_key => $id
		);
		$where_data_format_array = array(
			'%d'
		);
		
		$result = $wpdb->update( $this->table_name, $data_array, $where_data_array, $data_format_array, $where_data_format_array );
		
		if(FALSE === $result) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Database Error! Record with ID '.$id.' could not be deleted.';
			return $return_vars;
		} else {
			$return_vars['error'] = FALSE;
			$return_vars['result'] = 'Record with ID '.$id.' was deleted successfully.';
			return $return_vars;
		}
	}

	/**
	 * requirements - primary_key, table_name, 
	 * not_duplicate column_name=>data array
	 */
	public function check_duplicate(array $not_duplicate, $id=null) {
		
		$is_duplicate = FALSE;
		$primary_key_column = $this->primary_key;
		if(empty($id)) {
			$duplicate_data = $this->where_any($not_duplicate)
									->find();			
			$is_duplicate = empty($duplicate_data) ? FALSE : TRUE;
		} else {
			$duplicate_data = $this->where_any($not_duplicate)
									->where_not(array($primary_key_column=>$id))
									->find();
			$is_duplicate = empty($duplicate_data) ? FALSE : TRUE;
		}	
		
		return $is_duplicate;
	}
	
	/**
	 * insert data
	 */
	public function save(array $values_array, array $not_duplicate = array()) {
		global $wpdb;
		$return_vars = array(
			'error' => FALSE,
			'error_desc' => '',
			'result' => '',
			'insert_id' => ''
		);		
		
		if(empty($values_array)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Data not available.';
			return $return_vars;
		}
		
		if(empty($this->columns_datatype)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Columns datatype array is not set.';
			return $return_vars;
		}		
		
		// Check Duplicate
		//insert
		if(!empty($not_duplicate)) {
			$is_duplicate = $this->check_duplicate($not_duplicate);
			if($is_duplicate) {
				$return_vars['error'] = TRUE;
				$return_vars['error_desc'] = 'Duplicate data available.';
				return $return_vars;
			}
		}
		
		$data_array = $values_array;
		$data_format_array = array();
		foreach($values_array as $key=>$value) {
			if(array_key_exists($key, $this->columns_datatype)) {
				$data_format_array[] = $this->columns_datatype[$key];
			} else {
				$data_format_array[] = "%s";
			}
		}

		
		$result = $wpdb->insert($this->table_name, $data_array, $data_format_array);
		
		if(FALSE === $result) {
			//log error
			pedagoge_applog('DB Insert Error occured at '.__FUNCTION__.' class '.__CLASS__);
			pedagoge_applog('Calling class '.get_class($this));
			//pedagoge_applog(print_r($wpdb->error, TRUE));
			
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Data could not be inserted into the database.';
			return $return_vars;
			
		} else {			
			$return_vars['error'] = FALSE;
			$return_vars['result'] = 'Data was inserted successfully!';
			$return_vars['insert_id'] = $wpdb->insert_id;
			return $return_vars;			
		}
	}	
	
	/**
	 * Update Data
	 */
	public function update(array $values_array, $where_id, array $not_duplicate = array()) {
		global $wpdb;
		$return_vars = array(
			'error' => FALSE,
			'error_desc' => '',
			'result' => '',
			'updated_id' => ''
		);
		
		if(empty($this->primary_key)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Primary key was not set.';
			return $return_vars;
		}
		
		if(empty($values_array)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Data not available.';
			return $return_vars;
		}
		
		if(empty($this->columns_datatype)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Columns datatype array is not set.';
			return $return_vars;
		}

		if(empty($where_id) || !is_numeric($where_id)) {
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Record ID is not set for updating the record.';
			return $return_vars;
		}		
		
		// Check Duplicate
		//insert
		if(!empty($not_duplicate)) {
			$is_duplicate = $this->check_duplicate($not_duplicate, $where_id);
			if($is_duplicate) {
				$return_vars['error'] = TRUE;
				$return_vars['error_desc'] = 'Duplicate data available.';
				return $return_vars;
			}
		}

		$data_array = $values_array;
		$data_format_array = array();
		foreach($values_array as $key=>$value) {
			if(array_key_exists($key, $this->columns_datatype)) {
				$data_format_array[] = $this->columns_datatype[$key];
			} else {
				$data_format_array[] = "%s";
			}
		}
		
		$where_data_array = array(
			$this->primary_key => $where_id
		);
		$where_data_format_array = array(
			'%d'
		);
		
		$result = $wpdb->update( $this->table_name, $data_array, $where_data_array, $data_format_array, $where_data_format_array );
		if(FALSE === $result) {			
			//log error
			pedagoge_applog('DB update Error occured at '.__FUNCTION__.' class '.__CLASS__);
			pedagoge_applog('Calling class '.get_class($this));
			//pedagoge_applog(print_r($wpdb->error, TRUE));
			
			$return_vars['error'] = TRUE;
			$return_vars['error_desc'] = 'Data could not be updated in the database.';
			return $return_vars;
		} else {						
			$return_vars['error'] = FALSE;
			$return_vars['result'] = 'Data was updated successfully!';
			$return_vars['updated_id'] = $where_id;
			return $return_vars;
		}
	}
	
}