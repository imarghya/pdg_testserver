<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Academic Classes Model
 */
class ModelUserRole extends ModelMaster {	
			
	public function __construct() {
		$this->table_name = 'pdg_user_role';
		$this->set_primary_key('user_role_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['user_role_id']['value'] = null;
		$columns_array['user_role_id']['type'] = '%d';
		
		$columns_array['user_role_name']['value'] = null;
		$columns_array['user_role_name']['type'] = '%s';
		
		$columns_array['user_role_display']['value'] = null;
		$columns_array['user_role_display']['type'] = '%s';
		
		$columns_array['is_public_role']['value'] = null;
		$columns_array['is_public_role']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];;
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	/**
	 * returns public user roles
	 */
	public function get_public_roles() {
		
		//reset previous columns_array data
		$this->columns_array = $this->get_columns();
		
		$this->columns_array['is_public_role'] = 'yes';
		
		return $this->where($this->columns_array)->find();		
	}
	
	/**
	 * returns private user roles
	 */
	public function get_private_roles() {
		//reset previous columns_array data
		$this->columns_array = $this->get_columns();
		
		$this->columns_array['is_public_role'] = 'no';
		
		return $this->where($this->columns_array)->find();
	}
	
	/**
	 * returns all user roles public and private.
	 */
	public function get_roles() {
		//reset previous columns_array data
		$this->columns_array = $this->get_columns();
		
		$this->columns_array['deleted'] = '';
		
		return $this->where($this->columns_array)->find();		
	}	
}
