<?php

class ModelTrainer extends ModelMaster {

	public function __construct() {
		$this->table_name = 'pdg_trainer';
		$this->set_primary_key( 'trainer_id' );
		$this->columns          = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();
	}

	private function columns_definition() {
		$columns_array                        = array ();
		$columns_array['trainer_id']['value'] = null;
		$columns_array['trainer_id']['type']  = '%d';

		$columns_array['user_id']['value'] = null;
		$columns_array['user_id']['type']  = '%d';

		$columns_array['trainer_xp_comments']['value'] = null;
		$columns_array['trainer_xp_comments']['type']  = '%s';

		$columns_array['trainer_frequency']['value'] = null;
		$columns_array['trainer_frequency']['type']  = '%s';

		$columns_array['cv_path']['value'] = null;
		$columns_array['cv_path']['type']  = '%s';

		$columns_array['training_level']['value'] = null;
		$columns_array['training_level']['type']  = '%s';

		$columns_array['trainer_keywords']['value'] = null;
		$columns_array['trainer_keywords']['type']  = '%s';

		$columns_array['trainer_heading']['value'] = null;
		$columns_array['trainer_heading']['type']  = '%s';

		$columns_array['other_qualification']['value'] = null;
		$columns_array['other_qualification']['type']  = '%s';

		$columns_array['other_domains_technical']['value'] = null;
		$columns_array['other_domains_technical']['type']  = '%s';

		$columns_array['other_domains_nontechnical']['value'] = null;
		$columns_array['other_domains_nontechnical']['type']  = '%s';

		$columns_array['total_xp_id']['value'] = null;
		$columns_array['total_xp_id']['type']  = '%d';

		$columns_array['sample_course_material_path']['value'] = null;
		$columns_array['sample_course_material_path']['type']  = '%s';

		$columns_array['verified']['value'] = null;
		$columns_array['verified']['type']  = '%s';

		$columns_array['approved']['value'] = null;
		$columns_array['approved']['type']  = '%s';

		$columns_array['active']['value'] = null;
		$columns_array['active']['type']  = '%s';

		$columns_array['created']['value'] = null;
		$columns_array['created']['type']  = '%s';

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type']  = '%s';


		return $columns_array;
	}

	public function get_columns() {
		$all_columns = $this->columns_definition();

		$columns_array = array ();

		foreach ( $all_columns as $key => $value ) {
			$columns_array[ $key ] = $value['value'];;
		}

		return $columns_array;
	}

	public function get_columns_type() {

		$all_columns = $this->columns_definition();

		$columns_array = array ();

		foreach ( $all_columns as $key => $value ) {
			$columns_array[ $key ] = $value['type'];
		}

		return $columns_array;
	}

}
