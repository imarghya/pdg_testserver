<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Pedagoge Master Query
 * @author Niraj Kumar <nirajkvinit@yahoo.co.in>
 * @version 0.3
 * @updated_at 15/03/2016 
 */
class PDGQuery {	
	
    const ORDER_ASCENDING = 'ASC';
    const ORDER_DESCENDING = 'DESC';
	
    protected $limit = 0;
    protected $offset = 0;
    protected $where = array();
    protected $sort_by = '';
    protected $order = 'ASC';
    protected $search_term = null;
    protected $search_fields = array();
	
    protected $table_name;
    protected $primary_key;

    public function __construct($table_name) {
        $this->table_name = $table_name;
    }	
	
    /**
     * Return the string representation of the query.
     *
     * @return string
     */
    public function __toString() {
    	$query = $this->query_builder();
        return $query;
    }

    /**
     * Set the fields to include in the search.
     *
     * @param  array $fields
     */
    public function set_searchable_fields(array $fields) {
        $this->search_fields = $fields;
		return $this;
    }

    /**
     * Set the primary key column.
     *
     * @param string $primary_key
     */
    public function set_primary_key($primary_key) {
        $this->primary_key = $primary_key;
        $this->sort_by     = $primary_key;
		return $this;
    }

    /**
     * Set the maximum number of results to return at once.
     *
     * @param  integer $limit
     * @return self
     */
    public function limit($limit) {
        $this->limit = (int) $limit;

        return $this;
    }

    /**
     * Set the offset to use when calculating results.
     *
     * @param  integer $offset
     * @return self
     */
    public function offset($offset) {
        $this->offset = (int) $offset;

        return $this;
    }

    /**
     * Set the column we should sort by.
     *
     * @param  string $sort_by
     * @return self
     */
    public function sort_by($sort_by) {
        $this->sort_by = $sort_by;

        return $this;
    }

    /**
     * Set the order we should sort by.
     *
     * @param  string $order
     * @return self
     */
    public function order($order) {
        $this->order = $order;

        return $this;
    }
	
	private function fn_process_where($type, array $where_vars) {
		if(!empty($type) && !empty($where_vars)) {
    		foreach($where_vars as $key=>$value) {
    			$this->where[] = array('type' => $type, 'column' => $key, 'value' => $value);
    		}
    	}
	}

    /**
     * Add a `=` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where(array $where_vars, $column = null, $column_value = null) {
		if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('where', $where_vars);
        
        return $this;
    }

    /**
     * Add a `!=` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_not(array $where_vars, $column = null, $column_value = null) {
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('not', $where_vars);
        return $this;
    }

    /**
     * Add a `LIKE` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_like(array $where_vars, $column = null, $column_value = null) {
        //$this->where[] = array('type' => 'like', 'column' => $column, 'value' => $value);
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('like', $where_vars);

        return $this;
    }

    /**
     * Add a `NOT LIKE` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_not_like(array $where_vars, $column = null, $column_value = null) {
        //$this->where[] = array('type' => 'not_like', 'column' => $column, 'value' => $value);
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('not_like', $where_vars);

        return $this;
    }

    /**
     * Add a `<` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_lt(array $where_vars, $column = null, $column_value = null) {
        //$this->where[] = array('type' => 'lt', 'column' => $column, 'value' => $value);
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('lt', $where_vars);

        return $this;
    }

    /**
     * Add a `<=` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_lte(array $where_vars, $column = null, $column_value = null) {
        //$this->where[] = array('type' => 'lte', 'column' => $column, 'value' => $value);
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('lte', $where_vars);

        return $this;
    }

    /**
     * Add a `>` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_gt(array $where_vars, $column = null, $column_value = null) {
        //$this->where[] = array('type' => 'gt', 'column' => $column, 'value' => $value);
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('gt', $where_vars);

        return $this;
    }

    /**
     * Add a `>=` clause to the search query.
     *
     * @param  string $column
     * @param  string $value
     * @return self
     */
    public function where_gte(array $where_vars, $column = null, $column_value = null) {
        //$this->where[] = array('type' => 'gte', 'column' => $column, 'value' => $value);
        if(!empty($column) && !empty($column_value)) {			
			$where_vars[$column] = $column_value;	
		}
		$this->fn_process_where('gte', $where_vars);

        return $this;
    }
	
	/**
     * Add an `IN` clause to the search query.
     *
     * @param  string $column
     * @param  array  $value
     * @return self
     */
    public function where_in($column, array $in) {
        $this->where[] = array('type' => 'in', 'column' => $column, 'value' => $in);

        return $this;
    }

    /**
     * Add a `NOT IN` clause to the search query.
     *
     * @param  string $column
     * @param  array  $value
     * @return self
     */
    public function where_not_in($column, array $not_in) {
        $this->where[] = array('type' => 'not_in', 'column' => $column, 'value' => $not_in);

        return $this;
    }

    /**
     * Add an OR statement to the where clause (e.g. (var = foo OR var = bar OR
     * var = baz)).
     *
     * @param  array $where
     * @return self
     */
    public function where_any(array $where) {
        $this->where[] = array('type' => 'any', 'where' => $where);

        return $this;
    }

    /**
     * Add an AND statement to the where clause (e.g. (var1 = foo AND var2 = bar
     * AND var3 = baz)).
     *
     * @param  array $where
     * @return self
     */
    public function where_all(array $where) {
        $this->where[] = array('type' => 'all', 'where' => $where);

        return $this;
    }    

    /**
     * Get models where any of the designated fields match the given value.
     *
     * @param  string $search_term
     * @return self
     */
    public function search($search_term) {
        $this->search_term = $search_term;

        return $this;
    }	

    /**
     * Runs the same query as find, but with no limit and don't retrieve the
     * results, just the total items found.
     *
     * @return integer
     */
    public function total_count() {
        return $this->find(true);
    }

    /**
     * Compose & execute our query.
     *
     * @param  boolean $only_count Whether to only return the row count
     * @return array
     */
    public function find($only_count = false) {
        global $wpdb;

        $str_query = $this->query_builder($only_count);

		$this->query_logger($str_query);
		
		if($only_count) {
			$value = $wpdb->get_var($str_query);
			return empty($value) ? 0 : (int) $value;
		} else {
			$result = $wpdb->get_results($str_query);
			return $result;
		}
    }	
	
    /**
     * Compose the actual SQL query from all of our filters and options.
     *
     * @param  boolean $only_count Whether to only return the row count
     * @return string
     */
    public function query_builder($only_count = false) {
        	
        $table = $this->table_name;        
        $where  = '';
        $order  = '';
        $limit  = '';
        $offset = '';

        // Search
        if (!empty($this->search_term)) {
            $where .= ' AND (';

            foreach ($this->search_fields as $field) {
                $where .= '`' . $field . '` LIKE "%' . esc_sql($this->search_term) . '%" OR ';
            }

            $where = substr($where, 0, -4) . ')';
        }

        //where
        
        foreach ($this->where as $query_type) {
        				
        	switch($query_type['type']) {
				case 'where': // where					
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}					
					$where .= ' AND `' . $query_type['column'] . '` = "' . esc_sql($query_type['value']) . '"';
				break;
				case 'not': // where_not
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` != "' . esc_sql($query_type['value']) . '"';
				break;
				case 'like': // where_like
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` LIKE "' . esc_sql($query_type['value']) . '"';
				break;
				case 'not_like': // where_not_like
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` NOT LIKE "' . esc_sql($query_type['value']) . '"';
				break;
				case 'lt': // where_lt
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` < "' . esc_sql($query_type['value']) . '"';
				break;
				case 'lte': // where_lte
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` <= "' . esc_sql($query_type['value']) . '"';
				break;
				case 'gt': // where_gt
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` > "' . esc_sql($query_type['value']) . '"';
				break;
				case 'gte': // where_gte
					if(empty($query_type['value'])) {		        		
		        		continue;
		        	}
					$where .= ' AND `' . $query_type['column'] . '` >= "' . esc_sql($query_type['value']) . '"';
				break;
				case 'in': // where_in
					$where .= ' AND `' . $query_type['column'] . '` IN (';

	                foreach ($query_type['value'] as $value) {
	                    $where .= '"' . esc_sql($value) . '",';
	                }
	
	                $where = substr($where, 0, -1) . ')';
				break;
				case 'not_in': // where_not_in
					$where .= ' AND `' . $query_type['column'] . '` NOT IN (';

	                foreach ($query_type['value'] as $value) {
	                    $where .= '"' . esc_sql($value) . '",';
	                }
	
	                $where = substr($where, 0, -1) . ')';
				break;
				case 'any': // where_any
					
					$where .= ' AND (';

	                foreach ($query_type['where'] as $column => $value) {
	                    $where .= '`' . $column . '` = "' . esc_sql($value) . '" OR ';
	                }
	
	                $where = substr($where, 0, -4) . ')';
				break;
				case 'all': // where_all
					$where .= ' AND (';

	                foreach ($query_type['where'] as $column => $value) {
	                    $where .= '`' . $column . '` = "' . esc_sql($value) . '" AND ';
	                }
	
	                $where = substr($where, 0, -5) . ')';
				break;
        	}
		}

        // Finish where clause
        if (!empty($where)) {
            $where = ' WHERE ' . substr($where, 5);
        }

        // Order
        if (strstr($this->sort_by, '(') !== false && strstr($this->sort_by, ')') !== false) {
            // The sort column contains () so we assume its a function, therefore
            // don't quote it
            $order = ' ORDER BY ' . $this->sort_by . ' ' . $this->order;
        } else {
        	if(!empty($this->sort_by)) {
        		$order = ' ORDER BY `' . $this->sort_by . '` ' . $this->order;	
        	}            
        }

        // Limit
        if ($this->limit > 0) {
            $limit = ' LIMIT ' . $this->limit;
        }

        // Offset
        if ($this->offset > 0) {
            $offset = ' OFFSET ' . $this->offset;
        }

        // Query
        if ($only_count) {
            return "SELECT COUNT(*) FROM `{$table}`{$where}";
        }

        return "SELECT * FROM `{$table}`{$where}{$order}{$limit}{$offset}";
    }

	public function query_logger($query) {
		return '';
		$query_log_file =PEDAGOGE_PLUGIN_DIR.'storage/logs/dbqueries.log'; 
		$logger = new PedagogeLogWriter($query_log_file);
		$logger->applog($query);
		unset($logger);
	}
}