<?php

class ModelCountry extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_country';
		$this->set_primary_key('country_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['country_id']['value'] = null;
		$columns_array['country_id']['type'] = '%d';
		
		$columns_array['country_name']['value'] = null;
		$columns_array['country_name']['type'] = '%s';
		
		$columns_array['country_code']['value'] = null;
		$columns_array['country_code']['type'] = '%d';

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function country_info_by_id($country_id) {
		$return_var = null;
		
		if(!empty($country_id) && is_numeric($country_id)) {
			$return_var = $this->find_one($country_id);
		}
		return $return_var;
	}
	
	public function country_info_by_name($country_name) {
		
		$return_data=null;
		if(!empty($country_name)) {
			
			$columns_array = $this->get_columns();
			$columns_array['country_name'] = $country_name;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
	
}
