<?php

class ModelSchool extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_schools';
		$this->set_primary_key('school_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['school_id']['value'] = null;
		$columns_array['school_id']['type'] = '%d';
		
		$columns_array['school_name']['value'] = null;
		$columns_array['school_name']['type'] = '%s';
		
		$columns_array['school_city']['value'] = null;
		$columns_array['school_city']['type'] = '%d';

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
}

