<?php

class ModelViewTeachers extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'view_pdg_teachers';
		$this->set_primary_key('teacher_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['teacher_id']['value'] = null;
		$columns_array['teacher_id']['type'] = '%d';
		
		$columns_array['profile_slug']['value'] = null;
		$columns_array['profile_slug']['type'] = '%s';
		
		$columns_array['user_id']['value'] = null;
		$columns_array['user_id']['type'] = '%d';

		$columns_array['personal_info_id']['value'] = null;
		$columns_array['personal_info_id']['type'] = '%d';

		$columns_array['user_name']['value'] = null;
		$columns_array['user_name']['type'] = '%s';

		$columns_array['user_nicename']['value'] = null;
		$columns_array['user_nicename']['type'] = '%s';

		$columns_array['user_email']['value'] = null;
		$columns_array['user_email']['type'] = '%s';

		$columns_array['display_name']['value'] = null;
		$columns_array['display_name']['type'] = '%d';
		
		$columns_array['other_communication_mode']['value'] = null;
		$columns_array['other_communication_mode']['type'] = '%s';
		
		$columns_array['other_qualification']['value'] = null;
		$columns_array['other_qualification']['type'] = '%s';
		
		$columns_array['operation_hours_from']['value'] = null;
		$columns_array['operation_hours_from']['type'] = '%s';
		
		$columns_array['operation_hours_to']['value'] = null;
		$columns_array['operation_hours_to']['type'] = '%s';
		
		$columns_array['teaching_xp_id']['value'] = null;
		$columns_array['teaching_xp_id']['type'] = '%d';
		
		$columns_array['present_place_of_work']['value'] = null;
		$columns_array['present_place_of_work']['type'] = '%s';
		
		$columns_array['profile_heading']['value'] = null;
		$columns_array['profile_heading']['type'] = '%s';
		
		$columns_array['about_coaching']['value'] = null;
		$columns_array['about_coaching']['type'] = '%s';
		
		$columns_array['students_konws_by']['value'] = null;
		$columns_array['students_konws_by']['type'] = '%s';
		
		$columns_array['installment_allowed']['value'] = null;
		$columns_array['installment_allowed']['type'] = '%s';
		
		$columns_array['about_fees_structure']['value'] = null;
		$columns_array['about_fees_structure']['type'] = '%s';
		
		$columns_array['demo_allowed']['value'] = null;
		$columns_array['demo_allowed']['type'] = '%s';
		
		$columns_array['no_of_demo_classes']['value'] = null;
		$columns_array['no_of_demo_classes']['type'] = '%d';	
		
		$columns_array['price_per_demo_class']['value'] = 'no';
		$columns_array['price_per_demo_class']['type'] = '%f';
		
		$columns_array['profile_picture_url']['value'] = null;
		$columns_array['profile_picture_url']['type'] = '%s';
		
		$columns_array['video_url']['value'] = null;
		$columns_array['video_url']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];;
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}

}
