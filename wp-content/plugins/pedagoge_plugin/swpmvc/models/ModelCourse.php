<?php
class ModelCourse extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_course_type';
		$this->set_primary_key('course_type_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['course_type_id']['value'] = null;
		$columns_array['course_type_id']['type'] = '%d';
		
		$columns_array['course_type_name']['value'] = null;
		$columns_array['course_type_name']['type'] = '%s';
		
		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function course_info_by_id($course_type_id) {
		$return_var = null;
		
		if(!empty($course_type_id) && is_numeric($course_type_id)) {
			$return_var = $this->find_one($course_type_id);
		}
		return $return_var;
	}
	
	public function course_info_by_type($course_type) {
		
		$return_data=null;
		if(!empty($course_type)) {
			
			$columns_array = $this->get_columns();
			$columns_array['course_type'] = $course_type;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
}
?>