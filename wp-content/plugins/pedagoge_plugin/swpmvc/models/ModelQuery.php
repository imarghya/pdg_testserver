<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ){
die;
}

class ModelQuery extends ModelMaster {	
			
	//public function __construct() {
	//	$this->table_name = 'pdg_user_role';
	//	$this->set_primary_key('user_role_id');
	//	$this->columns = $this->get_columns();
	//	$this->columns_datatype = $this->get_columns_type();		
	//}
	
	//Get Subject list-----------------------------------------------
	public function get_subject_list_tag(){
	global $wpdb;
	$subject='';
	$subjects = $wpdb->get_results( "select subject_name from pdg_subject_name ORDER BY subject_name_id ASC" );
	foreach ( $subjects as $sub ) $subject.='"'.$sub->subject_name.'",';
	return rtrim($subject,',');	
	}
	//---------------------------------------------------------------
	//Get Locality list-----------------------------------------------
	public function get_locality_list_tag(){
	global $wpdb;
	$locality='';
	$localitys = $wpdb->get_results( "select locality from pdg_locality ORDER BY locality_id ASC" );
	foreach ( $localitys as $loc ) $locality.='"'.$loc->locality.'",';
	return rtrim($locality,',');	
	}
	//---------------------------------------------------------------
	//Get student details by query id--------------------------------
	public function get_student_details_by_qid($query_id){
	global $wpdb;
	 $sql="SELECT a.id,b.user_id,c.user_email,c.display_name FROM pdg_query AS a
	       LEFT OUTER JOIN pdg_student AS b ON a.student_id=b.user_id
	       LEFT OUTER JOIN wp_users AS c ON b.user_id = c.ID WHERE a.id='".$query_id."'";
	       $found_student_data = $wpdb->get_results($sql);
	       return $found_student_data;
	}

	//Insert Student Notification
	public function student_notification($query_id,$student_id,$type){
		global $wpdb;

		$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$query_id;
		$res_subject = $wpdb->get_results($sql_subject);
		$subject_name = $res_subject[0]->subject_name;
		$created_at=date('Y-m-d h:i:s a');
		$is_view = 0;
		if($type == 'approve')
		{
			$notification='Your Query ('.$subject_name.') has been approved by Admin';
		}
		if($type == 'disapprove')
		{
			$notification='Your Query ('.$subject_name.') has been disapproved by Admin';
		}
		if($type == "close")
		{
			$notification='Your Query ('.$subject_name.') has been closed by Admin';
		}
		if($type == "show_interest")
		{
			$notification='A Teacher has showed interest on your Query ('.$subject_name.')';
		}
		if($type == "match")
		{
			$notification='Your Query ('.$subject_name.') got matched';	
		}
		$sql_noti="INSERT INTO pdg_student_notification SET student_id='".$student_id."',notification='".$notification."',is_view='".$is_view."',created_at='".$created_at."'";
		$res_student_notification = $wpdb->get_results($sql_noti);
	}

	/*********************** GET ALL STUDENT NOTIFICATION ***********************/
	public function get_all_student_notification($student_id){
		global $wpdb;
		$sql="SELECT * FROM pdg_student_notification WHERE student_id='".$student_id."' ORDER BY id DESC";
		$res=$wpdb->get_results($sql);
		return $res;
	}

	/*********************** GET COUNT STUDENT NOTIFICATION ***********************/
	public function get_count_student_notification($student_id){
		global $wpdb;
		$sql="SELECT COUNT(*) AS count FROM pdg_student_notification WHERE student_id='".$student_id."' AND is_view=0";
		$res=$wpdb->get_results($sql);
		return $res[0]->count;
	}

	//View notification---------------------------------------------------
	public function view_notification($student_id){
	global $wpdb;
	$sql="UPDATE pdg_student_notification SET is_view=1 WHERE student_id='".$student_id."' AND is_view=0";
	$wpdb->get_results($sql);
	}
	//--------------------------------------------------------------------
	
	//Delete notification---------------------------------------------------
	public function delete_notification($noti_id){
	global $wpdb;
	$sql="DELETE FROM pdg_student_notification WHERE id='".$noti_id."'";
	//$sql="UPDATE pdg_student_notification SET is_view=1 WHERE student_id='".$student_id."' AND is_view=0";
	$wpdb->get_results($sql);
	}
	
	//---------------------------------------------------------------
	//Get queries list-----------------------------------------------
	public function get_queries($user_id,$role){
	global $wpdb;
	//if($role=='administrator' || $role=='finance'){
	if($role=='administrator'){
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.course_type_id,e.course_type,pui.mobile_no,srp.display_name AS sales_rep_name FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	LEFT OUTER JOIN wp_users AS srp ON srp.ID=a.current_sales_rep_id
	LEFT OUTER JOIN pdg_user_info AS pui ON a.student_id=pui.user_id
	LEFT OUTER JOIN pdg_course_type AS e ON d.course_type_id=e.course_type_id WHERE a.approved=0 AND a.current_sales_rep_id=0 GROUP BY a.id ORDER BY a.id DESC";
	}
	elseif($role=='finance'){
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.course_type_id,e.course_type,pui.mobile_no,srp.display_name AS sales_rep_name FROM pdg_query AS a
	INNER JOIN wp_users AS b ON a.student_id=b.ID
	INNER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	INNER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	INNER JOIN wp_users AS srp ON srp.ID=a.current_sales_rep_id
	INNER JOIN pdg_user_info AS pui ON a.student_id=pui.user_id
	INNER JOIN pdg_course_type AS e ON d.course_type_id=e.course_type_id 
	INNER JOIN pdg_inbound_payment AS pip ON a.id=pip.fk_query_id WHERE a.approved=1 AND a.current_sales_rep_id!=0 GROUP BY a.id ORDER BY a.id DESC";		
	}
	elseif($role=='sales'){
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.course_type_id,e.course_type,pui.mobile_no,srp.display_name AS sales_rep_name FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	LEFT OUTER JOIN wp_users AS srp ON srp.ID=a.current_sales_rep_id
	LEFT OUTER JOIN pdg_user_info AS pui ON a.student_id=pui.user_id
	LEFT OUTER JOIN pdg_course_type AS e ON d.course_type_id=e.course_type_id WHERE a.current_sales_rep_id='".$user_id."' GROUP BY a.id ORDER BY a.id DESC";	
	}
	//echo $sql; exit();
	$found_queriy_data = $wpdb->get_results($sql);
	return $found_queriy_data;
	}
	//--------------------------------------------------------------
	//Assign Sales Rep to query-------------------------------------
	public function assign_sales_rep($sales_rep_id,$query_ids){
	global $wpdb;
	$query_id_arr=explode(",",$query_ids);
	foreach($query_id_arr AS $query_id){
	$sql_q="UPDATE pdg_query SET current_sales_rep_id='".$sales_rep_id."',query_status='HOT' WHERE id='".$query_id."'";
	$wpdb->get_results($sql_q);
	//check exist or not-----------
	$sql_check="SELECT fk_sales_rep_id FROM pdg_sales_rep_assigned WHERE fk_sales_rep_id='".$sales_rep_id."' AND fk_query_id='".$query_id."'";
	$res=$wpdb->get_results($sql_check);
		if(empty($res)) {
		$sql_assign="INSERT INTO pdg_sales_rep_assigned SET fk_sales_rep_id='".$sales_rep_id."',fk_query_id='".$query_id."'";
		$wpdb->get_results($sql_assign);
		}
	}
	}
	//--------------------------------------------------------------
	
	//Change Query Status----------------------------------------
	public function change_query_status($query_id,$status){
	 global $wpdb;
	 $sql="UPDATE pdg_query SET query_status='".$status."' WHERE id='".$query_id."'";
	 $res=$wpdb->get_results($sql);
	}

	//Change FastPass Status----------------------------------------
	public function change_fast_pass_status($query_id,$status){
	 global $wpdb;
	 $sql="UPDATE pdg_query SET fast_pass='".$status."' WHERE id='".$query_id."'";
	 $res=$wpdb->get_results($sql);
	}
	//--------------------------------------------------------------
	//Change Follow Up Date-----------------------------------------
	public function change_follow_up_date($query_id,$follow_up_date){
	 global $wpdb;
	 $sql="UPDATE pdg_query SET follow_up_date='".$follow_up_date."' WHERE id='".$query_id."'";
	 $res=$wpdb->get_results($sql);
	}
	//--------------------------------------------------------------
	//Change Review Status------------------------------------------
	public function change_review_status($query_id,$status){
	 global $wpdb;
	 $sql="UPDATE pdg_query SET is_review='".$status."' WHERE id='".$query_id."'";
	 $res=$wpdb->get_results($sql);
	}
	//--------------------------------------------------------------
	//Query Approve-------------------------------------------------
	public function query_approve($query_ids){
		global $wpdb;
		$query_id_arr=explode(",",$query_ids);
		foreach($query_id_arr AS $query_id){
		$sql_q="UPDATE pdg_query SET approved=1,disapprove_msg='' WHERE id='".$query_id."'";
		$wpdb->get_results($sql_q);
		}
	}
	//--------------------------------------------------------------
	//Query DisApprove----------------------------------------------
	public function query_disapprove($query_ids,$message){
		global $wpdb;
		$query_id_arr=explode(",",$query_ids);
		foreach($query_id_arr AS $query_id){
		$sql_q="UPDATE pdg_query SET approved=0,query_status='CLOSED',disapprove_msg='".$message."',roc='".$message."' WHERE id='".$query_id."'";
		$wpdb->get_results($sql_q);
		}
	}
	//--------------------------------------------------------------
	
	//Query Close----------------------------------------------
	public function query_close($query_ids,$message){
		global $wpdb;
		$query_id_arr1=explode(",",$query_ids);
		$query_id_arr=array_unique($query_id_arr1);
		foreach($query_id_arr AS $query_id){
		$sql_q="UPDATE pdg_query SET approved=0,query_status='CLOSED',disapprove_msg='".$message."',roc='".$message."' WHERE id='".$query_id."'";
		$wpdb->get_results($sql_q);
		}
	}
	//--------------------------------------------------------------
	//Get Filter queries--------------------------------------------
	public function get_filter_queries($user_id,$role,$st_query_date,$end_query_date,$st_start_date,$end_start_date,$status,$header_query_status,$lead_source=''){
	global $wpdb;
	$query_str='WHERE 1 ';
	if($st_query_date!='' && $end_query_date!=''){
	$st_query_date="'".$st_query_date."'";
	$end_query_date="'".$end_query_date."'";
	$query_str .=' AND DATE_FORMAT(a.querydate,"%Y-%m-%d") BETWEEN '.$st_query_date.' AND '.$end_query_date;	
	}
	if($st_start_date!='' && $end_start_date!=''){
	$st_start_date="'".$st_start_date."'";
	$end_start_date="'".$end_start_date."'";
	$query_str .=' AND DATE_FORMAT(a.follow_up_date,"%Y-%m-%d") BETWEEN '.$st_start_date.' AND '.$end_start_date;	
	}
	if($status!='' && count($status)>0){
	$qstatus="'".implode("','",$status)."'";
	$query_str .='AND a.query_status IN ('.$qstatus.') ';
	}
	if($header_query_status!=''){
		if($header_query_status==0 || $header_query_status==1){
		 $query_str .=" AND a.approved = '".$header_query_status."' AND a.current_sales_rep_id=0 ";
		}
		else{
		$query_str .=" AND a.current_sales_rep_id !=0 ";	
		}
	}
	if($lead_source != "" && count($lead_source) > 0){
		//$leads="'".implode(",",$lead_source)."'";
		$leads = "'" . implode( "','", $lead_source ) . "'";
		$query_str .=" AND a.lead_source IN (".$leads.") ";	
	}
	//$query_str .=" ORDER BY a.id DESC";
	//if($role=='administrator' || $role=='finance'){
	if($role=='administrator'){
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.course_type_id,e.course_type,pui.mobile_no,srp.display_name AS sales_rep_name FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	LEFT OUTER JOIN wp_users AS srp ON srp.ID=a.current_sales_rep_id
	LEFT OUTER JOIN pdg_user_info AS pui ON a.student_id=pui.user_id
	LEFT OUTER JOIN pdg_course_type AS e ON d.course_type_id=e.course_type_id $query_str GROUP BY a.id ORDER BY a.id DESC";
	}
	elseif($role == 'finance'){
		$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.course_type_id,e.course_type,pui.mobile_no,srp.display_name AS sales_rep_name FROM pdg_query AS a
	INNER JOIN wp_users AS b ON a.student_id=b.ID
	INNER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	INNER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	INNER JOIN wp_users AS srp ON srp.ID=a.current_sales_rep_id
	INNER JOIN pdg_user_info AS pui ON a.student_id=pui.user_id
	INNER JOIN pdg_course_type AS e ON d.course_type_id=e.course_type_id 
	INNER JOIN pdg_inbound_payment AS pip ON a.id=pip.fk_query_id $query_str GROUP BY a.id ORDER BY a.id DESC";
	}
	elseif($role=='sales'){
	$query_str .=' AND a.current_sales_rep_id='.$user_id;
	
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.course_type_id,e.course_type,pui.mobile_no,srp.display_name AS sales_rep_name FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	LEFT OUTER JOIN wp_users AS srp ON srp.ID=a.current_sales_rep_id
	LEFT OUTER JOIN pdg_user_info AS pui ON a.student_id=pui.user_id
	LEFT OUTER JOIN pdg_course_type AS e ON d.course_type_id=e.course_type_id $query_str GROUP BY a.id ORDER BY a.id DESC";	
	}
	//echo $sql; exit();
	$found_queriy_data = $wpdb->get_results($sql);

	return $found_queriy_data;
	//echo $sql;
	//echo $qstatus;
	//print_r($status_arr);
	//exit;
	}
	//--------------------------------------------------------------
	//Get localitys from query table--------------------------------
	public function get_q_locality($loc){
	global $wpdb;
	$l=array();
	$locs=explode(",",$loc);
		foreach($locs AS $loc_id){
		$sql="SELECT locality FROM pdg_locality WHERE locality_id='".$loc_id."'";
		$res=$wpdb->get_results($sql);
		$l[]=@$res[0]->locality;
		}
	 return $l;
	}
	//--------------------------------------------------------------
	//Get Query Status----------------------------------------------
	public function get_query_status_byid($query_id){
	global $wpdb;
	$sql="SELECT query_status FROM pdg_query WHERE id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->query_status;
	}
	//--------------------------------------------------------------
	//Get Query Details For Clone-----------------------------------
	public function get_query_details($query_id){
	global $wpdb;
	$sql="SELECT *,b.display_name AS student_name,c.mobile_no,b.user_email FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_user_info AS c ON a.student_id=c.user_id
	WHERE a.id='".$query_id."'";
	
	//$sql="SELECT *,b.subject_type_id,c.subject_type FROM pdg_query AS a
	//LEFT OUTER JOIN pdg_course_category AS b ON a.subject=b.subject_name_id
	//LEFT OUTER JOIN pdg_subject_type AS c ON b.subject_type_id=c.subject_type_id
	//WHERE a.id='".$query_id."' GROUP BY a.id";
	
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get Subject list----------------------------------------------
	public function get_subject_list(){
	global $wpdb;
	$sql="SELECT subject_name_id,subject_name FROM pdg_subject_name WHERE deleted='no'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get School list----------------------------------------------
	public function get_school_list(){
	global $wpdb;
	$sql="SELECT school_id,school_name FROM pdg_schools WHERE deleted='no'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//Get College list 160118----------------------------------------------
	public function get_college_list(){
	global $wpdb;
	$sql="SELECT college_id,college_name FROM pdg_college WHERE deleted='no'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get Board list----------------------------------------------
	public function get_board_list(){
	global $wpdb;
	$sql="SELECT academic_board_id,academic_board FROM pdg_academic_board WHERE deleted='no'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//Get University list----------------------------------------------
	public function get_university_list(){
	global $wpdb;
	$sql="SELECT * FROM pdg_university WHERE deleted='no'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get Standard Type---------------------------------------------
	public function get_standard_list($subject_name_id){
	global $wpdb;
	$sql="SELECT a.subject_type FROM pdg_subject_type AS a
	LEFT OUTER JOIN pdg_course_category AS b ON a.subject_type_id=b.subject_type_id
	WHERE b.subject_name_id='".$subject_name_id."' GROUP BY a.subject_type";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Get Locality list----------------------------------------------
	public function get_locality_list(){
	global $wpdb;
	$sql="SELECT locality_id,locality FROM pdg_locality WHERE deleted='no'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//Submit Clone Query--------------------------------------------
	public function submit_clone_query($clone_student_id,$clone_subject,$clone_school,$clone_board,$clone_standard,$age,$clone_my_home,$clone_t_home,
						    $clone_institute,$clone_locality,$clone_query_date,$clone_requirement,$clone_student_name,$clone_phone_number,
						    $clone_user_email,$clone_school_type,$clone_board_type){
	global $wpdb;

	if($clone_school_type == "school"){
		$qry = 'school='.$clone_school.',college=0';
	}
	if($clone_school_type == "college"){
		$qry = 'school=0,college='.$clone_school;
	}
	if($clone_board_type == "board")
	{
		$qry_b = 'board='.$clone_board.',university=0';
	}
	if($clone_board_type == "university")
	{
		$qry_b = 'board=0,university='.$clone_board;
	}
	//insert into query table
	$sql="INSERT INTO pdg_query SET student_id='".$clone_student_id."',subject='".$clone_subject."',".$qry.",".$qry_b.",standard='".$clone_standard."',
	myhome='".$clone_my_home."',tutorhome='".$clone_t_home."',institute='".$clone_institute."',localities='".$clone_locality."',start_date='".$clone_query_date."',
	requirement='".$clone_requirement."',age='".$age."'";
	$res=$wpdb->get_results($sql);
	$query_id=$wpdb->insert_id;
	//update student
	$sql_stu_name_email="UPDATE wp_users SET display_name='".$clone_student_name."',user_email='".$clone_user_email."' WHERE ID='".$clone_student_id."'";
	$wpdb->get_results($sql_stu_name_email);
	
	$sql_stu_ph="UPDATE pdg_user_info SET mobile_no='".$clone_phone_number."' WHERE user_id='".$clone_student_id."'";
	$wpdb->get_results($sql_stu_ph);
	
	return $query_id;
	}
	//--------------------------------------------------------------
	
	//Submit Edit Query--------------------------------------------
	public function submit_edit_query($query_id,$clone_student_id,$clone_subject,$clone_school,$clone_board,$clone_standard,$age,$clone_my_home,$clone_t_home,
						    $clone_institute,$clone_locality,$clone_query_date,$clone_requirement,$clone_student_name,$clone_phone_number,
						    $clone_user_email,$clone_school_type,$clone_board_type){
	global $wpdb;

	if($clone_school_type == "school"){
		$qry = 'school='.$clone_school.',college=0';
	}
	if($clone_school_type == "college"){
		$qry = 'school=0,college='.$clone_school;
	}
	if($clone_board_type == "board")
	{
		$qry_b = 'board='.$clone_board.',university=0';
	}
	if($clone_board_type == "university")
	{
		$qry_b = 'board=0,university='.$clone_board;
	}

	//insert into query table
	$sql="UPDATE pdg_query SET subject='".$clone_subject."',".$qry.",".$qry_b.",standard='".$clone_standard."',
	myhome='".$clone_my_home."',tutorhome='".$clone_t_home."',institute='".$clone_institute."',localities='".$clone_locality."',start_date='".$clone_query_date."',
	requirement='".$clone_requirement."',age='".$age."' WHERE student_id='".$clone_student_id."' AND id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	//$query_id=$wpdb->insert_id;
	//update student
	$sql_stu_name_email="UPDATE wp_users SET display_name='".$clone_student_name."',user_email='".$clone_user_email."' WHERE ID='".$clone_student_id."'";
	$wpdb->get_results($sql_stu_name_email);
	
	$sql_stu_ph="UPDATE pdg_user_info SET mobile_no='".$clone_phone_number."' WHERE user_id='".$clone_student_id."'";
	$wpdb->get_results($sql_stu_ph);
	
	return 'success';
	}
	//--------------------------------------------------------------
	
	//Get subject with subject type for new query-------------------
	public function get_subject_with_type($subjects){
	global $wpdb;
	$sql="SELECT * from pdg_subject_name where subject_name in ($subjects)";
	$res = $wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------------
	//For Get course type id----------------------------------------
	public function course_type_id_arr($subject_name_id){
	global $wpdb;
	$tt=array();
	$sql="SELECT distinct course_type_id from pdg_course_category
	where subject_name_id in ('$subject_name_id') AND course_type_id !='NULL'";
	$res = $wpdb->get_results($sql);
	foreach($res AS $rr){
	$tt[]=	$rr->course_type_id;
	}
	return $tt;
	}
	//--------------------------------------------------------------
	//For submit new query data-------------------------------------
	public function submit_new_query_data($xx,$name,$email,$phone){
	global $wpdb;
	//$sql_chk="SELECT ID FROM wp_users WHERE user_email='".$email."'";
	$sql_chk="SELECT * FROM wp_users as wu, pdg_user_info AS pui WHERE wu.user_email='".$email."' AND pui.mobile_no = '".$phone."' AND wu.ID=pui.user_id";
	$res = $wpdb->get_results($sql_chk);
		if(empty($res)) {
		//Student Not exist
		/*$user_det=explode("@",$email);
		$user_name=$user_det[0];
		//insert into wp_users table
		$sql_insert_wpu="INSERT INTO wp_users SET user_login='".$user_name."',user_nicename='".$user_name."',user_email='".$email."',display_name='".$name."'";
		$wpdb->get_results($sql_insert_wpu);
		$user_id=$wpdb->insert_id;
		//insert into pdg_student
		$sql_insert_pdgs="INSERT INTO pdg_student SET user_id='".$user_id."',profile_slug='".$user_name."'";
		$wpdb->get_results($sql_insert_pdgs);
		//insert into pdg_user_info table
		$sql_insert_pdg_usin="INSERT INTO pdg_user_info SET user_id='".$user_id."',user_role_id=2,mobile_no='".$phone."'";
		$wpdb->get_results($sql_insert_pdg_usin);*/
		echo "fail";
		}
		else{
			//$user_id=$res[0]->ID;
			$user_id=$res[0]->ID;
			for($x=1;$x<=$xx;$x++){

				if($_SESSION['school_type'.$x] == "school"){
					$qry = 'school='.$_SESSION['school'.$x].',college=0';
				}
				/*else{
					$qry = 'school=0,college=0';	
				}*/
				else if($_SESSION['school_type'.$x] == "college"){
					$qry = 'school=0,college='.$_SESSION['school'.$x];
				}
				else{
					$qry = 'school=0,college=0';	
				}
				if($_SESSION['board_type'.$x] == "board")
				{
					$qry_b = 'board='.$_SESSION['board'.$x].',university=0';
				}
				/*else{
					$qry_b = 'board=0,university=0';	
				}*/
				else if($_SESSION['board_type'.$x] == "university")
				{
					$qry_b = 'board=0,university='.$_SESSION['board'.$x];
				}
				else{
					$qry_b = 'board=0,university=0';	
				}

			//$_SESSION['subject'.$x];
				$insert_pdg_query="INSERT INTO pdg_query SET student_id='".$user_id."',subject='".$_SESSION['subject'.$x]."',".$qry.",".$qry_b.",standard='".$_SESSION['standard'.$x]."',myhome='".$_SESSION['myhome'.$x]."',tutorhome='".$_SESSION['tutorhome'.$x]."',institute='".$_SESSION['institute'.$x]."',localities='".$_SESSION['localities'.$x]."',start_date='".$_SESSION['start_date'.$x]."',requirement='".$_SESSION['requirement'.$x]."',age='".$_SESSION['age'.$x]."',archived=1";
				
				$wpdb->get_results($insert_pdg_query);
			}
		}
		//Insert into pdg_query table
		/*for($x=1;$x<=$xx;$x++){
		//$_SESSION['subject'.$x];
		$insert_pdg_query="INSERT INTO pdg_query SET student_id='".$user_id."',subject='".$_SESSION['subject'.$x]."',school='".$_SESSION['school'.$x]."',
		board='".$_SESSION['board'.$x]."',standard='".$_SESSION['standard'.$x]."',myhome='".$_SESSION['myhome'.$x]."',tutorhome='".$_SESSION['tutorhome'.$x]."',
		institute='".$_SESSION['institute'.$x]."',localities='".$_SESSION['localities'.$x]."',start_date='".$_SESSION['start_date'.$x]."',
		requirement='".$_SESSION['requirement'.$x]."',age='".$_SESSION['age'.$x]."',archived=1";
		$wpdb->get_results($insert_pdg_query);
		}*/
	}
	//--------------------------------------------------------------
	//Get Query Details for Query View Page-----------------
	public function get_query_details_view($query_id){
	global $wpdb;
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,d.subject_name_id,b.user_email,d.course_type_id,e.mobile_no,f.academic_board
	FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	LEFT OUTER JOIN pdg_user_info AS e ON a.student_id=e.user_id
	LEFT OUTER JOIN pdg_academic_board AS f ON a.board=f.academic_board_id
	WHERE a.id='".$query_id."' GROUP BY a.id ORDER BY a.id DESC";
	//echo $sql; exit();
	$res=$wpdb->get_results($sql);
	return $res;
	}
	
	//Function for Student Dashboard****************************************************************************************************
	//Get student details---------------------
	public function get_student_details($user_id){
	global $wpdb;
	$sql="SELECT a.user_email,a.display_name,b.mobile_no FROM wp_users AS a
	LEFT OUTER JOIN pdg_user_info AS b ON a.ID=b.user_id WHERE a.ID='".$user_id."'";
	$found_student_data = $wpdb->get_results($sql);
	return $found_student_data;
	}
	//----------------------------------------
	//Submit student new query subject data---
	public function submit_stu_subject_data($user_id,$subject,$age,$school,$board,$standard,$myhome,$tutorhome,$institute,$localities,$start_date,$requirement,$school_type,$board_type,$query_id=''){
		global $wpdb;
	/*$insert_pdg_query="INSERT INTO pdg_query SET student_id='".$user_id."',subject='".$subject."',school='".$school."',
	board='".$board."',standard='".$standard."',myhome='".$myhome."',tutorhome='".$tutorhome."',
	institute='".$institute."',localities='".$localities."',start_date='".$start_date."',
	requirement='".$requirement."',age='".$age."'";
	$wpdb->get_results($insert_pdg_query);

	$created_at=date('Y-m-d h:i:s a');
	$noti_text = "A Query approval request received from Student (".$user_id.")";
	$sql_noti = "INSERT INTO pdg_admin_notification SET fk_query_id='".$wpdb->insert_id."',member_id='".$user_id."',notification='".$noti_text."',created_at='".$created_at."'";
	echo $sql_noti;
	$res_admin_notification = $wpdb->get_results($sql_noti);*/

	if($school_type == "school"){
		$qry = 'school='.$school.',college=0';
	}
	else if($school_type == "college"){
		$qry = 'school=0,college='.$school;
	}
	else{
		$qry = 'school=0,college=0';	
	}
	
	if($board_type == "board")
	{
		$qry_b = 'board='.$board.',university=0';
	}
	else if($board_type == "university")
	{
		$qry_b = 'board=0,university='.$board;
	}
	else
	{
		$qry_b = 'board=0,university=0';	
	}
		if($query_id == ""){
			$insert_pdg_query="INSERT INTO pdg_query SET student_id='".$user_id."',subject='".$subject."',".$qry.",".$qry_b.",standard='".$standard."',myhome='".$myhome."',tutorhome='".$tutorhome."',institute='".$institute."',localities='".$localities."',start_date='".$start_date."',requirement='".$requirement."',age='".$age."'";
			$wpdb->get_results($insert_pdg_query);
			$lastid = $wpdb->insert_id;
			
			$student_detail=$this->get_student_details($user_id);
			$student_name=$student_detail[0]->display_name;
			
			$created_at=date('Y-m-d h:i:s a');
			
			$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name` WHERE pdg_subject_name.subject_name_id = ".$subject;
			$res_subject = $wpdb->get_results($sql_subject);
			$subject_name = $res_subject[0]->subject_name;
			//$noti_text = "A Query approval request received from Student (".$student_name.")";
			$noti_text = "A Query approval against the subject (".ucfirst($subject_name).") from student (".$student_name.") is pending for Approval";
			$sql_noti = "INSERT INTO pdg_admin_notification SET fk_query_id='".$wpdb->insert_id."',member_id='".$user_id."',notification='".$noti_text."',created_at='".$created_at."'";
			//echo $sql_noti;
			$res_admin_notification = $wpdb->get_results($sql_noti);
			echo $lastid;
		}
		else
		{
			$update_pdg_query="UPDATE pdg_query SET student_id='".$user_id."',subject='".$subject."',".$qry.",".$qry_b.",standard='".$standard."',myhome='".$myhome."',tutorhome='".$tutorhome."',institute='".$institute."',localities='".$localities."',start_date='".$start_date."',requirement='".$requirement."',age='".$age."' WHERE id='".$query_id."'";
			$wpdb->get_results($update_pdg_query);
			$lastid = $wpdb->insert_id;
			//echo $lastid;
		}

	}
	//----------------------------------------
	//Update Student Data---------------------
	public function update_student_data($user_id,$name,$email,$phone,$query_ids=""){
	global $wpdb;
	$user_det=explode("@",$email);
	$user_name=$user_det[0];
	$query_id = explode(",", $query_ids);
	//insert into wp_users table
	//$sql_insert_wpu="UPDATE wp_users SET user_login='".$user_name."',user_nicename='".$user_name."',user_email='".$email."',display_name='".$name."' WHERE ID='".$user_id."'";
	//$wpdb->get_results($sql_insert_wpu);

	foreach ($query_id as $key => $a_query_id) {
		$update_pdg_query="UPDATE pdg_query SET contact_no='".$phone."',contact_email='".$email."' WHERE id='".$a_query_id."'";
		$wpdb->get_results($update_pdg_query);
	}

	//insert into pdg_student
	$sql_insert_pdgs="INSERT INTO pdg_student SET profile_slug='".$user_name."' WHERE user_id='".$user_id."'";
	$wpdb->get_results($sql_insert_pdgs);
	//insert into pdg_user_info table
	$sql_insert_pdg_usin="INSERT INTO pdg_user_info SET mobile_no='".$phone."' user_id='".$user_id."'";
	$wpdb->get_results($sql_insert_pdg_usin);	
	}
	//----------------------------------------
	//Get Student Query Pagination------------
	public function get_student_pagination(){
	global $wpdb;
	$userid=$_SESSION['member_id'];
	$sql="select id from pdg_query where student_id='".$userid."' and approved=1 and archived=0 and querydate >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH) ORDER BY id DESC" ;
	$recs=$wpdb->get_results($sql);
	return $recs;
	}
	//----------------------------------------
	//Get Student Query Details---------------
	public function get_student_query_details($query_id){
	global $wpdb;
	$sql="SELECT a.*,b.display_name AS student_name,c.subject_name,c.subject_name_id,b.user_email,d.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_academic_board AS d ON a.board=d.academic_board_id
	WHERE a.id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//----------------------------------------

	// Total Recommended Teacher Count
	public function count_recomended_list_teacher($query_id,$subject_name,$standard,$prefloc,$localities){
		global $wpdb;
		//$locs=explode(",",$localities);
		if($localities[0]!=''){
		 $locs1=$localities[0];
		}
		if($localities[1]!=''){
		 $locs2=$localities[1];
		}
		if($localities[2]!=''){
		 $locs3=$localities[2];
		}

		$student_id=$_SESSION['member_id'];
		
		$sql = "SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND ((CONCAT(',' , localities , ',') LIKE '%".$locs1."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs2."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs3."%')) AND (standard LIKE '%".$standard."%')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND ((CONCAT(',' , localities , ',') LIKE '%".$locs1."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs2."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs3."%'))) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))";

		$res=$wpdb->get_results($sql);
		$rowcount = $wpdb->num_rows;
		return $rowcount;
	}

	//GET Student Recomended Teacher Lists----
	public function get_student_recomended_list_teacher($query_id,$subject_name,$standard,$prefloc,$localities,$paged){
	global $wpdb;
	//$locs=explode(",",$localities);
	if($localities[0]!=''){
	 $locs1=$localities[0];
	 $sql_str .= "(CONCAT(',' , localities , ',') LIKE '%".$locs1."%') OR ";
	}
	if($localities[1]!=''){
	 $locs2=$localities[1];
	 $sql_str .= "(CONCAT(',' , localities , ',') LIKE '%".$locs2."%') OR ";
	}
	if($localities[2]!=''){
	 $locs3=$localities[2];
	 $sql_str .= "(CONCAT(',' , localities , ',') LIKE '%".$locs3."%')";
	}
	
	$sql_str = rtrim($sql_str," OR ");
	$student_id=$_SESSION['member_id'];

	/*$sql = "SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND ((CONCAT(',' , localities , ',') LIKE '%".$locs1."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs2."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs3."%')) AND (standard LIKE '%".$standard."%')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND ((CONCAT(',' , localities , ',') LIKE '%".$locs1."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs2."%')  OR (CONCAT(',' , localities , ',') LIKE '%".$locs3."%'))) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))";*/
	
	$sql = "SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND (".$sql_str.") AND (standard LIKE '%".$standard."%')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND (".$sql_str.")) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))";

	$resultsPerPage = 30;
	if($paged>0){
    	$page_limit = $resultsPerPage*($paged-1);
        $sql .=" LIMIT  $page_limit, $resultsPerPage";
    }
    else{
    	$sql .=" LIMIT 0 , $resultsPerPage";
    }
    //echo $sql;
	$res=$wpdb->get_results($sql);
	//$res='';
	return $res;
	}
	//----------------------------------------
	//Get Teacher Profile Slug----------------
	public function get_teacher_profile_slug($teacher_id){
	global $wpdb;
	$sqlp="SELECT profile_slug FROM pdg_teacher where user_id='".$teacher_id."'";
	$res=$wpdb->get_results($sqlp);
	return $res[0]->profile_slug;
	}
	//----------------------------------------
	//GET Student Recomended Institute Lists----
	public function get_student_recomended_list_institute($query_id,$subject_name){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	
	$sql = "SELECT * FROM view_pdg_all_institute WHERE (CONCAT_WS(',' , ti_subjects , ',') LIKE '%".$subject_name."%')
	 AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	AND (user_id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))";

	$res=$wpdb->get_results($sql);

	return $res;
	}
	//----------------------------------------
	//Get Teacher Profile Slug----------------
	public function get_institute_profile_slug($institute_id){
	global $wpdb;
	$sqlp="SELECT profile_slug FROM pdg_institutes where user_id='".$institute_id."'";
	$res=$wpdb->get_results($sqlp);
	return $res[0]->profile_slug;
	}
	//----------------------------------------
	//Get Student Recomended Invited List-----
	public function get_student_recomended_list_invite($subject_name,$query_id,$teacherinstid_arr){
	global $wpdb;
	//$teacherinstid_arr=array();
	$qstr='';
	$student_id=$_SESSION['member_id'];
	if(count($teacherinstid_arr) > 0){
	$notin=implode(",",$teacherinstid_arr);
	$qstr=" AND a.teacherinstid NOT IN ($notin)";
	}
	else{
	$qstr='';	
	}

	$sql = "SELECT a.type,ptim.* FROM pdg_invite AS a LEFT OUTER JOIN pdg_teacher_institute_master AS ptim ON a.teacherinstid = ptim.user_id WHERE a.studentid='".$student_id."' AND a.queryid='".$query_id."' AND a.teacherinstid NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."')";

	//echo $sql;
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//----------------------------------------
	//For Count Student Shortlist-------------
	public function count_student_shortlist($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT COUNT(*) AS count FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'";
	$res=$wpdb->get_results($sql);
	return $res[0]->count;
	}
	//--------------------------------------------
	//For Count Student Interestinyou-------------
	public function count_student_interestinyou($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT COUNT(*) AS count FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND approved = 1";
	$res=$wpdb->get_results($sql);
	return $res[0]->count;
	}
	//---------------------------------------------
	//Get student Teacher Shortlist----------------
	public function get_student_teacher_shortlist($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];

	$sql = "SELECT ptim.* FROM pdg_shortlist AS a LEFT JOIN pdg_teacher_institute_master AS ptim ON a.teacherinstid = ptim.user_id WHERE a.queryid='".$query_id."' AND a.studentid='".$student_id."' AND ptim.user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')";
	//echo "<br />Teacher : ".$sql;
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	
	//Get student Institute Shortlist----------------
	public function get_student_institute_shortlist($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	
	$sql="SELECT vpai.* FROM pdg_shortlist AS a LEFT JOIN view_pdg_all_institute AS vpai ON a.teacherinstid = vpai.user_id WHERE a.queryid='".$query_id."' AND a.studentid='".$student_id."' AND vpai.user_id != 'NULL' AND vpai.user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')";
	//echo "<br />Institute : ".$sql;
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	//Get Student shortlist invite-----------------
	public function get_student_invite_shortlist($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];

	$sql = "SELECT a.type,ptim.* FROM pdg_invite AS a LEFT OUTER JOIN pdg_teacher_institute_master AS ptim ON a.teacherinstid = ptim.user_id  WHERE a.studentid='".$student_id."' AND a.queryid='".$query_id."' AND a.teacherinstid IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."')";
	//echo $sql;
	$res=$wpdb->get_results($sql);
	return $res;	
	}
	//---------------------------------------------
	//Get student Teacher Interest In You----------------
	public function get_student_teacher_intyou($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	/*$sql="SELECT a.*,b.new_user_id,b.locality,b.locality1,b.locality2,b.locality3,b.subject_taught1,b.subject_taught2,b.subject_taught3,b.subject_taught4,b.name,b.experience,b.workexperience,b.hometutor,b.feeslower FROM pdg_interest_inyou AS a
	      LEFT OUTER JOIN user_data_teacher AS b ON b.new_user_id=a.teacherinst_id WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND b.name !='NULL' AND a.approved = 1";*/
	      //AND a.teacherinst_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')";
	      /*$sql="SELECT a.fk_query_id,a.fk_student_id,a.teacherinst_id,b.new_user_id,b.locality,b.locality1,b.locality2,b.locality3,b.subject_taught1,b.subject_taught2,b.subject_taught3,b.subject_taught4,b.name,b.experience,b.workexperience,b.hometutor,b.feeslower,vpt.user_id,CONCAT( vpt.first_name,  ' ', vpt.last_name ) AS view_name, vpt.localities, vpt.subjects, vpt.teaching_xp, vpt.gender AS view_gender FROM pdg_interest_inyou AS a
	      LEFT OUTER JOIN user_data_teacher AS b ON b.new_user_id=a.teacherinst_id LEFT OUTER JOIN view_pdg_teacher_extented AS vpt ON vpt.user_id = a.teacherinst_id WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND (b.name !='NULL' OR vpt.first_name !=  'NULL') AND a.approved = 1";*/
	      $sql = "SELECT a.fk_query_id,a.fk_student_id,a.teacherinst_id,b.* FROM pdg_interest_inyou AS a LEFT OUTER JOIN pdg_teacher_institute_master AS b ON a.teacherinst_id = b.user_id WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND a.approved = 1";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	//Get student Institute Interest in you list----------------
	public function get_student_institute_intyou($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT a.*,u.ID AS idd,c.nameofinst,c.learninglocality1,c.learninglocality2,c.learninglocality3,c.learninglocality4,c.learninglocality5,
	c.coursename1,c.coursename2,c.coursename3,c.coursename4,c.coursename5,c.yearsofexsistance,c.institutionlearninglocatiom,c.coursefeesinstitution1 FROM pdg_interest_inyou AS a
	LEFT OUTER JOIN wp_users AS u ON a.teacherinst_id=u.ID
	LEFT OUTER JOIN user_data_institution AS c ON c.emailinst=u.user_email WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND c.nameofinst !='NULL'
	AND a.teacherinst_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."')";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	//Get Student shortlist invite-----------------
	public function get_student_invite_intyou($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT a.teacherinstid,a.type,c.nameofinst,c.learninglocality1,c.learninglocality2,c.learninglocality3,c.learninglocality4,c.learninglocality5,
	c.coursename1,c.coursename2,c.coursename3,c.coursename4,c.coursename5,c.yearsofexsistance,c.institutionlearninglocatiom,c.coursefeesinstitution1,
	b.locality,b.locality1,b.locality2,b.locality3,b.subject_taught1,b.subject_taught2,b.subject_taught3,b.subject_taught4,b.name,b.experience,b.workexperience,b.hometutor,b.feeslower,
	u.ID AS idd FROM pdg_invite AS a
	LEFT OUTER JOIN wp_users AS u ON a.teacherinstid=u.ID
	LEFT OUTER JOIN user_data_teacher AS b ON b.new_user_id=u.ID
	LEFT OUTER JOIN user_data_institution AS c ON c.emailinst=u.user_email
	WHERE a.studentid='".$student_id."' AND a.queryid='".$query_id."' AND a.teacherinstid IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."')";
	$res=$wpdb->get_results($sql);
	return $res;	
	}
	//---------------------------------------------
	//Get student Teacher Matched List----------------
	public function get_student_teacher_matched($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	/*$sql="SELECT a.fk_query_id,a.fk_student_id,a.teacherinst_id,b.new_user_id,b.locality,b.locality1,b.locality2,b.locality3,b.subject_taught1,b.subject_taught2,b.subject_taught3,b.subject_taught4,b.name,b.workexperience,b.hometutor,b.feeslower,vpt.user_id,CONCAT( vpt.first_name,  ' ', vpt.last_name ) AS view_name, vpt.localities, vpt.subjects, vpt.teaching_xp, vpt.gender AS view_gender FROM pdg_matched_query AS a
	      LEFT OUTER JOIN user_data_teacher AS b ON b.new_user_id=a.teacherinst_id LEFT OUTER JOIN view_pdg_teacher_extented AS vpt ON vpt.user_id = a.teacherinst_id WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND (b.name !='NULL' OR vpt.first_name !=  'NULL')";*/
	      //AND a.teacherinst_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."') AND a.match_status=1 AND b.new_user_id=teacherinst_id";

	//$sql="SELECT a.fk_query_id,a.fk_student_id,a.teacherinst_id,b.* FROM pdg_matched_query AS a LEFT JOIN pdg_teacher_institute_master AS b ON a.teacherinst_id=b.user_id WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND a.match_status=1";
	$sql="SELECT a.fk_query_id,a.fk_student_id,a.teacherinst_id,b.* FROM pdg_matched_query AS a LEFT JOIN pdg_teacher_institute_master AS b ON a.teacherinst_id=b.user_id WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."'";

	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	//Get student Institute Matched list----------------
	public function get_student_institute_matched($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT a.*,u.ID AS idd,c.nameofinst,c.learninglocality1,c.learninglocality2,c.learninglocality3,c.learninglocality4,c.learninglocality5,
	c.coursename1,c.coursename2,c.coursename3,c.coursename4,c.coursename5,c.yearsofexsistance,c.institutionlearninglocatiom,c.coursefeesinstitution1 FROM pdg_matched_query AS a
	LEFT OUTER JOIN wp_users AS u ON a.teacherinst_id=u.ID
	LEFT OUTER JOIN user_data_institution AS c ON c.emailinst=u.user_email WHERE a.fk_query_id='".$query_id."' AND a.fk_student_id='".$student_id."' AND c.nameofinst !='NULL'
	AND a.teacherinst_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."') AND a.match_status=1 AND u.ID=teacherinst_id";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//---------------------------------------------
	//Get Student Matched invite-------------------
	public function get_student_invite_matched($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	/*$sql="SELECT a.teacherinstid,a.type,c.nameofinst,c.learninglocality1,c.learninglocality2,c.learninglocality3,c.learninglocality4,c.learninglocality5,
	c.coursename1,c.coursename2,c.coursename3,c.coursename4,c.coursename5,c.yearsofexsistance,c.institutionlearninglocatiom,c.coursefeesinstitution1,
	b.locality,b.locality1,b.locality2,b.locality3,b.subject_taught1,b.subject_taught2,b.subject_taught3,b.subject_taught4,b.subject_taught5,b.subject_taught6,b.subject_taught7,b.subject_taught8,b.name,b.workexperience,b.hometutor,b.feeslower,
	u.ID AS idd FROM pdg_invite AS a
	LEFT OUTER JOIN wp_users AS u ON a.teacherinstid=u.ID
	LEFT OUTER JOIN user_data_teacher AS b ON b.new_user_id=u.ID
	LEFT OUTER JOIN user_data_institution AS c ON c.emailinst=u.user_email
	WHERE a.studentid='".$student_id."' AND a.queryid='".$query_id."' AND a.teacherinstid IN (SELECT teacherinst_id FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND a.teacherinstid=teacherinst_id AND match_status=1)";*/
	$sql="SELECT a.type,b.* FROM pdg_invite AS a
	LEFT JOIN pdg_teacher_institute_master AS b ON a.teacherinstid=b.user_id 
	WHERE a.studentid='".$student_id."' AND a.queryid='".$query_id."' AND a.teacherinstid IN (SELECT teacherinst_id FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND a.teacherinstid=teacherinst_id AND match_status=1)";
	$res=$wpdb->get_results($sql);
	return $res;	
	}
	//---------------------------------------------
	//Archive Query--------------------------------
	public function query_archive($query_id){
	global $wpdb;
	$sql="UPDATE pdg_query SET archived=1 WHERE id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//Create Shortlist Query--------------------------------
	public function create_query_shortlist($query_id,$ti_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="INSERT INTO pdg_shortlist SET queryid='".$query_id."',studentid='".$student_id."',teacherinstid='".$ti_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//Remove Shortlist--------------------------------
	public function remove_query_shortlist($query_id,$ti_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="DELETE FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."' AND teacherinstid='".$ti_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//Create Invite Query--------------------------------
	public function create_query_invite($query_id,$ti_id,$type){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="INSERT INTO pdg_invite SET queryid='".$query_id."',studentid='".$student_id."',teacherinstid='".$ti_id."',type='".$type."',approved=1";
	$res=$wpdb->get_results($sql);
	$student_det=$this->get_student_details($student_id);
	$student_name=$student_det[0]->display_name;
	if($type=='t'){

	$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$query_id;
	$res_subject = $wpdb->get_results($sql_subject);
	$subject_name = $res_subject[0]->subject_name;

	$notification='You have received an invite from '.ucfirst($student_name).' against the Query ('.$subject_name.')';
	$sql_noti="INSERT INTO pdg_teacher_notification SET teacher_id='".$ti_id."',notification='".$notification."'";
	$wpdb->get_results($sql_noti);
	}
	}
	//---------------------------------------------
	//Create unInvite Query--------------------------------
	public function create_query_uninvite($query_id,$ti_id,$type){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="DELETE FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."' AND teacherinstid='".$ti_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	
	//Remove Shortlist--------------------------------
	public function stu_ignore_interest($query_id,$ti_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="DELETE FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id='".$ti_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//Submit Student Get Intro--------------------------------
	public function stu_get_intro($query_id,$ti_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql_chk_status="SELECT fk_query_id FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id='".$ti_id."'";
	$res_chk=$wpdb->get_results($sql_chk_status);
		if(empty($res_chk)){
		$sql="INSERT INTO pdg_matched_query SET fk_query_id='".$query_id."',fk_student_id='".$student_id."',teacherinst_id='".$ti_id."'";
		$res=$wpdb->get_results($sql);

		/************* Match Query Notification Arghya Saha **************/
		$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$query_id;
		$res_subject = $wpdb->get_results($sql_subject);
		$subject_name = $res_subject[0]->subject_name;
		$notification='Your interest for the Query ('.$subject_name.') got matched ';
		$sql_noti="INSERT INTO pdg_teacher_notification SET teacher_id='".$ti_id."',notification='".$notification."'";
		$wpdb->get_results($sql_noti);
		/************* End Of Match Query Notification Arghya Saha **************/

		}
		else{

		$sql="UPDATE pdg_matched_query SET match_status=1 WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id='".$ti_id."'";
		$res=$wpdb->get_results($sql);

		/************* Match Query Notification Arghya Saha **************/
		$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$query_id;
		$res_subject = $wpdb->get_results($sql_subject);
		$subject_name = $res_subject[0]->subject_name;

		$notification='Your interest for the Query ('.$subject_name.') got matched ';
		$sql_noti="INSERT INTO pdg_teacher_notification SET teacher_id='".$ti_id."',notification='".$notification."'";
		$wpdb->get_results($sql_noti);
		/************* End Of Match Query Notification Arghya Saha **************/
		}
	}


	/*********************** Get Intro Teacher Arghya Saha ***********************/
	public function get_intro_teacher($query_id,$teacher_id,$student_id){
		global $wpdb;
		$sql_chk_status="SELECT * FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id='".$teacher_id."'";
		//echo $sql_chk_status."<br />";
		$res_chk=$wpdb->get_results($sql_chk_status);
		//print_r(count($res_chk));
		//if(!empty($res_chk)){
		/*if(count($res_chk) > 0){
			echo "true";
		}
		elae{
			echo "false";
		}*/
		return count($res_chk);
	}

	//---------------------------------------------
	//Remove Match--------------------------------
	public function stu_unmatch($query_id,$ti_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="DELETE FROM pdg_matched_query WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' AND teacherinst_id='".$ti_id."'";
	$res=$wpdb->get_results($sql);
	}
	//---------------------------------------------
	//*********************************************************************************************************************************
	//*****************************************************************Past Query Model************************************************
	//Get Past Queries----------------------------------------
	public function get_past_queries(){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT *,b.subject_name,c.academic_board FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON b.subject_name_id=a.subject
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE (a.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR a.query_status='CLOSED') AND a.archived=0 AND a.student_id='".$student_id."' ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------
	//Submit Edit Query--------------------------------------------
	public function submit_past_edit_query($query_id,$clone_student_id,$clone_subject,$clone_school,$clone_board,$clone_standard,$age,$clone_my_home,$clone_t_home,
						    $clone_institute,$clone_locality,$clone_query_date,$clone_requirement,$clone_student_name,$clone_phone_number,
						    $clone_user_email,$clone_school_type,$clone_board_type){
	global $wpdb;

	if($clone_school_type == "school"){
		$qry = 'school='.$clone_school.',college=0';
	}
	if($clone_school_type == "college"){
		$qry = 'school=0,college='.$clone_school;
	}
	if($clone_board_type == "board")
	{
		$qry_b = 'board='.$clone_board.',university=0';
	}
	if($clone_board_type == "university")
	{
		$qry_b = 'board=0,university='.$clone_board;
	}

	//insert into query table
	//$query_date=date('Y-m-d h:i:s');
	//$clone_query_date=date('Y-m-d h:i:s',strtotime($clone_query_date));
	$sql="INSERT INTO pdg_query SET subject='".$clone_subject."',".$qry.",".$qry_b.",standard='".$clone_standard."',
	myhome='".$clone_my_home."',tutorhome='".$clone_t_home."',institute='".$clone_institute."',localities='".$clone_locality."',start_date='".$clone_query_date."',
	requirement='".$clone_requirement."',age='".$age."',student_id='".$clone_student_id."'";
	$res=$wpdb->get_results($sql);
	//$query_id=$wpdb->insert_id;
	//update student
	$sql_stu_name_email="UPDATE wp_users SET display_name='".$clone_student_name."',user_email='".$clone_user_email."' WHERE ID='".$clone_student_id."'";
	$wpdb->get_results($sql_stu_name_email);
	
	$sql_stu_ph="UPDATE pdg_user_info SET mobile_no='".$clone_phone_number."' WHERE user_id='".$clone_student_id."'";
	$wpdb->get_results($sql_stu_ph);
	
	return 'success';
	}
	//--------------------------------------------------------------
	//*********************************************************************************************************************************
	//********************************************************Archived Query***********************************************************
	//Get Past Queries----------------------------------------
	public function get_archived_queries(){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="SELECT *,b.subject_name,c.academic_board,(SELECT querydate FROM pdg_query WHERE a.querydate < DATE_SUB(CURDATE(), INTERVAL 3 MONTH) OR a.query_status='CLOSED' AND id=a.id LIMIT 1) AS past FROM pdg_query AS a
	LEFT OUTER JOIN pdg_subject_name AS b ON b.subject_name_id=a.subject
	LEFT OUTER JOIN pdg_academic_board AS c ON a.board=c.academic_board_id
	WHERE a.archived=1 AND a.student_id='".$student_id."' ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//--------------------------------------------------------
	//*********************************************************************************************************************************
	//Unarchived query----------------------------------------
	public function query_unarchive($query_id){
	global $wpdb;
	$sql="UPDATE pdg_query SET archived=0 WHERE id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	}
	
	//Get Rating-----------------------------------------------------
	public function pdg_avg_rating( $avg_rating ) {
		$return_rating = 0;

		if ( $avg_rating > 4.5 ) {
			$return_rating = 5;
		} else if ( $avg_rating > 4 ) {
			$return_rating = 4.5;
		} else if ( $avg_rating > 3.5 ) {
			$return_rating = 4;
		} else if ( $avg_rating > 3 ) {
			$return_rating = 3.5;
		} else if ( $avg_rating > 2.5 ) {
			$return_rating = 3;
		} else if ( $avg_rating > 2 ) {
			$return_rating = 2.5;
		} else if ( $avg_rating > 1.5 ) {
			$return_rating = 2;
		} else if ( $avg_rating > 1 ) {
			$return_rating = 1.5;
		} else if ( $avg_rating > 0.5 ) {
			$return_rating = 1;
		} else if ( $avg_rating > 0 ) {
			$return_rating = 0.5;
		}

		return $return_rating;
	}
	public function pdg_avg_rating_to_stars( $avg_rating ) {
		//$avg_rating = self::pdg_avg_rating( $avg_rating );
		$return_array = array ();
		$avg_rating = round( $avg_rating * 2, 0 ) / 2;
		if ( $avg_rating == 5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
			);
		} elseif ( $avg_rating == 4.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
			);

		} elseif ( $avg_rating == 4 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 3.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 3 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 2.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 2 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);
		} elseif ( $avg_rating == 1.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 1 ) {
			$return_array = array (
				array ( "show_stars" => "rating_full" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 0.5 ) {
			$return_array = array (
				array ( "show_stars" => "rating-half" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);

		} elseif ( $avg_rating == 0 ) {
			$return_array = array (
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
				array ( "show_stars" => "rating_border" ),
			);
		}

		return $return_array;
	}
	
	public function get_ratings($teacher_inst_id){
	global $wpdb;
	$student_id=@$_SESSION['member_id'];
	$rat=array();
	$rating_count=0;
	$ratings_total=0;
	$ratings_sql = "select * from pdg_reviews where tutor_institute_user_id ='".$teacher_inst_id."'";
	$rating_data = $wpdb->get_results( $ratings_sql );
	if ( ! empty( $rating_data ) ) {
		foreach ( $rating_data as $ratings ) {
			$rated_to_user_id = $ratings->tutor_institute_user_id;
			$rated_by_user_id = $ratings->reviewer_user_id;
			$star_rating = $ratings->overall_rating;
			$is_approved = $ratings->is_approved;
                               if($is_approved!='no'){
				$rating_count ++;
				$ratings_total += $star_rating;
			       }
			
		}
	}
	if ( $rating_count > 0 && $ratings_total > 0 ) {
		$average_rating = $ratings_total / $rating_count;
		$average_rating = self::pdg_avg_rating( $average_rating );
	}
	$rat['rating_count']=$rating_count;
	@$rat['rating_star']=self::pdg_avg_rating_to_stars( $average_rating );
	@$rat['avarage_rating']=$average_rating;
	
	//Recomended--
	$recommendation_count=0;
	$recommendations_sql = "select * from pdg_recommendations where recommended_to = '".$teacher_inst_id."'";
	$recommend_data = $wpdb->get_results( $recommendations_sql );
	if ( ! empty( $recommend_data ) ) {
		foreach ( $recommend_data as $recommend ) {
			$recommend_to_user_id = $recommend->recommended_to;
			$recommended_by_user_id = $recommend->recommended_by;

			//if ( $user_id == $recommend_to_user_id ) {
				//if ( $recommended_by_user_id == $loggedin_user_id ) {
					$current_user_has_recommended = true;
				//}
				$recommendation_count ++;
			//}
		}
	}
	$rat['like']=$recommendation_count;
	return $rat;
	}
	//-----------------------------------------------------------------------
	
	//Delete Query-----------------------------------------------------------
	public function delete_query($query_id){
	global $wpdb;
	$student_id=$_SESSION['member_id'];
	$sql="DELETE FROM pdg_query WHERE id='".$query_id."'";
	$wpdb->get_results($sql);

	$student_details = $this->get_student_details($student_id);
	$student_name=$student_details[0]->display_name;
	$created_at=date('Y-m-d h:i:s a');
	$noti_text = "Student (".$student_name .") has Deleted the Query (".$query_id.")";
	$sql_noti = "INSERT INTO pdg_admin_notification SET fk_query_id='".$query_id."',member_id='".$student_id."',notification='".$noti_text."',created_at='".$created_at."'";
	$res_admin_notification = $wpdb->get_results($sql_noti);

	return 'success';
	}
	//-----------------------------------------------------------------------
	//***********************************************************************Admin Query View*******************************************************
	//Get Teacher Responsess------------------------------------------------
	public function get_teacher_responses($query_id,$role){
	global $wpdb;
	$qstr='';
	if($role=='administrator'){
	//$qstr=' AND a.approved=0';	
	}
	if($role=='sales'){
	$qstr=' AND a.approved=1';	
	}
	$sql="SELECT a.*,b.average_rating,b.internal_rating_date_time FROM pdg_interest_inyou AS a
	LEFT OUTER JOIN pdg_teacher_internal_rating AS b ON a.teacherinst_id=b.user_id
	WHERE a.fk_query_id='".$query_id."' $qstr ORDER BY a.int_it DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------------
	//Get Student Responsess------------------------------------------------
	public function get_student_responses($query_id,$role){
	global $wpdb;
	$qstr='';
	if($role=='administrator'){
	//$qstr=' AND a.approved=0';	
	}
	if($role=='sales'){
	$qstr=' AND a.approved=1';	
	}
	$sql="SELECT a.*,b.average_rating,b.internal_rating_date_time FROM pdg_invite AS a
	LEFT OUTER JOIN pdg_teacher_internal_rating AS b ON a.teacherinstid=b.user_id
	WHERE a.queryid='".$query_id."' $qstr ORDER BY a.id DESC";
	$res=$wpdb->get_results($sql);
	//echo $sql;
	return $res;
	}
	//-----------------------------------------------------------------------
	//Get Teacher Inst Responses Details-------------------------------------
	public function get_teacher_responses_details($int_id){
	global $wpdb;
	$sql="SELECT a.*,b.average_rating,b.punctuality_rating,b.responsiveness_rating,b.behaviour_rating,b.communication_skills_rating,b.internal_rating_date_time FROM pdg_interest_inyou AS a
	LEFT OUTER JOIN pdg_teacher_internal_rating AS b ON a.teacherinst_id=b.user_id
	WHERE a.int_it='".$int_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------------
	//Get Teacher Inst Responses Details-------------------------------------
	public function get_teacher_rating($teacher_inst_id){
	global $wpdb;
	$sql="SELECT b.average_rating,b.punctuality_rating,b.responsiveness_rating,b.behaviour_rating,b.communication_skills_rating FROM pdg_teacher_internal_rating AS b
	WHERE b.user_id='".$teacher_inst_id."' ORDER BY internal_rating_date_time DESC";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------------
	//Responses Approve------------------------------------------------------
	public function respons_approve($id,$type){
	global $wpdb;
		if($type=='t'){
		$sql="UPDATE pdg_interest_inyou SET approved=1 WHERE int_it='".$id."'";
		$res=$wpdb->get_results($sql);
		}
		else{
		$sql="UPDATE pdg_invite SET approved=1 WHERE id='".$id."'";
		$res=$wpdb->get_results($sql);
		
		$sql_g="SELECT teacherinstid FROM pdg_invite WHERE id='".$id."'";
		$res_g=$wpdb->get_results($sql);
		$ti_id=$res_g[0]->teacherinstid;
		
		$notification='Your response for the Query has been approved by admin';
	        $sql_noti="INSERT INTO pdg_teacher_notification SET teacher_id='".$ti_id."',notification='".$notification."'";
	        $wpdb->get_results($sql_noti);
		}
	}
	//-----------------------------------------------------------------------
	//Responses Dispprove------------------------------------------------------
	public function respons_disapprove($id,$type,$message){
	global $wpdb;
		if($type=='t'){
		$sql="UPDATE pdg_interest_inyou SET approved=2 WHERE int_it='".$id."'";
		$res=$wpdb->get_results($sql);
		}
		else{
		$sql="UPDATE pdg_invite SET approved=2 WHERE id='".$id."'";
		$res=$wpdb->get_results($sql);	
		}
	}


	/**************** Create Notification For Approve Arghya Saha ****************************/ 
	public function create_notification_approve($q_id,$t_id){
		global $wpdb;

		$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$q_id;
		$res_subject = $wpdb->get_results($sql_subject);
		$subject_name = $res_subject[0]->subject_name;

		$notification='Your interest against the Query for '.$subject_name.' has been approved by admin';
		$sql_noti="INSERT INTO pdg_teacher_notification SET teacher_id='".$t_id."',notification='".$notification."'";
		$wpdb->get_results($sql_noti);
	}

	/**************** Create Notification For Disapprove Arghya Saha ****************************/ 
	public function create_notification_disapprove($q_id,$t_id){
		global $wpdb;

		$sql_subject = "SELECT pdg_subject_name.subject_name FROM `pdg_subject_name`,`pdg_query` WHERE pdg_subject_name.subject_name_id = pdg_query.subject AND pdg_query.id=".$q_id;
		$res_subject = $wpdb->get_results($sql_subject);
		$subject_name = $res_subject[0]->subject_name;

		$notification='Your interest against the Query for '.$subject_name.' has been disapproved by admin';
		$sql_noti="INSERT INTO pdg_teacher_notification SET teacher_id='".$t_id."',notification='".$notification."'";
		$wpdb->get_results($sql_noti);
	}




	//-----------------------------------------------------------------------
	//Get Recomended Teacher Institute for Query View page-------------------
	//For Teacher--
	public function get_recomended_teacher_on_view($query_id,$subject_name,$standard,$prefloc,$student_id){
	global $wpdb;
	//echo $prefloc;
	/*$sql="SELECT * FROM user_data_teacher  WHERE (subject_taught1 = '".$subject_name."' OR subject_taught2 = '".$subject_name."' OR subject_taught3 = '".$subject_name."' OR subject_taught4 = '".$subject_name."' OR subject_taught5 = '".$subject_name."' OR subject_taught6 = '".$subject_name."' OR subject_taught7 = '".$subject_name."' OR subject_taught8 = '".$subject_name."') AND (new_user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."'))
	 AND (new_user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	UNION
	SELECT * FROM user_data_teacher WHERE (subject_taught1 = '".$subject_name."' OR subject_taught2 = '".$subject_name."' OR subject_taught3 = '".$subject_name."' OR subject_taught4 = '".$subject_name."' OR subject_taught5 = '".$subject_name."' OR subject_taught6 = '".$subject_name."' OR subject_taught7 = '".$subject_name."' OR subject_taught8 = '".$subject_name."') AND (new_user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."'))
	AND (new_user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	UNION 
	SELECT * FROM user_data_teacher WHERE (subject_taught1 = '".$subject_name."' OR subject_taught2 = '".$subject_name."' OR subject_taught3 = '".$subject_name."' OR subject_taught4 = '".$subject_name."' OR subject_taught5 = '".$subject_name."' OR subject_taught6 = '".$subject_name."' OR subject_taught7 = '".$subject_name."' OR subject_taught8 = '".$subject_name."') AND (new_user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' ))
	AND (new_user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	UNION
	SELECT * FROM user_data_teacher WHERE (subject_taught1 = '".$subject_name."' OR subject_taught2 = '".$subject_name."' OR subject_taught3 = '".$subject_name."' OR subject_taught4 = '".$subject_name."' OR subject_taught5 = '".$subject_name."' OR subject_taught6 = '".$subject_name."' OR subject_taught7 = '".$subject_name."' OR subject_taught8 = '".$subject_name."') AND (new_user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."')) 
	AND (new_user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	UNION
	SELECT * FROM user_data_teacher WHERE (subject_taught1 = '".$subject_name."' OR subject_taught2 = '".$subject_name."' OR subject_taught3 = '".$subject_name."' OR subject_taught4 = '".$subject_name."' OR subject_taught5 = '".$subject_name."' OR subject_taught6 = '".$subject_name."' OR subject_taught7 = '".$subject_name."' OR subject_taught8 = '".$subject_name."' and classofteaching like '%".$standard."%') AND (new_user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."'))
	AND (new_user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
	UNION
	SELECT * FROM user_data_teacher WHERE (subject_taught1 = '".$subject_name."' OR subject_taught2 = '".$subject_name."' OR subject_taught3 = '".$subject_name."' OR subject_taught4 = '".$subject_name."' OR subject_taught5 = '".$subject_name."' OR subject_taught6 = '".$subject_name."' OR subject_taught7 = '".$subject_name."' OR subject_taught8 = '".$subject_name."') and (classofteaching like '%".$standard."%' AND hometutor = '".$prefloc."' AND new_user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."') )";*/

	$sql = "SELECT * FROM pdg_teacher_institute_master WHERE ((CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND (location_type LIKE '%".$prefloc."%') AND (standard LIKE '%".$standard."%')) AND (user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))
		UNION
		SELECT * FROM pdg_teacher_institute_master WHERE (CONCAT_WS(',' , subjects , ',') LIKE '%".$subject_name."%') AND (user_id NOT IN (SELECT teacherinst_id FROM pdg_interest_inyou WHERE fk_query_id='".$query_id."' AND fk_student_id='".$student_id."')) AND (user_id NOT IN (SELECT teacherinstid FROM pdg_invite WHERE queryid='".$query_id."' AND studentid='".$student_id."'))";


	$res=$wpdb->get_results($sql);
	return $res;
	}
	//For Institute--
	public function get_recomended_institute_on_view($query_id,$subject_name,$student_id){
	global $wpdb;
	$sql="SELECT a.*,b.id as userid FROM user_data_institution a INNER JOIN wp_users b ON a.emailinst = b.user_email WHERE 
	(a.coursename1 ='".$subject_name."' or a.coursename2 ='".$subject_name."' or a.coursename3 = '".$subject_name."' or a.coursename4 ='".$subject_name."' or a.coursename5 ='".$subject_name."')
	AND (b.id NOT IN (SELECT teacherinstid FROM pdg_shortlist WHERE queryid='".$query_id."' AND studentid='".$student_id."'))";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	
	//-----------------------------------------------------------------------
	//Get Matched Teacher id in view-----------------------------------------
	public function get_matched_teacher_ids($query_id,$student_id){
	global $wpdb;
	$sql="SELECT a.*,b.average_rating,b.internal_rating_date_time FROM pdg_matched_query AS a
	LEFT OUTER JOIN pdg_teacher_internal_rating AS b ON a.teacherinst_id=b.user_id
	WHERE a.fk_query_id='".$query_id."' AND fk_student_id='".$student_id."' ORDER BY a.m_id DESC";
	$res=$wpdb->get_results($sql);
	//echo $sql;
	return $res;
	}
	
	//-------------- Remove Disapprove teacher from List --------------//
	public function remove_disapprove_teacher($query_id,$student_id,$teacher_id){
	global $wpdb;
	$sql="DELETE FROM `pdg_matched_query` WHERE `fk_query_id`='".$query_id."' AND `fk_student_id`='".$student_id."' AND `teacherinst_id`='".$teacher_id."'";
	//echo $sql; exit();
	$res=$wpdb->get_results($sql);
	return $res;
	}

	//-------------- Remove Disapprove teacher from response List --------------//
	public function remove_disapprove_teacher_response($query_id,$student_id,$teacher_id){
	global $wpdb;
	$sql="DELETE FROM `pdg_interest_inyou` WHERE `fk_query_id`='".$query_id."' AND `fk_student_id`='".$student_id."' AND `teacherinst_id`='".$teacher_id."'";
	//echo $sql; exit();
	$res=$wpdb->get_results($sql);
	return $res;
	}
	
	//-----------------------------------------------------------------------
	//Check Teacher Institute Exist Or Not-----------------------------------
	public function teacher_exist($teacher_id){
	global $wpdb;
	$sql="SELECT ID FROM wp_users WHERE ID ='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	if(!empty($res)){
	return 1;	
	}
	else{
	return 0;	
	}
	}
	//-----------------------------------------------------------------------
	//Finalize Teacher of a query--------------------------------------------
	public function finalazie_teacher($query_id,$student_id,$teacher_id,$sales_rep_id){
	global $wpdb;
	$sql_del="DELETE FROM pdg_finalized_teacher WHERE fk_query_id='".$query_id."'";
	$wpdb->get_results($sql_del);
	$sql="INSERT INTO pdg_finalized_teacher SET fk_query_id='".$query_id."',fk_student_id='".$student_id."',teacherinst_id='".$teacher_id."',fk_sales_rep_id='".$sales_rep_id."'";
	$res=$wpdb->get_results($sql);
	$sql_q="UPDATE pdg_query SET query_status='BAKED' WHERE id='".$query_id."'";
	$wpdb->get_results($sql_q);
	}
	//-----------------------------------------------------------------------
	//Check Teacher finalize or not------------------------------------------
	public function check_finalazie_teacher($query_id){
	global $wpdb;
	$sql="SELECT teacherinst_id FROM pdg_finalized_teacher WHERE fk_query_id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	if(!empty($res)){
	return $res[0]->teacherinst_id;	
	}
	else{
	return 0;	
	}
	}
	//-----------------------------------------------------------------------
	//Update Inbound Payment-------------------------------------------------
	public function update_inbound_payment($query_id,$student_id,$teacher_id,$sales_rep_id,$inbound_payment_type,$payment_type_details,$inbound_payment_fee,$exp_date_payment,$fees,$inbound_payment_cycle,$vendor_invoice_number,$cash_trans){
	global $wpdb;
	$updated_on=date('Y-m-d h:i:s a');
	$sql_chk="SELECT updated_on FROM pdg_inbound_payment WHERE fk_query_id='".$query_id."'";
	$res_chk=$wpdb->get_results($sql_chk);
	if(empty($res_chk)){
	$sql_insert="INSERT INTO pdg_inbound_payment SET fk_query_id='".$query_id."',fk_student_id='".$student_id."',teacherinst_id='".$teacher_id."',fk_sales_rep_id='".$sales_rep_id."',
	payment_by='".$inbound_payment_type."',payment_by_details='".$payment_type_details."',fees_type='".$inbound_payment_fee."',exp_date_payment='".$exp_date_payment."',fees='".$fees."',cycle='".$inbound_payment_cycle."',
	invoice_no='".$vendor_invoice_number."',is_transferd='".$cash_trans."',updated_on='".$updated_on."'";
	$wpdb->get_results($sql_insert);
	$sql_q="UPDATE pdg_query SET query_status='PAY_EXP_DT' WHERE id='".$query_id."'";
	$wpdb->get_results($sql_q);
	}
	else{
	$sql_update="UPDATE pdg_inbound_payment SET fk_student_id='".$student_id."',teacherinst_id='".$teacher_id."',fk_sales_rep_id='".$sales_rep_id."',
	payment_by='".$inbound_payment_type."',payment_by_details='".$payment_type_details."',fees_type='".$inbound_payment_fee."',exp_date_payment='".$exp_date_payment."',fees='".$fees."',cycle='".$inbound_payment_cycle."',
	invoice_no='".$vendor_invoice_number."',is_transferd='".$cash_trans."',updated_on='".$updated_on."' WHERE fk_query_id='".$query_id."'";
	$wpdb->get_results($sql_update);
	}
	if($cash_trans==1){
	$sql_q="UPDATE pdg_query SET query_status='PAY_REC' WHERE id='".$query_id."'";
	$wpdb->get_results($sql_q);	
	}
	else{
	$sql_q="UPDATE pdg_query SET query_status='PAY_EXP_DT' WHERE id='".$query_id."'";
	$wpdb->get_results($sql_q);		
	}
	return $updated_on;
	}

	//----------------------------------------------------------------------
	//Payment Notification--------------------------------------------------

	public function payment_inbound_notification($user_role,$query_id,$sales_rep_id,$p_u_notification,$p_r_notification,$p_t_notification,$p_update,$p_received,$p_transfer){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		
		if($user_role == 'sales'){
			$sql_chk="SELECT * FROM pdg_payment_notification WHERE fk_query_id='".$query_id."'";
			$res_chk=$wpdb->get_results($sql_chk);
			if(empty($res_chk)){
				$sql_insert="INSERT INTO pdg_payment_notification SET fk_query_id='".$query_id."',sales_id='".$sales_rep_id."',payment_update_notification='".$p_u_notification."',payment_received_notification='".$p_r_notification."',payment_transfer_notification='".$p_t_notification."',payment_update='".$p_update."',payment_received='".$p_received."',payment_transfer='".$p_transfer."',is_view_payment_update='0',is_view_payment_receive='0',is_view_payment_transfer='0',created_at='".$created_at."'";
				$wpdb->get_results($sql_insert);
			}
			else
			{
				$sql_update="UPDATE pdg_payment_notification SET payment_update_notification='".$p_u_notification."',payment_update='".$p_update."',created_at='".$created_at."' WHERE fk_query_id='".$query_id."' AND sales_id='".$sales_rep_id."'";
				$wpdb->get_results($sql_update);
			}
		}
		if($user_role == 'finance'){
			$sql_update="UPDATE pdg_payment_notification SET payment_received_notification='".$p_r_notification."',payment_received='".$p_received."',created_at='".$created_at."' WHERE fk_query_id='".$query_id."'";
			$wpdb->get_results($sql_update);
		}
		if($user_role == "administrator"){
			$sql_chk="SELECT * FROM pdg_payment_notification WHERE fk_query_id='".$query_id."'";
			$res_chk=$wpdb->get_results($sql_chk);
			if(empty($res_chk)){
				$sql_insert="INSERT INTO pdg_payment_notification SET fk_query_id='".$query_id."',sales_id='".$sales_rep_id."',payment_update_notification='".$p_u_notification."',payment_received_notification='".$p_r_notification."',payment_transfer_notification='".$p_t_notification."',payment_update='".$p_update."',payment_received='".$p_received."',payment_transfer='".$p_transfer."',is_view_payment_update='0',is_view_payment_receive='0',is_view_payment_transfer='0',created_at='".$created_at."'";
				$wpdb->get_results($sql_insert);
			}
			else
			{
				$sql_update="UPDATE pdg_payment_notification SET payment_update_notification='".$p_u_notification."',payment_update='".$p_update."',payment_received_notification='".$p_r_notification."',payment_received='".$p_received."',payment_transfer_notification='".$p_t_notification."',payment_transfer='".$p_transfer."',created_at='".$created_at."' WHERE fk_query_id='".$query_id."'";
				$wpdb->get_results($sql_update);
			}
		}
	}

	public function payment_outbound_notification($query_id,$p_t_notification,$p_transfer){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		
		$sql_update="UPDATE pdg_payment_notification SET payment_transfer_notification='".$p_t_notification."',payment_transfer='".$p_transfer."',created_at='".$created_at."' WHERE fk_query_id='".$query_id."'";
		$wpdb->get_results($sql_update);
		
	}

	public function payment_update_count_notification(){
		global $wpdb;
		//$sql="SELECT fk_query_id,COUNT(*) AS count FROM pdg_payment_notification WHERE payment_update='1'";
		$sql="SELECT fk_query_id FROM pdg_payment_notification WHERE payment_update='1' AND is_view_payment_update='0'";
		$res=$wpdb->get_results($sql);
		$a_return = array();
		foreach($res as $result){
			$a_return['query_id'][] = $result->fk_query_id;
		}
		$a_return['count'] = count($a_return['query_id']);
		//echo "<pre>"; print_r($res);echo "</pre>";
		//return $res[0]->count;
		return $a_return;
	}

	public function payment_received_count_notification($sale_id){
		global $wpdb;
		//$sql="SELECT COUNT(*) AS count FROM pdg_payment_notification WHERE sales_id='".$sale_id."' AND payment_received='1'";
		$sql="SELECT fk_query_id FROM pdg_payment_notification WHERE (sales_id='".$sale_id."' OR sales_id='1') AND payment_received='1' AND is_view_payment_receive='0'";
		$res=$wpdb->get_results($sql);
		$a_return = array();
		foreach($res as $result){
			$a_return['query_id'][] = $result->fk_query_id;
		}
		$a_return['count'] = count($a_return['query_id']);
		return $a_return;
		//return $res[0]->count;
	}

	public function payment_transfer_count_notification($sale_id){
		global $wpdb;
		//$sql="SELECT COUNT(*) AS count FROM pdg_payment_notification WHERE sales_id='".$sale_id."' AND payment_transfer='1'";
		$sql="SELECT fk_query_id FROM pdg_payment_notification WHERE sales_id='".$sale_id."' AND payment_transfer='1' AND is_view_payment_transfer='0'";
		$res=$wpdb->get_results($sql);
		$a_return = array();
		foreach($res as $result){
			$a_return['query_id'][] = $result->fk_query_id;
		}
		$a_return['count'] = count($a_return['query_id']);
		return $a_return;
		//return $res[0]->count;
	}

	public function get_all_admin_notification(){
		global $wpdb;
		$sql_payment="SELECT payment_update_notification,payment_received_notification,payment_transfer_notification,created_at FROM pdg_payment_notification ORDER BY created_at DESC";
		$res_payment=$wpdb->get_results($sql_payment);

		$sql_admin = "SELECT notification,created_at FROM pdg_admin_notification ORDER BY created_at DESC";
		$res_admin = $wpdb->get_results($sql_admin);

		$arr_notification = array();
		$inc = 0;
		foreach ($res_payment as $key => $payment_noti) {
			if(!empty($payment_noti->payment_transfer_notification)){
				$arr_notification[$inc]['notification'] = $payment_noti->payment_transfer_notification;
				$arr_notification[$inc]['created_at'] = $payment_noti->created_at;
				$inc++;
			}
			if(!empty($payment_noti->payment_received_notification)){
				$arr_notification[$inc]['notification'] = $payment_noti->payment_received_notification;
				$arr_notification[$inc]['created_at'] = $payment_noti->created_at;
				$inc++;
			}
			if(!empty($payment_noti->payment_update_notification)){
				$arr_notification[$inc]['notification'] = $payment_noti->payment_update_notification;
				$arr_notification[$inc]['created_at'] = $payment_noti->created_at;
				$inc++;
			}
		}

		foreach ($res_admin as $key => $admin_noti) {
			//$key = $inc;
			$arr_notification[$inc]['notification'] = $admin_noti->notification;
			$arr_notification[$inc]['created_at'] = $admin_noti->created_at;
			$inc++;
		}

		return $arr_notification;
	}

  public function get_admin_count_notification(){
		global $wpdb;
		$sql_payment="SELECT payment_update_notification,payment_received_notification,payment_transfer_notification,created_at FROM pdg_payment_notification WHERE is_view=0 ORDER BY created_at DESC";
		$res_payment=$wpdb->get_results($sql_payment);

		$sql_admin = "SELECT notification,created_at FROM pdg_admin_notification WHERE is_view=0 ORDER BY created_at DESC";
		$res_admin = $wpdb->get_results($sql_admin);

		$arr_notification = array();
		$inc = 0;
		foreach ($res_payment as $key => $payment_noti) {
			if(!empty($payment_noti->payment_transfer_notification)){
				$arr_notification[$inc]['notification'] = $payment_noti->payment_transfer_notification;
				$arr_notification[$inc]['created_at'] = $payment_noti->created_at;
				$inc++;
			}
			if(!empty($payment_noti->payment_received_notification)){
				$arr_notification[$inc]['notification'] = $payment_noti->payment_received_notification;
				$arr_notification[$inc]['created_at'] = $payment_noti->created_at;
				$inc++;
			}
			if(!empty($payment_noti->payment_update_notification)){
				$arr_notification[$inc]['notification'] = $payment_noti->payment_update_notification;
				$arr_notification[$inc]['created_at'] = $payment_noti->created_at;
				$inc++;
			}
		}

		foreach ($res_admin as $key => $admin_noti) {
			//$key = $inc;
			$arr_notification[$inc]['notification'] = $admin_noti->notification;
			$arr_notification[$inc]['created_at'] = $admin_noti->created_at;
			$inc++;
		}

		return count($arr_notification);
	}
  
	//-----------------------------------------------------------------------
	//Get Update inbound payment data----------------------------------------
	public function get_update_inbound_payment($query_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_inbound_payment WHERE fk_query_id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------------
	//Update outbound payment data-------------------------------------------
	public function update_outbound_payment($query_id,$teacher_id,$student_id,$finance_rep_id,$outbound_payment_type,$payment_type_details,$distri_name,$paytm_mob_no,$bank_count,$cash_receive,$customer_invoice_number,$our_share,$teacher_share,$bank_data,$gst){
	global $wpdb;
	$updated_on=date('Y-m-d h:i:s a');
	
	$sql_chk="SELECT updated_on FROM pdg_outbound_payment WHERE fk_query_id='".$query_id."'";
	$res_chk=$wpdb->get_results($sql_chk);
		if(empty($res_chk)){
		 $sql="INSERT INTO pdg_outbound_payment SET fk_query_id='".$query_id."',fk_student_id='".$student_id."',teacherinst_id='".$teacher_id."',finance_rep_id='".$finance_rep_id."',
		 our_share='".$our_share."',teacher_share='".$teacher_share."',paytm_no='".$paytm_mob_no."',distributors_name='".$distri_name."',is_received='".$cash_receive."',customer_invoice_no='".$customer_invoice_number."',
		 payment_type='".$outbound_payment_type."',payment_by_details='".$payment_type_details."',gst='".$gst."',updated_on='".$updated_on."'";
		 //echo $sql;
		 $res=$wpdb->get_results($sql);
		 $bound_id=$wpdb->insert_id;

		if($cash_receive==1){
			$sql_q="UPDATE pdg_query SET query_status='PAID_TO' WHERE id='".$query_id."'";
			$wpdb->get_results($sql_q);	
		}

		// if($outbound_payment_type==3){
		////-------------------------------------------
		//$sql_del="DELETE FROM pdg_outbound_bank WHERE fk_query_id='".$query_id."'";
		//$wpdb->get_results($sql_del);
		////-------------------------------------------
		// $datas=explode("&",$bank_data);
		//	$i=1;
		//	$bank_array=array();
		//	foreach($datas as $key=> $val){
		//	$vals=explode("=",$val);
		//	$bank_array[$vals[0]]=$vals[1];
		//	$i++;
		//	}
		//	for($b=1;$b<=$bank_count;$b++){
		//		if($bank_array['bank_name'.$b]!=''){
		//		$sql_bank="INSERT INTO pdg_outbound_bank SET fk_query_id='".$query_id."', fk_outbound_id='".$bound_id."',bank_name='".$bank_array['bank_name'.$b]."',
		//		acc_holder_name='".$bank_array['acc_holder_name'.$b]."',acc_number='".$bank_array['acc_number'.$b]."',ifse_code='".$bank_array['ifsc_code'.$b]."'";
		//		$wpdb->get_results($sql_bank);
		//		}	
		//	}
		// }
		}
		else{
		$sql="UPDATE pdg_outbound_payment SET fk_student_id='".$student_id."',teacherinst_id='".$teacher_id."',finance_rep_id='".$finance_rep_id."',
		 our_share='".$our_share."',teacher_share='".$teacher_share."',paytm_no='".$paytm_mob_no."',distributors_name='".$distri_name."',is_received='".$cash_receive."',customer_invoice_no='".$customer_invoice_number."',
		 payment_type='".$outbound_payment_type."',payment_by_details='".$payment_type_details."',gst='".$gst."',updated_on='".$updated_on."' WHERE fk_query_id='".$query_id."'";
		 $res=$wpdb->get_results($sql);
		 $bound_id=$wpdb->insert_id;

		if($cash_receive==1){
			$sql_q="UPDATE pdg_query SET query_status='PAID_TO' WHERE id='".$query_id."'";
			$wpdb->get_results($sql_q);	
		}

		 //-------------------------------------------
		//$sql_del="DELETE FROM pdg_outbound_bank WHERE fk_query_id='".$query_id."'";
		//$wpdb->get_results($sql_del);
		//-------------------------------------------
		// if($outbound_payment_type==3){
		////-------------------------------------------
		//$sql_del="DELETE FROM pdg_outbound_bank WHERE fk_query_id='".$query_id."'";
		//$wpdb->get_results($sql_del);
		////-------------------------------------------
		// $datas=explode("&",$bank_data);
		//	$i=1;
		//	$bank_array=array();
		//	foreach($datas as $key=> $val){
		//	$vals=explode("=",$val);
		//	$bank_array[$vals[0]]=$vals[1];
		//	$i++;
		//	}
		//	for($b=1;$b<=$bank_count;$b++){
		//		if($bank_array['bank_name'.$b]!=''){
		//		$sql_bank="INSERT INTO pdg_outbound_bank SET fk_query_id='".$query_id."', fk_outbound_id='".$bound_id."',bank_name='".$bank_array['bank_name'.$b]."',
		//		acc_holder_name='".$bank_array['acc_holder_name'.$b]."',acc_number='".$bank_array['acc_number'.$b]."',ifse_code='".$bank_array['ifsc_code'.$b]."'";
		//		$wpdb->get_results($sql_bank);
		//		}	
		//	}
		// }	
		}
		
		return $updated_on;
	}
	//-----------------------------------------------------------------------
	//Get Update inbound payment data----------------------------------------
	public function get_update_outbound_payment($query_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_outbound_payment WHERE fk_query_id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------------
	//Get Outbound Bank Details----------------------------------------------
	public function get_outbound_bank_details($query_id){
	global $wpdb;
	$sql="SELECT * FROM pdg_outbound_bank WHERE fk_query_id='".$query_id."'";
	$res=$wpdb->get_results($sql);
	return $res;
	}
	//-----------------------------------------------------------------------
	
	//Update Query Plan------------------------------------------------------
	public function update_query_plan($query_id,$query_plan){
	 global $wpdb;
	 $sql="UPDATE pdg_query SET query_plan='".$query_plan."' WHERE id='".$query_id."'";
	 $res=$wpdb->get_results($sql);
	 return 'success';
	}
	//-----------------------------------------------------------------------
	//Update Query Fast pass Status------------------------------------------
	public function update_query_fast_pass($query_id,$fast_pass){
	 global $wpdb;
	 $sql="UPDATE pdg_query SET fast_pass='".$fast_pass."' WHERE id='".$query_id."'";
	 $res=$wpdb->get_results($sql);
	 return 'success';
	}
	//-----------------------------------------------------------------------
	//Get teacher plan-------------------------------------------------------
	public function get_teacher_plan($tech_id){
	 global $wpdb;
	 $sql="SELECT plan FROM user_data_teacher WHERE new_user_id='".$tech_id."'";
	 $res=$wpdb->get_results($sql);
	 return $res[0]->plan;
	}
	//-----------------------------------------------------------------------
	//Update teacher plan----------------------------------------------------
	public function update_teacher_ins_plan($t_type,$ti_id,$teacher_plan){
	global $wpdb;
	if($t_type=='teacher'){ //teacher
	 $sql="UPDATE user_data_teacher SET plan='".$teacher_plan."' WHERE new_user_id='".$ti_id."'";
	}
	else{ //institute--
	 
	}
	$res=$wpdb->get_results($sql);
	}
	//-----------------------------------------------------------------------
	//**********************************************************************************************************************************************
	






	//******************* SMS CODE - ARGHYA SAHA*****************//

	/***************** Get Member Contact Details ****************/

	public function get_member_contact_by_query($query_id){
		global $wpdb;
		$a_member = array();
		$query_holder_sql = "SELECT student_id FROM pdg_query WHERE id='".$query_id."'";
		$result_holder = $wpdb->get_results($query_holder_sql);
		$student_id = $result_holder[0]->student_id;
		if(!empty($student_id))
		{
			$sql="SELECT a.user_email,b.mobile_no,a.display_name FROM wp_users AS a LEFT OUTER JOIN pdg_user_info AS b ON a.ID=b.user_id WHERE a.ID='".$student_id."'";	
			$res_student_contact=$wpdb->get_results($sql);

			$a_member['student']['id'] = $student_id;
			$a_member['student']['email'] = $res_student_contact[0]->user_email;
			$a_member['student']['mobile_no'] = $res_student_contact[0]->mobile_no;
			$a_member['student']['name'] = $res_student_contact[0]->display_name;
		}
		

		$query_finalize_sql = "SELECT teacherinst_id FROM pdg_finalized_teacher WHERE fk_query_id='".$query_id."'";
		$result_finalize = $wpdb->get_results($query_finalize_sql);
		if(!empty($result_finalize)){
			$teacher_id = $result_finalize[0]->teacherinst_id;

			$sql_teacher="SELECT a.user_email,b.mobile_no,a.display_name FROM wp_users AS a LEFT OUTER JOIN pdg_user_info AS b ON a.ID=b.user_id WHERE a.ID='".$teacher_id."'";	
			$res_teacher_contact=$wpdb->get_results($sql_teacher);

			$a_member['teacher']['id'] = $teacher_id;
			$a_member['teacher']['email'] = $res_teacher_contact[0]->user_email;
			$a_member['teacher']['mobile_no'] = $res_teacher_contact[0]->mobile_no;
			$a_member['teacher']['name'] = $res_teacher_contact[0]->display_name;
		}
		return $a_member;
		/*$sql="SELECT a.user_email,b.mobile_no,a.display_name FROM wp_users AS a
		LEFT OUTER JOIN pdg_user_info AS b ON a.ID=b.user_id
		WHERE a.ID='".$member_id."'";
		$res=$wpdb->get_results($sql);
		return $res;*/
	}

	/*************** Get SMS Template *********************/
	
	public function get_sms_template(){
		global $wpdb;
		$sql="SELECT * FROM pdg_sms_template WHERE status=1";
		$res=$wpdb->get_results($sql);
		return $res;
	}

	public function get_sms_template_text($template_id)
	{
		global $wpdb;
		$sql="SELECT template_text FROM pdg_sms_template WHERE id='".$template_id."'";
		$res=$wpdb->get_results($sql);
		//echo "<pre>"; print_r($res);echo "</pre>";
		return $res;
	}

	/*******************06/01/2018**********************/
	public function add_activity_log($query_id,$member_id,$log,$is_view,$content=""){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		//$created_at = NOW();
		$sql_activity="INSERT INTO pdg_activity_log SET fk_query_id='".$query_id."',member_id='".$member_id."',activity_log='".addslashes($log)."',activity_content='".addslashes($content)."',is_view='".$is_view."',created_at=NOW()";
		//echo $sql_activity;
		$wpdb->get_results($sql_activity);
	}

	public function get_activity_logs($query_id){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		$sql_activity="SELECT * FROM pdg_activity_log WHERE fk_query_id='".$query_id."' ORDER BY created_at DESC";
		$result = $wpdb->get_results($sql_activity);
		return $result;
	}
	/*******************06/01/2018**********************/

	/*******************08/01/2018**********************/
	public function get_activity_logs_by_id($id){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		$sql_activity="SELECT * FROM pdg_activity_log WHERE id='".$id."' ORDER BY created_at DESC";
		$result = $wpdb->get_results($sql_activity);
		return $result;
	}
	public function add_activity_note($query_id,$note){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		$sql_activity="INSERT INTO pdg_activity_log SET fk_query_id='".$query_id."',activity_log='".$note."',is_view='1',created_at=NOW()";
		$wpdb->get_results($sql_activity);
	}

	public function update_activity_note($note_id,$note){
		global $wpdb;
		$created_at=date('Y-m-d h:i:s a');
		$sql_activity="UPDATE pdg_activity_log SET activity_log='".$note."',created_at=NOW() WHERE id='".$note_id."'";
		$wpdb->get_results($sql_activity);
	}
	/*******************08/01/2018**********************/

	//Read Notification By Sales Rep---------------------------------------------------

	public function read_receive_notification_by_sales($sales_id){
		global $wpdb;
		$sql="UPDATE pdg_payment_notification SET is_view_payment_receive=1 WHERE (sales_id='".$sales_id."' OR sales_id='1') AND is_view_payment_receive=0";
		$wpdb->get_results($sql);
	}

	public function read_transfer_notification_by_sales($sales_id){
		global $wpdb;
		$sql="UPDATE pdg_payment_notification SET is_view_payment_transfer=1 WHERE sales_id='".$sales_id."' AND is_view_payment_transfer=0";
		$wpdb->get_results($sql);
	}

	public function read_update_notification_by_finance(){
		global $wpdb;
		$sql="UPDATE pdg_payment_notification SET is_view_payment_update=1 WHERE is_view_payment_update=0";
		$wpdb->get_results($sql);
	}

	public function read_notification_by_admin(){
		global $wpdb;
		$sql_payment="UPDATE pdg_payment_notification SET is_view=1 WHERE is_view=0";
		$wpdb->get_results($sql_payment);

		$sql_admin="UPDATE pdg_admin_notification SET is_view=1 WHERE is_view=0";
		$wpdb->get_results($sql_admin);
	}


	////////////////////////////////////code by sudipta////////////////////////////

public function get_studentemail($ids){
	 global $wpdb;
	 $sql="SELECT *  FROM wp_users WHERE ID = (SELECT student_id FROM pdg_query WHERE id = '".$ids."')";
	 $res = $wpdb->get_results($sql);
	 return $res;
	}

	public function get_teacheremail($ids){
	 global $wpdb;
	$sql="SELECT user_email  FROM wp_users WHERE ID = (SELECT teacherinst_id FROM pdg_finalized_teacher WHERE fk_query_id ='".$ids."')";
	$res = $wpdb->get_results($sql);
	return $res;
	}


	function get_finalteachercount($ids) {
    global $wpdb;

    $wpdb->get_results("SELECT * FROM pdg_finalized_teacher WHERE fk_query_id ='".$ids."'");

    $rowcount = $wpdb->num_rows;

    return $rowcount;
}


function get_emailtemplate() {
    global $wpdb;

   $sql="SELECT * FROM pdg_email_templates WHERE status=1";

    $res = $wpdb->get_results($sql);
	return $res;
}


function get_emailtemplates($id) {
    global $wpdb;

   $sql="SELECT * FROM pdg_email_templates WHERE id='".$id."'";

    $res = $wpdb->get_results($sql);
	return $res;
}

function change_lead_source($q_id,$lead){
	global $wpdb;
   	$sql="UPDATE pdg_query SET lead_source='".$lead."' WHERE id='".$q_id."'";
    $res = $wpdb->get_results($sql);
	return $res;
}

function add_email_subscriber($email_id){
	global $wpdb;
	$sql = "INSERT INTO pdg_landing_email_subscribers SET email_id='".$email_id."'";
	$res = $wpdb->get_results($sql);
	return $res;
}

//--------------------------------------
function get_location_type($tiid){
global $wpdb;
$locations_sql = "select tuition_location_type from view_pdg_tutor_location where `tutor_institute_user_id` ='".$tiid."'";
$locations_db = $wpdb->get_results( $locations_sql );
return $locations_db[0]->tuition_location_type;	
}
//--------------------------------------
//--------------------------------------
function get_teacher_fees($tiid){
global $wpdb;
$fees_sql = "select fees_amount from view_pdg_min_fees where `tutor_institute_user_id` ='".$tiid."'";
$fees_db = $wpdb->get_results( $fees_sql );
return $fees_db[0]->fees_amount;		
}
//--------------------------------------
function get_current_sales_rep_id($query_id){
	global $wpdb;
	$sql = "SELECT * FROM pdg_query WHERE `id` ='".$query_id."'";
	$result = $wpdb->get_results($sql);
	return $result[0]->current_sales_rep_id;	
}

public function get_all_teacher_data(){
	global $wpdb;
	$sql = "SELECT * FROM view_pdg_all_teacher";
	$results = $wpdb->get_results($sql);
	return $results;
}

public function get_all_institute_data(){
	global $wpdb;
	$sql = "SELECT * FROM view_pdg_all_institute";
	$results = $wpdb->get_results($sql);
	return $results;
}

public function wp_insert_rows($row_arrays = array(), $wp_table_name, $update = false, $primary_key = null) {
		global $wpdb;
		$wp_table_name = esc_sql($wp_table_name);
		// Setup arrays for Actual Values, and Placeholders
		$values        = array();
		$place_holders = array();
		$query         = "";
		$query_columns = "";
		
		$query .= "INSERT INTO `{$wp_table_name}` (";
		foreach ($row_arrays as $count => $row_array) {
			foreach ($row_array as $key => $value) {
				if ($count == 0) {
					if ($query_columns) {
						$query_columns .= ", " . $key . "";
					} else {
						$query_columns .= "" . $key . "";
					}
				}
				
				$values[] = $value;
				
				$symbol = "%s";
				if (is_numeric($value)) {
					if (is_float($value)) {
						$symbol = "%f";
					} else {
						$symbol = "%d";
					}
				}
				if (isset($place_holders[$count])) {
					$place_holders[$count] .= ", '$symbol'";
				} else {
					$place_holders[$count] = "( '$symbol'";
				}
			}
			// mind closing the GAP
			$place_holders[$count] .= ")";
		}
		
		$query .= " $query_columns ) VALUES ";
		
		$query .= implode(', ', $place_holders);
		
		if ($update) {
			$update = " ON DUPLICATE KEY UPDATE $primary_key=VALUES( $primary_key ),";
			$cnt    = 0;
			foreach ($row_arrays[0] as $key => $value) {
				if ($cnt == 0) {
					$update .= "$key=VALUES($key)";
					$cnt = 1;
				} else {
					$update .= ", $key=VALUES($key)";
				}
			}
			$query .= $update;
		}
		
		$sql = $wpdb->prepare($query, $values);
		if ($wpdb->query($sql)) {
			return true;
		} else {
			return false;
		}
	}

	public function  count_student_queryview($query_id)
	{
		global $wpdb;
		$sql="SELECT COUNT(*) AS count FROM pdg_query_view WHERE query_id='".$query_id."'";
		$res=$wpdb->get_results($sql);
		return $res[0]->count;
	}

	/****************GET UNIVERSITY AND BOARD NAME*********************/
	public function get_university_name($id){
		
		global $wpdb;

		$sql = "SELECT university_name FROM pdg_university WHERE id='".$id."'";

		$result = $wpdb->get_results($sql);

		return $result[0]->university_name;
	}

	public function get_board_name($id){
		
		global $wpdb;

		$sql = "SELECT academic_board FROM pdg_academic_board WHERE academic_board_id='".$id."'";

		$result = $wpdb->get_results($sql);

		return $result[0]->academic_board;

	}
	/****************END OF GET UNIVERSITY AND BOARD NAME*********************/
}
?>