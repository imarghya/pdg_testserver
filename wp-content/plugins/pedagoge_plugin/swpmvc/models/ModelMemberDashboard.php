<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Academic Classes Model
 */
class ModelMemberDashboard extends ModelMaster {	
			
	//Get student details by query id--------------------------------
	public function get_student_details_by_qid($query_id){
	global $wpdb;
	 $sql="SELECT a.id,b.user_id,c.user_email,c.display_name FROM pdg_query AS a
	       LEFT OUTER JOIN pdg_student AS b ON a.student_id=b.student_id
	       LEFT OUTER JOIN wp_users AS c ON b.user_id = c.ID WHERE a.id='".$query_id."'";
	       $found_student_data = $wpdb->get_results($sql);
	       return $found_student_data;
	}
	//---------------------------------------------------------------
	
}
