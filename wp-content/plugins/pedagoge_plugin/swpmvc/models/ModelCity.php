<?php

class ModelCity extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_city';
		$this->set_primary_key('city_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['city_id']['value'] = null;
		$columns_array['city_id']['type'] = '%d';
		
		$columns_array['city_name']['value'] = null;
		$columns_array['city_name']['type'] = '%s';
		
		$columns_array['state_id']['value'] = null;
		$columns_array['state_id']['type'] = '%d';

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
}

