<?php
class ModelLanding extends ModelMaster {
	//Star Teacher---------------------------------------------------------------------------------------------------------------	
	//Get teacher list-----------------------------------------------
	public function get_teacher_list(){
	 global $wpdb;
	 $sql="SELECT teacher_institute_name AS name,user_id AS new_user_id FROM pdg_teacher_institute_master WHERE user_type='teacher' AND (user_id NOT IN (SELECT teacher_id FROM pdg_star_teacher))";
	 $res=$wpdb->get_results($sql);
	 return $res;
	}
       //----------------------------------------------------------------
       //Insert Star Teacher---------------------------------------------
       public function insert_star_teacher($teacher_id){
	global $wpdb;
	$sql="INSERT INTO pdg_star_teacher SET teacher_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	return 'success';
       }
       //----------------------------------------------------------------
       //Get star teacher list-------------------------------------------
       public function get_star_teacher_list(){
	global $wpdb;
	$sql="SELECT a.id,a.teacher_id,b.teacher_institute_name AS name FROM pdg_star_teacher AS a
	LEFT OUTER JOIN pdg_teacher_institute_master AS b ON a.teacher_id=b.user_id";
	$res=$wpdb->get_results($sql);
	return $res;
       }
       //----------------------------------------------------------------
        //Insert Star Teacher---------------------------------------------
       public function delete_star_teacher($id){
	global $wpdb;
	$sql="DELETE FROM pdg_star_teacher WHERE id='".$id."'";
	$res=$wpdb->get_results($sql);
	return 'success';
       }
       //----------------------------------------------------------------
       //-----------------------------------------------------------------------------------------------------------------------
       
       
       
       //Star Institute---------------------------------------------------------------------------------------------------------
       //Get institute list-----------------------------------------------
	public function get_inst_list(){
	 global $wpdb;
	 $sql="SELECT teacher_institute_name AS name,user_id AS new_user_id FROM pdg_teacher_institute_master WHERE user_type='institution' AND (user_id NOT IN (SELECT inst_id FROM pdg_star_institute))";
	 $res=$wpdb->get_results($sql);
	 return $res;
	}
       //----------------------------------------------------------------
       //Insert Star Institute---------------------------------------------
       public function insert_star_inst($teacher_id){
	global $wpdb;
	$sql="INSERT INTO pdg_star_institute SET inst_id='".$teacher_id."'";
	$res=$wpdb->get_results($sql);
	return 'success';
       }
       //----------------------------------------------------------------
       //Get star teacher list-------------------------------------------
       public function get_star_inst_list(){
	global $wpdb;
	$sql="SELECT a.id,a.inst_id,b.teacher_institute_name AS name FROM pdg_star_institute AS a
	LEFT OUTER JOIN pdg_teacher_institute_master AS b ON a.inst_id=b.user_id";
	$res=$wpdb->get_results($sql);
	return $res;
       }
       //----------------------------------------------------------------
        //Insert Star Teacher---------------------------------------------
       public function delete_star_inst($id){
	global $wpdb;
	$sql="DELETE FROM pdg_star_institute WHERE id='".$id."'";
	$res=$wpdb->get_results($sql);
	return 'success';
       }
       //----------------------------------------------------------------
       //-----------------------------------------------------------------------------------------------------------------------
}
