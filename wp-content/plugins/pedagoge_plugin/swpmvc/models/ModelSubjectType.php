<?php

class ModelSubjectType extends ModelMaster {
		
	public function __construct() {
		$this->table_name = 'pdg_subject_type';
		$this->set_primary_key('subject_type_id');
		$this->columns = $this->get_columns();
		$this->columns_datatype = $this->get_columns_type();		
	}
	
	private function columns_definition() {
		$columns_array = array();
		$columns_array['subject_type_id']['value'] = null;
		$columns_array['subject_type_id']['type'] = '%d';
		
		$columns_array['subject_type']['value'] = null;
		$columns_array['subject_type']['type'] = '%s';
		
		

		$columns_array['deleted']['value'] = 'no';
		$columns_array['deleted']['type'] = '%s';
		
		
		return $columns_array;
	}
	
	public function get_columns() {
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['value'];
		}
		
		return $columns_array;
	}
	
	public function get_columns_type() {
		
		$all_columns = $this->columns_definition();
			
		$columns_array = array();
		
		foreach($all_columns as $key=>$value) {
			$columns_array[$key] = $value['type'];
		}
		
		return $columns_array;		
	}
	
	public function subject_type_info_by_id($subject_type_id) {
		$return_var = null;
		
		if(!empty($subject_type_id) && is_numeric($subject_type_id)) {
			$return_var = $this->find_one($subject_type_id);
		}
		return $return_var;
	}
	
	public function subject_type_info_by_name($subject_type) {
		
		$return_data=null;
		if(!empty($subject_type)) {
			
			$columns_array = $this->get_columns();
			$columns_array['subject_type'] = $subject_type;
			
			$return_data = $this
							->where($columns_array)
							->find();
							
		}
		
		return $return_data;		
	}
	
}

