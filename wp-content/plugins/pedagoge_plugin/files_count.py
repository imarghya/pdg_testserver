import os
import csv

def count_code_files(directory):
	extensions = ('.js', '.css', '.php', '.sql')
	code_files_count = {'.js':0, '.css':0, '.php':0, '.sql':0,}
	code_lines_count = {'.js':0, '.css':0, '.php':0, '.sql':0,}
	excluded_directories = [
		'vendor', 
		'storage', 
		'references', 
		'proui', 
		'data', 
		'bower_components',
		'.git',
		'ace',
		'jquery-ui-1.11.4.custom',
		'jquery.bxslider',
		'helpers',
		'ckeditor',
		'ankitesh',
		'ankitesh',
		'plugins',
		'pages',
		'third-party',
		'themes'
	]
	
	code_stats_file_path = 'code_stats.csv'
	code_stats_file = open(code_stats_file_path, 'w')
	for root, directories, files in os.walk(directory):
		directories[:] = [d for d in directories if d not in excluded_directories]
		for filename in files:
			extracted_extension = os.path.splitext(filename)[1]
			if extracted_extension in extensions:
				code_files_count[extracted_extension] = code_files_count[extracted_extension] + 1
				filepath = os.path.join(root, filename)
				code_stats_file.write("%s\n" % filepath)
				#print(filepath)
				#print(rawpycount(filepath))
				line_count = rawpycount(filepath)
				code_lines_count[extracted_extension] = code_lines_count[extracted_extension] + line_count
				#with open(filepath) as f:				
					#print(sum(1 for _ in f))
						
	code_stats_file.close()	
	print(code_files_count)
	print(code_lines_count)

def _make_gen(reader):
    b = reader(1024 * 1024)
    while b:
        yield b
        b = reader(1024*1024)

def rawpycount(filename):
    f = open(filename, 'rb')
    f_gen = _make_gen(f.raw.read)
    return sum( buf.count(b'\n') for buf in f_gen )	
	
count_code_files(".")	