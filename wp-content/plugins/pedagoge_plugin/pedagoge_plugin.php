<?php
/**
 * Pedagoge Plugin
 * Pedagoge Plugin based on Wordpress MVC Framework
 * @link        https://www.pedagoge.com
 * @since        0.0.1
 * @package    pedagoge_plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Pedagoge Plugin
 * Plugin URI:        https://www.pedagoge.com
 * Description:       Pedagoge Plugin based on Wordpress MVC Framework
 * Version:           0.0.1
 * Author:            Niraj Kumar
 * Author URI:        https://github.com/nirajkvinit
 * Text Domain:       pedagoge_plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

show_admin_bar( false );
//////////////////////////////////////Constants Section Starts//////////////////////////////
define( "PEDAGOGE_COMBINE_MINIFY", false );
/**
 * PEDAGOGE Plugin URL Constant
 */
define( "PEDAGOGE_PLUGIN_URL", plugins_url() . "/pedagoge_plugin" );

/**
 * PEDAGOGE Theme URL Constant
 */
define( "PEDAGOGE_THEME_URL", get_template_directory_uri() );

/**
 * PEDAGOGE Plugin Directory Path Constant
 */
define( "PEDAGOGE_PLUGIN_DIR", plugin_dir_path( __FILE__ ) );

/**
 * PEDAGOGE Assets Dir URL Constant
 */
define( "PEDAGOGE_ASSETS_URL", PEDAGOGE_PLUGIN_URL . '/assets' );

/**
 * PEDAGOGE version 2 desktop Assets URL Constant
 */
define( "PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL", PEDAGOGE_THEME_URL . '/assets/ver2/desktop' );

/**
 * PEDAGOGE version 2 mobile Assets URL Constant
 */
define( "PEDAGOGE_THEME_V2_MOBILE_ASSETS_URL", PEDAGOGE_THEME_URL . '/assets/ver2/mobile' );


/**
 * PEDAGOGE PROUI Dir URL Constant
 */
define( "PEDAGOGE_PROUI_URL", PEDAGOGE_ASSETS_URL . '/proui' );
define( "PEDAGOGE_CP_ASSETS_URL", PEDAGOGE_ASSETS_URL . '/cp_assets' );

/**
 * PEDAGOGE Bower Components Dir URL Constant
 */
define( "BOWER_ROOT_URL", PEDAGOGE_ASSETS_URL . '/bower_components' );

/**
 * PEDAGOGE Bower Components Dir Constant
 */
define( "BOWER_ROOT_DIR", PEDAGOGE_PLUGIN_DIR . 'assets/bower_components' );

/**
 * PEDAGOGE MVC Dir Constant
 */
define( "PEDAGOGE_MVC_DIR", PEDAGOGE_PLUGIN_DIR . 'swpmvc' );

//////////////////////////////////////Constants Section Ends//////////////////////////////

include_once( PEDAGOGE_PLUGIN_DIR . 'includes/includes.php' );

/**
 * Set Default Timezone to website's default Timezone for better date/time calculations.
 */
date_default_timezone_set( pedagoge_get_wordpress_timezone_string() );

/**
 * Search and Load PHP Files automatically based on classes name
 */
spl_autoload_register( 'pedagoge_autoload_classes' );

/*****************************************************************************
 *****************************************************************************
 *                    Website Virtual Pages creator (Routes)
 *****************************************************************************
 *****************************************************************************/
/**
 * Pedagoge Application Endpoints (It can be modified)
 * Application would load as www.example.com/apps/...
 *
 * @todo Load this from wordpress options (Create a Plugin Settings Page in Wordpress Control Panel)
 * This plugin settings page will store the application configurations from wordpress options table.
 * Store everything in json. Also provide another option to save configurations in a XML configuration file.
 * Try the logger function.
 */
define( "PEDAGOGE_ENDPOINT_URL", '/' );

/**
 * Create Application's Routes
 */
$pedagoge_router = new PedagogeRouter();

//Cannot create masterpage as it is the homepage of the website.
//$pedagoge_router->fn_create_master_page(); 


//All the routes are include from routes.php file
include_once 'routes.php';



fn_pdg_dynamic_route_create( $pedagoge_router, $routes );

$pedagoge_router->fn_create_pages();

unset( $pedagoge_router ); //Clear garbage if any

/*****************************************************************************
 *****************************************************************************
 *                    Application Virtual Page creator ends here.
 *****************************************************************************
 *****************************************************************************/


/*****************************************************************************
 *****************************************************************************
 *            Catch WP login and signup and redirect to custom url
 *****************************************************************************
 *****************************************************************************/

add_action( 'login_form_register', 'pdg_wp_register' );
function pdg_wp_register() {
	wp_redirect( home_url( '/signup' ) );
	exit(); // always call `exit()` after `wp_redirect`
}

add_action( 'init', 'pdg_wp_login' );
function pdg_wp_login() {
	$login_page = home_url( '/login/' );
	$page_viewed = basename( $_SERVER['REQUEST_URI'] );

	if ( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET' ) {
		wp_redirect( $login_page );
		exit();
	}
}

add_action( 'wp_logout', 'logout_page' );
function logout_page() {
	$login_page = home_url( '/login/' );
	wp_redirect( $login_page );
	exit();
}

add_filter( 'authenticate', 'verify_username_password', 1, 3 );
function verify_username_password( $user, $username, $password ) {
	$login_page = home_url( '/login/' );
	//pedagoge_applog(__FUNCTION__.' called in '.__FILE__);
	if ( $username == "" || $password == "" ) {
		wp_redirect( $login_page );
		exit;
	}
}

/*****************************************************************************
 *****************************************************************************
 *    Catching WP login and signup and redirect to custom url ends here
 *****************************************************************************
 *****************************************************************************/

/**
 * Create Ajax Handler for Pedagoge Application
 * @todo elaborate and implement this properly
 */
$pedagoge_ajax_handler = new PDGAjaxController();

/**
 * Create/Delete Roles and permissions on plugin activation/deactivation
 * Will read from table pdg_user_role
 */
//function fn_activate_pedagoge_roles_permissions() {
	
	//do not need the commented code below becuase of different backup and restore strategy.
	
	/*$user_role_model = new ModelUserRole();
	$user_roles = $user_role_model->get_roles();
	
	if(!empty($user_roles)) {
		foreach($user_roles as $user_role) {
			$role_name = $user_role->user_role_name;
			$role_display = $user_role->user_role_display;
			add_role($role_name, $role_display, array('read' => true));
		}
	} else {
		add_role('teacher', 'Teacher', array('read' => true));
		add_role('student', 'Student', array('read' => true));
		add_role('institution', 'Institution', array('read' => true));
		add_role('guardian', 'Guardian', array('read' => true));
	}*/
	
	// Do not need this because updated scheduler is available.
	// However keeping this for reference and to satisfy scheduling requirements.
	// if ( ! wp_next_scheduled( 'pedagoge_hourly_event' ) ) {
		// wp_schedule_event( time(), 'hourly', 'pedagoge_hourly_event' );
	// }
//}

//add_action( 'pedagoge_hourly_event', 'pedagoge_hourly_scheduler' );

//function pedagoge_hourly_scheduler() {
	// Do not need this because updated scheduler is available.
	// However keeping this for reference and to satisfy scheduling requirements.
//}

//function fn_deactivate_pedagoge_roles_permissions() {
	//wp_clear_scheduled_hook( 'pedagoge_hourly_event' );
	
	//do not need the commented code below becuase of different backup and restore strategy.
	/*$user_role_model = new ModelUserRole();
	$user_roles = $user_role_model->get_roles();
	
	if(!empty($user_roles)) {
		foreach($user_roles as $user_role) {
			$role_name = $user_role->user_role_name;
			remove_role($role_name);			
		}
	} else {		
		remove_role('teacher');
		remove_role('student');
		remove_role('institution');
		remove_role('guardian');
	}*/
//}

// Never going to activate or deactive Pedagoge Plugin
//register_activation_hook( __FILE__, 'fn_activate_pedagoge_roles_permissions' );
//register_deactivation_hook( __FILE__, 'fn_deactivate_pedagoge_roles_permissions' );

/**
 * Scheduling Events
 */

add_filter( 'cron_schedules', 'pedagoge_add_weekly_schedule' );
function pedagoge_add_weekly_schedule( $schedules ) {

  $schedules['weekly'] = array(
    'interval' => 7 * 24 * 60 * 60, //7 days * 24 hours * 60 minutes * 60 seconds
    'display' => __( 'Once Weekly' )
  );

  $schedules['half_hourly'] = array(
    'interval' => 30 * 60,
    'display' => __( 'Half Hourly' )
  );
  return $schedules;
}
//weekly events
add_action('pedagoge_weekly_scheduler','fn_pedagoge_weekly_scheduler');
if(!wp_next_scheduled('pedagoge_weekly_scheduler')) {
	wp_schedule_event(time(), 'weekly', 'pedagoge_weekly_scheduler');
}

//daily events
add_action('pedagoge_daily_scheduler','fn_pedagoge_daily_scheduler');
if(!wp_next_scheduled('pedagoge_daily_scheduler')) {
	wp_schedule_event(time(), 'daily', 'pedagoge_daily_scheduler');
}

//half hourly events
add_action('pedagoge_half_hourly_scheduler','fn_pedagoge_half_hourly_scheduler');
if(!wp_next_scheduled('pedagoge_half_hourly_scheduler')) {
	wp_schedule_event(time(), 'half_hourly', 'pedagoge_half_hourly_scheduler');
}

//hourly events
add_action('pedagoge_hourly_scheduler','fn_pedagoge_hourly_scheduler');
if(!wp_next_scheduled('pedagoge_hourly_scheduler')) {
	wp_schedule_event(time(), 'hourly', 'pedagoge_hourly_scheduler');
}

function fn_pedagoge_weekly_scheduler() {
	$is_production_env = PedagogeUtilities::is_production_env();
	if($is_production_env) {
		ControlPanelTeachers::fn_prep_incomplete_profiles_for_reminder();	
	}
}

function fn_pedagoge_daily_scheduler() {
	$is_production_env = PedagogeUtilities::is_production_env();
	if($is_production_env) {		
		//send institute data dump
		ControlPanelPedagogeImpex::fn_institute_dump();
		
		//send teacher data dump
		ControlPanelPedagogeImpex::fn_teacher_dump();	
		
		// Expire Workshops //dependent on issue122 code
		ControllerManageworkshop::expire_workshop();
	}	
}

function fn_pedagoge_hourly_scheduler() {
	$is_production_env = PedagogeUtilities::is_production_env();
	if($is_production_env) {
		// Update cache
		PDGManageCache::fn_update_cache();
	}
}

function fn_pedagoge_half_hourly_scheduler() {
	$is_production_env = PedagogeUtilities::is_production_env();
	if($is_production_env) {
		ControlPanelTeachers::fn_send_incomplete_profile_reminder();	
	}
}
