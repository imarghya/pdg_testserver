<?php
/*
Template Name: Careers - Graphic Design
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;
							<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Graphic Design
						</div>
						<div class="opPara1 jobDescPara">
							<p><strong>About the Internship</strong>:<br/> The selected intern(s) will work on following
								during the internship:&nbsp;</p>
							<p>1. Will be helping us with designing software such as Illustrator, Photoshop &amp; after
								effects.&nbsp;<br/> 2. Must have a good hand in sketching.&nbsp;<br/> 3. Will help
								making creative content for the company, both offline/online like social media posts,
								flyers, brochures, GIF&#39;s.</p>
							<p><strong>Must have skills</strong>:<br/> -Illustrator<br/> -Photoshop<br/> -After
								Effects<br/> -A good hand at sketching</p>

							<p>Compensation: Competitive salary and performance based incentives</p>
							<p>&nbsp;</p>
							<p><strong>Location</strong>: Kolkata</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor, Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata -
								700091,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home_ver2' );