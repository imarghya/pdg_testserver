<?php
/*
Template Name: Careers Page
*/

get_header( 'home' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";

?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Why join us ?
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6 tileblockinner tile t1 titlePadding tilePos">
				<h1>Learn</h1>
			</div>
			<div class="col-xs-12 col-md-6 tileblockinner tile t2 titlePadding tilePos">
				<h1>Create</h1>
			</div>
			<div class="col-xs-12 col-md-6 tileblockinner tile t3 titlePadding tilePos">
				<h1>Explore</h1>
			</div>
			<div class="col-xs-12 col-md-6 tileblockinner tile t4 titlePadding tilePos">
				<h1>Fun</h1>
			</div>
		</div>
	</section>
	<section class="wrapper container-fluid section3-careers">
		<div class="row">
			<div class="col-xs-12 col-lg-12 col-md-12 jobHeader titlePadding text-center">
				<i class="fa fa-search" aria-hidden="true"></i> Openings
			</div>
		</div>
		<!--		Start full time job-->
		<div class="row">
			<div class="col-xs-12 col-md-6 opWrapper">
				<h4 class="text-center titleWrapper">Full time</h4>
				<!-- <div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle1">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Marketing
								Associate
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">We are looking for a marketing associate with a knack to learn and
							grow with a startup. An enthusiastic professional having excellent communication and strong
							networking skills is preferred. <a
									href="<?= $joburl; ?>-marketing-associate">See more details..</a>
						</div>
					</div>
				</div> -->
<!-- 				<div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle1">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Sales &
								Operations Head
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">We are looking for a Sales & Operations Head for Kolkata with
							experience in helping a business grow and the ability to create business plans for our
							future. <a
									href="<?= $joburl; ?>-sales-operations-head">See more details..</a>
						</div>
					</div>
				</div> -->
				<div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle1">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Sales Associate
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">The opportunity involves looking after a dynamic work flow right
							from generating a lead to meeting and fulfilling the requirements of clients. <a
									href="<?= $joburl; ?>-sales-associate">See more details..</a>
						</div>
					</div>
				</div>
				<!-- <div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle1">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Associate Web
								Developer
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">In today's day and age working with a start-up seems to be the
							&quot;thing to do&quot; and we here at Pedagoge look at business in its rawest form, which
							is to create impact.&nbsp;<a
									href="<?= $joburl; ?>-associate-web-developer">See more details..</a>
						</div>
					</div>
				</div> -->
				<div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle1">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Finance &
								Operations Associate
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">Pedagoge is looking for a finance and operations associate with a
							knack to learn and grow with a startup. An enthusiastic professional having excellent
							communication skills and well versed with Indian Accounting Practices and Policies is
							preferred.&nbsp;<a
									href="<?= $joburl; ?>-finance-operations-associate">See more details..</a>
						</div>
					</div>
				</div>
			</div>
			<!--end full time-->
			<!--start interns-->
			<div class="col-xs-12  col-md-6 opWrapper">
				<h4 class="text-center titleWrapper">Internships</h4>
<!-- 				<div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle4">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Web Developers
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">We are looking for proactive individuals who will help develop our
							website from an early stage. <a href="<?= $joburl; ?>-web-developers">See more details..</a>
						</div>
					</div>
				</div> -->
				<!-- <div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle4">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Graphic Design
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">Need interns who will be helping us with designing software such as
							Illustrator, Photoshop & After Effects.&nbsp;<a href="<?= $joburl; ?>-graphics-design">See
								more details..</a>
						</div>
					</div>
				</div> -->
				<div class="col-xs-12 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle4">
							<div class="col-xs-10 text-left">
								&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Business
								Development
							</div>
							<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
							                                    aria-hidden="true"></i></div>
						</div>
						<div class="opPara makeLink">Individuals who are willing to bring clients on board and have the
							ability to communicate with clarity in thought and making the message stand out. <a
									href="<?= $joburl; ?>-business-development">See more details..</a>
						</div>
					</div>
				</div>

			</div>
			<!--end interns-->
		</div>
	</section>
<?php
get_footer( 'home' );