<?php
/*
Template Name: Careers Sales Associate Page
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;
							<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Sales Associate
						</div>
						<div class="opPara1 jobDescPara">
							<p>As a sales associate you need to take part in activities and efforts to expand sales, meet sales targets, to attract new clients and making contacts with potential clients to create new business opportunities.</p>
							<p><u>Requirements and responsibilities</u>:&nbsp; </p>
							<ul>
								<li>Experience in managing an e-commerce sales force to achieve sales and profit goals.</li>
								<li>Excellent communication skills. (A must)</li>
								<li>Should be enthusiastic and proactive, willing to find solutions to every problem.</li>
								<li>Should have the ability to convince people with ease (on-field as well as over a call).</li>
								<li>Should be able to design and recommend online sales programs along with setting short and long-term online sales strategies.</li>
								<li>Increase business of the company by liaising with prospective leads and delivering their requirements.</li>
								<li>Should have prior experience of business development and sales.&nbsp;</li>
							</ul>
							<p><u>What should the candidate expect from us</u>:</p>
							<ul>
								<li>Working with the core team of a Nasscom 10,000 startup in the EduTech industry.</li>
								<li>Building and managing a team.</li>
								<li>Tremendous growth and learning opportunity.</li>
								<li>Deep insights into the private coaching, B2C and B2B education industry in India.</li>
								<li>Ownership of work and freedom to experiment new marketing concepts.</li>
							</ul>
							<p><strong>Experience:</strong> 1 to 3 years</p>
							<p><strong>Location:</strong> Kolkata</p>
							<p style="text-align: center; font-size: 15px;">&nbsp;</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor,
								Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata - 700091,<br/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West
								Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer('home_ver2');