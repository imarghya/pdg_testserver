<?php 
/*
Template Name: Comingsoon
*/

/**
 * Had to create different header, footer, and page templates because of spaghetti code
 * Will clean the code and create uniform structure (Header/Footer/Pages) after new design comes in.
 */

get_header('comingsoon');
?> 
 	<!-- Coming Soon Background -->
	<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
	<!-- <img src="<?php echo get_template_directory_uri(); ?>/comingsoonassets/img/placeholders/backgrounds/coming_soon_full_bg.jpg" alt="Coming Soon Background" class="animation-pulseSlow full-bg"> -->
	<!-- END Coming Soon Background -->
	
	<!-- Coming Soon -->
	<div class="full-page-container text-center ">
	    <!-- Title -->
	    <h1 class="text-light"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/home_01.png" /></h1>
	    <h2 class="h3 text-light">We are working hard so that you get to learn what you want, the way you want!</h2>
	    
	    <!-- END Title -->
	
	    <!-- Countdown -->
	    <div class="full-page-section">
	        <!-- Countdown.js (class is initialized in js/pages/readyComingSoon.js), for extra usage examples you can check out https://github.com/hilios/jQuery.countdown -->
	        <!-- <div class="js-countdown"></div> -->
	        <div class="row">
	        	
	        	<div class="col-md-12" id="contact_form_content">
	        		<h3 class="h4 text-light">In the meanwhile, please fill the form below to get in touch!</h3>
	        		<iframe src="https://docs.google.com/forms/d/1gT0es38eLO18BdOP3CVUCntAlzSU1bXPkH2zvUZq94g/viewform?embedded=true" width="700" height="750" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
	        	</div>	        	
	        </div>
	    </div>
	    <!-- END Countdown -->
	    <div class="row">
	    	<div class="col-sm-6 col-md-6">
                <center>
                    <h4 class="footer-heading" style="line-height: 0.2em;">Follow Us</h4>
                    <ul class="footer-nav footer-nav-social list-inline">
                        <li><a href="https://www.facebook.com/pedagoge0/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/pedagogebaba" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/pedagoge" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="https://www.instagram.com/pedagogebaba/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                    <br />
                    <p class="footer-nav">Give us a call on +91-98833 31570</p>
                                        
                </center>
            </div>
            <div class="col-sm-6 col-md-6">
            	<center>
                    <p class="footer-nav"> Subscribe to our &nbsp;<a href="http://blog.pedagoge.com/" target="_blank"><i class="fa fa-thumb-tack"></i>&nbsp;&nbsp;<b>BLOG</b></a> </p>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Footer/home_52.png">
                </center>
            </div>
	    </div>
	</div>
	<!-- END Coming Soon -->

<?php
get_footer('comingsoon');
