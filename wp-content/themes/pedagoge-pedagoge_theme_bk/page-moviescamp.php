<?php
/*
Template Name: Movies campaign
*/




get_header('home');
$theme_dir = get_template_directory_uri();
$search_url = home_url('/search/');
?>
	<div class="wrapper container-fluid">
	<div class="row headersection-moviescamp bgImg-movies">
		<div class="logo-marketingcamp">
			<div class="col-xs-6 text-left">
				<a href="<?php echo home_url('/'); ?>">
					<img class="logo-adjust" src="<?= $theme_dir; ?>/assets/img/logo.png" alt="Pedagoge Logo"
					     width="">
				</a>
			</div>
		</div>
		<div class="heading-moviescamp">
			<div class="col-xs-12 text-center heading-moviescamp heading-adjust-movies">
				<i>Don't be dramatic!</i><br><h2>Find the right teacher.</h2>
			</div>
		</div>
		<div class="container searchbar-moviescamp">
			<div class="hidden-md hidden-lg">
				<div class="row col-xs-12">
					<div class="col-xs-3 col-sm-2">
					</div>
					<div class="col-xs-8 search_locality nopadding">
						<select class="col-md-12 col-xs-4 search-locality" data-allow-clear="true"
						        style="color: #000000; width:100%;">
							<option value="">Enter Locality</option>
							<?php get_template_part('template-parts/header', 'search'); ?>
						</select>
					</div>
				</div>
				<div class="row col-xs-12">
					<div class="col-xs-3 col-sm-2">
					</div>
					<div class="col-xs-8 search_locality nopadding">
						<input type="text" class="form-control input-lg search_subject" id="search_subject"
						       placeholder="Enter Subject: eg: English, Dance, Guitar">
					</div>
				</div>
				<div class="row col-xs-12">
					<div class="col-xs-3 col-sm-2">
					</div>
					<div class="col-xs-8 search_locality nopadding">
						<button type="button"
						        class="col-xs-3 form-control input-lg btn btn-success cmd_home_search_btn">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="visible-md visible-lg">
				<div class="row col-md-12">
					<div class="col-md-3">
					</div>
					<div class="col-md-2 search_locality nopadding">
						<select class="col-md-12 col-xs-4 search-locality search-locality1" data-allow-clear="true"
						        style="color: #000000; width:100%;">
							<option value="">Enter Locality</option>
							<?php get_template_part('template-parts/header', 'search'); ?>
						</select>
					</div>
					<div class="col-md-4 search_locality nopadding">
						<input type="text" class="form-control input-lg search_subject search_subject1" id="search_subject"
						       placeholder="Enter Subject: eg: English, Dance, Guitar">
					</div>
					<div class="col-md-1 search_locality nopadding">
						<button type="button"
						        class="col-md-1 form-control input-lg btn btn-success cmd_home_search_btn1">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row container-fluid crop">
		<div class="bg-reel text-left">
			<img src="<?= $theme_dir; ?>/assets/img/movies/reel.png"
			     height="auto"/>
		</div>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-xs-2 visible-xs">
				</div>
				<div class="col-xs-12 divider1-section2-marketingcamp">
				</div>
				<div class="col-xs-10 col-sm-12 text-center big-mobilecamp">
					<span>CBSE se le ke UPSC tak,<br>Spanish se le ke Tennis tak...</span>


				</div>
				<div class="col-xs-1 visible-xs">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-1 visible-xs">
				</div>
				<div class="col-xs-12 divider1-section2-marketingcamp">
				</div>
				<div class="col-xs-10 col-sm-12 section3-marketingcamp">
					<div class="section2-content-marketingcamp">
						<span class="label-marketingcamp">Teacher search made easy</span>
						<button type="button" onclick="location.href='<?= $search_url ?>'" class="btn btn-info">SEARCH
							MORE!
						</button>
						<div class="clear-marketingcamp"></div>
					</div>
				</div>
				<div class="col-xs-1 visible-xs">
				</div>
			</div>
		</div>
	</section>
<?php
get_footer('home');