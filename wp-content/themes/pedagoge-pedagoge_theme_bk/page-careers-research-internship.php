<?php
/*
Template Name: Careers Research Internship
*/

get_header( 'home' );
$theme_dir = get_template_directory_uri();
$joburl    = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;
							<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Research Internship
						</div>
						<div class="opPara1 jobDescPara">
							<p class="p1"><span class="s1"><b>About the Internship:</b></span></p>
							<p class="p2"><span class="s1">The selected intern(s) will work on following during the internship:&nbsp;</span>
							</p>
							<p class="p2"><span class="s1">- Market Research of multiple cities based on various relevant parameters and data should be mainly primary data.</span>
							</p>
							<p class="p2"><span class="s1">- Extensive market research, competitive &amp; financial analysis, future prospects of a new division being set up by the company</span>
							</p>
							<p class="p2"><span class="s1">- Analyze a specific kind of SaaS product available in the country, its feedback and reviews</span>
							</p>
							<p class="p2"><span
									class="s1">- Research on corporate training landscape in India&nbsp;</span></p>
							<p class="p2"><span class="s1">- Competitive analysis and customer response technology involvement in it.</span>
							</p>
							<p class="p2"><span class="s1">- Researching on applications and websites with the same target group as the company.</span>
							</p>
							<p class="p2"><span class="s1">- Analyzing possible features for a website based on data of other marketplace model.</span>
							</p>
							<p class="p2"><span class="s1">- Interacting with clients, customers, doing field work (on-field research).</span>
							</p>
							<p class="p2"><span class="s1">- Doing research of potential and current usage of SaaS products in India for a teacher/professor.</span>
							</p>
							<p class="p3"><span class="s1"><b>Who can apply:</b></span></p>
							<p class="p1">
							</p>
							<ul class="ul1">
								<li class="li2"><span class="s1">Skill(s) required: MS-PowerPoint, MS-Excel, English Proficiency (Spoken) and English Proficiency (Written) and Hindi Proficiency (Written).</span>
								</li>
								<li class="li2"><span class="s1">Degree(s): Applicants pursuing any degree but having relevant skills and interest may apply.</span>
								</li>
								<li class="li2"><span class="s1">Year of degree completion: 2016 or earlier.</span></li>
								<li class="li2"><span class="s1">Only applicants who are available for a minimum duration of 3 month(s) can apply.</span>
								</li>
								<li class="li2"><span class="s1">Applicants should be available to start the internship immediately (&amp; no later than 9 Sep'16 ).</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor,
								Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata - 700091,<br/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West
								Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home' );