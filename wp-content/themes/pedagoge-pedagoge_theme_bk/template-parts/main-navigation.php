<?php
$current_page_slug = '';
/**
 * Current Page URL
 */
$pdg_current_page_url = "http" . ( ( $_SERVER['SERVER_PORT'] == 443 ) ? "s://" : "://" ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$wordpress_home_url   = trim( home_url() );

/**
 * Removing wordpress home url part from current url so that we can get the url path to work with.
 * This technique had to be implemented because earlier implementation did not work for localhost.
 */
$pdg_replaced_url = str_replace( $wordpress_home_url, '', $pdg_current_page_url );

/**
 * Seperate the url to path and query and just take url path.
 */
$pdg_processed_url_path = parse_url( $pdg_replaced_url, PHP_URL_PATH );

?>
<header>
	<div class="container">
		<!-- Site Logo -->
		<?php
		global $post;
		$slug = '';
		if ( isset( $post ) ) {
			$current_post = get_post( $post );
			if ( ! empty( $current_post ) ) {
				$slug = $current_post->post_name;
			}
		}
		//Change the logo on the navigation bar
		switch ( $slug ) {
			case 'business':
				$logoURL       = get_template_directory_uri() . '/assets/img/business/logo-business.png';
				$logoLeadToURL = home_url( '/business' );
				break;
			default:
				$logoURL       = get_template_directory_uri() . '/assets/img/pedagoge_beta.png';
				$logoLeadToURL = home_url( '/' );
				break;
		}
		?>
		<a href="<?php echo $logoLeadToURL; ?>" class="site-logo">
			<img src="<?php echo $logoURL; ?>" class="img-responsive"
			     alt="Pedagoge Logo" style="height: 1.4em;"> </a>
		<!-- Site Logo -->
		<!-- Site Navigation -->
		<nav>
			<!-- Menu Toggle -->
			<!-- Toggles menu on small screens -->
			<div class="menuToggle">
				<?php
				if ( $pdg_processed_url_path == '/search/' ) {
					get_template_part( 'template-parts/search-page', 'filter' );
				}
				?>
				<a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
					<i class="fa fa-bars"></i>
				</a>
			</div>
			<!-- END Menu Toggle -->

			<!-- Main Menu -->
			<ul class="site-nav">
				<!-- Toggles menu on small screens -->
				<li class="visible-xs visible-sm">
					<a href="javascript:void(0)" class="site-menu-toggle text-center">
						<i class="fa fa-times"></i>
					</a>
				</li>
				<!-- END Menu Toggle -->
				<?php
				$headerLinks = array (
					//'workshop'   => '<li><a href="' . home_url( '/workshop' ) . '" target="_blank"><i class="fa fa-cogs" aria-hidden="true"></i> Workshop</a></li>',
					'about-us'   => '<li><a href="' . home_url( '/about-us' ) . '" target="_blank"><i class="fa fa-anchor" aria-hidden="true"></i> About Us</a></li>',
					'contact-us' => '<li><a href="' . home_url( '/contact-us' ) . '" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contact Us</a></li>',
					'search'     => '<li><a href="' . home_url( '/search' ) . '" target="_blank"><i class="fa fa-search" aria-hidden="true"></i> Search</a></li>',
				);
				switch ( $slug ) {
					case 'business':
						$headerLinksItem = array ( 'contact-us' );
						break;
					default:
						$headerLinksItem = array ( 'workshop', 'about-us', 'contact-us', 'search' );
						break;
				}
				$linkGenerated = '';
				if ( ! empty( $headerLinksItem ) ) {
					foreach ( $headerLinksItem as $headerLinksItemValue ) {
						if ( isset( $headerLinks[ $headerLinksItemValue ] ) ) {
							$linkGenerated .= $headerLinks[ $headerLinksItemValue ];
						}
					}
				}
				echo $linkGenerated;
				switch ( $slug ) {
					case 'business':
						if ( is_user_logged_in() ) {
							get_template_part( 'template-parts/menu', 'loggedin' );
						} else {
							get_template_part( 'template-parts/menu', 'businesssignup' );
						}
						break;
					default:
						if ( is_user_logged_in() ) {
							get_template_part( 'template-parts/menu', 'loggedin' );
						} else {
							get_template_part( 'template-parts/menu', 'signup' );
						}
						break;
				}

				?>
			</ul>
			<!-- END Main Menu -->


		</nav>
		<!-- END Site Navigation -->
	</div>
</header>
<!-- END Site Header -->