<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pedagoge_theme
 */
$dashboard_menu_item = '<li>
            <a href="'.home_url('/dashboard').'"><i class="fa fa-tachometer" aria-hidden="true"></i> Control Panel</a>
        </li>
    ';
$wordpress_dashboard_menu_item = '<li>
            <a href="'.home_url('/wp-admin').'"><i class="fa fa-tachometer" aria-hidden="true"></i> Wordpress Dashboard</a>
        </li>
    ';    
$current_user = wp_get_current_user();
$loggedin_menu_items = '';
if(current_user_can('manage_options')) {
    $loggedin_menu_items = $dashboard_menu_item . $wordpress_dashboard_menu_item;
} else if (current_user_can('pdg_cap_cp_access')) {
    $loggedin_menu_items = $dashboard_menu_item;
} else {
 	$loggedin_menu_items = '
 		<li>
            <a href="'.home_url('/profile').'" class="active"><i class="fa fa-user"></i> Profile</a>
        </li>
        <li>
            <a href="'.home_url('/reset').'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Change Password</a>
        </li>
 	';
 }
 $str_current_user_full_name = '';
 $first_name = trim($current_user->user_firstname);
 $last_name = trim($current_user->user_lastname);
 $display_name = trim($current_user->display_name);
 $str_current_user_name = trim($current_user->user_login);
 
 if(!empty($display_name)) {
 	$str_current_user_full_name = $display_name;
 } else if(!empty($first_name)) {
 	$str_current_user_full_name = $first_name.' '.$last_name;
 } else {
 	$str_current_user_full_name = $str_current_user_name;
 }
 
?>

<li class="active">
    <a href="javascript:void(0)" class="site-nav-sub"><i class="fa fa-user"></i> <?php echo $str_current_user_full_name; ?><i class="fa fa-chevron-down"></i></a>
    <ul>
		<?= $loggedin_menu_items; ?>
        <li>
            <a href="<?php echo wp_logout_url(); ?>" id="pdg_menu_logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</a>
        </li>
    </ul>
</li>