<div class="home" id="home">
	<div class="overlay"></div>
</div>

<div class="home1" id="home1">
	<div class="container">
		<div class="row">
			<div class="home-text col-md-8">
				<div class="wow fadeInDown "><img class="mobileview1 invertmobileview" src="http://pedagoge.com/logo.png" style=""><br><br></div>
				<p><br></p>
				<!--<p class="lead wow fadeInDown"  data-wow-duration="1.5s" data-wow-offset="10" style="margin-top:-5%;">Start your search now!</p>-->
				<div class="row">
					<!-- newly added row-->
					<p id="homesearch">
					<div  style="margin-top:-12%; height : auto; border: 1px solid rgba(34,186,160,0.2); border-radius: 25px; padding-top:10px; padding-bottom:10px; background : rgba(34,186,160,0.3);"  class="wow fadeInDown" data-wow-duration="1.5s" data-wow-offset="10"  data-wow-duration="1.5s" data-wow-offset="10">
						<form class="form-inline" method="get" action="search/index.php">
							<div class="form-group">
								<label class="sr-only" for="exampleInputEmail2" >Interested Subject</label>
								<div id="the-basics1">
									<input style="width: 300px; border: 1px solid rgba(255,255,255,0.8);" name = "subject" class="typeahead  form-control" id="subject" placeholder="Interested Subject (Eg : Physics, Fitness)">
								</div>
							</div>
							<div class="form-group">
								<label class="sr-only" for="exampleInputPassword2">Locality</label>
								<div id="the-basics">
									<input style="width: 300px;border: 1px solid rgba(255,255,255,0.8);" name = "locality" id = "locality" class="typeahead form-control"  type="text" placeholder="Locality (Eg : Salt Lake, Alipore)">
								</div>
							</div>
							<button type="submit" class="btn btn-info btn-rounded   wow fadeInDown" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Search', 'true'])">Search</button>										
						</form>
						<br>
						<br>
						<div class="row mobileview">
							<div class="col-md-4">
								<a href="/register/?registeras=guardian">
									<div class="row guardianbg"></div>
									<div class="row">
								<a href="/register/?registeras=guardian" style="text-decoration: none; color:white;"><h4>Register as guardian</h4></a>
								</div>
								</a>
							</div>
							<div class="col-md-4">
								<a href="/register/registeras/">
									<div class="row teacherbg"></div>
									<div class="row">
								<a href="/register/registeras/" style="text-decoration: none; color:white;"><h4>Register as teacher</h4></a>
								</div>
								</a>
							</div>
							<div class="col-md-4">
								<a href="/register?registeras=student">
									<div class="row studentbg"></div>
									<div class="row">
								<a href="/register?registeras=student" style="text-decoration: none; color:white;"><h4>Register as student</h4></a>
								</div>
								</a>
							</div>
						</div>
						<!--removing the comment will make the text and image of walter white separate when seen on mobile screen-->	
						<!--<div class="row">
							<div class="col-xs-6 col-sm-3">
							</div>
							<div class="col-xs-6 col-sm-3">		
								<a href="/register" style="text-decoration: none; color:white;"><h4>Register as teacher</h4></a>									
							</div>
							<div class="col-xs-6 col-sm-3">	
								<a href="/register" style="text-decoration: none; color:white;"><h4>Register as student</h4></a>					
							</div>								
							<div class="col-xs-6 col-sm-3">
							</div>	
							</div>	-->					
					</div>
					</p>
				</div>
				<p><br></p>
				<div class="row">
					<p class="lead wow fadeInDown mobileview"  data-wow-duration="1.5s" data-wow-offset="10">
						Welcome to the world of improved learning experiences!  <br> Search and connect with private teachers (academic & non-academic), based on individual needs. Reviews and recommendations sourced from organically built trusted community of students and teachers facilitate well informed decisions. 
					</p>
				</div>
			</div>
						
			<div class="scroller">
				<a href="#section-1">
					<div class="mouse">
						<div class="wheel"></div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>