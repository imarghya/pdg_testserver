<footer>		                   
	<hr>
	<div class="container">
		<div class="row centered text-center">
			<div class = "col-md-4 centered">
				<h3>CONNECT</h3>
				<br/>
				<a href="<?php echo home_url('/about-us'); ?>">About Us</a><br/>
				<a href="<?php echo home_url('/#contact'); ?>">Get In Touch</a>
			</div>
			<div class = "col-md-4 centered">
				<h3>LEGAL</h3>
				<br/>
				<a href="<?php echo home_url('/privacy-policy'); ?>">Privacy Policy</a><br/>
				<a href="<?php echo home_url('/terms-and-condition'); ?>">Terms &amp; Condition</a>			
			</div>
			<div class = "col-md-4 centered">
				<h3>SERVICE</h3>
				<br/>
				<a href="<?php echo home_url('/teachers'); ?>">Pedagoge For Teacher</a><br/>
				<a href="<?php echo home_url('/#features1'); ?>">Pedagoge For Students</a>		
			</div>			
		</div>
		<br/> <hr>
		<div class="col-md-12">
			<center><img src = "<?php echo get_template_directory_uri(); ?>/assets/images/10kstartup.png" class="img-responsive"></center>
		</div>
		<p class="text-center no-s"><?php echo date('Y'); ?> &copy; <a href="<?php echo home_url('/') ?>">Pedagoge</a>.</p>
	</div>
</footer>