<?php
	global $post;
	$slug = get_post( $post )->post_name;
?>
<div class="<?php echo ($slug=='teachers'?'teacherhome':'home'); ?>" id="home" style="height:70%;"> <!-- Div with ID home starts -->			
    <div class="overlay"></div>            
    <div class="container"> <!-- div with class container starts here -->            	
        <div class="row">                	
            <div class="home-text col-md-8">
                <h1 class="wow fadeInDown">
                	<img src="http://pedagoge.com/logo.png" class="pedagoge_logo_img" width=30% />
                </h1>
                <p><br></p>
				<?php												
					switch($slug) {
						case 'privacy-policy':
							get_template_part( 'template-parts/pedagoge/lead', 'privacy' );
							break;
						case 'terms-and-conditions':
							get_template_part( 'template-parts/pedagoge/lead', 'terms' );
							break;
						case 'teachers':
							get_template_part( 'template-parts/pedagoge/lead', 'teachers' );
							break;
					} 
					 
				?>
            </div>
            
            <div class="scroller">
			   <!-- <div class="mouse"><div class="wheel"></div></div> -->
            </div>
            
        </div>
        
    </div>  <!-- div with class container ends here -->
    
</div> <!-- Div with ID home ends here -->