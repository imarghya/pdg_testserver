<!-- Login modal starts here -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Login To Pedagoge</h4>
            </div>
            <div class="modal-body">
                <form class="m-t-md" action="../includes/process_login.php" method="post" name="login_form" id="login_form">
					<div class="form-group">
						<input id="pe_email" type="email" class="form-control validate[required]" name="email" placeholder="Email" value="<?=isset($_COOKIE['login_email'])?$_COOKIE['login_email']:''?>" >
					</div>
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control" placeholder="Password" value="<?=isset($_COOKIE['login_password'])?$_COOKIE['login_password']:''?>" >
					</div>
					<input type="button" class="btn btn-success btn-block" value="Login" onclick="formhash(this.form, this.form.password);" /> 												
					<a href="../forgot.html" class="display-block text-center m-t-md text-sm">Forgot Password?</a>
					<br>
					<label><input type="checkbox" id="showpassword" value="no"> Show Password</label>
					<label class="remember" style="margin-left: 10px;"><input type="checkbox" name="rememberme" id="rememberme" class="" value="no" <?=isset($_COOKIE['remember_me'])?'checked':''?>> Remember me</label>
					<p class="text-center m-t-xs text-sm">Do not have an account?</p>
					<a href="../../register" class="btn btn-default btn-block m-t-md">Create an account</a>
				</form>
            </div>                                                        
        </div>
    </div>
</div>
<!-- Login modal ends here -->

<!-- Signup Modal starts here -->
<div class="modal fade bs-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Sign Up To Pedagoge</h4>
            </div>
            <div class="modal-body">
                <form class="m-t-md" action="../register/index.php" method="post" name="registration_form">
    
					<div class="form-group">
						<input type="name" name="name" id="name" class="form-control" placeholder="First name" required>
					</div>

					<div class="form-group">
						<input type="name" name="lastname" id="lastname" class="form-control" placeholder="Last name" required>
					</div>

					<div class="form-group">
						<input type="email" name="email" id="username" class="form-control" placeholder="Email" required>
					</div>
					
					<div class="form-group">
						<input type="password" name="password" id="password1" class="form-control" placeholder="Password" required>
					</div>
					
					<div class="form-group">
						<select type="role" name="role" id="role" class="form-control m-b-sm" required>
							<option selected="true" disabled>Sign Up as</option>
							<option name="role" value="student">Student</option>
							<option name="role" value="teacher">Teacher</option>  
							<option name="role" value="institution">Institution</option> 
							<option name="role" value="guardian">Guardian</option>                                                           
						</select>
					</div>
					
					<label>
						By clicking signup, you are agreeing to the <a href="terms/">terms and conditions</a> and the <a href="privacypolicy/">privacy policy</a> set by Pedagoge.
					</label>

					<br>
					<label><input type="checkbox" class="showpassword" value="no"> Show Password</label>

					<input type="button" class="btn btn-success btn-block m-t-xs" value="Sign Up" onclick="return regformhash(this.form,this.form.name,this.form.lastname,this.form.email,this.form.password,this.form.role);"/>
					<p class="text-center m-t-xs text-sm">Already have an account?</p>
					<a href="../login" class="btn btn-default btn-block m-t-xs">Login</a>
					
				</form>									
            </div>                                                        
        </div>
    </div>
</div>
<!-- Signup Modal ends here -->