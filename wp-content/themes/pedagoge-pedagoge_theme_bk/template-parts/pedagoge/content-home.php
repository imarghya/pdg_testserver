<section id="section-1">
	<div class="container" id="features">
		<div class="row">
			<div class="col-md-12">
				<center>
					<h1 style="margin-bottom:-40px;">How to start learning?</h1>
				</center>
				<section id="cd-timeline" class="cd-container">
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-success">
							<!-- <i class="fa fa-compass"></i> -->
							<img class="icon_timeline" src = "<?php echo get_stylesheet_directory_uri(); ?>/assets/images/homepage/discover.png" alt="Pedagoge Discover"> 
						</div>
						<!-- cd-timeline-img -->
						<div class="cd-timeline-content">
							<h2>DISCOVER</h2>
							<p>Start by entering your desired location of coaching and interested subjects into the search bar. <br><br>
								<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Learn more', 'true'])">Learn more</button>				
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-danger">
							<i class="fa fa-hand-o-up"></i>
						</div>
						<!-- cd-timeline-img -->
						<div class="cd-timeline-content">
							<h2>RESERVE</h2>
							<p>You get assured benefits when you book a seat in a new class through Pedagoge. When a teacher responds to your request, we send you an email to let you know.<br><br>
								<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg1" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Learn more', 'true'])">Learn more</button>	 
							</p>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-info">
							<i class="fa fa-user-plus"></i>
						</div>
						<!-- cd-timeline-img -->
						<div class="cd-timeline-content">
							<h2>JOIN</h2>
							<p>Once you know where you're attending and under whose tutelage, all you have to do is connect with your respective teacher/coach/institute and get going! <br><br>
								<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg2" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Learn more', 'true'])">Learn more</button>	
							</p>
						</div>
						<!-- cd-timeline-content -->
					</div>
					<!-- cd-timeline-block -->
				</section>
				<!-- cd-timeline -->
			</div>
		</div>
		<!-- Row -->
	</div>
</section>

<!-- <div class=" background-image-centered" style="color:inherit; background-color:#20ac94; background-image: url(http://www.carbonite.com/globalassets/images-breakers/sphere-center1.png);"> -->
<div class=" background-image-centered parallax-window2">
	<div class="container-fluid gray-headline-text-block">
		<div class="container">
			<div class="one-column">
				<div class="row">
					<div class="col-md-12 content-overlay">
						<center>
							<br><br><br><br>
							<h2>We have classes ranging from yearly to hourly basis. From long term to daily workshops. From engineering coaching to bakery courses. From personality development to physical fitness.<br><br></h2>
							<div class="row"  style="padding-left: 25%;">
								<div class="col-md-3">
									<center>
										<h2><span id="teacher">0</span><br>Teachers</h2>
									</center>
								</div>
								<div class="col-md-3">
									<center>
										<h2><span id="institution">0</span><br>Institutions</h2>
									</center>
								</div>
								<div class="col-md-3">
									<center>
										<h2><span id="student">0</span><br>Students</h2>
									</center>
								</div>
							</div>
							<br><br><br><br>
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section id="section-2">
	<div class="container" id="features1">
		<div class="row features-list">
			<center>
				<h2>Why Pedagoge for Learning?</h2>
			</center>
			<div class="col-md-6 col-sm-3 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
				<div class="feature-icon">
					<i class="fa fa-users"></i>
				</div>
				<center>
					<h2>Building a Community of Learners</h2>
					<p>Nothing is more powerful than being informed, take informed decisions when making choices about your future. Peruse through our reviews while choosing where to learn.</p>
				</center>
			</div>
			<div class="col-md-6 col-sm-3 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">
				<div class="feature-icon">
					<i class="fa fa-info"></i>
				</div>
				<center>
					<h2>Know Your Teacher</h2>
					<p>Find a teacher/coach matching your criteria and check the teacher's profile before making any monetary transaction. Only pay for a learning experience worth your expectations.</p>
				</center>
			</div>
		</div>
		<div class="row features-list">
			<div class="col-md-6 col-sm-3 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
				<div class="feature-icon">
					<i class="fa fa-shield"></i>
				</div>
				<center>
					<h2>Trusted Network</h2>
					<p>We do the hard work and build a trusted and verified network of teachers for you. We constantly thrive to improve our listings and learn from student needs, so that you can have a hassle free learning experience.</p>
				</center>
			</div>
			<div class="col-md-6 col-sm-3  wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
				<div class="feature-icon">
					<i class="fa fa-briefcase"></i>
				</div>
				<center>
					<h2>Dedicated Customer Service</h2>
					<p>We're here for you before, during, and after your experience. Feel free to reach out to us at any time. We'll be happy to talk about any concerns or feedback you have. Or just drop in a word to tell us how you think we are doing.</p>
				</center>
			</div>
		</div>
	</div>
</section>
<!-- <div class=" background-image-centered" style="color:inherit; background-color:#20ac94; background-image: url(http://www.carbonite.com/globalassets/images-breakers/sphere-center1.png);"> -->
<div class=" background-image-centered parallax-window1">
	<div class="container-fluid gray-headline-text-block">
		<div class="container">
			<div class="one-column">
				<div class="row">
					<div class="col-md-12 content-overlay">
						<center>
							<br><br><br><br>
							<h2>Why shoot in the blind when you can make informed choices on Pedagoge with our verified listing of teachers.<br><br></h2>
							<div class="row"  style="padding-left: 25%;">
								<div class="col-md-3">
									<center>
										<h2><span id="totalsubjects">0</span><br>Courses<br>Offered</h2>
									</center>
								</div>
								<div class="col-md-3">
									<center>
										<h2><span id="coachingexperience">0</span><br>Teaching<br>Experience</h2>
									</center>
								</div>
								<div class="col-md-3">
									<center>
										<h2><span id="localitiescovered">0</span><br>Localities<br>Covered</h2>
									</center>
								</div>
							</div>
							<br><br><br><br>
						</center>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'template-parts/pedagoge/content', 'contact_form' ); ?>