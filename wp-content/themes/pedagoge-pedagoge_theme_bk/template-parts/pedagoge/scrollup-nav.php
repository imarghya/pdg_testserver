<div class="scrollup_nav">	
	<a href="<?php echo esc_url(home_url('/teachers')); ?>"><button type="button" class="btn btn-success btn-rounded btn wow fadeInUp mobileview"  data-wow-duration="1s" data-wow-offset="10">For Teachers</button></a>
	<a href="<?php echo esc_url(home_url('/#features1')); ?>"><button type="button" class="btn btn-success btn-rounded btn wow fadeInUp mobileview"  data-wow-duration="1.5s" data-wow-offset="10">For Students</button></a>
	<button type="button" class="btn btn-success btn-rounded btn wow fadeInUp "  data-toggle="modal" data-target=".bs-example-modal-sm" data-wow-duration="2s" data-wow-offset="10">Log In</button>
    <button type="button" class="btn btn-success btn-rounded btn wow fadeInUp"  data-toggle="modal" data-target=".bs-example-modal-sm1"   data-wow-duration="0.5s" data-wow-offset="10">Sign Up</button>		
</div>
