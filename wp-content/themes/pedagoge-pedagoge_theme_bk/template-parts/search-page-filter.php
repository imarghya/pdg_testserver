<!-- Filter Menu Toggle -->
<!-- Toggles filter menu on small screens -->
<a href="javascript:void(0)" class="btn btn-default mob-filter-menu-toggle visible-xs visible-sm" data-toggle="collapse" data-target="#mob-filter">
	<i class="fa fa-filter" aria-hidden="true"></i> Filter
</a>
<!-- END filter Menu Toggle -->