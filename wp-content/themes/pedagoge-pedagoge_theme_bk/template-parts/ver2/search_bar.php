<div class="isMobileTablet visible-xs visible-sm"></div>
<div class="isMobile visible-xs"></div>
<div class="isTablet visible-sm"></div>
<div class="container-fluid search non-scrollable-fixed-search control-group clearfix hide_on_search">
	<div class="row">
		<div class="sm-container-fluid">
			<div class="col-xs-12 col-sm-5 white xs-search-padding-adjust">
				<select id="search_subject" class="form-control"></select>
			</div>
			<div class="col-xs-12 visible-xs">
			</div>
			<div class="col-xs-12 col-sm-5 make-border white xs-search-padding-adjust">
				<select id="search-locality" class="form-control"></select>
			</div>
			<div class="col-xs-12 visible-xs">
			</div>
			<div class="">
				<a href="javascript:void(0)"
				   class="btn btn-raised btn-warning search_btn noroundcorner"><i
						class="material-icons">&#xE8B6;</i></a>
			</div>
		</div>
	</div>
</div>