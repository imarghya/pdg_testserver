<!--Modal request callback-->

<div id="request-teacher-options-modal" data-callback-name="home_callback" class="callback_init modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tell us about your requirements</h4>
			</div>
			<form id="home_callback" class="bs-component">
				<div class="modal-body">
					<div class="col-xs-12">
						<div class="show_result"></div>
						<h5>Contact us:<strong>+91-90739 22835</strong> <strong>+91-90739 22837</strong></h5>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_sub">Tuition Required For*</label>
							<input class="form-control validate[required]" id="callback_sub" type="text"
							       autocomplete="off">
							<span class="help-block">Enter subjects and separate using commas</span>
						</div>
						<br />
						<div class="form-group label-floating">
							<label class="control-label" for="callback_tut_loc">Tuition location*</label>
							<select id="callback_tut_loc" class="form-control validate[required]">
								<option value="teacher">Teacher's Place</option>
								<option value="student">Student's Place</option>
								<option value="institute">Institute</option>
							</select>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_loc">Locality*</label>
							<input class="form-control validate[required]" id="callback_loc" type="text"
							       autocomplete="off">
							<span class="help-block">Enter multiple localities using commas</span>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_name">Name*</label>
							<input class="form-control validate[required]" id="callback_name" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_phn">Phone Number*</label>
							<input class="form-control validate[required, custom[phone]]" id="callback_phn" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_email">Email ID</label>
							<input class="form-control validate[custom[email]]" id="callback_email" type="text"
							       autocomplete="off">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary text-bold submit" data-loading-text="Processing...">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--/Modal request callback-->