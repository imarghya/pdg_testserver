<?php
/*
Template Name: Careers - Finance & Operations Associate
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">
							&nbsp;<i class="fa fa-circle" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Finance & Operations Associate
						</div>
						<div class="opPara1 jobDescPara">
							<p>Pedagoge is looking for a finance and operations associate with a knack to learn and grow
								with a startup. An enthusiastic professional having excellent communication skills and
								well versed with Indian Accounting Practices and Policies is preferred.</p>

							<p><strong><u>Responsibilities:</u></strong></p>

							<ul>
								<li>Day to day accounting of transactions.</li>
								<li>Book Keeping/Auditing.</li>
								<li>Preparation and presentation of Financial Statements, Cash Flow, BRS and the
									likes.
								</li>
								<li>Computation and return filings: TDS, P.Tax, Service Tax, etc.</li>
								<li>Guiding employees regarding compliance with tax laws.</li>
								<li>Should be able to do proper Financial and Tax research.</li>
								<li>Dealing with banks and clients involving finances of the company.</li>
								<li>Making calls to clients to collect online reviews.</li>
								<li>Maintaining and managing good Customer Relations.</li>
								<li>Should be able to convince people over calls.</li>
							</ul>

							<p><strong><u>Requirements:</u></strong></p>

							<ul>
								<li>Graduate from any reputed college with finance background.</li>
								<li>Preference will be given to candidates who have cleared CA IPCC.</li>
								<li>Should be well versed with Microsoft Office.</li>
								<li>Should have knowledge about Indian Tax Laws (Direct &amp; Indirect).</li>
								<li>Should have good communication skills.</li>
								<li>Must have a good command over English, Hindi and Bengali.</li>
								<li>Inclination to learn new concept and adapt to newer ideas.</li>
								<li>Eagerness to work in a team.</li>
								<li>Willingness to work overtime hand hustle. Pro-activeness is a must</li>
								<li>Ability to handle responsibilities and meet deadlines</li>
							</ul>

							<p><strong><u>What can the candidate expect:</u></strong></p>

							<ul>
								<li>Working with a Nasscom 10,000 startup in the EduTech industry</li>
								<li>Ownership of work and freedom to experiment new developing concepts</li>
								<li>Directly reporting to the CEO and/or CRM Head.</li>
								<li>Working with Sales team to create better relationships with end consumers.</li>
							</ul>

							<p><strong>Compensation:</strong> Competitive salary and performance based incentives</p>

							<p><strong>Location:&nbsp;</strong>Kolkata</p>

						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor, Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata -
								700091,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home_ver2' );