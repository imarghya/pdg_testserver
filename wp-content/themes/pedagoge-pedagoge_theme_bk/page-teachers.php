<?php
/*
Template Name: For Teachers Page
*/
/**
 * Had to create different header, footer, and page templates because of spaghetti code
 * Will clean the code and create uniform structure (Header/Footer/Pages) after new design comes in.
 */
get_header('home');
$theme_dir = get_template_directory_uri(); ?>
	<section class="site-section site-section-light site-section-top parallax-image">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="text-center animation-slideDown text-black hidden-xs heading-forteachers">
						<strong>AN EASY ACCESS TO HUNDREDS OF STUDENTS</strong>
					</h3>
					<h4 class="text-center animation-slideDown text-black visible-xs heading-forteachers font-90">
						<strong>AN EASY ACCESS TO HUNDREDS OF STUDENTS</strong>
					</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="bg-handsup">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/hands.png" width="50%"
						     height="auto"/><img
							src="<?= $theme_dir; ?>/assets/img/forteachers/hands.png" width="50%"
							height="auto"/>
						<hr class="hr-handsup">
					</div>
				</div>
			</div>
		</div>
		<br>
	</section>
	<section class="section2-forteachers">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 col-sm-4 text-center">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/online-pressence.png"
						     width="auto" height="100px" align="center"/>
						<p>FREE ONLINE PRESENCE</p>
					</div>
					<div class="col-xs-12 col-sm-4 text-center">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/registration.png" width="auto"
						     height="100px" align="center"/>
						<p>EASY REGISTRATION</p>
					</div>
					<div class="col-xs-12 col-sm-4 text-center">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/pay.png" width="auto" height="100px"
						     align="center"/>
						<p>PAY ONLY WHEN YOU RECEIVE STUDENTS</p>
					</div>
				</div>
			</div>
		</div>
		<br>
	</section>
	<section class="section3-forteachers">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2>HOW IT WORKS?</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 col-sm-3 text-center ">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/signup.png"
						     width="auto" height="100px" align="center"/>
						<p class="text-bold">SIGNUP</p>
						<p class="blocktext-forteachers">Create a free profile for yourself and establish an online
							presence.</p>
					</div>
					<div class="col-xs-12 col-sm-3 text-center ">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/respond.png" width="auto"
						     height="100px" align="center"/>
						<p class="text-bold">RESPOND TO QUERY</p>
						<p class="blocktext-forteachers">As the requirements arise, respond with your availability.</p>
					</div>
					<div class="col-xs-12 col-sm-3 text-center ">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/schedule.png" width="auto" height="100px"
						     align="center"/>
						<p class="text-bold">SCHEDULE</p>
						<p class="blocktext-forteachers">Interact with the students and schedule your classes.</p>
					</div>
					<div class="col-xs-12 col-sm-3 text-center ">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/teach.png" width="auto" height="100px"
						     align="center"/>
						<p class="text-bold">TAKE CLASSES</p>
						<p class="blocktext-forteachers">You're all set.</br>Let the learning begin.</p>
					</div>
				</div>
			</div>
		</div>
		<br>
	</section>
	<section class="section4-forteachers">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="hearfromus-forteachers">Hear what the teachers have to say about us!</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="hidden-xs col-sm-2 col-md-3">
					</div>
					<div class="col-xs-12 col-sm-8 col-md-6">
						<div class="carousel slide carousal-forteachers" data-ride="carousel">
							<div class="carousel-inner slider-forteachers">
								<div class="item active adjust-height text-center">
									<img src="<?= $theme_dir; ?>/assets/img/forteachers/anand.jpg"
									     width="auto" height="120px" align="center"
									     class="circular-testimony-forteachers"/>
									<p class="teachername-forteachers">Anand Nopany</p>
									<q>
										<p class="closeq">Pedagoge is a suitable platform for me to get students. The online presence they offer is extremely helpful to reach out to more students. Moreover, the team has been extremely cooperative and supportive of all my needs.</p>
									</q>
								</div>
								<div class="item adjust-height text-center">
									<img src="<?= $theme_dir; ?>/assets/img/forteachers/garima.jpg"
									     width="auto" height="120px" align="center"
									     class="circular-testimony-forteachers"/>
									<p class="teachername-forteachers">Garima Jhawar</p>
									<q>
										<p class="closeq">Working with Pedagoge has been a wonderful experience. The team is helpful and supportive. Now, I can cater to students all over the city. This has been extremely helpful.</p>
									</q>
								</div>
								<div class="item adjust-height text-center">
									<img src="<?= $theme_dir; ?>/assets/img/forteachers/manju.jpg"
									     width="auto" height="120px" align="center"
									     class="circular-testimony-forteachers"/>
									<p class="teachername-forteachers">Manju Rathi</p>
									<q>
										<p class="closeq">I am very pleased to be registered with Pedagoge. It’s a dream come true for me and a wonderful platform to be a part of. It allows me to get recognition and increase my reach to students.</p>
									</q>
								</div>
							</div>
						</div>
					</div>
					<div class="hidden-xs col-sm-2 col-md-3">
					</div>
				</div>
			</div>
		</div>
		<br>
	</section>
	<section class="section5-forteachers">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-sm-6 text-right hidden-xs">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/teacher.png"
						     width="auto" height="120px" align="center" class="clickable clickable-teachers"/>
						<p class="teachername-forteachers">Register as Teacher</p>
					</div>
					<div class="col-sm-6 text-left hidden-xs">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/institution.png"
						     width="auto" height="120px" align="center" class="clickable clickable-institution"/>
						<p class="teachername-forteachers">Register as Institution</p>
					</div>
					<div class="col-xs-12 text-center visible-xs">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/teacher.png"
						     width="auto" height="120px" align="center" class="clickable clickable-teachers"/>
						<p class="teachername-forteachers">Register as Teacher</p>
					</div>
					<div class="col-xs-6 text-left visible-xs divider-section5-forteachers">
					</div>
					<div class="col-xs-12 text-center visible-xs">
						<img src="<?= $theme_dir; ?>/assets/img/forteachers/institution.png"
						     width="auto" height="120px" align="center" class="clickable clickable-institution"/>
						<p class="teachername-forteachers">Register as Institution</p>
					</div>
				</div>
			</div>
		</div>
		<br>
	</section>
	<section class="section6-forteachers">
		<div class="container">
			<div class="row text-left">
				<div class="hidden-xs col-sm-2 col-md-3">
				</div>
				<div class="col-xs-12 col-sm-8 col-md-6">
					<div id="div_signup_result"></div>
					<form class="m-t-md" name="registration_form" id="registration_form">
						<div id="scrollToHere-forteachers"></div>
						
						<div class="form-group select_user_role">
							<select type="role" name="select_user_role" class="form-control m-b-sm select_user_role_option">
							</select>
							<input type="hidden" name="hidden_register_user_role" value="" id="hidden_register_user_role"/>
						</div>
						
						<div class="form-group">							
							<input type="text" name="txt_register_full_name" id="txt_register_full_name" class="form-control validate[required] txt_register_full_name" placeholder="Your Full Name" required>
						</div>
						
						<div class="form-group">
							<input type="email" name="txt_register_email_address" id="txt_register_email_address" class="form-control validate[required, custom[email]] completer" placeholder="Email" required>
						</div>
						
						<div class="form-group">							
							<input type="text" maxlength="10" name="txt_register_mobile_number" id="txt_register_mobile_number" class="form-control validate[required] positive" placeholder="10 Digits Mobile Number" required>
						</div>

						<div class="form-group">
							<input type="password" name="txt_register_password" id="txt_register_password" class="form-control validate[required]" placeholder="Password" required>
						</div>
						
						<p><input type="checkbox" class="chk_register_show_password" value="no"> Show Password</p>
						
						<p class="blocktext-forteachers">By clicking Register, 
							you are agreeing to the <a href="<?php echo home_url('/terms'); ?>">terms and conditions</a> 
							and the <a href="<?php echo home_url('/privacy-policy'); ?>">privacy policy</a> set by Pedagoge.
						</p>
						
						<input type="button" id="cmd_register_user" class="btn btn-success btn-block m-t-xs" value="Register"/><br /><br />
						
						<a href="<?php echo home_url('/login'); ?>/" title="Pedagoge Login" id="cmd_show_login_form" class="btn btn-info btn-block m-t-xs">Already have an account? Login</a>
						   
					</form>
				</div>
				<div class="hidden-xs col-sm-2 col-md-3">
				</div>
			</div>
		</div>
		<br>
	</section>


<?php
get_footer('home');