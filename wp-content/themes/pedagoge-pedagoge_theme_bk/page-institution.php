<?php
/*
Template Name: Institutions Page
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
?>
	<main class="padding-fix no-select">
		<!-- scroll to top -->
		<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
		<!--/scroll to top -->
		<!-- scroll to bottom -->
		<a href="javascript:void(0);" class="scroll-to-bottom"><i class="material-icons">&#xE313;</i></a>
		<!--/scroll to bottom -->
		<div class="isMobileTablet visible-xs visible-sm"></div>
		<div class="isMobile visible-xs"></div>
		<div class="isTablet visible-sm"></div>

		<!--section-top-->
		<section class="section-top padding-fix background-color">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8">
						<div class="main-heading-wrapper">
							<div class="white-text text-bold">
								<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/heading-text.svg"
								     title="DISCOVER NEW LEARNING EXPERIENCES" alt="DISCOVER NEW LEARNING EXPERIENCES"
								     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/institution/heading-text.png';this.className='responsive-png'"/>
							</div>
						</div>
					</div>
					<div class="col-md-4 hidden-xs hidden-sm">
						<div class="post-req-wrapper callback_init background-color z-depth-2 padding-fix" data-callback-name="institution_callback_form">
							<form class="institution_callback_form contact_form" id="institution_callback_form">
								<div class="show_result center-block"></div>
								<div class="black-text text-bold light-blue-shade-2 item-title text-center">
									Post Your Trainer/Workshop Requirement here
								</div>
								<div class="form-group label-floating">
									<label for="fullname" class="control-label">Full Name*</label>
									<input type="text" class="form-control validate[required]" id="fullname">
								</div>
								<div class="form-group label-floating">
									<label for="inst_school_name" class="control-label">Institute/School Name*</label>
									<input type="text" class="form-control validate[required]" id="inst_school_name">
								</div>
								<div class="form-group label-floating">
									<label for="designation" class="control-label">Designation</label>
									<input type="text" class="form-control" id="designation">
								</div>
								<div class="form-group label-floating">
									<label for="phoneno" class="control-label">Phone Number*</label>
									<input type="text" class="form-control validate[required, custom[phone]]" id="phoneno">
								</div>
								<div class="form-group label-floating">
									<label for="emailid" class="control-label">Email ID</label>
									<input type="text" class="form-control validate[custom[email]]" id="emailid">
								</div>
								<div class="form-group label-floating">
									<button type="button" id="post_req" class="contact_submit btn btn-raised white-text text-bold green-shade-2 btn-lg btn-block post_req" data-loading-text="Please wait ...">
										POST REQUIREMENT
									</button>
								</div>
								<div class="form-group label-floating">
									<a href="#" class="register_as_trainer black-text text-bold btn-lg btn-block text-center" data-toggle="modal" data-target="#register-as-trainer-modal">
										Register me as a trainer </a>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="row margin-top-20">
					<div class="col-xs-12">
						<div class="content-wrapper center-block text-center">
							<h2 class="white-text text-bold">ABOUT PEDAGOGE FOR INSTITUTIONS</h2>
							<p class="white-text item-desc">Pedagoge for Institution is a service where schools and
								institutions partner with Pedagoge with the objective to hire experienced and specialist
								teachers and conduct extra-curricular workshops to offer better services to their
								pupils. We partner with multi-domain professional coaches and teachers who are engaged
								and deployed in the institutions to conduct workshops, regular classes and improve
								performance.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/section-top-->

		<!--section-why-work-withus-->
		<section class="section-why-work-withus background-color">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 text-center center-block">
						<h2 class="white-text text-bold">WHY WORK WITH US?</h2>
					</div>
				</div>
				<div class="row item-wrapper padding-fix">
					<div class="col-xs-6 col-md-2 col-md-offset-1">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Increased-Scope.svg" title="Increased Scope" alt="Increased Scope"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Increased-Scope.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-bold text-center">
								Increased<br/>Scope
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-md-2">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Detailed-Info.svg"
							     title="Detailed Info" alt="Detailed Info"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Detailed-Info.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-bold text-center">
								Detailed<br/>Info
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-md-2">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Basket-Of-Teachers.svg"
							     title="Basket Of Teachers" alt="Basket Of Teachers"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Basket-Of-Teachers.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-bold text-center">
								Basket of<br/>Teachers
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-md-2">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/MultiLevel-Verification.svg"
							     title="MultiLevel Verification" alt="MultiLevel Verification"
							     onerror="this.onerror=null;this.src= '<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/MultiLevel-Verification.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-bold text-center">
								MultiLevel<br/>Verification
							</p>
						</div>
					</div>
					<div class="col-xs-12 col-md-2">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Alliance-Team.svg"
							     title="Dedicated Alliance Team" alt="Dedicated Alliance Team"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhyWorkWithUs/Alliance-Team.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-bold text-center">
								Dedicated<br/>Alliance Team
							</p>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!--/section-why-work-withus-->

		<!--section-what-we-do-->
		<section class="section-what-we-do background-color margin-top-20">
			<div class="container-fluid">
				<div class="row margin-bottom-20">
					<div class="col-xs-12 text-center center-block">
						<h2 class="white-text text-bold">WHAT WE DO?</h2>
					</div>
				</div>
				<div class="row item-wrapper padding-fix">
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhatWeDo/Teacher-Recruitment.svg"
							     title="Teacher Recruitment" alt="Teacher Recruitment"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhatWeDo/Teacher-Recruitment.png';this.className='responsive-png'"/>
							<p class="item-desc text-bold text-center">
								Teacher<br/>Recruitment
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhatWeDo/Teacher-Sourcing.svg"
							     title="Teacher Sourcing" alt="Teacher Sourcing"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhatWeDo/Teacher-Sourcing.png';this.className='responsive-png'"/>
							<p class="item-desc text-bold text-center">
								Teacher<br/>Sourcing
							</p>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhatWeDo/Workshops.svg"
							     title="Workshops" alt="Workshops"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/WhatWeDo/Workshops.png';this.className='responsive-png'"/>
							<p class="item-desc text-bold text-center">
								Workshops<br/>&nbsp;
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/section-what-we-do-->

		<!--section-modules-->
		<section class="section-modules background-color margin-top-20">
			<div class="container-fluid">
				<div class="row margin-bottom-20">
					<div class="col-xs-12 text-center center-block">
						<h2 class="white-text text-bold">MODULES</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-12 modules-panel-wrapper">
						<div class="modules-panel-sub-wrapper">
							<!--modules-->
							<form id="modules-panel-form" class="modules-panel">
								<div id="modules-panel-encloser" class="modules-panel-encloser" role="tablist" aria-multiselectable="true">
									<div class="panel-group" id="modules-panel-accordion">
										<!--item-->
										<div class="panel panel-module">
											<div class="panel-heading border-bottom-fix" role="button" data-toggle="collapse" data-parent="#modules-panel-accordion" href="#content-1" aria-expanded="true" aria-controls="content-1" id="heading-1">
												<h4 class="panel-title text-bold">
													<a class="accordion-toggle collapsed truncate no-select text-bold" data-toggle="collapse" data-parent="#modules-panel-accordion" href="#content-1" aria-expanded="true" aria-controls="content-1">HEALTH
														& FITNESS</a>
												</h4>
											</div>
											<div id="content-1" class="panel-collapse collapse panel-body mathematics" role="tabpanel" aria-labelledby="heading-1">
												<div class="form-group nomargin-top-10">
													<div class="row">
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Self Defence</h5>
															<?= is_array( get_post_custom_values( "institution_self_defence" ) ) ? get_post_custom_values( "institution_self_defence" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Yoga</h5>
															<?= is_array( get_post_custom_values( "institution_yoga" ) ) ? get_post_custom_values( "institution_yoga" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Food & Nutrition</h5>
															<?= is_array( get_post_custom_values( "institution_food_nutrition" ) ) ? get_post_custom_values( "institution_food_nutrition" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Preparation</h5>
															<?= is_array( get_post_custom_values( "institution_marathon_preparation" ) ) ? get_post_custom_values( "institution_marathon_preparation" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
													</div>

												</div>
											</div>
										</div>
										<!--/item-->
										<!--item-->
										<div class="panel panel-module">
											<div class="panel-heading border-bottom-fix" role="button" data-toggle="collapse" data-parent="#modules-panel-accordion" href="#content-2" aria-expanded="true" aria-controls="content-2" id="heading-2">
												<h4 class="panel-title text-bold">
													<a class="accordion-toggle collapsed truncate no-select text-bold" data-toggle="collapse" data-parent="#modules-panel-accordion" href="#content-2" aria-expanded="true" aria-controls="content-2">SKILL
														DEVELOPMENT</a>
												</h4>
											</div>
											<div id="content-2" class="panel-collapse collapse panel-body mathematics" role="tabpanel" aria-labelledby="heading-2">
												<div class="form-group nomargin-top-10">
													<div class="row">
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Coding</h5>
															<?= is_array( get_post_custom_values( "institution_coding" ) ) ? get_post_custom_values( "institution_coding" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Bakery</h5>
															<?= is_array( get_post_custom_values( "institution_bakery" ) ) ? get_post_custom_values( "institution_bakery" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Musical</h5>
															<?= is_array( get_post_custom_values( "institution_musical_instruments" ) ) ? get_post_custom_values( "institution_musical_instruments" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Dance Forms</h5>
															<?= is_array( get_post_custom_values( "institution_dancing" ) ) ? get_post_custom_values( "institution_dancing" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Design & Graphics</h5>
															<?= is_array( get_post_custom_values( "institution_design_graphics" ) ) ? get_post_custom_values( "institution_design_graphics" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!--/item-->
										<!--item-->
										<div class="panel panel-module">
											<div class="panel-heading border-bottom-fix" role="button" data-toggle="collapse" data-parent="#modules-panel-accordion" href="#content-3" aria-expanded="true" aria-controls="content-3" id="heading-3">
												<h4 class="panel-title text-bold">
													<a class="accordion-toggle collapsed truncate no-select text-bold" data-toggle="collapse" data-parent="#modules-panel-accordion" href="#content-3" aria-expanded="true" aria-controls="content-3">COUNSELLING</a>
												</h4>
											</div>
											<div id="content-3" class="panel-collapse collapse panel-body mathematics" role="tabpanel" aria-labelledby="heading-3">
												<div class="form-group nomargin-top-10">
													<div class="row">
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Career Related</h5>
															<?= is_array( get_post_custom_values( "institution_career_related" ) ) ? get_post_custom_values( "institution_career_related" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Psycho-metric
																Understanding</h5>
															<?= is_array( get_post_custom_values( "institution_psychometric_understanding" ) ) ? get_post_custom_values( "institution_psychometric_understanding" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Motivational Instruments</h5>
															<?= is_array( get_post_custom_values( "institution_motivational_instruments" ) ) ? get_post_custom_values( "institution_motivational_instruments" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">General Knowledge</h5>
															<?= is_array( get_post_custom_values( "institution_general_knowledge" ) ) ? get_post_custom_values( "institution_general_knowledge" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
														<div class="col-xs-12 module_item_block_wrapper white-text">
															<h5 class="text-bold nomargin">Personality Development</h5>
															<?= is_array( get_post_custom_values( "institution_personality_development" ) ) ? get_post_custom_values( "institution_personality_development" )[0] : "" ?>
															<div class="col-xs-12 border-dotted text-white"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!--/item-->
									</div>
								</div>
							</form>
							<!--/modules-->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/section-modules-->

		<!--section-activities-students-like-->
		<section class="section-activities-students-like background-color hidden-xs hidden-sm">
			<div class="container-fluid">
				<div class="row margin-bottom-20">
					<div class="col-xs-12 text-center center-block">
						<h2 class="white-text text-bold">ACTIVITIES STUDENTS LIKE</h2>
					</div>
				</div>
				<div class="row item-wrapper padding-fix">
					<div class="col-xs-12">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/ActivitiesStudentsLike/Activities-Student-Like-Chart.svg"
							     title="Activities Student Like Chart" alt="Activities Student Like Chart"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/ActivitiesStudentsLike/Activities-Student-Like-Chart.png';this.className='responsive-png'"/>
						</div>
					</div>
				</div>
				<div class="row item-wrapper padding-fix margin-top-50">
					<div class="col-xs-6 no-padding">
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color1"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Sports<br/>&nbsp;
							</p>
						</div>
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color2"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Music &<br/>Instruments
							</p>
						</div>
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color3"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Marital<br/>Arts
							</p>
						</div>
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color4"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Foreign<br/>Language
							</p>
						</div>
					</div>
					<div class="col-xs-6 no-padding">
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color5"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Cooking<br/>Baking
							</p>
						</div>
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color6"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Dancing<br/>&nbsp;
							</p>
						</div>
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color7"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Theater &<br/>Acting
							</p>
						</div>
						<div class="col-xs-3 content-wrapper center-block text-center">
							<div class="circle-item center-block color8"></div>
							<p class="item-desc white-text text-center margin-top-10">
								Acts &<br/>Crafts
							</p>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!--/section-what-we-do-->

		<!--section-students-speak-->
		<section class="section-students-speak background-color hidden-xs hidden-sm">
			<div class="container-fluid">
				<div class="row margin-bottom-20">
					<div class="col-xs-12 text-center center-block">
						<h2 class="white-text text-bold">STUDENT'S SPEAK</h2>
					</div>
				</div>
				<div class="row item-wrapper padding-fix">
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS1.svg?ver=0.2"
							     title="SS1" alt="SS1"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS1.png?ver=0.2';this.className='responsive-png'"/>
							<p class="white-text item-desc text-center margin-top-10">
								76% Students<br/> want to pursue<br/> co-curricular activities<br/>&nbsp;<br/>
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS2.svg"
							     title="SS2" alt="SS2"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS2.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-center margin-top-10">
								3 Most opted activities<br/> - Sports (58%)<br/> - Music G. Instruments (27%)<br/> -
								Foreign Language (24%)<br/>

							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS3.svg"
							     title="SS3" alt="SS3"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS3.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-center margin-top-10">
								Only 37% are able<br/>to pursue their interested<br/> activity currently<br/>
								&nbsp;<br/>
							</p>
						</div>
					</div>
				</div>
				<div class="row item-wrapper padding-fix margin-top-50">
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS4.svg"
							     title="SS4" alt="SS4"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS4.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-center margin-top-10">
								63% Students<br/>showed willingness to<br/>dedicate upto 2 hours<br/>for co-curricular
								activities<br/>
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS5.svg"
							     title="SS5" alt="SS5"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS5.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-center margin-top-10">
								67% Students<br/>showed interest towards<br/> motivational workshops<br/> to help them
								in examinations
							</p>
						</div>
					</div>
					<div class="col-xs-6 col-sm-4">
						<div class="content-wrapper center-block text-center">
							<img data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS6.svg"
							     title="SS6" alt="SS6"
							     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/institution/StudentsSpeak/SS6.png';this.className='responsive-png'"/>
							<p class="white-text item-desc text-center margin-top-10">
								85% Students<br/> are engaged in<br/> academic tuitions<br/> &nbsp;<br/>
							</p>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!--/section-students-speak-->

		<!--section-xs-institution-callback-form-->
		<section class="section-xs-institution-callback-form background-color hidden-lg hidden-md">
			<div class="container-fluid">
				<div class="row item-wrapper padding-fix">
					<div class="col-md-4">
						<div class="post-req-wrapper callback_init background-color z-depth-2 padding-fix" data-callback-name="institution_callback_form-xs">
							<form class="institution_callback_form-xs contact_form" id="institution_callback_form-xs">
								<div class="show_result center-block"></div>
								<div class="black-text text-bold light-blue-shade-2 item-title text-center">
									Post Your Trainer/Workshop Requirement here
								</div>
								<div class="form-group label-floating">
									<label for="fullname-xs" class="control-label">Full Name*</label>
									<input type="text" class="form-control validate[required]" id="fullname-xs">
								</div>
								<div class="form-group label-floating">
									<label for="inst_school_name-xs" class="control-label">Institute/School
										Name*</label>
									<input type="text" class="form-control validate[required]" id="inst_school_name-xs">
								</div>
								<div class="form-group label-floating">
									<label for="designation-xs" class="control-label">Designation</label>
									<input type="text" class="form-control" id="designation-xs">
								</div>
								<div class="form-group label-floating">
									<label for="phoneno-xs" class="control-label">Phone Number*</label>
									<input type="text" class="form-control validate[required, custom[phone]]" id="phoneno-xs">
								</div>
								<div class="form-group label-floating">
									<label for="emailid-xs" class="control-label">Email ID</label>
									<input type="text" class="form-control validate[custom[email]]" id="emailid-xs">
								</div>
								<div class="form-group label-floating">
									<button type="button" id="post_req-xs" class="contact_submit btn btn-raised white-text text-bold green-shade-2 btn-lg btn-block post_req" data-loading-text="Please wait ...">
										POST REQUIREMENT
									</button>
								</div>
								<div class="form-group label-floating">
									<a href="#" class="register_as_trainer black-text text-bold btn-lg btn-block text-center" data-toggle="modal" data-target="#register-as-trainer-modal">
										Register me as a trainer </a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/section-xs-institution-callback-form-->

		<!--Modal register as a trainer-->
		<div id="register-as-trainer-modal" data-callback-name="register_as_trainer_modal_callback" class="callback_init modal fade" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Register as a trainer</h4>
					</div>
					<form id="profile_callback" class="bs-component">
						<div class="modal-body">
							<div class="col-xs-12">
								<div class="show_result"></div>
								<h5>Contact us: +91-9073922835</h5>
								<div class="form-group label-floating">
									<label class="control-label" for="callback_name-register-as-trainer">Name*</label>
									<input class="form-control validate[required]" id="callback_name-register-as-trainer" type="text"
									       autocomplete="off">
								</div>
								<div class="form-group label-floating">
									<label class="control-label" for="callback_phn-register-as-trainer">Phone
										Number*</label>
									<input class="form-control validate[required, custom[phone]]" id="callback_phn-register-as-trainer" type="text"
									       autocomplete="off">
								</div>
								<div class="form-group label-floating">
									<label class="control-label" for="callback_email-register-as-trainer">Email
										ID*</label>
									<input class="form-control validate[required, custom[email]]" id="callback_email-register-as-trainer" type="text"
									       autocomplete="off">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary text-bold submit" data-loading-text="Processing...">
								Submit
							</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!--/Modal register as a trainer-->
	</main>

<?php
get_footer( 'home_ver2' );