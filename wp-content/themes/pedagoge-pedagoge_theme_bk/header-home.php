<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */


global $post;
$slug = '';
if ( isset( $post ) ) {
	$current_post = get_post( $post );
	if ( ! empty( $current_post ) ) {
		$slug = $current_post->post_name;
	}
}
$is_production_environment = false;
$server_name = $_SERVER['SERVER_NAME'];
switch ( $server_name ) {
	case 'www.pedagoge.com':
	//case 'www.pedagog.in':
	//case 'www.pedagoge.net':
	case 'dev.pedagoge.com':
	case 'test.pedagoge.com':
	$is_production_environment = true;
		break;
}
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>
<html class="no-js"> <![endif]-->
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		
		<?php get_template_part( 'template-parts/google_conversion_10052017'); ?>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Select2 css: It will be removed once tested -->
		<!-- Select2 css ends -->
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<meta name="description" content="Homepage of Pedagoge.">
		<meta name="author" content="pedagoge">
		<link rel="shortcut icon"
		      href="<?php echo get_template_directory_uri(); ?>/assets/img/fav_icon_logo_orb_small.png">
		<?php
		wp_head();
		if ( is_front_page() ) {
			get_template_part( 'template-parts/homepage', 'css' );
		}
		?>		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<?php get_template_part( 'template-parts/hot', 'jar' );
		get_template_part( 'template-parts/fb', 'pixel' ); ?>
	</head>


<body <?php body_class();
echo pedagoge_body_tags(); ?>>
<div id="page-container"> <!-- Div page-container starts here -->
<?php
$excludeheader = array ( 'findtherightteacher', 'movies', 'movie' , 'placements-technoindia' );
if ( ! in_array( $slug, $excludeheader ) ) {
	get_template_part( 'template-parts/main', 'navigation' );
}
?>