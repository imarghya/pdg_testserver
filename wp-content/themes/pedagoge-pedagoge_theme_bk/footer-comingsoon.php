<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */

	wp_footer(); ?>
	<script type="text/javascript">
			$(document).ready(function () {
		        $('.change-background').backstretch([
		            "<?php echo get_template_directory_uri(); ?>/assets/img/Slider_Images/School_Girl_small.jpg",
		            "<?php echo get_template_directory_uri(); ?>/assets/img/Slider_Images/Art_small.jpg",
		            "<?php echo get_template_directory_uri(); ?>/assets/img/Slider_Images/Music.jpg",
		            "<?php echo get_template_directory_uri(); ?>/assets/img/Slider_Images/Sport.jpg"
		        ], {duration: 3000, fade: 750});
		    });
		    
		</script>	
	</body>
</html>
