var currentBackground = 0;
			
var backgrounds = [];
var images = [];

$(document).ready(function() 
{
	$("#login_form").validationEngine({scroll: false});
	
	$("#mail_send").validationEngine({scroll: false});
	
	$("#email").keyup(function(){
		//alert('hi');
		if ($("#email").val()!="") {
			//alert('bapan');
			if (!$("#email").hasClass("validate[[required], custom[email]]")) {
				$("#email").removeClass("validate[required]");
				$("#email").addClass("validate[[required], custom[email]]");
			}
		} else {
			if ($("#email").hasClass("validate[[required], custom[email]]")) {
				$("#email").removeClass("validate[[required], custom[email]]");
				$("#email").addClass("validate[required]");
			}
		}
	});
	
	$("#email_send").keyup(function(){
		//alert('hi');
		if ($("#email_send").val()!="")
		{
			//alert('bapan');
			if (!$("#email_send").hasClass("validate[[required], custom[email]]")) {
				$("#email_send").removeClass("validate[required]");
				$("#email_send").addClass("validate[[required], custom[email]]");
			}
		}
		else
		{
			if ($("#email_send").hasClass("validate[[required], custom[email]]")) {
				$("#email_send").removeClass("validate[[required], custom[email]]");
				$("#email_send").addClass("validate[required]");
			}
		}
	});
	
	$('#showpassword').change(function() {
		if($(this).is(":checked")) {
			$("#password").prop('type', 'text'); 
		} else {
			$("#password").prop('type', 'password');
		}       
	});
			
	$('.showpassword').change(function() {
		if($(this).is(":checked")) {
			$("#password1").prop('type', 'text'); 
		} else {
			$("#password1").prop('type', 'password');
		}
	});
	
	prefetchSliderImages(["assets/images/homepage/SchoolGirl.jpg", "assets/images/homepage/Fitness.jpg", "assets/images/homepage/Music.jpg", "assets/images/homepage/Art.jpg", "assets/images/homepage/Sport.jpg"]);
	var backgrounds = images[0].src;
	$('#home').css({
		'background-image': "url('" + backgrounds + "')",
		'background-position': "90% 50%"
	});

	setTimeout(changeBackground, 10000);
	
	$('.parallax-window2').parallax({
		imageSrc: 'assets/images/homepage/SchoolGirl.jpg'
	});
	$('.parallax-window1').parallax({
		imageSrc: 'assets/images/homepage/Fitness.jpg'
	});
	
});

function send_mail() {
	if($("#mail_send").validationEngine('validate', {scroll: false})){
		var name = $('#name').val();
		var email1 = $('#email_send').val();
		var subject_mail = $('#subject_mail').val();
		var message1 = $('#message').val();
		var pedagoge = 'Pedagoge.com';
		$.ajax({
			type: 'POST',
			url: 'sendemail.php',
			data: {name : name, email1 : email1, subject_mail : subject_mail, message1 : message1, pedagoge: pedagoge},
			cache: false,
			success: function(data){
				$('.succ_msg').show();
				$('.hide_msg').hide();
				$('#name').val('');
				$('#email_send').val('');
				$('#subject_mail').val('');
				$('#message').val('');
			}
		});
	}
}

function prefetchSliderImages(sources) {
			        
	var loadedImages = 0;
	var numImages = sources.length;
	for (var i=0; i < numImages; i++) {
		images[i] = new Image();
		images[i].onload = function() {
			if (++loadedImages >= numImages) {
				
			}
		};
		images[i].src = sources[i]; //fork chrome version bug
	}
}