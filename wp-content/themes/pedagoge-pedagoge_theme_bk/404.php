<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package pedagoge_theme
 */

get_header('404');
?>

<br><br><br><br>
	<div class="container">
        <h3>&nbsp;<i class="fa fa-chevron-circle-left text-muted"></i> <a href="<?php echo home_url('/'); ?>">Go Home</a></h3>
    	<br><br><br><br>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <h1 class="animation-tossing h1"><b><i class="fa fa-exclamation-circle text-warning"></i> 404</b></h1>
                <br><br><br><br>
                <h3>Oops, we are sorry but the page you are looking for was not found..<br>But do not worry, we will have a look into it...</h3>
            </div>
        </div>
    </div>
<br><br><br><br><br><br><br><br>
</div>

<?php
get_footer('home');
