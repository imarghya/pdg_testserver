(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
			var PDG_Login = function () {
				var mainObj = this, settingsObj;
				//Settings
				mainObj.settings = function () {
					var $bodyEl = $( 'body' ),
						$isMobile = $( '.isMobile' );
					
					return {
						$bodyEl: $bodyEl,
						$isMobile: $isMobile,
					}
				};
				
				//Load initial page search results
				mainObj.init = function () {
					settingsObj = mainObj.settings();
					mainObj.bindUIActions();
				};
				
				mainObj.bindUIActions = function () {
					resizeHandler();
					buttonHandler();
				};
				
				//offset controls for the UI
				var resizeHandler = function () {
						$( window ).resize( function () {
							
						} ).resize();
					},
					buttonHandler = function () {
					
					};
				
			}, loginObj;
			loginObj = new PDG_Login;
			loginObj.init();
		}
	);
} );
