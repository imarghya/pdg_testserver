(function () {
	/*Fix the rendering bug in Internet Explorer 10 in Windows 8 and Windows Phone 8*/
	if ( "-ms-user-select" in document.documentElement.style && navigator.userAgent.match( /IEMobile\/10\.0/ ) ) {
		var msViewportStyle = document.createElement( "style" );
		msViewportStyle.appendChild( document.createTextNode( "@-ms-viewport{width:auto!important}" )
		);
		document.getElementsByTagName( "head" )[0].appendChild( msViewportStyle );
	}
	
	var navU = navigator.userAgent,
		/*Fix the select rendering bug in android stock browser*/
		// Android Mobile
		isAndroidMobile = navU.indexOf( 'Android' ) > - 1 && navU.indexOf( 'Mozilla/5.0' ) > - 1 && navU.indexOf( 'AppleWebKit' ) > - 1,
		
		// Apple webkit
		regExAppleWebKit = new RegExp( /AppleWebKit\/([\d.]+)/ ),
		resultAppleWebKitRegEx = regExAppleWebKit.exec( navU ),
		appleWebKitVersion = (resultAppleWebKitRegEx === null ? null : parseFloat( regExAppleWebKit.exec( navU )[1] )),
		
		// Chrome
		regExChrome = new RegExp( /Chrome\/([\d.]+)/ ),
		resultChromeRegEx = regExChrome.exec( navU ),
		chromeVersion = (resultChromeRegEx === null ? null : parseFloat( regExChrome.exec( navU )[1] )),
		
		// Native Android Browser
		isAndroidBrowser = isAndroidMobile && (appleWebKitVersion !== null && appleWebKitVersion < 537) || (chromeVersion !== null && chromeVersion < 37);
	
	if ( isAndroidBrowser ) {
		$( 'body' ).addClass( 'android-stock-browser' );
		$( 'select.form-control' ).removeClass( 'form-control' ).css( 'width', '100%' );
	}
})();

/**
 * Description: var_dump
 * param:arg
 */
function var_dump ( arg ) {
	console.log( arg );
}

String.prototype.replace_array = function ( find, replace ) {
	var replaceString = this, regex;
	for ( var i = 0; i < find.length; i ++ ) {
		regex = new RegExp( find[i], "g" );
		replaceString = replaceString.replace( regex, replace[i] );
	}
	return replaceString;
};

var alertMessagesToasts = {
		error: function ( message, timer ) {
			timer = typeof timer === "undefined" ? 5000 : timer;
			var messageContent = $( '<span class="red-text center-text center-block">' + message + '</span>' );
			Materialize.toast( messageContent, timer );
		},
		success: function ( message, timer ) {
			timer = typeof timer === "undefined" ? 5000 : timer;
			var messageContent = $( '<span class="green-text center-text center-block">' + message + '</span>' );
			Materialize.toast( messageContent, timer );
		}, info: function ( message, timer ) {
			timer = typeof timer === "undefined" ? 5000 : timer;
			var messageContent = $( '<span class="blue-text center-text center-block">' + message + '</span>' );
			Materialize.toast( messageContent, timer );
		}, warning: function ( message, timer ) {
			timer = typeof timer === "undefined" ? 5000 : timer;
			var messageContent = $( '<span class="orange-text center-text center-block">' + message + '</span>' );
			Materialize.toast( messageContent, timer );
		}
	},
	pdg_sanitize = {
		//If no default parameter is found (in the 'value') then it allocates one from 'defaultValue'
		inputdefaultparam: function ( value, defaultValue ) {
			if ( "undefined" === typeof value || value === "" || value === null ) {
				value = defaultValue;
			}
			return value;
		},
		//if the 'value' is found to be "undefined" then return an empty string or array (according to the "isArray")
		noundefined: function ( value, isArray ) {
			value = pdg_sanitize.inputdefaultparam( value, "" );
			isArray = pdg_sanitize.inputdefaultparam( isArray, false );
			if ( value.length < 1 ) {
				if ( isArray ) {
					return [];
				}
				else {
					return "".toString();
				}
			}
			return value;
		},
		
		/**
		 * Check if input is a function
		 * @param fn
		 * @returns boolean
		 */
		isFunction: function ( fn ) {
			var getType = {};
			return fn && getType.toString.call( fn ) === '[object Function]';
		}
	},
	
	/**
	 * Strip the search parameter from the beginning and end of the string
	 * @param string
	 * @param search
	 */
	rtrim = function ( string, search ) {
		string = string.toString();
		if ( pdg_sanitize.noundefined( search ) == "" ) {
			return string.trim();
		}
		var regex = new RegExp( "^" + search + "+|" + search + "+$", "g" );
		return string.replace( regex, "" ).trim();
	},
	urlParam = {
		get: function ( param ) {
			var data = {},
				parts = window.location.href.replace( /[?&]+([^=&]+)=([^&]*)/gi,
					function ( m, key, value ) {
						data[key] = decodeURI( value );
					} );
			if ( pdg_sanitize.noundefined( param ) == "" ) {
				return data;
			}
			if ( typeof data[param] === "undefined" ) {
				return null;
			}
			return data[param];
		},
		geturlnohash: function ( url ) {
			if ( pdg_sanitize.noundefined( url ) != "" ) {
				var hash = url.split( '#' )[0];
				if ( hash ) {
					return hash;
				} else {
					return null;
				}
			}
			return window.location.href.split( '#' )[0];
		},
		gethash: function ( url ) {
			if ( pdg_sanitize.noundefined( url ) != "" ) {
				var hash = url.split( '#' )[1];
				if ( hash ) {
					return hash;
				} else {
					return null;
				}
			}
			return location.hash.replace( '#', '' );
		},
		parsehash: function ( param, url ) {
			var url_data = "";
			if ( pdg_sanitize.noundefined( url ) != "" ) {
				url_data = url;
			}
			var hash = urlParam.gethash( url_data );
			if ( pdg_sanitize.noundefined( hash ) == "" ) {
				return null;
			}
			var pieces = hash.split( "&" ),
				data = {}, i, parts;
			for ( i = 0; i < pieces.length; i ++ ) {
				parts = pieces[i].split( "=" );
				if ( parts.length < 2 ) {
					parts.push( "" );
				}
				data[decodeURIComponent( parts[0] )] = decodeURIComponent( parts[1] );
			}
			if ( pdg_sanitize.noundefined( param ) == "" ) {
				return data;
			}
			if ( typeof data[param] === "undefined" ) {
				return null;
			}
			return data[param];
		},
		removehash: function () {
			window.location.replace( "#" );
		},
		geturlpath: function () {
			return window.location.pathname;
		}
	},
	htmlHandle = {
		htmlEscape: function ( str ) {
			str = pdg_sanitize.noundefined( str );
			return str
				.replace( /&/g, '&amp;' )
				.replace( /"/g, '&quot;' )
				.replace( /'/g, '&#39;' )
				.replace( /</g, '&lt;' )
				.replace( />/g, '&gt;' );
		},
		htmlUnescape: function ( str ) {
			str = pdg_sanitize.noundefined( str );
			return str
				.replace( /&quot;/g, '"' )
				.replace( /&#39;/g, "'" )
				.replace( /&lt;/g, '<' )
				.replace( /&gt;/g, '>' )
				.replace( /&amp;/g, '&' );
		}
	},
	chipHandle = {
		showChip: function ( text, classname, data_id ) {
			var data_id_attr = "";
			if ( data_id !== "undefined" ) {
				data_id_attr = "data-locality-id='" + data_id + "'";
			}
			return '<div class="chip height-auto chip-wrapper"><i class="close material-icons right">&#xE5CD;</i> <span class="' + classname + '" ' + data_id_attr + '>' + text + '</span></div>';
		},
		closeChip: function ( e ) {
			
		}
	},
	spinnerHandle = function ( styles ) {
		var spinnerHandleObj = this;
		spinnerHandleObj.parentclass = pdg_sanitize.inputdefaultparam( styles.parentclass, ".loadingSpinner" );
		spinnerHandleObj.sizeOfSpinner = pdg_sanitize.inputdefaultparam( styles.size, "small" );
		spinnerHandleObj.positionclassOfSpinner = pdg_sanitize.inputdefaultparam( styles.positionclass, "text-center" );
		spinnerHandleObj.theme = function () {
			return '<div class="container-fluid load-spinner-wrapper"><div class="row"><div class="' + spinnerHandleObj.positionclassOfSpinner + '"><div class="preloader-wrapper preloader-style-adjust ' + spinnerHandleObj.sizeOfSpinner + ' active"><div class="spinner-layer spinner-blue"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-yellow"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div><div class="spinner-layer spinner-green"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div></div></div>';
		};
		spinnerHandleObj.show = function () {
			$( spinnerHandleObj.parentclass ).show().html( spinnerHandleObj.theme() );
		};
		spinnerHandleObj.hide = function () {
			$( spinnerHandleObj.parentclass ).hide();
		};
	},
	
	/**
	 * Function to create various types of messages such as Info/Error/Success/Warning
	 * @param $div_id
	 * @param $message
	 * @param $alert_type
	 * @param $timeout
	 */
	pdg_show_alerts = function ( $div_id, $message, $alert_type, $timeout ) {
		$alert_type = (typeof $alert_type === "undefined") ? 'info' : $alert_type;
		$timeout = (typeof $timeout === "undefined") ? null : $timeout;
		
		var $result_area = null,
			$message_desc = '',
			$classname = '',
			setTimeoutobj;
		
		if ( typeof $div_id === "string" ) {
			$result_area = jQuery( "#" + $div_id );
		} else {
			$result_area = $div_id;
		}
		$result_area.html( '' );
		
		switch ( $alert_type ) {
			case 'error':
				$classname = $alert_type;
				$message_desc = 'Error:';
				break;
			case 'info':
				$classname = $alert_type;
				$message_desc = 'Information:';
				break;
			case 'success':
				$classname = $alert_type;
				$message_desc = 'Thank You:';
				break;
			case 'warning':
				$classname = $alert_type;
				$message_desc = 'Warning:';
				break;
			default:
				$classname = '';
				$message_desc = $message;
				break;
		}
		var $output = '<div class="alert alert-block fade in custom-style-message-box custom-style-message-' + $classname + '"  style="">  <strong class="custom-style-message-desc">' + $message_desc + '</strong> <hr class="custom-style-message-hr custom-style-message-hr-' + $classname + '"/> <div class="custom-style-message-text"><li>' + $message.replace( /Error:|Errors:|Error(s):|Error!|Errors!|Error(s)!|Error!:|Errors!:|Error(s)!:/gi, '' ).trim() + '</li></div> </div>';
		$result_area.html( $output );
		
		/*if ( $timeout !== null ) {
		 setTimeoutobj = setTimeout( function () {
		 $result_area.html( "" );
		 }, $timeout );
		 }*/
		
	},
	
	//selectize event init
	selectizeHandler = function ( param ) {
		param.map( function ( obj ) {
			//prepopulate check if defined
			var selector_obj = obj.selector.selectize( obj.values );
			$selectize_obj = selector_obj[0].selectize;
			if ( "undefined" !== typeof obj.prepopulate ) {
				var $selectize_obj, search_obj, createAllowed, prepopulate_values, search_term, addOption_obj = {};
				createAllowed = obj.values.create;
				prepopulate_values = obj.prepopulate.values;
				
				if ( createAllowed && typeof prepopulate_values === "object" && prepopulate_values[0] !== "" ) {
					
					prepopulate_values.map( function ( populate_obj, index ) {
						var addOption_obj = {}, addOption_array = [];
						search_term = populate_obj.trim();
						addOption_obj[obj.values.valueField] = search_term;
						
						addOption_array.push( addOption_obj );
						$selectize_obj.addOption( addOption_array );
					} );
					
					$selectize_obj.setValue( prepopulate_values );
				}
				else if ( ! createAllowed ) {
					$selectize_obj.setValue( prepopulate_values );
				}
			}
		} );
	},
	characterCounter = {
		min: function ( $textarea_selector, counter_selector, trigger_value, trim ) {
			trim = pdg_sanitize.inputdefaultparam( trim, true );
			var len = "",
				charCount;
			if ( trim ) {
				len = $textarea_selector.val().trim().length;
			}
			else {
				len = $textarea_selector.val().length;
			}
			
			if ( len < trigger_value ) {
				charCount = trigger_value - len;
				$( counter_selector ).html( '<span class="character_counter_min"><span class="counter_info">' + charCount + '</span><span class="count_desc_info"> more character(s) required.</span></span>' );
			} else {
				charCount = len;
				$( counter_selector ).html( '<span class="character_counter_min">' + charCount + '</span> character(s)</span>' );
				
			}
		},
		max: function ( $textarea_selector, counter_selector, trigger_value, trim ) {
			trim = pdg_sanitize.inputdefaultparam( trim, true );
			var len = "",
				charCount;
			if ( trim ) {
				len = $textarea_selector.val().trim().length;
			}
			else {
				len = $textarea_selector.val().length;
			}
			
			if ( len >= trigger_value ) {
				charCount = len - trigger_value;
				$( counter_selector ).html( '<span class="character_counter_max"><span class="count_desc_error">You have exceeded the limit by </span><span class="count_error">' + charCount + '</span><span class="count_desc_error"> character(s)</span></span>' );
			} else {
				charCount = trigger_value - len;
				$( counter_selector ).html( '<span class="character_counter_max"><span class="counter_info">' + charCount + '</span><span class="count_desc_info"> character(s) left.</span></span>' );
			}
		}
	},
	isTouchDevice = function () {
		return (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
	},
	isiOS = function () {
		return /iPad|iPhone|iPod/.test( navigator.userAgent ) && ! window.MSStream;
	};

function changeURLHash ( param ) {
	if ( param === "undefined" || param == null ) {
		param = "";
	} else {
		param = "#" + param;
	}
	var scrollV, scrollH, loc = window.location;
	if ( "pushState" in history ) {
		history.pushState( "", document.title, loc.pathname + loc.search + param );
	} else {
		scrollV = document.body.scrollTop;
		scrollH = document.body.scrollLeft;
		loc.hash = param;
		document.body.scrollTop = scrollV;
		document.body.scrollLeft = scrollH;
	}
}

/*
 * Keep checking for an element to load
 * @param: selector, eg: input#id
 * @param: callback_fn, callback function to execute after element is successfully loaded
 * @param: time, time interval to keep checking for the element
 * */
function waitForElementLoad ( selector, callback_fn, time, beforeload_callback_fn ) {
	time = pdg_sanitize.inputdefaultparam( time, 500 );
	var callback = pdg_sanitize.inputdefaultparam( callback_fn, function () {
	} );
	var beforeload_callback = pdg_sanitize.inputdefaultparam( beforeload_callback_fn, function () {
	} );
	if ( ! pdg_sanitize.isFunction( callback ) ) {
		return false;
	}
	if ( document.querySelector( selector ) != null ) {
		callback();
		return selector;
	}
	else {
		setTimeout( function () {
			waitForElementLoad( selector, callback, time, beforeload_callback );
		}, time );
	}
	if ( pdg_sanitize.isFunction( beforeload_callback ) ) {
		beforeload_callback();
	}
}

/*
 * Keep checking for a variable to be defined;
 * @param: variable
 * @param: callback_fn, callback function to execute after element is successfully loaded
 * @param: allowEmpty, Allow empty string
 * @param: time, time interval to keep checking for the element
 * */
function waitForVariable ( variable, callback_fn, allowEmpty, time ) {
	time = pdg_sanitize.inputdefaultparam( time, 500 );
	allowEmpty = pdg_sanitize.inputdefaultparam( allowEmpty, false );
	
	var callback = pdg_sanitize.inputdefaultparam( callback_fn, function () {
	} );
	if ( ! pdg_sanitize.isFunction( callback ) ) {
		return false;
	}
	if ( typeof variable != "undefined" ) {
		if ( ! allowEmpty && (variable == "" || variable == null) ) {
			return false;
		}
		callback();
		return variable;
	}
	else {
		setTimeout( function () {
			waitForVariable( variable, callback, allowEmpty, time );
		}, time );
	}
}


/**
 * The ajax calls are routed through here.
 * @param param; data to be sent via ajax call
 * @param identifier; the unique identifier for each ajax call
 * @param ajaxData; class and function name;
 eg: {
						'pedagoge_callback_class': 'ControllerName',
						'pedagoge_callback_function': '_fn_ajax',
					}
 * @returns {*}
 */
function pdg_ajax_handler ( data, identifier, ajaxData ) {
	var dynamicData = {};
	dynamicData[identifier] = data;
	dynamicData['nonce'] = $ajaxnonce;
	dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
	dynamicData['pedagoge_callback_class'] = ajaxData['pedagoge_callback_class'];
	dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
	return $.ajax( {
		url: $ajax_url,
		type: "post",
		data: dynamicData
	} );
}


/**
 * Get the locality names from the ids
 */
get_locality_name_by_id = function ( localityids, stringify ) {
};

(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
		var PDG_Main = function () {
			var mainObj = this, settingsObj;
			//Settings
			mainObj.settings = function () {
				var $bodyEl = $( 'body' ),
					$slideWrapperEl = $( '.slideWrapper' ),
					$slideOuterEl = $( '.slideOuter' ),
					$kick_start_sliding = $slideOuterEl.find( '.kick-start-sliding' ),
					$isMobileEl = $( '.isMobile' ),
					$isTabletEl = $( '.isTablet' ),
					$slideRightEl = $slideOuterEl.find( '.right' ),
					$slideLeftEl = $slideOuterEl.find( '.left' ),
					slideWrapperVar = 'slideWrapper',
					scrollPosLimit = 125,
					$scroll_to_top = $( '.scroll-to-top' ),
					$scroll_to_bottom = $( '.scroll-to-bottom' ),
					$start_searchEl = $( ".start_search" ),
					$search_page_type = $( ".search-page-type" ).data( "type" );
				return {
					$bodyEl: $bodyEl,
					$isMobileEl: $isMobileEl,
					$isTabletEl: $isTabletEl,
					$slideWrapperEl: $slideWrapperEl,
					slideWrapperVar: slideWrapperVar,
					scrollPosLimit: scrollPosLimit,
					$slideOuterEl: $slideOuterEl,
					$kick_start_sliding: $kick_start_sliding,
					$slideRightEl: $slideRightEl,
					$slideLeftEl: $slideLeftEl,
					$scroll_to_top: $scroll_to_top,
					$scroll_to_bottom: $scroll_to_bottom,
					$start_searchEl: $start_searchEl,
					$search_page_type: $search_page_type,
				};
			};
			
			//Load initial page search results
			mainObj.init = function () {
				settingsObj = mainObj.settings();
				mainObj.bindUIActions();
			};
			
			mainObj.bindUIActions = function () {
				initMaterialize();
				JQueryExtend();
				resizeHandler();
				consoleLogHandler();
				readMoreHandler();
				slideHandler();
				scrollToTop();
				startSearchInit();
				nudgeSpotHandler();
				modalScrollFix();
				chipEventHandler();
			};
			
			//offset controls for the UI
			var resizeHandler = function () {
					$( window ).resize( function () {
						responsiveHiddendynamicImageLoad();
					} ).resize();
				},
				chipEventHandler = function () {
					//click event for chip close
					$( document ).on( 'click', '.chip .close', function ( e ) {
						$( e.target ).parent().remove();
					} );
				},
				JQueryExtend = function () {
					//disable items
					$.fn.extend( {
						disable: function ( state ) {
							return this.each( function () {
								var $this = $( this );
								if ( $this.is( 'input, button, textarea, select' ) ) {
									this.disabled = state;
								}
								else {
									$this.toggleClass( 'disabled', state );
								}
							} );
						}
					} );
					//On Enter
					$.fn.onEnter = function ( callback ) {
						this.keyup( function ( e ) {
							if ( e.keyCode == 13 ) {
								e.preventDefault();
								if ( typeof callback == 'function' ) {
									callback.apply( this );
								}
							}
						} );
						return this;
					};
					
				},
				consoleLogHandler = function () {
					var_dump( "Okay smartypants, if you are interested in joining the company email your resume to niraj.kumar@pedagoge.com. Also, don't forget to mention this little adventure yours!" );
				},
				readMoreHandler = function () {
					var expandable_text_var = '.expandable-text',
						$expandable_text_El = $( expandable_text_var ),
						item_body_var = 'p.item-body',
						item_shade_var = 'p.item-shade',
						item_readmore_var = 'p.item-readmore',
						data_collapsedHeight_var = 'data-collapsedHeight',
						$item_body = $expandable_text_El.find( item_body_var );
					$.each( $item_body, function () {
						var totalHeight = $( this )[0].scrollHeight,
							max_height_item_body = $( this ).css( 'max-height' ).replace( /[^0-9.]/g, "" );
						if ( totalHeight <= max_height_item_body ) {
							$( this ).find( "~ " + item_shade_var ).remove();
							$( this ).parent( expandable_text_var ).find( "~ " + item_readmore_var ).remove();
						}
					} );
					
					
					$expandable_text_El.find( "~ " + item_readmore_var + " .toggler" ).on( 'click', function () {
						var type = 'more',
							$el = $( this ),
							$parentEl = $el.parent(),
							$moreBtnEl = $parentEl.find( '.more' ),
							$lessBtnEl = $parentEl.find( '.less' ),
							$item_body = $parentEl.siblings( expandable_text_var ).find( item_body_var ),
							$item_shade = $parentEl.siblings( expandable_text_var ).find( item_shade_var ),
							totalHeight = $item_body[0].scrollHeight,
							maxHeight = 99999,
							collapsedHeight = pdg_sanitize.noundefined( $item_body.attr( data_collapsedHeight_var ) ) != "" ? $item_body.attr( data_collapsedHeight_var ) : $item_body.height();
						
						if ( $el.hasClass( 'less' ) ) {
							type = 'less';
							totalHeight = maxHeight = collapsedHeight;
						}
						
						$item_body.css( {
							"height": totalHeight,
							"max-height": maxHeight
						} ).animate( {
							"height": totalHeight
						} );
						
						switch ( type ) {
							case 'less':
								$item_shade.fadeIn();
								$lessBtnEl.hide();
								$moreBtnEl.fadeIn().show();
								break;
							case 'more':
							default:
								$item_body.attr( data_collapsedHeight_var, collapsedHeight );
								$item_shade.fadeOut();
								$moreBtnEl.hide();
								$lessBtnEl.fadeIn().removeClass( 'hide' ).show();
								break;
						}
						
						return false;
					} );
				},
				slideHandler = function ( param ) {
					if ( settingsObj.$slideWrapperEl.length <= 0 ) {
						return false;
					}
					var swipeObj = new Hammer( document.getElementsByClassName( settingsObj.slideWrapperVar )[0] );
					//Check if swipe left/right
					swipeObj.on( "panleft panright", function ( ev ) {
						if ( ev.type == 'panleft' ) {
							settingsObj.$slideRightEl.trigger( 'click' );
						}
						if ( ev.type == 'panright' ) {
							settingsObj.$slideLeftEl.trigger( 'click' );
						}
					} );
					
					sliderXScrollInit();
				},
				sliderXScrollInit = function () {
					var leftPos, scrollPos = 0, slideObj, direction = "right";
					//Slide Right
					settingsObj.$slideRightEl.click( function () {
						scrollTheSlide( settingsObj.$slideWrapperEl, settingsObj.scrollPosLimit, "right" );
						direction = "right";
					} );
					
					//Slide Left
					settingsObj.$slideLeftEl.click( function () {
						scrollTheSlide( settingsObj.$slideWrapperEl, settingsObj.scrollPosLimit, "left" );
						direction = "left";
					} );
					
					autoScroll();
					settingsObj.$kick_start_sliding.on( 'click', function () {
						autoScroll();
					} );
					
					settingsObj.$slideWrapperEl.hover( function () { //mouse enter
						$( this ).addClass( 'pause' );
					}, function () { //mouse leave
						$( this ).removeClass( 'pause' );
					} );
					
					function autoScroll () {
						if ( settingsObj.$slideOuterEl.is( ':visible' ) ) {
							var sliderLoop = setInterval( function () {
								if ( ! settingsObj.$slideWrapperEl.hasClass( 'pause' ) ) {
									slideObj = scrollTheSlide( settingsObj.$slideWrapperEl, settingsObj.scrollPosLimit, direction );
									leftPos = slideObj.leftPos;
									if ( leftPos == 0 ) {
										direction = "right";
									}
									else if ( scrollPos != 0 && scrollPos == slideObj.scrollPos.scrollLeft ) {
										direction = "left";
									}
									scrollPos = slideObj.scrollPos.scrollLeft;
								}
							}, 2500 );
						}
					}
					
					/*settingsObj.$slideWrapperEl.scroll(function () {
					 var $elem = settingsObj.$slideWrapperEl;
					 var newScrollLeft = $elem.scrollLeft(),
					 width = $elem.outerWidth(),
					 scrollWidth = $elem.get(0).scrollWidth;
					 });*/
				},
				scrollTheSlide = function ( selector, scrollPosLimit, direction ) {
					direction = pdg_sanitize.inputdefaultparam( direction, "right" );
					scrollPosLimit = pdg_sanitize.inputdefaultparam( scrollPosLimit, 200 );
					var leftPos = selector.scrollLeft(), scrollPos = {};
					switch ( direction ) {
						case "left":
							scrollPos = { scrollLeft: leftPos - scrollPosLimit };
							break;
						case "right":
						default:
							scrollPos = { scrollLeft: leftPos + scrollPosLimit };
							break;
						
					}
					selector.stop( true, false ).animate( scrollPos, "fast" );
					
					return {
						leftPos: leftPos,
						scrollPos: scrollPos,
					};
				},
				scrollToTop = function () {
					$( window ).scroll( function () {
						if ( $( this ).scrollTop() >= 50 ) {
							if ( ! settingsObj.$scroll_to_top.add( settingsObj.$scroll_to_bottom ).is( ':visible' ) ) {
								settingsObj.$scroll_to_top.add( settingsObj.$scroll_to_bottom ).fadeIn( 200 ).delay( 5000 ).fadeOut( 200 );
							}
						}
					} );
					settingsObj.$scroll_to_top.click( function () {
						settingsObj.$bodyEl.animate( {
							scrollTop: 0
						}, 500 );
					} );
					settingsObj.$scroll_to_bottom.click( function () {
						settingsObj.$bodyEl.animate( {
							scrollTop: $( document ).height() - $( window ).height()
						}, 500 );
					} );
				},
				responsiveHiddendynamicImageLoad = function () {
					var flagStart = false;
					
					function fn () {
						var $dynamicImages = $( "img[data-src]" );
						$dynamicImages.each( function ( index ) {
							if ( $( this ).is( ':visible' ) && typeof $dynamicImages[index] != 'undefined' ) {
								this.src = $( this ).data( 'src' );
								$dynamicImages[index] = undefined;
							}
						} );
					}
					
					$( document ).ajaxComplete( function () {
						flagStart = true;
						fn();
					} );
					if ( ! flagStart ) {
						fn();
					}
				},
				startSearchInit = function () {
					settingsObj.$start_searchEl.on( 'click', function () {
						$( "#search_subject-selectized" ).click();
					} );
				},
				nudgeSpotHandler = function () {
					var nudgespot_container_var = ".nudgespot-container";
					
					if ( settingsObj.$search_page_type === "search" || settingsObj.$search_page_type === "profile" ) {
						waitForElementLoad( nudgespot_container_var, function () {
							$( nudgespot_container_var ).hide();
						}, 500 );
						return;
					}
					$( document ).on( 'shown.bs.modal', function () {
						$( nudgespot_container_var ).hide();
					} ).on( 'hide.bs.modal', function () {
						$( nudgespot_container_var ).show();
					} );
				},
				modalScrollFix = function () {
					var scrollPos = 0;
					if ( isiOS ) {
						$( '.modal' ).on( 'show.bs.modal', function () {
							var $body_el = $( 'body' );
							scrollPos = $body_el.scrollTop();
							$body_el.css( {
								overflow: 'hidden',
								position: 'fixed',
								top: - scrollPos
							} );
						} ).on( 'hide.bs.modal', function () {
							var $body_el = $( 'body' );
							$body_el.css( {
								overflow: '',
								position: '',
								top: ''
							} ).scrollTop( scrollPos );
						} );
					}
				},
				initMaterialize = function () {
					$.material.options = {
						"validate": true,
						"input": true,
						"ripples": true,
						"checkbox": true,
						"togglebutton": true,
						"radio": true,
						"arrive": true,
						"autofill": false,
						
						"withRipples": [
							".btn:not(.btn-link)",
							".card-image",
							".navbar a:not(.withoutripple)",
							".dropdown-menu a",
							".nav-tabs a:not(.withoutripple)",
							".withripple",
							".pagination li:not(.active):not(.disabled) a:not(.withoutripple)"
						].join( "," ),
						"inputElements": "input.form-control, textarea.form-control, .search:not(select.form-control), select.form-control",
						"checkboxElements": ".checkbox > label > input[type=checkbox], label.checkbox-inline > input[type=checkbox]",
						"togglebuttonElements": ".togglebutton > label > input[type=checkbox]",
						"radioElements": ".radio > label > input[type=radio], label.radio-inline > input[type=radio]"
					};
					$.material.init();
				};
		}, pdgMainObj;
		pdgMainObj = new PDG_Main;
		pdgMainObj.init();
	} );
} );
