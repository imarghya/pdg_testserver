(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
			var PDG_Institution = function () {
				var mainObj = this, settingsObj;
				//Settings
				mainObj.settings = function () {
					var $bodyEl = $( 'body' ),
						$isMobile = $( '.isMobile' ),
						$isMobileTablet = $( '.isMobileTablet' ),
						$isTablet = $( '.isTablet' ),
						$section = $( 'section' ),
						dataTogglePopover = '[data-toggle=popover]',
						$popoverEl = $( ".popover" );
					
					return {
						$bodyEl: $bodyEl,
						$isMobile: $isMobile,
						$isMobileTablet: $isMobileTablet,
						$isTablet: $isTablet,
						$section: $section,
						$popoverEl: $popoverEl,
						dataTogglePopover: dataTogglePopover,
					}
				};
				
				//Load initial page search results
				mainObj.init = function () {
					settingsObj = mainObj.settings();
					mainObj.bindUIActions();
				};
				
				mainObj.bindUIActions = function () {
					resizeHandler();
					popoverFix();
				};
				
				//offset controls for the UI
				var resizeHandler = function () {
						$( window ).resize( function () {
						} ).resize();
					},
					scrollHandler = function () {
						$( window ).scroll( function () {
						
						} );
					},
					popoverFix = function () {
						settingsObj.$bodyEl.popover( {
							selector: settingsObj.dataTogglePopover,
							trigger: "hover click",
							placement: 'top',
							html: false,
							delay: {
								show: 500,
								hide: 100
							},
						} ).on( "show.bs.popover", function ( e ) {
							// hide all other popovers
							setTimeout( function () {
								$( '.popover' ).fadeOut( 'slow' );
							}, 10000 );
							$( settingsObj.dataTogglePopover ).not( e.target ).popover( "destroy" );
							settingsObj.$popoverEl.remove();
						} );
					};
				
			}, institutionObj;
			institutionObj = new PDG_Institution;
			institutionObj.init();
		}
	);
} );
