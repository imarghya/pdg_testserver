$(document).ready(function () {
	var $id = 0;
	var $maxlen;
	var $message;
	$maxlen = $('.review-textarea label').length;
	$('label').click(function (evt) {
		evt.preventDefault();
		$id = $(this).attr("for");
		$(range(1, $id, 'nohover', 0) + "," + range($id, $maxlen, 'hover', 1)).hide();
		$(range(1, $id, 'hover', 0) + "," + range($id, $maxlen, 'nohover', 1)).show();
	});


	$(".form-review").submit(function (event) {
		event.preventDefault();
	});

	$(".submit-btn").click(function (event) {
		event.preventDefault();
		var $message = '';
		var $result_area = $("#result_area");
		var $submit_button = $(this);
		var $reviewComment = $("#reviewComment");
		var $ajax_loader = $("#img_contact_loader");

		var $reviewComment_value = $reviewComment.val();
		var $rating_value = $id;

		if ($rating_value == 0) {
			$message = "Invalid rating!";
			pdg_show_alerts($result_area, $message, 'error');
			return;
		}
		if ($reviewComment_value.length <= 2) {
			$reviewComment.select().focus();
			$message = "Comment is required!";
			pdg_show_alerts($result_area, $message, 'error');
			return;
		}

		$submit_button.button('loading');

		$ajax_loader.show();

		var submit_data = {
			action: 'pedagoge_visitor_ajax_handler',
			pedagoge_callback_function: 'thankyoucallback_ajax',
			pedagoge_callback_class: 'PDGBusinessContactForm',
			reviewComment_value: $reviewComment_value,
			rating_value: $rating_value
		};

		$.post($ajax_url, submit_data, function (response) {
			try {
				var $response_data = $.parseJSON(response);

				var $error = $response_data.error,
					$message = $response_data.message;

				if ($error) {
					pdg_show_alerts($result_area, $message, 'error');
				} else {
					$('.stars, #reviewComment, .submit-btn, .rateus').hide();
					pdg_show_alerts($result_area, $message, 'success');
				}
			} catch (err) {
				$message = 'Unexpected Error in processing your request! Please try again!' + err;
				pdg_show_alerts($result_area, $message, 'error');
			} finally {
				$submit_button.button('reset');
				$ajax_loader.hide();
			}

		}).complete(function () {
			$submit_button.button('reset');
			$ajax_loader.hide();
		});

	});

});

function range(start, end, type, step) {
	start = parseInt(start, 10) + parseInt(step, 10);
	end = parseInt(end, 10) + parseInt(step, 10);
	var text = "";
	for (var i = start; i <= end; i++) {
		text += '.' + i + '.' + type + ',';
	}
	return text.slice(0, -1);
}