$(document).ready(function () {
	$(".opCard", this).click(function (e) {
		if (e.target.className.indexOf("makeLink") > -1) {
			//window.location = $(this).find(".makeLink").attr("data-url");
			e.stopPropagation();
		}
		else if ($(this).find(".flip-up-opCard").hasClass('fa-flip-vertical')) {
			$(this).find(".opPara").hide();
			$(this).find(".opCard").removeClass('opCardHeight');
			$(this).find(".flip-up-opCard").removeClass('fa-flip-vertical');
		}
		else {
			$(".opPara").hide();
			$(".opCard").removeClass('opCardHeight');
			$(".flip-up-opCard").removeClass('fa-flip-vertical');
			$(this).find(".opPara").show();
			$(this).find(".opCard").addClass('opCardHeight');
			$(this).find(".flip-up-opCard").addClass('fa-flip-vertical');
		}

	});

	//Whether the browser is IE or not
	var notIE8 = true;
	var detectIEVar = detectIE();
	if (detectIEVar === false) {
		//Not a MS based browser
	}
	else if (detectIEVar <= 8) {
		//IE8
		$('body').addClass('ie');
		notIE8 = false;
	}
	//Add 'ie-edge' to the body to render on ie10 and Edge only styles
	else if (detectIEVar >= 10) {
		$('body').addClass('ie-edge');
	}
	//Disable text selection in IE
	if (detectIEVar < 10) {
		document.onselectstart = function () {
			return false;
		}
	}
	//Non IE8 browsers are given fancy touch
	if (notIE8) {
	}
});
//Detect browsers
function detectIE() {
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}
	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}
	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// Edge (IE 12+) => return version number
		return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}
	// other browser
	return false;
}


