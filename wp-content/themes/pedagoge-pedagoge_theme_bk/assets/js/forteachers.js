var $alert_time_out_var = null, $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';

function fn_alert_message($div_id, $message, $alert_type, $timer) {

	clearTimeout($alert_time_out_var);

	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000 : $timer;


	var $result_area = null;
	if (typeof $div_id === "string") {
		$result_area = jQuery("#" + $div_id);
	} else {
		$result_area = $div_id;
	}
	
	var $str_message = '';
	
	$result_area.html('');


	switch ($alert_type) {
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'info':
			$str_message = '<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'success':
			$str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'warning':
			$str_message = '<div class="alert alert-block alert-primary fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'blank_success':
			$str_message = '<span class="label label-success fade in blink_me">' + $message + '</span>';
			break;
		case 'blank_error':
			$str_message = '<span class="label label-primary fade in blink_me">' + $message + '</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html($str_message);


	if ($timer) {
		$alert_time_out_var = setTimeout(function () {
			$result_area.html('');
		}, $timer);
	}
}

$(document).ready(function () {
	$(".numeric").numeric();
	$(".integer").numeric(false, function() { console.log("Integers only"); this.value = ""; this.focus(); });
	$(".positive").numeric({ negative: false }, function() { console.log("No negative values"); this.value = ""; this.focus(); });
	$(".positive-integer").numeric({ decimal: false, negative: false }, function() { console.log("Positive integers only"); this.value = ""; this.focus(); });
    $(".decimal-2-places").numeric({ decimalPlaces: 2 });	
	
	$('.clickable-institution, .clickable-teachers').click(function () {
		var $section6_forteachers = $('.section6-forteachers');
		$section6_forteachers.show();
		$('html, body').animate({
			scrollTop: $section6_forteachers.stop().offset().top - 200
		}, 500);
	});
	$('.clickable-teachers').click(function () {
		$('.clickable-teachers').removeClass('grayscale-forteachers');
		$('.clickable-institution').addClass('grayscale-forteachers');
		$('.select_user_role_option').html('<option selected="selected" value="teacher">Teacher</option>');
	});
	$('.clickable-institution').click(function () {
		$('.clickable-institution').removeClass('grayscale-forteachers');
		$('.clickable-teachers').addClass('grayscale-forteachers');
		$('.select_user_role_option').html('<option selected="selected" value="institution">Institution</option>');
	});


	$("#registration_form").validationEngine({scroll: false});


	$('.chk_register_show_password').change(function () {
		if ($(this).is(":checked")) {
			$("#txt_register_password").prop('type', 'text');
		} else {
			$("#txt_register_password").prop('type', 'password');
		}
	});

	$("#cmd_register_user").click(function (event) {
		event.preventDefault(); //incase button becomes submit button.
		
		var $button = $(this),
			$txt_register_full_name = $( "#txt_register_full_name" ),
			$txt_register_email_address = $("#txt_register_email_address"),
			$txt_register_mobile_number = $( "#txt_register_mobile_number" ),
			$txt_register_password = $("#txt_register_password"),
			$user_role = $(".select_user_role_option").val(),
			$loader = $("#img_signup_loader");

		if ($("#registration_form").validationEngine('validate', {scroll: false})) {

			/**
			 * Checking for user role manually, so that multiple scenarios can be covered.
			 */
			if ($user_role.length <= 0) {
				var $select_user_role = $(".select_user_role");
				if ($select_user_role.is(":visible")) {
					$select_user_role.validationEngine('showPrompt', 'Please select your user role.', 'load');
					return false;
				} else {
					/**
					 * execution will never reach the code below.
					 * still setting default user role in case any weired bug creeps up.
					 */
					$user_role = 'student';
				}
			}


			$button.button('loading');
			$loader.show();
			var submit_data = {
				nonce: $('.ajaxnonce').val(),
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'user_signup_ajax',
				pedagoge_callback_class: 'ControllerSignup',
				full_name: $txt_register_full_name.val(),
				email_address: $txt_register_email_address.val(),
				mobile_number: $txt_register_mobile_number.val(),
				password: $txt_register_password.val(),
				user_role: $user_role,
			};

			$.post($ajax_url, submit_data, function (response) {
				try {
					var $response_data = $.parseJSON(response);

					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message;

					if ($error) {
						//handle login error
						$button.button('reset');
						fn_alert_message('div_signup_result', $message, 'error');

						switch ($error_type) {							
							case 'duplicate_email':
								$txt_register_email_address.select().focus();
								break;
						}


					} else {
						fn_alert_message('div_signup_result', $message, 'success');
						setTimeout(function () {
							//location.reload(true); // do this later.
							window.location = $('.siteUrl').val() + "/profile";
						}, 500);


					}
				} catch (err) {
					$button.button('reset');
				} finally {
					$loader.hide();
				}
			}).complete(function () {
				//$button.button('reset');
				$loader.hide();
			});
		}
	});
});

///////////////////////////////////////Signup Form Processing Ends////////////////////////////////////////////////

/*
 *
 * Copyright (c) 2006-2014 Sam Collett (http://www.texotela.co.uk)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version 1.4.1
 * Demo: http://www.texotela.co.uk/code/jquery/numeric/
 *
 */
(function(factory){if(typeof define === 'function' && define.amd){define(['jquery'], factory);}else{factory(window.jQuery);}}(function($){$.fn.numeric=function(config,callback){if(typeof config==="boolean"){config={decimal:config,negative:true,decimalPlaces:-1}}config=config||{};if(typeof config.negative=="undefined"){config.negative=true}var decimal=config.decimal===false?"":config.decimal||".";var negative=config.negative===true?true:false;var decimalPlaces=typeof config.decimalPlaces=="undefined"?-1:config.decimalPlaces;callback=typeof callback=="function"?callback:function(){};return this.data("numeric.decimal",decimal).data("numeric.negative",negative).data("numeric.callback",callback).data("numeric.decimalPlaces",decimalPlaces).keypress($.fn.numeric.keypress).keyup($.fn.numeric.keyup).blur($.fn.numeric.blur)};$.fn.numeric.keypress=function(e){var decimal=$.data(this,"numeric.decimal");var negative=$.data(this,"numeric.negative");var decimalPlaces=$.data(this,"numeric.decimalPlaces");var key=e.charCode?e.charCode:e.keyCode?e.keyCode:0;if(key==13&&this.nodeName.toLowerCase()=="input"){return true}else if(key==13){return false}var allow=false;if(e.ctrlKey&&key==97||e.ctrlKey&&key==65){return true}if(e.ctrlKey&&key==120||e.ctrlKey&&key==88){return true}if(e.ctrlKey&&key==99||e.ctrlKey&&key==67){return true}if(e.ctrlKey&&key==122||e.ctrlKey&&key==90){return true}if(e.ctrlKey&&key==118||e.ctrlKey&&key==86||e.shiftKey&&key==45){return true}if(key<48||key>57){var value=$(this).val();if($.inArray("-",value.split(""))!==0&&negative&&key==45&&(value.length===0||parseInt($.fn.getSelectionStart(this),10)===0)){return true}if(decimal&&key==decimal.charCodeAt(0)&&$.inArray(decimal,value.split(""))!=-1){allow=false}if(key!=8&&key!=9&&key!=13&&key!=35&&key!=36&&key!=37&&key!=39&&key!=46){allow=false}else{if(typeof e.charCode!="undefined"){if(e.keyCode==e.which&&e.which!==0){allow=true;if(e.which==46){allow=false}}else if(e.keyCode!==0&&e.charCode===0&&e.which===0){allow=true}}}if(decimal&&key==decimal.charCodeAt(0)){if($.inArray(decimal,value.split(""))==-1){allow=true}else{allow=false}}}else{allow=true;if(decimal&&decimalPlaces>0){var dot=$.inArray(decimal,$(this).val().split(""));if(dot>=0&&$(this).val().length>dot+decimalPlaces){allow=false}}}return allow};$.fn.numeric.keyup=function(e){var val=$(this).val();if(val&&val.length>0){var carat=$.fn.getSelectionStart(this);var selectionEnd=$.fn.getSelectionEnd(this);var decimal=$.data(this,"numeric.decimal");var negative=$.data(this,"numeric.negative");var decimalPlaces=$.data(this,"numeric.decimalPlaces");if(decimal!==""&&decimal!==null){var dot=$.inArray(decimal,val.split(""));if(dot===0){this.value="0"+val;carat++;selectionEnd++}if(dot==1&&val.charAt(0)=="-"){this.value="-0"+val.substring(1);carat++;selectionEnd++}val=this.value}var validChars=[0,1,2,3,4,5,6,7,8,9,"-",decimal];var length=val.length;for(var i=length-1;i>=0;i--){var ch=val.charAt(i);if(i!==0&&ch=="-"){val=val.substring(0,i)+val.substring(i+1)}else if(i===0&&!negative&&ch=="-"){val=val.substring(1)}var validChar=false;for(var j=0;j<validChars.length;j++){if(ch==validChars[j]){validChar=true;break}}if(!validChar||ch==" "){val=val.substring(0,i)+val.substring(i+1)}}var firstDecimal=$.inArray(decimal,val.split(""));if(firstDecimal>0){for(var k=length-1;k>firstDecimal;k--){var chch=val.charAt(k);if(chch==decimal){val=val.substring(0,k)+val.substring(k+1)}}}if(decimal&&decimalPlaces>0){var dot=$.inArray(decimal,val.split(""));if(dot>=0){val=val.substring(0,dot+decimalPlaces+1);selectionEnd=Math.min(val.length,selectionEnd)}}this.value=val;$.fn.setSelection(this,[carat,selectionEnd])}};$.fn.numeric.blur=function(){var decimal=$.data(this,"numeric.decimal");var callback=$.data(this,"numeric.callback");var negative=$.data(this,"numeric.negative");var val=this.value;if(val!==""){var re=new RegExp("^" + (negative?"-?":"") + "\\d+$|^" + (negative?"-?":"") + "\\d*" + decimal + "\\d+$");if(!re.exec(val)){callback.apply(this)}}};$.fn.removeNumeric=function(){return this.data("numeric.decimal",null).data("numeric.negative",null).data("numeric.callback",null).data("numeric.decimalPlaces",null).unbind("keypress",$.fn.numeric.keypress).unbind("keyup",$.fn.numeric.keyup).unbind("blur",$.fn.numeric.blur)};$.fn.getSelectionStart=function(o){if(o.type==="number"){return undefined}else if(o.createTextRange&&document.selection){var r=document.selection.createRange().duplicate();r.moveEnd("character",o.value.length);if(r.text=="")return o.value.length;return Math.max(0,o.value.lastIndexOf(r.text))}else{try{return o.selectionStart}catch(e){return 0}}};$.fn.getSelectionEnd=function(o){if(o.type==="number"){return undefined}else if(o.createTextRange&&document.selection){var r=document.selection.createRange().duplicate();r.moveStart("character",-o.value.length);return r.text.length}else return o.selectionEnd};$.fn.setSelection=function(o,p){if(typeof p=="number"){p=[p,p]}if(p&&p.constructor==Array&&p.length==2){if(o.type==="number"){o.focus()}else if(o.createTextRange){var r=o.createTextRange();r.collapse(true);r.moveStart("character",p[0]);r.moveEnd("character",p[1]-p[0]);r.select()}else{o.focus();try{if(o.setSelectionRange){o.setSelectionRange(p[0],p[1])}}catch(e){}}}}}));