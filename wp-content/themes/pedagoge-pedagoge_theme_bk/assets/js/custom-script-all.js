/**
 * Function to create various types of messages such as Info/Error/Success/Warning
 * @param $div_id
 * @param $message
 * @param $alert_type
 */
function pdg_show_alerts($div_id, $message, $alert_type) {
	$alert_type = (typeof $alert_type === "undefined") ? 'info' : $alert_type;

	var $result_area = null;
	if (typeof $div_id === "string") {
		$result_area = jQuery("#" + $div_id);
	} else {
		$result_area = $div_id;
	}
	var $message_desc = '', $classname = '';
	$result_area.html('');

	switch ($alert_type) {
		case 'error':
			$classname = $alert_type;
			$message_desc = 'Error:';
			break;
		case 'info':
			$classname = $alert_type;
			$message_desc = 'Information:';
			break;
		case 'success':
			$classname = $alert_type;
			$message_desc = 'Success:';
			break;
		case 'warning':
			$classname = $alert_type;
			$message_desc = 'Warning:';
			break;
		default:
			$classname = '';
			$message_desc = $message;
			break;
	}
	var $output = '<div class="alert alert-block fade in custom-style-message-box custom-style-message-' + $classname + '"  style="">  <strong class="custom-style-message-desc">' + $message_desc + '</strong> <hr class="custom-style-message-hr custom-style-message-hr-' + $classname + '"/> <div class="custom-style-message-text"><li>' + $message.replace(/Error:|Errors:|Error(s):|Error!|Errors!|Error(s)!|Error!:|Errors!:|Error(s)!:/gi, '').trim() + '</li></div> </div>';
	$result_area.html($output);
}