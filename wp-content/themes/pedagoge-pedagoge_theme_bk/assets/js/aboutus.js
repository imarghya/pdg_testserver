/**
 *
 * Script for About Us page
 * Author : Mainak RC
 *
 */

$(document).ready(function () {
	$('#founders').hide();
	$('#founders_Stats').hide();
	$('#team').hide();

	$('#founderHeading').click(function () {
		$(this).removeClass('effectsOnHover text-muted').addClass('bold');
		$('#aboutHeading').removeClass('bold').addClass('effectsOnHover text-muted');
		$('#teamHeading').removeClass('bold').addClass('effectsOnHover text-muted');
		
		$('#about').hide();
		$('#about_Stats').hide();
		$('#team').hide();

		$('#founders').show();
		$('#founders_Stats').show();
	});

	$('#teamHeading').click(function () {
		$(this).removeClass('effectsOnHover text-muted').addClass('bold');
		$('#founderHeading').removeClass('bold').addClass('effectsOnHover text-muted');
		$('#aboutHeading').removeClass('bold').addClass('effectsOnHover text-muted');

		$('#founders').hide();
		$('#founders_Stats').hide();
		$('#about').hide();
		$('#about_Stats').hide();

		$('#team').show();
	});

	$('#aboutHeading').click(function () {
		$(this).removeClass('effectsOnHover text-muted').addClass('bold');
		$('#founderHeading').removeClass('bold').addClass('effectsOnHover text-muted');
		$('#teamHeading').removeClass('bold').addClass('effectsOnHover text-muted');

		$('#founders').hide();
		$('#founders_Stats').hide();
		$('#team').hide();

		$('#about').show();
		$('#about_Stats').show();
	});

	$('[data-toggle="popover"]').popover();

	$('body').on('click', function (e) {
    	$('[data-toggle="popover"]').each(function () {
        	//the 'is' for buttons that trigger popups
        	//the 'has' for icons within a button that triggers a popup
        	if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            	$(this).popover('hide');
        	}
    	});
	});

});
