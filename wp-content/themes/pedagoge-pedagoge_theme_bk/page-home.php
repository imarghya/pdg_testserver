<?php
/*
Template Name: Version 2 - Home Page
@description: This will route the home page according to the device
*/
global $isVersion2;
$isVersion2 = true;
$theme_dir = get_template_directory_uri();
get_header( 'home_ver2' );
?>
	<main>
		<!-- scroll to top -->
		<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
		<!--/scroll to top -->

		<section class="section-main-top container-fluid z-depth-1">
			<div class="row">
				<div class="bg-image-main">
					<div class="container-fluid">
						<div class="row text-center">
							<h1 class="main-heading">TEACHER SEARCH MADE EASY</h1>
							<h3 class="sub-heading nomargin">Find teachers in your locality, at your fingertips.</h3>
						</div>
					</div>
					<div class="hide search-page-type" data-type="home"></div>
					<?php
					require_once( locate_template( 'template-parts/ver2/search_bar.php' ) );
					?>
					<div class="clearfix">
						<br/> <a href="#"
						         class="btn btn-raised btn-warning start_a_class_btn noroundcorner fixed-callback valign-wrapper"
						         data-toggle="modal" data-backdrop="static" data-keyboard="false"
						         data-target="#request-teacher-options-modal"><i class="material-icons valign">
								&#xE0B0;</i>&nbsp;&nbsp;Request Callback</a>
					</div>

				</div>
			</div>
		</section>
		<section class="container-fluid section-strips hidden-xs slider-strips">
			<div class="row">
				<h1 class="main-heading text-center">TRENDING SUBJECTS</h1>
			</div>
			<div class="row">
				<div class="slideOuter no-select">
					<!--Kick start the slides-->
					<div class="kick-start-sliding hide"></div>
					<!--/Kick start the slides-->
					<!--Slides start-->
					<div class="arrowX left"><i class="material-icons">&#xE314;</i></div>
					<div class="col-xs-12 slideWrapper">
						<div class="slideInner">
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Accounts&pdg_locality=" id="gtm_Accounting"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/accounting.svg"
											title="Accounting"
											alt="Accounting"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/accounting.png';this.className='responsive-png'" id="gtm_Accounting_img"/>
								</a> <h5>Accounting</h5>
							</div>
							<div class="slideX">
								<a href="<?php echo home_url( 'search' ) ?>?pdg_subject=All%20Subjects&pdg_locality=" id="gtm_Allsubjects">
								
								<img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/all-subjects.svg"
											title="All Subjects"
											alt="All Subjects"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/all-subjects.png';this.className='responsive-png'" id="gtm_Allsubjects_img"/>
								</a> <h5>All Subjects</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Commerce&pdg_locality=" id="gtm_commerce"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/commerce.svg"
											title="Commerce" alt="Commerce"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/commerce.png';this.className='responsive-png'" id="gtm_commerce_img"/>
								</a> <h5>Commerce</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Economics&pdg_locality=" id="gtm_Economics"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/economics.svg"
											title="Economics"
											alt="Economics"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/economics.png';this.className='responsive-png'" id="gtm_Economics_img"/>
								</a> <h5>Economics</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=English&pdg_locality=" id="gtm_English"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/english.svg"
											title="English" alt="English"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/english.png';this.className='responsive-png'" id="gtm_English_img"/>
								</a> <h5>English</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Guitar&pdg_locality=" id="gtm_Guitar"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/guitar.svg"
											title="Guitar" alt="Guitar"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/guitar.png';this.className='responsive-png'" id="gtm_Guitar_img"/>
								</a> <h5>Guitar</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Gym&pdg_locality=" id="gtm_Gym"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/gym.svg"
											title="Gym" alt="Gym"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/gym.png';this.className='responsive-png'" id="gtm_Gym_img"/>
								</a> <h5>Gym</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Mathematics&pdg_locality=" id="gtm_Mathematics"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/maths.svg"
											title="Mathematics" alt="Mathematics"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/maths.png';this.className='responsive-png'" id="gtm_Mathematics_img"/>
								</a> <h5>Mathematics</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Painting&pdg_locality=" id="gtm_Painting"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/painting.svg"
											title="Painting" alt="Painting"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/painting.png';this.className='responsive-png'" id="gtm_Painting_img"/>
								</a> <h5>Painting</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=All%20Subjects%20(Pre%20School)&pdg_locality=" id="gtm_Preschool"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/preschool.svg"
											title="Pre-School"
											alt="Pre-School"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/preschool.png';this.className='responsive-png'" id="gtm_Preschool_img"/>
								</a> <h5>Pre-School</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Spoken%20English&pdg_locality=" id="gtm_SpokenEnglish"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/spoken-english.svg"
											title="Spoken English"
											alt="Spoken English"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/spoken-english.png';this.className='responsive-png'" id="gtm_SpokenEnglish_img"/>
								</a> <h5>Spoken English</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=All%20Western%20Dance%20Forms&pdg_locality=" id="gtm_Westerndance"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/western-dance.svg"
											title="Western Dance"
											alt="Western Dance"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/western-dance.png';this.className='responsive-png'" id="gtm_Westerndance_img"/>
								</a> <h5>Western Dance</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Yoga&pdg_locality=" id="gtm_Yoga"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/yoga.svg?1.2"
											title="Yoga" alt="Yoga"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/yoga.png?1.2';this.className='responsive-png'" id="gtm_Yoga_img"/>
								</a> <h5>Yoga</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Karate&pdg_locality=" id="gtm_Karate"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/karate.svg"
											title="Karate" alt="Karate"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/karate.png';this.className='responsive-png'" id="gtm_Karate_img"/>
								</a> <h5>Karate</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Common%20Admission%20Test%20(CAT),Graduate%20Management%20Aptitude%20Test%20(GMAT),Graduate%20Record%20Examination%20(GRE),Narsee%20Monjee%20Aptitude%20Test%20(NMAT),Post%20Graduate%20Diploma%20in%20Basic%20Counselling&pdg_locality=" id="gtm_MBA"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/MBA.svg"
											title="MBA" alt="MBA"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/MBA.png';this.className='responsive-png'" id="gtm_MBA_img"/>
								</a> <h5>MBA</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=science&pdg_locality=" id="gtm_Science"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/science.svg?v=0.2"
											title="Science" alt="Science"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/science.png?v=0.2';this.className='responsive-png'" id="gtm_Science_img"/>
								</a> <h5>Science</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Social%20Science&pdg_locality=" id="gtm_SocialScience"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/social-science.svg"
											title="Social Science" alt="Social Science"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/social-science.png';this.className='responsive-png'" id="gtm_SocialScience_img"/>
								</a> <h5>Social Science</h5>
							</div>
							<div class="slideX">
								<a href="<?= home_url( 'search' ) ?>?pdg_subject=Zumba&pdg_locality=" id="gtm_Zumba"><img
											data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/svgs/zumba.svg"
											title="Zumba" alt="Zumba"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/strips/pngs/zumba.png';this.className='responsive-png'" id="gtm_Zumba_img"/>
								</a> <h5>Zumba</h5>
							</div>
						</div>
					</div>
					<!--Slides end-->
					<div class="arrowX right"><i class="material-icons">&#xE315;</i></div>
				</div>
			</div>
		</section>
		<section class="container-fluid section-popular-courses">
			<div class="row">
				<div class="card card-section-styles">
					<div class="container-fluid text-center">
						<div class="row comic-strip bg1 margin-bottom-20">
							<h1 class="main-heading">POPULAR COURSES</h1>
						</div>
						<div class="row">
							<div class="container-fluid-xs">
								<div class="row-xs"> <!--1-->
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Science,PCMB%20(Physics%20Chemistry%20Maths%20Biology),English,Mathematics,Social%20Science,Career%20Counselling&pdg_locality=" id="gtm_schoolsubjects"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/school_subjects.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/school_subjects.png';this.className='valign responsive-png'" id="gtm_schoolsubjects_img"></a>
										</div>
										<p class="desc-styles">SCHOOL SUBJECTS</p>
									</div>
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Classical%20Dance,All%20Western%20Dance%20Forms,Music,Synthesizer/Keyboard&pdg_locality=" id="gtm_performingarts"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/performing_arts.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/performing-arts.png';this.className='valign responsive-png'" id="gtm_performingarts_img"></a>
										</div>
										<p class="desc-styles">PERFORMING ARTS</p>
									</div>
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Bachelor%20Of%20Commerce%20(BCOM),Narsee%20Monjee%20Aptitude%20Test%20(NMAT),Bachelor%20of%20Business%20Administration%20(BBA)%20Entrance,PHP,Java,Web%20Development,Interior%20Designing%20(Professional)&pdg_locality=" id="gtm_collegecourses"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/college_course.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/college-course.png';this.className='valign responsive-png'" id="gtm_collegecourses_img"></a>
										</div>
										<p class="desc-styles">COLLEGE COURSES</p>
									</div>
								</div>
							</div>
							<div class="container-fluid-xs">
								<div class="row-xs"> <!--2-->
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Chartered%20Accountancy%20(CA%20All%20Levels),Chartered%20Accountancy%20Foundation%20(CPT),Chartered%20Accountancy%20Foundation%20(CPT),Chartered%20Accountancy%20(CA%20All%20Levels),Cost%20Accountancy%20(All%20Levels),Cost%20Accountancy%20Foundation%20(ICWA/CMA),Actuarial%20Common%20Entrance%20Test%20(ACET)&pdg_locality=" id="gtm_professionalcourses"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/professional_course.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/professional-course.png';this.className='valign responsive-png'" id="gtm_professionalcourses_img"></a>
										</div>
										<p class="desc-styles">PROFESSIONAL COURSES</p>
									</div>
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Gym,Strength%20&amp;%20Conditioning,Kettlebell%20class,Yoga,Fitness,CrossFit,Weight%20Training,Zumba,Aerobics,Kickboxing&pdg_locality=" id="gtm_getfit"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/fitness.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/fitness.png';this.className='valign responsive-png'" id="gtm_getfit_img"></a>
										</div>
										<p class="desc-styles">GET FIT</p>
									</div>
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Cricket,Chess,Taekwondo,Badminton,Football,Chess%20Boxing,Karate,Basketball,Table%20Tennis&pdg_locality=" id="gtm_sportscourses"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/sport_courses.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/sports.png';this.className='valign responsive-png'" id="gtm_sportscourses_img"></a>
										</div>
										<p class="desc-styles">SPORT COURSES</p>
									</div>
								</div>
							</div>
							<div class="container-fluid-xs">
								<div class="row-xs"> <!--3-->
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Drawing,Painting,Calligraphy,Graphic%20Designing,Photography,Fashion%20Designing&pdg_locality=" id="gtm_creativearts"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/creative.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/creative-arts.png';this.className='valign responsive-png'" id="gtm_creativearts_img"></a>
										</div>
										<p class="desc-styles">CREATIVE ARTS</p>
									</div>
									<div class="col-xs-4 col-sm-3 ">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Bakery,Cooking,Spoken%20English,Photography,Self%20Defence&pdg_locality=" id="gtm_lifestyle"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/lifestyle_courses.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/lifestyle_course.png';this.className='valign responsive-png'" id="gtm_lifestyle_img"></a>
										</div>
										<p class="desc-styles">LIFESTYLE</p>
									</div>
									<div class="col-xs-4 col-sm-3 visible-xs">
										<div class="center-block card decorated_cards">
											<a href="<?= home_url( 'search' ) ?>?pdg_subject=Common%20Admission%20Test%20(CAT),Graduate%20Management%20Aptitude%20Test%20(GMAT),Graduate%20Record%20Examination%20(GRE),Common%20Law%20Aptitude%20Test%20(CLAT),All%20India%20Pre-medical%20Test%20(AIPMT),Bank%20PO,Bank%20Clerical%20Exam%20(IBPS),International%20English%20Language%20Testing%20System%20(IELTS),Test%20Of%20English%20as%20a%20Foreign%20Language%20(TOEFL)&pdg_locality=" id="gtm_Testpreparation"><img
														src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/test_prep.svg"
														onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/test_prep.png';this.className='valign responsive-png'" id="gtm_Testpreparation_img"></a>
										</div>
										<p class="desc-styles">Test Preparation</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="container-fluid section-how-it-works">
			<div class="row">
				<div class="card card-section-styles">
					<div class="container-fluid text-center">
						<div class="row comic-strip bg2 margin-bottom-20">
							<h1 class="main-heading">HOW IT WORKS</h1>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="center-block card decorated_cards">
									<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/search.svg"
									     data-toggle="popover" tabindex="0" role="button"
									     data-content="From CBSE to UPSC, from Spanish to Tennis, From Yoga to Zumba, whatever the requirement be, Pedagoge has home tutors and institutions for all your requirements. Search from more than 5000 profiles online."
									     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/search.png';this.className='valign responsive-png'" id="gtm_search">
								</div>
								<p class="desc-styles">SEARCH</p>
							</div>
							<div class="col-xs-4">
								<div class="center-block card decorated_cards">
									<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/meet.svg"
									     data-toggle="popover" tabindex="0" role="button"
									     data-content="Once you have shortlisted your preferred teacher/institution profiles based on your customised requirement, Pedagoge helps you to connect with the tutors. Now, you can also request for a Demo Class with Pedagoge!"
									     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/meet_the_teacher.png';this.className='valign responsive-png'" id="gtm_MYR">
								</div>
								<p class="desc-styles">MATCH YOUR REQUIREMENT</p>
							</div>
							<div class="col-xs-4">
								<div class="center-block card decorated_cards">
									<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/start_learn.svg"
									     data-toggle="popover" tabindex="0" role="button"
									     data-content="After you have chosen the perfect tuition for yourself, Pedagoge leaves it at the discretion and flexibility of the learner and the tutor to commence the class at their will."
									     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/start_learning.png';this.className='valign responsive-png'" id="gtm_SL">
								</div>
								<p class="desc-styles">START LEARNING</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="container-fluid section-why-pedagoge">
			<div class="row">
				<div class="card card-section-styles">
					<div class="container-fluid text-center">
						<div class="row comic-strip bg3 margin-bottom-20">
							<h1 class="main-heading">WHY PEDAGOGE?</h1>
						</div>
						<div class="row section-desc margin-bottom-20">
							<p>
								<?php
									$home_why_pedagoge = '';
									if(is_array(get_post_custom_values( "home_why_pedagoge" ))) {
										$home_why_pedagoge = get_post_custom_values( "home_why_pedagoge" );
										$home_why_pedagoge = $home_why_pedagoge[0]; 
									}									
									echo $home_why_pedagoge; 
								?>
							</p>
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-3">
								<div class="center-block card decorated_cards">
									<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/community.svg"
									     data-toggle="popover" tabindex="0" role="button"
									     data-content="Pedagoge has a community of learners who provide their individual reviews and recommendations based on personal experience. This helps users make a more informed decision."
									     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/community_of_learners.png';this.className='valign responsive-png'">
								</div>
								<p class="desc-styles">LEARNER'S COMMUNITY</p>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="center-block card decorated_cards">
									<img
											src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/know_your_teacher.svg"
											data-toggle="popover" tabindex="0" role="button"
											data-content="Get to know about your teacher before you join! Get information such as class days, timing, fee, photos, reviews etc. before you join the class."
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/know_your_teachers.png';this.className='valign responsive-png'">
								</div>
								<p class="desc-styles">KNOW YOUR TEACHERS</p>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="center-block card decorated_cards">
									<img
											src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/free_for_students.svg"
											data-toggle="popover" tabindex="0" role="button"
											data-content="Pedagoge provides all these services free of cost to all the learners. Now learners get to choose their best tuition from an array of options completely free of cost."
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/free_for_students.png';this.className='valign responsive-png'">
								</div>
								<p class="desc-styles">FREE FOR STUDENTS</p>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="center-block card decorated_cards">
									<img
											src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/svgs/personal_assistance.svg"
											data-toggle="popover" tabindex="0" role="button"
											data-content="Our executives shall be with you to assist you at every step. Be it taking your requirement, advising teacher options or coordinating with the teachers, we are happy to help over a call also!"
											onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/pngs/personal_assistance.png';this.className='valign responsive-png'">
								</div>
								<p class="desc-styles">PERSONALIZED ASSISTANCE</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="container-fluid section-stats">
			<div class="row">
				<div class="card card-section-styles">
					<div class="container-fluid">
						<div class="row text-center comic-strip bg5">
							<div class="white-text item-counter col-xs-4">
								<h3 class="count">6000+</h3>
								<p class="item-desc">TEACHERS</p>
							</div>
							<div class="white-text item-counter col-xs-4">
								<h3 class="count">1000+</h3>
								<p class="item-desc">INSTITUTES</p>
							</div>
							<div class="white-text item-counter col-xs-4">
								<h3 class="count">15000+</h3>
								<p class="item-desc">STUDENTS</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<h3 class="hearfromus-forteachers text-bold">What users have to say about Pedagoge</h3>
							</div>
						</div>
						<div class="testimonial-wrapper valign-wrapper">
							<div class="col-xs-12">
								<div class="hidden-xs col-sm-2 col-md-3">
								</div>
								<div class="col-xs-12 col-sm-8 col-md-6">
									<div class="carousel slide carousal-forteachers carousel-fade" data-ride="carousel" data-interval="8000">
										<div class="carousel-inner slider-forteachers">
											<div class="item active adjust-height text-justify">
												<p class="closeq">"Pedagoge is a suitable platform for me to get
													students. The online presence they offer is extremely helpful to
													reach out to more students. Moreover, the team has been extremely
													cooperative and supportive of all my needs."</p>
												<p class="teachername-forteachers text-bold text-right"> - Anand Nopany,
													Accounts Teacher</p>
											</div>
											<div class="item adjust-height text-justify">
												<p class="closeq">"Working with Pedagoge has been a wonderful
													experience. The team is helpful and supportive. Now, I can cater to
													students all over the city. This has been extremely helpful."</p>
												<p class="teachername-forteachers text-bold text-right"> - Garima
													Jhawar, All Subjects Teacher</p>
											</div>
											<div class="item adjust-height text-justify">
												<p class="closeq">"I am very pleased to be registered with Pedagoge.
													It’s a dream come true for me and a wonderful platform to be a part
													of. It allows me to get recognition and increase my reach to
													students."</p>
												<p class="teachername-forteachers text-bold text-right"> - Manju Rathi,
													All Subjects Teacher</p>
											</div>
											<div class="item adjust-height text-justify">
												<p class="closeq">"I am quite pleased with the computer teacher you have
													provided me and she perfectly fulfils my requirements. Even my
													daughter is completely at ease in her company. She has developed a
													keen interest in Computer. She is able to clarify her doubts. I am
													quite pleased with your service. I have even recommended Pedagoge to
													some of my friends."</p>
												<p class="teachername-forteachers text-bold text-right"> - Swati
													Sadhukhan, Parent</p>
											</div>
											<div class="item adjust-height text-justify">
												<p class="closeq">"Pedagoge is a good platform where we can get the
													right and experienced teacher for our children."</p>
												<p class="teachername-forteachers text-bold text-right"> - Vandana
													Rathi, Parent</p>
											</div>
											<div class="item adjust-height text-justify">
												<p class="closeq">"Pedagoge is the solution for all my tuition
													requirements. Now I don’t have to ask again and again to my friends,
													relatives for tuition teachers. I am satisfied by the standard of
													teachers provided by Pedagoge. Thank you so much. Keep up the good
													work!"</p>
												<p class="teachername-forteachers text-bold text-right"> - Bhavana
													Agarwal, Student</p>
											</div>
											<div class="item adjust-height text-justify">
												<p class="closeq">"This is the first time I have hired a teacher from
													Pedagoge and till now it has been a good experience. A great online
													platform for hiring teachers for both academic and non
													academic."</p>
												<p class="teachername-forteachers text-bold text-right"> - Pooja
													Bhartia, Student</p>
											</div>


										</div>
									</div>
								</div>
								<div class="hidden-xs col-sm-2 col-md-3">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="container-fluid section-contact-us hidden-xs">
			<div class="row">
				<div class="card card-section-styles">
					<div class="container-fluid">
						<div class="row text-center comic-strip bg4 margin-bottom-20">
							<h1 class="main-heading">CONTACT US</h1>
						</div>
						<div class="row text-center">
							<h3 class="sub-heading">Fill in details to request teacher options.</h3>
						</div>

						<div class="container-fluid contact_us clearfix callback_init" data-callback-name="home_contactus_form">
							<form id="home_contactus_form" class="form-inline contact_form" action="#">
								<div class="show_result center-block"></div>
								<div class="row">
									<div class="col-xs-3">
										<div class="form-group label-floating">
											<label for="contact_name" class="control-label">Name*</label>
											<input type="text" class="form-control validate[required]"
											       id="contact_name">
										</div>
									</div>
									<div class="col-xs-3">
										<div class="form-group label-floating">
											<label for="contact_phone" class="control-label">Phone*</label>
											<input type="text" class="form-control validate[required, custom[phone]]"
											       id="contact_phone">
										</div>
									</div>
									<div class="col-xs-3">
										<div class="form-group label-floating">
											<label for="contact_locality" class="control-label">Locality*</label>
											<input type="text" class="form-control validate[required]"
											       id="contact_locality">
										</div>
									</div>
									<div class="col-xs-3">
										<div class="form-group label-floating">
											<label for="contact_subject" class="control-label">Subject*</label>
											<input type="text" class="form-control validate[required]"
											       id="contact_subject">
										</div>
									</div>

									<div class="col-xs-12 center-block text-center">
										<a href="#"
										   class="btn btn-raised contact_submit noroundcorner" data-loading-text="Processing...">
											REQUEST TEACHER OPTIONS</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php
get_template_part( 'template-parts/ver2/modal-req_callback' );
get_footer( 'home_ver2' );
