<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */

	global $post;
	$slug ='';
	if(isset($post)) {
		$current_post = get_post( $post );
		if(!empty($current_post)) {
			$slug = $current_post->post_name;
		}	
	} 
	

?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head <?php language_attributes(); ?>>
        <meta charset="<?php bloginfo( 'charset' ); ?>">

        <title>Pedagoge Website will be launched soon!</title>

        <meta name="description" content="Pedagoe website is coming soon.">
        <meta name="author" content="pedagoge">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        
        <link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">       

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/fav_icon_logo_orb_small.png">
        
        <!-- END Icons -->
        
			
		<?php wp_head(); ?>
		
		<style type="text/css">
		   .full-page-section {
			    margin: 23px -20px;
			    padding: 13px 0;
			}
			.footer-nav, .footer-heading {
		   	color:#FFFFFF;
		   }
		   .footer-nav a {
		   	font-size: 2em!important;
		   }
		 </style>
		
    </head>
    <body class="change-background">