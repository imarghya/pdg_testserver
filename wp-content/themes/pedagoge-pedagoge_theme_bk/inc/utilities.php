<?php

/**
 * Function for logging values.
 */
function fn_applog($str_val) {
	if(empty($str_val)) {
		$str_val='\n Empty call';
	}
	
	$my_file = get_template_directory().'/applog.txt';
	
	$str_date_time = date('d-m-Y h:i:s A - '); // add timestamp
	$str_val = $str_date_time.$str_val." \n"; // create newline
	
	
	$handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file);	
	fwrite($handle, $str_val);
	fclose($handle);
	
}

// Add specific CSS class by filter
add_filter( 'body_class', 'pedagoge_body_class' );
function pedagoge_body_class( $classes ) {
	// add 'class-name' to the $classes array
	$classes[] = 'class-name';
	// return the $classes array
	return $classes;
}

function pedagoge_body_tags() {
		
	global $post;
	
	$slug ='';
	if(isset($post)) {
		$current_post = get_post( $post );
		if(!empty($current_post)) {
			$slug = $current_post->post_name;
		}	
	} 
	
	$body_tag = '';
	
	switch($slug) {
	 	case 'privacy-policy':
			$body_tag = ' data-spy="scroll" data-target="#header"';
			break;		
	 }
	return $body_tag;
}

// Remove Emojis (Wordpress Default)
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );