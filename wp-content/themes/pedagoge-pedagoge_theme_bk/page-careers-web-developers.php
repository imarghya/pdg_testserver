<?php
/*
Template Name: Careers Web Developers Page
*/

/*get_header('home');
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;
							<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Web Developers
						</div>
						<div class="opPara1 jobDescPara">
							<p>The selected intern(s) will work on following during the internship:</p>
							<p>1. Building reusable components, and optimizing existing architecture<br />2. Devising efficient ways of querying data<br />3. Socket Programming and Real Time application development<br />4. Development Operations.<br />Who can apply:</p>
							<p>Only applicants from Kolkata can apply.<br />Skill(s) required: PHP, MySQL, JavaScript, jQuery, Wordpress and AJAX and Bootstrap.<br />Degree(s): Engineering (B.Tech/B.E/MCA &amp; Similar).<br />Year of degree completion: Applicants currently in any year of study or recent graduates but having relevant skills and interest may apply.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor,
								Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata - 700091,<br/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West
								Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer('home_ver2');