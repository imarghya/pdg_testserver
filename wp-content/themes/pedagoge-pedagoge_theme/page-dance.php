<?php
/*
Template Name: Dance - Landing-jul-09
*/

get_header('home');
$search_url = home_url('/search/');
$theme_dir = get_template_directory_uri();
$obj = new PDGThemesFunctions;
$idList = '2553,2405,2410,2337';
$val = $obj->landingJul09SQL($idList);
?>
	<section class="wrapper container-fluid section1-landingjul09">
		<div class="row header-landingjul09 bgImg dance">
			<div class="col-xs-12">
				Find the right dance teacher
			</div>
		</div>
	</section>
	<section class="wrapper container-fluid section2-landingjul09 text-center center-block">
		<div class="row">
			<?php
			unset($cv);
			$result = $val['result'];
			foreach ($result as $cv) {
				$userID = $cv->user_id;
				$teacher_profile_image_path = PEDAGOGE_PLUGIN_DIR . $val['profile_image_path'] . $userID . '/profile.jpg';
				if (file_exists($teacher_profile_image_path)) {
					$file_timestamp = date('U', filemtime($teacher_profile_image_path));
					$teacher_profile_image_url = PEDAGOGE_PLUGIN_URL . '/' . $val['profile_image_path'] . $userID . '/profile.jpg?';
				}
				else {
					$teacher_profile_image_url = PEDAGOGE_ASSETS_URL . '/images/sample.jpg';
				}
				$slugData = $val['slugData'];
				$profile_slug = $slugData[$userID];

				?>
				<div class="text-center center-block">
					<div class="col-xs-12 col-md-3 animation-fadeInQuick" data-toggle="animation-appear"
					     data-animation-class="animation-fadeInQuick" data-element-offset="-100">
						<div class="store-item">
							<div class="store-item-image">
								<a href="<?= $profile_slug ?>"
								   target="_blank">
									<img
										src="<?= $teacher_profile_image_url; ?>"
										alt="" class="img-responsive img-display">
									<div class="float_teacher"
									     style="position: absolute; z-index: 1001; font-weight: bold; font-size: 15px; width: 100%; color: rgb(51, 51, 51); height: 20px; text-align: center;"></div>
								</a>
							</div>
							<div class="store-item-info clearfix">
								<div class="text-center">
									<a href="<?= $profile_slug ?>"
									   target="_blank"><strong><?= $cv->teacher_name; ?></strong></a><br>
									<small><i class="fa fa-map-marker text-muted"></i>
										<a href="javascript:void(0)" class="text-muted"><?= $cv->locality; ?></a><br>
									</small>
								</div>

								<div class="text-center truncate" data-toggle="tooltip" data-placement="bottom" title=""
								     data-original-title="<?= $cv->subject ?>"><i
										class="fa fa-tags"></i> <?= $cv->subject ?>
								</div>
								<br>
								<div class="text-center">
									<a href="<?= $profile_slug ?>"
									   target="_blank"
									   class="btn btn-success">View complete profile</a>
								</div>

							</div>
						</div>
					</div>
				</div>
				<?php
			}
			?>
		</div>
		<div class="row">
			<div class="col-xs-12 text-right">
				<a class="view-more-landingjul09"
				   href="<?= $search_url ?>/?pdg_subject=Dance&pdg_locality="
				   role="button">View More</a> <span class="text-blue">></span>
			</div>
		</div>
		<!--Dance Ends-->
	</section>

	<section class="wrapper container-fluid section3-landingjul09 center-block">
		<h2>Let us help you</h2>
		<hr class="hr-override-helpyou"/>
		<hr class="hr-helpyou"/>
		<div class="hidden-sm hidden-xs col-xs-3"></div>
		<div id="result_area" class="col-xs-12 col-md-6 center-block resultMsgs"></div>
		<div class="hidden-sm hidden-xs col-xs-3"></div>
		<form class="form-landingjul09">
			<div class="form-group col-xs-12 col-md-6">
				<label for="Name">Name</label>
				<input type="email" class="form-control" id="Name" placeholder="Name">
			</div>
			<div class="form-group col-xs-12 col-md-6">
				<label for="Mobile">Mobile</label>
				<input type="text" class="form-control" id="Mobile" placeholder="Mobile">
			</div>
			<div class="hidden-xs hidden-sm col-md-12 divider"></div>
			<div class="form-group col-xs-12 col-md-6">
				<label for="Email">Email address</label>
				<input type="email" class="form-control" id="Email" placeholder="Email">
			</div>
			<div class="form-group col-xs-12 col-md-6">
				<label for="Requirement">Requirement</label>
				<input type="text" class="form-control" id="Requirement" placeholder="Requirement">
			</div>
			<div id="result_area_success" class="resultMsgs"></div>
			<div class="form-group col-xs-12 text-center">
				<button type="submit" class="btn btn-info submit-form1-landingjul09">Submit</button>
			</div>
			<div class="image_loader">
				<img class="center-block" style="display:none;"
				     src="<?= PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif"
				     id="img_contact_loader"/>
			</div>
		</form>
	</section>

	<section class="wrapper container-fluid section4-landingjul09 text-center center-block">
		<h2>TEACHER SEARCH MADE EASY</h2>
		<div class="col-xs-12 col-md-4">
			<span class="number-highlight">1800+</span><br/><span class="text-highlight">TEACHERS</span>
		</div>
		<div class="col-xs-12 col-md-4">
			<span class="number-highlight">200+</span><br/><span class="text-highlight">HAPPY STUDENTS</span>
		</div>
		<div class="col-xs-12 col-md-4">
			<span class="number-highlight">250+</span><br/><span class="text-highlight">INSTITUTIONS</span>
		</div>
	</section>
<?php
get_footer('home');