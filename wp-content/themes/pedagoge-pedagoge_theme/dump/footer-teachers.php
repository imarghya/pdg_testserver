<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */	

	get_template_part( 'template-parts/modal', 'loginsignup' );
	get_template_part( 'template-parts/modal', 'teachers' );
	get_template_part( 'template-parts/content', 'footer' );
	wp_footer(); 
?>	
	</body>
</html>
