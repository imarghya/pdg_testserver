<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */

	global $post;
	$slug = get_post( $post )->post_name;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		
		<?php wp_head(); ?>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
	</head>
	
	<body <?php body_class(); echo pedagoge_body_tags(); ?>>
		<?php get_template_part( 'template-parts/navigation', 'none' ); ?>		
		
		<div class="teacherhome" id="home"> <!-- Div with ID home starts -->
			
			<div class="overlay"></div>
            
            <div class="container"> <!-- div with class container starts here -->
            	
                <div class="row">
                	
                    <div class="home-text col-md-8">
                        <h1 class="wow fadeInDown">
                        	<img src="http://pedagoge.com/logo.png" width=30% class="pedagoge_logo_img" />
                        </h1>
                        <p><br></p>
						<?php get_template_part( 'template-parts/lead', 'teachers' ); ?>
                    </div>
                    
                    <div class="scroller">
					   <!-- <div class="mouse"><div class="wheel"></div></div> -->
                    </div>
                    
                </div>
                
            </div>  <!-- div with class container ends here -->
            
        </div> <!-- Div with ID home ends here -->
        
        <?php get_template_part( 'template-parts/scrollup', 'nav' ); ?>