<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */

	global $post;
	$slug ='';
	if(isset($post)) {
		$current_post = get_post( $post );
		if(!empty($current_post)) {
			$slug = $current_post->post_name;
		}	
	} 
	

?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		
		<meta name="description" content="Homepage of Pedagoge.">
    	<meta name="author" content="pedagoge">
    	
    	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/fav_icon_logo_orb_small.png">
		
		<?php wp_head(); ?>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
	</head>
	
	<body class="body-404" <?php body_class(); echo pedagoge_body_tags(); ?>>
		<div id="page-container"> <!-- Div page-container starts here -->
			
			<header>
	            <div class="container">
	                <!-- Site Logo -->
	                <a href="<?php echo home_url('/'); ?>" class="site-logo">
	                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/home_01.png" class="img-responsive" width="50%"> 
	                </a>
	                <!-- Site Logo -->
	
	                <!-- Site Navigation -->
	                <nav>
	                    <!-- Menu Toggle -->
	                    <!-- Toggles menu on small screens -->
	                    <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
	                        <i class="fa fa-bars"></i>
	                    </a>
	                    <!-- END Menu Toggle -->
	
	                    <!-- Main Menu -->
	                    <ul class="site-nav">
	                        <!-- Toggles menu on small screens -->
	                        <li class="visible-xs visible-sm">
	                            <a href="javascript:void(0)" class="site-menu-toggle text-center">
	                                <i class="fa fa-times"></i>
	                            </a>
	                        </li>
	                        <!-- END Menu Toggle -->
<!--
	                        <li>
	                            <a href="<?php echo home_url('/for-teachers'); ?>">For Teachers</a>
	                        </li>
	                        <li>
	                            <a href="<?php echo home_url('/for-students'); ?>">For Students</a>
	                        </li>
	                        
-->
							<li>
	                            <a href="<?php echo home_url('/contact-us'); ?>">Contact Us</a>
	                        </li>
	                        <?php
	                        	if(is_user_logged_in()) {
	                        		get_template_part( 'template-parts/menu', 'loggedin' );
	                        	} else {
	                        		get_template_part( 'template-parts/menu', 'signup' );
	                        	}
	                        ?>
	                    </ul>
	                    <!-- END Main Menu -->
	                </nav>
	                <!-- END Site Navigation -->
	            </div>
	        </header>
	        <!-- END Site Header -->