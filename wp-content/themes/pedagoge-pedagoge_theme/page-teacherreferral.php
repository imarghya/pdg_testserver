<?php 
/*
Template Name: Teacher Referral Page
*/

/**
 * Had to create different header, footer, and page templates because of spaghetti code
 * Will clean the code and create uniform structure (Header/Footer/Pages) after new design comes in.
 */

get_header('home');
?>

<!-- 
#### ##    ## ######## ########   #######  
 ##  ###   ##    ##    ##     ## ##     ## 
 ##  ####  ##    ##    ##     ## ##     ## 
 ##  ## ## ##    ##    ########  ##     ## 
 ##  ##  ####    ##    ##   ##   ##     ## 
 ##  ##   ###    ##    ##    ##  ##     ## 
#### ##    ##    ##    ##     ##  #######  
-->
<section class="site-section site-section-light site-section-top change-background">
    <div class="container">
        <div class="row">
            <div class="col-md-12 hidden-sm hidden-xs text-center">
                <div class="input-group center">
                    <input type="text" class="form-control input-lg btn-round search-box" placeholder="Interested Subject">
                    <input type="text" class="form-control input-lg search-box" placeholder="Teacher / Institution">
                    <input type="text" class="form-control input-lg search-box" placeholder="Kolkata">
                    <button type="button" class="form-control input-lg btn btn-success btn-round search-btn">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>

            <div class="col-sm-6 col-sm-offset-3 col-xs-12 hidden-md hidden-lg">
                <div class="input-group">
                    <input type="text" class="form-control search-xs-box" placeholder="Interested Subject">
                    <input type="text" class="form-control search-xs-box" placeholder="Teacher / Institution">
                    <input type="text" class="form-control search-xs-box" placeholder="Kolkata">
                    <button type="button" class="form-control btn btn-success btn-xs-round btn-block">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END Intro -->

<!-- 
########     ###    ##    ## ##    ## ######## ########  
##     ##   ## ##   ###   ## ###   ## ##       ##     ## 
##     ##  ##   ##  ####  ## ####  ## ##       ##     ## 
########  ##     ## ## ## ## ## ## ## ######   ########  
##     ## ######### ##  #### ##  #### ##       ##   ##   
##     ## ##     ## ##   ### ##   ### ##       ##    ##  
########  ##     ## ##    ## ##    ## ######## ##     ## 
 -->
<section class="site-content site-section site-section-light parallax-image" style="background: #D36E13;">
    <div class="container">
        <div class="site-block text-center">
            <h3 class="site-heading text-center">
                TEACHERS HELP SHAPE <strong>LIFE</strong>!<br><br>
                HELP OTHERS TO SHAPE THEIR LIVES...<br><br>
                And guess what, you get rewarded for it!
            </h3>
        </div>
    </div>
</section>

 <!--
 ########     ###     ######  ####  ######  
##     ##   ## ##   ##    ##  ##  ##    ## 
##     ##  ##   ##  ##        ##  ##       
########  ##     ##  ######   ##  ##       
##     ## #########       ##  ##  ##       
##     ## ##     ## ##    ##  ##  ##    ## 
########  ##     ##  ######  ####  ######  


######## ########   #######  ##     ## 
##       ##     ## ##     ## ###   ### 
##       ##     ## ##     ## #### #### 
######   ########  ##     ## ## ### ## 
##       ##   ##   ##     ## ##     ## 
##       ##    ##  ##     ## ##     ## 
##       ##     ##  #######  ##     ## 
 -->
<section class="">
    <div class="container"><br><br>
        <div class="black-border">
            <h2 class="text-center">Teacher Referral Program</h2><br><br>
            <form>
                <div class="row">
                    <div class="col-sm-10 col-md-6 col-sm-offset-1 col-md-offset-3 site-block" data-toggle="animation-appear" data-animation-class="animation-fadeInRight">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Teacher / Institution Name : *</label>
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <input class="form-control" name="txt_teacherOrInstitutionName" placeholder="Teacher / Institution Name"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Contact Person Phone : *</label>
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <input class="form-control" name="txt_contactPersonPhone" placeholder="Contact Person Phone"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Contact Person Email : *</label>
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <input class="form-control" name="txt_contactPersonEmail" placeholder="Contact Person Email"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Subjects Taught : *</label>
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <input class="form-control" name="txt_subjectsTaught" placeholder="Subjects Taught"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Locality : *</label>
                            </div>
                            
                            <div class="col-md-6 form-group">
                                <input class="form-control" name="txt_locality" placeholder="Locality" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 site-block" data-toggle="animation-appear" data-animation-class="animation-fadeInRight">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Referred by - Name : </label>
                                <textarea class="form-control" name="txt_referredByName" placeholder=""></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>Referred by - Phone : </label>
                                <textarea class="form-control" name="txt_referredByName" placeholder=""></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>Referred by - Email : </label>
                                <textarea class="form-control" name="txt_referredByName" placeholder=""></textarea>
                            </div>
                        </div><br>
                        <div class="text-center">
                            <a href="#Terms&Conditions" class="btn btn-lg btn-primary center">T &amp; C</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div><br><br>
</section>

<section class="site-content site-section site-section-light" style="color: #888;">
    <div class="container">
        <div class="site-block">
            <h3 class="site-heading text-center">
                SELECT WHAT YOU WANT TO <strong>GET</strong>!<br><br>
                <button class="btn btn-success btn-lg">AMAZON</button>
                <button class="btn btn-success btn-lg">FLIPKART</button>
                <button class="btn btn-success btn-lg">SNAPDEAL</button>
                <button class="btn btn-success btn-lg">FREECHARGE</button>
                <button class="btn btn-success btn-lg">PAYTM</button>
            </h3>
        </div>
    </div>
</section>

<?php
get_footer('home');