function formhash(form, password) {
    // Create a new element input, this will be our hashed password field. 
    var p = document.createElement("input");
    
    // Add the new element to our form. 
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
    
    var pass = document.createElement("input"); 
    form.appendChild(pass);
    pass.name = "pass";
    pass.type = "hidden";
    pass.value = password.value;
 
 
    // Add the new element to our form. 
    
 
    // Make sure the plaintext password doesn't get sent. 
    password.value = "";
 
    // Finally submit the form.
    if($("#login_form").validationEngine('validate', {scroll: false})){
    form.submit();
    }
}
 
function regformhash(form,uid,lastname,email, password, conf) {
     // Check each field has a value
    if (uid.value == ''         || 
          email.value == ''     || 
          password.value == ''  || 
          conf.value == ''      ||
          lastname.value == ''
          ) {		
        alert('You must provide all the requested details. Please try again');
        return false;
    }

 
    // Check the username
 
    re = /^[1-9][0-9]*|0$/; 
    if(re.test(uid.value)) { 
        alert("Username must contain only letters, numbers and underscores. Please try again"); 
        form.username.focus();
        return false; 
    }
 
    // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
    if (password.value.length < 6) {
        alert('Passwords must be at least 6 characters long.  Please try again');
        form.password.focus();
        return false;
    }
 
    // At least one number, one lowercase and one uppercase letter 
    // At least six characters 
 
    //var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
    //if (!re.test(password.value)) {
    //    alert('Passwords must contain at least one number, one lowercase and one uppercase letter.  Please try again');
     //   return false;
    //}
 
    // Check password and confirmation are the same
	// We are not using this.
    // if (password.value != conf.value) {
        // alert('Your password and confirmation do not match. Please try again');
        // form.password.focus();
        // return false;
    // }
 
    // Create a new element input, this will be our hashed password field. 
    var p = document.createElement("input");
 
    // Add the new element to our form. 
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
 
    // Make sure the plaintext password doesn't get sent. 
    password.value = "";
    //Finally submit the form. 
    form.submit();
	console.log(form);
    return true;
}