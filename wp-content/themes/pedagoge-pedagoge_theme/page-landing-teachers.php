<?php
/*
Template Name: Landing Teachers
*/
?>

<?php
get_header('landing');

global $wpdb;
$sql="SELECT b.display_name,c.localities,a.fk_student_id,a.teacherinst_id,d.subject_name,d.subject_name_id FROM pdg_matched_query AS a
LEFT OUTER JOIN wp_users AS b ON a.fk_student_id=b.ID
LEFT OUTER JOIN pdg_query AS c ON a.fk_query_id=c.id
LEFT OUTER JOIN pdg_subject_name AS d ON c.subject=d.subject_name_id
ORDER BY a.m_id DESC LIMIT 1";
$res=$wpdb->get_results($sql);
//echo "<pre>"; print_r($res); echo "</pre>";
$tid=$res[0]->teacherinst_id;
$localities=$res[0]->localities;
$loc_arr=explode(",",$localities);
$subject_link=home_url().'/search/?pdg_subject='.$res[0]->subject_name.'&pdg_locality=/';


$sql_tech="SELECT a.user_id,a.profile_slug,b.name FROM pdg_teacher AS a LEFT OUTER JOIN user_data_teacher AS b ON a.user_id=b.new_user_id WHERE a.user_id='".$tid."'";
$res_tech=$wpdb->get_results($sql_tech);
if(!empty($res_tech)){ //Teacher
$profile_link=home_url().'/teacher/'.$res_tech[0]->profile_slug;
$ad_name=$res_tech[0]->name;
$profile_img=plugins_url()."/pedagoge_plugin/storage/uploads/images/".$res_tech[0]->user_id ."/profile.jpg";
}
else{ //Institute
$sql_tech="SELECT user_id,profile_slug,institute_name FROM pdg_institutes  WHERE user_id='".$tid."'";
$res_tech=$wpdb->get_results($sql_tech);
$profile_link=home_url().'/institute/'.$res_tech[0]->profile_slug;
$ad_name=$res_tech[0]->institute_name;
$profile_img=plugins_url()."/pedagoge_plugin/storage/uploads/images/". $res_tech[0]->userid ."/profile.jpg";
}


//-------------------Latest Query-------------------
$sql_query="SELECT a.*,b.display_name AS student_name,c.subject_name,d.subject_name_id,b.user_email,d.course_type_id,e.mobile_no,f.academic_board
	FROM pdg_query AS a
	LEFT OUTER JOIN wp_users AS b ON a.student_id=b.ID
	LEFT OUTER JOIN pdg_subject_name AS c ON a.subject=c.subject_name_id
	LEFT OUTER JOIN pdg_course_category AS d ON c.subject_name_id=d.subject_name_id
	LEFT OUTER JOIN pdg_user_info AS e ON a.student_id=e.user_id
	LEFT OUTER JOIN pdg_academic_board AS f ON a.board=f.academic_board_id WHERE a.approved=1 GROUP BY a.id ORDER BY a.id DESC LIMIT 10";
	
$res_query=$wpdb->get_results($sql_query);
//--------------------------------------------------


/************************ OUR STAR TEACHERS **************************/

$sql_star_teacher = "SELECT pst.teacher_id, udt.name, udt.subject_taught1, udt.subject_taught2, udt.educational_qualification, udt.educational_qualificationPG, pt.profile_slug FROM pdg_star_teacher AS pst 
LEFT JOIN wp_users AS wu ON pst.teacher_id = wu.ID
LEFT JOIN user_data_teacher AS udt ON pst.teacher_id = udt.new_user_id
LEFT JOIN pdg_teacher AS pt ON pst.teacher_id = pt.user_id
GROUP BY pst.teacher_id";
$res_star_teacher = $wpdb->get_results($sql_star_teacher);

/******************** END OF OUR STAR TEACHERS ********************/

/************************ OUR STAR INSTITUTES **************************/

$sql_star_institute = "SELECT pi.user_id, pi.profile_slug, pi.institute_name
FROM pdg_star_institute AS psi
LEFT JOIN pdg_institutes AS pi ON psi.inst_id = pi.user_id
GROUP BY psi.inst_id";
$res_star_institute = $wpdb->get_results($sql_star_institute);

//echo "<pre>"; print_r($sql_star_institute); echo "</pre>";

/************************ END OF OUR STAR INSTITUTES **************************/
?>
  <input id="page_id" type="hidden" value="2" />	    
    <div class="wrapper">
      <div class="main"> 
        <!-- page 1-->
        <section id="page1" class="header_wrapper">
            <div class="container clearfix for_model_set_bottom">
            	<div class="first-panel-vm">
                  <div class="row margin_top_60 for1920_margin1 text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">
                        <h1>Why Pedagoge</h1>
                    </div>
                  </div>
                  <div class="large-12 text-center hidden-lg hidden-md hidden-sm visible-xs">
                  	<div class="owl-carousel owl-theme">
                        <div class="item h1_heading post_match_learn">
                            <h2>Free Online Presence</h2>
                            <figure>
    							<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_1.png" alt="Why Pedagoge">
                                <figcaption>	
                                    <p>Free online profile for thousands of students to access.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="item h1_heading post_match_learn">
                            <h2>Personalized Assistance</h2>
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_2.png" alt="Why Pedagoge">
    
                                <figcaption>
                                    <p>Our team is always ready to help you over a call.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="item h1_heading post_match_learn">
                            <h2>Pay Only When You Receive Students</h2>
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_3.png" alt="Why Pedagoge">
                                <figcaption>
                                    <p>We ask for money only when we give you business.</p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                  </div>
                  <div class="rows text-center visible-lg visible-md visible-sm hidden-xs">
                    <div class="col-xs-12 col-md-4 col-sm-4 h1_heading post_match_learn">
                    	<h2>Free Online Presence</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_1.png" alt="Why Pedagoge">
                        	<figcaption>
                            	<p>Free online profile for thousands of students to access.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-4 col-sm-4 h1_heading post_match_learn">
                    	<h2>Personalized Assistance</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_2.png" alt="Why Pedagoge">
                        	<figcaption>
                            	<p>Our team is always ready to help you over a call.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-4 col-sm-4 h1_heading post_match_learn">
                    	<h2>Pay Only When You Receive Students</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_3.png" alt="Why Pedagoge">
                        	<figcaption>
                            	<p>We ask for money only when we give you business.</p>
                            </figcaption>
                        </figure>
                    </div>
                  </div>
                  <div class="row text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12 margin_top_20 h1_heading">
                        <h1>How It Works</h1>
                    </div>
                  </div>
                  <div class="large-12 text-center hidden-lg hidden-md hidden-sm visible-xs">
                  	<div class="owl-carousel owl-theme">
                        <div class="item h1_heading post_match_learn why_pedagoge">
                            <h2>Signup</h2>
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_4.png" alt="Why Pedagoge">
    
                                <figcaption>
                                    <p>Create a free profile and establish an online presence.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="item h1_heading post_match_learn why_pedagoge">
                            <h2>Respond To Query</h2>
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_5.png" alt="Why Pedagoge">
    
                                <figcaption>
                                    <p>As the requirements arise, respond with your availability.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="item h1_heading post_match_learn why_pedagoge">
                            <h2>Schedule</h2>
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_6.png" alt="Why Pedagoge">
    
                                <figcaption>
                                    <p>Interact with the students and schedule your classes.</p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="item h1_heading post_match_learn why_pedagoge">
                            <h2>Take Classes</h2>
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_7.png" alt="Why Pedagoge">
    
                                <figcaption>
                                    <p>You're all set. Let the teaching begin.</p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                  </div>
                  <div class="rows text-center visible-lg visible-md visible-sm hidden-xs">
                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">
                    	<h2>Signup</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_4.png" alt="Why Pedagoge">

                        	<figcaption>
                            	<p>Create a free profile and establish an online presence.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">
                    	<h2>Respond To Query</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_5.png" alt="Why Pedagoge">

                        	<figcaption>
                            	<p>As the requirements arise, respond with your availability.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">
                    	<h2>Schedule</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_6.png" alt="Why Pedagoge">

                        	<figcaption>
                            	<p>Interact with the students and schedule your classes.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">
                    	<h2>Take Classes</h2>
                        <figure>
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/t_item_7.png" alt="Why Pedagoge">

                        	<figcaption>
                            	<p>You're all set. Let the teaching begin.</p>
                            </figcaption>
                        </figure>
                    </div>
                  </div>
                  </div>
                  
            </div>
        </section>
        <!-- page 2 -->
        <section id="page2" class="header_wrapper">
            <div class="container clearfix for_model_set_bottom">
            	<div class="second-panel-vm">
                  <div class="row margin_top_60 for1920_margin text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">
                        <h1>What Students Are Searching For</h1>
                    </div>
                  </div>
                  
                  <div class="student_searching_wrapper">
                      <div class="row">
                            <div class="col-xs-12 col-md-12 col-sm-12 student_searching">
                            	<ul class="student_query">
					<?php
					$query_model = new ModelQuery();
					foreach($res_query AS $q){
					$localities = $query_model->get_q_locality($q->localities);	
				        ?>
                                	<li>
                                    	<div class="row">
                                        	<div class="col-xs-6 col-md-4 col-sm-4 text-uppercase color_white tutor_home">
                                            	<h2><?php echo $q->subject_name; ?> Home-Tutor In</h2>
                                            </div>
                                            <div class="col-xs-6 col-md-3 col-sm-3 pull-right text-uppercase text-right show_interest">
                                            	<form method="post">
                                                    
                                                    <a href="javascript:void(0);" class="sinterest">Show Interest</a>
                                                    <button class="get_land_query" type="button" q_id="<?php echo $q->id; ?>">Expand</button>
                                                </form>
                                            </div>
                                            <div class="col-xs-12 col-md-5 col-sm-5 tutor_place">
                                            	<ul class="choose_place">
						  <?php foreach($localities AS $loc){ ?>	
                                                    <li><?php echo $loc; ?></li>
						  <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
				<?php } ?>	
                                </ul>
				
                                <form method="post">
                                	<a href="javascript:void(0);" class="see_more">See More</a>
                                </form>
                            </div>
                      </div>
                  </div>
                  </div>
                  
            </div>
        </section>
        <!-- page 3-->
        <section id="page3">
            <div class="container clearfix for_model_set_bottom">
            <div class="second-panel-vm">
              <div class="row margin_top_60 for1920_margin1 text-center">
                  <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">
                      <h1>Our Star Teachers</h1>
                  </div>
               </div>
               
               <div class="large-12 text-center">
                    <div class="owl-carousel owl-theme">
                        <?php
                        //echo "<pre>"; print_r($res_star_teacher); echo "</pre>";
                        foreach ($res_star_teacher as $key => $value) {
                            $teacher_profile_link = home_url().'/teacher/'.$value->profile_slug;
                            $teacher_name = $value->name;
                            $teacher_profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/".$value->teacher_id ."/profile.jpg";
                            //$qualification = ($value->educational_qualificationPG != ""?$value->educational_qualificationPG:$value->educational_qualification);
                            $qualification = "";
                            //subject_taught1
                            //subject_taught2
                            $sub_teacher_taught = "";
                            $sub_teacher_taught1 = "";
                            $sql_subject = "SELECT subjects FROM view_pdg_teacher_extented WHERE user_id ='".$value->teacher_id."'";
                            $res_teacher_sub = $wpdb->get_results($sql_subject);
                            $sub_teacher_taught1 = $res_teacher_sub[0]->subjects;

                            $a_sub_teacher_taught = explode(",",$sub_teacher_taught1);

                            for($i = 0; $i<2; $i++){
                                if($i == 0){
                                    $sub_teacher_taught = $a_sub_teacher_taught[$i];
                                }
                                else{
                                    if($a_sub_teacher_taught[$i] != "")
                                    {
                                        $sub_teacher_taught .= ','.$a_sub_teacher_taught[$i];    
                                    }
                                }
                            }

                            if($sub_teacher_taught1 == "NULL"){
                                $sub_teacher_taught = "";
                            }
                        ?>
                        <div class="item h1_heading post_match_learn student_image">
                            <a href="<?= $teacher_profile_link;?>">
                            <figure>
                            <div class="svg_holder no_padding">
                                <img src="<?= $teacher_profile_img;?>" alt="<?= $teacher_name;?>">
                            </div>
                            <figcaption>
                                <h3><?= $teacher_name;?> <span class="text-uppercase"><?= $sub_teacher_taught;?></span></h3>
                            </figcaption>
                            </figure>
                            </a>
                        </div>
                        <?php
                        }
                        ?>
				        
                  </div>
				  </div>
                  
               <div class="row text-center">
                  <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">
                      <h1>Our Star Institutes</h1>
                  </div>
               </div>
               
               <div class="large-12 text-center">
				<div class="owl-carousel owl-theme">
                    <?php
                    foreach ($res_star_institute as $key => $ins_value) {

                        $institute_profile_link = home_url().'/institute/'.$ins_value->profile_slug;
                        $institute_name = $ins_value->institute_name;
                        $institute_profile_img = plugins_url()."/pedagoge_plugin/storage/uploads/images/".$ins_value->user_id ."/profile.jpg";

                        $sub_institute_taught = "";
                        $sub_institute_taught1 = "";
                        $sql_subject_ins = "SELECT subjects FROM view_pdg_institutes_extented WHERE user_id ='".$ins_value->user_id."'";
                        $res_institute_sub = $wpdb->get_results($sql_subject_ins);
                        $sub_institute_taught1 = $res_institute_sub[0]->subjects;
                        
                        $a_sub_institute_taught = explode(",",$sub_institute_taught1);

                        for($j = 0; $j<2; $j++){
                            if($j == 0){
                                $sub_institute_taught = $a_sub_institute_taught[$j];
                            }
                            else{
                                if($a_sub_institute_taught[$j] != "")
                                {
                                    $sub_institute_taught .= ','.$a_sub_institute_taught[$j];
                                }
                            }
                        }

                        if($sub_institute_taught1 == "NULL"){
                            $sub_institute_taught = "";
                        }
                    ?>
                    <div class="item h1_heading post_match_learn student_image">
                        <a href="<?= $institute_profile_link;?>">
                        <figure>
                            <div class="svg_holder no_padding">
                                <img src="<?= $institute_profile_img;?>" alt="<?= $institute_name;?>">
                            </div>

                            <figcaption>
                                <h3><?= $institute_name;?> <span class="text-uppercase"><?= $sub_institute_taught;?></span></h3>
                            </figcaption>
                        </figure>
                        </a>
                    </div>
                    <?php
                    }
                    ?>
				    
                  </div>
		          </div>
               </div>
               
               </div>
        </section>
        <!-- page 4-->
        <section id="page4">
           <div class="container clearfix for_model_set_bottom">
           		<div class="row only_footer_margin for1920_margin1 text-center">
                  <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">
                      <h1>Latest Matches</h1>
                  </div>
               </div>
               <div class="row">
                   <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">

                      <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-12 match_decoration">
                                <div class="logo_holder">
                                    <img src="<?php echo $profile_img; ?>" alt="<?php echo $ad_name; ?>">
                                </div>
                            </div>
                            <?php
                      if(!empty($res)){
                      ?>
                            <div class="col-md-9 col-sm-8 col-xs-12 match_decoration">
                                <p class="margin_top_40"><?php echo $res[0]->display_name; ?> just matched with <a href="<?php echo $profile_link; ?>" class="text-uppercase green_color"><?php echo $ad_name; ?></a> for <a href="<?php echo $subject_link; ?>" class="text-uppercase green_color"><?php echo $res[0]->subject_name; ?></a> classes in
				<?php
				$comma='';
				if(count($loc_arr)>1){
				$comma=',';		
				}
				$i=1;
				foreach($loc_arr as $loc_id){
				$sql_loc="SELECT locality  FROM pdg_locality WHERE locality_id='".$loc_id."'";
				$res_loc=$wpdb->get_results($sql_loc);
				?>
				<?php if($i!=1){echo $comma;} ?><a href="<?php echo home_url().'/search/?pdg_subject=&pdg_locality='.$loc_id; ?>" class="text-uppercase green_color"><?php echo $res_loc[0]->locality; ?></a>
				<?php $i++;} ?>
				</p>
                            </div>
                            <?php
                        }
                            ?>
                      </div>
                      
                      <div class="row">
                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 text-center h1_heading match_decoration">
                              <h1>An Easy Access To Hundreds Of Students</h1>
                              <!--<a href="<?php echo home_url('/login'); ?>" class="register_now">Register Now</a>-->
                              <?php
                              if(!is_user_logged_in())
                              {
                              ?>
                              <a href="<?php echo home_url('/login'); ?>" class="register_now">Register Now</a>
                              <?php
                              }
                              ?>
                          </div>
                       </div>
                       
                       
                  </div>
              </div>

<!--  Modal  -->
<?php
get_footer('landing');
?>