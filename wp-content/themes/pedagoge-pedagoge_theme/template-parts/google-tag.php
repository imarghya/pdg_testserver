<?php
$is_production_environment = false;
$server_name = $_SERVER['SERVER_NAME'];
switch ( $server_name ) {
	case 'www.pedagoge.com':
	//case 'www.pedagog.in':
	//case 'www.pedagoge.net':
	case 'dev.pedagoge.com':
	case 'test.pedagoge.com':
	$is_production_environment = true;
		break;
}

if ( $is_production_environment ):
	?>
	<!-- Nudgespot starts-->
	<script type="text/javascript">(function (d, n) {
			var s, a, p;
			s = document.createElement("script");
			s.type = "text/javascript";
			s.async = true;
			s.src = (document.location.protocol === "https:" ? "https:" : "http:") + "//cdn.nudgespot.com" + "/nudgespot.js";
			a = document.getElementsByTagName("script");
			p = a[a.length - 1];
			p.parentNode.insertBefore(s, p.nextSibling);
			window.nudgespot = n;
			n.init = function (t) {
				function f(n, m) {
					var a = m.split('.');
					2 == a.length && (n = n[a[0]], m = a[1]);
					n[m] = function () {
						n.push([m].concat(Array.prototype.slice.call(arguments, 0)))
					}
				}

				n._version = 0.1;
				n._globals = [t];
				n.people = n.people || [];
				n.params = n.params || [];
				m = "track register unregister identify set_config people.delete people.create people.update people.create_property people.tag people.remove_Tag".split(" ");
				for (var i = 0; i < m.length; i++)f(n, m[i])
			}
		})(document, window.nudgespot || []);
		nudgespot.init("391e860c1d5146fef77e58c1b48270e0");</script>
		
	<!-- Nudgespot ends-->

	<!-- Social Login/Logout  -->
	<meta name="google-signin-client_id" content="1053909614292-15tc9n68r82v0ukhbrv17shfh1n0vch5.apps.googleusercontent.com">
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<div class="g-signin2" data-onsuccess="" style="display: none;"></div>

	<script type="text/javascript">
		// Load the SDK asynchronously
		/*(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		//.com 144701779268408
		//.net 1464551020225382

		window.fbAsyncInit = function () {
			FB.init({
				appId: '144701779268408',
				cookie: true,  // enable cookies to allow the server to access
				// the session
				xfbml: true,  // parse social plugins on this page
				version: 'v2.5' // use graph api version 2.5
			});
			jQuery('#cmd_login_facebook').prop( 'disabled' , false).removeClass('disabled');
			
			// Checking to make sure the popup does not get blocked
			FB.getLoginStatus( function ( response ) {				
			} );
		};*/
		
		/**
		 * Function to be called at the logout action to logout from google or facebook login session. 
		 */
		function fn_logout_user_from_all_services() {
			/**
			 * check the FB login status from the API status and 
			 * if user is loggin via facebook then callback the function facebook_logout 
			 */
			//FB.getLoginStatus(facebook_logout);
			
			/**
			 * Since this is a common function, call google_signout function as well;
			 * because we do not know in advance whether user has loggedin via google or facebook. 
			 */
			google_sign_out();
		}
		
		/**
		 * If a user has loggedin via facebook then call this function to logout from the website. 
		 */
		function facebook_logout(response) {
			/*if (!response.session) {	// if user has no fb login session then do nothing..
				return;
			}
			
			FB.logout(); // else log him out from the FB login session.*/
		}
		
		
		function google_on_load() {
			gapi.load('auth2', function () {
				gapi.auth2.init();
				//gapi.auth2.getAuthInstance();
			});
		}
		
		/**
		 * If a user has loggedin via google then call this function to logout from the website. 
		 */
		function google_sign_out() {
			var auth2 = gapi.auth2.getAuthInstance();
			auth2.signOut().then(function () {
				//console.log('User signed out.');
			});
		}
		
		$(document).ready(function () {
			$("#pdg_menu_logout").click(function (event) {
				event.preventDefault();
				var $logout_url = $(this).prop('href');
				fn_logout_user_from_all_services();
				window.location.href = $logout_url;
			});
		});
		
	</script>

	<?php
endif;

if ( ! $is_production_environment ):
	?>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#pdg_menu_logout").click(function (event) {
				event.preventDefault();
				var $logout_url = $(this).prop('href');
				window.location.href = $logout_url;
			});
		});
	</script>
<?php endif; ?>