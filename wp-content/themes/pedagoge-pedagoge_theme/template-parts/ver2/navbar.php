<?php
global $wp_query, $post;

$current_page_slug = '';
/**
 * Current Page URL
 */
$pdg_current_page_url = "http" . ( ( $_SERVER['SERVER_PORT'] == 443 ) ? "s://" : "://" ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$wordpress_home_url = trim( home_url() );

/**
 * Removing wordpress home url part from current url so that we can get the url path to work with.
 * This technique had to be implemented because earlier implementation did not work for localhost.
 */
$pdg_replaced_url = str_replace( $wordpress_home_url, '', $pdg_current_page_url );

/**
 * Seperate the url to path and query and just take url path.
 */
$pdg_processed_url_path = parse_url( $pdg_replaced_url, PHP_URL_PATH );

/*
 * Description: Exclude any elements out of the header
 * If the template include is coming from plugins then '$post' wont work
 * so first query '$wp_query->virtual_page' and if it returns '$slug' then its a plugin else
 * it is the theme which is trying to inclue the template file.
 * '$excludeheader' array contains the slug names of whose certin element has to be excluded.
*/
$slug = "";
if ( isset( $wp_query->virtual_page ) ) {
	$slug = trim( $wp_query->virtual_page->getUrl(), '/' );
}

if ( $slug == "" && isset( $post ) ) {
	$current_post = get_post( $post );
	if ( ! empty( $current_post ) ) {
		$slug = $current_post->post_name;
	}
}

$allowFixedSearch = false;
$allowSearchIcon = false;
$allowColMdLgPedagogeLogoInPrimaryBar = " visible-md visible-lg";
$disallowColMdLgPedagogeLogoInPrimaryBar = "";

$excludeheader = array ( 'login', 'register', 'signup', 'reset', 'about-us', 'workshop', 'faq');

if ( $slug != "" && ! in_array( $slug, $excludeheader ) ) {
	$allowFixedSearch = true;
	$allowSearchIcon = true;
	$allowColMdLgPedagogeLogoInPrimaryBar = "";
} elseif ( $slug == "" ) {
	$allowFixedSearch = true;
	$allowSearchIcon = true;
	$allowColMdLgPedagogeLogoInPrimaryBar = "";
}


$home_url_text_class = "hide";
if ( $slug == "about-us" || $slug == "faq"){
	$home_url_text_class = "hidden-xs visible-sm visible-md visible-lg ";
	$disallowColMdLgPedagogeLogoInPrimaryBar = " hidden-sm hidden-md hidden-lg";
}
?>
<!--Site Header-->
<header>
	<nav class="navbar navbar-default navbar-primary bg-color navbar-fixed-top">
		<div class="navbar-header">
			<div class="navbar-toggle-wrapper">
				<!-- menu icon-->
				<button type="button" class="navbar-toggle valign-wrapper" data-toggle="collapse"
				        data-target=".navbar-responsive-collapse">
					<span class="material-icons valign">&#xE5D2;</span>
				</button>
				<?php if ( $allowFixedSearch ) {
					?>
					<!--search icon-->
					<button type="button" class="navbar-toggle start_search valign-wrapper"
					        data-toggle="collapse" id="gtm_Search">
						<span class="material-icons valign">&#xE8B6;</span>
					</button>
				<?php } ?>
				<a class="navbar-brand visible-xs visible-sm <?= $allowColMdLgPedagogeLogoInPrimaryBar ?>" href="<?= home_url() ?>">
					<span class="<?= $home_url_text_class ?> text-white"><!--<i class="material-icons md-24 inline">&#xE88A;</i>&nbsp;--><span class="inline">Home</span></span>
					<img
							class=" <?= $disallowColMdLgPedagogeLogoInPrimaryBar ?>"
							src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
							onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='valign responsive-png'"></a>
			</div>
		</div>
		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<?php get_template_part( 'template-parts/ver2/menuitems' ); ?>
		</div>
	</nav>
	<?php
	if ( $allowFixedSearch ) {
		?>
		<nav class="navbar navbar-default navbar-secondary hidden-xs bg-color navbar-fixed-top hide_on_search">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand hidden-sm" href="<?= home_url() ?>">
						<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
								onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='valign responsive-png'">
					</a>
				</div>
			</div>
		</nav>
		<?php
	}
	?>
</header>
<!--/Site Header-->