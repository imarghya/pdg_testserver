<?php
global $wp_query;
$menu_for_logged_in_user = '';
$str_current_user_full_name = '';
$isLoggedIn = false;
if ( is_user_logged_in() ) {
	$isLoggedIn = true;
	$current_user = wp_get_current_user();
	$str_current_user_full_name = '';
	$first_name = trim( $current_user->user_firstname );
	$last_name = trim( $current_user->user_lastname );
	$display_name = trim( $current_user->display_name );
	$str_current_user_name = trim( $current_user->user_login );
	
	$role='';//administrator
	if ( !empty( $current_user->roles ) && is_array( $current_user->roles ) ) {
	    foreach ( $current_user->roles as $role );	
	}
	//echo $role;
	
	if ( ! empty( $display_name ) ) {
		$str_current_user_full_name = $display_name;
	} else if ( ! empty( $first_name ) ) {
		$str_current_user_full_name = $first_name . ' ' . $last_name;
	} else {
		$str_current_user_full_name = $str_current_user_name;
	}
	if ( current_user_can( 'manage_options' )) {
		$menu_for_logged_in_user = '<li><a href="' . home_url( '/dashboard' ) . '">Control Panel</a></li> 
		<li>
            <a href="'.home_url('/wp-admin').'"><i class="fa fa-tachometer" aria-hidden="true"></i> Wordpress Dashboard</a>
        </li>';
	}else if (current_user_can('pdg_cap_cp_access')){
		$menu_for_logged_in_user = '<li><a href="' . home_url( '/dashboard' ) . '">Control Panel</a></li> 
		<li>';
	}else if ($role=='finance'){
	$menu_for_logged_in_user = '<li><a href="' . home_url( '/dashboard' ) . '">Control Panel</a></li> 
	<li>';
	}else {
		$dashboard='';
		if($role=='teacher'){
		$teacher_model = new ModelTeacher();
		$teacher_slug= $teacher_model->get_teacher_slug($current_user->ID);
		$user_data_teacher=$teacher_model->get_user_data_teacher($current_user->ID);
		//print_r($user_data_teacher);
		if($user_data_teacher[0]->approved=='yes'){
		$dashboard='<li><a href="'.home_url( '/teacherdashboard' ).'">Dashboard</a></li>';	
		}
		$dashboard .='<li><a href="'.home_url( '/teacher/'.$teacher_slug ).'">Profile</a></li>';
		}
		else{
		$dashboard.='<li><a href="'.home_url( '/memberdashboard/?query_type=active' ).'">Dashboard</a></li>';	
		}
		$menu_for_logged_in_user = $dashboard.'
			<li><a href="'.home_url( '/reset' ).'">Change Password</a></li>
		';
	}
}
$slug = "";
if ( isset( $wp_query->virtual_page ) ) {
	$slug = trim( $wp_query->virtual_page->getUrl(), '/' );
}
$allowLoginUrl = false;
$allowHomeURL = true;
$excludeheader = array ( 'login', 'register', 'signup', 'reset' );
if ( $slug != "" && ! in_array( $slug, $excludeheader ) ) {
	$allowLoginUrl = true;
	$allowHomeURL = false;
} elseif ( $slug == "" ) {
	$allowLoginUrl = true;
	$allowHomeURL = false;
}
?>
<ul class="nav navbar-nav navbar-right padding-fix">
	<?php
	if ( $allowHomeURL ) {
		?>
		<li><a href="<?= home_url( '' ) ?>" title="Home">Home</a></li>
		<?php
	}
	?>
	<li><a href="<?= home_url( 'about-us' ) ?>" id="gtm_Aboutus" title="About Us">About Us</a></li>
	<!-- <li><a href="<?= home_url( 'workshop' ) ?>" id="gtm_workshop" title="Workshop">Workshop</a></li> -->
	<li><a href="<?= home_url( 'institution' ) ?>" id="gtm_School_institution" title="School/Institution">School/Institution</a></li>
	<li><a href="http://blog.pedagoge.com" id="gtm_Blog" title="Blog">Blog</a></li>
	<li><a href="<?= home_url( 'faq' ) ?>" id="gtm_faq" title="Frequently Asked Questions">FAQ's</a></li>
	<li><a href="<?= home_url( 'contact-us' ) ?>" id="gtm_Contactus" title="Contact Us">Contact Us</a></li>

	<?php if ( ! $isLoggedIn ) {
		if ( $allowLoginUrl ) {
			?>
			<li class="login">
				<a href="<?= home_url( 'login' ) ?>?nexturl=/" id="gtm_Loginregister" title="Login/Register">Login/Register</a>
			</li>
			<?php
		}
	} else { ?>
		<li class="dropdown login">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
			   aria-haspopup="true"
			   aria-expanded="false"><i class="material-icons">&#xE853;</i>&nbsp;<?= $str_current_user_full_name; ?>
				<span class="caret"></span></a>
			<ul class="dropdown-menu">
				<?= $menu_for_logged_in_user; ?>				
				<li><a href="<?php echo wp_logout_url(); ?>" id="pdg_menu_logout">Log out</a></li>
			</ul>
		</li>
	<?php } ?>
</ul>