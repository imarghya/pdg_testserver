<h4 class="footer-heading text-center">Follow Us</h4>
<ul class="list-inline text-center">
	<li><a href="https://www.facebook.com/pedagoge0/" target="_blank"><img
				src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/s-m-d/f.png" title="facebook"></a>
	</li>
	<li><a href="https://twitter.com/pedagogebaba" target="_blank"><img
				src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/s-m-d/t.png" title="twitter"></a>
	</li>
	<li><a href="https://www.linkedin.com/company/pedagoge" target="_blank"><img
				src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/s-m-d/l.png" title="linkedin"></a></li>
	<li><a href="https://www.instagram.com/pedagogebaba/" target="_blank"><img
				src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/s-m-d/i.png" title="instagram"></a></li>
</ul>