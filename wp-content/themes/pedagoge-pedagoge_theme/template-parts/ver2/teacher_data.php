<?php
global $wpdb;
$teacher_data = json_encode( array_map( 'teacher_data_filter', PDGManageCache::fn_load_cache( 'teacher_names_data', true ) ) );
function teacher_data_filter( $param ) {
	if ( trim( $param ) !== "" ) {
		return array (
			'teacher' => $param,
		);
	}

	return false;
}

wp_footer();
?>
<div class="td_ hide"><?php echo $teacher_data; ?></div>