<!--Modal request callback-->

<div id="request-callback-modal" data-callback-name="search_callback" class="modal fade callback_init" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Request Callback</h4>
			</div>
			<form class="bs-component" id="search_callback">
				<div class="modal-body">
					<div class="col-xs-12">
						<div class="show_result"></div>
						<h5>Contact us: +91-9073922835</h5>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_teachername">Teacher's Name</label>
							<input class="form-control" readonly="readonly" id="callback_teachername" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating hide">
							<label class="control-label" for="callback_url">URL</label>
							<input class="form-control" id="callback_url" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_sub">Subject*</label>
							<input class="form-control validate[required]" id="callback_sub" type="text"
							       autocomplete="off">
							<span class="help-block">Enter subjects and separate using commas</span>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_loc">Locality*</label>
							<input class="form-control validate[required]" id="callback_loc" type="text"
							       autocomplete="off">
							<span class="help-block">Enter multiple localities using commas</span>
						</div>
						<br/>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_tut_loc">Place*</label>
							<select id="callback_tut_loc" class="form-control validate[required]">
								<option value="teacher">Teacher's Place</option>
								<option value="student">Student's Place</option>
								<option value="institute">Institute</option>
							</select>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_pref_mode_comm">Preferred mode of
								communication*</label>
							<div class="checkbox">
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="email"> Email </label>&nbsp;
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="whatsapp"> Whatsapp </label>&nbsp;
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="message"> Message </label>&nbsp;
								<label> <input type="checkbox" class="validate[minCheckbox[1]] pref_mode_comm"
								               name="group[group_pref_mode_comm]" value="phone"> Phone </label>
							</div>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_pref_time_comm">Preferred time of
								communication*</label>
							<div class="checkbox">
								<label> <input type="checkbox" value="1" class="validate[minCheckbox[1]] pref_time_comm"
								               name="group[group_pref_time_comm]"> 10 - 12 PM </label>&nbsp; <label>
									<input type="checkbox" value="2" class="validate[minCheckbox[1]] pref_time_comm"
									       name="group[group_pref_time_comm]"> 12 - 3 PM </label>&nbsp; <label>
									<input type="checkbox" value="3" class="validate[minCheckbox[1]] pref_time_comm"
									       name="group[group_pref_time_comm]"> 3 - 6 PM </label>&nbsp; <label>
									<input type="checkbox" value="4" class="validate[minCheckbox[1]] pref_time_comm"
									       name="group[group_pref_time_comm]"> 6 - 9 PM </label>
							</div>
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_name">Name*</label>
							<input class="form-control validate[required]" id="callback_name" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_phn">Phone Number*</label>
							<input class="form-control validate[required, custom[phone]]" id="callback_phn" type="text"
							       autocomplete="off">
						</div>
						<div class="form-group label-floating">
							<label class="control-label" for="callback_email">Email ID</label>
							<input class="form-control validate[custom[email]]" id="callback_email" type="text"
							       autocomplete="off">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary text-bold submit" data-loading-text="Processing...">
						Submit
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--/Modal request callback-->