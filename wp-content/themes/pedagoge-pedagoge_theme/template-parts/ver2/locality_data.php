<?php
global $wpdb;
$locality_data = json_encode( array_map( 'locality_data_filter', PDGManageCache::fn_load_cache( 'registered_locality', true ) ) );
function locality_data_filter( $param ) {
	return array (
		'locality_id' => $param['locality_id'],
		'locality'    => $param['locality'],
	);
}

wp_footer();
?>
<div class="ld_ hide"><?php echo $locality_data; ?></div>