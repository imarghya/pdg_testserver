<nav id="header" class="navbar navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            
            <a class="navbar-brand" href="#home">
				<img style="height:40px; margin-top:-10px;" class="img-responsive" alt="Pedagoge Logo" src="http://pedagoge.com/logo.png">
			</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo home_url('/'); ?>">Home</a></li>                  						
                <li><a href="<?php echo home_url('/#contact'); ?>">Contact</a></li>
				<li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm" >Log In</a></li>                    
				<li><a href="" data-toggle="modal" data-target=".bs-example-modal-sm1">Sign Up</a></li>				
			</ul>		
        </div>
	</div>
</nav>