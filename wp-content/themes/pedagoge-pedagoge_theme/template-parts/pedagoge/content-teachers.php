<?php
/**
 * Template part for displaying content of teacher's pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pedagoge_theme
 */

?>

<section id="section-1">		
	<div class="container" id="howitworks">               			
		<div class="row">
			<div class="col-md-12">
				<center><h1 style="margin-bottom:-40px;">How to start teaching?</h1></center>
				<section id="cd-timeline" class="cd-container">
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-success">
							<i class="fa fa-list-alt"></i>
						</div> <!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>LIST</h2>
							<p>Whether you offer math tuition or bakery class, we list your centre free of cost. Once you're ready to start welcoming students, you can publish your listing for the world to see. If you believe you're good, let the world know.<br><br>
							<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg">Learn more</button>	</p>				
						</div> <!-- cd-timeline-content -->
					</div> <!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-danger">
							<i class="fa fa-sitemap"></i>
						</div> <!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>CONNECT</h2>
							<p>Once a booking request is made, you will receive an e-mail informing you about the request. You can connect with the student to understand his/her needs or requirements and to send out announcements through our platform.</p>
						</div> <!-- cd-timeline-content -->
					</div> <!-- cd-timeline-block -->

					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-info">
							<i class="fa fa-clipboard "></i>
						</div> <!-- cd-timeline-img -->

						<div class="cd-timeline-content">
							<h2>TEACH</h2>
							<p>Once you have a confirmed reservation and payment is secured, it's time to prepare for your students.<br>
							<button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-lg1">Learn more</button>	</p>
						</div> <!-- cd-timeline-content -->
					</div> <!-- cd-timeline-block -->
				</section> <!-- cd-timeline -->
			</div>
		</div><!-- Row -->
	</div>
</section>

<div class=" background-image-centered" style="color:inherit; background-color:#20ac94; background-image: url(http://www.carbonite.com/globalassets/images-breakers/sphere-center1.png);">
    <div class="container-fluid gray-headline-text-block">
        <div class="container">
            <div class="one-column">
                <div class="row">
                    <div class="col-md-12 content-overlay">                        
                        <center>
							<br>
							<h2>We seek reviews and comparisons before purchasing a product, why not do the same before betting on our source of knowledge?</h2>
							<br>
						</center>                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<section id="section-2">	
    <div class="container" id="benefits">    	
        <div class="row features-list">        	
			 <center> 
			 	<h2>Why Pedagoge for Teaching?</h2>
			 </center>			
			<br>			
            <div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
                <div class="feature-icon">
                    <i class="fa fa-laptop"></i>
                </div>                
               <center> 
               		<h2>Free listing = Free Publicity</h2>
                	<p>Creating a listing page for any coaching centre or as an individual teacher is free. There is no harm is getting your name out there and if nothing else using this for free publicity!</p>
                </center>                
            </div>            
            <div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">            	
                <div class="feature-icon">
                    <i class="fa fa-comments"></i>
                </div>                
                <center>
                	<h2>Communication is easy and effective</h2>
                	<p>We save time and effort of the teachers who connect with students on our platform. This leads to more effective screening of students and reduces the excess effort taken by the teachers.</p>
                </center>                
            </div>            
		</div>		
		<div class="row features-list">			
            <div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
                <div class="feature-icon">
                    <i class="fa fa-gamepad"></i>
                </div>
                <center><h2>You're always in control</h2>
                	<p>You set the price for your listing, your availability, batch strength, class pictures and booking requirements for your students. You can also set custom prices to earn more during demand hike or crash courses.</p>
                </center>
            </div>            
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
                <div class="feature-icon">
                    <i class="fa fa-umbrella"></i>
                </div>
                <center><h2>Personal data protection</h2>
                    <p>Your contact information will not be shared with the students until they've committed to attending your class. Potential students will only be allowed access to those details that are needed to make a decision. We take data protection VERY seriously!
						<br><br><br>
					</p>
                </center>
            </div>		
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
                <div class="feature-icon">
                    <i class="fa fa-heartbeat"></i>
                </div>
                <center>
                	<h2>Never-ending support</h2>
                	<p>Pedagoge's support team will always be around to help you with any questions or concerns that you might have. Give us a chance to show us we care! You will not be disappointed.</p>
                </center>
            </div>			
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">                
                <div class="feature-icon">
                    <i class="fa fa-graduation-cap"></i>
                </div>                
                <center>
                	<h2>Know your student</h2>
                	<p>A dedicated student is a teacher's delight! Get the facility to know a student on a personal level and bond before the learning experience has actually commenced.</p>
                </center>
            </div>            
        </div>
    </div>
</section>

<div class="background-image-centered" style="color:inherit; background-color:#20ac94; background-image: url(http://www.carbonite.com/globalassets/images-breakers/sphere-center1.png);">
    <div class="container-fluid gray-headline-text-block">
        <div class="container">
            <div class="one-column">
                <div class="row">
                    <div class="col-md-12 content-overlay">                        
                        <center>
	                        <br>
							<h2> Pedagoge is an online platform for all individuals who want to reach out and connect with private coaches, trainers, tutors and instructors.</h2>
	                        <br>
                    	</center>                     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
	
<?php get_template_part( 'template-parts/pedagoge/content', 'contact_form' ); ?>