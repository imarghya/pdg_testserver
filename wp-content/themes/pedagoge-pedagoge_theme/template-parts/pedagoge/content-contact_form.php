<div id="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 wow rotateInUpLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">                
                <a href="../#contact" class="btn btn-success btn-lg btn-rounded contact-button hide_msg"><i class="fa fa-envelope-o"></i></a>                
                <h2 class="hide_msg">Let's keep in touch</h2>                
                <form id="mail_send" method="post" class="m-t-md hide_msg" action="">                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <input id="name_mail" type="text" style="border-radius: 0px;" class="validate[required] form-control input-lg contact-name" placeholder="Name">
                            </div>
                            <div class="col-sm-6">      
                                <input id="email_send" type="email" style="    border-radius: 0px;" class=" validate[required] form-control input-lg" placeholder="Email">
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <input id="subject_mail" type="text"  style="border-radius: 0px;"class="validate[required] form-control input-lg" placeholder="Subject">
                    </div>                    
                    <div class="form-group">
                        <textarea id="message" class="validate[required] form-control" style="    border-radius: 0px;" rows="4=6" placeholder="Message"></textarea>
                    </div>                    
                    <button type="button" class="btn btn-default btn-lg" onclick="send_mail()">Send Message</button>                
                </form>				
				<a href="#contact" class="btn btn-success btn-lg btn-rounded contact-button succ_msg" style="display: none; width: 200px !important; height: 200px !important; line-height: 198px; font-size: 51px;"><i class="fa fa-envelope-o"></i></a>				
				<div class="alert alert-success succ_msg" style="display: none; margin-top: 6%;">
					<strong><center>Thank you for getting in touch with us.<br> We shall reply to you shortly!</center></strong>
				</div>
            </div>
        </div>
    </div>
</div>