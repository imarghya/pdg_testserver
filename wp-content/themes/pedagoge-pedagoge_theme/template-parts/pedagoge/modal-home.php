<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'times', 'true'])"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myLargeModalLabel">DISCOVER</h4>
			</div>
			<div class="modal-body">
				<h3>Narrow down search using filters</h3>
				We display images of listings, teacher profiles and reviews to help you make informed decisions when considering a teacher. We've also created search filters so you can narrow your results by type of class price, location, extra features including amenities like AC, batch size etc. For example, if you're looking for private tuition at home, select "Student's Place"; or if you're looking for coaching at teacher's place or a coaching/training centre, go ahead and select the "Teacher's Place" or "Coaching Institute" filter.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Close', 'true'])">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'time', 'true'])"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myLargeModalLabel">RESERVE</h4>
			</div>
			<div class="modal-body">
				<h3>Research and Contact Preferred Teacher</h3>
				If not finished already, complete your profile to get access to booking the teacher through us and receive a welcome benefit once the registration is complete.
				<h3>Payment and confirmation</h3>
				The fee to be paid via Pedagoge will be mentioned in the billing details. One shall receive their benefits with it. We'll ask for your payment details so that we can hold your reservation. Once business is taken care of, you can get started with classes. We offer demo classes too! <br><br>
				The payment option shall be at the discretion of the teacher. With the payment cleared, you receive your benefits. Find a 'Welcome to the Family' mail waiting in your inbox that includes your receipt, class details and best wishes for a great experience. If you don't hear back within the defined time period, your request automatically expires.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Close', 'true'])">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'times', 'true'])"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myLargeModalLabel">JOIN</h4>
			</div>
			<div class="modal-body">
				<h3>Before you get there</h3>
				Message your chosen teacher to ensure attendance and coordinate your arrival details. We'll let you know and confirm your class details, receipt, and teacher contact information.
				<h3>After your class, help us out!</h3>
				Write a review about the teacher or the institute and in some cases both, so that other students around you can learn through your experiences. If you liked what you saw on Pedagoge, do spread the word! Ask us about our referral program.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="_gaq.push(['_trackEvent', 'button', 'clicked', 'Close', 'true'])">Close</button>
			</div>
		</div>
	</div>
</div>

<div style="top: 8px; left: 100px; position: fixed; color: green; z-index: 999; height: 40px; ">
	<img src="http://pedagoge.com/logo.png" class="invertmobileview1" style="height:50px; ">
</div>

<div id="sroll_to_top_now" style="z-index: 999;">
	<a href="javascript:" id="return-to-top" style="z-index: 999;"><i class="fa fa-chevron-up" style="z-index: 999;"></i></a>
</div>