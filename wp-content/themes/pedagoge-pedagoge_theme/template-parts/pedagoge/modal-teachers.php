<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">LIST</h4>
            </div>
            <div class="modal-body">
					<h3>Description</h3> 
					Students often search for teachers that meet specific needs, so accurately describing your teaching methodology will help attract the kind of students that are best for you. Provide relevant information about the infrastructure of your centre - washrooms, ACs, Smart classes, include helpful details about the character and experience of the teacher(s), batch strength, as well as transportation options in the surrounding area.
					<h3>Photos</h3>
					Photographs help students picture themselves in your class. Go ahead and upload images but make sure your photos are well lit, in focus, and representative of the actual class your students have access to.  
					We also offer free photography to active teachers in most areas. Just let us know that you might want our help, and we'll send someone over to you as soon as possible.
					<h3>Pricing and Availability</h3>
					You know your centre and its schedule best; you decide the price and update your calendar to reflect when how many seats are available in a batch. You can also choose to set custom prices for individual dates and courses. We display your listing at the price you set and when you intimate us that your tuition is unavailable, we make sure not to show it in search results.

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">TEACH</h4>
            </div>
            <div class="modal-body">
					<h3>Preparing your space</h3>
					Give your students an environment conducive to learning and make sure they are equipped  for a unique learning experience, whether it's any paraphernalia to be used at class, a clean washroom, availability of water and snacks, maps to find their way, or instructions for how the class shall be conducted. First impressions are invaluable!
					<h3>Coordinating logistics and ground rules</h3>
					Coordinate with the student through us and finalize the date, time and location of the class. Let your student know if you'll meet them in person or you have a reception where they have to wait. Also, do convey the guidelines and regulations for the class.
					<h3>Writing reviews</h3>
					Genuine reviews are the cornerstone of Pedagoge's trusted community. You can write a review and share comments about your experience with us. Your students will be asked to review you and your class too. So, make sure you make a good impression!

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>