<?php 

$found_localities = PDGManageCache::fn_load_cache('registered_locality');
$str_localities_options = '';		
if(!empty($found_localities)) {
	
	foreach($found_localities as $found_locality) {
		$locality_name = $found_locality->locality;
		$locality_id = $found_locality->locality_id;
		$str_localities_options .= '<option value="'.$locality_id.'">'.$locality_name.'</option>';
	}
}

echo $str_localities_options;