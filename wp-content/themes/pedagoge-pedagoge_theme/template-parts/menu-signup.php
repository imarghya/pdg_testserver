<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package pedagoge_theme
 */
$next_URL = $_SERVER['REQUEST_URI'];
?>

<li class="btn btn-lg btn-primary btn-round">
    <a href="<?php echo home_url('/login?nexturl='.$next_URL); ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> Login/Sign Up</a>
</li>