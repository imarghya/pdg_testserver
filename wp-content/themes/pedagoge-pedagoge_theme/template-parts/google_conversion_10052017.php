<?php
global $post, $wp_query;

$include_conversion_page = FALSE;

if ( isset( $post ) ) {
	$slug = $post->post_name;
	$is_virtual = $post->is_virtual;
		
	if($is_virtual) {
		$slug = trim( $wp_query->virtual_page->getUrl(), '/' );
	}
	
	switch($slug) {
		//case 'home':
		case 'contact-us':
		case 'signup':
		case 'thank-you':
			$include_conversion_page = TRUE;
			break;
	}
	if($include_conversion_page) {
		switch ( $_SERVER['SERVER_NAME'] ) {
			case 'www.pedagoge.com':
			//case 'www.pedagog.in':
			//case 'www.pedagoge.net':
			case 'dev.pedagoge.com':
			case 'test.pedagoge.com':
				break;
			default:
				$include_conversion_page = FALSE;
				break;
		}	
	}
}

if($include_conversion_page): ?>

	<!-- Added on 10/05/2017 -->
	<!-- Google Code for Conversion Code Conversion Page -->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 854876075;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "QA15COyvxXAQq7_RlwM";
		var google_remarketing_only = false;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/854876075/?label=QA15COyvxXAQq7_RlwM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
	<!-- End Added on 10/05/2017 -->
<?php endif; ?>