<?php

/*

Template Name: Landing-Students

*/

?>



<?php

get_header('landing');



global $wpdb;

$sql="SELECT b.display_name,c.localities,a.fk_student_id,a.teacherinst_id,d.subject_name,d.subject_name_id FROM pdg_matched_query AS a

LEFT OUTER JOIN wp_users AS b ON a.fk_student_id=b.ID

LEFT OUTER JOIN pdg_query AS c ON a.fk_query_id=c.id

LEFT OUTER JOIN pdg_subject_name AS d ON c.subject=d.subject_name_id

ORDER BY a.m_id DESC LIMIT 1";

$res=$wpdb->get_results($sql);

$tid=$res[0]->teacherinst_id;

$localities=$res[0]->localities;

$loc_arr=explode(",",$localities);

$subject_link=home_url().'/search/?pdg_subject='.$res[0]->subject_name.'&pdg_locality=/';





$sql_tech="SELECT a.user_id,a.profile_slug,b.name FROM pdg_teacher AS a LEFT OUTER JOIN user_data_teacher AS b ON a.user_id=b.new_user_id WHERE a.user_id='".$tid."'";

$res_tech=$wpdb->get_results($sql_tech);

if(!empty($res_tech)){ //Teacher

$profile_link=home_url().'/teacher/'.$res_tech[0]->profile_slug;

$ad_name=$res_tech[0]->name;

$profile_img=plugins_url()."/pedagoge_plugin/storage/uploads/images/".$res_tech[0]->user_id ."/profile.jpg";

}

else{ //Institute

$sql_tech="SELECT user_id,profile_slug,institute_name FROM pdg_institutes  WHERE user_id='".$tid."'";

$res_tech=$wpdb->get_results($sql_tech);

$profile_link=home_url().'/institute/'.$res_tech[0]->profile_slug;

$ad_name=$res_tech[0]->institute_name;

$profile_img=plugins_url()."/pedagoge_plugin/storage/uploads/images/". $res_tech[0]->userid ."/profile.jpg";

}

?>

<input id="page_id" type="hidden" value="1" />

    <div class="wrapper">

      <div class="main"> 

        <!-- page 1-->

        <section id="page1" class="header_wrapper">

          <!--<div class="content">-->

            <div class="container clearfix for_model_set_bottom">

            	

                <div class="first-panel-vm">

                  <div class="row text-center margin_top_60">

                    <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">

                        <h1>How it works</h1>

                    </div>

                  </div>

                  <div class="rows text-center hidden-xs visible-sm visible-md visible-lg">

                    <div class="col-xs-12 col-md-4 col-sm-4 h1_heading post_match_learn">

                    	<h2>Post</h2>

                        <figure>

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item1.png" alt="Post">

                        	<figcaption>

                            	<p>Post a query and see the magic happen.</p>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 h1_heading post_match_learn">

                    	<h2>Match</h2>

                        <figure>

			    <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item2.png" alt="Post">

                        	<figcaption>

                            	<p>We have hundres of teacher for every query you post.</p>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col-xs-12 col-md-4 col-sm-4 h1_heading post_match_learn">

                    	<h2>Learn</h2>

                        <figure>

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item3.png" alt="Post">

                        	<figcaption>

                            	<p>You can start your class on your desired time.</p>

                            </figcaption>

                        </figure>

                    </div>

                  </div>

                 

                  

                  <div class="large-12 text-center visible-xs hidden-sm hidden-md hidden-lg">

					<div class="owl-carousel owl-theme">

                        <div class="item h1_heading post_match_learn">

                            <h2>Post</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item1.png" alt="Post">

                                <figcaption>

                                    <p>Post a query and see the magic happen.</p>

                                </figcaption>

                            </figure>

                        </div>

                        <div class="item h1_heading post_match_learn">

                            <h2>Match</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item2.png" alt="Post">

                                <figcaption>

                                    <p>We have hundres of teacher for every query you post.</p>

                                </figcaption>

                            </figure>

                        </div>

                        <div class="item h1_heading post_match_learn">

                            <h2>Learn</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item3.png" alt="Post">

                                <figcaption>

                                    <p>You can start your class on your desired time.</p>

                                </figcaption>

                            </figure>

                        </div>

                  	</div>

                  </div>

                  

                  

                  <div class="row text-center">

                    <div class="col-md-12 col-sm-12 col-xs-12 margin_top_20 h1_heading">

                        <h1>Why pedagoge</h1>

                    </div>

                  </div>

                  <div class="rows text-center hidden-xs visible-sm visible-md visible-lg">

                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">

                    	<h2>Learner's Community</h2>

                        <figure>

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item4.png" alt="Post">

                        	<figcaption>

                            	<p>The Pedagoge community is full of awesome people,</p>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">

                    	<h2>Know your Teacher</h2>

                        <figure>

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item5.png" alt="Post">

                        	<figcaption>

                            	<p>Know your teacher in & out with our extensive database</p>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">

                    	<h2>Free for Students</h2>

                        <figure>

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item6.png" alt="Post">

                        	<figcaption>

                            	<p>Our services are 100% free of cost for students.</p>

                            </figcaption>

                        </figure>

                    </div>

                    <div class="col-xs-12 col-md-3 col-sm-3 h1_heading post_match_learn why_pedagoge">

                    	<h2>Personalized Assistance</h2>

                        <figure>

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item7.png" alt="Post">

                        	<figcaption>

                            	<p>Our team is always ready to help you over a call.</p>

                            </figcaption>

                        </figure>

                    </div>

                  </div>

                  

                  </div>

                  

                  <div class="large-12 text-center visible-xs hidden-sm hidden-md hidden-lg">

                      <div class="owl-carousel owl-theme">

                        <div class="item h1_heading post_match_learn why_pedagoge">

                            <h2>Learner's Community</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item4.png" alt="Post">

                                <figcaption>

                                    <p>The Pedagoge community is full of awesome people,</p>

                                </figcaption>

                            </figure>

                        </div>

                        <div class="item h1_heading post_match_learn why_pedagoge">

                            <h2>Know your Teacher</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item5.png" alt="Post">

                                <figcaption>

                                    <p>Know your teacher in & out with our extensive database</p>

                                </figcaption>

                            </figure>

                        </div>

                        <div class="item h1_heading post_match_learn why_pedagoge">

                            <h2>Free for Students</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item6.png" alt="Post">

                                <figcaption>

                                    <p>Our services are 100% free of cost for students.</p>

                                </figcaption>

                            </figure>

                        </div>

                        <div class="item h1_heading post_match_learn why_pedagoge">

                            <h2>Personalized Assistance</h2>

                            <figure>

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/item7.png" alt="Post">

                                <figcaption>

                                    <p>Our team is always ready to help you over a call.</p>

                                </figcaption>

                            </figure>

                        </div>

                      </div>

                  </div>

                 

            </div>

        </section>

        <!-- page 2-->

        <section id="page2">

            <div class="container clearfix for_model_set_bottom">

            

             <div class="second-panel-vm">

             

              <div class="row margin_top_60 text-center">

                  <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">

                      <h1>Localities</h1>

                  </div>

               </div>

               

               <div class="large-12 text-center">

				<div class="owl-carousel owl-theme">

				<div class="item h1_heading post_match_learn">

                		<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=1325,203,29,134,138,143,153,53,301,166,114,33,22,107">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/south_kolkata.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=105,296,44,163">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/south_west_kolkata.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=274,310,76,278,36,90">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/north_kolkata.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=260,239,326">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/north_east_kolkata.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=1595,218,73,316,328,2,177">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/central_kolkata.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=364,506,417,330,790,575,844,1293,426,613,339,931,1051">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/south_delhi.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=414,1511,1400,528,536,924,829,779">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/south_west_delhi.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=&pdg_locality=1527,429,420,421,428,419,770,977,631">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/west_delhi.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>&nbsp;</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                  </div>

				  </div>

                  

               <div class="row text-center">

                  <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">

                      <h1>Extra cirricular activities</h1>

                  </div>

               </div>

               

               <div class="large-12 columns text-center">

				<div class="owl-carousel owl-theme">

                    <div class="item h1_heading post_match_learn">

                    		<a href="<?php echo home_url(); ?>/search/?pdg_subject=Yoga&pdg_locality=">

                            <figure>

                                <div class="svg_holder">

                                <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/catg5.png" alt="Categories">

                                </div>

    

                                <figcaption>

                                    <h3>Yoga</h3>

                                </figcaption>

                            </figure>

                            </a>

                        </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=Guitar&pdg_locality=">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/catg6.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>Guitar</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=Zumba&pdg_locality=">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/catg7.png" alt="Categories">

                            </div>



                        	<figcaption>

                            	<h3>Zumba</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                    <div class="item h1_heading post_match_learn">

                    	<a href="<?php echo home_url(); ?>/search/?pdg_subject=Bharatnatyam Dance&pdg_locality=">

                        <figure>

                        	<div class="svg_holder">

                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/catg8.png" alt="Categories">

                            </div>

                        	<figcaption>

                            	<h3>Bharatanatyam</h3>

                            </figcaption>

                        </figure>

                        </a>

                    </div>

                 </div>

               </div>

               

               </div>

               

            </div>

	    

	    

	    

        </section>

        <!-- page 3-->

        <section id="page3">

           <div class="container clearfix for_model_set_bottom">

           <!--<div class="third-panel-vm">-->

           		<div class="row text-center only_footer_margin">

                  <div class="col-md-12 col-sm-12 col-xs-12 h1_heading">

                      <h1>Latest Matches</h1>

                  </div>

               </div>

               <div class="row">

                   <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 col-lg-offset-2">

                      <div class="row">

                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 match_decoration">

                                <div class="logo_holder">

                                    <img src="<?php echo $profile_img; ?>" alt="<?php echo $ad_name; ?>">

                                </div>

                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 match_decoration">

                                <p class="margin_top_40"><a href="<?php echo $profile_link; ?>" class="text-uppercase green_color"><?php echo $ad_name; ?></a>  matched with <?php echo $res[0]->display_name; ?> for <a href="<?php echo $subject_link; ?>" class="text-uppercase green_color"> <?php echo $res[0]->subject_name; ?></a>  classes in

				<?php

				$comma='';

				if(count($loc_arr)>1){

				$comma=',';		

				}

				$i=1;

				foreach($loc_arr as $loc_id){

				$sql_loc="SELECT locality  FROM pdg_locality WHERE locality_id='".$loc_id."'";

				$res_loc=$wpdb->get_results($sql_loc);

				?>

				<?php if($i!=1){echo $comma;} ?><a href="<?php echo home_url().'/search/?pdg_subject=&pdg_locality='.$loc_id; ?>" class="text-uppercase green_color"><?php echo $res_loc[0]->locality; ?></a>

				<?php $i++;} ?>

				</p>

                            </div>

                      </div>

                      <div class="row">

                          <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1 text-center h1_heading match_decoration">

                              <h1>For all your learning needs We're the one stop solution!</h1>

                              

                              <!--<div class="input-group">

                                  <input class="form-control" placeholder="What do you wish to learn?" type="text">

                                  <span class="input-group-btn">

                                    <button class="btn search" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>

                                  </span>

                              </div>-->

                              <form>

                              	<div class="form-group">

                                	<!--<button type="button" class="post_a_query">Post A Query</button>-->

<a href="<?php echo home_url('/login'); ?>" class="post_a_query">Post A Query</a>

                                </div>

                              </form>

                              

                          </div>

                       </div>

                       

                       

                  </div>

		   

		   

              </div>

             <!-- </div>-->

              

             

                 

<?php

get_footer('landing');

?>