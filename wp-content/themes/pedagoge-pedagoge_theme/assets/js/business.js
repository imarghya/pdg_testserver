var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
$(document).ready(function () {
	var $isMobile = false, $isSM = false;
	if ($(".checkmob-business").is(":visible")) {
		$isMobile = true;
	}
	if ($(".checksm-business").is(":visible")) {
		$isSM = true;
	}

	var $trigger = 'hover', $animation = true, $html = true, $container = 'body';

	$('.img1-business, .img2-business, .img3-business, .img4-business, .img5-business, .img6-business').webuiPopover({
		placement: 'auto-top',
		animation: 'pop',
		trigger: 'hover',
		width: function () {
			return ($isMobile) ? "auto" : 300;
		}
	});

	$(".form1, .form2").submit(function (event) {
		event.preventDefault();
	});

	$(".submit-form1-business").click(function (event) {
		event.preventDefault();
		var $message = '';
		var $result_area = $("#result_area");
		var $submit_button = $(this);
		var $companyName = $("#companyName");
		var $yourName = $("#yourName");
		var $email = $("#email");
		var $phone = $("#phone");
		var $designation = $("#designation");
		var $ajax_loader = $("#img_contact_loader");

		var $companyName_value = $companyName.val();
		var $yourName_value = $yourName.val();
		var $email_value = $email.val();
		var $phone_value = $phone.val();
		var $designation_value = $designation.val();

		if ($companyName_value.length <= 2) {
			$yourName.select().focus();
			$message = "Invalid company name!";
			triggerError($message, $companyName, 'error', $isSM);
			return;
		}
		else {
			$($companyName).popover('destroy');
		}

		if ($yourName_value.length <= 2) {
			$yourName.select().focus();
			$message = "Invalid name!";
			triggerError($message, $yourName, 'error', $isSM);
			return;
		}
		else {
			$($yourName).popover('destroy');
		}

		if ($phone_value.length <= 8) {
			$phone.select().focus();
			$message = "Invalid contact number!";
			triggerError($message, $phone, 'error', $isSM);
			return;
		}
		else {
			$($phone).popover('destroy');
		}

		$submit_button.button('loading');

		$ajax_loader.show();

		var submit_data = {
			action: 'pedagoge_visitor_ajax_handler',
			pedagoge_callback_function: 'business_form_ajax',
			pedagoge_callback_class: 'PDGBusinessContactForm',
			contact_companyName_value: $companyName_value,
			contact_name_value: $yourName_value,
			contact_email_value: $email_value,
			contact_number_value: $phone_value,
			contact_designation_value: $designation_value,
			formNumber: 1
		};

		$.post($ajax_url, submit_data, function (response) {
			try {
				var $response_data = $.parseJSON(response);

				var $error = $response_data.error,
					$message = $response_data.message;

				if ($error) {
					triggerError($message, $email, 'error', $isSM);
					$('.form1-business').popover('destroy');
				} else {
					triggerError($message, '.form1-business', 'success', $isSM);

				}
			} catch (err) {
				$message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
				triggerError($message, $email, 'error', $isSM);
			} finally {
				$submit_button.button('reset');
				$ajax_loader.hide();
			}

		}).complete(function () {
			$submit_button.button('reset');
			$ajax_loader.hide();
		});

	});

	$(".submit-form2-business").click(function (event) {
		event.preventDefault();
		var $message2 = '';
		var $result_area2 = $("#result_area2");
		var $submit_button2 = $(this);
		var $yourName2 = $("#yourName2");
		var $email2 = $("#email2");
		var $phone2 = $("#phone2");
		var $ajax_loader2 = $("#img_contact_loader2");

		var $yourName2_value = $yourName2.val();
		var $email2_value = $email2.val();
		var $phone2_value = $phone2.val();

		if ($yourName2_value.length <= 2) {
			$yourName2.select().focus();
			$message2 = "You need to input your name!";
			fn_alert_message($result_area2, $message2, 'error');
			return;
		}

		if ($phone2_value.length <= 8) {
			$phone2.select().focus();
			$message2 = "You need to input your contact number!";
			fn_alert_message($result_area2, $message2, 'error');
			return;
		}

		$submit_button2.button('loading');

		$ajax_loader2.show();

		var submit_data = {
			action: 'pedagoge_visitor_ajax_handler',
			pedagoge_callback_function: 'business_form_ajax',
			pedagoge_callback_class: 'PDGBusinessContactForm',
			contact_name_value: $yourName2_value,
			contact_email_value: $email2_value,
			contact_number_value: $phone2_value,
			formNumber: 2
		};

		$.post($ajax_url, submit_data, function (response) {
			try {
				var $response_data = $.parseJSON(response);

				var $error = $response_data.error,
					$message2 = $response_data.message;

				if ($error) {
					fn_alert_message($result_area2, $message2, 'error');
				} else {
					var $str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message2 + '</div>';
					$result_area2.html($str_message);
					$('.submit-form2-business').css('visibility', 'hidden');
					$('.form2').trigger("reset").find('input, textarea, button, select').attr('disabled', 'disabled');
				}
			} catch (err) {
				$message2 = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
				fn_alert_message($result_area2, $message2, 'error');
			} finally {
				$submit_button2.button('reset');
				$ajax_loader2.hide();
			}

		}).complete(function () {
			$submit_button2.button('reset');
			$ajax_loader2.hide();
		});

	});
	$("#registration_form").validationEngine({scroll: false, promptPosition: "topLeft:0"});

	$("#txt_register_email_address").on('input', function (event) {
		var $email = $(this).val();
		var $user_name = $email.split("@");
		$("#txt_register_user_name").val($user_name[0]);
	});


	$('.chk_register_show_password').change(function () {
		if ($(this).is(":checked")) {
			$("#txt_register_password").prop('type', 'text');
		} else {
			$("#txt_register_password").prop('type', 'password');
		}
	});

	$("#cmd_register_user").click(function (event) {
		event.preventDefault(); //incase button becomes submit button.

		var $button = $(this),
			$txt_register_first_name = $("#txt_register_first_name"),
			$txt_register_last_name = $("#txt_register_last_name"),
			$txt_register_email_address = $("#txt_register_email_address"),
			$txt_register_user_name = $("#txt_register_user_name"),
			$txt_register_password = $("#txt_register_password"),
			$user_role = "trainer",
			$loader = $("#img_signup_loader");


		if ($("#registration_form").validationEngine('validate', {scroll: false, promptPosition: "topLeft:0"})) {
			/**
			 * Checking for user role manually, so that multiple scenarios can be covered.
			 */
			$button.button('loading');
			$loader.show();
			var submit_data = {
				nonce: $('.ajaxnonce').val(),
				action: $pedagoge_visitor_ajax_handler,
				pedagoge_callback_function: 'user_signup_ajax',
				pedagoge_callback_class: 'ControllerSignup',
				first_name: $txt_register_first_name.val(),
				last_name: $txt_register_last_name.val(),
				email_address: $txt_register_email_address.val(),
				user_name: $txt_register_user_name.val(),
				password: $txt_register_password.val(),
				user_role: $user_role
			};


			$.post($ajax_url, submit_data, function (response) {
				try {
					var $response_data = $.parseJSON(response);
					var $error = $response_data.error,
						$error_type = $response_data.error_type,
						$message = $response_data.message;

					if ($error) {
						//handle login error
						$button.button('reset');
						pdg_show_alerts('div_signup_result', $message, 'error');
						switch ($error_type) {
							case 'duplicate_username':
								$txt_register_user_name.select().focus();
								break;
							case 'duplicate_email':
								$txt_register_email_address.select().focus();
								break;
						}

					} else {
						pdg_show_alerts('div_signup_result', $message, 'success');
						setTimeout(function () {
							//location.reload(true); // do this later.
							window.location = $('.siteUrl').val() + "/profile";
						}, 500);


					}
				} catch (err) {
					$button.button('reset');
				} finally {
					$loader.hide();
				}


			}).complete(function () {
				//$button.button('reset');
				$loader.hide();
			});
		}
	});
});
function triggerError(message, selector, type, $isSM) {
	if ($isSM) {
		if (type == 'success') {
			$('.submit-form1-business').css('visibility', 'hidden');
			$('.form1').trigger("reset").find('input, textarea, button, select').attr('disabled', 'disabled');
		}
		var $result_area = $("#result_area");
		fn_alert_message($result_area, message, type);
		return;
	}
	var $type = '', $trigger, $delay = null;
	if (type == 'error') {
		$type = "<div style='color: red; font-weight: bold;'>Error</div>";
		$trigger = 'manual';

	}
	else {
		$("*").each(function () {
			var popover = $.data(this, "bs.popover");
			if (popover) {
				$(this).popover('hide');
			}
		});
		$('.submit-form1-business').css('visibility', 'hidden');
		$('.form1').trigger("reset").find('input, textarea, button, select').attr('disabled', 'disabled');
		$type = "<div style='color: green; font-weight: bold;'>Success</div>";
		$trigger = 'hover';
		$delay = 3000;
	}

	$(selector).popover({
		trigger: $trigger,
		placement: 'left',
		title: $type,
		content: "<div style='color: #000000; text-align: left;'>" + message + "</div>",
		width: 300,
		html: true,
		container: 'body',
		animation: true,
		delay: {
			hide: $delay
		}
	});
	$(selector).popover('show');

}
var $alert_time_out_var = null;

/**
 * Function to create various types of messages such as Info/Error/Success/Warning
 * @param (string) (Required) Area where message will be displayed
 * @param (string) (Required) The content of the alert message.
 * @param (string) (Required) Types of alert (error/info/success/warning)
 * @param (true/false) (Required) whether the message will be removed after 6 seconds or not
 */
function fn_alert_message($div_id, $message, $alert_type, $timer) {

	clearTimeout($alert_time_out_var);

	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000000 : $timer;

	var $result_area = null;
	if (typeof $div_id === "string") {
		$result_area = jQuery("#" + $div_id);
	} else {
		$result_area = $div_id;
	}

	var $str_message = '';


	$result_area.html('');

	switch ($alert_type) {
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'info':
			$str_message = '<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'success':
			$str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'warning':
			$str_message = '<div class="alert alert-block alert-primary fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'blank_success':
			$str_message = '<span class="label label-success fade in blink_me">' + $message + '</span>';
			break;
		case 'blank_error':
			$str_message = '<span class="label label-primary fade in blink_me">' + $message + '</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html($str_message);

	if ($timer) {
		$alert_time_out_var = setTimeout(function () {
			$result_area.html('');
		}, $timer);
	}
}


//ref http://stackoverflow.com/questions/2855865/jquery-regex-validation-of-e-mail-address
function isValidEmailAddress(emailAddress) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(emailAddress);
}