$(document).ready(function () {

	$(".form-landingjul09").submit(function (event) {
		event.preventDefault();
	});

	$(".submit-form1-landingjul09").click(function (event) {
		event.preventDefault();
		var $message = '';
		var $result_area = $("#result_area");
		var $submit_button = $(this);
		var $name = $("#Name");
		var $email = $("#Email");
		var $phone = $("#Mobile");
		var $Requirement = $("#Requirement");
		var $ajax_loader = $("#img_contact_loader");

		var $name_value = $name.val();
		var $email_value = $email.val();
		var $phone_value = $phone.val();
		var $Requirement_value = $Requirement.val();

		if ($name_value.length <= 2) {
			$name.select().focus();
			$message = "Invalid name!";
			triggerError($message, $name, 'error');
			return;
		}

		if ($phone_value.length <= 8) {
			$phone.select().focus();
			$message = "Invalid phone!";
			triggerError($message, $phone, 'error');
			return;
		}

		if ($email_value.length <= 3) {
			$email.select().focus();
			$message = "Invalid email address!";
			triggerError($message, $email, 'error');
			return;
		}

		if ($Requirement_value.length <= 8) {
			$Requirement.select().focus();
			$message = "Invalid requirement!";
			triggerError($message, $Requirement, 'error');
			return;
		}

		$submit_button.button('loading');

		$ajax_loader.show();

		var submit_data = {
			action: 'pedagoge_visitor_ajax_handler',
			pedagoge_callback_function: 'landingjul09_ajax',
			pedagoge_callback_class: 'PDGBusinessContactForm',
			contact_name_value: $name_value,
			contact_email_value: $email_value,
			contact_number_value: $phone_value,
			contact_requirement_value: $Requirement_value
		};

		$.post($ajax_url, submit_data, function (response) {
			try {
				var $response_data = $.parseJSON(response);

				var $error = $response_data.error,
					$message = $response_data.message;

				if ($error) {
					triggerError($message, $email, 'error');
					$('.form1-business').popover('destroy');
				} else {
					triggerError($message, '.form1-business', 'success');

				}
			} catch (err) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
				triggerError($message, $email, 'error');
			} finally {
				$submit_button.button('reset');
				$ajax_loader.hide();
			}

		}).complete(function () {
			$submit_button.button('reset');
			$ajax_loader.hide();
		});

	});

});
function triggerError(message, selector, type) {
	if (type == 'success') {
		$('.submit-form1-landingjul09').css('visibility', 'hidden');
		$('.form-landingjul09').trigger("reset").find('input, textarea, button, select').attr('disabled', 'disabled');
	}
	var $result_area = $("#result_area");
	fn_alert_message($result_area, message, type);
}
var $alert_time_out_var = null;

/**
 * Function to create various types of messages such as Info/Error/Success/Warning
 * @param (string) (Required) Area where message will be displayed
 * @param (string) (Required) The content of the alert message.
 * @param (string) (Required) Types of alert (error/info/success/warning)
 * @param (true/false) (Required) whether the message will be removed after 6 seconds or not
 */
function fn_alert_message($div_id, $message, $alert_type, $timer) {

	clearTimeout($alert_time_out_var);

	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000000 : $timer;

	var $result_area = null;
	if (typeof $div_id === "string") {
		$result_area = jQuery("#" + $div_id);
	} else {
		$result_area = $div_id;
	}

	var $str_message = '';


	$result_area.html('');

	switch ($alert_type) {
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'info':
			$str_message = '<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'success':
			$str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'warning':
			$str_message = '<div class="alert alert-block alert-primary fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'blank_success':
			$str_message = '<span class="label label-success fade in blink_me">' + $message + '</span>';
			break;
		case 'blank_error':
			$str_message = '<span class="label label-primary fade in blink_me">' + $message + '</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html($str_message);

	if ($timer) {
		$alert_time_out_var = setTimeout(function () {
			$result_area.html('');
		}, $timer);
	}
}

function isValidEmailAddress(emailAddress) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(emailAddress);
}