$(document).ready(function () {
	$("#pdg_contact_form").submit(function (event) {
		event.preventDefault();
	});


	$("#submit_contact_form").click(function (event) {
		event.preventDefault();
		var $message = '';
		var $result_area = $("#result_area");
		var $submit_button = $(this);
		var $contact_name = $("#contact_name");
		var $contact_email = $("#contact_email");
		var $contact_number = $("#contact_number");
		var $contact_message = $("#contact_message");
		var $ajax_loader = $("#img_contact_loader");

		var $contact_name_value = $contact_name.val();
		var $contact_email_value = $contact_email.val();
		var $contact_number_value = $contact_number.val();
		var $contact_message_value = $contact_message.val();
		var $hnpt_value = $("#txt_hnpt_fld").val();

		if ($contact_name_value.length <= 2) {
			$contact_name.select().focus();
			$message = "You need to input your name!";
			fn_alert_message($result_area, $message, 'error');
			return;
		}

		if ($contact_number_value.length <= 8) {
			$contact_number.select().focus();
			$message = "You need to input your contact number!";
			fn_alert_message($result_area, $message, 'error');
			return;
		}


		if ($contact_message_value.length <= 3) {
			$contact_message.select().focus();
			$message = "You need to input your message!";
			fn_alert_message($result_area, $message, 'error');
			return;
		}

		if ($hnpt_value.length > 0) {
			return;
		}

		$submit_button.button('loading');

		$ajax_loader.show();

		var submit_data = {

			action: 'pedagoge_visitor_ajax_handler',
			pedagoge_callback_function: 'contact_form_ajax',
			pedagoge_callback_class: 'PDGContactForm',

			contact_name_value: $contact_name_value,
			contact_email_value: $contact_email_value,
			contact_number_value: $contact_number_value,
			contact_message_value: $contact_message_value,
			hnpt_val: $hnpt_value
		};

		$.post($ajax_url, submit_data, function (response) {
			try {
				var $response_data = $.parseJSON(response);

				var $error = $response_data.error,
					$message = $response_data.message;

				if ($error) {
					fn_alert_message($result_area, $message, 'error');
				} else {
					//fn_alert_message($result_area, $message, 'success');
					var $str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
					$(".div_contact_form_area").html($str_message);
				}
			} catch (err) {
				var $message = '<strong>Unexpected Error in processing your request! Please try again!</strong><br/> ' + err;
				fn_alert_message($result_area, $message, 'error');
			} finally {
				$submit_button.button('reset');
				$ajax_loader.hide();
			}

		}).complete(function () {
			$submit_button.button('reset');
			$ajax_loader.hide();
		});

	});
});

var $alert_time_out_var = null;

/**
 * Function to create various types of messages such as Info/Error/Success/Warning
 * @param (string) (Required) Area where message will be displayed
 * @param (string) (Required) The content of the alert message.
 * @param (string) (Required) Types of alert (error/info/success/warning)
 * @param (true/false) (Required) whether the message will be removed after 6 seconds or not
 */
function fn_alert_message($div_id, $message, $alert_type, $timer) {

	clearTimeout($alert_time_out_var);

	$alert_type = (typeof $alert_type === "undefined") ? '' : $alert_type;
	$timer = (typeof $timer === "undefined") ? 8000 : $timer;

	var $result_area = null;
	if (typeof $div_id === "string") {
		$result_area = jQuery("#" + $div_id);
	} else {
		$result_area = $div_id;
	}

	var $str_message = '';


	$result_area.html('');

	switch ($alert_type) {
		case 'error':
			$str_message = '<div class="alert alert-block alert-danger fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'info':
			$str_message = '<div class="alert alert-block alert-info fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'success':
			$str_message = '<div class="alert alert-block alert-success fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'warning':
			$str_message = '<div class="alert alert-block alert-primary fade in blink_me"> <button type="button" class="close" data-dismiss="alert">&times;</button> ' + $message + '</div>';
			break;
		case 'blank_success':
			$str_message = '<span class="label label-success fade in blink_me">' + $message + '</span>';
			break;
		case 'blank_error':
			$str_message = '<span class="label label-primary fade in blink_me">' + $message + '</span>';
			break;
		default:
			$str_message = $message;
			break;
	}
	$result_area.html($str_message);

	if ($timer) {
		$alert_time_out_var = setTimeout(function () {
			$result_area.html('');
		}, $timer);
	}
}

//ref http://stackoverflow.com/questions/2855865/jquery-regex-validation-of-e-mail-address
function isValidEmailAddress(emailAddress) {
	var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	return pattern.test(emailAddress);
};