(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
			var PDG_Aboutus = function () {
				var mainObj = this, settingsObj, founders_slider_bottom_block_ht, rohit_block_ht, mohit_block_ht, about_us_heading_flag = false, isTouchActivatedMobileTablet = false;
				//Settings
				mainObj.settings = function () {
					var $bodyEl = $( 'body' ),
						$isMobile = $( '.isMobile' ),
						$isMobileTablet = $( '.isMobileTablet' ),
						$isTablet = $( '.isTablet' ),
						$fullpage = $( '#fullpage' ),
						$foundersCarousel = $( '#foundersCarousel' ),
						$nav_navbar = $( 'nav.navbar' ),
						$section = $( 'section' ),
						$section_top = $section.filter( '.section-top' ),
						$section_top_h1_main_heading = $section_top.find( 'h1.main-heading' ),
						$section_top_telescope_guy_wrapper_img = $section_top.find( '.telescope-guy-wrapper img' ),
						$section_aboutus = $section.filter( '.section-aboutus' ),
						$section_aboutus_contents_block = $section_aboutus.find( '.contents-block' ),
						$section_aboutus_heading_block = $section_aboutus.find( '.heading-block' ),
						$founders_slider_bottom_block = $( '.founders-slider-bottom-block' ),
						$founders_slider_bottom_block_rohit = $founders_slider_bottom_block.find( '.rohit' ),
						$founders_slider_bottom_block_mohit = $founders_slider_bottom_block.find( '.mohit' );
					
					return {
						$bodyEl: $bodyEl,
						$isMobile: $isMobile,
						$isMobileTablet: $isMobileTablet,
						$isTablet: $isTablet,
						$fullpage: $fullpage,
						$foundersCarousel: $foundersCarousel,
						$nav_navbar: $nav_navbar,
						$section: $section,
						$section_aboutus: $section_aboutus,
						$section_aboutus_contents_block: $section_aboutus_contents_block,
						$section_aboutus_heading_block: $section_aboutus_heading_block,
						$section_top: $section_top,
						$section_top_h1_main_heading: $section_top_h1_main_heading,
						$section_top_telescope_guy_wrapper_img: $section_top_telescope_guy_wrapper_img,
						$founders_slider_bottom_block: $founders_slider_bottom_block,
						$founders_slider_bottom_block_rohit: $founders_slider_bottom_block_rohit,
						$founders_slider_bottom_block_mohit: $founders_slider_bottom_block_mohit,
					}
				};
				
				//Load initial page search results
				mainObj.init = function () {
					settingsObj = mainObj.settings();
					mainObj.bindUIActions();
				};
				
				mainObj.bindUIActions = function () {
					calculate_founders_slider_bottom_block_height();
					sliderHandler();
					resizeHandler();
					orientationHandler();
				};
				
				//offset controls for the UI
				var isTouchActivatedMobileTablet_fn = function () {
						if ( settingsObj.$isMobile.is( ':visible' ) ) {
							isTouchActivatedMobileTablet = true;
						}
						else if ( settingsObj.$isMobileTablet.is( ':visible' ) && isTouchDevice() ) {
							isTouchActivatedMobileTablet = true;
						}
						return isTouchActivatedMobileTablet;
					},
					sliderHandler = function () {
						settingsObj.$foundersCarousel.on( 'slide.bs.carousel', function ( e ) {
							var $active = $( e.target ).find( '.carousel-inner > .item.active' ),
								current = $active.index(),
								next = $( e.relatedTarget ),
								to = next.index();
							switch ( to ) {
								case 1:
									settingsObj.$founders_slider_bottom_block_rohit.hide();
									settingsObj.$founders_slider_bottom_block_mohit.show();
									break;
								case 0:
								default:
									settingsObj.$founders_slider_bottom_block_mohit.hide();
									settingsObj.$founders_slider_bottom_block_rohit.show();
									break;
							}
						} );
					},
					resizeHandler = function () {
						$( window ).resize( function () {
							calculate_founders_slider_bottom_block_height();
						} );
					},
					orientationHandler = function () {
						$( window ).on( "orientationchange", function ( event ) {
							calculate_founders_slider_bottom_block_height();
						} );
					},
					scrollHandler = function () {
						$( window ).scroll( function () {
						
						} );
					},
					buttonHandler = function () {
					
					},
					calculate_founders_slider_bottom_block_height = function () {
						rohit_block_ht = settingsObj.$founders_slider_bottom_block_rohit.height();
						mohit_block_ht = settingsObj.$founders_slider_bottom_block_mohit.height();
						settingsObj.$founders_slider_bottom_block_mohit.hide();
						founders_slider_bottom_block_ht = settingsObj.$founders_slider_bottom_block.height();
						fix_founders_slider_bottom_block_height();
					},
					fix_founders_slider_bottom_block_height = function () {
						settingsObj.$founders_slider_bottom_block_rohit.css( {
							'min-height': founders_slider_bottom_block_ht,
						} );
						settingsObj.$founders_slider_bottom_block_mohit.css( {
							'min-height': founders_slider_bottom_block_ht,
						} );
					}
				
			}, aboutusObj;
			aboutusObj = new PDG_Aboutus;
			aboutusObj.init();
		}
	);
} );
