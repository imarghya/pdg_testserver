dynamic_locality_ids = locality_ids = pdg_sanitize.noundefined( urlParam.get( 'pdg_locality' ) );
dynamic_subject_value = subject_value = pdg_sanitize.noundefined( urlParam.get( 'pdg_subject' ) );

(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
		var searchObj,
			PDG_Search_Desktop = function () {
				var mainObj = this, settingsObj, search_locality_loaded = false, search_subject_loaded = false, ajaxSearchCompleted = true, $search_data_locality, $search_data_subject, $search_data_teachers;
				//Settings
				mainObj.settings = function () {
					var other_filters_search_card_wrapper = $( '.other-filters-search-card-wrapper' ),
						site_footer = $( '.site-footer' ),
						load_more_wrapper = $( '.load-more-wrapper' ),
						search_locality = $( '#search-locality' ),
						search_teacher = $( '#search_teacher' ),
						search_subject = $( '#search_subject' ),
						search_btn = $( '.search_btn' ),
						$isMobileTabletEl = $( '.isMobileTablet' ),
						$isTabletEl = $( '.isTablet' ),
						$navbarBrandImgEl = $( '.navbar-brand img' ),
						$bgImageMainEl = $( '.bg-image-main' ),
						$start_a_class_btnEl = $bgImageMainEl.find( 'a.start_a_class_btn' ),
						search_page_type = $( '.search-page-type' ).data( "type" ),
						search_data_subject = $( '.search-data-subject' ),
						search_data_locality = $( '.search-data-locality' ),
						$bodyEl = $( 'body' ),
						exitSearch = $( '.exit_search' ),
						start_filter = $( ".start_filter" ),
						search_result_main = $( ".search_result_main" ),
						search_result_item_cards = search_result_main.find( ".search_result_item_cards" ),
						dataTogglePopover = '[data-toggle=popover]',
						$popoverEl = $( ".popover" ),
						$searched_subject_name = $( '.searched_subject_name' ),
						$searched_locality_name = $( '.searched_locality_name' ),
						search_locality_selectizeObj = {
							maxItems: 6,
							persist: false,
							maxOptions: 5,
							valueField: 'locality_id',
							hideSelected: true,
							labelField: 'locality',
							searchField: 'locality',
							loadThrottle: 300,
							openOnFocus: true,
							placeholder: "Select Locality",
							scrollDuration: 300,
							sortField: {
								field: 'locality',
								direction: 'asc'
							},
							onChange: function ( value ) {
								search_locality_loaded = true;
								var search_selectize_control_multi = $( '.search .selectize-control.multi .selectize-input' );
								search_selectize_control_multi.scrollTop( search_selectize_control_multi[0].scrollHeight );
								value = pdg_sanitize.noundefined( value, true );
								locality_ids = value.join( "," );
								$searched_locality_name.data( 'value', locality_ids );
								dynamic_locality_ids = locality_ids;
							},
							render: {
								option_create: function ( data, escape ) {
									return '<div class="create"><strong>' + escape( data.input ) + '</strong></div>';
								}
							},
							//options: $.parseJSON( $json_search_data_locality.text() ),
							create: false
						},
						search_subject_selectizeObj = {
							maxItems: 6,
							maxOptions: 5,
							persist: false,
							valueField: 'subject_name',
							hideSelected: true,
							loadThrottle: 300,
							labelField: 'subject_name',
							searchField: 'subject_name',
							placeholder: "What do you wish to learn?",
							sortField: {
								field: 'subject_name',
								direction: 'asc'
							},
							onChange: function ( value ) {
								var selectize_control_single = $( '.search .selectize-control.multi .selectize-input' );
								selectize_control_single.scrollTop( selectize_control_single[0].scrollHeight );
								value = pdg_sanitize.noundefined( value, true );
								subject_value = value.join( "," );
								$searched_subject_name.data( 'value', subject_value );
								dynamic_subject_value = subject_value;
							},
							render: {
								option_create: function ( data, escape ) {
									return '<div class="create"><strong>' + escape( data.input ) + '</strong></div>';
								}
							},
							//options: $.parseJSON( $json_search_data_subject.text() ),
							create: true
						},
						search_teacher_selectizeObj = {
							maxItems: 1,
							maxOptions: 5,
							persist: false,
							valueField: 'teacher',
							hideSelected: true,
							loadThrottle: 300,
							labelField: 'teacher',
							searchField: 'teacher',
							placeholder: "Select Teacher",
							sortField: {
								field: 'teacher',
								direction: 'asc'
							},
							onChange: function ( value ) {
								value = pdg_sanitize.noundefined( value );
								teacher_value = value;
								search_btn.click();
							},
							render: {
								option_create: function ( data, escape ) {
									return '<div class="create"><strong>' + escape( data.input ) + '</strong></div>';
								}
							},
							//options: $.parseJSON( $json_search_data_teacher.text() ),
							create: false
						},
						$callback_sub_El = $( '#callback_sub' ),
						$callback_loc_El = $( '#callback_loc' );
					
					return {
						search_btn: search_btn,
						search_page_type: search_page_type,
						other_filters_search_card_wrapper: other_filters_search_card_wrapper,
						site_footer: site_footer,
						$isMobileTabletEl: $isMobileTabletEl,
						$isTabletEl: $isTabletEl,
						$bgImageMainEl: $bgImageMainEl,
						$navbarBrandImgEl: $navbarBrandImgEl,
						$start_a_class_btnEl: $start_a_class_btnEl,
						load_more_wrapper: load_more_wrapper,
						search_locality: search_locality,
						search_subject: search_subject,
						search_teacher: search_teacher,
						$bodyEl: $bodyEl,
						exitSearch: exitSearch,
						start_filter: start_filter,
						search_result_main: search_result_main,
						search_result_item_cards: search_result_item_cards,
						$popoverEl: $popoverEl,
						dataTogglePopover: dataTogglePopover,
						search_locality_selectizeObj: search_locality_selectizeObj,
						search_subject_selectizeObj: search_subject_selectizeObj,
						search_teacher_selectizeObj: search_teacher_selectizeObj,
						search_data_subject: search_data_subject,
						search_data_locality: search_data_locality,
						$searched_subject_name: $searched_subject_name,
						$searched_locality_name: $searched_locality_name,
						$callback_sub_El: $callback_sub_El,
						$callback_loc_El: $callback_loc_El,
					};
				};
				
				//Load initial page search results
				mainObj.init = function () {
					settingsObj = mainObj.settings();
					mainObj.bindUIActions();
					//This should work only on main search page
					if ( 'undefined' !== typeof PDG_Search ) {
						searchObj = new PDG_Search( 20, true );
						searchObj.init();
					}
				};
				
				mainObj.bindUIActions = function () {
					popoverFix();
					//makeCardClickable();
					searchBtnHandler();
					resizeHandler();
					scrollHandler();
					startFilterHandler();
					exitFilterHandler();
					selectizeHandlerInit();
					prefill_search_result_item_req_call();
					search_result_profile_view_button_handler();
				};
				
				//offset controls for the UI
				var selectizeHandlerInit = function () {
						_ld_();
						_sd_();
						switch ( settingsObj.search_page_type ) {
							case  'search':
								_td_();
								break;
							default:
								break;
						}
						/**
						 * Wait until the ajax calls are done so that all the required values can be populated by the search fields
						 */
						$( document ).ajaxStop( function () {
							settingsObj.search_locality_selectizeObj.options = $search_data_locality.items;
							settingsObj.search_subject_selectizeObj.options = $search_data_subject.items;
							selectizeHandler( [
								{
									selector: settingsObj.search_locality,
									values: settingsObj.search_locality_selectizeObj,
									prepopulate: {
										values: dynamic_locality_ids.split( "," ),
									}
								}, {
									selector: settingsObj.search_subject,
									values: settingsObj.search_subject_selectizeObj,
									prepopulate: {
										values: dynamic_subject_value.split( "," ),
									}
								}
							] );
							switch ( settingsObj.search_page_type ) {
								case  'search':
									settingsObj.search_teacher_selectizeObj.options = $search_data_teachers.items;
									selectizeHandler( [
										{
											selector: settingsObj.search_teacher,
											values: settingsObj.search_teacher_selectizeObj,
										}
									] );
									break;
								default:
									break;
							}
							
							placeholderFix();
						} );
					},
					_ld_ = function () {
						var searchDataArray = {
								'ld_': true,
							},
							ajaxData = {
								'pedagoge_callback_function': '_ld_ajax',
							};
						
						bindAjax( searchDataArray, ajaxData, function ( data ) {
							$search_data_locality = $.parseJSON( htmlHandle.htmlUnescape( data ) );
							var process_locality_name_by_id = function () {
									var locality_array = [];
									$.each( $search_data_locality.items, function ( index, value ) {
										locality_array[value.locality_id] = value.locality;
									} );
									return locality_array;
								},
								data_locality_array = process_locality_name_by_id();
							
							//Get the locality name from the id. Parsed locality data is processed here for quicker locality name search
							get_locality_name_by_id = function ( localityids, stringify ) {
								localityids = pdg_sanitize.noundefined( localityids );
								localityids = rtrim( localityids, "," ).trim();
								if ( localityids == "" ) {
									return "";
								}
								var localityids_array = localityids.split( "," ),
									return_array = [];
								
								$.each( localityids_array, function ( index, value ) {
									return_array.push( data_locality_array[value] );
								} );
								if ( pdg_sanitize.noundefined( stringify ) == "" || stringify === false || stringify === null ) {
									return return_array;
								}
								return rtrim( return_array.join( "," ), "," ).trim();
							};
						} );
						return $search_data_locality;
					},
					_sd_ = function () {
						var searchDataArray = {
								'sd_': true,
							},
							ajaxData = {
								'pedagoge_callback_function': '_sd_ajax',
							};
						
						bindAjax( searchDataArray, ajaxData, function ( data ) {
							$search_data_subject = $.parseJSON( htmlHandle.htmlUnescape( data ) );
						} );
						return $search_data_subject;
					},
					_td_ = function () {
						var searchDataArray = {
								'td_': true,
							},
							ajaxData = {
								'pedagoge_callback_function': '_td_ajax',
							};
						
						bindAjax( searchDataArray, ajaxData, function ( data ) {
							$search_data_teachers = $.parseJSON( htmlHandle.htmlUnescape( data ) );
						} );
						return $search_data_teachers;
					},
					/**
					 * Bind ajax
					 * @param param
					 * @param ajaxData
					 * @param callback_fn
					 */
					bindAjax = function ( param, ajaxData, callback_fn ) {
						ajaxSearchCompleted = false;
						loadSearchAjax( param, "values", ajaxData ).done( function ( data ) {
							ajaxSearchCompleted = true;
							callback_fn( data );
						} );
					},
					loadSearchAjax = function ( param, identifier, ajaxData ) {
						var dynamicData = {};
						dynamicData[identifier] = param;
						dynamicData['nonce'] = $ajaxnonce;
						dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
						dynamicData['pedagoge_callback_class'] = 'ControllerSearch';
						dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
						return $.ajax( {
							url: $ajax_url,
							type: "post",
							data: dynamicData
						} );
					},
					filterStyleCorrector = function () {
						//side filter bar position and style props maintained here.
						if ( ! settingsObj.$isMobileTabletEl.is( ':visible' ) ) {
							// medium devices and up
							settingsObj.other_filters_search_card_wrapper.removeClass( 'mobileScroll' ).addClass( 'fixedWidthStyle' );
						}
						else {
							//smartphones and tablets
							settingsObj.other_filters_search_card_wrapper.removeClass( 'fixedWidthStyle' ).addClass( 'mobileScroll' );
						}
					},
					searchItemsStyleCorrector = function () {
						$( document ).ajaxComplete( function () {
							if ( ! settingsObj.$isMobileTabletEl.is( ':visible' ) ) {
								// medium devices and up
								settingsObj.search_result_item_cards.find( ".card" ).removeClass( 'search-clickable-card' );
							}
							else {
								//smartphones and tablets
								settingsObj.search_result_item_cards.find( ".card" ).addClass( 'search-clickable-card' );
							}
						} );
					},
					searchBtnHandler = function () {
						//search button on click handler
						settingsObj.search_btn.on( 'click', function () {
							switch ( settingsObj.search_page_type ) {
								case 'search':
									searchObj.fetchSearchData( true );
									break;
								case 'home':
								default:
									var search_url;
									search_url = site_var.url + "search?pdg_subject=" + rtrim( subject_value, "," ) + "&pdg_locality=" + rtrim( locality_ids, "," );
									window.location = encodeURI( search_url );
									break;
							}
						} );
					},
					resizeHandler = function () {
						$( window ).resize( function () {
							filterStyleCorrector();
							searchItemsStyleCorrector();
						} ).resize();
					},
					scrollHandler = function () {
						$( window ).scroll( function () {
							/*if (!settingsObj.$isMobileTabletEl.is(':visible')) {
							 //Keeps the filter bar right side stick to left and stop following the scroll; this is only for "fixed width window"
							 settingsObj.other_filters_search_card_wrapper.css('left', -$(window).scrollLeft());
							 }*/
							//Keeps the filter bar have a fixed scroll to the left
							filterSideBarScrollPosition();
						} );
					},
					filterSideBarScrollPosition = function () {
						filterStyleCorrector();
						if ( settingsObj.other_filters_search_card_wrapper.length > 0 && ! settingsObj.$isMobileTabletEl.is( ':visible' ) ) {
							//sidebar - filter
							if ( settingsObj.other_filters_search_card_wrapper.offset().top + settingsObj.other_filters_search_card_wrapper.outerHeight()
								>= settingsObj.site_footer.offset().top ) {
								settingsObj.other_filters_search_card_wrapper.css( {
									'position': 'absolute',
									'bottom': '-' + (settingsObj.site_footer.offset().top - settingsObj.site_footer.outerHeight() - settingsObj.load_more_wrapper.outerHeight() + 5) + 'px'
									//todo:  these can be made fixed values
								} );
							}
							if ( $( document ).scrollTop() + window.innerHeight < settingsObj.site_footer.offset().top ) {
								settingsObj.other_filters_search_card_wrapper.css( {
									'position': 'fixed',
									'top': 'auto',
									'bottom': 'auto'
								} );
							}
						}
					},
					placeholderFix = function () {
						/*Sometimes the placeholder breaks due to a dynamic value insert from selectize js file.
						 * This is one time fix for initial load condition*/
						var selectizedSearchInputSelector = {
							'locality': 'input#search-locality-selectized',
							'subject': 'input#search_subject-selectized',
							'teacher': 'input#search_teacher-selectized',
						};
						if ( typeof $search_data_subject != "undefined" && $search_data_subject.status == "success" ) {
							waitForElementLoad( selectizedSearchInputSelector.subject, function () {
								if ( ! search_locality_loaded ) {
									$( selectizedSearchInputSelector.subject ).css( { 'width': '200' } );
								}
							}, 500 );
						}
						if ( typeof $search_data_locality != "undefined" && $search_data_locality.status == "success" ) {
							waitForElementLoad( selectizedSearchInputSelector.locality, function () {
								if ( ! search_subject_loaded ) {
									$( selectizedSearchInputSelector.locality ).css( { 'width': '200' } );
								}
							}, 500 );
						}
						if ( typeof $search_data_teachers != "undefined" && $search_data_teachers.status == "success" ) {
							waitForElementLoad( selectizedSearchInputSelector.teacher, function () {
								if ( ! search_subject_loaded ) {
									$( selectizedSearchInputSelector.teacher ).css( { 'width': 'auto' } );
								}
							}, 500 );
						}
					},
					popoverFix = function () {
						$( document ).ajaxComplete( function () {
							settingsObj.$bodyEl.popover( {
								selector: settingsObj.dataTogglePopover,
								trigger: "hover click",
								placement: 'top',
								html: false,
								delay: {
									show: 500,
									hide: 100
								},
							} ).on( "show.bs.popover", function ( e ) {
								// hide all other popovers
								setTimeout( function () {
									$( '.popover' ).fadeOut( 'slow' );
								}, 2500 );
								$( settingsObj.dataTogglePopover ).not( e.target ).popover( "destroy" );
								settingsObj.$popoverEl.remove();
							} );
						} );
					},
					makeCardClickable = function () {
						//make the entire search card clickable while maintaining the default action for the buttons/anchors
						$( document ).ajaxComplete( function () {
							var flagClickedBtns = false;
							$( ".search-clickable-card" ).on( 'click', 'button, a, .btn, [data-toggle="popover"]', function ( e ) {
								flagClickedBtns = true;
								return;
							} ).click( function ( e ) {
								if ( e.target != this ) {
									if ( ! flagClickedBtns ) {
										window.location.href = $( this ).data( 'url' );
									} else {
										flagClickedBtns = false;
									}
									return;
								}
							} );
						} );
					},
					
					startFilterHandler = function () {
						settingsObj.start_filter.on( 'click', function () {
							settingsObj.search_result_main.removeClass( 'background-adjust' ).addClass( 'padding-top-filter' );
						} );
					},
					exitFilterHandler = function () {
						settingsObj.exitSearch.on( 'click', function () {
							settingsObj.search_result_main.addClass( 'background-adjust' ).removeClass( 'padding-top-filter' );
						} );
					},
					prefill_search_result_item_req_call = function () {
						//search page request callback button click action; such as prefill the subject and locality values
						$( document ).ajaxComplete( function () {
							var $callback_teachername = $( "#callback_teachername" ),
								$callback_url = $( "#callback_url" ),
								$search_result_item_req_call = $( ".search_result_item_req_call" ),
								$post_req_no_result = $( ".post_req_no_result" );
							$search_result_item_req_call.attr( 'disabled', false );
							$search_result_item_req_call.add( $post_req_no_result ).on( 'click', function () {
								var locality_names = get_locality_name_by_id( dynamic_locality_ids, true ),
									target_id_var = $( this ).data( 'target' );
								$( target_id_var ).find( '.show_result' ).html( "" );
								settingsObj.$callback_loc_El.val( locality_names ).parent( ".form-group.label-floating.is-empty" ).removeClass( "is-empty" );
								settingsObj.$callback_sub_El.val( dynamic_subject_value ).parent( ".form-group.label-floating.is-empty" ).removeClass( "is-empty" );
							} );
							$search_result_item_req_call.on( 'click', function () {
								$callback_teachername.val( $( this ).parents( ".card" ).find( '.name.item' ).text() ).parent( ".form-group.label-floating.is-empty" ).removeClass( "is-empty" );
								$callback_url.val( $( this ).parents( ".card" ).data( 'url' ) ).parent( ".form-group.label-floating.is-empty" ).removeClass( "is-empty" );
							} );
							
							//Request callback on Empty search results
							$post_req_no_result.on( 'click', function () {
								$callback_teachername.val( "Empty Search results; < No Name >" ).parent( ".form-group.label-floating.is-empty" ).removeClass( "is-empty" );
								$callback_url.val( "Empty Search results; < No url >" ).parent( ".form-group.label-floating.is-empty" ).removeClass( "is-empty" );
							} );
						} );
					},
					search_result_profile_view_button_handler = function () {
						//search_result_profile_view_button action
						$( document ).ajaxComplete( function () {
							$( '.search_result_profile_view_button, a.search_result_profile_view_link' ).on( "click", function () {
								if ( subject_value != "" ) {
									var url = $( this ).attr( 'href' ),
										tracking_url = url + "#subjects=" + rtrim( subject_value, "," );
									if ( pdg_sanitize.noundefined( urlParam.parsehash( 'subjects', url ) ) == "" ) {
										$( this ).attr( 'href', encodeURI( tracking_url ) );
									}
								}
							} );
						} );
					};
			}, desktopSearchObj;
		
		desktopSearchObj = new PDG_Search_Desktop;
		desktopSearchObj.init();
	} );
} );
