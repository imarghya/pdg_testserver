dynamic_locality_ids = locality_ids = pdg_sanitize.noundefined( urlParam.get( 'pdg_locality' ) );
dynamic_subject_value = subject_value = pdg_sanitize.noundefined( urlParam.get( 'pdg_subject' ) );

var PDG_Search, teacher_value;
(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
		PDG_Search = function ( numberOfItemsPerPage_param, instantSearch_param ) {
			var mainObj = this, pageNumber = 0, searchDataArray, settingsObj, template_theme_search_result_item_cards, json_search_results, ajaxSearchCompleted = true, searchResults, locationTypeValueArray;
			numberOfItemsPerPage_param = pdg_sanitize.inputdefaultparam( numberOfItemsPerPage_param, 16 );
			instantSearch_param = pdg_sanitize.inputdefaultparam( instantSearch_param, false );
			
			//Settings
			mainObj.settings = function () {
				var search_result_item_cards_area = $( ".search_result_item_cards" ),
					search_result_item_load_more_area = $( ".search_result_item_load_more" ),
					spinnerObj = new spinnerHandle( {
						'parentclass': '.pageLoadingSpinner',
						'size': 'small',
						'positionclass': 'text-center'
					} ),
					moreButton = $( '.search_result_load_more_button' ),
					$bodyEl = $( 'body' ),
					$isMobile = $( '.isMobile' ),
					filterButton = $( '.filterBtn' ),
					resetButton = $( '.resetBtn' ),
					sortButton = $( '.sortBtn' ),
					exitSearch = $( '.exit_search' ),
					start_filter = $( ".start_filter" ),
					start_sort = $( ".start_sort" ),
					hide_on_search = $( ".hide_on_search" ),
					show_on_search = $( ".show_on_search" ),
					show_on_sort = $( ".show_on_sort" ),
					search_result_noitem_cards = $( ".search_result_noitem_cards" ),
					search_field = $( "#search" ),
					locationType = $( '.checkbox.chk_filter_teacher_location_type [type="checkbox"]' ),
					formFilter = $( '.filter-form' ),
					search_result_item_cards_template = $( '#search_result_item_cards_template' ),
					search_suggested_locality_ribbon = $( '#search_suggested_locality_ribbon' ),
					$checkBoxEl = $( '.checkbox [type="checkbox"]' ),
					filter_institute_teacher = $checkBoxEl.filter( '.chk_filter_institute_teacher' ),
					filter_locationType = $checkBoxEl.filter( '.chk_filter_teacher_location_type' ),
					filter_teaching_xp = $checkBoxEl.filter( '.chk_filter_teaching_xp' ),
					filter_class_type = $checkBoxEl.filter( '.chk_filter_class_type' ),
					filter_academic_board = $checkBoxEl.filter( '.chk_filter_academic_board' ),
					filter_fees_range = $checkBoxEl.filter( '.chk_filter_fees_range' ),
					filter_days_taught = $checkBoxEl.filter( '.chk_filter_days_taught' ),
					filter_days_taught_all_day = $( '.chk_filter_days_taught_all_day' ),
					sort_exp = $checkBoxEl.filter( '.chk_sort_exp' ),
					sort_reviews = $checkBoxEl.filter( '.chk_sort_reviews' ),
					sort_fee = $checkBoxEl.filter( '.chk_sort_fee' ),
					$searched_subject_name = $( '.searched_subject_name' ),
					$searched_locality_name = $( '.searched_locality_name' ),
					$search_result_profile_view_button = $( '.search_result_profile_view_button' ),
					$callback_sub_El = $( '#callback_sub' ),
					$callback_loc_El = $( '#callback_loc' );
				return {
					instantSearch: instantSearch_param,
					numberOfItemsPerPage: numberOfItemsPerPage_param,
					search_result_item_cards_area: search_result_item_cards_area,
					search_result_item_load_more_area: search_result_item_load_more_area,
					$bodyEl: $bodyEl,
					$isMobile: $isMobile,
					spinnerObj: spinnerObj,
					moreButton: moreButton,
					filterButton: filterButton,
					resetButton: resetButton,
					sortButton: sortButton,
					exitSearch: exitSearch,
					start_filter: start_filter,
					start_sort: start_sort,
					search_result_noitem_cards: search_result_noitem_cards,
					hide_on_search: hide_on_search,
					show_on_search: show_on_search,
					show_on_sort: show_on_sort,
					search_field: search_field,
					locationType: locationType,
					formFilter: formFilter,
					search_result_item_cards_template: search_result_item_cards_template,
					search_suggested_locality_ribbon: search_suggested_locality_ribbon,
					$checkBoxEl: $checkBoxEl,
					filter_institute_teacher: filter_institute_teacher,
					filter_locationType: filter_locationType,
					filter_teaching_xp: filter_teaching_xp,
					filter_class_type: filter_class_type,
					filter_academic_board: filter_academic_board,
					filter_fees_range: filter_fees_range,
					filter_days_taught: filter_days_taught,
					filter_days_taught_all_day: filter_days_taught_all_day,
					sort_exp: sort_exp,
					sort_reviews: sort_reviews,
					sort_fee: sort_fee,
					$searched_subject_name: $searched_subject_name,
					$searched_locality_name: $searched_locality_name,
					$search_result_profile_view_button: $search_result_profile_view_button,
					$callback_sub_El: $callback_sub_El,
					$callback_loc_El: $callback_loc_El,
				};
			};
			
			//Load initial page search results
			mainObj.init = function () {
				settingsObj = mainObj.settings();
				mainObj.fetchSearchData();
				mainObj.bindUIActions();
			};
			
			mainObj.themes = {
				search_result_item_card: mainObj.settings().search_result_item_cards_template.html(),
				search_suggested_locality_ribbon: mainObj.settings().search_suggested_locality_ribbon.html()
			};
			
			var common_search_data = function () {
				return searchDataArray = {
					"loggedin_user_id": pdg_sanitize.noundefined( $current_user_id ),
					"loggedin_user_role": pdg_sanitize.noundefined( $current_user_role ),
					"subject_name": pdg_sanitize.inputdefaultparam( pdg_sanitize.noundefined( subject_value ), pdg_sanitize.noundefined( settingsObj.$searched_subject_name.data( 'value' ) ) ),
					"locality_names": pdg_sanitize.inputdefaultparam( pdg_sanitize.noundefined( locality_ids ), pdg_sanitize.noundefined( settingsObj.$searched_locality_name.data( 'value' ) ) ),
					"location_type_array": pdg_sanitize.inputdefaultparam( locationTypeValueArray, mainObj.getFilterCheckedItems( settingsObj.filter_locationType ) ),
					"filter_institute_teacher_array": mainObj.getFilterCheckedItems( settingsObj.filter_institute_teacher ),
					"teaching_xp_array": mainObj.getFilterCheckedItems( settingsObj.filter_teaching_xp ),
					"class_type_array": mainObj.getFilterCheckedItems( settingsObj.filter_class_type ),
					"academic_board_array": mainObj.getFilterCheckedItems( settingsObj.filter_academic_board ),
					"fees_range_array": mainObj.getFilterCheckedItems( settingsObj.filter_fees_range ),
					"days_taught_array": mainObj.getFilterCheckedItems( settingsObj.filter_days_taught ),
					"sort_exp_array": mainObj.getFilterCheckedItems( settingsObj.sort_exp ),
					"sort_reviews_array": mainObj.getFilterCheckedItems( settingsObj.sort_reviews ),
					"sort_fee_array": mainObj.getFilterCheckedItems( settingsObj.sort_fee ),
					'found_locality_name': '',
					'institute_teacher_name': pdg_sanitize.noundefined( teacher_value ),
					'class_timing_array': '',
					"pagination_no": pageNumber,
					"search_type": "filter",
					"no_of_records_per_page": settingsObj.numberOfItemsPerPage,
				};
			};
			
			/**
			 * Search data is handled and routed via ajax here
			 */
			mainObj.fetchSearchData = function ( clearResultData ) {
				clearResultData = pdg_sanitize.inputdefaultparam( clearResultData, false );
				if ( clearResultData ) {
					pageNumber = 0;
					settingsObj.search_result_item_cards_area.html( "" );
				}
				mainObj.deactivateFormElements();
				var searchDataArray = common_search_data(),
					ajaxData = {
						'pedagoge_callback_function': 'fn_search_ajax',
					};
				
				mainObj.bindAjax( searchDataArray, ajaxData );
				return searchDataArray;
			};
			
			/**
			 * Clubbed Localities Search suggestion data is handled and routed via ajax here
			 * Author - Niraj; Created 12-Dec-2016
			 * Modified - Ganesh; Modified 13-Dec-2016
			 * Optimized: added: deactivateFormElements, minimized the code and reused the modular components here
			 */
			mainObj.clubbedLocalities = function ( param ) {
				if ( param == "" ) {
					return false;
				}
				mainObj.deactivateFormElements();
				var searchDataArray = common_search_data(),
					ajaxData = {
						'pedagoge_callback_function': 'fn_search_clubbed_locality_ajax',
					};
				mainObj.bindAjax( searchDataArray, ajaxData );
			};
			
			
			mainObj.loadSearchAjax = function ( param, identifier, ajaxData ) {
				var dynamicData = {};
				dynamicData[identifier] = param;
				dynamicData['nonce'] = $ajaxnonce;
				dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
				dynamicData['pedagoge_callback_class'] = 'ControllerSearch';
				dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
				return $.ajax( {
					url: $ajax_url,
					type: "post",
					data: dynamicData
				} );
			};
			
			mainObj.bindAjax = function ( param, ajaxData ) {
				ajaxSearchCompleted = false;
				settingsObj.search_result_noitem_cards.addClass( "hide" );
				mainObj.loadSearchAjax( param, "searchData", ajaxData ).done( function ( data ) {
					ajaxSearchCompleted = true;
					mainObj.activateFormElements();
					mainObj.renderSearchResults( JSON.parse( data ) );
				} );
			};
			
			mainObj.renderSearchResults = function ( param ) {
				json_search_results = param;
				var initialLoad = false;
				if ( settingsObj.search_result_item_cards_area.hasClass( 'initialLoad' ) ) {
					initialLoad = true;
					settingsObj.search_result_item_cards_area.removeClass( 'initialLoad' );
				}
				if ( param.status == "error" ) {
					switch ( param.type ) {
						case 'lastpage':
							settingsObj.moreButton.hide();
							break;
						case 'endofitem':
							mainObj.clubbedLocalities( locality_ids );
							settingsObj.moreButton.hide();
							break;
						case 'empty':
							settingsObj.search_result_noitem_cards.removeClass( "hide" );
							settingsObj.moreButton.hide();
							break;
						case 'security':
							alertMessagesToasts.error( "Session Expired! Please reload the page again." );
							break;
						default:
							break;
					}
					return;
				}
				
				if ( param.status == "success" ) {
					switch ( param.type ) {
						case 'lastpage':
							mainObj.clubbedLocalities( locality_ids );
							settingsObj.moreButton.hide();
							break;
						case 'empty_clubbed_locality':
							settingsObj.moreButton.hide();
							break;
						case 'clubbed_locality_end':
							settingsObj.search_result_item_cards_area.append( mainObj.themes.search_suggested_locality_ribbon );
							settingsObj.moreButton.hide();
							break;
						default:
							break;
					}
				}
				if ( param.items.length <= 0 && pageNumber == 0 && settingsObj.search_result_item_cards_area.length < 1 ) {
					settingsObj.search_result_noitem_cards.removeClass( "hide" );
				}
				
				template_theme_search_result_item_cards = mainObj.themes.search_result_item_card;
				Mustache.parse( template_theme_search_result_item_cards );
				if ( ! initialLoad ) {
					settingsObj.search_result_item_cards_area.append( Mustache.render( template_theme_search_result_item_cards, json_search_results ) );
				}
				if ( settingsObj.$isMobile.is( ':visible' ) && pageNumber == 0 ) {
					//smartphones
					settingsObj.$bodyEl.animate( {
						scrollTop: settingsObj.search_result_item_cards_area.offset().top - 60
					} );
				}
			};
			
			mainObj.bindUIActions = function () {
				//More button init
				settingsObj.moreButton.on( "click", function () {
					pageNumber ++;
					mainObj.fetchSearchData();
				} );
				
				//Go button init;
				settingsObj.filterButton.on( 'click', function () {
					mainObj.fetchSearchData( true );
					settingsObj.exitSearch.trigger( "click" );
				} );
				
				//exitSearch button
				settingsObj.exitSearch.on( 'click', function () {
					settingsObj.show_on_sort.add( settingsObj.show_on_search ).hide();
					settingsObj.show_on_sort.add( settingsObj.hide_on_search ).show().removeClass( 'hide' );
				} );
				
				//sort button
				settingsObj.sortButton.on( 'click', function () {
					mainObj.fetchSearchData( true );
					settingsObj.show_on_sort.hide();
					settingsObj.exitSearch.trigger( "click" );
				} );
				
				//start filter
				settingsObj.start_filter.on( 'click', function () {
					settingsObj.hide_on_search.hide().addClass( 'hide' );
					settingsObj.show_on_sort.hide();
					settingsObj.show_on_search.show();
					//settingsObj.search_field.focus();
				} );
				
				//start sort
				settingsObj.start_sort.on( 'click', function () {
					settingsObj.hide_on_search.hide().addClass( 'hide' );
					settingsObj.show_on_search.hide();
					settingsObj.show_on_sort.show();
				} );
				
				//resetButton action
				settingsObj.resetButton.on( "click", function () {
					settingsObj.formFilter.find( 'input[type=checkbox]' ).prop( 'checked', false );
					mainObj.fetchSearchData( true );
				} );
				
				//filter_days_taught_all_day action
				settingsObj.filter_days_taught_all_day.on( "click", function () {
					settingsObj.filter_days_taught.prop( 'checked', this.checked )
				} );
				
				
				mainObj.checkboxToRadio( [
					{
						selector: settingsObj.filter_institute_teacher,
					}, {
						selector: settingsObj.sort_exp
					}, {
						selector: settingsObj.sort_reviews,
					}, {
						selector: settingsObj.sort_fee,
					}
				] );
				
				//Teaching Location (Place:); Instant filtering field
				settingsObj.locationType.change( function () {
					locationTypeValueArray = settingsObj.locationType.filter( ':checked' ).map( function () {
						return this.value
					} ).get();
					mainObj.fetchSearchData( true );
				} );
				
				//Instant filtering fields; All checkfield fields in the "form-filter"
				if ( settingsObj.instantSearch ) {
					settingsObj.formFilter.find( 'input[type=checkbox]' ).change( function () {
						mainObj.fetchSearchData( true );
					} );
				}
			};
			
			mainObj.getFilterCheckedItems = function ( selector ) {
				return pdg_sanitize.noundefined( selector.filter( ':checked' ).map( function () {
					return this.value
				} ).get(), true );
			};
			
			//element state WHILE ajax is loading
			mainObj.deactivateFormElements = function () {
				settingsObj.spinnerObj.show();
				//todo: make the select disable and enable working
				settingsObj.formFilter.find( 'input, textarea, button' ).attr( 'disabled', true );
				settingsObj.moreButton.hide();
				var $search_result_item_req_call = $( ".search_result_item_req_call" );
				$search_result_item_req_call.attr( 'disabled', true );
			};
			
			//element state AFTER ajax load is complete
			mainObj.activateFormElements = function () {
				settingsObj.spinnerObj.hide();
				//todo: make the select disable and enable working
				settingsObj.formFilter.find( 'input, textarea, button' ).attr( 'disabled', false );
				settingsObj.moreButton.removeClass( 'hide' ).show().disable( false );
			};
			
			//Disallow multiple values for checkbox
			mainObj.checkboxToRadio = function ( param ) {
				param.map( function ( obj ) {
					obj.selector.on( 'change', function () {
						obj.selector.not( this ).prop( 'checked', false );
					} );
				} )
			};
		};
	} );
} );
