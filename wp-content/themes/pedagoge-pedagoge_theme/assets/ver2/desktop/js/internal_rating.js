(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
			var PDG_rating = function () {
				var mainObj = this, settingsObj;
				//Settings
				mainObj.settings = function () {
					var $bodyEl = $( 'body' ),	
						cmd_reload_teachers_internal_rating = $("#cmd_reload_teachers_internal_rating"),
						modal_teachers_internal_rating = $("#modal_teachers_internal_rating"),
						txt_teacher_review = $("#txt_teacher_review"),
												
						div_teacher_internal_rating_save_result_area = $("#div_teacher_internal_rating_save_result_area"),						
						cmd_save_teachers_internal_rating = $("#cmd_save_teachers_internal_rating"),						
						div_teacher_internal_rating = $("#div_teacher_internal_rating"),
						hidden_teacher_user_id = $("#hidden_teacher_user_id").val();						
					
					return {
						cmd_reload_teachers_internal_rating : cmd_reload_teachers_internal_rating,
						modal_teachers_internal_rating : modal_teachers_internal_rating,
						txt_teacher_review : txt_teacher_review,

						div_teacher_internal_rating_save_result_area : div_teacher_internal_rating_save_result_area,						
						cmd_save_teachers_internal_rating : cmd_save_teachers_internal_rating,						
						div_teacher_internal_rating : div_teacher_internal_rating,
						hidden_teacher_user_id : hidden_teacher_user_id,
					};
				};
				
				//Load initial page search results
				mainObj.init = function () {
					settingsObj = mainObj.settings();
					mainObj.bindUIActions();
				};
				
				mainObj.bindUIActions = function () {
				    //resetRatingModal();
					reloadInternalRating();
					saveInternalRating();
					
				};
				
				
				
                    /*********** internal rating *********/ 

//                    resetRatingModal = function() {
//						fn_reset_internal_rating_taking_form();
//						settingsObj.modal_teachers_internal_rating.on('hidden.bs.modal', function(e){							
//							fn_reset_internal_rating_taking_form();							
//						});
//					},
//					fn_reset_internal_rating_taking_form(); = function() {						
//						settingsObj.txt_teacher_review.val('');
//						settingsObj.div_teacher_internal_rating_save_result_area.html('');
//					},					
					reloadInternalRating = function() {
						settingsObj.cmd_reload_teachers_internal_rating.on('click', function(){
							//console.log("this is working");
							settingsObj.cmd_reload_teachers_internal_rating.button( 'loading' );
							var submit_data = {
								teacher_user_id: $("#hidden_teacher_user_id").val(),
								
							};
							var ajaxData = {
								'pedagoge_callback_class': 'ControllerTeacher',
								'pedagoge_callback_function': 'fn_return_teacher_internal_rating_ajax',
							};
							pdg_ajax_handler( submit_data, "teacherInfo", ajaxData ).done( function ( data ) {
								try {
									var $response_data = $.parseJSON( data ),
										$error = $response_data.error,
										$error_type = $response_data.error_type,
										$message = $response_data.message,
										$data = $response_data.data;
									if ( ! $error ) {
										settingsObj.div_teacher_internal_rating.html($data);
										$.notify($message, 'success');
									} else {
										$.notify($message, 'error');
									}
								} catch ( err ) {
								} finally {
									settingsObj.cmd_reload_teachers_internal_rating.button( 'reset' );
								}
							} );
						});
					},
					saveInternalRating = function() {
						settingsObj.cmd_save_teachers_internal_rating.on('click', function(){
							var $teacher_review = settingsObj.txt_teacher_review.val();//$("#txt_teacher_review").val();
		
							var $result_area = settingsObj.div_teacher_internal_rating_save_result_area;
							var $rating_area = settingsObj.div_teacher_internal_rating;
							
							//settingsObj.cmd_save_teachers_internal_rating.button( 'loading' );
							var radioValue = $("input[name='payment_plan']:checked").val();
							var submit_data = {
								teacher_user_id: settingsObj.hidden_teacher_user_id,
								teacher_review : $teacher_review,
								punctuality_rating : $("#punctuality_rating").val(),
								responsiveness_rating : $("#responsiveness_rating").val(),
								behaviour_rating : $("#behaviour_rating").val(),
								communication_skills_rating : $("#communication_skills_rating").val(),
								teacher_reviews_text : $("#txt_teacher_review").val(),
								rating_giver_name : $("#txt_teacher_rating_maker").val(),
								classes_accepted :  $("#classes_accepted").val(),
								classes_provided : $("#classes_provided").val(),
								demo_converted : $("#demo_converted").val(),
								demo_provided : $("#demo_provided").val(),
								classes_ongoing : $("#classes_ongoing").val(),
								payment_plan : radioValue,
							};
							var ajaxData = {
								'pedagoge_callback_class': 'ControllerTeacher',
								'pedagoge_callback_function': 'fn_save_teacher_internal_rating_ajax',
							};
							var classes_accepted =  $("#classes_accepted").val();
							var classes_provided = $("#classes_provided").val();
							var demo_converted = $("#demo_converted").val();
							var demo_provided = $("#demo_provided").val();
							var demo_converted_fix =$("#demo_converted_fix").val();
							var classes_ongoing = $("#classes_ongoing").val();
							var err1=0;
							var err2=0;
							var err3=0;
							var err4=0;
							if (classes_accepted > classes_provided) {
							 $.notify('Class Accepted must be less than or equal to Class Provided!', 'error');
							 err1=1;
							}
							if (demo_converted > demo_provided) {
							$.notify('Demo Converted must be less than or equal to Demo Provided!', 'error');
							err2=1;
							}
							if (demo_converted < demo_converted_fix) {
							$.notify('Demo Converted Can not be less than last converted data!', 'error');	//code
							err3=1;
							}
							if (classes_ongoing > classes_accepted) {
							$.notify('Classes Ongoing must be less than or equal to Class Accepted!!', 'error');	//code
							err4=1;
							}
							if(err1==0 && err2==0 && err3==0 && err4==0){
								pdg_ajax_handler( submit_data, "ratingData", ajaxData ).done( function ( data ) {
									try {
										var $response_data = $.parseJSON( data ),
											$error = $response_data.error,
											$error_type = $response_data.error_type,
											$message = $response_data.message,
											$data = $response_data.data;
										if ( ! $error ) {
											//settingsObj.div_teacher_notes.html($data);
											$.notify($message, 'success');
											//fn_reset_notes_taking_form();
											//settingsObj.cmd_reload_teachers_internal_rating.click();
											load_rating();
										} else {
											$.notify($message, 'error');
										}
									} catch ( err ) {
									} finally {
										settingsObj.cmd_save_teachers_internal_rating.button( 'reset' );
										//test
									}
								});
							}
						});		
					};


					/*********** internal rating *********/

			}, ratingObj;
			ratingObj = new PDG_rating;
			ratingObj.init();
			
		}
	);
} );
/*******************************************/
function load_rating() {
var submit_data = {
teacher_user_id: $("#hidden_teacher_user_id").val(),

};
var ajaxData = {
'pedagoge_callback_class': 'ControllerTeacher',
'pedagoge_callback_function': 'fn_return_teacher_internal_rating_ajax',
};
pdg_ajax_handler( submit_data, "teacherInfo", ajaxData ).done( function ( data ) {
try {
	var $response_data = $.parseJSON( data ),
		$error = $response_data.error,
		$error_type = $response_data.error_type,
		$message = $response_data.message,
		$data = $response_data.data;
	if ( ! $error ) {
		$("#div_teacher_internal_rating").html($data);
		//$.notify($message, 'success');
	} else {
		$.notify($message, 'error');
	}
} catch ( err ) {
} finally {
	//settingsObj.cmd_reload_teachers_internal_rating.button( 'reset' );
}
} );	//code
}