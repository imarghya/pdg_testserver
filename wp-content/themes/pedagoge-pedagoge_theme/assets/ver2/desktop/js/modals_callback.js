(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
		
		var PDG_Modals_Callback = function () {
			var mainObj = this, settingsObj;
			//Settings
			//todo: work on the script to accept data from multiple modals in the same page
			mainObj.settings = function () {
				var $bodyEl = $( 'body' ),
					show_result_var = '.show_result',
					$checkBoxEl = $( '[type="checkbox"]' ),
					callback_init_var = '.callback_init',
					$callback_El = $( callback_init_var ),
					$modal_El = $( '.modal' ),
					$submit_btn = $modal_El.find( '.submit' ),
					$contact_form_El = $( '.contact_form' ),
					$contact_submit_btn = $contact_form_El.find( '.contact_submit' );
				
				return {
					$bodyEl: $bodyEl,
					callback_init_var: callback_init_var,
					$callback_El: $callback_El,
					$modal_El: $modal_El,
					show_result_var: show_result_var,
					$submit_btn: $submit_btn,
					$contact_submit_btn: $contact_submit_btn,
					$contact_form_El: $contact_form_El,
					$checkBoxEl: $checkBoxEl,
				};
			};
			
			//Load initial page search results
			mainObj.init = function () {
				settingsObj = mainObj.settings();
				mainObj.bindUIActions();
			};
			
			mainObj.bindUIActions = function () {
				mainObj.callback_button_action();
				mainObj.callback_submit_button_action();
			};
			
			
			mainObj.callback_button_action = function () {
			};
			
			mainObj.callback_submit_button_action = function () {
				settingsObj.$submit_btn.on( 'click', function () {
					$( this ).button( 'loading' );
					mainObj.btnFireAction( "modal", $( this ) );
					$( this ).button( 'reset' );
				} );
				settingsObj.$contact_submit_btn.on( 'click', function ( e ) {
					$( this ).button( 'loading' );
					mainObj.btnFireAction( "contact_form", $( this ) );
					$( this ).button( 'reset' );
					e.preventDefault();
				} );
			};
			
			mainObj.btnFireAction = function ( type, $this ) {
				var $callback_El = $this.parents( settingsObj.callback_init_var ),
					$show_result = $callback_El.find( settingsObj.show_result_var ),
					$callback_name = $callback_El.data( 'callback-name' ),
					//callback_name = pdg_sanitize.noundefined( $callback_name ),
					callback_name=$callback_name,
					$callbackForm_El = $callback_El.find( 'form' );
				
				
				if ( callback_name == "" ) {
					return false;
				}
				$show_result.html( "" );
				$callbackForm_El.validationEngine( 'attach', {
					scroll: false,
					autoHidePrompt: true,
					autoHideDelay: 3500,
					fadeDuration: 0.3,
					promptPosition: "topLeft:0"
				} );
				
				switch ( callback_name ) {
					case 'home_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.home_callbackForm( $callback_El, $show_result );
						}
						break;
					case 'search_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.search_callbackForm( $callback_El, $show_result );
						}
						break;
					case 'home_contactus_form':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.home_contactus_form( $callback_El, $show_result );
						}
						break;
					case 'profile_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.profile_callback_form( $callback_El, $show_result );
						}
						break;
					case 'profile_takeademo_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.profile_takeademo_callback( $callback_El, $show_result );
						}
						break;
					case 'profile_takethiscourse_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.profile_takethiscourse_callback( $callback_El, $show_result );
						}
						break;
					case 'profile_review_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.profile_review_callback( $callback_El, $show_result );
						}
						break;
					case 'institution_callback_form':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.institution_callback_form( $callback_El, $show_result );
						}
						break;
					case 'institution_callback_form-xs':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.institution_callback_form_xs( $callback_El, $show_result );
						}
						break;
					case 'register_as_trainer_modal_callback':
						if ( $callbackForm_El.validationEngine( 'validate' ) ) {
							callback_form_run.register_as_trainer_modal_callback( $callback_El, $show_result );
						}
						break;
					default:
						break;
				}
			};
			
			/**
			 * Define the callback from actions here
			 */
			var callback_form_run = {
					home_callbackForm: function ( $callback_El, $show_result ) {
						var data = {
								"subject": $( '#callback_sub' ).val(),
								"tuition_location": $( '#callback_tut_loc' ).val(),
								"locality": $( '#callback_loc' ).val(),
								"name": $( '#callback_name' ).val(),
								"phone": $( '#callback_phn' ).val(),
								"email": $( '#callback_email' ).val(),
							},
							ajaxData = {
								'pedagoge_callback_function': 'home_callback',
							};
						
						mainObj.loadAjax( data, "searchData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					home_contactus_form: function ( $callback_El, $show_result ) {
						var data = {
								"name": $( '#contact_name' ).val(),
								"phone": $( '#contact_phone' ).val(),
								"locality": $( '#contact_locality' ).val(),
								"subject": $( '#contact_subject' ).val(),
							},
							ajaxData = {
								'pedagoge_callback_function': 'home_contactus_form',
							};
						
						mainObj.loadAjax( data, "searchData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					search_callbackForm: function ( $callback_El, $show_result ) {
						var data = {
								"teachername": $( '#callback_teachername' ).val(),
								"pageurl": $( '#callback_url' ).val(),
								"subject": $( '#callback_sub' ).val(),
								"tuition_location": $( '#callback_tut_loc' ).val(),
								"locality": $( '#callback_loc' ).val(),
								"name": $( '#callback_name' ).val(),
								"phone": $( '#callback_phn' ).val(),
								"email": $( '#callback_email' ).val(),
								"pref_mode_comm_array": mainObj.getFilterCheckedItems( settingsObj.$checkBoxEl.filter( '.pref_mode_comm' ) ),
								"pref_time_comm_array": mainObj.getFilterCheckedItems( settingsObj.$checkBoxEl.filter( '.pref_time_comm' ) )
							},
							ajaxData = {
								'pedagoge_callback_function': 'search_callback',
							};
						
						mainObj.loadAjax( data, "searchData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					profile_callback_form: function ( $callback_El, $show_result ) {
						var data = {
								"teachername": $( '#callback_teachername' ).val(),
								"pageurl": window.location.href,
								"subject": $( '#callback_sub' ).val(),
								"tuition_location": $( '#callback_tut_loc' ).val(),
								"locality": $( '#callback_loc' ).val(),
								"name": $( '#callback_name' ).val(),
								"phone": $( '#callback_phn' ).val(),
								"email": $( '#callback_email' ).val(),
								"pref_mode_comm_array": mainObj.getFilterCheckedItems( settingsObj.$checkBoxEl.filter( '.pref_mode_comm' ) ),
								"pref_time_comm_array": mainObj.getFilterCheckedItems( settingsObj.$checkBoxEl.filter( '.pref_time_comm' ) )
							},
							ajaxData = {
								'pedagoge_callback_function': 'profile_callback_form',
							};
						
						mainObj.loadAjax( data, "searchData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					profile_takeademo_callback: function ( $callback_El, $show_result ) {
						var data = {
								"teachername": $( '#callback_teachername-takeademo' ).val(),
								"pageurl": window.location.href,
								"subject": $( '#callback_sub-takeademo' ).val(),
								"tuition_location": $( '#callback_tut_loc-takeademo' ).val(),
								"locality": $( '#callback_loc-takeademo' ).val(),
								"name": $( '#callback_name-takeademo' ).val(),
								"phone": $( '#callback_phn-takeademo' ).val(),
								"email": $( '#callback_email-takeademo' ).val(),
							},
							ajaxData = {
								'pedagoge_callback_function': 'profile_takeademo_callback',
							};
						
						mainObj.loadAjax( data, "searchData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					profile_takethiscourse_callback: function ( $callback_El, $show_result ) {
						var data = {
								"teachername": $( '#callback_teachername-takethiscourse' ).val(),
								"pageurl": $( '#callback_pageurl-takethiscourse' ).val(),
								"subject": $( '#callback_sub-takethiscourse' ).val(),
								"fees": $( '#callback_fees-takethiscourse' ).val(),
								"tuition_location": $( '#callback_tut_loc-takethiscourse' ).val(),
								"locality": $( '#callback_loc-takethiscourse' ).val(),
								"batchsize": $( '#callback_batchsize-takethiscourse' ).val(),
								"length": $( '#callback_length-takethiscourse' ).val(),
								"time": $( '#callback_time-takethiscourse' ).val(),
								"name": $( '#callback_name-takethiscourse' ).val(),
								"phone": $( '#callback_phn-takethiscourse' ).val(),
								"email": $( '#callback_email-takethiscourse' ).val(),
							},
							ajaxData = {
								'pedagoge_callback_function': 'profile_takethiscourse_callback',
							};
						
						mainObj.loadAjax( data, "searchData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					profile_review_callback: function ( $callback_El, $show_result ) {
						var $write_review_btn = $( ".write_review_btn" ),
							current_user_id = $write_review_btn.data( 'current_user' ),
							teacher_user_id = $write_review_btn.data( 'teacher_user_id' ),
							rating_data = [],
							errorMsgs = "",
							is_anonymous = 'no',
							is_recommending = false,
							pdg_star_rating,
							overall_rating_value,
							other_rating_value = 0,
							other_rating_empty_flag = true,
							review_text_min_count = 50,
							ratingObj = {
								ratedFill: "#19539f",
								halfStar: true,
							},
							$overall_rating = $( "#overall-rating" ).rateYo(),
							$star1_rating = $( "#star1-rating" ).rateYo(),
							$star2_rating = $( "#star2-rating" ).rateYo(),
							$star3_rating = $( "#star3-rating" ).rateYo(),
							$star4_rating = $( "#star4-rating" ).rateYo(),
							$review_text = $( "#txt_review_field" );
							
						if ( mainObj.getFilterCheckedItems( settingsObj.$checkBoxEl.filter( '.chk_anonymous_review' ) ).length > 0 ) {
							is_anonymous = 'yes';
						}
						if ( mainObj.getFilterCheckedItems( settingsObj.$checkBoxEl.filter( '.chk_recommend_teacher' ) ) ) {
							is_recommending = true;
						}
						
						if ( current_user_id <= 0 || teacher_user_id <= 0 ) {
							errorMsgs = 'Not Logged in!';
							
						}
						overall_rating_value = $overall_rating.rateYo( "rating" );
						pdg_star_rating = {
							star1: $star1_rating,
							star2: $star2_rating,
							star3: $star3_rating,
							star4: $star4_rating,
						};
						
						$.each( pdg_star_rating, function ( index, value ) {
							var rating_value = value.rateYo( "rating" ),
								star_data = {
									'name': index,
									'value': rating_value
								};
							if ( rating_value <= 0 ) {
								other_rating_empty_flag = false;
							}
							
							other_rating_value = rating_value + other_rating_value;
							rating_data.push( star_data );
						} );
						
						if ( overall_rating_value <= 0 ) {
							errorMsgs = 'Rating is mandatory.';
						}
						else if ( other_rating_value > 0 && ! other_rating_empty_flag ) {
							errorMsgs = 'All Detailed Ratings are mandatory.';
						}
						
						else if ( $review_text.val().trim().length < review_text_min_count ) {
							errorMsgs = 'Minimum ' + review_text_min_count + ' characters is required for review.';
						}
						
						if ( errorMsgs !== "" ) {
							pdg_show_alerts( $show_result, errorMsgs, 'error' );
							$callback_El.animate( {
								scrollTop: $show_result.offset().top - 50
							} );
							return;
						}
						var data = {
								"rated_by": current_user_id,
								"rated_to": teacher_user_id,
								"rating_data": rating_data,
								"review_text": $review_text.val().trim(),
								"overall_rating": overall_rating_value,
								"recommending" : is_recommending,
								"is_anonymous": is_anonymous,
							},
							ajaxData = {
								'pedagoge_callback_class': 'ControllerSearch',
								'pedagoge_callback_function': 'search_rating_and_review_ajax_ver2',
							};
						
						mainObj.loadAjax( data, "reviewData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El, function ( $response_data ) {
								var $error = $response_data.error;
								
								if ( ! $error ) {
									if ( is_recommending ) {
									 //$( ".recommend_btn" )[0].click();
									 var asset_url=$("#asset_url").val();
									$(".write_recomended").attr("idd",1);
									$(".write_recomended p").text('Unrecommend');
									$(".write_recomended img").attr('src',asset_url+'/teacher/img/un.png');
									$("#chk_recommend_teacher").attr("disabled",true);
									}
									$callback_El.find( ".hide_after_success" ).remove();
									$write_review_btn.prop( 'disabled', true );
									
									$(".write_review_btn p").text('Thanks for rating');
									$("#review-profile-modal").modal("hide");
									$("#modal_review_success").modal("show");
								}
							} );
						} );
						
						return data;
					},
					institution_callback_form: function ( $callback_El, $show_result ) {
						var data = {
								"fullname": $( '#fullname' ).val(),
								"inst_school_name": $( '#inst_school_name' ).val(),
								"designation": $( '#designation' ).val(),
								"phoneno": $( '#phoneno' ).val(),
								"emailid": $( '#emailid' ).val(),
							},
							
							ajaxData = {
								'pedagoge_callback_function': 'institution_callback_form',
							};
						
						mainObj.loadAjax( data, "contactData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					institution_callback_form_xs: function ( $callback_El, $show_result ) {
						var data = {
								"fullname": $( '#fullname-xs' ).val(),
								"inst_school_name": $( '#inst_school_name-xs' ).val(),
								"designation": $( '#designation-xs' ).val(),
								"phoneno": $( '#phoneno-xs' ).val(),
								"emailid": $( '#emailid-xs' ).val(),
							},
							
							ajaxData = {
								'pedagoge_callback_function': 'institution_callback_form',
							};
						
						mainObj.loadAjax( data, "contactData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					register_as_trainer_modal_callback: function ( $callback_El, $show_result ) {
						var data = {
								"name": $( '#callback_name-register-as-trainer' ).val(),
								"phone": $( '#callback_phn-register-as-trainer' ).val(),
								"email": $( '#callback_email-register-as-trainer' ).val(),
							},
							
							ajaxData = {
								'pedagoge_callback_function': 'register_as_trainer_modal_callback',
							};
						
						mainObj.loadAjax( data, "contactData", ajaxData ).done( function ( data ) {
							loadAfterAjax( data, $show_result, $callback_El );
						} );
						return data;
					},
					
				},
				loadAfterAjax = function ( data, $show_result, $callback_El, callback_fn ) {
					var $response_data,
						callback = pdg_sanitize.inputdefaultparam( callback_fn, function () {
						} );
					try {
						$response_data = $.parseJSON( data );
						var $error = $response_data.error,
							$message = $response_data.message;
						
						if ( $error ) {
							pdg_show_alerts( $show_result, $message, 'error' );
						} else {
							pdg_show_alerts( $show_result, $message, 'success' );
							$callback_El.find( 'form' ).trigger( 'reset' );
							if (typeof($response_data.url) !== 'undefined') {
								//window.location = $response_data.url
								console.log($response_data.url);
								setTimeout(function(){ window.location = $response_data.url; }, 1000);
							}
						}
					} catch ( err ) {
						$message = 'Unexpected error occured while processing your request! Please try again.' + err;
					}
					$callback_El.animate( {
						scrollTop: $show_result.offset().top - 400
					} );
					if ( ! pdg_sanitize.isFunction( callback ) ) {
						return;
					}
					callback( $response_data, $show_result, $callback_El );
				};
			
			
			mainObj.loadAjax = function ( param, identifier, ajaxData ) {
				var dynamicData = {};
				dynamicData['pedagoge_callback_class'] = 'PDGBusinessContactForm';
				if ( typeof ajaxData['pedagoge_callback_class'] != "undefined" ) {
					dynamicData['pedagoge_callback_class'] = ajaxData['pedagoge_callback_class'];
				}
				dynamicData[identifier] = param;
				dynamicData['nonce'] = $ajaxnonce;
				dynamicData['action'] = 'pedagoge_visitor_ajax_handler';
				dynamicData['pedagoge_callback_function'] = ajaxData['pedagoge_callback_function'];
				return $.ajax( {
					url: $ajax_url,
					type: "post",
					data: dynamicData
				} );
			};
			
			mainObj.getFilterCheckedItems = function ( selector ) {
				return pdg_sanitize.noundefined( selector.filter( ':checked' ).map( function () {
					return this.value;
				} ).get(), true );
			};
			
		}, modalsObj;
		modalsObj = new PDG_Modals_Callback();
		modalsObj.init();
		
	} );
} );