(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
			var PDG_Profile = function () {
				var mainObj = this, settingsObj;
				//Settings
				mainObj.settings = function () {
					var $bodyEl = $( 'body' ),
						$isMobile = $( '.isMobile' ),
						$scroll_to_here_on_start = $( '.scroll_to_here_on_start' ),
						$section_info = $( '.section-info' ),
						$courses_panel_sub_wrapperEl = $( '.courses-panel-sub-wrapper' ),
						panel_panel_profile_var = '.panel.panel-profile',
						$panel_profileEl = $courses_panel_sub_wrapperEl.find( panel_panel_profile_var ),
						border_full_fix = 'border-full-fix',
						border_bottom_fix = 'border-bottom-fix',
						bg_color_fix = 'bg-color-fix',
						panel_heading = '.panel-heading',
						panel_collapse = '.panel-collapse',
						panel_body = '.panel-body',
						$req_a_callback_btn = $( '.req_a_callback_btn' ),
						subjects_hash = urlParam.parsehash( "subjects" ),
						$take_this_course_btn = $( '.take_this_course_btn' ),
						$item_profile_name = $( ".item-profile-name" ),
						form_group_is_empty_var = '.form-group.label-floating.is-empty',
						$recommend_btn = $( ".recommend_btn" ),
						$try_a_demo_btn = $( ".try_a_demo_btn" ),
						$chk_recommend_teacher = $( ".chk_recommend_teacher" ),
						$total_recommendation_count = $( ".total_recommendation_count" ),
						current_logged_in_user = parseInt( $recommend_btn.data( 'current_user' ) ),
						teacher_user_id = parseInt( $recommend_btn.data( 'teacher_user_id' ) ),
						locality_id_data = $( "#locality_id_data" ).data( 'value' ),
						subjects_names_data = $( "#subjects_names_data" ).data( 'value' ),
						profile_suggestion_item_cards_template = $( "#profile_suggestion_item_cards_template" ),
						$section_similar_profile_El = $( ".section-similar-profile" ),
						$section_similar_profile_slideOuter = $section_similar_profile_El.find( ".slideOuter" ),
						$section_similar_profile_slideInner = $section_similar_profile_slideOuter.find( ".slideInner" ),
						$kick_start_sliding = $section_similar_profile_slideOuter.find( '.kick-start-sliding' ),
						spinnerObj = new spinnerHandle( {
							'parentclass': '.similar-profile-spinner',
							'size': 'small',
							'positionclass': 'text-center'
						} ),
						cmd_reload_teachers_notes = $("#cmd_reload_teachers_notes"),
						modal_teachers_notes = $("#modal_teachers_notes"),
						txt_teacher_notes = $("#txt_teacher_notes"),
						txt_teacher_note_maker = $("#txt_teacher_note_maker"),						
						div_teacher_notes_save_result_area = $("#div_teacher_notes_save_result_area"),						
						cmd_save_teachers_notes = $("#cmd_save_teachers_notes"),						
						div_teacher_notes = $("#div_teacher_notes"),
						hidden_teacher_user_id = $("#hidden_teacher_user_id").val();						
					
					return {
						$bodyEl: $bodyEl,
						$isMobile: $isMobile,
						$scroll_to_here_on_start: $scroll_to_here_on_start,
						$section_info: $section_info,
						$courses_panel_sub_wrapperEl: $courses_panel_sub_wrapperEl,
						panel_panel_profile_var: panel_panel_profile_var,
						$panel_profileEl: $panel_profileEl,
						border_full_fix: border_full_fix,
						border_bottom_fix: border_bottom_fix,
						bg_color_fix: bg_color_fix,
						panel_heading: panel_heading,
						panel_collapse: panel_collapse,
						panel_body: panel_body,
						$req_a_callback_btn: $req_a_callback_btn,
						subjects_hash: subjects_hash,
						$take_this_course_btn: $take_this_course_btn,
						form_group_is_empty_var: form_group_is_empty_var,
						$recommend_btn: $recommend_btn,
						$try_a_demo_btn: $try_a_demo_btn,
						$item_profile_name: $item_profile_name,
						$chk_recommend_teacher: $chk_recommend_teacher,
						$total_recommendation_count: $total_recommendation_count,
						current_logged_in_user: current_logged_in_user,
						teacher_user_id: teacher_user_id,
						locality_id_data: locality_id_data,
						subjects_names_data: subjects_names_data,
						profile_suggestion_item_cards_template: profile_suggestion_item_cards_template.html(),
						$section_similar_profile_El: $section_similar_profile_El,
						$section_similar_profile_slideOuter: $section_similar_profile_slideOuter,
						$section_similar_profile_slideInner: $section_similar_profile_slideInner,
						$kick_start_sliding: $kick_start_sliding,
						spinnerObj: spinnerObj,
						
						cmd_reload_teachers_notes : cmd_reload_teachers_notes,						
						modal_teachers_notes : modal_teachers_notes,
						txt_teacher_notes : txt_teacher_notes,
						txt_teacher_note_maker : txt_teacher_note_maker,						
						div_teacher_notes_save_result_area : div_teacher_notes_save_result_area,						
						cmd_save_teachers_notes : cmd_save_teachers_notes,						
						div_teacher_notes : div_teacher_notes,
						hidden_teacher_user_id : hidden_teacher_user_id,
					};
				};
				
				//Load initial page search results
				mainObj.init = function () {
					settingsObj = mainObj.settings();
					mainObj.bindUIActions();
				};
				
				mainObj.bindUIActions = function () {
					autoScrolltoBody();
					resizeHandler();
					panelTabsHandler();
					smartUrlHandler();
					buttonHandler();
					profileStarRatingHandler();
					reviewTextHandler();
					recommendBtnHandler();
					recommendCheckboxModalHandler();
					suggestedProfile();
					addThisCopyLinkFix();
					resetNotesModal();
					reloadTeacherNotes();
					saveTeacherNotes();	
				};
				
				//offset controls for the UI
				var resizeHandler = function () {
						$( window ).resize( function () {
							
						} ).resize();
					},
					autoScrolltoBody = function () {
						if ( settingsObj.$isMobile.is( ':visible' ) ) {
							//smartphones
							var scrollToTopPos = settingsObj.$scroll_to_here_on_start.offset().top - 60;
							$( window ).scrollTop( scrollToTopPos );
							$( window ).on( 'beforeunload', function () {
								$( window ).scrollTop( scrollToTopPos );
							} );
						}
					},
					panelTabsHandler = function () {
						settingsObj.$panel_profileEl.on( 'shown.bs.collapse', function () {
							fn_OpenState( $( this ) );
						} ).on( 'hidden.bs.collapse', function () {
							fn_CloseState( $( this ) );
						} );
						
						$( settingsObj.$panel_profileEl.find( settingsObj.panel_collapse ) ).each( function () {
							if ( $( this ).hasClass( 'collapse in' ) ) {
								fn_OpenState( $( this ).parent() );
							} else {
								fn_CloseState( $( this ).parent() );
							}
						} );
						
						function fn_OpenState ( $this ) {
							$this.addClass( settingsObj.border_full_fix ).find( settingsObj.panel_heading ).removeClass( settingsObj.border_bottom_fix ).addClass( settingsObj.bg_color_fix );
							$this.find( settingsObj.panel_body ).addClass( settingsObj.bg_color_fix );
							settingsObj.$bodyEl.animate( {
								scrollTop: $this.offset().top - 150
							}, 500 );
							
						}
						
						function fn_CloseState ( $this ) {
							$this.removeClass( settingsObj.border_full_fix ).find( settingsObj.panel_heading ).addClass( settingsObj.border_bottom_fix ).removeClass( settingsObj.bg_color_fix );
							$this.find( settingsObj.panel_body ).removeClass( settingsObj.bg_color_fix );
						}
					},
					smartUrlHandler = function () {
						var flag_collapsed = false;
						if ( pdg_sanitize.noundefined( settingsObj.subjects_hash ) == "" ) {
							return false;
						}
						settingsObj.subjects_hash.split( "," ).filter( function ( param ) {
							if ( flag_collapsed ) {
								return false;
							}
							var subject_value = rtrim( param.toLowerCase().replace( /[^a-z0-9]+/ig, "-" ).replace( /--+/g, '-' ), "-" ).trim(),
								$el = $( settingsObj.panel_collapse + '.' + subject_value );
							if ( $el.addClass( 'in' ).length > 0 ) {
								$el.parent( settingsObj.panel_panel_profile_var ).find( '.accordion-toggle' ).removeClass( 'collapsed' );
								flag_collapsed = true;
							}
						} );
						
						//remove #subjects after page load and correct tab is loaded
						urlParam.removehash();
					},
					buttonHandler = function () {
						settingsObj.$req_a_callback_btn.on( 'click', function () {
							$( '#callback_sub' ).val( decodeURI( settingsObj.subjects_names_data ) ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_loc' ).val( decodeURI( get_locality_name_by_id( settingsObj.locality_id_data, true ) ) ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_teachername' ).val( settingsObj.$item_profile_name.text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
						} );
						
						settingsObj.$try_a_demo_btn.on( 'click', function () {
							$( '#callback_sub-takeademo' ).val( decodeURI( settingsObj.subjects_names_data ) ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_loc-takeademo' ).val( decodeURI( get_locality_name_by_id( settingsObj.locality_id_data, true ) ) ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_teachername-takeademo' ).val( settingsObj.$item_profile_name.text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
						} );
						
						settingsObj.$take_this_course_btn.on( 'click', function () {
							var $el = $( this ).parents( '.course_item_block_wrapper' ),
								callback_pageurl = $( this ).parents( '.panel-collapse.panel-body' ).attr( 'class' ),
								find_array = ["panel-collapse", "panel-body", "collapse", " in", "bg-color-fix"],
								replace_array = ["", "", "", "", "", ""];
							callback_pageurl = callback_pageurl.replace_array( find_array, replace_array );
							callback_pageurl = rtrim( callback_pageurl.toLowerCase().replace( /[^a-z0-9-]+/ig, "," ).replace( /,,+/g, ',' ), "," ).trim();
							$( '#callback_teachername-takethiscourse' ).val( settingsObj.$item_profile_name.text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_pageurl-takethiscourse' ).val( urlParam.geturlnohash() + "#subjects=" + callback_pageurl ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_sub-takethiscourse' ).val( $el.find( '.item.subjects' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_fees-takethiscourse' ).val( $el.find( '.item.fees' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_loc-takethiscourse' ).val( $el.find( '.item.locality' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_tut_loc-takethiscourse' ).val( $el.find( '.item.typeoflocation' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_batchsize-takethiscourse' ).val( $el.find( '.item.class_capacity' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_length-takethiscourse' ).val( $el.find( '.item.course_length' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
							$( '#callback_time-takethiscourse' ).val( $el.find( '.item.class_duration' ).text() ).parent( settingsObj.form_group_is_empty_var ).removeClass( "is-empty" );
						} );
						
					},
					profileStarRatingHandler = function () {
						var ratingObj = {
								ratedFill: "#19539f",
								halfStar: true,
							},
							$overall_rating = $( "#overall-rating" ).rateYo( ratingObj ),
							$star1_rating = $( "#star1-rating" ).rateYo( ratingObj ),
							$star2_rating = $( "#star2-rating" ).rateYo( ratingObj ),
							$star3_rating = $( "#star3-rating" ).rateYo( ratingObj ),
							$star4_rating = $( "#star4-rating" ).rateYo( ratingObj );
					},
					reviewTextHandler = function () {
						$( "#txt_review_field" ).keyup( function () {
							characterCounter.min( $( this ), ".txt_review_field_counter", 50 );
						} );
					},
					recommendBtnHandler = function () {
						var $recommend_btn = settingsObj.$recommend_btn,
							current_logged_in_user = settingsObj.current_logged_in_user,
							teacher_user_id = settingsObj.teacher_user_id;
						$recommend_btn.on( 'click', function ( e ) {
							$recommend_btn.button( 'loading' );
							var has_recommended = $recommend_btn.data( 'recommended' ),
								recommendation_counter = parseInt( $recommend_btn.data( 'recommendation_count' ) );
							if ( current_logged_in_user > 0 && teacher_user_id > 0 ) {
								var submit_data = {
									recommended_by: current_logged_in_user,
									recommended_to: teacher_user_id,
									has_recommended: has_recommended
								};
								var ajaxData = {
									'pedagoge_callback_class': 'ControllerSearch',
									'pedagoge_callback_function': 'search_recommend_teacher_ajax_ver2',
								};
								pdg_ajax_handler( submit_data, "recommendData", ajaxData ).done( function ( data ) {
									try {
										var $response_data = $.parseJSON( data ),
											$error = $response_data.error,
											$error_type = $response_data.error_type,
											$message = $response_data.message;
										if ( ! $error ) {
											if ( has_recommended == 'yes' ) {
												/**
												 *
												 * 2. less counter
												 * 3. change has recommended to no
												 */
												recommendation_counter --;
												$recommend_btn.data( 'recommendation_count', recommendation_counter );
												settingsObj.$total_recommendation_count.html( recommendation_counter );
												$recommend_btn.data( 'recommended', 'no' );
												$recommend_btn.html( '<i class="material-icons">&#xE8DC;</i>&nbsp;Recommend' );
											} else {
												/**
												 *
												 * 2. increase counter
												 * 3. change has recommended to yes
												 */
												recommendation_counter ++;
												$recommend_btn.data( 'recommendation_count', recommendation_counter );
												settingsObj.$total_recommendation_count.html( recommendation_counter );
												$recommend_btn.data( 'recommended', 'yes' );
												$recommend_btn.html( '<i class="material-icons">&#xE8DB;</i>&nbsp;Unrecommend' );
											}
											$recommend_btn.prop( 'disabled', false );
											$recommend_btn.removeClass( 'disabled' );
											recommendCheckboxModalHandler();
										}
									} catch ( err ) {
									} finally {
									}
								} );
							}
						} );
					},
					recommendCheckboxModalHandler = function () {
						var has_recommended = settingsObj.$recommend_btn.data( 'recommended' );
						if ( has_recommended == "yes" ) {
							settingsObj.$chk_recommend_teacher.attr( "disabled", true ).prop( 'checked', false );
						}
						else {
							settingsObj.$chk_recommend_teacher.attr( "disabled", false );
						}
					},
					suggestedProfile = function () {
						settingsObj.spinnerObj.show();
						var searchDataArray = {
								"subject_name": settingsObj.subjects_names_data,
								"locality_names": settingsObj.locality_id_data,
								"pagination_no": 0,
								"no_of_records_per_page": 13,
							},
							ajaxData = {
								'pedagoge_callback_function': 'fn_search_ajax',
								'pedagoge_callback_class': 'ControllerSearch',
							};
						pdg_ajax_handler( searchDataArray, "searchData", ajaxData ).done( function ( data ) {
							var parsedData = JSON.parse( data );
							render_ajax_data( parsedData );
						} );
					},
					render_ajax_data = function ( data ) {
						if ( data.status !== "success" ) {
							settingsObj.$section_similar_profile_El.hide();
							return false;
						}
						Mustache.parse( settingsObj.profile_suggestion_item_cards_template );
						settingsObj.$section_similar_profile_slideInner.html( Mustache.render( settingsObj.profile_suggestion_item_cards_template, data ) );
						var $section_similar_profile_slideX = settingsObj.$section_similar_profile_slideInner.find( ".slideX" ),
							total_valid_slideX = 0,
							min_slideX_width = 146.3333,
							slideInner_calculated_width = 0;
						$section_similar_profile_slideX.each( function () {
							if ( $( this ).data( 'profile_data_id' ) == settingsObj.teacher_user_id ) {
								$( this ).remove();
							}
							else {
								++ total_valid_slideX;
							}
						} );
						
						if ( total_valid_slideX <= 0 ) {
							settingsObj.$section_similar_profile_El.hide();
							return false;
						}
						
						slideInner_calculated_width = Math.round( min_slideX_width * total_valid_slideX );
						settingsObj.$section_similar_profile_slideInner.css( {
							width: slideInner_calculated_width + 'px',
						} );
						settingsObj.spinnerObj.hide();
						settingsObj.$section_similar_profile_slideOuter.removeClass( 'hide' );
						settingsObj.$kick_start_sliding.trigger( 'click' );
					},
					addThisCopyLinkFix = function () {
						/*var input_at_opy_link_share_page_url_var = "input.at-copy-link-share-page-url";
						
						function eventHandler ( evt ) {
							if ( evt.type === 'addthis.menu.share' && evt.data.service === "link" ) {
								waitForElementLoad( input_at_opy_link_share_page_url_var, function () {
									$( document ).find( input_at_opy_link_share_page_url_var ).keypress( function ( e ) {
										$( this ).select();
									} )
								}, 250 );
							}
						}
						
						addthis.addEventListener( 'addthis.menu.share', eventHandler );*/
					},
					resetNotesModal = function() {
						fn_reset_notes_taking_form();
						settingsObj.modal_teachers_notes.on('hidden.bs.modal', function(e){							
							fn_reset_notes_taking_form();							
						});
					},
					fn_reset_notes_taking_form = function() {						
						settingsObj.txt_teacher_notes.val('');
						settingsObj.div_teacher_notes_save_result_area.html('');
					},				
					reloadTeacherNotes = function() {
						settingsObj.cmd_reload_teachers_notes.on('click', function(){
							settingsObj.cmd_reload_teachers_notes.button( 'loading' );
							var submit_data = {
								teacher_user_id: settingsObj.hidden_teacher_user_id,								
							};
							var ajaxData = {
								'pedagoge_callback_class': 'ControllerTeacher',
								'pedagoge_callback_function': 'fn_return_teacher_notes_ajax',
							};
							pdg_ajax_handler( submit_data, "teacherInfo", ajaxData ).done( function ( data ) {
								try {
									var $response_data = $.parseJSON( data ),
										$error = $response_data.error,
										$error_type = $response_data.error_type,
										$message = $response_data.message,
										$data = $response_data.data;
									if ( ! $error ) {
										settingsObj.div_teacher_notes.html($data);
										$.notify($message, 'success');
									} else {
										$.notify($message, 'error');
									}
								} catch ( err ) {
								} finally {
									settingsObj.cmd_reload_teachers_notes.button( 'reset' );
								}
							} );
						});
					},
					saveTeacherNotes = function() {
						settingsObj.cmd_save_teachers_notes.on('click', function(){
							
							var $notes_text = settingsObj.txt_teacher_notes.val();//$("#txt_teacher_notes").val();
							var $notes_taker = settingsObj.txt_teacher_note_maker.val();
							
							var $result_area = settingsObj.div_teacher_notes_save_result_area;
							var $notes_area = settingsObj.div_teacher_notes;
							
							if($notes_text.length <= 5) {
								$.notify(settingsObj.txt_teacher_notes,'Please write your notes!', 'error');
								return;
							}
							
							if($notes_taker.length <= 5) {								
								$.notify(settingsObj.txt_teacher_note_maker,'Please write your Name!!', 'error');
								return;
							}
							
							settingsObj.cmd_save_teachers_notes.button( 'loading' );
							var submit_data = {
								teacher_user_id: settingsObj.hidden_teacher_user_id,
								teacher_note : $notes_text,
								note_taker : $notes_taker
							};
							var ajaxData = {
								'pedagoge_callback_class': 'ControllerTeacher',
								'pedagoge_callback_function': 'fn_save_teacher_notes_ajax',
							};
							pdg_ajax_handler( submit_data, "notesData", ajaxData ).done( function ( data ) {
								try {
									var $response_data = $.parseJSON( data ),
										$error = $response_data.error,
										$error_type = $response_data.error_type,
										$message = $response_data.message,
										$data = $response_data.data;
									if ( ! $error ) {
										//settingsObj.div_teacher_notes.html($data);
										$.notify($message, 'success');
										fn_reset_notes_taking_form();
										//settingsObj.cmd_reload_teachers_notes.click();
										notes_reload();
									} else {
										$.notify($message, 'error');
									}
								} catch ( err ) {
								} finally {
									settingsObj.cmd_save_teachers_notes.button( 'reset' );
									//test
								}
							} );
						});
					};
			}, profileObj;
			profileObj = new PDG_Profile;
			profileObj.init();
		}
	);
} );

function notes_reload() {
var submit_data = {
teacher_user_id: $("#hidden_teacher_user_id").val(),								
};
var ajaxData = {
'pedagoge_callback_class': 'ControllerTeacher',
'pedagoge_callback_function': 'fn_return_teacher_notes_ajax',
};
pdg_ajax_handler( submit_data, "teacherInfo", ajaxData ).done( function ( data ) {
try {
	var $response_data = $.parseJSON( data ),
		$error = $response_data.error,
		$error_type = $response_data.error_type,
		$message = $response_data.message,
		$data = $response_data.data;
	if ( ! $error ) {
		$("#div_teacher_notes").html($data);
		//$.notify($message, 'success');
	} else {
		$.notify($message, 'error');
	}
} catch ( err ) {
} finally {
	//settingsObj.cmd_reload_teachers_notes.button( 'reset' );
}
} );	
}

