(function ( pedagogeJquery ) {
	pedagogeJquery( window.jQuery, window, document );
	
})( function ( $, window, document ) {
	$( function () {
		var PDG_Desktop_Home = function () {
			var mainObj = this, settingsObj, did_scroll = false, $window = $( window ), search_distance;
			//Settings
			mainObj.settings = function () {
				var dataTogglePopover = '[data-toggle=popover]',
					$popoverEl = $( ".popover" ),
					$search = $( ".search" ),
					$bodyEl = $( 'body' ),
					$isMobileEl = $( '.isMobile' ),
					$isTabletEl = $( '.isTablet' ),
					$navbarBrandImgEl = $( '.navbar-brand img' ),
					$bgImageMainEl = $( '.bg-image-main' ),
					$start_a_class_btnEl = $bgImageMainEl.find( 'a.start_a_class_btn' );
				
				return {
					
					$bodyEl: $bodyEl,
					$isMobileEl: $isMobileEl,
					$isTabletEl: $isTabletEl,
					$popoverEl: $popoverEl,
					$search: $search,
					$bgImageMainEl: $bgImageMainEl,
					$navbarBrandImgEl: $navbarBrandImgEl,
					$start_a_class_btnEl: $start_a_class_btnEl,
					dataTogglePopover: dataTogglePopover,
				};
			};
			
			//Load initial page search results
			mainObj.init = function () {
				settingsObj = mainObj.settings();
				mainObj.bindUIActions();
			};
			
			mainObj.bindUIActions = function () {
				resizeHandler();
				fixedSearchBarScrollInit();
				popoverFix();
			};
			
			var searchBoxSiblingsHandlerOnFixed = function () {
					//hide the logo on tablet devices
					if ( ! settingsObj.$isTabletEl.is( ':visible' ) ) {
						// medium devices or smartphone
						settingsObj.$start_a_class_btnEl.removeClass( 'hide' );
					}
					else {
						//tablets
						if ( settingsObj.$search.hasClass( 'fixed-search' ) ) {
							settingsObj.$start_a_class_btnEl.addClass( 'hide' );
						}
						else {
							settingsObj.$start_a_class_btnEl.removeClass( 'hide' );
						}
					}
				},
				fixedSearchBarScrollInit = function () {
					search_distance = settingsObj.$search.offset().top;
					$window.scroll( function ( event ) {
						did_scroll = true;
					} );
					if ( ! settingsObj.$isMobileEl.is( ':visible' ) ) {
						setInterval( function () {
							if ( did_scroll ) {
								did_scroll = false;
								if ( $window.scrollTop() + 50 >= search_distance ) {
									settingsObj.$search.addClass( "fixed-search" );
								}
								else {
									settingsObj.$search.removeClass( "fixed-search" );
								}
								searchBoxSiblingsHandlerOnFixed();
							}
						}, 100 );
					}
				},
				resizeHandler = function () {
					$( window ).resize( function () {
						searchBoxSiblingsHandlerOnFixed();
					} ).resize();
				},
				popoverFix = function () {
					settingsObj.$bodyEl.popover( {
						selector: settingsObj.dataTogglePopover,
						trigger: "hover click",
						placement: 'top',
						html: false,
						delay: {
							show: 500,
							hide: 100
						},
					} ).on( "show.bs.popover", function ( e ) {
						// hide all other popovers
						setTimeout( function () {
							$( '.popover' ).fadeOut( 'slow' );
						}, 10000 );
						$( settingsObj.dataTogglePopover ).not( e.target ).popover( "destroy" );
						settingsObj.$popoverEl.remove();
					} );
				};
		}, homeDesktopObj;
		homeDesktopObj = new PDG_Desktop_Home();
		homeDesktopObj.init();
		
	} );
} );