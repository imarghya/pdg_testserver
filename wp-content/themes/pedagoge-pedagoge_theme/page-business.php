<?php
/*
Template Name: Business Page
*/

get_header( 'home' );
$theme_dir = get_template_directory_uri();
?>
	<section class="wrapper container-fluid section1-business">
		<div class="row header-business bgImg clearfix">
			<div class="col-xs-12 col-md-8 main-section1-business">
				Pedagoge for business is a dedicated B2B platform where organisations and businesses partner with
				Pedagoge with the objective of analysing their employee training needs and conduct trainings to develop
				a more effective and efficient workforce.
			</div>
			<div class="col-xs-12 col-md-3 form1-business text-center">
				<form class="form1">
					<h4>Get in touch to hire a corporate trainer</h4>
					<div id="result_area" class="resultMsgs"></div>
					<div class="form-group">
						<input type="text" class="form-control" id="companyName" placeholder="Company Name*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="yourName" placeholder="Your Name*">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="designation" placeholder="Designation">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="phone" placeholder="Phone Number*">
					</div>
					<div class="form-group">
						<input type="email" class="form-control" id="email" placeholder="Email ID*">
					</div>
					<div id="result_area_success" class="resultMsgs"></div>
					<button type="submit" class="btn btn-info input-block-level form-control submit-form1-business">
						Submit
					</button>
					<div class="image_loader">
						<img class="center-block" style="display:none;"
						     src="<?= PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif"
						     id="img_contact_loader"/>
					</div>
				</form>
			</div>
			<div class="col-xs-12 col-md-3 text-center interested-business" data-toggle="modal"
			     data-target="#interestedModal">
				<h4>If you are a corporate trainer,<br>click here</h4>
			</div>

			<div class="col-xs-12 col-md-8 getstarted-section1-business">
				<h2>Get started today! Call: <span class="text-blue">+91-8621993262</span> or email: <span
						class="text-blue">business@pedagoge.com</span>
				</h2>
			</div>
		</div>
		<div id="interestedModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- interestedModal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title-business">Interested in associating with us as a corporate trainer?</h4>
					</div>
					<div class="modal-body">
						<div id="result_area2" class="resultMsgs"></div>
						<form class="form2 form2-business">
							<h5 class="modal-title-business">Fill your details</h5>
							<div class="form-group">
								<input type="text" class="form-control" id="yourName2" placeholder="Your Name*">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="phone2" placeholder="Phone Number*">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" id="email2" placeholder="Email ID*">
							</div>
							<button type="submit"
							        class="btn btn-info input-block-level form-control submit-form2-business">Submit
							</button>
							<div class="image_loader">
								<img class="center-block" style="display:none;"
								     src="<?= PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif"
								     id="img_contact_loader2"/>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div id="signupModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- interestedModal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title-business">Signup?</h4>
					</div>
					<div class="modal-body">
						<div id="div_signup_result"></div>
						<form class="m-t-md" name="registration_form" id="registration_form">
							<div id="scrollToHere-forteachers"></div>
							<div class="form-group">
								<input type="email" name="txt_register_email_address" id="txt_register_email_address"
								       class="form-control validate[required, custom[email]]" placeholder="Email"
								       required>
							</div>
							<div class="form-group">
								<input type="text" name="txt_register_user_name" id="txt_register_user_name"
								       class="form-control validate[required]" placeholder="Username" required>
							</div>

							<div class="form-group">
								<input type="password" name="txt_register_password" id="txt_register_password"
								       class="form-control validate[required]" placeholder="Password" required>
							</div>
							<p><input type="checkbox" class="chk_register_show_password" value="no"> Show
								Password</p>
							<p class="blocktext-forteachers">By clicking Register, you are agreeing to the <a
									href="<?php echo home_url( '/terms' ); ?>">terms
									and conditions</a> and the <a href="<?php echo home_url( '/privacy-policy' ); ?>">privacy
									policy</a> set by Pedagoge.
							</p>
							<input type="button" id="cmd_register_user" class="btn btn-success btn-block m-t-xs"
							       value="Register"/>
							<a href="<?php echo home_url( '/login' ); ?>/" title="Pedagoge Login"
							   id="cmd_show_login_form"
							   class="btn btn-info btn-block m-t-xs">Already have an account? Login</a>
						</form>

					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="wrapper container-fluid section2-business text-center center-block">
		<div class="row"><h2>BENEFITS OF USING PEDAGOGE FOR BUSINESS</h2></div>
		<div class="row">
			<div class="col-xs-1 visible-xs checkmob-business" style="height: 0;"></div>
			<div class="col-xs-1 hidden-md hidden-lg checksm-business" style="height: 0;"></div>
			<div class="col-xs-12 col-md-4 divider-section2-business">
				<img src="<?= $theme_dir ?>/assets/img/business/1.png" align="center" class="img1-business"
				     title="Training Need Analysis" data-customplacement="right"
				     data-content="Pedagoge conducts free training need analysis surveys for organisations. These survey tools are highly customised, easy to use and do not involve any cost for the organisations.">
				<h4>Training Need Analysis</h4>
			</div>
			<div class="col-xs-12 col-md-4 divider-section2-business">
				<img src="<?= $theme_dir ?>/assets/img/business/2.png" align="center" class="img2-business"
				     title="Customised Training Analysis" data-customplacement="right"
				     data-content="Pedagoge specialises into framing and conducting training analysis tools for organisations that are - industry, department and hierarchy specific.">
				<h4>Customised Training Analysis</h4>
			</div>
			<div class="col-xs-12 col-md-4 divider-section2-business">
				<img src="<?= $theme_dir ?>/assets/img/business/3.png" align="center" class="img3-business"
				     title="Experienced Trainers" data-customplacement="left"
				     data-content="Pedagoge has alliances with multi-domain professional corporate trainers to fulfil your training needs of the hour.">
				<h4>Experienced Trainers</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-4 divider-section2-business">
				<img src="<?= $theme_dir ?>/assets/img/business/4.png" align="center" class="img4-business"
				     title="Higher Need-Resource ratio" data-customplacement="right"
				     data-content="Pedagoge has partnered with multiple trainers for each domain and module requirement. We have more than enough trainers for your need!">
				<h4>Higher Need-Resource ratio</h4>
			</div>
			<div class="col-xs-12 col-md-4 divider-section2-business">
				<img src="<?= $theme_dir ?>/assets/img/business/5.png" align="center" class="img5-business"
				     title="Seamless Integration" data-customplacement="right"
				     data-content="Pedagoge works closely with organisations for planning and executing the trainings. Our trainers are flexible to suit your training demands. We’re in it for you!">
				<h4>Seamless Integration</h4>
			</div>
			<div class="col-xs-12 col-md-4 divider-section2-business">
				<img src="<?= $theme_dir ?>/assets/img/business/6.png" align="center" class="img6-business"
				     title="Hassle free" data-customplacement="left"
				     data-content="Pedagoge operates at a fast speed with minimal documentation mode and aims at coordinating quickly and efficiently with the organisations. There is no red tape!">
				<h4>Hassle free</h4>
			</div>
		</div>
	</section>
<?php
get_footer( 'home' );