<?php
/*
Template Name: Careers - Marketing Associate
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Marketing
							Associate
						</div>
						<div class="opPara1 jobDescPara">
							<p>Pedagoge is looking for a marketing associate with a knack to learn and grow with a</p>

							<p>startup. An enthusiastic professional having excellent communication and strong</p>

							<p>networking skills is preferred.</p>

							<p><strong>Some of the primary roles &amp; responsibilities include:</strong></p>

							<p><span>&bull; </span>Should be able to design and recommend marketing programs along with
							</p>

							<p>setting short and long-term goals</p>

							<p>&bull; Plan and execute complete stand-alone marketing campaigns with ownership</p>

							<p>and accountability</p>

							<p>&bull; Prepare and adhere to budgets and prepare ROI strategies</p>

							<p>&bull; Execute offline campaigns on-field in schools, colleges, complexes &amp;
								events</p>

							<p>&bull; Deal with vendors and probable affiliates for increasing brand presence</p>

							<p>&bull; Generate offline and online sales qualified leads for the company</p>

							<p>&bull; Ideate and execute low budget BTL or guerrilla campaigns</p>

							<p>&bull; Collaborate with sales and sourcing to develop strategic partnership for</p>

							<p>affiliate marketing</p>

							<p><strong>Skillset required:</strong></p>

							<p>&bull; Excellent communication skills. Ability to negotiate and convince people</p>

							<p>&bull; Working knowledge of English, Hindi &amp; Bengali</p>

							<p>&bull; Strong networking skills</p>

							<p>&bull; Inclination to learn new concept and adapt to newer ideas</p>

							<p>&bull; Willingness to work overtime hand hustle. Pro-activeness is a must</p>

							<p>What should the candidate expect from us:</p>

							<p>&bull; Working with a Nasscom 10,000 startup in the EduTech industry</p>

							<p>&bull; Setting future marketing communication guidelines for the company</p>

							<p>&bull; Direct reporting to the Marketing Manager</p>

							<p>&bull; Deep insights into the private coaching and B2B education industry in India</p>

							<p>&bull; Knowledge about online and offline marketing campaigns</p>

							<p>&bull; Ownership of work and freedom to experiment new marketing concepts</p>

							<p><strong>Experience:</strong> 0 to 2 years</p>

							<p><strong>Location:</strong> Kolkata</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor, Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata -
								700091,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home_ver2' );