<?php
/*
Template Name: findtherightteacher
*/


get_header('home');
$theme_dir = get_template_directory_uri();
$search_url = home_url('/search/');
?>
	<div class="wrapper container-fluid">
		<div class="row headersection-marketingcamp bgImg">
			<div class="logo-marketingcamp">
				<div class="col-xs-6 text-left">
					<a href="<?php echo home_url('/'); ?>">
						<img class="logo-adjust" src="<?= $theme_dir; ?>/assets/img/logo.png" alt="Pedagoge Logo"
						     width="">
					</a>
				</div>
			</div>
			<div class="heading-marketingcamp">
				<div class="col-xs-12 text-center heading-marketingcamp heading-adjust">
					TEACHER SEARCH MADE EASY!
				</div>
			</div>
		</div>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="text-center section2-marketingcamp section2-marketingcamp-xs">
					<h3>Some of our popular searches are:</h3>
					<div class="col-xs-12 divider-section2-marketingcamp">
					</div>
					<div class="col-xs-6 col-sm-2 btn-aling-right-xs">
						<a class="btn btn1"
						   href="<?= $search_url ?>/?pdg_subject=Common%20Admission%20Test%20(CAT)&pdg_locality="
						   role="button">CAT</a>
					</div>
					<div class="col-xs-6 col-sm-2 btn-aling-left-xs">
						<a class="btn btn2"
						   href="<?= $search_url ?>?pdg_subject=JEE(Mains%20and%20Advanced)&pdg_locality="
						   role="button">JEE</a>
					</div>
					<div class="col-xs-12 visible-xs divider-section2-marketingcamp">
					</div>
					<div class="col-xs-6 col-sm-2 btn-aling-right-xs">
						<a class="btn btn3"
						   href="<?= $search_url ?>?pdg_subject=Chartered%20Accountancy%20Foundation%20(CPT)&pdg_locality="
						   role="button">CA</a>
					</div>
					<div class="col-xs-6 col-sm-2 btn-aling-left-xs">
						<a class="btn btn4"
						   href="<?= $search_url ?>?pdg_subject=Graduate%20Management%20Aptitude%20Test%20(GMAT)&pdg_locality="
						   role="button">GMAT</a>
					</div>
					<div class="col-xs-12 visible-xs divider-section2-marketingcamp">
					</div>
					<div class="col-xs-6 col-sm-2 btn-aling-right-xs">
						<a class="btn btn5" href="<?= $search_url ?>?pdg_subject=Pre%20Medical%20Test%20(PMT)&pdg_locality="
						   role="button">PMT</a>
					</div>
					<div class="col-xs-6 col-sm-2 btn-aling-left-xs">
						<a class="btn btn6"
						   href="<?= $search_url ?>?pdg_subject=Common%20Law%20Aptitude%20Test%20(CLAT)&pdg_locality="
						   role="button">CLAT</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 divider1-section2-marketingcamp">
				</div>
				<div class="col-xs-1 visible-xs">
				</div>
				<div class="col-xs-10 col-sm-12 section2-marketingcamp-border">
					<p>COMPETITIVE EXAMS ARE CRUCIAL.<br>START FINDING THE RIGHT TEACHER NOW!
					</p>


				</div>
				<div class="col-xs-1 visible-xs">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 divider1-section2-marketingcamp">
				</div>
				<div class="col-xs-1 visible-xs">
				</div>
				<div class="col-xs-10 col-sm-12 section3-marketingcamp">
					<div class="section2-content-marketingcamp">
						<span class="label-marketingcamp">LOOKING FOR SOMETHING ELSE? </span>
						<button type="button" onclick="location.href='<?= $search_url ?>'" class="btn btn-info">SEARCH
							HERE!
						</button>
						<div class="clear-marketingcamp"></div>
					</div>
				</div>
				<div class="col-xs-1 visible-xs">
				</div>
			</div>
		</div>
	</section>
<?php
get_footer('home');