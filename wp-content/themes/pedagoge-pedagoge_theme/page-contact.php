<?php 
/*
Template Name: Contact Us Page
*/

/**
 * Had to create different header, footer, and page templates because of spaghetti code
 * Will clean the code and create uniform structure (Header/Footer/Pages) after new design comes in.
 */
global $isVersion2;
$isVersion2 = true;
get_header( 'home_ver2' );
?>

<!-- 
 ######   #######  ##    ## ########    ###     ######  ######## 
##    ## ##     ## ###   ##    ##      ## ##   ##    ##    ##    
##       ##     ## ####  ##    ##     ##   ##  ##          ##    
##       ##     ## ## ## ##    ##    ##     ## ##          ##    
##       ##     ## ##  ####    ##    ######### ##          ##    
##    ## ##     ## ##   ###    ##    ##     ## ##    ##    ##    
 ######   #######  ##    ##    ##    ##     ##  ######     ##   
 -->
<br />
<section class="site-section site-section-top">
    <div class="container"><br><br><br>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 site-block" data-toggle="animation-appear" data-animation-class="animation-fadeInRight">
                <h1 class="site-heading text-center"><strong>CONTACT US</strong></h1>
                <h4 class="text-center">
                    <strong>If you would like to know more, discuss a possible project or simply say hi, feel free to contact us here.</strong>
                </h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4 col-md-offset-2 col-sm-6 form-group site-block" data-toggle="animation-appear" data-animation-class="animation-fadeInRight"><br>
            	<h5>
            		<i class="fa fa-phone fa-fw"></i>&nbsp;(+91)9883331570
            	</h5>
            	<h5>
            		<i class="fa fa-whatsapp fa-fw"></i>&nbsp;(+91)9073138973
            	</h5>
            	<h5>
            		<i class="fa fa-envelope-o fa-fw"></i>&nbsp;hello@pedagoge.com
            	</h5>
            	<br><br>
            	<span class="conatct-social">
        			<a href="https://www.facebook.com/pedagoge0/" target="_blank" class="conatct-social-icons"><i class="fa fa-facebook" style="padding: 0 0.2em;"></i></a>
                    <a href="https://twitter.com/pedagogebaba" target="_blank" class="conatct-social-icons"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.linkedin.com/company/pedagoge" target="_blank" class="conatct-social-icons"><i class="fa fa-linkedin"></i></a>
                    <a href="https://www.instagram.com/pedagogebaba/" target="_blank" class="conatct-social-icons"><i class="fa fa-instagram"></i></a>
            	</span>
            </div>

            <div class="col-md-4 col-sm-6 form-group site-block div_contact_form_area" data-toggle="animation-appear" data-animation-class="animation-fadeInRight">
                <form id="pdg_contact_form">
                    <label for="contact_name">Name *</label>
                    <input type="text" maxlength="50" id="contact_name" class="form-control input-lg" placeholder="Your name" required style="border-bottom-color: black; border-bottom: 0.2em solid;">
                    <br>
                    <label for="contact_email">Email *</label>
                    <input type="text" maxlength="100" id="contact_email" class="form-control input-lg .btn-bottom-black" placeholder="Your email" required style="border-bottom-color: black; border-bottom: 0.2em solid;">
                    <br>
                    <label for="contact_number">Contact No.</label>
                    <input type="text" maxlength="50" id="contact_number" class="form-control input-lg .btn-bottom-black" required placeholder="Your contact no" style="border-bottom-color: black; border-bottom: 0.2em solid;">
                    <br>
                    <label for="contact_message">Message *</label>
                    <textarea id="contact_message" rows="5" class="form-control input-lg .btn-bottom-black" required placeholder="Let us know how we can assist" style="border-bottom-color: black; border-bottom: 0.2em solid;"></textarea>
                    <br>
                    <input type="text" value="" id="txt_hnpt_fld" style="display:none !important; visibility:hidden !important;"/>
                    <div class="form-group text-center">
						<button id="submit_contact_form" class="btn btn-lg btn-signup btn-round" data-loading-text="Sending ...">Send Message</button>
                    </div>
                    <div class="image_loader">
                    	<img class="center-block" style="display:none;" src="<?php echo PEDAGOGE_ASSETS_URL;?>/images/ajax_loaders/loader.gif" id="img_contact_loader" />
                    </div>
                    <div id="result_area"></div>
                    
                </form>
            </div>
        </div>
    </div>
</section>

<?php
get_footer('home_ver2');