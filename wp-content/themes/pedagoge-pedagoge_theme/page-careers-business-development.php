<?php
/*
Template Name: Careers Business Development Page
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;
							<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Business Development
						</div>
						<div class="opPara1 jobDescPara">
							<p>Ability to bring clients on board and smooth talking them into being associated with
								us.<br/>Communicate with clarity in thought and making the message stand out.<br/>Capability
								to sell anything under the sun with confidence and persistence.<br/>Training and
								developing peers and juniors if the candidate is stellar in performance.<br/>Degree(s):
								Applicants pursuing any degree but having relevant skills and interest may apply.<br/>Year
								of degree completion: Applicants currently in any year of study or recent graduates but
								having relevant skills and interest may apply.<br/>Only applicants who are available
								full time for a minimum duration of 2 month(s) can apply.<br/>Applicants should be
								available to start the internship immediately.<br/>People residing in Kolkata and
								knowing the city very well.<br/>Interested in business development/marketing and
								sales<br/>Having the ability to convince people.<br/>Developing business from scratch
								i.e. generating leads and closing it with success<br/>Has good communication skills in
								languages such as English, Hindi and Bengali.<br/>Candidates willing to travel the city.<br/>Is
								excited about the startup environment and wants to challenge the status quo<br/>Wants to
								make himself/herself financially independent</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor,
								Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata - 700091,<br/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West
								Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home_ver2' );