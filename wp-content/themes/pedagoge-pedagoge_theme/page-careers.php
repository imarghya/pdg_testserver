<?php
/*
Template Name: Careers Page
*/

//global $isVersion2;
//$isVersion2 = true;
get_header( 'home' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";

?>
<div class="controller">
	<div class="row">
		<!-- slider -->
		<div id="myCarousel" class="carousel slide arrow_controller" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
				  <img class="img-responsive" id="slider1" src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/careers/team_group.jpg">
				</div>
				<div class="item">
				  <img class="img-responsive" id="slider2" src="<?php echo PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL; ?>/images/careers/collage.jpg">					  
				</div>
			</div>
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!-- /slider -->	
	</div>
</div>

    <div class="row">
		<div class="col-xs-12 joinHeader titlePadding text-center">
			<i class="fa fa-briefcase" aria-hidden="true"></i> Work With Us
		</div>
	</div>

<section class="wrapper container-fluid section3-careers">
	<div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 jobHeader titlePadding text-center">
			<i class="fa fa-search" aria-hidden="true"></i> Openings
		</div>
	</div>
	<!--		Start full time job-->
	<div class="row">
		<div class="col-xs-12 col-md-6 opWrapper">
			<h4 class="text-center titleWrapper">Full time</h4>
		    
			<div class="col-xs-12 opWrapper">
				<div class="opCard">
					<div class="opCardHeader opCardHeaderStyle1">
						<div class="col-xs-10 text-left">
							&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Student Consultant
						</div>
						<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
															aria-hidden="true"></i></div>
					</div>
					<div class="opPara makeLink">The opportunity involves looking after a dynamic work flow right
						from generating a lead to meeting and fulfilling the requirements of clients. <a
								href="<?php echo $joburl; ?>-student-consultant">See more details..</a>
					</div>
				</div>
			</div>
			
		</div>
		<!--end full time-->
		<!--start interns-->
		<div class="col-xs-12  col-md-6 opWrapper">
			<h4 class="text-center titleWrapper">Internships</h4>
			
			<div class="col-xs-12 opWrapper">
				<div class="opCard">
					<div class="opCardHeader opCardHeaderStyle4">
						<div class="col-xs-10 text-left">
							&nbsp;<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Business
							Development
						</div>
						<div class="col-xs-2 text-right"><i class="fa fa-chevron-circle-down fa-lg flip-up-opCard"
															aria-hidden="true"></i></div>
					</div>
					<div class="opPara makeLink">Individuals who are willing to bring clients on board and have the
						ability to communicate with clarity in thought and making the message stand out. <a
								href="<?php echo $joburl; ?>-business-development">See more details..</a>
					</div>
				</div>
			</div>
		</div>
		<!--end interns-->
	</div>
</section>
<?php
get_footer('home');
?>