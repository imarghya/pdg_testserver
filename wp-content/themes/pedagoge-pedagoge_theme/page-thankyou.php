<?php
/*
Template Name: Thank you
*/

get_header( 'home' );
$theme_dir = get_template_directory_uri();
$requestParamList = array ();
$responseParamList = array ();
?>
	<main class="padding-fix center-block">
		
		<section class="wrapper container-fluid section1 text-center center-block">
			<div class="row">
				<div class="congratulations">
					<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/congratulations.png">
				</div>
			</div>
			<div class="row">
				<div class="animation marginT10 animation">
					<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/animation.gif">
				</div>
			</div>
		</section>
		<section class="wrapper container-fluid section3">
			<div class="row">
				<div class="review col-md-6">
					<h3 class="bold text-center">Review us:</h3>
					<form class="form-review">
						<div class="form-group review-textarea center-block text-center">
							<div id="result_area" class="resultMsgs"></div>
							<div class="stars ">
								<label for="1">
									<input type="radio" class="radio-inline" name="reviewRadio" id="1" value="1">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star.png" class="1 nohover">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star_hover.png"
									     class="1 hover">
								</label>
								<label for="2">
									<input type="radio" class="radio-inline" name="reviewRadio" id="2" value="2">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star.png" class="2 nohover">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star_hover.png"
									     class="2 hover">
								</label>
								<label for="3">
									<input type="radio" class="radio-inline" name="reviewRadio" id="3" value="3">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star.png" class="3 nohover">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star_hover.png"
									     class="3 hover">
								</label>
								<label for="4">
									<input type="radio" class="radio-inline" name="reviewRadio" id="4" value="4">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star.png" class="4 nohover">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star_hover.png"
									     class="4 hover">
								</label>
								<label for="5">
									<input type="radio" class="radio-inline" name="reviewRadio" id="5" value="5">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star.png" class="5 nohover">
									<img src="<?= $theme_dir; ?>/assets/img/thankyou-payment/star_hover.png"
									     class="5 hover">
								</label></div>
							<span class="rateus">Rate us</span>
							<textarea class="form-control" id="reviewComment" rows="3" cols="2" name="reviewComment"
							          placeholder="Comments..."></textarea>
							<button class="btn-block btn submit-btn">Submit</button>
							<div class="image_loader">
								<img class="center-block" style="display:none;"
								     src="<?= PEDAGOGE_ASSETS_URL; ?>/images/ajax_loaders/loader.gif"
								     id="img_contact_loader2"/>
							</div>
						</div>
					</form>
				</div>
				<div class="contactus col-md-6 text-center">
					<h3 class="bold text-center">Contact us:</h3>
					<div class="text-left text-inline-block">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;hello@pedagoge.com<br/>
						<i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp;+91-90731-38973<br/>
						<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;+91-98833-31570<br/>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php
get_footer( 'home' );
