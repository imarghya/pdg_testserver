<?php
/*
Template Name: Careers - Associate UI/UX Developer
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>

	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;
							<i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Associate Web Developer
						</div>
						<div class="opPara1 jobDescPara">
							<p>In today&#39;s day and age working with a start-up seems to be the &quot;thing
								to do&quot; and we here at</p>

							<p>Pedagoge look at business in its rawest form, which is to create impact. Our
								approach is</p>

							<p>strategy driven, we believe that set targets can be achieved with the correct
								strategy and</p>

							<p>not merely though burning up resources. Working with us, one would learn how
								to hustle</p>

							<p>and get things done; we provide you an environment with a steep learning
								curve.</p>

							<p>&nbsp;</p>

							<p><strong>Responsibilities</strong>:</p>

							<p>- Managing and improving a dynamic website catering to the under 25
								population</p>

							<p>- Troubleshoot, test and maintain the core product software and databases to
								ensure</p>

							<p>strong optimization and functionality</p>

							<p>- Knowledge of relational databases, version control tools and developing
								services</p>

							<p>- Experience in common third-party APIs (Google, Facebook, Payment
								Gateways)</p>

							<p>- Passion for excellent design, coding practices and developing new ideas</p>

							<p>- Be responsible and accountable for your work with minimum guidance</p>

							<p>- Setting periodic goals and achieving them on timely basis</p>

							<p>&nbsp;</p>

							<p><strong>Requirements</strong>:</p>

							<p>- Excellent Web Designing skills</p>

							<p><span>- </span>Bootstrap, jQuery, JavaScript, HTML5, CSS3. All of
								these skills are a must</p>

							<p><span>- </span>Minimum 1.5 years experience in front-end website
								development</p>

							<p><span>- </span>Knowledge in PHP and WordPress is a plus point</p>

							<p><span>- </span>Self-motivated person with a knack for process
								improvement</p>

							<p><span>- </span>Eager to take initiative, learn and try new
								technologies</p>

							<p><span>- </span>Ability to handle responsibilities and meet
								deadlines</p>

							<p>What can the candidate expect from us:</p>

							<p><span>- </span>Working with a Nasscom 10,000 startup in the EduTech
								industry</p>

							<p><span>- </span>Ownership of work and freedom to experiment new
								developing concepts</p>

							<p><span>- </span>Directly reporting to the Senior Developer of the
								team</p>

							<p><span>- </span>Working with Marketing team to create landing pages,
								integrate and monitor</p>

							<p>analytics on the website and get real-time feedback from end consumers</p>

							<p><span>- </span>Training newcomers, creating processes and
								responsibility of website UI/UX</p>

							<p>Compensation: Competitive salary and performance based incentives</p>

							<p>&nbsp;</p>

							<p><strong>Location</strong>: Kolkata</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor, Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata -
								700091,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer( 'home_ver2' );