<?php
/*
Template Name: placements-technoindia
*/

get_header( 'home' );
$theme_dir = get_template_directory_uri();
?>
	<script>
		console.log("Okay smartypants, if you are interested in joining the company email your resume to ganeshr@pedagoge.com. Also, don't forget to mention this little adventure yours");
	</script>
	<section class="section1 bg-image-main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 text-left">
					<a href="<?php echo home_url( '/' ); ?>">
						<img class="logo" src="<?= $theme_dir; ?>/assets/img/logo.png" alt="Pedagoge Logo"
						     width="">
					</a>
				</div>
			</div>
			<div class="row hr">
			</div>
			<div class="content-wrapper">
				<div class="row center-block text-center">
					<h1 class="heading">WHY SHOULD YOU WORK AT PEDAGOGE?</h1>
				</div>
				<div class="hidden-xs hidden-sm divider-50"></div>
				<div class="row center-block text-center col-xs-12 hidden-xs hidden-sm">
					<div class="col-xs-4">
						<h2 class="subheading">SHAPE THE CULTURE
							AROUND YOU</h2>
						<p>At Pedagoge you not only decide where you work, you also decide how you work.</p>
					</div>

					<div class="col-xs-4">
						<h2 class="subheading">VERSATILITY</h2>
						<p>Everyone at Pedagoge is
							expected to wear multiple hats. We teach you to adopt new skills, responsibilities
							and become a brain box!
						</p>
					</div>

					<div class="col-xs-4">
						<h2 class="subheading">YOU LEARN; ALOT!</h2>
						<p>Learning leads to increased responsibility, multiple opportunities whilst accelerating talent
							and knowledge.</p>
					</div>
				</div>
				<div class="row center-block text-center col-xs-12 visible-xs visible-sm">
					<div id="slideWhyText" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<h2 class="subheading">SHAPE THE CULTURE
									AROUND YOU</h2>
								<p>At Pedagoge you not only decide where you work, you also decide how you work.</p>
							</div>

							<div class="item">
								<h2 class="subheading">VERSATILITY</h2>
								<p>Everyone at Pedagoge is
									expected to wear multiple hats. We teach you to adopt new skills, responsibilities
									and become a brain box!
								</p>
							</div>

							<div class="item">
								<h2 class="subheading">YOU LEARN; ALOT!</h2>
								<p>Learning leads to increased responsibility, multiple opportunities whilst
									accelerating talent and knowledge.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row center-block text-center">
					<div class="col-xs-12">
						<img src="<?= $theme_dir ?>/assets/img/placements-technoindia/why.png"
						     class="img-responsive center-block why">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section2">
		<div class="container-fluid">
			<div class="row center-block text-center culture vertical-align">
				<div class="col-xs-12">
					<h2 class="card-heading">CULTURE AT PEDAGOGE</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 hidden-xs hidden-sm"></div>
				<div class="col-xs-12 col-md-6 center-block text-center">
					<p class="desc">We, at Pedagoge, believe that it is an
						alive and evolving organization
						that grows with the values its people bring to it.
					</p>
				</div>
				<div class="col-md-3 hidden-xs hidden-sm"></div>
			</div>
			<div class="row content-spot">
				<div class="row center-block text-center col-xs-12 visible-xs visible-sm">
					<div id="slideCulture" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<div>
									<p class="black-color">Work
									<p class="blue-color">HARD.</p>
									<p class="black-color">Party</p>
									<p class="blue-color">HARDER.</p>
								</div>
							</div>

							<div class="item">
								<div>
									<p class="black-color">Eager-To-Learn</p>
									<p class="blue-color">HUSTLERS</p>
								</div>
							</div>

							<div class="item">
								<div>
									<p class="blue-color">PARTICIPATIVE</p>
								</div>
							</div>

							<div class="item">
								<div>
									<p class="black-color">Freedom of</p>
									<p class="blue-color">EXPRESSION</p>
								</div>
							</div>

							<div class="item">
								<div>
									<p class="black-color">Vision of
										a common</p>
									<p class="blue-color">GOAL</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 hidden-xs hidden-sm"></div>
				<div class="col-xs-12 col-md-8 image-spot">
					<img src="<?= $theme_dir ?>/assets/img/placements-technoindia/culture.png"
					     class="img-responsive center-block cultural">
					<div class="hidden-xs hidden-sm">
						<div class="work absolute">
							<p class="black-color">Work
							<p class="blue-color">HARD.</p>
							<p class="black-color">Party</p>
							<p class="blue-color">HARDER.</p>
						</div>


						<div class="eager absolute">
							<p class="black-color">Eager-To-Learn</p>
							<p class="blue-color">HUSTLERS</p>
						</div>


						<div class="participative absolute">
							<p class="blue-color">PARTICIPATIVE</p>
						</div>


						<div class="freedom absolute">
							<p class="black-color">Freedom of</p>
							<p class="blue-color">EXPRESSION</p>
						</div>


						<div class="vision absolute">
							<p class="black-color">Vision of
								a common</p>
							<p class="blue-color">GOAL</p>
						</div>
					</div>
				</div>
				<div class="col-md-2 hidden-xs hidden-sm"></div>
			</div>
		</div>
	</section>

	<section class="section3">
		<div class="container-fluid">
			<div class="row center-block text-center views vertical-align">
				<div class="col-xs-12">
					<h2 class="card-heading">COMPANY VIEWS</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="row">
						<img
							src="<?= $theme_dir ?>/assets/img/placements-technoindia/company_views1.png"
							class="img-responsive center-block company_views1 pull-left">
					</div>
				</div>
				<div class="col-xs-12 content-spot col-md-4 center-block text-center">
					<p class="blue-color">Trust</p>
					<p class="blue-color">Hustle</p>
					<p class="blue-color">Jugaad</p>
					<p class="blue-color">Execution</p>
					<p class="blue-color">Innovation</p>
					<p class="blue-color">Due Diligence</p>
				</div>
				<div class="col-xs-12 col-md-4 text-right">
					<div class="row">
						<img
							src="<?= $theme_dir ?>/assets/img/placements-technoindia/company_views2.png"
							class="img-responsive center-block company_views2 pull-right">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section4">
		<div class="container-fluid">
			<div class="row center-block opportunities vertical-align">
				<div class="col-xs-12">
					<h2 class="card-heading">OPPORTUNITIES</h2>
				</div>
			</div>
			<div class="row center-block text-center panels-container vertical-align">
				<div class="col-xs-12">
					<div id="panel-wrapper" class="size-adjust" role="tablist" aria-multiselectable="true">
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading1">
								<h5 class="opportunities_panel_heading1_style text-left">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel1" aria-expanded="true"
									   aria-controls="opportunities_panel_panel1"
									   class="right accordion-toggle collapsed">
										Business Expansion Coordinator
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel1" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading1">
								<div class="card-block text-left">
									As we’re growing rapidly, we are looking to expand into more cities. As part of
									that, we are looking for a business expansion coordinator with experience in
									helping a business grow and the ability to create business plans for our future.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading2">
								<h5 class="opportunities_panel_heading2_style text-right">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel2" aria-expanded="true"
									   aria-controls="opportunities_panel_panel2"
									   class="left accordion-toggle collapsed">
										Sales Associate
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel2" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading2">
								<div class="card-block text-left">
									The opportunity involves looking after a dynamic work flow right from generating a
									lead to meeting and fulfilling the requirements of clients.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading2">
								<h5 class="opportunities_panel_heading3_style text-left">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel3" aria-expanded="true"
									   aria-controls="opportunities_panel_panel3"
									   class="right accordion-toggle collapsed">
										Backend Web Developer
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel3" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading3">
								<div class="card-block text-left">
									In today's day and age working with a start-up seems to be the "thing to do" and we
									here at Pedagoge look at business in its rawest form, which is to create impact.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading4">
								<h5 class="opportunities_panel_heading4_style text-right">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel4" aria-expanded="true"
									   aria-controls="opportunities_panel_panel4"
									   class="left accordion-toggle collapsed">
										Web Developers
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel4" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading4">
								<div class="card-block text-left">
									Looking for proactive individuals who will help develop our website from an early
									stage.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading5">
								<h5 class="opportunities_panel_heading5_style text-left">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel5" aria-expanded="true"
									   aria-controls="opportunities_panel_panel5"
									   class="right accordion-toggle collapsed">
										Research Internship
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel5" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading5">
								<div class="card-block text-left">
									Need interns who will be helping with market research of multiple cities based on
									various relevant parameters and data (both primary and secondary).
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading6">
								<h5 class="opportunities_panel_heading6_style text-right">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel6" aria-expanded="true"
									   aria-controls="opportunities_panel_panel6"
									   class="left accordion-toggle collapsed">
										Finance
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel6" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading6">
								<div class="card-block text-left">
									This profile entails responsibilities of managing income, expenses and other money
									transactions
									so that the organization remains profitable. Candidate needs to liaise with all
									departments of the organization and bring in a financial balance.
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header panel-heading" role="tab" id="opportunities_panel_heading7">
								<h5 class="opportunities_panel_heading7_style text-left">
									<a data-toggle="collapse" data-parent="#panel-wrapper"
									   href="#opportunities_panel_panel7" aria-expanded="true"
									   aria-controls="opportunities_panel_panel7"
									   class="right accordion-toggle collapsed">
										Business Development
									</a>
								</h5>
							</div>

							<div id="opportunities_panel_panel7" class="collapse panel-body" role="tabpanel"
							     aria-labelledby="opportunities_panel_heading7">
								<div class="card-block text-left">
									Individuals who are willing to bring clients on board and have the ability to
									communicate with
									clarity in thought and making the message stand out.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home' );