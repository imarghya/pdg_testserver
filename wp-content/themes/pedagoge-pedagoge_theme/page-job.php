<?php
/*
Template Name: job
*/
global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
global $post;
$title = get_the_title();
?>
<main class="padding-fix center-block">
	<div class="search_result_main desktop background-adjust non-scrollable-fixed-search-wrapper">
		<div class="hide search-page-type" data-type="profile"></div>
	</div>
	<div class="container">
		<div>
			
		</div>
	</div>













		<!--section-info-->
		<section class="section-info">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 center-block">
						<h1 class="main-heading text-center"><?= $title?></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 center-block">
						<?php
						$content = apply_filters( 'the_content', $post->post_content );
						echo $content;
						?>
					</div>
				</div>
			</div>
		</section>
		<!--/section-info-->
	</main>

	<?php
	get_footer( 'home_ver2' );
