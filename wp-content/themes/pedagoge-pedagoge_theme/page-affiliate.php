<?php 
/*
Template Name: Affiliate with Us Page
*/

/**
 * Had to create different header, footer, and page templates because of spaghetti code
 * Will clean the code and create uniform structure (Header/Footer/Pages) after new design comes in.
 */

//get_header('home');

global $isVersion2;
$isVersion2 = true;
get_header( 'home_ver2' );
?>

<section class="site-content site-section">
    <div class="container"><br><br><br>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12 site-block" data-toggle="animation-appear" data-animation-class="animation-fadeInRight">
                <h1 class="h1 site-heading text-center"><strong>Affiliate With Us</strong></h1>
            </div>

            <div class="col-md-8 col-sm-12 col-xs-12 form-group site-block" data-toggle="animation-appear" data-animation-class="animation-fadeInRight"><br>
                <h3>
                    We are always in the lookout for smart, frugal, jugaad and innovative affiliations!<br><br>
                    For all affiliations, you can get in touch with the following person:<br><br>
                        <i class="fa fa-user fa-fw"></i>&nbsp;Siddharth Bhagat<br><br>
                        <i class="fa fa-envelope-o fa-fw"></i>&nbsp;<b><span style="font-size: 0.6em; text-decoration: underline;">sidhartha.bhagat@pedagoge.com</span></b><br><br>
                        <i class="fa fa-phone fa-fw"></i>&nbsp;+91 8033172719<br><br>
                        If you want to be all corporate-ish, then drop in a mail to: hello@pedagoge.com<br><br>
                        We shall get back to you in a jiffy!<br><br>
                </h3>
            </div>
        </div>
    </div>
</section>

<?php
//get_footer('home');
get_footer('home_ver2');
?>