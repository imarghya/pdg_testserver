 <?php
 $pagename = get_query_var('pagename');
/**
 * The header for pedagoge version 2 theme.
 *
 */
global $post;
$slug = '';
if ( isset( $post ) ) {
	$current_post = get_post( $post );
	if ( ! empty( $current_post ) ) {
		$slug = $current_post->post_name;
	}
}
$is_production_environment = false;
$server_name = $_SERVER['SERVER_NAME'];
switch ( $server_name ) {
	case 'www.pedagoge.com':
	//case 'www.pedagog.in':
	//case 'www.pedagoge.net':
	case 'dev.pedagoge.com':
	case 'test.pedagoge.com':
	$is_production_environment = true;
		break;
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<?php get_template_part( 'template-parts/google_conversion_10052017'); ?>
	
	<?php if ( $is_production_environment ): ?>			
		<!-- Google Tag Manager -->
		<script>(function ( w, d, s, l, i ) {
				w[l] = w[l] || [];
				w[l].push( {
					'gtm.start': new Date().getTime(), event: 'gtm.js'
				} );
				var f = d.getElementsByTagName( s )[0],
					j = d.createElement( s ), dl = l != 'dataLayer' ? '&l=' + l : '';
				j.async = true;
				j.src =
					'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
				f.parentNode.insertBefore( j, f );
			})( window, document, 'script', 'dataLayer', 'GTM-PPFH97' );</script>
		<!-- End Google Tag Manager -->

	<?php endif; ?>	
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	      
	<meta name="HandheldFriendly" content="true"/>


	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!--<meta name="description" content="Homepage of Pedagoge.">-->

          <title>Pedagoge - Online Tuition Teacher| Private Tutors | Home Tutors</title>
          
	  <meta name="description" content="Find verified profiles for home tutor, online teacher or coaching institute for any subject or activity like Mathematics, CAT, Dance, Spanish.">
	  
	  <meta name="keywords" content="Need Tuition Teacher, Need Tutors In Delhi NCR, Experienced Tutor In NCR, Experienced Tutor In Kolkata, Tutor For Home Tuition In Kolkata, Online Classes, Home Tutor Required In Kolkata, Online Tuition, All Subject Home Teacher In Kolkata, All Subject Teacher In Delhi NCR, Montessori Home Tutors In NCR, Find Best Institute In Kolkata, Private Tuition Teacher">

	<meta name="author" content="pedagoge">
	<link rel="shortcut icon" href="<?php echo PEDAGOGE_THEME_URL; ?>/assets/img/fav_icon_logo_orb_small.png">
	<?php
	if(!is_page('landing-students') || !is_page('landing-teachers')){
	wp_head();
	}
	?>
	
	<?php 
/*************** Start OF Landing Page **********************/
if(is_page('landing-students') || is_page('landing-teachers')){
?>	

   				<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

                <link href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/bootstrap.css" type="text/css" rel="stylesheet" media="screen">

                <!-- onepage scroll stylesheet-->

                <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/onepage-scroll.css">

                <!-- theme stylesheet-->

                <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/style.default.css" id="theme-stylesheet">

                 

                <link href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/owl.carousel.min.css" rel="stylesheet" type="text/css">

                <link href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">

                <!-- Navigation Menu-->

                <link href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/menu.css" rel="stylesheet" type="text/css" media="screen">

                <!-- Custom stylesheet - for your changes-->

		<?php if(is_page('landing-students')){?>

                <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/custom.css">

                <?php }else{ ?>

		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/custom_teacher.css">

                <?php } ?> 
                
                <?php 
				//get_template_part( 'template-parts/hot', 'jar' );
				//get_template_part( 'template-parts/fb', 'pixel' );
		?>
    <?php
	}
	/*************** Start OF Landing Page **********************/
		?>
	<?php // get_template_part( 'template-parts/hot', 'jar' );get_template_part( 'template-parts/fb', 'pixel' ); ?>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>    <![endif]-->
	<!--  Android 5+ material Color-->
	<meta name="theme-color" content="#4d738a">
	<script type="text/javascript">
		var site_var = {};
		site_var.url = "<?php echo site_url();?>/";
		(function ( i, s, o, g, r, a, m ) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push( arguments )
				}, i[r].l = 1 * new Date();
			a = s.createElement( o ),
				m = s.getElementsByTagName( o )[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore( a, m )
		})( window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga' );

		ga( 'create', 'UA-70113984-1', 'auto' );
		ga( 'send', 'pageview' );

	</script>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push( {
			google_ad_client: "ca-pub-5262286787304369",
			enable_page_level_ads: true
		} );
	</script>
    
    
    
    
</head>
<body <?php body_class( "body-container" ); echo pedagoge_body_tags(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPFH97"
	        height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<?php if(trim($pagename)!='landing-students' && trim($pagename)!='landing-teachers'){ echo '<div class="body-wrapper">'; } ?>

<?php
$excludeVersion2Header = array ( '' );
$excludeVersion2HeaderMobile = array ( '' );
if ( ! in_array( $slug, $excludeVersion2Header ) ) {
	get_template_part( 'template-parts/ver2/navbar' );
} else if (! in_array( $slug, $excludeVersion2HeaderMobile ) ) {
	get_template_part( 'template-parts/ver2/navbar-mobile' );
}
?>