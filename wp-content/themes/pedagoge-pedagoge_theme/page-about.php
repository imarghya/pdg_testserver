<?php
/*
Template Name: About Us Page
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
?>
	<main class="padding-fix no-select">
		<!-- scroll to top -->
		<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
		<!--/scroll to top -->
		<!-- scroll to bottom -->
		<a href="javascript:void(0);" class="scroll-to-bottom"><i class="material-icons">&#xE313;</i></a>
		<!--/scroll to bottom -->
		<div class="isMobileTablet visible-xs visible-sm"></div>
		<div class="isMobile visible-xs"></div>
		<div class="isTablet visible-sm"></div>

		<!--section-top-->
		<section class="section-top padding-fix background-color valign-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 position-static">
						<div class="main-heading-wrapper">
							<h1 class="main-heading white-text text-bold">
								<span class="clean-text">PEDAGOGE</span><span class="clean-text">SE</span><span class="clean-text">POOCHO</span>
							</h1>
						</div>
					</div>
					<div class="col-xs-12 position-static telescope-guy-wrapper">
						<img src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/guy.svg?ver=0.2"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/guy.png?ver=0.2';this.className='responsive-png'">
					</div>
				</div>
			</div>
		</section>
		<!--/section-top-->

		<!--section-aboutus-->
		<section class="section-aboutus">
			<div class="container-fluid heading-block">
				<div class="row">
					<div class="sub-heading-wrapper">
						<h2 class="sub-heading text-right text-bold no-margin">
							About Us
						</h2>
					</div>
				</div>
			</div>
			<div class="container-fluid contents-block padding-fix background-color">
				<div class="col-xs-12">
					<div class="row">
						<div class="dictionary-wrapper">
							<p class="item-name white-text text-bold no-margin">
								Pedagogy
							</p>
							<p class="item-phonetics white-text no-margin">
								/ˈpɛdəɡɒdʒi,ˈpɛdəɡɒɡi/
							</p>
							<p class="item-desc white-text no-margin">
								<i>Noun</i>
							</p>
							<p class="item-desc white-text">
								Noun: <span class="text-bold">pedagogy</span>; plural noun: <span class="text-bold">pedagogies</span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="row">
						<div class="content-wrapper">
							<div class="col-md-8 right">
								<div class="row">
									<p class="item-body white-text text-justify">
										<?= is_array( get_post_custom_values( "about_us_about_pedagoge" ) ) ? get_post_custom_values( "about_us_about_pedagoge" )[0] : "" ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/section-aboutus-->

		<!--section-founders-->
		<section class="section-founders background-color">
			<div class="container-fluid heading-block">
				<div class="row">
					<div class="sub-heading-wrapper">
						<h2 class="sub-heading text-right text-bold no-margin">
							Founder
						</h2>
					</div>
				</div>
			</div>
			<div class="container-fluid contents-block padding-fix">
				<div class="founders-slider-top-block background-color row">
					<div class="col-xs-12">
						<div class="founders-quote center-block">
							<?= is_array( get_post_custom_values( "about_us_founders_quote" ) ) ? get_post_custom_values( "about_us_founders_quote" )[0] : "" ?>
						</div>
						<div class="slide-wrapper">
							<div id="foundersCarousel" class="carousel" data-ride="carousel">
								<!--<ol class="carousel-indicators">
									<li data-target="#foundersCarousel" data-slide-to="0" class="active"></li>
									<li data-target="#foundersCarousel" data-slide-to="1"></li>
								</ol>-->
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="rohit center-block text-center">
											<img class="img-responsive center-block" alt="Rohit Mall" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/rohit.svg"
											     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/rohit.png';this.className='img-responsive center-block'">
											<h3 class="item-name text-bold">ROHIT MALL</h3>
										</div>
									</div>

									<!--<div class="item">
										<div class="mohit center-block text-center">
											<img class="img-responsive center-block" alt="Mohit Daga" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/mohit.svg"
											     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/mohit.png';this.className='img-responsive center-block'">
											<h3 class="item-name text-bold">MOHIT DAGA</h3>
										</div>
									</div>-->
								</div>
								<!--<a class="left carousel-control" href="#foundersCarousel" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span> </a>
								<a class="right carousel-control" href="#foundersCarousel" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span> </a>-->
							</div>
						</div>
					</div>
				</div>
				<div class="founders-slider-bottom-block background-color row">
					<div class="col-xs-12">
						<div class="slide-contents-wrapper">
							<div class="rohit">
								<p class="item-desc text-bold text-justify"><?= is_array( get_post_custom_values( "about_us_founders_rohit" ) ) ? get_post_custom_values( "about_us_founders_rohit" )[0] : "" ?></p>
							</div>
							<div class="mohit">
								<p class="item-desc text-bold text-justify"><?= is_array( get_post_custom_values( "about_us_founders_mohit" ) ) ? get_post_custom_values( "about_us_founders_mohit" )[0] : "" ?></p>

							</div>
						</div>
					</div>
				</div>
		</section>
		<!--/section-founders-->

		<!--section-stats-->
		<section class="section-stats background-color padding-fix">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-4 center-block center-text">
						<img class="img-responsive center-block" alt="HOURS OF BRAINSTORMING" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/brainstorming.svg"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/brainstorming.png';this.className='img-responsive center-block'">
						<p class="item-count white-text text-bold text-center center-block margin-top-10"><?= is_array( get_post_custom_values( "about_us_hours_of_brainstorming" ) ) ? get_post_custom_values( "about_us_hours_of_brainstorming" )[0] : "" ?></p>
						<p class="item-desc white-text text-bold text-center center-block">HOURS OF BRAINSTORMING</p>
					</div>
					<div class="col-xs-12 col-sm-4 center-block center-text">
						<img class="img-responsive center-block" alt="CUPS OF COFFEE" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/coffee.svg?ver=0.2"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/coffee.png?ver=0.2';this.className='img-responsive center-block'">
						<p class="item-count white-text text-bold text-center center-block margin-top-10"><?= is_array( get_post_custom_values( "about_us_cups_of_coffee" ) ) ? get_post_custom_values( "about_us_cups_of_coffee" )[0] : "" ?></p>
						<p class="item-desc white-text text-bold text-center center-block">CUPS OF COFFEE</p>
					</div>

					<div class="col-xs-12 col-sm-4 center-block center-text">
						<img class="img-responsive center-block" alt="FISTBUMPS COMPLETED" src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/fistbumps.svg"
						     onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/about-us/fistbumps.png';this.className='img-responsive center-block'">
						<p class="item-count white-text text-bold text-center center-block margin-top-10"><?= is_array( get_post_custom_values( "about_us_fistbumps_completed" ) ) ? get_post_custom_values( "about_us_fistbumps_completed" )[0] : "" ?></p>
						<p class="item-desc white-text text-bold text-center center-block">FISTBUMPS COMPLETED</p>
					</div>
				</div>
		</section>
		<!--/section-stats-->
	</main>

<?php
get_footer( 'home_ver2' );