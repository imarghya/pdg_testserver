<?php
/*
Template Name: Buyer, Seller Terms & Conditions
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
global $post;
?>
	<main class="padding-fix center-block">
		<!-- scroll to top -->
		<a href="javascript:void(0);" class="scroll-to-top"><i class="material-icons">&#xE316;</i></a>
		<!--/scroll to top -->
		<!-- scroll to bottom -->
		<a href="javascript:void(0);" class="scroll-to-bottom"><i class="material-icons">&#xE313;</i></a>
		<!--/scroll to bottom -->
		<div class="search_result_main desktop background-adjust non-scrollable-fixed-search-wrapper">
			<div class="hide search-page-type" data-type="profile"></div>
			<?php
			require_once( locate_template( 'template-parts/ver2/search_bar.php' ) );
			?>
		</div>
		<!--section-info-->
		<section class="section-info">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 center-block">
						<h1 class="main-heading text-center">Buyer Seller Terms</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 center-block">
						<?php
						$content = apply_filters( 'the_content', $post->post_content );
						echo $content;
						?>
					</div>
				</div>
			</div>
		</section>
		<!--/section-info-->
	</main>

<?php
get_footer( 'home_ver2' );