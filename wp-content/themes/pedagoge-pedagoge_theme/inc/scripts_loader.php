<?php


add_filter( 'script_loader_tag', function ( $tag, $handle ) {

	switch ( $handle ) {
		case 'wp-embed':
			return str_replace( ' src', ' async="async" src', $tag );
			break;
		case 'backstretch':
		case 'bootstrap':
		case 'homepage_plugins':
		case 'modernizr':
			return str_replace( ' src', ' defer="defer" src', $tag );
			break;
		default:
			return $tag;
			break;
	}
	/*if ( 'backstretch' !== $handle ) {

	}*/
}, 10, 2 );


/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'pedagoge_theme_scripts' );


function pedagoge_theme_scripts() {
	global $post, $wp_scripts, $isVersion2;

	$slug = '';
	if ( isset( $post ) ) {
		$current_post = get_post( $post );
		if ( ! empty( $current_post ) ) {
			$slug = $current_post->post_name;
		}
	}
	// Register Styles and Scripts
	fn_register_pedagoge_scripts();

	/*whether the pedagoge version 2 theme is used here*/
	if ( ! isset( $isVersion2 ) || ! $isVersion2 ) {
		#Theme - version 1

		//Load Common/Plugins Styles
		wp_enqueue_style( 'bootstrap-style' );

		//Load Common/Plugins JS
		wp_enqueue_script( 'modernizr' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'bootstrap' );
		if ( is_front_page() ) {
			wp_enqueue_script( 'homepage_plugins' );
		} else {
			wp_enqueue_style( 'jqueryui' );
			wp_enqueue_style( 'select2' );
			wp_enqueue_style( 'pedagoge_plugins-style' );
			wp_enqueue_style( 'pdg_main-style' );
			wp_enqueue_style( 'pdg_theme-style' );
			wp_enqueue_style( 'pdg_custom-style' );


			wp_enqueue_script( 'jqueryui' );
			wp_enqueue_script( 'select2' );
			wp_enqueue_script( 'backstretch' );
			wp_enqueue_script( 'plugins' );
			wp_enqueue_script( 'app' );
		}

		switch ( $slug ) {
			case 'privacy-policy':
			case 'terms-and-conditions':
			case 'teachers':
				break;
			case 'forteachers':
				wp_enqueue_script( 'validationEngine_custom_script' );
				wp_enqueue_script( 'validationEngine_custom_script_en' );
				wp_enqueue_script( 'forteachers_plugins' );
				wp_enqueue_style( 'forteachers_styles' );
				wp_enqueue_style( 'validationEngine_custom_styles' );
				break;
			case 'findtherightteacher':
				wp_enqueue_script( 'pdg_marketingcamp' );
				wp_enqueue_style( 'pdg_marketingcamp' );
				break;
			case 'business':
				wp_enqueue_script( 'pdg_business' );
				wp_enqueue_script( 'webui_popover' );
				wp_enqueue_style( 'pdg_business' );
				wp_enqueue_style( 'webui_popover' );
				wp_enqueue_script( 'validationEngine_script' );
				wp_enqueue_script( 'validationEngine_script_en' );
				wp_enqueue_style( 'validationEngine_styles' );
				break;
			case 'movie':
			case 'movies':
				wp_enqueue_style( 'pdg_moviescamp' );
				wp_enqueue_style( 'pdg_marketingcamp' );
				wp_enqueue_script( 'homepage_plugins' );
				wp_enqueue_script( 'pdg_marketingcamp' );
				break;
			case 'payment':
				wp_enqueue_style( 'thankyou-payment' );
				wp_enqueue_script( 'thankyou-payment' );
				break;
			case 'thank-you':
				wp_enqueue_style( 'thankyou-payment' );
				wp_enqueue_script( 'thankyou-payment' );
				break;
			/*Anything starting with "Careers" will be treated as the careers page*/
			case substr( $slug, 0, 7 ) === 'careers':
				wp_enqueue_style( 'pdg_careers' );
				wp_enqueue_script( 'pdg_careers' );
				break;
			case 'arts':
			case 'competitiveexams':
			case 'dance':
			case 'schoolsubjects':
				wp_enqueue_style( 'pedagoge_landingjul09' );
				wp_enqueue_script( 'pedagoge_landingjul09' );
				break;

			case 'about-us':
				wp_enqueue_script( 'aboutus' );
				wp_enqueue_style( 'font-clicker-script' );
				wp_enqueue_style( 'aboutus' );
				break;
			default:
				if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
					wp_enqueue_script( 'comment-reply' );
				}
				break;
		}
	} else {
		#theme - version 2
		if ( is_front_page() ) {
			//todo: work on the optimization here
			common_scripts();
			#desktop styles/script load
			wp_enqueue_style( 'ver2_home' );
			wp_enqueue_script( 'ver2_home' );
			wp_enqueue_script( 'ver2_search_common_ui' );
		} else {
			switch ( $slug ) {
				case 'terms':
				case 'faq':
				case 'buyer-seller-terms':
				case 'privacy-policy':
					common_scripts();
					//styles
					wp_enqueue_style( 'ver2_staticpages' );
					//script
					wp_enqueue_script( 'ver2_search_common_ui' );
					wp_enqueue_script( 'ver2_staticpages' );
					break;
				case 'about-us':
					common_scripts();
					//styles
					wp_enqueue_style( 'ver2_aboutus' );

					//script
					wp_enqueue_script( 'ver2_aboutus' );
					break;
				case 'institution':
					common_scripts();
					//styles
					wp_enqueue_style( 'ver2_institution' );

					//script
					wp_enqueue_script( 'ver2_institution' );
					break;
				case 'contact-us':
					common_scripts();
					wp_enqueue_style( 'ver2_staticpages' );
					wp_enqueue_script( 'contactus' );
					break;
				case 'affiliates':
					common_scripts();
					wp_enqueue_style( 'ver2_staticpages' );
					break;

				case 'careers-associate-web-developer':
				case 'careers-business-development':      
				case 'careers-finance-operations-associate':
				case 'careers-graphics-design':			      
                case 'careers-marketing-associate':
				case 'careers-sales-associate':
                case 'careers-sales-operations-head':
				case 'careers-web-developers':
				case 'careers-student-consultant':
				      common_scripts();
				      wp_enqueue_style('pdg_careers');
				      break;  
				default:
					break;
			}
		}
	}
}

function common_scripts() {
	wp_enqueue_style( 'quicksand' );
	wp_enqueue_style( 'material_icons' );
	wp_enqueue_style( 'bootstrap-style' );
	wp_enqueue_style( 'bootstrap_material_design' );
	wp_enqueue_style( 'material_ripples' );
	wp_enqueue_style( 'ver2_global_all' );
	wp_enqueue_style( 'fontawesome-style' );
	wp_enqueue_script( 'ver2_homepage_plugins' );
	wp_enqueue_script( 'ver2_global_all' );
	wp_enqueue_script( 'ver2_social_login' );
	wp_enqueue_style( 'validationEngine_styles' );
	wp_enqueue_script( 'validationEngine_script' );
	wp_enqueue_script( 'validationEngine_script_en' );
	wp_enqueue_script( 'ver2_modals_callback' );

}


/**
 * Register all scripts here...
 */
function fn_register_pedagoge_scripts() {


	$PEDAGOGE_THEME_URL = get_stylesheet_directory_uri();
	$common_css_ver = '0.2';
	$custom_css_ver = '0.2';

	$common_js_version = '0.2';

	$custom_js_version = '0.3';

	#<Themes version 2 files starts>
	$ver2_css = '0.65';
	$ver2_js = '0.68';

	#css------------------------------->
	//Common CSS files
	wp_register_style( 'quicksand', 'https://fonts.googleapis.com/css?family=Quicksand:400,700' );
	wp_register_style( 'material_icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
	wp_register_style( 'bootstrap_material_design', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/libs/bootstrap-material-design.min.css', array (), $ver = '0.1' );
	wp_register_style( 'material_ripples', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/libs/ripples.min.css' );
	wp_register_style( 'fullpage', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.min.css' );

	//Custom CSS files
	wp_register_style( 'ver2_global_all', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/global-all.css', array (), $ver = $ver2_css );
	wp_register_style( 'ver2_home', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/home.css', array (), $ver = $ver2_css );
	wp_register_style( 'ver2_staticpages', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/staticpages.css', array (), $ver = $ver2_css );
	wp_register_style( 'ver2_aboutus', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/aboutus.css', array (), $ver = $ver2_css );
	wp_register_style( 'ver2_aboutus', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/aboutus.css', array (), $ver = $ver2_css );
	wp_register_style( 'ver2_institution', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/css/institution.css', array (), $ver = $ver2_css );

	#js------------------------------->
	//Common JS files
	wp_register_script( 'jquery_2_1_4', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js', array (), 0.1, true );
	wp_register_script( 'mustache', 'https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.2.1/mustache.min.js', array (), 0.1, true );
	wp_register_script( 'selectize', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.3/js/selectize.min.js', array (), 0.1, true );
	wp_register_script( 'materialize', 'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js', array (), 0.1, true );
	wp_register_script( 'hammer', 'https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js', array (), 0.1, true );
	wp_register_script( 'fullpage', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.2/jquery.fullPage.min.js', array (), 0.1, true );
	wp_register_script( 'fullpage_extensions', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/libs/fullpage/jquery.fullpage.extensions.min.js', array (), $ver2_js, true );
	wp_register_script( 'scrolloverflow', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/libs/fullpage/scrolloverflow.min.js', array (), $ver2_js, true );
	wp_register_script( 'bigtext', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/libs/bigtext.js', array (), $ver2_js, true );

	//Custom JS files
	wp_register_script( 'ver2_homepage_plugins', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/ver2_pluginshome.min.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_global_all', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/global-all.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_search', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/search.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_home', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/home.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_search_common_ui', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/search-common-ui.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_social_login', PEDAGOGE_ASSETS_URL . '/js/social_login.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_modals_callback', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/modals_callback.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_profile', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/profile.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_staticpages', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/staticpages.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_aboutus', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/aboutus.js', array (), $ver2_js, true );
	wp_register_script( 'ver2_institution', PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL . '/js/institution.js', array (), $ver2_js, true );

	#</Themes version 2 files ends>


	#<Old Themes (version 1) files start>
	//Register Stylesheets
	wp_register_style( 'bootstrap-style', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css' );
	wp_register_style( 'jqueryui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css' );
	wp_register_style( 'jqueryuihome', $PEDAGOGE_THEME_URL . '/assets/css/homepage/jquery-ui.css' );
	wp_register_style( 'fontawesome-style', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.1/css/font-awesome.min.css' );

	wp_register_style( 'pedagoge_plugins-style', $PEDAGOGE_THEME_URL . '/assets/css/plugins.css' );
	wp_register_style( 'pedagoge_plugins_home', $PEDAGOGE_THEME_URL . '/assets/css/homepage/plugins.min.css' );

	wp_register_style( 'comingsoon_plugins-style', $PEDAGOGE_THEME_URL . '/comingsoonassets/css/plugins.css' );
	wp_register_style( 'comingsoon_main-style', $PEDAGOGE_THEME_URL . '/comingsoonassets/css/main.css' );
	wp_register_style( 'comingsoon_theme-style', $PEDAGOGE_THEME_URL . '/comingsoonassets/css/themes.css' );

	wp_register_style( 'pdg_main-style', $PEDAGOGE_THEME_URL . '/assets/css/main.css' );
	wp_register_style( 'pdg_theme-style', $PEDAGOGE_THEME_URL . '/assets/css/themes.css' );
	wp_register_style( 'pdg_custom-style', $PEDAGOGE_THEME_URL . '/assets/css/custom.css', array (), $ver = '1.4' );

	wp_register_style( 'pdg_main_home', $PEDAGOGE_THEME_URL . '/assets/css/homepage/main.min.css' );
	wp_register_style( 'pdg_theme_home', $PEDAGOGE_THEME_URL . '/assets/css/homepage/themes.css' );
	wp_register_style( 'pdg_custom_home', $PEDAGOGE_THEME_URL . '/assets/css/homepage/custom.min.css', array (), $ver = '1.4' );
	wp_register_style( 'forteachers_styles', $PEDAGOGE_THEME_URL . '/assets/css/forteachers.css', array (), $ver = '1.5' );
	wp_register_style( 'validationEngine_custom_styles', $PEDAGOGE_THEME_URL . '/assets/css/validationEngine.jquery.css', array (), $ver = '1.4' );
	wp_register_style( 'validationEngine_styles', $PEDAGOGE_THEME_URL . '/assets/css/validationEngine/validationEngine.jquery.css', array (), $ver = '1.4' );
	wp_register_style( 'forteachers_completer', $PEDAGOGE_THEME_URL . '/assets/css/completer.min.css', array (), $ver = '1.4' );

	wp_register_style( 'select2', $PEDAGOGE_THEME_URL . '/assets/css/select2.css', array (), $ver = '1.4' );
	wp_register_style( 'select2home', $PEDAGOGE_THEME_URL . '/assets/css/homepage/select2.css', array (), $ver = '1.4' );
	wp_register_style( 'homepage', $PEDAGOGE_THEME_URL . '/assets/css/homepage/minified/all_min.css', array (), $ver = '1.6' );

	wp_register_style( 'pdg_marketingcamp', $PEDAGOGE_THEME_URL . '/assets/css/marketingcamp.css', array (), $ver = '2.0' );
	wp_register_style( 'pdg_moviescamp', $PEDAGOGE_THEME_URL . '/assets/css/moviescamp.css', array (), $ver = '2.0' );
	wp_register_style( 'aboutus', $PEDAGOGE_THEME_URL . '/assets/css/about-us.css', array (), $ver = '1.8' );

	wp_register_style( 'pdg_careers', $PEDAGOGE_THEME_URL . '/assets/css/careers.css', array (), $ver = '1.2' );
	wp_register_style( 'pdg_business', $PEDAGOGE_THEME_URL . '/assets/css/business.css', array (), $ver = '1.3' );
	wp_register_style( 'webui_popover', $PEDAGOGE_THEME_URL . '/assets/css/webui-popover.min.css', array (), $ver = '1.2' );
	wp_register_style( 'pedagoge_landingjul09', $PEDAGOGE_THEME_URL . '/assets/css/landingjul09.css', array (), $ver = '1.3' );
	wp_register_style( 'thankyou-payment', $PEDAGOGE_THEME_URL . '/assets/css/thankyou-payment.css', array (), $ver = '1.3' );


	//Register JavaScripts
	wp_deregister_script( 'jquery' );    //deregister jquery registered by WordPress
	wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js', array (), false, true );
	wp_register_script( 'bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js', array (), false, true );
	wp_register_script( 'plugins', $PEDAGOGE_THEME_URL . '/assets/js/plugins.js', array (), $common_js_version, true );
	wp_register_script( 'app', $PEDAGOGE_THEME_URL . '/assets/js/app.js', array (), $common_js_version, true );
	wp_register_script( 'backstretch', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js', array (), false, true );
	wp_register_script( 'modernizr', $PEDAGOGE_THEME_URL . '/assets/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js', array (), $common_js_version, true );

	wp_register_script( 'jqueryui', $PEDAGOGE_THEME_URL . '/assets/jquery-ui-1.11.4.custom/jquery-ui.min.js', array (), $common_js_version, true );

	wp_register_script( 'aboutus', $PEDAGOGE_THEME_URL . '/assets/js/aboutus.js', array (), '1.4', true );
	wp_register_script( 'contactus', $PEDAGOGE_THEME_URL . '/assets/js/contact_us.js', array (), $custom_js_version, true );

	wp_register_script( 'select2', $PEDAGOGE_THEME_URL . '/assets/js/select2.min.js', array (), '1.1', true );

	wp_register_script( 'homepage_plugins', $PEDAGOGE_THEME_URL . '/assets/js/pluginshome.min.js', array (), $custom_js_version, true );
	wp_register_script( 'forteachers_plugins', $PEDAGOGE_THEME_URL . '/assets/js/forteachers.js', array (), '1.4', true );
	wp_register_script( 'validationEngine_custom_script', $PEDAGOGE_THEME_URL . '/assets/js/jquery.validationEngine.js', array (), '1.2', true );
	wp_register_script( 'validationEngine_custom_script_en', $PEDAGOGE_THEME_URL . '/assets/js/jquery.validationEngine-en.js', array (), '1.2', true );
	wp_register_script( 'validationEngine_script', $PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine.js', array (), '1.2', true );
	wp_register_script( 'validationEngine_script_en', $PEDAGOGE_THEME_URL . '/assets/js/validationEngine/jquery.validationEngine-en.js', array (), '1.2', true );
	wp_register_script( 'pdg_careers', $PEDAGOGE_THEME_URL . '/assets/js/careers.js', array (), '1.3', true );
	wp_register_script( 'pdg_business', $PEDAGOGE_THEME_URL . '/assets/js/webui-popover.min.js', array (), '1.6', true );
	wp_register_script( 'webui_popover', $PEDAGOGE_THEME_URL . '/assets/js/business.js', array (), '1.6', true );
	wp_register_script( 'pedagoge_completer', $PEDAGOGE_THEME_URL . '/assets/js/completer.js', array (), '1.2', true );
	wp_register_script( 'pedagoge_landingjul09', $PEDAGOGE_THEME_URL . '/assets/js/landingjul09.js', array (), '1.3', true );
	wp_register_script( 'thankyou-payment', $PEDAGOGE_THEME_URL . '/assets/js/thankyou-payment.js', array (), '2.0', true );
	#</Old Themes (version 1) files ends>
}