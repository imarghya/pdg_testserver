  <div class="footer_wrapper">
              	<div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-12 footer_navigation">
                    	<ul>
                            <li><a href="<?php echo home_url('/terms'); ?>">Terms & Conditions</a></li>
                            <li><a href="<?php echo home_url('/privacy-policy'); ?>">Privacy policy</a></li>
                            <?php /*?><li><a href="<?php echo home_url('/sitemap.xml'); ?>">Site map</a></li><?php */?>
                            <li><a href="<?php echo home_url('/about-us'); ?>">About Us</a></li>
                            <li><a href="<?php echo home_url('/business'); ?>">Business</a></li>
                            <li><a href="<?php echo home_url('/contact-us'); ?>">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-3 col-sm-3 col-xs-12 contact_us">
                    	<h4>Powered by</h4>
                        <a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/logo_1000.png" alt="Logo 1000" class="margin_top_20">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 contact_us">
                    	<h4>Selected for</h4>
                        <a href="#">
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/logo_plan.png" alt="Logo 1000">
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 contact_us">
                    	<h4>Selected for</h4>
                        <a href="#">
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/logo_box.png" alt="Logo 1000">

                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 contact_us">
                    	<h4>Backed by</h4>
                        <a href="#">
                        	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/logo_star.png" alt="Logo 1000">
                        </a>
                    </div>
                </div>
                
                <div class="row">
                	<div class="col-xs-12 col-sm-3 col-md-3 col-lg-4 footer_social">
                    	<h3>Follow Us</h3>
                        <ul class="social_logo">
                        	<li><a href="https://www.facebook.com/pedagoge0/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/pedagogebaba" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/pedagoge" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/pedagogebaba/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4 text-center footer_social">
                    	<p>&copy; Pedagoge 2017</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 footer_social">
                    	<h3>Subscribe to our Newsletter</h3>
                        <div class="input-group">
                             <input class="form-control" id="sub_newsletter" placeholder="Enter your email address" type="text">
                             <span class="input-group-btn">
                                 <button class="btn search" id="subscribe_btn" type="button">GO</button>
                             </span>
                        </div>
                    </div>
                </div>
              </div>
              
            </div>
	    
	    
        </section>
        
      </div>
    </div>
    	<div class="container">
    	    <div class="row"  id="stickyfooter">
                  	<div class="col-xs-12 answer_all_question hidden-xs visible-lg visible-md visible-sm">
                    	<figure>
                        	<div class="comment_box">
                            	<a href="#" data-toggle="modal" data-target="#student_teacher_ans_qus">
                            	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/comment_box.png" alt="Comment Box">
                                </a>
                            </div>
                            <div class="cartoon_boy">
                            	<button type="button" class="no_show">X</button>
                                <a href="#" data-toggle="modal" data-target="#student_teacher_ans_qus">
                            	<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/model_boy.png" alt="Boy">
                                    </a>
                            </div>
                        	<figcaption>
                            	<a href="#" data-toggle="modal" data-target="#student_teacher_ans_qus"><p>I've got answers to all your questions...</p></a>
                            </figcaption>
                        </figure>
                    </div>
              </div>
		</div>
    
        <aside class="qustion_answer_model_wrapper">
        <div class="modal fade" id="student_teacher_ans_qus" role="dialog">
           <div class="modal-dialog">
             <!-- Modal content-->
                <div class="modal-content">
                    <!--<div class="modal-header background_blue">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>-->
                    <form action="" method="post">
                      <div class="modal-body">
                         <div class="row">
                             <div class="col-sm-12 qustion_answer">
                             	
                                
                                <div id="myCarousel" class="carousel slide container-fluid no_padding" data-ride="carousel">
                                  <!-- Indicators -->
                                  <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                      <div class="container">
                                        <div class="carousel-caption model_boy">
                                            <p>
                                                Introducing Pedagoge baba, the smart and loveable nerd that we all know and admire. The guy who doesn't just lead the school football team to glory but also features regularly on the merit rolls.
						He is someone you introduce as "one of the smartest people I know". In short, he is the modern day 'class ka baba' personified!
                                            </p>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/boy.png" alt="Student">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="item">
                                      <div class="container">
                                        <div class="carousel-caption model_boy">
					  <?php
					  $cat='';
					  if(is_page('landing-students')){
					   $cat='parentsstudents';
					   }
					   else{
					   $cat='teacher';	      
					   }
					  ?>
                                        	<div class="second_part">
                                        	<h3>Frequently Asked Questions
						<?php if(is_page('landing-students')){ ?>
						<span>Parent/Student Related</span>
						<?php }else{ ?>
						<span>Teacher Related</span>
						<?php } ?>
						</h3>
                                         	<div class="student_teacher_tab">
							<?php
							$args = array('post_type' => 'faqconc');
							$loop = new WP_Query($args);
              
							if($loop->have_posts()) {
							while($loop->have_posts()) : $loop->the_post();
								$post_id=get_the_id();
							        $getProductCat = get_the_terms( $post_id, 'faqconc_cat' );
							       $pcat= $getProductCat[0]->slug;
							 if($pcat==$cat){ 
							?>
					
                                                       <button class="accordion"><?php echo get_the_title(); ?></button>
							<div class="panel">
							  <p><?php echo get_the_content(); ?></p>
							</div>
                                                       <?php } endwhile; } ?>
                                             </div>
                                             </div>
                                             
                                             <div class="second_part_cartoon">
                                             		<img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/boy1.png" alt="Student">
                                             </div>
                                             
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/arrow_left.png" alt="Arrow Left">
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/landing/images/arrow_right.png" alt="Arrow Right">
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>
                                
                                
                                
                                
                             </div>
                         </div>
                     </div>
                     <!--<div class="modal-footer margin_top_10 margin_bottom_10">
                                      
                 	</div>-->
                  </form>
             </div>
           </div>
        </div>
     </aside>


     <aside class="qustion_answer_model_wrapper student_searching_model_wrapper">
        <div class="modal fade" id="student_searching_for" role="dialog">
           <div class="modal-dialog" id="md_content">
             <!-- Modal content-->
           </div>
        </div>
     </aside>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/alertify.core.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/landing/css/alertify.default.css">

 <!-- Javascript files-->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/bootstrap.min.js"></script>
    <!--<script src="<?php //echo get_template_directory_uri(); ?>/assets/landing/js/jquery.stickyelement.js"></script>-->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/jquery.onepage-scroll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/front.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/jquery.meanmenu.js"></script>
    
    <script src="<?php echo get_template_directory_uri(); ?>/assets/landing/js/alertify.min.js"></script>
    
    <script type="text/javascript">
        
    /* Owl Model */

<!-- Multyple Image Slider -->

var owl = $('.owl-carousel');
      owl.owlCarousel({
        margin: 10,
		/*autoplay: true,*/
        autoplayTimeout: 2000,
        loop: true,
		nav: true,
		navText : ["<img src='<?php echo get_template_directory_uri(); ?>/assets/landing/images/myprevimage.png'>","<img src='<?php echo get_template_directory_uri(); ?>/assets/landing/images/mynextimage.png'>"],
        responsive: {
          0: {
            items: 1
          },
		  480: {
            items: 2
          },
		  640: {
            items: 3
          },
		  768: {
            items: 3
          },
		  960: {
            items: 4
          }
        }
      })
	  
	  $('.owl-carousel').on('mouseleave',function(e){
    owl.trigger('play.owl.autoplay');
})
$('.owl-carousel').on('mouseover',function(e){
    owl.trigger('stop.owl.autoplay');
})    
        
        
        
$('.carousel').carousel({
	interval: false
}); 


$(document).ready(function(){
$(".no_show").click(function(){
    $("#stickyfooter").hide();
});

//$(window).scroll(function() {
//   if($(window).scrollTop() + $(window).height() == $(document).height()) {
//       alert("bottom!");
//       // getData();
//   }
//});

});


/* Slim Scroll */
$(function(){
					$('.student_teacher_tab').slimScroll({
						height: '260px',
						width: '98%',
						size: '8px',
						alwaysVisible: true,
						distance: '0px',
						color: '#2a3543',
						opacity: 1,
						wheelStep: 50,
						distance: '0',
						wheelStep: 50
					});
				});
				
				
				
/* Slim Scroll */

</script>
<script type="text/javascript" charset="utf-8">
var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>';
var $ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
var $pedagoge_callback_class = 'ControllerQueries';
var $pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler';
var $pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler';
</script>
<script>
$(document).ready(function(){
 //For Save query-----------------------------------------
 $(document).on('click', '.get_land_query', function(e){
  e.preventDefault();
   var query_id=$(this).attr("q_id");
   //call ajax-----------------------------------------------
        var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'get_land_query',
        pedagoge_callback_class: 'ControllerQueries',
        query_id : query_id,
        };
          var dataToPost='';
          $.post($ajax_url, submit_data)
            .done(function(response, status, jqxhr){
               // alert(response);
		$("#md_content").html(response);
		$("#student_searching_for").modal("show");
            })
            .fail(function(jqxhr, status, error){ 
               //alert(error);
            });
         //--------------------------------------------------------
 });	 

$(document).on("click","#subscribe_btn",function(){
  
  var email_id = $("#sub_newsletter").val();
  //alert(email_id);

if ($.trim(email_id).length == 0) {
  alertify.error("Error: Email field is mandatory !!!");
  e.preventDefault();
}
if (validateEmail(email_id)) {
var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'add_email_subscriber',
        pedagoge_callback_class: 'ControllerQueries',
        email_id : email_id,
    };
    
var dataToPost='';
$.post($ajax_url, submit_data)
.done(function(response, status, jqxhr){
//alertify.set('notifier','position', 'bottom-right');
$("#sub_newsletter").val('');
alertify.success("Success: Thankyou for Subscribing to our Newsletter !!");
})
.fail(function(jqxhr, status, error){ 
//alertify.set('notifier','position', 'bottom-right');
alertify.error("Error: Something went wrong !!!");
});
}
else {
alertify.error("Error: Invalid Email Address !!!");
}


  /*if($.trim(email_id)!=''){

    var submit_data = {
        nonce: $ajaxnonce,
        action: $pedagoge_visitor_ajax_handler,
        pedagoge_callback_function: 'add_email_subscriber',
        pedagoge_callback_class: 'ControllerQueries',
        email_id : email_id,
    };
    
    var dataToPost='';
    $.post($ajax_url, submit_data)
      .done(function(response, status, jqxhr){
        alertify.set('notifier','position', 'bottom-right');
    alertify.success("Success: Thankyou for Subscribing to our Newsletter !!");
    })
    .fail(function(jqxhr, status, error){ 
      alertify.set('notifier','position', 'bottom-right');
        alertify.error("Error: Something went wrong !!!");
    });
  }
 else{
  alertify.set('notifier','position', 'bottom-right');
    alertify.error("Error: Please Insert the Email ID !!!");
  }*/

});

});

function validateEmail(sEmail) {
  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
  if (filter.test(sEmail)) {
    return true;
  }
  else {
    return false;
  }
}
</script>

<?php
if(is_user_logged_in())
{
$user_id = get_current_user_id();
$user = new WP_User( $user_id );
$user_role = $user->roles[0];
if($user_role == "teacher")
{
?>
<script type="text/javascript">
    $(document).on("click",".sinterest",function(){
        window.location.href= "<?php echo home_url('/teacherdashboard'); ?>";
    });

    $(document).on("click",".see_more",function(){
        window.location.href= "<?php echo home_url('/teacherdashboard'); ?>";
    });
</script>
<?php
}
else
{
?>
<script type="text/javascript">
    $(document).on("click",".sinterest",function(){
      alert("To Show interest you will have to log in as a Teacher.");
    });

    $(document).on("click",".see_more",function(){
      alert("You are not logged in as a Teacher . To See More you will have to log in as a Teacher.");
    });
</script>

<?php
}
}
else
{
?>
<script type="text/javascript">
    $(document).on("click",".sinterest",function(){
        window.location.href= "<?php echo home_url().'/login/?nexturl='; ?>";
    });

    $(document).on("click",".see_more",function(){
        window.location.href= "<?php echo home_url().'/login/?nexturl='; ?>";
    });
</script>
<?php
}
?>
  </body>
</html>