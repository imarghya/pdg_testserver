<?php
/*
Template Name: Careers - Sales & Operations Head
*/

global $isVersion2;

$isVersion2 = true;
get_header( 'home_ver2' );
$theme_dir = get_template_directory_uri();
$joburl = get_home_url() . "/careers";
?>
	<section class="wrapper container-fluid section1-careers">
		<div class="row  header-careers bgImg ">
			We are hiring!
		</div>
	</section>
	<section class="wrapper container-fluid section2-careers">
		<div class="row">
			<div class="col-xs-12 joinHeader titlePadding text-center">
				<i class="fa fa-briefcase" aria-hidden="true"></i> Job Description
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle2">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;Sales
							& Operations Head
						</div>
						<div class="opPara1 jobDescPara">
							<p>Pedagoge for Institution is a dedicated B2B platform of Pedagoge where schools and
								institutions</p>

							<p>partner with Pedagoge with the objective of conducting workshops and engaging
								specialized</p>

							<p>trainers to improve the quality of students in the institution.</p>

							<p>&nbsp;</p>

							<p>We are looking for a Sales &amp; Operations Head for Kolkata with experience in helping a
								business</p>

							<p>grow and the ability to create business plans for our future.</p>

							<p>&nbsp;</p>

							<p><strong>Responsibilities:</strong></p>

							<p><span>&bull; </span>Managing a team of people responsible for generating revenue and
								business</p>

							<p>opportunities for the company</p>

							<p><span>&bull; </span>Motivating the team to manage relationships and on-board vendors to
								successfully</p>

							<p>achieve targets</p>

							<p><span>&bull; </span>Become a part of the core committee of the company to forward
								suggestions, implement</p>

							<p>new ideas and analyze performance</p>

							<p><span>&bull; </span>Research on schools, colleges and institutions to evaluate the scope
								of becoming their</p>

							<p>trusted vendors for outsourcing extra-curricular workshops or teacher hiring</p>

							<p><span>&bull; </span>Interacting with clients, customers, prospects by doing field work
							</p>

							<p><span>&bull; </span>Building and managing a team of interns and full timers to landscape
								the city and physically</p>

							<p>on-board private teachers/institutes from academic &amp; non-academic domain</p>

							<p><span>&bull; </span>Prepare weekly reports on progress and make necessary recommendations
								to the</p>

							<p>management</p>

							<p><span>&bull; </span>Stay connected &ndash; Constant networking with influencers in the
								city from the industry</p>

							<p>&nbsp;</p>

							<p><strong>Requirements:</strong></p>

							<p><span>&bull; </span>Degree in marketing, communications or business
							</p>

							<p><span>&bull; </span>Minimum of 1 year working experience in fields relating to
								analytics/business</p>

							<p>development/consulting/sales/ecommerce</p>

							<p><span>&bull; </span>Past experience of leading a team of more than 5 people</p>

							<p><span>&bull; </span>Knowledge of ecommerce and internet-based business models</p>

							<p><span>&bull; </span>Advanced MS Excel experience is a must</p>

							<p><span>&bull; </span>Excellent verbal and written communications skills in English,
								Hindi/Bengali Strong</p>

							<p>analytical skills, detail-oriented and a self-starter</p>

							<p><span>&bull; </span>Proven ability to meet deadlines and ability to work in a fast-paced
								environment</p>

							<p><span>&bull; </span>Must be able to manage and organize multiple projects at one time</p>
							<p>&nbsp;</p>
							<p><strong>Location</strong>: Kolkata</p>

							<p>&nbsp;</p>

							<p>If some of the requirements don&rsquo;t resonate with you, but you still think you would
								be a good fit for</p>

							<p>the role or want to talk about other roles, please get in touch with us.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-6 opWrapper">
					<div class="opCard">
						<div class="opCardHeader opCardHeaderStyle3">&nbsp;<i class="fa fa-circle"
						                                                      aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Apply
							to us
						</div>
						<div class="opPara1 jobDescPara">
							<p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;careers@pedagoge.com
							</p>
							<p><i class="fa fa-location-arrow" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;EN - 12, 4th
								Floor, Sector V,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Salt Lake, Kolkata -
								700091,<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;West Bengal, India</p></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer( 'home_ver2' );