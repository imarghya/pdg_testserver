<?php
/**
 * The template for displaying the footer - version 2 theme..
 */
global $post;

$slug = '';
if ( isset( $post ) ) {
	$current_post = get_post( $post );
	if ( ! empty( $current_post ) ) {
		$slug = $current_post->post_name;
	}
}

$allowSponsoredLogos = false;
if ( is_front_page() ) {
	$allowSponsoredLogos = true;
}
/*$excludeheader = array ( 'terms' );
if ( $slug != "" && ! in_array( $slug, $excludeheader ) ) {
	$allowFixedSearch = true;
} elseif ( $slug == "" ) {
	$allowFixedSearch = true;
}*/

?>
<!-- Custom Javascript Variables -->
<script type="text/javascript">
	var $ajaxurl = $ajax_url = '<?php echo admin_url( "admin-ajax.php" ); ?>',
		$pedagoge_users_ajax_handler = 'pedagoge_users_ajax_handler',
		$pedagoge_visitor_ajax_handler = 'pedagoge_visitor_ajax_handler',
		$ajaxnonce = '<?php echo wp_create_nonce( "pedagoge" ); ?>';
</script>
<?php
wp_footer();
?>
<footer class="site-footer">
	<div class="container-fluid">
		<div class="row logos">
			<div class="hidden-xs col-sm-4 pedagoge_logo"><a class="navbar-brand" href="<?= home_url() ?>"><img
							src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Pedagoge.svg"
							onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Pedagoge.png';this.className='valign responsive-png'"></a>
			</div>
			<div class="col-sm-4 pedagoge_sm hidden-xs"></div>
			<div class="col-xs-12 col-sm-4 nasscom_logo">
				<div class="row">
					<div class="col-xs-12">
						<?php get_template_part( 'template-parts/ver2/social_media_home' ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-center center-block margin-top-20">
			<div class="col-sm-2"><a href="<?= home_url( 'terms' ) ?>" class="white-text notext-decoration">Terms and
					Conditions</a></div>
			<div class="col-sm-2"><a href="<?= home_url( 'privacy-policy' ) ?>" class="white-text notext-decoration">Privacy
					Policy</a></div>
			<div class="col-sm-2"><a href="<?= home_url( 'sitemap.xml' ) ?>" class="white-text notext-decoration">Site
					Map</a></div>
			<div class="col-sm-2"><a href="<?= home_url( 'careers' ) ?>"
			                         class="white-text notext-decoration" id="gtm_career">Careers</a></div>
			<div class="col-sm-2"><a href="<?= home_url( 'business' ) ?>"
			                         class="white-text notext-decoration" id="gtm_Business">Business</a></div>
			<div class="col-sm-2"><a href="<?= home_url( 'forteachers' ) ?>" class="white-text notext-decoration" id="gtm_RAT">Register
					as Teacher</a></div>
		</div>
		<?php if ( $allowSponsoredLogos ) { ?>
			<div class="row text-center center-block margin-top-10 section-association hidden-xs hidden-sm">
				<div class="col-xs-4 center-block text-center"><img alt="Nasscom StartUp"
				                                                    title="Nasscom StartUp"
				                                                    data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Nasscom_StartUp.svg?1"
				                                                    onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Nasscom_StartUp.png?1';this.className='valign responsive-png'">
				</div>
				<div class="col-xs-4 center-block text-center"><img alt="Product Conclave"
				                                                    title="Product Conclave"
				                                                    data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/Product_conclave.svg?1"
				                                                    onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/Product_conclave.png?1';this.className='valign responsive-png'">
				</div>
				<div class="col-xs-4 center-block text-center"><img alt="StartUp India"
				                                                    title="StartUp India"
				                                                    data-src="<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/svgs/StartUpIndia.svg?1"
				                                                    onerror="this.onerror=null;this.src='<?= PEDAGOGE_THEME_V2_DESKTOP_ASSETS_URL ?>/images/logos/pngs/StartUpIndia.png?1';this.className='valign responsive-png'">
				</div>
			</div>
		<?php } ?>
		<div class="row text-center center-block margin-top-10">
			&copy; Pedagoge <?= date( 'Y' ) ?>
		</div>
	</div>
</footer>
</div>
<?php
wp_footer();
?>
</body>
<?php
get_template_part( 'template-parts/google', 'tag' ); ?>
</html>