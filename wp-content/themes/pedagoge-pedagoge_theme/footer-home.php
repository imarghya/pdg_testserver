<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pedagoge_theme
 */


?>


<!--
########  #######   #######  ######## ######## ########
##       ##     ## ##     ##    ##    ##       ##     ##
##       ##     ## ##     ##    ##    ##       ##     ##
######   ##     ## ##     ##    ##    ######   ########
##       ##     ## ##     ##    ##    ##       ##   ##
##       ##     ## ##     ##    ##    ##       ##    ##
##        #######   #######     ##    ######## ##     ##
-->


<footer class="site-footer site-section footer-bottom-padding hide-footer">
	<div class="container">
		<!-- Footer Links -->
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<br><br>
				<a href="<?php echo home_url('/'); ?>" target="_blank" title="Pedagoge"><img
						src="<?php echo get_template_directory_uri(); ?>/assets/img/Footer/home_35.png"
						class="img-responsive center hide-logo-footer" alt="Pedagoge Logo"></a>
				<div style="display: none;" class="changeto-business-logo-footer">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/business/logo-business.png"
					     align="center" class="img-responsive center" alt="Pedagoge Logo">
				</div>
			</div>
			<div class="col-sm-6 col-md-4 hidden-sm">
				<h4 class="footer-heading text-center" style="line-height: 0.2em;">Follow Us</h4>
				<ul class="footer-nav footer-nav-social list-inline text-center">
					<li><a href="https://www.facebook.com/pedagoge0/" target="_blank"><i class="fa fa-facebook"></i></a>
					</li>
					<li><a href="https://twitter.com/pedagogebaba" target="_blank"><i class="fa fa-twitter"></i></a>
					</li>
					<li><a href="https://www.linkedin.com/company/pedagoge" target="_blank"><i
								class="fa fa-linkedin"></i></a></li>
					<li><a href="https://www.instagram.com/pedagogebaba/" target="_blank"><i
								class="fa fa-instagram"></i></a></li>
				</ul>


			</div>
			<div class="col-sm-6 col-md-4">
				<br><br>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/Footer/home_52.png"
				     class="img-responsive center" alt="NASSCOM 10,000 Start up">
			</div>
		</div>


		<div class="row">
			<div class="col-sm-12 hidden-xs hidden-md hidden-lg">
				<h4 class="footer-heading text-center" style="line-height: 0.2em;">Follow Us</h4>
				<ul class="footer-nav footer-nav-social list-inline text-center">
					<li><a href="https://www.facebook.com/pedagoge0/" target="_blank"><i class="fa fa-facebook"></i></a>
					</li>
					<li><a href="https://twitter.com/pedagogebaba" target="_blank"><i class="fa fa-twitter"></i></a>
					</li>
					<li><a href="https://www.linkedin.com/company/pedagoge" target="_blank"><i
								class="fa fa-linkedin"></i></a></li>
					<li><a href="https://www.instagram.com/pedagogebaba/" target="_blank"><i
								class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
		</div>
		<br>


		<div class="row footer-nav">
			<?php
			global $post;
			$slug = '';
			if (isset($post)) {
				$current_post = get_post($post);
				if (!empty($current_post)) {
					$slug = $current_post->post_name;
				}
			}
			$excludeheader = array ( 'business' );
			if (!in_array($slug, $excludeheader)) {
				?>
				<div class="col-md-2 text-center">
					<a href="<?php echo home_url('/terms'); ?>">Terms &amp; Conditions</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo home_url('/privacy-policy'); ?>">Privacy Policy</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo home_url('/affiliates'); ?>">Affiliate with us</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo home_url('/contact-us'); ?>">Contact Us</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo home_url('/careers'); ?>">Careers</a>
				</div>
				<div class="col-md-2 text-center">
					<a href="<?php echo home_url('/sitemap.xml'); ?>">Site Map</a>
				</div>
				<?php
			}
			else { ?>
				<div class="col-md-4 text-center">
					<a href="<?php echo home_url(''); ?>">Main Site</a>
				</div>
				<div class="col-md-4 text-center">
					<a href="<?php echo home_url('/terms'); ?>">Terms &amp; Conditions</a>
				</div>
				<div class="col-md-4 text-center">
					<a href="<?php echo home_url('/privacy-policy'); ?>">Privacy Policy</a>
				</div>
			<?php }

			?>


		</div>
		<br>
		<div class="text-center">
			<a href="<?php echo home_url('/'); ?>" target="_blank"
			   title="Pedagoge">Pedagoge</a> &copy; <?php echo date('Y'); ?>
		</div>
		<!-- END Footer Links -->
	</div>
</footer>
<!-- END Footer -->
</div> <!-- Div page-container ends here -->
<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>


<?php
global $wpdb;
$subject_data = PDGManageCache::fn_load_cache('registered_subjects');


$str_found_subjects = '';
if (!empty($subject_data)) {
	$subjects_data_array = array ();
	foreach ($subject_data as $subjects) {
		$subject_name = $subjects->subject_name;
		if (!in_array($subject_name, $subjects_data_array)) {
			$subjects_data_array[] = $subject_name;
		}
	}
	$str_found_subjects = implode(',', $subjects_data_array);
}
wp_footer();
?>
<div id="div_hidden_subjects_list" style="display:none;"><?php echo $str_found_subjects; ?></div>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/assets/js/custom-script-all.js?ver=1.7'></script>
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/assets/css/custom-styles-all.css?ver=1.5' type='text/css' media='all' />
<script type="text/javascript">
	$(document).ready(function () {
		$('.change-background').backstretch([
			"<?php echo get_template_directory_uri(); ?>/assets/img/slider/school_girl.jpg",
			"<?php echo get_template_directory_uri(); ?>/assets/img/slider/slider_art.jpg",
			"<?php echo get_template_directory_uri(); ?>/assets/img/slider/slider_fitness.jpg",
			"<?php echo get_template_directory_uri(); ?>/assets/img/slider/slider_music.jpg",
		], {duration: 3000, fade: 750});


		//$('#search-locality').select2();


		$('.search-locality').select2({
			placeholder: "Select Locality"
		});


		var $subjects_list_array = $("#div_hidden_subjects_list").html().split(",");


		$(".search_subject").autocomplete({
			source: function (request, response) {
				var results = $.ui.autocomplete.filter($subjects_list_array, request.term);
				response(results.slice(0, 10));
			},
			minLength: 2,
			open: function (event, ui) {
				var suggestElements = $(this).data("uiAutocomplete").menu.element[0].children //Array of all Suggested Results
					, suggestElementsLength = suggestElements.length	//Length of the array of the suggested results
					, inpt = $(this) //Search Bar Object
					, original = inpt.val();	//Search Bar Text


				for (var i = suggestElementsLength; i != -1; i--) {
					var elementText = $($(this).data("uiAutocomplete").menu.element[0].children[i]).text();	//ith Suggested Search Element


					//If there is a match between the Suggested Result and the Searched Term then replace and highlight
					if (elementText.toLowerCase().indexOf(original.toLowerCase()) === 0) {
						inpt.val(elementText);//change the input to the first match
						inpt[0].selectionStart = original.length;//highlight from end of input
						inpt[0].selectionEnd = elementText.length;//highlight to the end
					}
				}
			}
		});


		$(".search_subject").keypress(function (e) {
			if (e.which == 13) {
				var $subject_name_length = $(this).val().length;
				if ($subject_name_length > 2) {
					$(".cmd_home_search_btn").trigger("click");
				}
			}
		});


		var $ajaxurl = $ajax_url = '<?php echo admin_url("admin-ajax.php"); ?>';
		var $ajaxnonce = '';
		var submit_data = {
			action: 'pedagoge_visitor_ajax_handler',
			pedagoge_callback_function: 'fn_load_secret_key',
			pedagoge_callback_class: 'PDGContactForm',
		};


		$.post($ajax_url, submit_data, function (response) {
			$ajaxnonce = response;
			$('.ajaxnonce').val($ajaxnonce);
		});


		$(".cmd_home_search_btn").click(function (event) {
			var $subject_name = $(".search_subject").val();
			var $locality_name = $('.search-locality').val();
			var values_for_parameters = [];
			values_for_parameters.push(encodeURIComponent('pdg_subject') + "=" + encodeURIComponent($subject_name));
			values_for_parameters.push(encodeURIComponent('pdg_locality') + "=" + encodeURIComponent($locality_name));
			var $url_parameters = values_for_parameters.join("&");
			var $search_page_url = '<?php echo home_url('/search'); ?>';
			var $search_url = $search_page_url + '?' + $url_parameters;
			if ($subject_name.length <= 0 && $locality_name.length <= 0) {
				window.location = $search_page_url;
			} else {
				window.location = $search_url;
			}
		});
		$(".cmd_home_search_btn1").click(function (event) {
			var $subject_name = $(".search_subject1").val();
			var $locality_name = $('.search-locality1').val();
			var values_for_parameters = [];
			values_for_parameters.push(encodeURIComponent('pdg_subject') + "=" + encodeURIComponent($subject_name));
			values_for_parameters.push(encodeURIComponent('pdg_locality') + "=" + encodeURIComponent($locality_name));
			var $url_parameters = values_for_parameters.join("&");
			var $search_page_url = '<?php echo home_url('/search'); ?>';
			var $search_url = $search_page_url + '?' + $url_parameters;
			if ($subject_name.length <= 0 && $locality_name.length <= 0) {
				window.location = $search_page_url;
			} else {
				window.location = $search_url;
			}
		});


		$('#btn-scroll').click(function () {
			$('html, body').animate({
				scrollTop: $("#promo1").offset().top
			}, 1500);
			return false;
		});
	});
</script>
<input type="hidden" class="ajaxnonce" value="" style="display: none;">
<input type="hidden" class="siteUrl" value="<?= get_site_url(); ?>" style="display: none;">
<?php get_template_part('template-parts/google', 'tag'); ?>
</body>
</html>